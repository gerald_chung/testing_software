//---------------------------------------------------------------------------

#ifndef NI6210_USB_H
#define NI6210_USB_H

#include <vcl.h>
#include "NIDAQmx.h"
#include <stdio.h>
#include  <io.h>
#include <setupapi.h>
#include "Utils.h"


//-----------------------------------------------------------------------
// definitions
//-----------------------------------------------------------------------
#define DAQmxErrChk(functionCall) ((DAQmxFailed(functionCall)) ? NI6210_ERR : NI6210_OK)

// #define SAMPLES_TO_READ (500)
// #define SAMPLE_RATE (10000.0)

#define SAMPLES_TO_READ (1000)
#define SAMPLE_RATE (10000.0)

//-----------------------------------------------------------------------
// types
//-----------------------------------------------------------------------


typedef struct {
	HANDLE readhandle;
	HANDLE writehandle;
	bool connected;
} NI6210_OK_device_t;

typedef enum {
	NI6210_OK = 0x00,
	NI6210_ERR,
	NI6210_OK_HIB_LIB_LOAD_ERROR,
	NI6210_OK_CONNECT_ERROR
} NI6210_ret_t;



//simple controller class to control USBMM_Threads
class NI6210_USB_Interface {

	 public:

		// Start the thread suspended
		__fastcall NI6210_USB_Interface(TMemo *USBLogMemo)
		{
			Memo = USBLogMemo;
			MemoMutex = new TMutex(false);
			NI6210_Handle = 0;
			dataReady = false;
		}

		__fastcall ~NI6210_USB_Interface(void) {
		}

		bool __fastcall Start(void);
		void __fastcall Stop(void);
		bool __fastcall MeterConnected(uint8_t meter);
		float __fastcall GetOutput(uint8_t meter);
		void __fastcall StartNewMeasurement(void);
		bool __fastcall DataAvailable(void);
		void __fastcall AddToLog(AnsiString line);
		void __fastcall DMM_Task(void);
		void __fastcall SetDQA_Address_Name(AnsiString sName)
		{
        sDQA_Address_Name = sName;
		}

	private:

		//Memo for user messages
		TMemo *Memo;
		//Locks for shared resources
		TMutex *MemoMutex;
		//handle for USB daq
		TaskHandle NI6210_Handle;
		//keep track of the latest data
		bool dataReady;
		//latest measurements
		double curValues[4];
//		double newValues[4];
		double newValues[5]; // Add additional dummy value. This channel is shorted to discharge DAQ instrumentation amplifier charge.
		AnsiString sDQA_Address_Name;


		NI6210_ret_t __fastcall GetAnalogInputs(double *buff);

};


//---------------------------------------------------------------------------
#endif
