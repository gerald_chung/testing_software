//---------------------------------------------------------------------------

#ifndef AGILENT_34970A_H
#define AGILENT_34970A_H

#include <vcl.h>
#include "NIDAQmx.h"
#include <stdio.h>
#include  <io.h>
#include <setupapi.h>
#include "Utils.h"
#include "visa.h"


//-----------------------------------------------------------------------
// definitions
//-----------------------------------------------------------------------

#define AGILENT_SAMPLES_TO_READ (2)
//#define AGILENT_GPIB_ADDRESS ("USB0::0x0957::0x2007::MY49013702::0::INSTR")
#define AGILENT_GPIB_ADDRESS ("USB0::0x0957::0x2007::MY49021786::0::INSTR")

//-----------------------------------------------------------------------
// types
//-----------------------------------------------------------------------

typedef enum {
	AGILENT_OK = 0x00,
	AGILENT_ERR,
	AGILENT_OK_HIB_LIB_LOAD_ERROR,
	AGILENT_OK_CONNECT_ERROR
} AGILENT_ret_t;


//simple controller class to control USBMM_Threads
class AGILENT_Interface {

	 public:

		// Start the thread suspended (Constructor)
		__fastcall AGILENT_Interface(TMemo *USBLogMemo)
		{
			Memo = USBLogMemo;
			AGILENT_Session = 0;
			dataReady = false;
		}

		//Destructor
		__fastcall ~AGILENT_Interface(void) {
		}

		bool __fastcall Start(void);
		void __fastcall Stop(void);
		bool __fastcall MeterConnected(uint8_t meter);
		double __fastcall GetOutput(uint8_t meter);
		void __fastcall StartNewMeasurement(void);
		bool __fastcall DataAvailable(void);
		void __fastcall AddToLog(AnsiString line);
		void __fastcall DMM_Task(void);


	private:

		//Memo for user messages
		TMemo *Memo;

		//session for AGILENT daq
		ViSession defaultRM;
		ViSession AGILENT_Session;
		//keep track of the latest data
		bool dataReady;
		//latest measurements
		double curValues[4];
		double newValues[4];

	   	AGILENT_ret_t __fastcall GetAnalogInputs(double *buff);
		void __fastcall DecodeRetString(char *buff, double* retValues);

};


//---------------------------------------------------------------------------
#endif
