//---------------------------------------------------------------------------

#pragma hdrstop

#include "NI6210_USB.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


//============================================================================
//============================================================================
//============================================================================
//		NI6210 USB CONTROLLER FUNCTION IMPLEMENTATIONS
//============================================================================
//============================================================================
//============================================================================


//---------------------------------------------------------------------------
// Add a line to the USB connection log...
//---------------------------------------------------------------------------
void __fastcall NI6210_USB_Interface::AddToLog(AnsiString line)
{
	MemoMutex->Acquire();
	Memo->Lines->Add(line);
	MemoMutex->Release();
}

//---------------------------------------------------------------------------

NI6210_ret_t __fastcall NI6210_USB_Interface::GetAnalogInputs(double *buff)
{
	long read = 0;
	uint32_t i;
	double data[SAMPLES_TO_READ*5];
	NI6210_ret_t result;

   result = DAQmxErrChk (DAQmxStartTask(NI6210_Handle));

	//if task was started
	if(result == NI6210_OK)
	{
		result = DAQmxErrChk (DAQmxReadAnalogF64(NI6210_Handle,-1,10.0,DAQmx_Val_GroupByChannel,&data[0],SAMPLES_TO_READ*5,&read,NULL));
		//if any samples were read.
		if(result == NI6210_OK && read > 0)
		{   //clear buffer
			buff[0] = 0;
			buff[1] = 0;
			buff[2] = 0;
			buff[3] = 0;
			buff[4] = 0;

			//add all samples
			for(i=0; i<(unsigned long)read; i++){
//				buff[0] += data[i];
//				buff[1] += data[i+read];
//				buff[2] += data[i+(2*read)];
//				buff[3] += data[i+(3*read)];

				// Remainder of program expects buff order to be;
				// buff[0] = TxdV
				// buff[1] = TxdI
				// buff[2] = RxdV
				// buff[3] = RxdI
				// However scan order is now TxdI, RxdI, RxdV, TRxdV, ShortedChannel - so copy to appropriate buff[].
				// NOTE: Scan order has been changed to stop DAQ diff amp discharging stored charge from higher readings into lower readings.
				// Shorted channel discharges DAQ diff amp each time the scan completes, but this channel data is discarded.

				//edVinDaqAddr->Text

				buff[1] += data[i]; // TxdI  (Iin)
				buff[3] += data[i+read]; // RxdI (I out)
				buff[2] += data[i+(2*read)]; // RxdV (V out)
				buff[0] += data[i+(3*read)]; // TxdV  (V in)
				buff[4] += data[i+(4*read)]; // Dummy shorted value
			}
			//calculate average
			buff[0] = buff[0] / read; // TxdV
			buff[1] = buff[1] / read; // TxdI
			buff[2] = buff[2] / read; // RxdV
			buff[3] = buff[3] / read; // RxdI
			buff[4] = buff[4] / read; // Dummy shorted value
		}
		//stop task regardless of succes
		DAQmxStopTask(NI6210_Handle);
	}

	return result;
}

//---------------------------------------------------------------------------
// Tell all threads to go and get a new measurement
//---------------------------------------------------------------------------
void __fastcall NI6210_USB_Interface::StartNewMeasurement(void){

	//if the NIDAQ task is available
	if(NI6210_Handle != 0){
		//invalidate current data
		dataReady = false;
		//get new data
		if(GetAnalogInputs(&newValues[0]) != NI6210_OK)
		{
			AddToLog("Error getting new measurement");
			Stop();
		} else {
			curValues[0] = newValues[0]; // TxdV
			curValues[1] = newValues[1]; // TxdI
			curValues[2] = newValues[2]; // RxdV
			curValues[3] = newValues[3]; // RxdI
			// Discard newValues[4]. This is the shorted channel value.
			dataReady = true;
		}
	}

}

//---------------------------------------------------------------------------
// Ask all threads whether new data is available, if so, return true
//---------------------------------------------------------------------------
bool __fastcall NI6210_USB_Interface::DataAvailable(void){
	 return dataReady;
}

//---------------------------------------------------------------------------
// Get the latest output string of a certain Node
//---------------------------------------------------------------------------
float __fastcall NI6210_USB_Interface::GetOutput(uint8_t channel){
	return curValues[channel];
}

//---------------------------------------------------------------------------
// Return connection state for node
//---------------------------------------------------------------------------
bool __fastcall NI6210_USB_Interface::MeterConnected(uint8_t channel)
{
	   return (NI6210_Handle != 0) ? true : false;
}

//---------------------------------------------------------------------------
// Kill all threads
//---------------------------------------------------------------------------
void __fastcall NI6210_USB_Interface::Stop(void)
{
	DAQmxStopTask(NI6210_Handle);
	DAQmxClearTask(NI6210_Handle);
	NI6210_Handle = 0;
	AddToLog("NI6210 Task Stopped");
}

//---------------------------------------------------------------------------
// Attempt to start DAQ analog in task. Return true if successful
//---------------------------------------------------------------------------
bool __fastcall NI6210_USB_Interface::Start(void)
{
int32 Error_Result;
//const char Channel_List [] = "dev1/ai1,dev1/ai0,dev1/ai2,dev1/ai3";

	//create the NI daq task
	NI6210_ret_t result = DAQmxErrChk(DAQmxCreateTask("",&NI6210_Handle));

	//if successful configure and start
	if(result == NI6210_OK){
		AddToLog("NI6210 Task Created");

		// Create NIDAQmx Custom Scale. Needed because we attenuate the TXD Vcc measurement by 2.466. Scale Y=mx+c where m=2.466 c=0
	  Error_Result = DAQmxCreateLinScale("TXDScale", 2.466, 0, DAQmx_Val_Volts, "Volts");
	  // Scale to make current value correlate with other measurement devices
	  Error_Result = DAQmxCreateLinScale("IScale", 10, 0, DAQmx_Val_Volts, "Amps");


		AddToLog("NI6210 Scale Created");

		/*
		 * Create individual channels with different gains. NOTE: This arrangement is not ideal, as measurements should go from low
		 * volts to high volts on the DAQ MUX to improve settling. I've left the arrangement as it is to match the existing code.
		 *
		 * Ch0= TXD volts 10v range with scale. Ch1= TXD current 0.2v range with scal. Ch2= RXD volts 10v range. Ch3= RXD current 0.2v range with scale.
		 */

		// TXD volts and current measurements
/*	  Error_Result = DAQmxCreateAIVoltageChan(NI6210_Handle,AnsiString(sDQA_Address_Name + "/ai0").c_str(),"",DAQmx_Val_Diff,0,24.0,DAQmx_Val_FromCustomScale,"TXDScale");
	  Error_Result = DAQmxCreateAIVoltageChan(NI6210_Handle,AnsiString(sDQA_Address_Name + "/ai1").c_str(),"",DAQmx_Val_Diff,0,0.2,DAQmx_Val_FromCustomScale,"IScale");
		// RXD volts and current measurement
	  Error_Result = DAQmxCreateAIVoltageChan(NI6210_Handle,AnsiString(sDQA_Address_Name  + "/ai2").c_str(),"",DAQmx_Val_Diff,0,10.0,DAQmx_Val_Volts,NULL);
	  Error_Result = DAQmxCreateAIVoltageChan(NI6210_Handle,AnsiString(sDQA_Address_Name + "/ai3").c_str(),"",DAQmx_Val_Diff,0,0.2,DAQmx_Val_FromCustomScale,"IScale");
*/

	 // Scan order is TxdI, RxdI, RxdV, TxdV, DummyShortedChan
	  Error_Result = DAQmxCreateAIVoltageChan(NI6210_Handle,AnsiString(sDQA_Address_Name + "/ai1").c_str(),"",DAQmx_Val_Diff,0,0.2,DAQmx_Val_FromCustomScale,"IScale");
	  Error_Result = DAQmxCreateAIVoltageChan(NI6210_Handle,AnsiString(sDQA_Address_Name + "/ai3").c_str(),"",DAQmx_Val_Diff,0,0.2,DAQmx_Val_FromCustomScale,"IScale");
	  Error_Result = DAQmxCreateAIVoltageChan(NI6210_Handle,AnsiString(sDQA_Address_Name  + "/ai2").c_str(),"",DAQmx_Val_Diff,0,10.0,DAQmx_Val_Volts,NULL);
	  Error_Result = DAQmxCreateAIVoltageChan(NI6210_Handle,AnsiString(sDQA_Address_Name + "/ai0").c_str(),"",DAQmx_Val_Diff,0,24.0,DAQmx_Val_FromCustomScale,"TXDScale");
	  // Dummy shorted channel to discharge DAQ instrumentation amplifier
	  Error_Result = DAQmxCreateAIVoltageChan(NI6210_Handle,AnsiString(sDQA_Address_Name + "/ai5").c_str(),"",DAQmx_Val_Diff,0,0.2,DAQmx_Val_Volts,NULL);

		AddToLog("NI6210 Channels Created");

		// Set sample clock. Convert clock is synched to this by default.
	  Error_Result = DAQmxCfgSampClkTiming(NI6210_Handle,"",SAMPLE_RATE,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,SAMPLES_TO_READ);

	  // Debug - gives error code -200489, "Specified channel cannot be added to the task, because a channel with the same name is already in the task."
//	  Error_Result = DAQmxAddGlobalChansToTask(NI6210_Handle, Channel_List);


	//if unsuccessfull, indicate to user
	} else {
		AddToLog("NI6210 TaskCreate Error");
	}

	//return result
	return (result == NI6210_OK) ? true : false ;

}

//---------------------------------------------------------------------------

void __fastcall NI6210_USB_Interface::DMM_Task(void)
{

}

//---------------------------------------------------------------------------
