object mainForm: TmainForm
  Left = 0
  Top = 384
  Caption = 'Auto_Plotter_BH_SIandT V1.0 - 27 Apr 2016'
  ClientHeight = 762
  ClientWidth = 636
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Shape1: TShape
    Left = 29
    Top = 27
    Width = 14
    Height = 17
    Brush.Color = clSilver
    Shape = stCircle
  end
  object Label17: TLabel
    Left = 59
    Top = 27
    Width = 102
    Height = 13
    Caption = 'Plotter Connected'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object TabControl1: TTabControl
    Left = 4
    Top = 8
    Width = 625
    Height = 746
    TabOrder = 0
    Tabs.Strings = (
      '1 Set-up'
      '2 Control'
      '3 Maintenance'
      '4 Miscellaneous')
    TabIndex = 0
    OnChange = TabControl1Change
    object Panel5: TPanel
      Left = 3
      Top = 40
      Width = 620
      Height = 669
      BevelOuter = bvNone
      TabOrder = 3
      object GroupBox7: TGroupBox
        Left = 6
        Top = 2
        Width = 599
        Height = 658
        Caption = 'Misc Instruments'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object grMiscInstruments: TGroupBox
          Left = 11
          Top = 41
          Width = 297
          Height = 611
          Caption = 'GW LCR 8110 Meter'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          object Label61: TLabel
            Left = 89
            Top = 25
            Width = 13
            Height = 13
            Caption = 'No'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label62: TLabel
            Left = 145
            Top = 25
            Width = 24
            Height = 13
            Caption = 'Baud'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label68: TLabel
            Left = 12
            Top = 93
            Width = 86
            Height = 13
            Caption = 'Comport Message'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label85: TLabel
            Left = 12
            Top = 128
            Width = 79
            Height = 13
            Caption = 'Message History'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lbStatusGWLCR: TLabel
            Left = 88
            Top = 584
            Width = 47
            Height = 13
            Caption = 'STATUS:'
          end
          object Label97: TLabel
            Left = 122
            Top = 92
            Width = 138
            Height = 13
            Caption = 'Meas. Time mS (for est.)'
          end
          object btnLCRGW: TButton
            Left = 13
            Top = 23
            Width = 70
            Height = 17
            Caption = 'Open Port'
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            OnClick = btnLCRGWClick
          end
          object edMiscIntruComportNo: TEdit
            Left = 109
            Top = 21
            Width = 25
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            Text = '0'
          end
          object Button10: TButton
            Left = 11
            Top = 72
            Width = 149
            Height = 18
            Caption = 'Send Above Command'
            TabOrder = 2
            OnClick = Button10Click
          end
          object Memo1: TMemo
            Left = 10
            Top = 144
            Width = 277
            Height = 38
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
          object edBaudGenSerial: TEdit
            Left = 175
            Top = 21
            Width = 48
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Text = '9600'
          end
          object Button11: TButton
            Left = 232
            Top = 22
            Width = 57
            Height = 17
            Caption = 'Close'
            TabOrder = 5
            OnClick = Button11Click
          end
          object cbMiscInstrumentCmds: TComboBox
            Left = 11
            Top = 49
            Width = 190
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            Text = 'Commands List'
            OnDblClick = cbMiscInstrumentCmdsDblClick
          end
          object GroupBox5: TGroupBox
            Left = 7
            Top = 331
            Width = 281
            Height = 246
            Caption = 'LCR Test Frequencies'
            TabOrder = 7
            object Label74: TLabel
              Left = 11
              Top = 23
              Width = 80
              Height = 13
              Caption = 'GWLCR freqHz 1'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label75: TLabel
              Left = 11
              Top = 46
              Width = 80
              Height = 13
              Caption = 'GWLCR freqHz 2'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label76: TLabel
              Left = 12
              Top = 63
              Width = 80
              Height = 13
              Caption = 'GWLCR freqHz 3'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label77: TLabel
              Left = 11
              Top = 87
              Width = 80
              Height = 13
              Caption = 'GWLCR freqHz 4'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label78: TLabel
              Left = 11
              Top = 109
              Width = 80
              Height = 13
              Caption = 'GWLCR freqHz 5'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label79: TLabel
              Left = 11
              Top = 130
              Width = 80
              Height = 13
              Caption = 'GWLCR freqHz 6'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label80: TLabel
              Left = 11
              Top = 151
              Width = 80
              Height = 13
              Caption = 'GWLCR freqHz 7'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label81: TLabel
              Left = 12
              Top = 174
              Width = 80
              Height = 13
              Caption = 'GWLCR freqHz 8'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label82: TLabel
              Left = 11
              Top = 195
              Width = 80
              Height = 13
              Caption = 'GWLCR freqHz 9'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label83: TLabel
              Left = 11
              Top = 218
              Width = 86
              Height = 13
              Caption = 'GWLCR freqHz 10'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label86: TLabel
              Left = 170
              Top = 7
              Width = 37
              Height = 13
              Caption = 'Data 1'
            end
            object Label87: TLabel
              Left = 226
              Top = 6
              Width = 37
              Height = 13
              Caption = 'Data 2'
            end
            object edTestFreqHz1of10: TEdit
              Left = 107
              Top = 19
              Width = 58
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              Text = '4.0e2'
            end
            object edTestFreqHz2of10: TEdit
              Left = 107
              Top = 42
              Width = 58
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              Text = '5.0e3'
            end
            object edTestFreqHz3of10: TEdit
              Left = 107
              Top = 63
              Width = 58
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              Text = '50.0e3'
            end
            object edTestFreqHz4of10: TEdit
              Left = 106
              Top = 85
              Width = 59
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              Text = '475.0e3'
            end
            object edTestFreqHz5of10: TEdit
              Left = 106
              Top = 105
              Width = 59
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
              Text = '1.0e6'
            end
            object edTestFreqHz6of10: TEdit
              Left = 105
              Top = 126
              Width = 60
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 5
              Text = '10e6'
            end
            object edTestFreqHz7of10: TEdit
              Left = 105
              Top = 148
              Width = 60
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 6
            end
            object edTestFreqHz8of10: TEdit
              Left = 105
              Top = 170
              Width = 60
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 7
            end
            object edTestFreqHz9of10: TEdit
              Left = 105
              Top = 192
              Width = 60
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 8
            end
            object edTestFreqHz10of10: TEdit
              Left = 104
              Top = 215
              Width = 61
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 9
            end
            object edLCRfreq1_Data1: TEdit
              Left = 168
              Top = 19
              Width = 51
              Height = 21
              TabOrder = 10
            end
            object edLCRfreq1_Data2: TEdit
              Left = 222
              Top = 19
              Width = 48
              Height = 21
              TabOrder = 11
            end
            object edLCRfreq2_Data1: TEdit
              Left = 168
              Top = 42
              Width = 51
              Height = 21
              TabOrder = 12
            end
            object edLCRfreq2_Data2: TEdit
              Left = 222
              Top = 42
              Width = 48
              Height = 21
              TabOrder = 13
            end
            object edLCRfreq3_Data1: TEdit
              Left = 168
              Top = 65
              Width = 51
              Height = 21
              TabOrder = 14
            end
            object edLCRfreq4_Data1: TEdit
              Left = 167
              Top = 86
              Width = 51
              Height = 21
              TabOrder = 15
            end
            object edLCRfreq5_Data1: TEdit
              Left = 168
              Top = 107
              Width = 51
              Height = 21
              TabOrder = 16
            end
            object edLCRfreq6_Data1: TEdit
              Left = 167
              Top = 128
              Width = 51
              Height = 21
              TabOrder = 17
            end
            object edLCRfreq7_Data1: TEdit
              Left = 166
              Top = 149
              Width = 51
              Height = 21
              TabOrder = 18
            end
            object edLCRfreq8_Data1: TEdit
              Left = 166
              Top = 172
              Width = 51
              Height = 21
              TabOrder = 19
            end
            object edLCRfreq9_Data1: TEdit
              Left = 167
              Top = 193
              Width = 51
              Height = 21
              TabOrder = 20
            end
            object edLCRfreq10_Data1: TEdit
              Left = 165
              Top = 215
              Width = 51
              Height = 21
              TabOrder = 21
            end
            object edLCRfreq3_Data2: TEdit
              Left = 221
              Top = 65
              Width = 48
              Height = 21
              TabOrder = 22
            end
            object edLCRfreq4_Data2: TEdit
              Left = 222
              Top = 87
              Width = 48
              Height = 21
              TabOrder = 23
            end
            object edLCRfreq5_Data2: TEdit
              Left = 221
              Top = 109
              Width = 48
              Height = 21
              TabOrder = 24
            end
            object edLCRfreq6_Data2: TEdit
              Left = 221
              Top = 130
              Width = 48
              Height = 21
              TabOrder = 25
            end
            object edLCRfreq7_Data2: TEdit
              Left = 221
              Top = 150
              Width = 48
              Height = 21
              TabOrder = 26
            end
            object edLCRfreq8_Data2: TEdit
              Left = 220
              Top = 173
              Width = 48
              Height = 21
              TabOrder = 27
            end
            object edLCRfreq9_Data2: TEdit
              Left = 220
              Top = 195
              Width = 48
              Height = 21
              TabOrder = 28
            end
            object edLCRfreq10_Data2: TEdit
              Left = 220
              Top = 215
              Width = 48
              Height = 21
              TabOrder = 29
            end
          end
          object GroupBox6: TGroupBox
            Left = 8
            Top = 184
            Width = 281
            Height = 148
            Caption = 'LCR Test Configuration'
            TabOrder = 8
            object Label70: TLabel
              Left = 13
              Top = 17
              Width = 93
              Height = 13
              Caption = 'GWLCR man_graph'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label71: TLabel
              Left = 13
              Top = 41
              Width = 71
              Height = 13
              Caption = 'GWLCR ac_rdc'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label73: TLabel
              Left = 13
              Top = 108
              Width = 115
              Height = 13
              Caption = 'GWLCR l;c;r;q;d (2max)'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label72: TLabel
              Left = 13
              Top = 86
              Width = 97
              Height = 13
              Caption = 'GWLCR lev eg 200m'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label84: TLabel
              Left = 13
              Top = 64
              Width = 86
              Height = 13
              Caption = 'GWLC cct ser_par'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label96: TLabel
              Left = 13
              Top = 128
              Width = 169
              Height = 13
              Caption = 'GWLCR speed max_fast_med_slow'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object edGWLCR_Mode: TEdit
              Left = 191
              Top = 15
              Width = 81
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              Text = 'man'
            end
            object edGWLCR_Meas: TEdit
              Left = 191
              Top = 38
              Width = 83
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              Text = 'ac'
            end
            object edGWLCR_Level: TEdit
              Left = 191
              Top = 84
              Width = 83
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              Text = '200m'
            end
            object edGWLCR_Comp: TEdit
              Left = 191
              Top = 103
              Width = 83
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              Text = 'l;r'
            end
            object edGWLCR_cct: TEdit
              Left = 191
              Top = 60
              Width = 82
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
              Text = 'ser'
            end
            object edGWLCR_Speed: TEdit
              Left = 191
              Top = 124
              Width = 83
              Height = 21
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 5
              Text = 'med'
            end
          end
          object btRunMiscInstruTtest: TButton
            Left = 9
            Top = 581
            Width = 74
            Height = 24
            Caption = 'Run Test'
            TabOrder = 9
            OnClick = btRunMiscInstruTtestClick
          end
          object btMiscInstruCal: TButton
            Left = 208
            Top = 580
            Width = 81
            Height = 25
            Caption = 'Run Cal'
            TabOrder = 10
            OnClick = btMiscInstruCalClick
          end
          object edMiscInstruMsg: TEdit
            Left = 10
            Top = 106
            Width = 279
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 11
            Text = 'Last Message'
          end
          object edLCR_EstTestTime: TEdit
            Left = 246
            Top = 84
            Width = 42
            Height = 21
            TabOrder = 12
            Text = '2000'
          end
          object Button26: TButton
            Left = 232
            Top = 48
            Width = 57
            Height = 25
            Caption = 'Stop LCR'
            TabOrder = 13
            OnClick = Button26Click
          end
        end
        object cbMiscInstrumentType: TComboBox
          Left = 10
          Top = 19
          Width = 113
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          Text = 'Select Instrument'
          OnChange = cbMiscInstrumentTypeChange
          Items.Strings = (
            'GW Instek LCR 8110')
        end
        object TiEvmLDC1614: TGroupBox
          Left = 319
          Top = 41
          Width = 263
          Height = 261
          Caption = 'TiEvmLDC1614'
          TabOrder = 2
          object Label88: TLabel
            Left = 16
            Top = 16
            Width = 237
            Height = 13
            Caption = 'Read File save by Ti LDC1614EVM -> nw.exe GUI'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label89: TLabel
            Left = 18
            Top = 50
            Width = 179
            Height = 13
            Caption = 'Ti Data csv file eg c:\\temp\\data.csv'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label90: TLabel
            Left = 112
            Top = 122
            Width = 29
            Height = 13
            Caption = 'L (uH)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label91: TLabel
            Left = 179
            Top = 122
            Width = 22
            Height = 13
            Caption = 'Freq'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label92: TLabel
            Left = 39
            Top = 140
            Width = 11
            Height = 13
            Caption = 'L0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label93: TLabel
            Left = 39
            Top = 161
            Width = 11
            Height = 13
            Caption = 'L1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label94: TLabel
            Left = 39
            Top = 184
            Width = 11
            Height = 13
            Caption = 'L2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label95: TLabel
            Left = 38
            Top = 203
            Width = 11
            Height = 13
            Caption = 'L3'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lbTiEVMStatus: TLabel
            Left = 17
            Top = 242
            Width = 67
            Height = 13
            Caption = 'lbTiEVMStatus'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label98: TLabel
            Left = 59
            Top = 229
            Width = 118
            Height = 13
            Caption = 'Meas. Time mS (for est.)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object cbTiEvmLDC_on_Off: TCheckBox
            Left = 25
            Top = 31
            Width = 145
            Height = 17
            Caption = 'cbTiEvmLDC_on_Off'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = cbTiEvmLDC_on_OffClick
          end
          object edTiLCDEvmFileAddress: TEdit
            Left = 19
            Top = 67
            Width = 230
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            Text = 'c:\\temp\\data.csv'
          end
          object Button12: TButton
            Left = 18
            Top = 90
            Width = 81
            Height = 25
            Caption = 'Test CSV File'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            OnClick = Button12Click
          end
          object edTiEVM_L0: TEdit
            Left = 74
            Top = 137
            Width = 73
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
          end
          object edTiEVM_L1: TEdit
            Left = 74
            Top = 158
            Width = 73
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
          end
          object edTiEVM_L2: TEdit
            Left = 74
            Top = 179
            Width = 73
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
          end
          object edTiEVM_L3: TEdit
            Left = 74
            Top = 200
            Width = 73
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
          end
          object edTiEVM_LFreq0: TEdit
            Left = 152
            Top = 138
            Width = 73
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
          end
          object edTiEVM_LFreq1: TEdit
            Left = 152
            Top = 159
            Width = 73
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 8
          end
          object edTiEVM_LFreq2: TEdit
            Left = 151
            Top = 180
            Width = 73
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 9
          end
          object edTiEVM_LFreq3: TEdit
            Left = 151
            Top = 201
            Width = 73
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 10
          end
          object Button13: TButton
            Left = 168
            Top = 88
            Width = 89
            Height = 25
            Caption = 'Stop Test'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 11
            OnClick = Button13Click
          end
          object edTiEVM_EstTestTime: TEdit
            Left = 183
            Top = 223
            Width = 42
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 12
            Text = '1000'
          end
        end
        object Button4: TButton
          Left = 473
          Top = 525
          Width = 113
          Height = 26
          Caption = 'Button4'
          TabOrder = 3
          Visible = False
          OnClick = Button4Click
        end
        object GroupBox8: TGroupBox
          Left = 319
          Top = 312
          Width = 263
          Height = 188
          Caption = 'Coil Array Using LCR Meter (20 Max)'
          TabOrder = 4
          object Label69: TLabel
            Left = 96
            Top = 24
            Width = 17
            Height = 13
            Caption = 'No.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label100: TLabel
            Left = 168
            Top = 24
            Width = 24
            Height = 13
            Caption = 'Baud'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lbFoDCoilStatus: TLabel
            Left = 16
            Top = 167
            Width = 60
            Height = 13
            Caption = 'Coil Status'
          end
          object Label125: TLabel
            Left = 67
            Top = 111
            Width = 124
            Height = 13
            Caption = 'Wait before Read LCR mS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Button16: TButton
            Left = 16
            Top = 24
            Width = 73
            Height = 25
            Caption = 'Open Port'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnClick = Button16Click
          end
          object edBaudArdFoDSerialNo: TEdit
            Left = 119
            Top = 23
            Width = 41
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            Text = '0'
          end
          object edBaudArdFoDSerialBd: TEdit
            Left = 200
            Top = 24
            Width = 41
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            Text = '9600'
          end
          object Button17: TButton
            Left = 16
            Top = 80
            Width = 131
            Height = 25
            Caption = 'Send Above Command'
            TabOrder = 3
            OnClick = Button17Click
          end
          object cbFoDArduinoCommands: TComboBox
            Left = 16
            Top = 56
            Width = 177
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            Text = 'Arduino Commands'
          end
          object Button18: TButton
            Left = 17
            Top = 127
            Width = 105
            Height = 25
            Caption = 'Read Coils (8max)'
            TabOrder = 5
            OnClick = Button18Click
          end
          object cbFodResults: TComboBox
            Left = 128
            Top = 131
            Width = 128
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            Text = 'cbFodResults'
          end
          object EdWaitBeforeLCRRead: TEdit
            Left = 197
            Top = 106
            Width = 57
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
            Text = '100'
          end
          object Button19: TButton
            Left = 199
            Top = 53
            Width = 60
            Height = 25
            Caption = 'Close Port'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 8
            OnClick = Button19Click
          end
        end
      end
    end
    object Panel4: TPanel
      Left = 2
      Top = 38
      Width = 620
      Height = 591
      BevelOuter = bvNone
      TabOrder = 2
      object cbDebug: TCheckBox
        Left = 16
        Top = 568
        Width = 273
        Height = 17
        Caption = 'Turn On Debug Messages'
        TabOrder = 4
      end
      object Panel8: TPanel
        Left = 10
        Top = 8
        Width = 311
        Height = 192
        BevelOuter = bvLowered
        TabOrder = 2
        object lbCaptionPlotterControl: TLabel
          Left = 14
          Top = 10
          Width = 272
          Height = 13
          Caption = 'Plotter Control (Use Set up Tab to Connect First)'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbRelayControl: TLabel
          Left = 197
          Top = 15
          Width = 94
          Height = 13
          Caption = 'Ref V Relay Control'
          Visible = False
        end
        object btHomePlotter: TButton
          Left = 13
          Top = 29
          Width = 119
          Height = 24
          Caption = 'HOME PLOTTER'
          Enabled = False
          TabOrder = 0
          OnClick = btHomePlotterClick
        end
        object pnlManualPlotterCmd: TPanel
          Left = 10
          Top = 97
          Width = 126
          Height = 80
          BevelOuter = bvLowered
          Enabled = False
          TabOrder = 1
          object Label52: TLabel
            Left = 8
            Top = 32
            Width = 102
            Height = 13
            Caption = 'Note: Delete text '#39' ='#39
          end
          object Button9: TButton
            Left = 6
            Top = 51
            Width = 115
            Height = 25
            Caption = 'PLOTTER SEND CMD'
            TabOrder = 0
            OnClick = Button9Click
          end
          object cbPlotterSendCmd: TComboBox
            Left = 6
            Top = 8
            Width = 118
            Height = 21
            TabOrder = 1
            Text = 'Plotter Command'
            Items.Strings = (
              'G1 X0 Y0 Z0 F3000'
              'G20 = inches'
              'G21 = metric'
              'G28 = Home'
              'G90 = Set Abs Co-ord'
              'G91 = Set Rel Co-ord'
              'M114 = Get Current loc'
              'M340 P0 S105 = Act')
          end
        end
      end
      object plComms: TPanel
        Left = 10
        Top = 330
        Width = 598
        Height = 223
        BevelOuter = bvLowered
        TabOrder = 0
        object lbCaptionUSBMeters: TLabel
          Left = 5
          Top = 29
          Width = 82
          Height = 13
          Caption = 'USB Meter Log'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbCaptionTestLog: TLabel
          Left = 188
          Top = 29
          Width = 101
          Height = 13
          Caption = 'Test Progress Log'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbCaptionPlotterComms: TLabel
          Left = 400
          Top = 29
          Width = 122
          Height = 13
          Caption = 'Plotter Command Log'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 5
          Top = 10
          Width = 151
          Height = 13
          Caption = 'PROGRAM STATE MONITOR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbErrorStatus: TLabel
          Left = 188
          Top = 10
          Width = 69
          Height = 13
          Caption = 'Program State'
        end
        object Label10: TLabel
          Left = 369
          Top = 10
          Width = 49
          Height = 13
          Caption = 'Reg Name'
        end
        object memoTestLog: TMemo
          Left = 188
          Top = 48
          Width = 192
          Height = 161
          ScrollBars = ssVertical
          TabOrder = 0
        end
        object memoPlotterLog: TMemo
          Left = 398
          Top = 48
          Width = 190
          Height = 161
          ScrollBars = ssVertical
          TabOrder = 1
          OnChange = memoPlotterLogChange
        end
        object memoUSBLog: TMemo
          Left = 5
          Top = 48
          Width = 167
          Height = 161
          ScrollBars = ssVertical
          TabOrder = 2
          OnChange = memoUSBLogChange
        end
        object edRegistryName: TEdit
          Left = 424
          Top = 5
          Width = 169
          Height = 21
          TabOrder = 3
          Text = 'Auto_Plotter_Test_SIandT'
        end
        object cbSaveLog: TCheckBox
          Left = 315
          Top = 25
          Width = 65
          Height = 17
          Caption = 'Save Log'
          TabOrder = 4
        end
      end
      object Panel7: TPanel
        Left = 336
        Top = 8
        Width = 268
        Height = 191
        BevelOuter = bvLowered
        TabOrder = 1
        object lbUSBConnected: TLabel
          Left = 177
          Top = 43
          Width = 79
          Height = 13
          Caption = 'USB Data Status'
        end
        object lbInputVoltage: TLabel
          Left = 40
          Top = 65
          Width = 20
          Height = 13
          Caption = 'V in '
        end
        object lbOutputVoltage: TLabel
          Left = 39
          Top = 117
          Width = 25
          Height = 13
          Caption = 'V out'
        end
        object lbIputCurrent: TLabel
          Left = 41
          Top = 89
          Width = 15
          Height = 13
          Caption = 'I in'
        end
        object lbOutputCurrent: TLabel
          Left = 40
          Top = 145
          Width = 23
          Height = 13
          Caption = 'I out'
        end
        object shInputVoltage: TShape
          Left = 16
          Top = 61
          Width = 14
          Height = 17
          Brush.Color = clSilver
          Shape = stCircle
        end
        object shInputCurrent: TShape
          Left = 16
          Top = 87
          Width = 14
          Height = 17
          Brush.Color = clSilver
          Shape = stCircle
        end
        object shOutputVoltage: TShape
          Left = 15
          Top = 115
          Width = 14
          Height = 17
          Brush.Color = clSilver
          Shape = stCircle
        end
        object shOutputCurrent: TShape
          Left = 15
          Top = 144
          Width = 14
          Height = 17
          Brush.Color = clSilver
          Shape = stCircle
        end
        object Label2: TLabel
          Left = 18
          Top = 12
          Width = 165
          Height = 13
          Caption = 'DAQ MANUAL CONTROL PANEL'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 174
          Top = 29
          Width = 80
          Height = 13
          Caption = 'Vin  Iin Vout Iout'
        end
        object lbEffCalcManual: TLabel
          Left = 95
          Top = 167
          Width = 59
          Height = 23
          Caption = '100%'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label18: TLabel
          Left = 17
          Top = 170
          Width = 57
          Height = 13
          Caption = 'Efficiency ='
        end
        object Label63: TLabel
          Left = 152
          Top = 64
          Width = 35
          Height = 13
          Caption = 'Daq No'
        end
        object Label64: TLabel
          Left = 152
          Top = 88
          Width = 35
          Height = 13
          Caption = 'Daq No'
        end
        object Label65: TLabel
          Left = 152
          Top = 112
          Width = 35
          Height = 13
          Caption = 'Daq No'
        end
        object Label66: TLabel
          Left = 152
          Top = 144
          Width = 35
          Height = 13
          Caption = 'Daq No'
        end
        object ebInputVoltage: TEdit
          Left = 72
          Top = 60
          Width = 75
          Height = 21
          Enabled = False
          TabOrder = 0
        end
        object ebInputCurrent: TEdit
          Left = 72
          Top = 88
          Width = 73
          Height = 21
          Enabled = False
          TabOrder = 1
        end
        object ebOutputVoltage: TEdit
          Left = 72
          Top = 114
          Width = 73
          Height = 21
          Enabled = False
          TabOrder = 2
        end
        object ebOutputCurrent: TEdit
          Left = 73
          Top = 140
          Width = 73
          Height = 21
          Enabled = False
          TabOrder = 3
        end
        object btCloseUSB: TButton
          Left = 83
          Top = 31
          Width = 59
          Height = 22
          Caption = 'CLOSE'
          Enabled = False
          TabOrder = 4
          OnClick = btCloseUSBClick
        end
        object btOpenUSB: TButton
          Left = 18
          Top = 31
          Width = 59
          Height = 22
          Caption = 'OPEN'
          Enabled = False
          TabOrder = 5
          OnClick = btOpenUSBClick
        end
        object edVinDaqAddr: TEdit
          Left = 193
          Top = 59
          Width = 65
          Height = 21
          Enabled = False
          TabOrder = 6
          Text = 'Daq Port'
        end
        object edIinDaqAddr: TEdit
          Left = 193
          Top = 86
          Width = 65
          Height = 21
          Enabled = False
          TabOrder = 7
          Text = 'Daq Port'
        end
        object edVoutDaqAddr: TEdit
          Left = 193
          Top = 113
          Width = 64
          Height = 21
          Enabled = False
          TabOrder = 8
          Text = 'Daq Port'
        end
        object edIoutDaqAddr: TEdit
          Left = 193
          Top = 140
          Width = 65
          Height = 21
          Enabled = False
          TabOrder = 9
          Text = 'Daq Port'
        end
      end
      object plCallibration: TPanel
        Left = 337
        Top = 205
        Width = 271
        Height = 119
        BevelOuter = bvLowered
        TabOrder = 3
        object lbCaptionIinMultiplier: TLabel
          Left = 143
          Top = 32
          Width = 125
          Height = 13
          Caption = 'Current_In (Offset / Mult)'
        end
        object lbCaptionVinMultiplier: TLabel
          Left = 17
          Top = 32
          Width = 106
          Height = 13
          Caption = 'Volt_In (Offset / Mult)'
        end
        object lbCaptionVoutMultiplier: TLabel
          Left = 17
          Top = 78
          Width = 102
          Height = 13
          Caption = 'V_Out (Offset / Mult)'
        end
        object lbCaptionIoutMultiplier: TLabel
          Left = 143
          Top = 78
          Width = 118
          Height = 13
          Caption = 'Current_Out (Off / Mult)'
        end
        object lbCaptionMultipliers: TLabel
          Left = 16
          Top = 11
          Width = 187
          Height = 13
          Caption = 'DAQ Callibration  ( Input + A ) * B'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object ebVinMultiplier: TEdit
          Left = 79
          Top = 51
          Width = 58
          Height = 21
          TabOrder = 0
          Text = '0'
          OnExit = ebVinMultiplierChange
        end
        object ebIinMultiplier: TEdit
          Left = 205
          Top = 51
          Width = 58
          Height = 21
          TabOrder = 1
          Text = '0'
          OnExit = ebIinMultiplierChange
        end
        object ebIoutMultiplier: TEdit
          Left = 204
          Top = 97
          Width = 58
          Height = 21
          TabOrder = 2
          Text = '0'
          OnExit = ebIoutMultiplierChange
        end
        object ebVoutMultiplier: TEdit
          Left = 79
          Top = 97
          Width = 58
          Height = 21
          TabOrder = 3
          Text = '0'
          OnExit = ebVoutMultiplierChange
        end
        object ebVinOffset: TEdit
          Left = 15
          Top = 51
          Width = 58
          Height = 21
          TabOrder = 4
          Text = '0'
          OnChange = ebVinOffsetChange
          OnExit = ebVinOffsetChange
        end
        object ebIinOffset: TEdit
          Left = 143
          Top = 51
          Width = 56
          Height = 21
          TabOrder = 5
          Text = '0'
          OnExit = ebIinOffsetChange
        end
        object ebVoutOffset: TEdit
          Left = 15
          Top = 97
          Width = 58
          Height = 21
          TabOrder = 6
          Text = '0'
          OnExit = ebVoutOffsetChange
        end
        object ebIoutOffset: TEdit
          Left = 143
          Top = 97
          Width = 56
          Height = 21
          TabOrder = 7
          Text = '0'
          OnExit = ebIoutOffsetChange
        end
        object Button14: TButton
          Left = 208
          Top = 8
          Width = 57
          Height = 17
          Caption = 'Update'
          TabOrder = 8
          OnClick = Button14Click
        end
      end
      object GroupBox4: TGroupBox
        Left = 8
        Top = 208
        Width = 313
        Height = 113
        Caption = 'Sequencer ini File'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        Visible = False
        object meSeqIniFile: TMemo
          Left = 17
          Top = 24
          Width = 281
          Height = 61
          TabOrder = 0
        end
        object btnReadSeqIni: TButton
          Left = 17
          Top = 88
          Width = 65
          Height = 17
          Caption = 'Read Seq ini'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          OnClick = btnReadSeqIniClick
        end
        object btnSaveSeqIni: TButton
          Left = 220
          Top = 88
          Width = 73
          Height = 17
          Caption = 'Save ini File'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          OnClick = btnSaveSeqIniClick
        end
      end
    end
    object Panel2: TPanel
      Left = 3
      Top = 37
      Width = 617
      Height = 706
      BevelOuter = bvNone
      TabOrder = 0
      object btRelayOff: TButton
        Left = 533
        Top = 444
        Width = 49
        Height = 25
        Caption = 'OFF'
        Enabled = False
        TabOrder = 0
        Visible = False
        OnClick = btRelayOffClick
      end
      object btRelayOn: TButton
        Left = 534
        Top = 417
        Width = 50
        Height = 25
        Caption = 'ON'
        TabOrder = 1
        Visible = False
        OnClick = btRelayOnClick
      end
      object Panel10: TPanel
        Left = 8
        Top = 475
        Width = 601
        Height = 197
        BevelOuter = bvLowered
        TabOrder = 2
        object lbFilename: TLabel
          Left = 12
          Top = 55
          Width = 119
          Height = 13
          Caption = 'Filename below (no ext.)'
        end
        object lbFolderName: TLabel
          Left = 13
          Top = 9
          Width = 160
          Height = 13
          Caption = 'Folder to Store CSV (eg C:\Test\)'
        end
        object Label25: TLabel
          Left = 9
          Top = 102
          Width = 40
          Height = 13
          Caption = 'Tx Make'
        end
        object Label26: TLabel
          Left = 122
          Top = 101
          Width = 50
          Height = 13
          Caption = 'Tx Version'
        end
        object Label27: TLabel
          Left = 216
          Top = 101
          Width = 53
          Height = 13
          Caption = 'Tx Build No'
        end
        object Label28: TLabel
          Left = 10
          Top = 145
          Width = 41
          Height = 13
          Caption = 'Rx Make'
        end
        object Label29: TLabel
          Left = 123
          Top = 145
          Width = 51
          Height = 13
          Caption = 'Rx Version'
        end
        object Label30: TLabel
          Left = 217
          Top = 145
          Width = 54
          Height = 13
          Caption = 'Rx Build No'
        end
        object Label31: TLabel
          Left = 326
          Top = 117
          Width = 45
          Height = 13
          Caption = 'Z (mm)->'
        end
        object Label32: TLabel
          Left = 324
          Top = 141
          Width = 48
          Height = 13
          Caption = 'RxLoad->'
        end
        object Label33: TLabel
          Left = 313
          Top = 163
          Width = 58
          Height = 13
          Caption = 'Step(mm)->'
        end
        object Label34: TLabel
          Left = 456
          Top = 116
          Width = 58
          Height = 13
          Caption = 'Rx Orient->'
        end
        object Label35: TLabel
          Left = 454
          Top = 140
          Width = 61
          Height = 13
          Caption = 'ONtime(S)->'
        end
        object Label36: TLabel
          Left = 451
          Top = 164
          Width = 64
          Height = 13
          Caption = 'Mod Depth->'
        end
        object Label19: TLabel
          Left = 83
          Top = 101
          Width = 32
          Height = 13
          Caption = 'Tx No.'
        end
        object Label38: TLabel
          Left = 83
          Top = 145
          Width = 33
          Height = 13
          Caption = 'Rx No.'
        end
        object Label44: TLabel
          Left = 282
          Top = 101
          Width = 38
          Height = 13
          Caption = 'Tx Volts'
        end
        object Label99: TLabel
          Left = 491
          Top = 82
          Width = 58
          Height = 13
          Caption = 'Seq Append'
        end
        object ebFilename: TEdit
          Left = 9
          Top = 74
          Width = 476
          Height = 21
          TabOrder = 0
          Text = '*.INI'
          OnExit = ebFilenameChange
        end
        object ebFolderName: TEdit
          Left = 9
          Top = 28
          Width = 579
          Height = 21
          TabOrder = 1
          Text = '*.INI'
          OnExit = ebFolderNameChange
        end
        object edTxVersion: TEdit
          Left = 121
          Top = 117
          Width = 89
          Height = 21
          TabOrder = 2
        end
        object edTxBuild: TEdit
          Left = 216
          Top = 118
          Width = 60
          Height = 21
          TabOrder = 3
        end
        object edRxVers: TEdit
          Left = 121
          Top = 163
          Width = 89
          Height = 21
          TabOrder = 4
        end
        object edRxBuild: TEdit
          Left = 216
          Top = 163
          Width = 60
          Height = 21
          TabOrder = 5
        end
        object edRxLoad: TEdit
          Left = 377
          Top = 136
          Width = 71
          Height = 21
          TabOrder = 6
        end
        object edTestWait: TEdit
          Left = 518
          Top = 136
          Width = 75
          Height = 21
          TabOrder = 7
        end
        object edRxStep: TEdit
          Left = 377
          Top = 160
          Width = 71
          Height = 21
          TabOrder = 8
        end
        object edTxModDepth: TEdit
          Left = 518
          Top = 163
          Width = 75
          Height = 21
          TabOrder = 9
        end
        object btUpdateFilename: TButton
          Left = 485
          Top = 55
          Width = 105
          Height = 17
          Caption = 'Update Filename'
          TabOrder = 10
          OnClick = btUpdateFilenameClick
        end
        object Button5: TButton
          Left = 356
          Top = 55
          Width = 123
          Height = 17
          Caption = 'Set Generic Filename'
          TabOrder = 11
          OnClick = Button5Click
        end
        object cbTxMakeOptions: TComboBox
          Left = 9
          Top = 117
          Width = 68
          Height = 21
          TabOrder = 12
        end
        object edTxNumber: TEdit
          Left = 80
          Top = 116
          Width = 33
          Height = 21
          TabOrder = 13
        end
        object edRxNumber: TEdit
          Left = 82
          Top = 163
          Width = 33
          Height = 21
          TabOrder = 14
        end
        object cbRxMakeOptions: TComboBox
          Left = 8
          Top = 163
          Width = 68
          Height = 21
          TabOrder = 15
        end
        object cbRxOrientation: TComboBox
          Left = 518
          Top = 112
          Width = 75
          Height = 21
          TabOrder = 16
        end
        object cbRxZHeight: TComboBox
          Left = 377
          Top = 112
          Width = 71
          Height = 21
          TabOrder = 17
        end
        object edTxVDC: TEdit
          Left = 282
          Top = 118
          Width = 35
          Height = 21
          TabOrder = 18
          Text = '19'
        end
        object ebFilenameSeqAppend: TEdit
          Left = 551
          Top = 76
          Width = 41
          Height = 21
          TabOrder = 19
        end
      end
      object GroupBox1: TGroupBox
        Left = 302
        Top = 155
        Width = 314
        Height = 68
        Caption = 'Rx COMMUNICATION'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        object Label22: TLabel
          Left = 154
          Top = 24
          Width = 33
          Height = 13
          Caption = 'Vers->'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label23: TLabel
          Left = 89
          Top = 24
          Width = 20
          Height = 13
          Caption = 'Port'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label24: TLabel
          Left = 8
          Top = 47
          Width = 200
          Height = 13
          Caption = 'Note: Rx must be powered by Tx not USB'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Button2: TButton
          Left = 3
          Top = 19
          Width = 73
          Height = 22
          Caption = 'Rx Version'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = Button2Click
        end
        object edRxVersion: TEdit
          Left = 193
          Top = 19
          Width = 112
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object edRxComport: TEdit
          Left = 115
          Top = 20
          Width = 33
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
      end
      object Panel16: TPanel
        Left = -321
        Top = 60
        Width = 289
        Height = 461
        BevelOuter = bvLowered
        TabOrder = 4
        object Label3: TLabel
          Left = 8
          Top = 8
          Width = 143
          Height = 13
          Caption = 'PLOTTER SETTINGS PANEL'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object pnlPlotterControl: TPanel
          Left = 8
          Top = 52
          Width = 275
          Height = 401
          BevelOuter = bvLowered
          TabOrder = 0
          object lbStepSize: TLabel
            Left = 186
            Top = 221
            Width = 43
            Height = 13
            Caption = 'Step size'
          end
          object lbYSteps: TLabel
            Left = 94
            Top = 221
            Width = 53
            Height = 13
            Caption = 'NR Y Steps'
          end
          object lbXSteps: TLabel
            Left = 9
            Top = 223
            Width = 53
            Height = 13
            Caption = 'NR X Steps'
          end
          object lblPowerUpWait: TLabel
            Left = 6
            Top = 95
            Width = 122
            Height = 13
            Caption = 'After Power up Wait (ms)'
          end
          object lbOffTime: TLabel
            Left = 161
            Top = 96
            Width = 98
            Height = 13
            Caption = 'Power Off Time (ms)'
          end
          object lbYOffset: TLabel
            Left = 7
            Top = 167
            Width = 54
            Height = 13
            Caption = 'Y Plot Start'
          end
          object lbXOffset: TLabel
            Left = 8
            Top = 141
            Width = 54
            Height = 13
            Caption = 'X Plot Start'
          end
          object Label8: TLabel
            Left = 147
            Top = 142
            Width = 40
            Height = 13
            Caption = 'X Offset'
          end
          object Label9: TLabel
            Left = 148
            Top = 166
            Width = 40
            Height = 13
            Caption = 'Y Offset'
          end
          object Label13: TLabel
            Left = 22
            Top = 330
            Width = 90
            Height = 13
            Caption = 'Distance to Dest ::'
            Visible = False
          end
          object Label14: TLabel
            Left = 110
            Top = 327
            Width = 62
            Height = 16
            Caption = 'Value mm'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object Label15: TLabel
            Left = 24
            Top = 330
            Width = 88
            Height = 13
            Caption = 'Set Coordinates ::'
            Visible = False
          end
          object Label16: TLabel
            Left = 168
            Top = 327
            Width = 62
            Height = 16
            Caption = 'Value mm'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = False
          end
          object Label12: TLabel
            Left = 5
            Top = 8
            Width = 96
            Height = 13
            Caption = 'Plotter Comport No.'
          end
          object Label40: TLabel
            Left = 53
            Top = 268
            Width = 25
            Height = 13
            Caption = 'X mm'
          end
          object Label41: TLabel
            Left = 141
            Top = 269
            Width = 25
            Height = 13
            Caption = 'Y mm'
          end
          object ebStepSize: TEdit
            Left = 185
            Top = 240
            Width = 78
            Height = 21
            TabOrder = 0
            Text = '5'
            OnChange = ebStepSizeChange
            OnExit = ebStepSizeChange
          end
          object ebYSteps: TEdit
            Left = 94
            Top = 240
            Width = 72
            Height = 21
            TabOrder = 1
            Text = '10'
            OnChange = ebYStepsChange
            OnExit = ebYStepsChange
          end
          object ebOffTime: TEdit
            Left = 159
            Top = 112
            Width = 105
            Height = 21
            TabOrder = 2
            Text = '1000'
            OnChange = ebOffTimeChange
            OnExit = ebOffTimeChange
          end
          object ebPowerUpWait: TEdit
            Left = 7
            Top = 111
            Width = 109
            Height = 21
            TabOrder = 3
            Text = '12000'
            OnChange = ebPowerUpWaitChange
            OnExit = ebPowerUpWaitChange
          end
          object ebXSteps: TEdit
            Left = 7
            Top = 240
            Width = 73
            Height = 21
            TabOrder = 4
            Text = '10'
            OnChange = ebXStepsChange
            OnExit = ebXStepsChange
          end
          object ebYOffset: TEdit
            Left = 198
            Top = 163
            Width = 66
            Height = 21
            TabOrder = 5
            Text = '0'
            OnChange = ebYOffsetChange
            OnExit = ebYOffsetChange
          end
          object ebXOffset: TEdit
            Left = 199
            Top = 139
            Width = 66
            Height = 21
            TabOrder = 6
            Text = '0'
            OnChange = ebXOffsetChange
            OnExit = ebXOffsetChange
          end
          object btnHomePlotterSet: TButton
            Left = 167
            Top = 3
            Width = 99
            Height = 22
            Caption = 'HOME PLOTTER'
            Enabled = False
            TabOrder = 7
            OnClick = btHomePlotterClick
          end
          object btnManGoToTestStart: TBitBtn
            Left = 8
            Top = 190
            Width = 112
            Height = 25
            Caption = 'Manual Go to Start'
            Enabled = False
            TabOrder = 8
            OnClick = btnManGoToTestStartClick
          end
          object btnGoToOffset: TButton
            Left = 153
            Top = 190
            Width = 112
            Height = 25
            Caption = 'Go to Offset Above'
            Enabled = False
            TabOrder = 9
            OnClick = btGotoXYClick
          end
          object ebXSetpointStart: TEdit
            Left = 67
            Top = 140
            Width = 66
            Height = 21
            TabOrder = 10
            Text = '0'
            OnChange = ebXSetpointStartChange
          end
          object ebYSetpointStart: TEdit
            Left = 66
            Top = 165
            Width = 65
            Height = 21
            TabOrder = 11
            Text = '0'
            OnChange = ebYSetpointStartChange
          end
          object edPlotterComportNo: TEdit
            Left = 107
            Top = 5
            Width = 33
            Height = 21
            TabOrder = 12
            Text = '0'
          end
          object btnGoToStepEnd: TButton
            Left = 5
            Top = 307
            Width = 157
            Height = 22
            Caption = 'Go to Step End Location'
            Enabled = False
            TabOrder = 13
            OnClick = btnGoToStepEndClick
          end
          object edXStepmm: TEdit
            Left = 7
            Top = 265
            Width = 43
            Height = 21
            Enabled = False
            TabOrder = 14
          end
          object edYStepmm: TEdit
            Left = 93
            Top = 265
            Width = 42
            Height = 21
            Enabled = False
            TabOrder = 15
          end
          object Panel12: TPanel
            Left = 169
            Top = 269
            Width = 99
            Height = 61
            BevelOuter = bvLowered
            TabOrder = 16
            object Label59: TLabel
              Left = 7
              Top = -1
              Width = 83
              Height = 13
              Caption = 'xmm   ymm   zmm'
            end
            object edLoadXmm: TEdit
              Left = 3
              Top = 13
              Width = 30
              Height = 21
              TabOrder = 0
              Text = '0'
              OnChange = edLoadXmmChange
            end
            object edLoadYmm: TEdit
              Left = 35
              Top = 13
              Width = 30
              Height = 21
              TabOrder = 1
              Text = '0'
              OnChange = edLoadYmmChange
            end
            object btnLoadUnload: TButton
              Left = 5
              Top = 38
              Width = 92
              Height = 20
              Caption = 'Load/Unload'
              Enabled = False
              TabOrder = 2
              OnClick = btnLoadUnloadClick
            end
            object edLoadZmm: TEdit
              Left = 67
              Top = 13
              Width = 30
              Height = 21
              TabOrder = 3
              Text = '100'
            end
          end
          object pnl3DPlot: TPanel
            Left = 4
            Top = 27
            Width = 262
            Height = 56
            TabOrder = 17
            Visible = False
            object lb3DPSU: TLabel
              Left = 4
              Top = 8
              Width = 58
              Height = 13
              Caption = '3D PSU Port'
            end
            object Label45: TLabel
              Left = 12
              Top = 33
              Width = 47
              Height = 13
              Caption = 'Zmm (?.?)'
            end
            object edPSU3DPlot: TEdit
              Left = 68
              Top = 8
              Width = 33
              Height = 21
              TabOrder = 0
              Text = '0'
            end
            object cbOrienZRotation: TComboBox
              Left = 107
              Top = 8
              Width = 50
              Height = 21
              TabOrder = 1
              Text = 'Orient'
              OnChange = cbOrienZRotationChange
            end
            object cbAngleYRotation: TComboBox
              Left = 164
              Top = 8
              Width = 49
              Height = 21
              TabOrder = 2
              Text = 'Angle'
            end
            object ed3DZHeight: TEdit
              Left = 67
              Top = 30
              Width = 34
              Height = 21
              TabOrder = 3
              Text = '50.1'
              OnChange = ed3DZHeightChange
            end
          end
          object Panel14: TPanel
            Left = 4
            Top = 342
            Width = 264
            Height = 55
            BevelOuter = bvLowered
            TabOrder = 18
            object Label53: TLabel
              Left = 11
              Top = 20
              Width = 118
              Height = 13
              Caption = 'X Offset Step / No Steps'
            end
            object Label54: TLabel
              Left = 11
              Top = 39
              Width = 118
              Height = 13
              Caption = 'Y Offset Step / No Steps'
            end
            object Label55: TLabel
              Left = 9
              Top = 1
              Width = 115
              Height = 13
              Caption = 'Default for Full Plot =  0'
            end
            object edXOffsetStart: TEdit
              Left = 145
              Top = 11
              Width = 38
              Height = 21
              TabOrder = 0
              Text = '0'
            end
            object edyOffsetStart: TEdit
              Left = 144
              Top = 32
              Width = 37
              Height = 21
              TabOrder = 1
              Text = '0'
            end
            object edXOffsetSteps: TEdit
              Left = 184
              Top = 11
              Width = 33
              Height = 21
              TabOrder = 2
              Text = '0'
            end
            object edyOffsetSteps: TEdit
              Left = 183
              Top = 32
              Width = 33
              Height = 21
              TabOrder = 3
              Text = '0'
            end
            object btnGotToPlotOffset: TButton
              Left = 218
              Top = 11
              Width = 41
              Height = 41
              Caption = 'Go To'
              TabOrder = 4
              OnClick = btnGotToPlotOffsetClick
            end
          end
        end
        object cbPlotterType: TComboBox
          Left = 8
          Top = 26
          Width = 136
          Height = 21
          TabOrder = 1
          Text = 'Select Plotter Type'
          OnChange = cbPlotterTypeChange
        end
        object btnPlotComportOpen: TButton
          Left = 158
          Top = 19
          Width = 57
          Height = 27
          Caption = 'Open'
          Enabled = False
          TabOrder = 2
          OnClick = btnPlotComportOpenClick
        end
        object btnPlotComportClose: TButton
          Left = 223
          Top = 19
          Width = 56
          Height = 26
          Caption = 'Close'
          Enabled = False
          TabOrder = 3
          OnClick = btnPlotComportCloseClick
        end
      end
      object Panel1: TPanel
        Left = 300
        Top = 7
        Width = 314
        Height = 144
        BevelOuter = bvLowered
        TabOrder = 5
        object TXCommMemoCaption: TLabel
          Left = 231
          Top = 2
          Width = 69
          Height = 13
          Caption = 'TX Comms Log'
        end
        object Label7: TLabel
          Left = 8
          Top = 5
          Width = 114
          Height = 13
          Caption = 'TX COMMUNICATION'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label20: TLabel
          Left = 196
          Top = 24
          Width = 68
          Height = 13
          Caption = 'Tx Comport->'
        end
        object TXCommMemo: TMemo
          Left = 93
          Top = 48
          Width = 201
          Height = 62
          ScrollBars = ssVertical
          TabOrder = 0
        end
        object btTXOpenComms: TButton
          Left = 8
          Top = 51
          Width = 75
          Height = 23
          Caption = 'OPEN'
          TabOrder = 1
          OnClick = btTXOpenCommsClick
        end
        object btTXCloseComms: TButton
          Left = 8
          Top = 77
          Width = 75
          Height = 21
          Caption = 'CLOSE'
          Enabled = False
          TabOrder = 2
          OnClick = btTXCloseCommsClick
        end
        object cbPSU_ONOff: TCheckBox
          Left = 7
          Top = 102
          Width = 85
          Height = 17
          Caption = 'PSU ON/OFF'
          Enabled = False
          TabOrder = 3
          Visible = False
          OnClick = cbPSU_ONOffClick
        end
        object btScanPower: TButton
          Left = 6
          Top = 119
          Width = 81
          Height = 22
          Caption = ' Scan + Power'
          Enabled = False
          TabOrder = 4
          OnClick = btScanPowerClick
        end
        object edTxOpenedVersion: TEdit
          Left = 8
          Top = 23
          Width = 177
          Height = 21
          TabOrder = 5
          Text = 'Tx Version'
        end
        object Button3: TButton
          Left = 136
          Top = 1
          Width = 89
          Height = 17
          Caption = 'Button3'
          TabOrder = 6
          Visible = False
          OnClick = Button3Click
        end
        object edTxComportNumber: TEdit
          Left = 270
          Top = 21
          Width = 33
          Height = 21
          TabOrder = 7
          Text = '0'
        end
        object cbModDepth: TComboBox
          Left = 93
          Top = 116
          Width = 120
          Height = 21
          Enabled = False
          TabOrder = 8
          Text = 'FSK Mod Depth'
          OnChange = cbModDepthChange
        end
      end
      object plPSUControl: TPanel
        Left = 296
        Top = 255
        Width = 162
        Height = 134
        BevelOuter = bvLowered
        TabOrder = 6
        object lbPSUPower: TLabel
          Left = 6
          Top = 4
          Width = 22
          Height = 13
          Caption = 'PSU'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label47: TLabel
          Left = 87
          Top = 40
          Width = 13
          Height = 13
          Caption = 'No'
        end
        object Label48: TLabel
          Left = 9
          Top = 84
          Width = 21
          Height = 13
          Caption = 'Vset'
        end
        object lbPSUAmps: TLabel
          Left = 89
          Top = 82
          Width = 19
          Height = 13
          Caption = 'Iset'
        end
        object Label131: TLabel
          Left = 70
          Top = 62
          Width = 89
          Height = 13
          Caption = '(Dbl click to set VI)'
        end
        object Label132: TLabel
          Left = 104
          Top = 120
          Width = 57
          Height = 13
          Caption = '(Volt / Amp)'
        end
        object btEnablePSU: TButton
          Left = 6
          Top = 59
          Width = 31
          Height = 19
          Caption = 'ON'
          Enabled = False
          TabOrder = 0
          OnClick = btEnablePSUClick
        end
        object btDisablePSU: TButton
          Left = 38
          Top = 59
          Width = 29
          Height = 19
          Caption = 'OFF'
          Enabled = False
          TabOrder = 1
          OnClick = btDisablePSUClick
        end
        object cbEVK: TCheckBox
          Left = 6
          Top = 18
          Width = 129
          Height = 17
          Caption = 'Select'
          TabOrder = 2
          OnClick = cbEVKClick
        end
        object btnOpenPSUGWPort: TButton
          Left = 6
          Top = 38
          Width = 61
          Height = 19
          Caption = 'Open Port'
          Enabled = False
          TabOrder = 3
          OnClick = btnOpenPSUGWPortClick
        end
        object edPSUComport: TEdit
          Left = 113
          Top = 37
          Width = 40
          Height = 21
          Enabled = False
          TabOrder = 4
          Text = '0'
        end
        object edPSUSetVoltage: TEdit
          Left = 45
          Top = 79
          Width = 41
          Height = 21
          Enabled = False
          TabOrder = 5
          Text = '5'
          OnDblClick = edPSUSetVoltageDblClick
        end
        object edPSUAmps: TEdit
          Left = 112
          Top = 79
          Width = 41
          Height = 21
          TabOrder = 6
          Text = '1'
          OnDblClick = edPSUAmpsDblClick
        end
        object cbPSUOptions: TComboBox
          Left = 59
          Top = 14
          Width = 94
          Height = 21
          Enabled = False
          TabOrder = 7
          Text = 'cbPSUOptions'
          OnChange = cbPSUOptionsChange
        end
        object cbRunPSU_OnOff: TCheckBox
          Left = 5
          Top = 118
          Width = 95
          Height = 17
          Caption = 'PS on/off cycle'
          Checked = True
          Enabled = False
          State = cbChecked
          TabOrder = 8
          OnClick = cbRunPSU_OnOffClick
        end
        object Button15: TButton
          Left = 5
          Top = 100
          Width = 62
          Height = 18
          Caption = 'DAQ V/I->'
          TabOrder = 9
          OnClick = Button15Click
        end
        object edPSUDAQV: TEdit
          Left = 67
          Top = 100
          Width = 48
          Height = 21
          TabOrder = 10
          Text = 'PSU DAQ'
        end
        object edPSUDAQI: TEdit
          Left = 113
          Top = 100
          Width = 48
          Height = 21
          TabOrder = 11
          Text = 'PSU DAQ'
        end
        object cbPSU_DaQ: TCheckBox
          Left = 5
          Top = 101
          Width = 61
          Height = 17
          Caption = 'DaQ On'
          TabOrder = 12
          OnClick = cbPSU_DaQClick
        end
      end
      object Panel13: TPanel
        Left = 461
        Top = 255
        Width = 149
        Height = 134
        BevelOuter = bvLowered
        TabOrder = 7
        object Label46: TLabel
          Left = 5
          Top = 2
          Width = 45
          Height = 13
          Caption = 'Rx Load'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label49: TLabel
          Left = 83
          Top = 38
          Width = 13
          Height = 13
          Caption = 'No'
        end
        object Label50: TLabel
          Left = 64
          Top = 99
          Width = 73
          Height = 13
          Caption = '(mVolt / mAmp)'
        end
        object Button7: TButton
          Left = 6
          Top = 97
          Width = 56
          Height = 16
          Caption = 'All OFF'
          Enabled = False
          TabOrder = 0
          OnClick = Button7Click
        end
        object btne_Load: TButton
          Left = 6
          Top = 39
          Width = 65
          Height = 17
          Caption = 'Open Port'
          Enabled = False
          TabOrder = 1
          OnClick = btne_LoadClick
        end
        object cbe_LoadSelect: TCheckBox
          Left = 4
          Top = 17
          Width = 94
          Height = 17
          Caption = 'Select e-Load'
          TabOrder = 2
          OnClick = cbe_LoadSelectClick
        end
        object edeLoadPort: TEdit
          Left = 102
          Top = 38
          Width = 33
          Height = 21
          TabOrder = 3
          Text = '0'
        end
        object Button8: TButton
          Left = 7
          Top = 78
          Width = 55
          Height = 17
          Caption = 'Turn ON'
          Enabled = False
          TabOrder = 4
          OnClick = Button8Click
        end
        object cbELoadCommands: TComboBox
          Left = 6
          Top = 58
          Width = 131
          Height = 21
          Enabled = False
          TabOrder = 5
          Text = 'cbELoadCommands'
          OnClick = cbELoadCommandsClick
        end
        object edELoadValue: TEdit
          Left = 77
          Top = 80
          Width = 60
          Height = 21
          TabOrder = 6
          Text = '0'
          OnDblClick = edELoadValueDblClick
        end
        object cbeLoadOptions: TComboBox
          Left = 56
          Top = 16
          Width = 89
          Height = 21
          Enabled = False
          TabOrder = 7
          Text = 'cbeLoadOptions'
          OnChange = cbeLoadOptionsChange
        end
        object Button27: TButton
          Left = 6
          Top = 114
          Width = 56
          Height = 17
          Caption = 'DAQ V/I->'
          TabOrder = 9
          OnClick = Button27Click
        end
        object edeLoadDAQV: TEdit
          Left = 63
          Top = 112
          Width = 40
          Height = 21
          TabOrder = 10
          Text = 'eload DAQ'
        end
        object edeLoadDAQI: TEdit
          Left = 101
          Top = 112
          Width = 41
          Height = 21
          TabOrder = 11
          Text = 'eload DAQ'
        end
        object cb_E_LoadDaQ: TCheckBox
          Left = 6
          Top = 113
          Width = 57
          Height = 23
          Caption = 'DaQ On'
          TabOrder = 12
          OnClick = cb_E_LoadDaQClick
        end
        object rgLoad: TRadioGroup
          Left = 3
          Top = 34
          Width = 141
          Height = 96
          Caption = 'Rx Resistor Load'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Items.Strings = (
            '0.38W'
            '0.75W'
            '1.5W')
          ParentFont = False
          TabOrder = 8
          OnClick = rgLoadClick
        end
      end
      object Panel6: TPanel
        Left = 300
        Top = 225
        Width = 311
        Height = 25
        BevelOuter = bvLowered
        TabOrder = 8
        object Label60: TLabel
          Left = 11
          Top = 3
          Width = 24
          Height = 13
          Caption = 'DAQ'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label67: TLabel
          Left = 162
          Top = 8
          Width = 58
          Height = 13
          Caption = '<-Ref Maint'
        end
        object cbDAQType: TComboBox
          Left = 41
          Top = 3
          Width = 120
          Height = 21
          TabOrder = 0
          Text = 'Please enter DAQ Option'
          OnChange = cbDAQTypeChange
        end
        object Button1: TButton
          Left = 228
          Top = 4
          Width = 73
          Height = 17
          Caption = 'Start DAQ'
          TabOrder = 1
          OnClick = Button1Click
        end
      end
      object Panel18: TPanel
        Left = 7
        Top = 5
        Width = 289
        Height = 464
        BevelOuter = bvLowered
        TabOrder = 9
        object Label102: TLabel
          Left = 7
          Top = 4
          Width = 143
          Height = 13
          Caption = 'PLOTTER SETTINGS PANEL'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label105: TLabel
          Left = 9
          Top = 144
          Width = 123
          Height = 13
          Caption = 'After owner up Wait (mS)'
        end
        object Label106: TLabel
          Left = 152
          Top = 144
          Width = 99
          Height = 13
          Caption = 'Power Off Time (mS)'
        end
        object Label107: TLabel
          Left = 9
          Top = 200
          Width = 70
          Height = 13
          Caption = 'Xmm Plot Start'
        end
        object Label108: TLabel
          Left = 149
          Top = 199
          Width = 62
          Height = 13
          Caption = 'XStep Offset'
        end
        object Label109: TLabel
          Left = 9
          Top = 224
          Width = 70
          Height = 13
          Caption = 'Ymm Plot Start'
        end
        object Label110: TLabel
          Left = 148
          Top = 223
          Width = 62
          Height = 13
          Caption = 'YStep Offset'
        end
        object Label111: TLabel
          Left = 9
          Top = 271
          Width = 36
          Height = 13
          Caption = 'X Steps'
        end
        object Label112: TLabel
          Left = 62
          Top = 271
          Width = 36
          Height = 13
          Caption = 'Y Steps'
        end
        object Label113: TLabel
          Left = 111
          Top = 271
          Width = 43
          Height = 13
          Caption = 'Step size'
        end
        object Label114: TLabel
          Left = 8
          Top = 308
          Width = 25
          Height = 13
          Caption = 'X mm'
        end
        object Label115: TLabel
          Left = 62
          Top = 308
          Width = 25
          Height = 13
          Caption = 'Y mm'
        end
        object Label126: TLabel
          Left = 10
          Top = 183
          Width = 205
          Height = 13
          Caption = '(Note: Start x,y = 0,0 is Centre of Plotter)'
        end
        object cbPlotterType2: TComboBox
          Left = 7
          Top = 25
          Width = 140
          Height = 21
          TabOrder = 0
          Text = 'cbPlotterType2'
          OnChange = cbPlotterType2Change
        end
        object btnPlotComportOpen2: TButton
          Left = 152
          Top = 21
          Width = 57
          Height = 25
          Caption = 'Open'
          TabOrder = 1
          OnClick = btnPlotComportOpen2Click
        end
        object btnPlotComportClose2: TButton
          Left = 219
          Top = 21
          Width = 57
          Height = 25
          Caption = 'Close'
          TabOrder = 2
          OnClick = btnPlotComportClose2Click
        end
        object Panel19: TPanel
          Left = 7
          Top = 50
          Width = 278
          Height = 87
          BevelOuter = bvLowered
          TabOrder = 3
          object Label101: TLabel
            Left = 8
            Top = 10
            Width = 96
            Height = 13
            Caption = 'Plotter Comport No.'
          end
          object Label103: TLabel
            Left = 8
            Top = 32
            Width = 58
            Height = 13
            Caption = '3D PSU Port'
          end
          object Label104: TLabel
            Left = 8
            Top = 56
            Width = 47
            Height = 13
            Caption = 'Zmm (?.?)'
          end
          object Label128: TLabel
            Left = 125
            Top = 59
            Width = 65
            Height = 13
            Caption = '(290mm Max)'
          end
          object Label130: TLabel
            Left = 222
            Top = 50
            Width = 50
            Height = 13
            Caption = 'zmm Move'
          end
          object edPlotterComportNo2: TEdit
            Left = 109
            Top = 5
            Width = 33
            Height = 21
            TabOrder = 0
            Text = '0'
          end
          object btnHomePlotterSet2: TButton
            Left = 176
            Top = 4
            Width = 90
            Height = 22
            Caption = 'HOME PLOTTER'
            TabOrder = 1
            OnClick = btnHomePlotterSet2Click
          end
          object edPSU3DPlot2: TEdit
            Left = 80
            Top = 29
            Width = 41
            Height = 21
            TabOrder = 2
            Text = '0'
          end
          object cbOrienZRotation2: TComboBox
            Left = 128
            Top = 30
            Width = 68
            Height = 21
            TabOrder = 3
            Text = 'cbOrienZRotation2'
            OnChange = cbOrienZRotation2Change
          end
          object cbAngleYRotation2: TComboBox
            Left = 202
            Top = 29
            Width = 59
            Height = 21
            TabOrder = 4
            Text = 'cbAngleYRotation2'
          end
          object ed3DZHeight2: TEdit
            Left = 80
            Top = 53
            Width = 41
            Height = 21
            TabOrder = 5
            Text = '280'
            OnChange = ed3DZHeight2Change
          end
          object Button24: TButton
            Left = 196
            Top = 52
            Width = 21
            Height = 16
            Caption = '^'
            TabOrder = 6
            OnClick = Button24Click
          end
          object Button25: TButton
            Left = 196
            Top = 68
            Width = 21
            Height = 16
            Caption = 'v'
            TabOrder = 7
            OnClick = Button25Click
          end
          object edZheightNudge: TEdit
            Left = 224
            Top = 63
            Width = 41
            Height = 21
            TabOrder = 8
            Text = '0.1'
          end
        end
        object ebPowerUpWait2: TEdit
          Left = 8
          Top = 159
          Width = 121
          Height = 21
          TabOrder = 4
          Text = '12000'
          OnChange = ebPowerUpWait2Change
        end
        object ebOffTime2: TEdit
          Left = 152
          Top = 160
          Width = 113
          Height = 21
          TabOrder = 5
          Text = '1000'
          OnChange = ebOffTime2Change
        end
        object ebXSetpointStart2: TEdit
          Left = 82
          Top = 197
          Width = 44
          Height = 21
          TabOrder = 6
          Text = '0'
          OnChange = ebXSetpointStart2Change
        end
        object ebXOffset2: TEdit
          Left = 221
          Top = 197
          Width = 44
          Height = 21
          TabOrder = 7
          Text = '0'
          OnChange = ebXOffset2Change
        end
        object ebYSetpointStart2: TEdit
          Left = 83
          Top = 220
          Width = 43
          Height = 21
          TabOrder = 8
          Text = '0'
          OnChange = ebYSetpointStart2Change
        end
        object ebYOffset2: TEdit
          Left = 220
          Top = 221
          Width = 44
          Height = 21
          TabOrder = 9
          Text = '0'
          OnChange = ebYOffset2Change
        end
        object btnManGoToTestStart2: TButton
          Left = 8
          Top = 243
          Width = 121
          Height = 17
          Caption = 'Manual Go to Start'
          TabOrder = 10
          OnClick = btnManGoToTestStart2Click
        end
        object btGotoXY2: TButton
          Left = 147
          Top = 243
          Width = 118
          Height = 17
          Caption = 'Go to Offset Above'
          TabOrder = 11
          OnClick = btGotoXY2Click
        end
        object ebXSteps2: TEdit
          Left = 7
          Top = 288
          Width = 49
          Height = 21
          TabOrder = 12
          Text = '10'
          OnChange = ebXSteps2Change
        end
        object ebYSteps2: TEdit
          Left = 58
          Top = 288
          Width = 52
          Height = 21
          TabOrder = 13
          Text = '10'
          OnChange = ebYSteps2Change
        end
        object ebStepSize2: TEdit
          Left = 111
          Top = 288
          Width = 44
          Height = 21
          TabOrder = 14
          Text = '10'
          OnChange = ebStepSize2Change
        end
        object edXStepmm2: TEdit
          Left = 7
          Top = 323
          Width = 36
          Height = 21
          Enabled = False
          TabOrder = 15
          Text = '0'
        end
        object edYStepmm2: TEdit
          Left = 59
          Top = 323
          Width = 33
          Height = 21
          Enabled = False
          TabOrder = 16
          Text = '0'
        end
        object Panel20: TPanel
          Left = 159
          Top = 312
          Width = 124
          Height = 71
          BevelOuter = bvLowered
          TabOrder = 17
          object Label118: TLabel
            Left = 87
            Top = 13
            Width = 21
            Height = 13
            Caption = 'zmm'
          end
          object Label117: TLabel
            Left = 45
            Top = 12
            Width = 22
            Height = 13
            Caption = 'ymm'
          end
          object Label116: TLabel
            Left = 7
            Top = 13
            Width = 22
            Height = 13
            Caption = 'xmm'
          end
          object Label127: TLabel
            Left = 2
            Top = 2
            Width = 121
            Height = 13
            Caption = '(x,y = Offset from Start)'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object edLoadXmm2: TEdit
            Left = 6
            Top = 28
            Width = 35
            Height = 21
            TabOrder = 0
            Text = '0'
            OnChange = edLoadXmm2Change
          end
          object edLoadYmm2: TEdit
            Left = 43
            Top = 28
            Width = 35
            Height = 21
            TabOrder = 1
            Text = '0'
            OnChange = edLoadYmm2Change
          end
          object edLoadZmm2: TEdit
            Left = 83
            Top = 27
            Width = 35
            Height = 21
            TabOrder = 2
            Text = '200'
            OnChange = edLoadZmm2Change
          end
          object btnLoadUnload2: TButton
            Left = 21
            Top = 53
            Width = 89
            Height = 15
            Caption = 'Load/Unload'
            TabOrder = 3
            OnClick = btnLoadUnload2Click
          end
        end
        object Panel21: TPanel
          Left = 7
          Top = 387
          Width = 277
          Height = 73
          BevelOuter = bvLowered
          TabOrder = 18
          object Label119: TLabel
            Left = 14
            Top = 58
            Width = 175
            Height = 13
            Caption = 'Default for Full Plot = 0 for all above'
          end
          object Label120: TLabel
            Left = 15
            Top = 21
            Width = 51
            Height = 13
            Caption = 'X Steps ->'
          end
          object Label121: TLabel
            Left = 15
            Top = 38
            Width = 51
            Height = 13
            Caption = 'Y Steps ->'
          end
          object Label122: TLabel
            Left = 89
            Top = 2
            Width = 58
            Height = 13
            Caption = 'Start Offset'
          end
          object Label123: TLabel
            Left = 8
            Top = 2
            Width = 53
            Height = 13
            Caption = 'SUB PLOT'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label124: TLabel
            Left = 156
            Top = 2
            Width = 86
            Height = 13
            Caption = 'Steps from Offset'
          end
          object edXOffsetStart2: TEdit
            Left = 88
            Top = 17
            Width = 41
            Height = 21
            TabOrder = 0
            Text = '0'
          end
          object edXOffsetSteps2: TEdit
            Left = 154
            Top = 18
            Width = 41
            Height = 21
            TabOrder = 1
            Text = '0'
          end
          object edyOffsetStart2: TEdit
            Left = 88
            Top = 37
            Width = 41
            Height = 21
            TabOrder = 2
            Text = '0'
          end
          object edyOffsetSteps2: TEdit
            Left = 154
            Top = 38
            Width = 41
            Height = 21
            TabOrder = 3
            Text = '0'
          end
          object btnGotToPlotOffset2: TButton
            Left = 201
            Top = 29
            Width = 70
            Height = 31
            Caption = 'Go To'
            TabOrder = 4
            OnClick = btnGotToPlotOffset2Click
          end
        end
        object btnGoToStepEnd2: TBitBtn
          Left = 7
          Top = 352
          Width = 121
          Height = 24
          Caption = 'Go to Step Location'
          TabOrder = 19
          OnClick = btnGoToStepEnd2Click
        end
        object GroupBox9: TGroupBox
          Left = 160
          Top = 262
          Width = 126
          Height = 47
          Caption = 'Manual Move'
          TabOrder = 20
          object Label129: TLabel
            Left = 77
            Top = 5
            Width = 45
            Height = 13
            Caption = 'Move mm'
          end
          object Button20: TButton
            Left = 30
            Top = 13
            Width = 21
            Height = 16
            Caption = '^'
            TabOrder = 0
            OnClick = Button20Click
          end
          object edNudgeAmount: TEdit
            Left = 80
            Top = 18
            Width = 41
            Height = 21
            TabOrder = 1
            Text = '0.1'
          end
          object Button21: TButton
            Left = 30
            Top = 28
            Width = 21
            Height = 16
            Caption = 'v'
            TabOrder = 2
            OnClick = Button21Click
          end
          object Button22: TButton
            Left = 51
            Top = 18
            Width = 20
            Height = 17
            Caption = '>'
            TabOrder = 3
            OnClick = Button22Click
          end
          object Button23: TButton
            Left = 10
            Top = 18
            Width = 20
            Height = 17
            Caption = '<'
            TabOrder = 4
            OnClick = Button23Click
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 297
        Top = 388
        Width = 205
        Height = 81
        Caption = 'Retry Criteria'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 10
        object Label39: TLabel
          Left = 8
          Top = 17
          Width = 113
          Height = 11
          Caption = 'RETRY Rx_VDC min value'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label43: TLabel
          Left = 9
          Top = 37
          Width = 109
          Height = 11
          Caption = 'Signal Strength min eg100'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label42: TLabel
          Left = 9
          Top = 56
          Width = 105
          Height = 11
          Caption = 'END RX_VDC min Value'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object edRxVoltReScanLimit: TEdit
          Left = 145
          Top = 12
          Width = 35
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Text = '4.7'
          OnChange = edRxVoltReScanLimitChange
        end
        object edSigStrenReScanLimit: TEdit
          Left = 145
          Top = 30
          Width = 35
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          Text = '150'
          OnChange = edSigStrenReScanLimitChange
        end
        object edRxVoltFinalTestLimit: TEdit
          Left = 145
          Top = 52
          Width = 34
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Text = '4.7'
          OnChange = edRxVoltFinalTestLimitChange
        end
      end
    end
    object Panel3: TPanel
      Left = 3
      Top = 38
      Width = 620
      Height = 699
      BevelOuter = bvNone
      TabOrder = 1
      object lbTestControl: TLabel
        Left = 10
        Top = 0
        Width = 118
        Height = 13
        Caption = 'TEST CONTROL PANEL'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 165
        Top = 4
        Width = 78
        Height = 13
        Caption = 'Total Plots Done'
      end
      object Label11: TLabel
        Left = 170
        Top = 103
        Width = 71
        Height = 13
        Caption = 'Retry Count->'
        Visible = False
      end
      object lbCaptionDistance: TLabel
        Left = 16
        Top = 310
        Width = 90
        Height = 13
        Caption = 'Distance to Dest ::'
      end
      object lbDistance: TLabel
        Left = 110
        Top = 310
        Width = 62
        Height = 16
        Caption = 'Value mm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbCaptionCords: TLabel
        Left = 18
        Top = 291
        Width = 88
        Height = 13
        Caption = 'Set Coordinates ::'
      end
      object lbCords: TLabel
        Left = 112
        Top = 288
        Width = 62
        Height = 16
        Caption = 'Value mm'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label21: TLabel
        Left = 170
        Top = 138
        Width = 83
        Height = 13
        Caption = 'Grid Colour eg 50'
      end
      object Label37: TLabel
        Left = 16
        Top = 78
        Width = 64
        Height = 13
        Caption = 'Mod Depth->'
        Visible = False
      end
      object lbStatusGridSeq: TLabel
        Left = 303
        Top = 157
        Width = 48
        Height = 13
        Caption = 'Status ind'
      end
      object Label51: TLabel
        Left = 471
        Top = 156
        Width = 91
        Height = 13
        Caption = 'No of Seq Rows ->'
      end
      object lbIniFilename: TLabel
        Left = 6
        Top = 688
        Width = 389
        Height = 13
        Caption = 
          '[Above Left: Dble Click to Open Seq, Above Right: Dble Click to ' +
          'Save Seq Ini File]'
      end
      object lblProgramStateIndicator: TLabel
        Left = 13
        Top = 686
        Width = 3
        Height = 13
      end
      object StrGridSeqTest: TStringGrid
        Left = 7
        Top = 559
        Width = 599
        Height = 91
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing, goFixedColClick]
        ScrollBars = ssVertical
        TabOrder = 21
        OnClick = StrGridSeqTestClick
        OnDblClick = StrGridSeqTestDblClick
        OnDrawCell = StrGridSeqTestDrawCell
        OnExit = StrGridSeqTestExit
        OnSelectCell = StrGridSeqTestSelectCell
      end
      object rteIniFile: TRichEdit
        Left = 480
        Top = 175
        Width = 130
        Height = 386
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        Lines.Strings = (
          'rTextEdit')
        ParentFont = False
        PlainText = True
        TabOrder = 22
        Visible = False
        OnDblClick = rteIniFileDblClick
      end
      object Chart1: TChart
        Left = 20
        Top = 319
        Width = 589
        Height = 149
        Legend.Visible = False
        Title.Text.Strings = (
          'Efficiency')
        View3D = False
        TabOrder = 13
        Visible = False
        ColorPaletteIndex = 13
        object Series1: TLineSeries
          Marks.Arrow.Visible = True
          Marks.Callout.Brush.Color = clBlack
          Marks.Callout.Arrow.Visible = True
          Marks.Visible = False
          Brush.BackColor = clDefault
          Pointer.InflateMargins = True
          Pointer.Style = psRectangle
          Pointer.Visible = False
          XValues.Name = 'X'
          XValues.Order = loAscending
          YValues.Name = 'Y'
          YValues.Order = loNone
        end
      end
      object GridPanel1: TGridPanel
        Left = 18
        Top = 212
        Width = 536
        Height = 350
        Caption = 'gdPlotResults'
        ColumnCollection = <
          item
            Value = 50.000000000000000000
          end
          item
            Value = 50.000000000000000000
          end>
        ControlCollection = <>
        RowCollection = <
          item
            Value = 50.000000000000000000
          end
          item
            Value = 50.000000000000000000
          end>
        TabOrder = 7
      end
      object StringGrid2: TStringGrid
        Left = 7
        Top = 175
        Width = 599
        Height = 383
        TabOrder = 20
        Visible = False
        OnDblClick = StringGrid2DblClick
        OnDrawCell = StringGrid2DrawCell
        ColWidths = (
          64
          64
          64
          64
          64)
      end
      object StringGrid1: TStringGrid
        Left = 8
        Top = 175
        Width = 600
        Height = 384
        TabOrder = 8
        OnDblClick = StringGrid1DblClick
        OnDrawCell = StringGrid1DrawCell
        RowHeights = (
          24
          24
          24
          24
          24)
      end
      object StringGrid3: TStringGrid
        Left = 9
        Top = 560
        Width = 599
        Height = 89
        TabOrder = 29
        OnDblClick = StringGrid3DblClick
      end
      object btStartTest: TButton
        Left = 12
        Top = 119
        Width = 83
        Height = 18
        Caption = 'START'
        TabOrder = 0
        OnClick = btStartTestClick
      end
      object btnPause: TButton
        Left = 106
        Top = 117
        Width = 75
        Height = 21
        Caption = 'Pause'
        Enabled = False
        TabOrder = 1
        OnClick = btnPauseClick
      end
      object cbManual_Mode: TCheckBox
        Left = 147
        Top = 53
        Width = 121
        Height = 17
        Caption = 'Tx Loop-detect Off'
        TabOrder = 2
        Visible = False
        OnClick = cbManual_ModeClick
      end
      object btStopTest: TButton
        Left = 187
        Top = 117
        Width = 87
        Height = 21
        Caption = 'STOP'
        Enabled = False
        TabOrder = 3
        OnClick = btStopTestClick
      end
      object ebRepeat: TEdit
        Left = 250
        Top = 1
        Width = 50
        Height = 21
        TabOrder = 4
        Text = '0'
      end
      object cbEnablePSUControl: TCheckBox
        Left = 151
        Top = 68
        Width = 123
        Height = 17
        Caption = 'Tx PSU Auto ON/OFF'
        TabOrder = 5
        Visible = False
        OnClick = cbEnablePSUControlClick
      end
      object Panel9: TPanel
        Left = 306
        Top = 5
        Width = 300
        Height = 133
        BevelOuter = bvLowered
        TabOrder = 6
        object lbEff: TLabel
          Left = 127
          Top = 65
          Width = 35
          Height = 23
          Caption = '0%'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbCaptionEff: TLabel
          Left = 18
          Top = 69
          Width = 57
          Height = 13
          Caption = 'Efficiency ='
        end
        object lbOutputPower: TLabel
          Left = 143
          Top = 52
          Width = 26
          Height = 13
          Caption = '0.0W'
        end
        object lbCaptionOutputPower: TLabel
          Left = 18
          Top = 50
          Width = 57
          Height = 13
          Caption = 'Power Rx ='
        end
        object lbInputPower: TLabel
          Left = 144
          Top = 33
          Width = 26
          Height = 13
          Caption = '0.0W'
        end
        object lbCaptionInputPower: TLabel
          Left = 17
          Top = 34
          Width = 59
          Height = 13
          Caption = 'Power Tx  ='
        end
        object lblTimeRemaining: TLabel
          Left = 18
          Top = 90
          Width = 112
          Height = 13
          Caption = 'Time Remaining for Plot'
        end
        object timeRemainlb: TLabel
          Left = 145
          Top = 89
          Width = 46
          Height = 13
          Caption = 'Test Time'
        end
        object lbQiSignal_strength: TLabel
          Left = 141
          Top = 10
          Width = 10
          Height = 19
          Alignment = taCenter
          Caption = '_'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbQiSignalStrengthCaption: TLabel
          Left = 19
          Top = 14
          Width = 104
          Height = 13
          Caption = 'Tx Signal Strength'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 196
          Top = 52
          Width = 41
          Height = 13
          Caption = 'Rload = '
        end
        object lblRxLoadName: TLabel
          Left = 243
          Top = 52
          Width = 15
          Height = 13
          Caption = '?W'
        end
        object lbCountDownState: TLabel
          Left = 17
          Top = 108
          Width = 80
          Height = 13
          Caption = 'Wait Countdown'
        end
        object lblWaitCountdown: TLabel
          Left = 152
          Top = 108
          Width = 5
          Height = 13
          Caption = '?'
        end
        object lbRetryNotice: TLabel
          Left = 195
          Top = 108
          Width = 70
          Height = 13
          Caption = 'Re-Try Status '
          Visible = False
        end
        object lbRepeatCountStatus: TLabel
          Left = 197
          Top = 88
          Width = 67
          Height = 13
          Caption = 'Repeat Count'
        end
        object lbControlMessage: TLabel
          Left = 20
          Top = 4
          Width = 87
          Height = 11
          Caption = 'Warning Message'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -9
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = lbControlMessageClick
        end
        object cbCoilStatus: TComboBox
          Left = 196
          Top = 12
          Width = 97
          Height = 21
          TabOrder = 0
          Text = 'Coil Status'
          Visible = False
        end
      end
      object cbDoNotMoveXY: TCheckBox
        Left = 12
        Top = 97
        Width = 118
        Height = 17
        Caption = 'Do Not Move XY'
        TabOrder = 10
        Visible = False
      end
      object cbHoleRetry: TCheckBox
        Left = 151
        Top = 91
        Width = 121
        Height = 12
        Caption = 'Retry ON/OFF'
        TabOrder = 11
        Visible = False
        OnClick = cbHoleRetryClick
      end
      object ebHoleRetryNo: TEdit
        Left = 247
        Top = 91
        Width = 27
        Height = 21
        TabOrder = 12
        Text = '*.INI'
        Visible = False
      end
      object cbShowProgChart: TCheckBox
        Left = 83
        Top = 140
        Width = 125
        Height = 17
        Caption = 'Start Chart Recording'
        TabOrder = 14
        Visible = False
        OnClick = cbShowProgChartClick
      end
      object btnClearChart: TButton
        Left = 419
        Top = -4
        Width = 87
        Height = 17
        Caption = 'Clear Chart'
        TabOrder = 15
        Visible = False
        OnClick = btnClearChartClick
      end
      object cbPowerSupplyStatus: TCheckBox
        Left = 304
        Top = 142
        Width = 159
        Height = 17
        Caption = 'Power Supply ON/Off Status'
        TabOrder = 16
        Visible = False
        OnClick = cbPowerSupplyStatusClick
      end
      object edGridColourThreshold: TEdit
        Left = 260
        Top = 140
        Width = 35
        Height = 21
        TabOrder = 17
        Text = '50'
        OnChange = edGridColourThresholdChange
      end
      object edTxModDepthView: TEdit
        Left = 86
        Top = 70
        Width = 43
        Height = 21
        TabOrder = 18
        Visible = False
      end
      object Button6: TButton
        Left = 413
        Top = 677
        Width = 99
        Height = 25
        Caption = 'Read CSV to Grid'
        TabOrder = 19
        Visible = False
        OnClick = Button6Click
      end
      object btnStartSeqGrid: TBitBtn
        Left = 12
        Top = 118
        Width = 84
        Height = 21
        Caption = 'Run Seq'
        TabOrder = 9
        OnClick = btnStartSeqGridClick
      end
      object btnStopSeqGrid: TButton
        Left = 187
        Top = 117
        Width = 88
        Height = 21
        Caption = 'Stop Seq'
        Enabled = False
        TabOrder = 23
        OnClick = btnStopSeqGridClick
      end
      object edSeqRowCount: TEdit
        Left = 566
        Top = 150
        Width = 43
        Height = 21
        TabOrder = 24
        Text = '20'
        OnExit = edSeqRowCountExit
      end
      object FileListBox1: TFileListBox
        Left = 7
        Top = 654
        Width = 316
        Height = 26
        FileEdit = ebHoleRetryNo
        ItemHeight = 13
        Mask = '*.INI'
        TabOrder = 25
        OnDblClick = FileListBox1DblClick
      end
      object cbSeqIniFilenames: TComboBox
        Left = 322
        Top = 654
        Width = 286
        Height = 21
        TabOrder = 26
        OnDblClick = cbSeqIniFilenamesDblClick
      end
      object Panel15: TPanel
        Left = 9
        Top = 24
        Width = 291
        Height = 38
        BevelOuter = bvSpace
        TabOrder = 27
        object shPlotterConnected: TShape
          Left = 9
          Top = 2
          Width = 14
          Height = 17
          Brush.Color = clSilver
          Shape = stCircle
          OnMouseActivate = shPlotterConnectedMouseActivate
        end
        object lbPlotterConnected: TLabel
          Left = 29
          Top = 1
          Width = 57
          Height = 13
          Caption = 'Plotter ON'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object shpPSUConnected: TShape
          Left = 102
          Top = 3
          Width = 14
          Height = 17
          Brush.Color = clSilver
          Shape = stCircle
        end
        object Label56: TLabel
          Left = 121
          Top = 3
          Width = 40
          Height = 13
          Caption = 'PSU ON'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object shpDAQConnected: TShape
          Left = 9
          Top = 18
          Width = 14
          Height = 17
          Brush.Color = clSilver
          Shape = stCircle
        end
        object Label58: TLabel
          Left = 29
          Top = 20
          Width = 42
          Height = 13
          Caption = 'DAQ ON'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object shpELoadConnected: TShape
          Left = 177
          Top = 3
          Width = 14
          Height = 17
          Brush.Color = clSilver
          Shape = stCircle
        end
        object Label57: TLabel
          Left = 197
          Top = 4
          Width = 57
          Height = 13
          Caption = 'e-Load ON'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object shpMISDataConnected: TShape
          Left = 101
          Top = 20
          Width = 14
          Height = 17
          Brush.Color = clSilver
          Shape = stCircle
        end
        object lbMisc: TLabel
          Left = 121
          Top = 21
          Width = 25
          Height = 13
          Caption = 'Misc'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object cbSimPlotting: TCheckBox
        Left = 9
        Top = 65
        Width = 225
        Height = 25
        Caption = 'Simulate Plotting'
        TabOrder = 28
      end
      object cbPlotAreaOptions: TComboBox
        Left = 12
        Top = 96
        Width = 85
        Height = 21
        TabOrder = 30
        Text = 'Set Plot Area'
      end
      object Panel17: TPanel
        Left = 148
        Top = 63
        Width = 152
        Height = 25
        TabOrder = 31
        object lbTxConnectedStatus: TLabel
          Left = 35
          Top = 9
          Width = 32
          Height = 13
          Caption = 'Tx ON'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object shpTxConnected: TShape
          Left = 15
          Top = 5
          Width = 14
          Height = 17
          Brush.Color = clSilver
          Shape = stCircle
        end
      end
      object cbGridTypeDisplay: TComboBox
        Left = 8
        Top = 153
        Width = 100
        Height = 21
        TabOrder = 32
        Text = 'Efficiency'
        OnChange = cbGridTypeDisplayChange
      end
    end
  end
  object btnProgramClose: TButton
    Left = 525
    Top = 723
    Width = 90
    Height = 27
    Caption = 'Close Program'
    TabOrder = 1
    OnClick = btnProgramCloseClick
  end
  object ListBox1: TListBox
    Left = 944
    Top = 576
    Width = 1
    Height = 25
    ItemHeight = 13
    TabOrder = 2
  end
  object tmMeterUpdate: TTimer
    Enabled = False
    Interval = 500
    OnTimer = tmMeterUpdateTimer
    Left = 368
    Top = 65533
  end
  object tmTest: TTimer
    Enabled = False
    Interval = 200
    OnTimer = tmTestTimer
    Left = 424
    Top = 65533
  end
  object tmPlotter: TTimer
    Enabled = False
    Interval = 100
    OnTimer = tmPlotterTimer
    Left = 312
    Top = 65533
  end
  object MainMenu1: TMainMenu
    object GUI1: TMenuItem
      Caption = 'GUI Menu'
      object CloseGUI1: TMenuItem
        Caption = 'Close GUI'
        OnClick = CloseGUI1Click
      end
    end
    object FormSize1: TMenuItem
      Caption = 'Form Size'
      object Big_View: TMenuItem
        Caption = 'Wide View'
        OnClick = BClick
      end
    end
    object Help1: TMenuItem
      Caption = 'Help'
      object About1: TMenuItem
        Caption = 'About'
        OnClick = About1Click
      end
      object Help_RegEdit: TMenuItem
        Caption = 'Registry Settings'
        OnClick = Help_RegEditClick
      end
      object SoftwarePCRequirements1: TMenuItem
        Caption = 'Software PC Requirements'
        OnClick = SoftwarePCRequirements1Click
      end
    end
  end
  object tmInit_SM: TTimer
    Enabled = False
    Interval = 1
    OnTimer = tmInit_SMTimer
    Left = 536
    Top = 65533
  end
  object tmAcknowledge: TTimer
    Enabled = False
    Interval = 511
    OnTimer = tmAcknowledgeTimer
    Left = 480
    Top = 65533
  end
  object tmPlotter_NRT: TTimer
    Enabled = False
    OnTimer = tmPlotter_NRTTimer
    Left = 592
    Top = 65533
  end
  object Qi_Comport: TApdComPort
    AutoOpen = False
    TraceName = 'APRO.TRC'
    LogName = 'APRO.LOG'
    OnTriggerAvail = Qi_ComportTriggerAvail
    Left = 88
  end
  object Plotter_Comport: TApdComPort
    AutoOpen = False
    TraceName = 'APRO.TRC'
    LogName = 'APRO.LOG'
    OnTriggerAvail = Plotter_ComportTriggerAvail
    Left = 48
  end
  object PSU_Comport: TApdComPort
    TraceName = 'APRO.TRC'
    LogName = 'APRO.LOG'
    Left = 128
  end
  object tmSeqTest2: TTimer
    Enabled = False
    Interval = 1
    OnTimer = tmSeqTest2Timer
    Left = 592
    Top = 40
  end
  object tmSeqTestWait: TTimer
    Interval = 1
    OnTimer = tmSeqTestWaitTimer
    Left = 592
    Top = 72
  end
  object PSU_ComportGW: TApdComPort
    Baud = 115200
    TraceName = 'APRO.TRC'
    LogName = 'APRO.LOG'
    OnTriggerAvail = PSU_ComportGWTriggerAvail
    Left = 168
  end
  object eLoad_Comport: TApdComPort
    TraceName = 'APRO.TRC'
    LogName = 'APRO.LOG'
    OnTriggerAvail = eLoad_ComportTriggerAvail
    Left = 208
  end
  object Gen_Comport: TApdComPort
    TraceName = 'APRO.TRC'
    LogName = 'APRO.LOG'
    OnPortClose = Gen_ComportPortClose
    OnTriggerAvail = Gen_ComportTriggerAvail
    Left = 248
  end
  object Arduino_FOD: TApdComPort
    TraceName = 'APRO.TRC'
    LogName = 'APRO.LOG'
    Left = 288
  end
end
