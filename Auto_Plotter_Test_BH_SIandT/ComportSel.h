//----------------------------------------------------------------------------
#ifndef ComportSelH
#define ComportSelH
//----------------------------------------------------------------------------
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
#include <StdCtrls.hpp>
#include <Controls.hpp>
#include <Forms.hpp>
#include <Graphics.hpp>
#include <Classes.hpp>
#include <SysUtils.hpp>
#include <Windows.hpp>
#include <System.hpp>
//----------------------------------------------------------------------------
class TDeviceSelectionForm : public TForm
{
__published:
	TBitBtn *dsfOkBitBtn;
	TBitBtn *dsfCancelBitBtn;
	TComboBox *dsfComboBox;
	TLabel *Label1;
	TEdit *edBaudRateSet;
	TLabel *Label2;
	void __fastcall dsfCancelBitBtnClick(TObject *Sender);
	void __fastcall dsfOkBitBtnClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);

private:
	System::Word FComNumber;
	bool FEnumerated;
	Classes::TStringList* FPortItemList;
	AnsiString FDeviceName;
	BOOL __fastcall TDeviceSelectionForm::COM_exists( int port);
	void __fastcall TDeviceSelectionForm::EnumComPorts(void);
	void __fastcall TDeviceSelectionForm::EnumAllPorts(void);
	bool __fastcall TDeviceSelectionForm::IsPortAvailable( int port);
public:
	virtual __fastcall TDeviceSelectionForm(TComponent* AOwner, AnsiString windowLabel);
	__property System::Word ComNumber = {read=FComNumber, write=FComNumber, nodefault};
	__property AnsiString DeviceName = {read=FDeviceName, write=FDeviceName};
};
//----------------------------------------------------------------------------
extern PACKAGE TDeviceSelectionForm *DeviceSelectionForm;
//----------------------------------------------------------------------------
#endif    
