/***************************************************************************H1*

	  Filename    : SerialIO
      Description : Handles all the serial IO
      Created     : January 1, 2003
      Last Edit   : January 1, 2003
 
*******************************************************************************
                    Copyright (c) 2003 - Michael Heyns
       This file contains confidential and proprietary information.
      All use, disclosure, and/or reproduction is strictly prohibited.
						   All rights reserved.
*******************************************************************************

Specifying Serial Ports Larger than COM9

PSS ID Number: Q115831

Authored 05-Jun-1994			Last modified 25-May-1995

The information in this article applies to:

 - Microsoft Win32 Software Development Kit (SDK), versions 3.1, 3.5,
   3.51, and 4.0

CreateFile() can be used to get a handle to a serial port. The "Win32
Programmer's Reference" entry for "CreateFile()" mentions that the share
mode must be 0, the create parameter must be OPEN_EXISTING, and the
template must be NULL.

CreateFile() is successful when you use "COM1" through "COM9" for the name

of the file; however, the message "INVALID_HANDLE_VALUE" is returned if you
use "COM10" or greater.

If the name of the port is \\.\COM10, the correct way to specify the serial
port in a call to CreateFile() is as follows:

   CreateFile(
      "\\\\.\\COM10",     // address of name of the communications device
      fdwAccess,          // access (read-write) mode
	  0,                  // share mode
      NULL,               // address of security descriptor

      OPEN_EXISTING,      // how to create
	  0,                  // file attributes
	  NULL                // handle of file with attributes to copy
   );

NOTES: This syntax also works for ports COM1 through COM9. Certain boards
will let you choose the port names yourself. This syntax works for those
names as well.

Additional reference words: 3.10 3.50 4.00 95
KBCategory: kbprg
KBSubcategory: BseCommapi

******************************************************************************/

#include "main.h"

/*----------------------------------------------------------------------
 * Writes a character buffer to a serial interface
 *----------------------------------------------------------------------*/
int __fastcall sndWriteSerial(HANDLE hCom, char *Str)
{
	unsigned long Written = 0;
	if (hCom != NULL)
		WriteFile(hCom, Str, strlen(Str), &Written, NULL);
	return(Written);
}

/*----------------------------------------------------------------------
 * Writes a character buffer to a serial interface
 *----------------------------------------------------------------------*/
int __fastcall sndWriteSerial_N(HANDLE hCom, char *Str, uint32_t len)
{
	unsigned long Written = 0;
	if (hCom != NULL)
		WriteFile(hCom, Str, len, &Written, NULL);
	return(Written);
}

/*----------------------------------------------------------------------
 * Reads a character buffer from a serial interface
 *----------------------------------------------------------------------*/
int __fastcall sndReadSerial(HANDLE hCom, char *Str, unsigned long max)
{
	unsigned long now = 0;

	if (hCom != NULL)
		ReadFile(hCom, Str, max, &now, NULL);
	// do not terminate the buf here - the caller must do it
	return((int)now);
}

/*----------------------------------------------------------------------
 * Opens a serial interface
 * Timeout is till the FIRST character AND between each character
 *----------------------------------------------------------------------*/
HANDLE __fastcall sndOpenSerialPort(int Port, int Baud, bool Handshaking, int ReadTimeout, int WriteTimeout)
{
	DCB dcb;
	HANDLE hCom;
//	DWORD dwError;
	BOOL fSuccess;
	COMMTIMEOUTS CommTimeouts;

	// validate port number
//	if (Port < 0 || Port > MAXPORTS)
	if (Port < 0 || Port > 200)
		return(NULL);

	// create the 'file'
	char Name[15];
	sprintf(Name, "\\\\.\\COM%d", Port);
	hCom = CreateFile(Name,
		GENERIC_READ | GENERIC_WRITE,
		0,    					/* comm devisndOpenSerialPortces must be opened w/exclusive-access */
		NULL, 					/* no security attrs */
		OPEN_EXISTING, 			/* comm devices must use OPEN_EXISTING */
		0,    					/* not overlapped I/O */
		NULL  					/* hTemplate must be NULL for comm devices */
		);

	// test handle
	if (hCom == INVALID_HANDLE_VALUE)
	{
//		dwError = GetLastError();
		/* handle error */
		return(NULL);
	}

	// clear the port
	char ch;
	unsigned long now;
	while (sndDataWaiting(hCom))
		ReadFile(hCom, &ch, 1, &now, NULL);

	// flush the port
	PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

	// clear all outstanding errors
	DWORD errors;
	COMSTAT comstat;
	ClearCommError(hCom, &errors, &comstat);

	// Omit the call to SetupComm to use the default queue sizes and
	// Get the current configuration.
	fSuccess = GetCommState(hCom, &dcb);
	if (!fSuccess)
	{
		/* Handle the error. */
		return(NULL);
	}

	// Fill in the DCB (Device Control Block)
	dcb.BaudRate = Baud;
	dcb.ByteSize = 8;
	dcb.Parity = NOPARITY;
	dcb.StopBits = ONESTOPBIT;
	dcb.fOutxDsrFlow = false;
	dcb.fDtrControl = false;
	dcb.fDsrSensitivity = false;

	if (Handshaking)
	{
		dcb.fOutxCtsFlow = true;
		dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
	}
	else
	{
		dcb.fOutxCtsFlow = false;
		dcb.fRtsControl = RTS_CONTROL_DISABLE;
	}

	// set this state
	fSuccess = SetCommState(hCom, &dcb);
	if (!fSuccess)
	{
		/* Handle the error. */
		return(NULL);
	}

	// set a timeout value
	// Timeout = (MULTIPLIER * number_of_bytes) + CONSTANT
	CommTimeouts.ReadIntervalTimeout = ReadTimeout;  //0;//ReadTimeout;
	CommTimeouts.ReadTotalTimeoutMultiplier = 1;
	CommTimeouts.ReadTotalTimeoutConstant = ReadTimeout; //0;//ReadTimeout;
	CommTimeouts.WriteTotalTimeoutMultiplier = 1; //0;//1;
	CommTimeouts.WriteTotalTimeoutConstant = WriteTimeout; //0;//WriteTimeout;
	SetCommTimeouts(hCom, &CommTimeouts);

	// clear the port
	while (sndDataWaiting(hCom))
		ReadFile(hCom, &ch, 1, &now, NULL);

	// flush the port - again
	PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

	// clear all outstanding errors - again
	ClearCommError(hCom, &errors, &comstat);

	// save the handle
	return(hCom);
}

/*----------------------------------------------------------------------
 * Closes the port
 *----------------------------------------------------------------------*/
void __fastcall sndClosePort(HANDLE hCom)
{
	if (hCom != NULL)
		if (CloseHandle(hCom))
		{
			//ShowMessage("Closed");
		}
		else
		{
			ShowMessage("NOT Closed");
		} // End If

		//CloseHandle(hCom);
}

/*----------------------------------------------------------------------
 * Flushes the port
 *----------------------------------------------------------------------*/
void __fastcall sndFlushPort(HANDLE hCom)
{
	if (hCom != NULL)
	{
		// flush the port
		PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

		// clear all outstanding errors
		DWORD errors;
		COMSTAT comstat;
		ClearCommError(hCom, &errors, &comstat);

		// clear the port
		char ch;
		unsigned long now;
		while (sndDataWaiting(hCom))
			ReadFile(hCom, &ch, 1, &now, NULL);

		// flush the port - again
		PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

		// clear all outstanding errors - again
		ClearCommError(hCom, &errors, &comstat);
	}
}

/*----------------------------------------------------------------------
 * Returns number of bytes in input queue
 *----------------------------------------------------------------------*/
int __fastcall sndDataWaiting(HANDLE hCom)
{
	if (hCom != NULL)
	{
		DWORD errors;
		COMSTAT comstat;
		ClearCommError(hCom, &errors, &comstat);
		return(comstat.cbInQue);
	}
	return(0);
}

/******************************************************************************
								End of file
******************************************************************************/

