/***************************************************************
Filename:	Auto_Plotter_Test.cpp
Purpose:	2D Efficiency Plotter
Creation:   16 June 2014, Tom Vocke, Joseph Bowkett

Version		Date	 Name	Comments
V1          16/6/14	 TV  	First Release
V2			30/6/14  GC     Sorted Data format etc

**************************************************************/

/*
=================
Doc TEMPLATE
=================


MOA RunSequenceSteps
	 [FormCreate]

[MAIN]
  | 1) [SUB1]
  | 2) [SUB1]
  |      | 1) [sub1]
  |      � end
  � end

|
If []
| [Sub x]
'- Else
  | [Sub x]
End If
|
While ()
| | [Sub x]
| | [Sub x]
| , -[Break]
| |
|<'

Do
| | [Sub x]
| | [Sub x]
| , -[Break]
| |
|<' While ()

	[:xx]  Add Colon to get direct to Function. Ignore colon to go where used

************************************
CODE DOC STARTS HERE
************************************


Registry
[Reg_ReadString]
[Reg_WriteString]

Eff Plot ini File
[SaveEffPlotIniFileData]
[SaveDEFAULTEffPlotIniFileData]
[ReadEffPlotIniFileData]

[StartSeqTest]
  | 1) [ReadIniFile]
  | 2) [SaveIniFileData]
  | 3) tmSeqTest2->Enabled = true;
  � end

  [SaveSeqIniFileData]
  [ReadSeqIniFileData]

[tmSeqTest2]
  | 1) [:RunSequencer3]
  |      | 1)[:RunPlotSequencer]
  |      |	   | [:RunSIMULATION(ColNumber, RowNumber, iRow, SeqRowRunCounter)]
  |		 |	   | [:RunPlotterSequence2(ColNumber, RowNumber, iRow, SeqRowRunCounter)]
  |      |     |     | [:WriteResults2SeqGrid]
  |      |     |     ` end
  |		 |	   | [MovePlotterToPointMAIN(ColNumber, RowNumber, iRow, SeqRowRunCounter)]
  |      |     ` end
  |      ` end
  � end

  [SaveGridData2CSV]


  [GetGridTypeDropDownIndex]


  Misc
  [RunMiscInstrumentTest()]
  [RunFoDCoilTest()]
  [RunMiscInstrumentTest_Ti_LDC]

  if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")


  *****************************************************
  Seq Keywords
  *****************************************************

	[CreateSeqSeqKeywordList] <<<< This has List of KEYWORDS!!
	[CheckSeqKeywords]


	[ShowGridKeywordTitleDataOptions]  MUST CHECK this if new Keywords added

  [SeqSeqKeywordLookup]

  [OverallSeqSeqKeywordCheck]

  [GetSeqGridColNofText]

  [SetSeqData2Form]  Set Seq Grid Form Data

  // Following to edit keywords



  *****************************************************
  Ends Seq Keywords
  *****************************************************

  [WriteResults2SeqGrid]

  [TestTimeRemaining]

  [ReadDaQDataValuesTimer]
  [ReadDaQDataValuesSeq]


  StringGrid1DblClick



  gblTotalPlotStepCount = total Step count

  Max cols set by -> SEQ_COL_MAX

  C++ Code Grid Address C++ (Start at Top Left)
	   Columns ->
	 0 ___ ___ ___
   R  |0,0|0,1|0,2|
   o  |---|---|---|
   w  |1,0|1,1|1,2|
   s  |---|---|---|
	  |2,0|2,1|2,2|
	  `-----------' etc

  Plot Point Address relative to Grid is (Start at bottom left ignore first column)
	   ___ ___ ___
	  |   |2,1|2,2|
	  |---|---|---|
	  |   |1,1|1,2|
   ^  |---|---|---|
   |  |   |0,0|0,2|
   y 0`-----------' etc
	  x ->

  Column = x Direction
  Row = y Direction

StrGridSeqTest->Cells[0][0] = "SEQ GRID";


	  [ClearEff_Sig_Grids]
	  [InitDataResultsGrid]

			SeqGridRowNumber = SeqRowRunCounter;
			SeqGridStepNumber = SeqStepCounter;

	 Measurment RESULTS in sub [GenerateCSVLine2]

	INDICATORS
	shpDAQConnected
	shPlotterConnected

	Note Code uses tappro5.0 package located in Project folder. Master Source is in:-
	"X:\Software\Embarcadero\AddOn\Embarcadero_Plugins"

	Last code edit location mark = //28/7/15

	[FormCreate]

	State Machine
	[tmTestTimer] = STATE MACHINE TIMER

	[InitPlotGrid]

	Rgistry Name = sREG

	[updateEfficiency]


	Tx comms
	[btTXOpenCommsClick]

	[tmInit_SM] = timer for Tx

	[Reset_GUI] Turns on Tx timer

	[tmInit_SMTimer]

	[Qi_ComportRxChar]
	[Comport_Rx_Data]

	[Rx_Thread]

	[WriteToCSV]

	catch (Exception &E)
	ShowMessage("Error in State Machine -> " & E.Message);


	[Transmit_Message_N]

	[Qi_ComportTriggerAvail]
	[ProcessRxMessage]


	************************************
	SIMPLE SERIAL PORT



	************************************

	[PSU_TurnON]

	For Warning messages use following on Control Tab Form
	mainForm->lbControlMessage->Caption = "WARNING: UAB DAQ NOT connected (Click this to Clear)";

	--------------------------------------------
	MOA
	--------------------------------------------
	[StrGridSeqTest]
	[SeqGrid_Init]

	[SaveIniFileData(false)]
	[ReadIniFile()]

	AnsiString ColTitles[20];
	ColTitles[0] = "1_No.";
	ColTitles[1] = "2_Seq Ctrl.";
	ColTitles[2] = "3_Xmm";
	ColTitles[3] = "4_Ymm";
	ColTitles[4] = "5_Zmm";
	ColTitles[5] = "6_Rot 1_0 45 90";
	ColTitles[6] = "7_Rot 2_";
	ColTitles[7] = "8_PSU_VDC";
	ColTitles[8] = "9_PSU_Imax_Amps";
	ColTitles[9] = "10_eload Type_CC or Res";
	ColTitles[10] = "11_eload Value_eg 5";
	ColTitles[11] = "12_OFF Time_mS";
	ColTitles[12] = "13_ON Time_mS";
	ColTitles[13] = "14_Timestamp";
	ColTitles[14] = "15_V in";
	ColTitles[15] = "16_I In";
	ColTitles[16] = "17_V out";
	ColTitles[17] = "18_I out";
	ColTitles[18] = "19_Efficiency";
	ColTitles[19] = "20_";


*/


// ---------------------------------------------------------------------------

 // For Ini files  Place at Top!!
#include <inifiles.hpp>
//#include "IniFile.h"

#include "vcl.h"
#include <stdio.h>
#include <io.h>
#include <setupapi.h>
#pragma hdrstop

#include "mainGUI.h"
#include "Utils.h"
#include "ComportSel.h"
#include "_SerialIO.h"

#include "Lib_Misc.h"

AnsiString testFile_Name;

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "AdPort"
#pragma link "OoMisc"
#pragma resource "*.dfm"
TmainForm *mainForm;

//=====================================================================================

// ***
// GLOBAL VARS
// ***

AnsiString FoDArduinoCmds [50];  // Coil number is 2 x eg coil 1 = 2, coil 2 = 4 etc

AnsiString FoDCoilResult [20][21];  // 20 coils max each has 20 data 1..10 for freq L, 11..20 for r

AnsiString SeqGridKeyword [150][1];
// see also const int SEQ_KEYWORD_MAX = 150;
// Storage for recall of Data
AnsiString TestDataType [60][100][100];
/*
TestData format [Type as below][x (ie col)][(y ie Row)]
0 Eff
1 Sig
2 Vin
3 Iin
4 Pin
5 Vout
6 Iout
7 Pout

Data LCR
8 LCRFreq1
9 LCRFreqVal1
10 LCRFreqVal2

11 LCRFreq2
12 LCRFreqVal1
13 LCRFreqVal2

14 LCRFreq3
15 LCRFreqVal1
16 LCRFreqVal2

17 LCRFreq4
18 LCRFreqVal1
19 LCRFreqVal2

20 LCRFreq5
21 LCRFreqVal1
22 LCRFreqVal2

23 LCRFreq6
24 LCRFreqVal1
25 LCRFreqVal2

26 LCRFreq7
27 LCRFreqVal1
28 LCRFreqVal2

29 LCRFreq8
30 LCRFreqVal1
31 LCRFreqVal2

32 LCRFreq9
33 LCRFreqVal1
34 LCRFreqVal2

35 LCRFreq10
36 LCRFreqVal1
37 LCRFreqVal2

38 TiLDC_L1
39 TiLDC_L2
40 TiLDC_L3
41 TiLDC_L4

42 TiLDC_L1freq
43 TiLDC_L2freq
44 TiLDC_L3freq
45 TiLDC_L4freq


*/

AnsiString MiscLCRFreqVal1Val2Result [10][10][10];
// [Freq][Value 1][Value 2]

AnsiString MiscTiEVM_LDCResult [10];
bool StopTi_EVMTest;
int FailureCount;

bool GUIDisableCheckBox;

int SeqColMax;
int SeqRowMax;

bool SeqTestDone;
bool SeqTestRunning;
int SeqStepCount;

int SeqRepeatCount;
AnsiString RepeatSeqNoStr;

int SeqStepCounter;
int SeqRowRunCounter;

int gblTotalPlotStepCount;

int SeqGridRowNumber;
int SeqGridStepNumber;

int CountDownProgressWait;
int CountDownProgressOnWait;

//TCustomIniFile* SettingsFile;
AnsiString IniFilename;

int SelGridRowNumber;
int SelGridColNumber;
int RetryCount;

bool SelectedGrid;
bool PlotOnSelectedPoint;

static bool OFFSeqGridTimeDone;
static bool ONSeqGridTimeDone;

int	PSU_ComportGWNumber;

//bool PauseState = false;
//bool DAQStartedMeasurement = false;
//bool TestStarted = false;

int TXComPortNo = 0;

bool TxFlgRunning;

bool flgRunGenComport;

char* FSKDepthCmd;

bool Waiting_On_Version = false;

bool TxReScan;
int Re_Scan_Count;
AnsiString strRetryCountSet;


#define MEASUREMENT_TIME 1000



// Update
float WaitTimerTicks;
float WaitTimerTicksMax;
float WaitOffTimerTicks;
float WaitOffTimerTicksMax;


// ***
// ENDS GLOBAL VARS
// ***

//=====================================================================================




// ***
// GLOBAL CONST
// ***

const int SEQ_COL_MAX = 1000;
const int SEQ_KEYWORD_MAX = 150;

// ***
// ENDS GLOBAL CONST
// ***

enum TX_STATES
	{
	TX_BUFF_EMPTY, INQUIRE, INQUIRE_WAIT, SENDING_RESPONSE, SENDING_DATA,
	WAITING_FOR_ACK
	} ;

enum Message
	{
	START, SEQ, HDR, DATA
	} ;

char TxBuffer2[ 400 ];
static char RiBuf[ 400 ];
static unsigned char TxSeqNum = '0' - 1;
static unsigned char NackCount = 0;
static unsigned char RxSeqNum = 'X';
bool Recived_Message = FALSE;
bool Expert_User = false;
enum TX_STATES Tx_State = INQUIRE;
unsigned int Binary_Len;

typedef enum Tri_Coil_State_
{
	TCS_Nothing = 0,TCS_Detected,TCS_NearBy,TCS_Coupled,TCS_Powered,TCS_Failed_Sweep,TCS_Not_Coupled
} Tri_Coil_State_t;



int Qi_Signal_Strength = 0;
bool First_Qi_Coil = true;
int Qi_Coil_Num_1 = 0;
int Qi_Coil_Num_2 = 0;
bool PauseState = false;
bool DAQStartedMeasurement = false;
bool TestStarted = false;
bool SeqRowStarted = false;



//******************************************************
// GW Instek LCR Meter Commands
//******************************************************

AnsiString GWLCR_id = "*idn?";
AnsiString GWLCR_rst = "*rst";
AnsiString GWLCR_hwoptions = "*opt?";
AnsiString GWLCR_runCal_oc = ":cal:oc-trim 4";
AnsiString GWLCR_runCal_sc = ":cal:sc-trim 4";
AnsiString GWLCR_getCalresult = ":cal:res?";
AnsiString GWLCR_meas_test = ":meas:test:";
AnsiString GWLCR_meas_speed = "meas:speed ";
AnsiString GWLCR_meas_lev = ":meas:lev ";
AnsiString GWLCR_meas_freq = ":meas:freq ";
AnsiString GWLCR_meas_cct = ":meas:equ-cct ";
AnsiString GWLCR_meas_func = ":meas:func:";
AnsiString GWLCR_meas_trig = ":meas:trig";
AnsiString GWLCR_trig = ":trig";
AnsiString GWLCR_mult_set = ":multi:set";
AnsiString GWLCR_mult_loadFile = ":multi:load ";
AnsiString GWLCR_mult_save = ":multi:save";

AnsiString GWLCR_mult_test = ":multi:test ";
AnsiString GWLCR_mult_func = ":multi:func ";

AnsiString GWLCR_mult_freq = "multi:freq ";
AnsiString GWLCR_mult_lev = ":multi:lev ";
AnsiString GWLCR_mult_speed = ":multi:speed ";

AnsiString GWLCR_mult_delay = ":multi:delay ";
AnsiString GWLCR_mult_HiLim = ":multi:hi-lim ";
AnsiString GWLCR_mult_LoLim = ":multi:lo-lim ";

AnsiString GWLCR_mult_run = ":multi:run";
AnsiString GWLCR_mult_trig = ":multi:trig";

//			cbMiscInstrumentCmds->Items->Add("---Graph Cmds--");
AnsiString GWLCR_graph = ":graph";
AnsiString GWLCR_graph_func = ":graph:func ";

AnsiString GWLCR_graph_sweep = ":graph:sweep ";
AnsiString GWLCR_graph_startFreq = ":graph:st ";
AnsiString GWLCR_graph_stopFreq = ":graph:sp ";
AnsiString GWLCR_graph_level = ":graph:lev ";
AnsiString GWLCR_graph_speed = ":graph:speed ";
AnsiString GWLCR_graph_step = ":graph:step ";

AnsiString GWLCR_graph_trig = ":graph:trig ";

AnsiString GWLCR_graph_autoFit = ":graph:autofit ";

AnsiString GWLCR_graph_mkFreq = ":graph:mkf ";
AnsiString GWLCR_graph_mkPeak = ":graph:peak ";
AnsiString GWLCR_graph_mkVal = ":graph:mk?";
AnsiString GWLCR_graph_mkValFreq = ":graph:mkf?";


//******************************************************
// Ends GW Instek LCR Meter Commands
//******************************************************


#define STX 0x02
#define ETX 0x03
#define DLE 0x10
#define DCR 0x13


#define MEASUREMENT_TIME 1000

// Qi message headers
#define QI_H_SIG_STRENGTH 0x01
#define QI_H_EOT 0x02
#define QI_H_HOLDOFF 0x06
#define QI_H_C_ERROR 0x03
#define QI_H_REC_POWER 0x04
#define QI_H_CH_STATUS 0x05
#define QI_H_CONFIG 0x51
#define QI_H_ID 0x71
#define QI_H_EXT_ID 0x81
// usual length for each type of message
#define QI_H_SIG_STRENGTH_LEN 3
#define QI_H_EOT_LEN 3
#define QI_H_HOLDOFF_LEN 3
#define QI_H_C_ERROR_LEN 3
#define QI_H_REC_POWER_LEN 3
#define QI_H_CH_STATUS_LEN 3
#define QI_H_CONFIG_LEN 7
#define QI_H_ID_LEN 9
#define QI_H_EXT_ID_LEN 10
//Qi error codes
typedef enum qi_error_code
{
	QI_OK = 0,
	QI_CHECKSUM_MISMATCH,
	QI_PARITY_ERROR,
	QI_INVALID_MESSAGE,
	QI_INVALID_CONTENT,
	QI_MSG_SIZE_MISMATCH,
	QI_MSG_PENDING,
	QI_NO_MSG_PENDING,
	QI_START_POWER_TRANSFER,
	QI_STOP_POWER_TRANSFER,
	QI_USING_EXTENDED_ID,
	QI_USING_NORMAL_ID,
	QI_ERROR
} qi_error_code_t;

// typedef

// stores configuration/ID data associated with messages
typedef struct qiMsg_Conf_t {
	uint8_t configured; // zero if this is an unconfigured packet
	// properties coming from config packet
	uint8_t power_class;
	uint8_t max_power;
	uint8_t proprietary;
	uint8_t optional_packets_count;
	uint8_t window_size;
	uint8_t window_offset;
	// properties coming from ID packet
	uint8_t major_version;
	uint8_t minor_version;
	uint16_t manufacturer_code;
	uint32_t basic_device_id;
	uint8_t extended_id_present;
	// properties coming from extended ID packet
	uint64_t extended_id;
} qiMsg_Conf_t;

#define QI_MAX_DATA 10 //maximum data bytes in a message
typedef struct qiMsg_t {
	uint8_t header; // header byte
	uint8_t data[QI_MAX_DATA]; // data bytes
	uint8_t num_bytes;
	// total amount of valid bytes (including header and checksum)
	uint8_t checksum; // checksum byte

	TDateTime received_time; // when was this packet received?

	qiMsg_Conf_t config;
	// with what configuration does this message correspond?

	// this will store the checksum evaluation result
	uint8_t checksum_ok;
	// this will store whether anything else is wrong with the message
	uint8_t errors;
	uint8_t errorcode;
	uint8_t checked_for_errors; // has it been checked yet?
	uint16_t transitions[50];
	uint8_t num_trans;
	AnsiString statusString; //qi status messages from RX
	uint8_t isStatus;
} qiMsg_t;

bool Received_Message = false;


//for state machine
typedef enum Tx_Init_State_ {
	Tx_Init_Idle = 0,
	Tx_Init_Version ,
	Tx_Init_Version_Wait ,
	Tx_Init_Manual_Mode ,
	Tx_Init_Manual_Mode_Wait ,
	Tx_Init_Q_Message_Enable ,
	Tx_Init_Q_Message_Enable_Wait ,
	Tx_Init_Start_Scan_Power ,           //28/7/15
	Tx_Init_Start_Scan_Power_Wait ,     //28/7/15
	Tx_Init_ModDepth_Mode ,
	Tx_Init_CheckTx ,
} Tx_Init_State_t;

static Tx_Init_State_t Tx_Init_State = Tx_Init_Idle; //state variable


//Q MESSAGE SETTINGS DEFINITIONS
#define Q_MESSAGE_CONFIG_VERSION (0x01)
//Settings stored by BM_One
const uint32_t Q_Message_BM_All_Detection           = (0x00000001 << 0);
const uint32_t Q_Message_BM_Buck_Boost_Voltage      = (0x00000001 << 1);
const uint32_t Q_Message_BM_Charging_Current        = (0x00000001 << 2);
const uint32_t Q_Message_BM_Loop_Detection          = (0x00000001 << 3);
//const uint32_t Q_Message_BM_Error_State             = (0x00000001 << 4);    //Do not use
const uint32_t Q_Message_BM_Control_Error           = (0x00000001 << 5);
const uint32_t Q_Message_BM_Decoded_Qi_Messages     = (0x00000001 << 6);
const uint32_t Q_Message_BM_FOD_Calibration_Status  = (0x00000001 << 7);
const uint32_t Q_Message_BM_FOD_Reading             = (0x00000001 << 8);
const uint32_t Q_Message_BM_FOD_Config              = (0x00000001 << 9);
const uint32_t Q_Message_BM_Rx_Bicoil_List          = (0x00000001 << 10);
//const uint32_t Q_Message_BM_FOD_Reading             = (0x00000001 << 9);
//const uint32_t Q_Message_BM_FOD_Reading             = (0x00000001 << 10);
//const uint32_t Q_Message_BM_FOD_Reading             = (0x00000001 << 11);
//const uint32_t Q_Message_BM_FOD_Reading             = (0x00000001 << 12);
//const uint32_t Q_Message_BM_F                   = 0x00000020;
//const uint32_t Q_Message_BM_G                   = 0x00000040;
//const uint32_t Q_Message_BM_H                   = 0x00000080;  //TODO give them better names
//      And so on up to 0x00000001<<31
//Settings stored by BM_Two
//const uint32_t Q_Message_BM_T                   = 0x00000001;
//const uint32_t Q_Message_BM_U                   = 0x00000002;
//const uint32_t Q_Message_BM_V                   = 0x00000004;
//const uint32_t Q_Message_BM_W                   = 0x00000008;
//const uint32_t Q_Message_BM_X                   = 0x00000010;
//const uint32_t Q_Message_BM_Y                   = 0x00000020;
//const uint32_t Q_Message_BM_Z                   = 0x00000040;
//      And so on up to 0x00000001<<31

bool Waiting_On_Manual_Mode = false;
bool Waiting_On_Q_Message_Enable = false;
bool Waiting_On_Scan_n_Power = false;       //28/7/15


/******************** COMMENT OUT **************************
// #define EVENT_DRIVEN

class Rx_Thread : public TThread
	{
private:

	HANDLE MyReadHandle;
	int BytesRead;
	uint8_t byteBuff[ MAX_BYTES_TO_PROCESS ];
	int Node;
	OVERLAPPED o;

	void __fastcall SaveData( )
		{
		mainForm->Comport_Rx_Data( BytesRead, byteBuff, Node );
		}

	void __fastcall Execute( void )
		// This is what gets run when the thread starts
		{
		DWORD dwEvtMask;

		while ( !Terminated )
			{

#ifdef EVENT_DRIVEN
			if ( WaitCommEvent( MyReadHandle, &dwEvtMask, &o ) )
				{
				BytesRead = sdReadSerial( MyReadHandle,
					( char * ) &byteBuff[ 0 ], MAX_BYTES_TO_PROCESS );
				if ( BytesRead )
					{
					Synchronize( SaveData );
					}
				}
#else

			if ( sdDataWaiting( MyReadHandle ) )
				{
				BytesRead = sdReadSerial( MyReadHandle,
					( char * ) &byteBuff[ 0 ], MAX_BYTES_TO_PROCESS );
				if ( BytesRead )
					{
					Synchronize( SaveData );
					}
				}
			Sleep( 1 );
#endif

			if ( !Terminated ) // This is optional but normal
				{
				// Synchronize(UpdateTheMainThreadDataOrControls);
				} ;
			} ;
		}
//	;

public:
	/* You can provide any data the thread needs to run properly */
/*
	__fastcall Rx_Thread( HANDLE ReadHandle, int fNode ) :
		TThread( TRUE ) // Start the thread suspended
		{
		// Use the StartupData to initialize class instance data here
		Node = fNode;
		MyReadHandle = ReadHandle;
		FreeOnTerminate = true;

#ifdef EVENT_DRIVEN
		SetCommMask( MyReadHandle, EV_RXCHAR );

		memset( & o, 0, sizeof( o ) );
		o.hEvent = CreateEvent(
			NULL, // default security attributes
			TRUE, // manual-reset event
			FALSE, // not signaled
			NULL // no name
			);

		// Initialize the rest of the OVERLAPPED structure to zero.
		o.Internal = 0;
		o.InternalHigh = 0;
		o.Offset = 0;
		o.OffsetHigh = 0;

		assert( o.hEvent );
#endif

		Resume( ); // Thread now runs
		}
	;

	__fastcall ~Rx_Thread( void )
		{
		// Cleanup, deallocate, etc.
		}
	;

	void __fastcall Terminate( void )
		// This is what you call to terminate the thread; use Suspend to pause it
		{
		TThread::Terminate( );
		// If the main loop is waiting on an event, set the event here
		}
	;
	} ;

#ifdef USB_PSU
Rx_Thread * PSUComPort_Rx_Thread;
#endif

Rx_Thread * PlotterComPort_Rx_Thread;


//Rx_Thread * TXComPort_Rx_Thread;

typedef enum Rx_Thread_Node_ {
	RX_NODE_PSU_COMPORT = 0,
	RX_NODE_PLOTTER_COMPORT ,
	RX_NODE_TX_COMPORT ,
} Rx_Thread_Node_t;

// ---------------------------------------------------------------------------

void __fastcall TmainForm::Comport_Rx_Data( int Count, uint8_t * pData,	int Node )
{

   if (Node == RX_NODE_TX_COMPORT)
	{
	Qi_ComportRxChar(Count,pData);
	}

   if (Node == RX_NODE_PLOTTER_COMPORT)
	{
	if (Plotter != NULL)
		{
		Plotter->ProcessRXData(Count,pData);
		}
	}

 }
************************************************ */

// ---------------------------------------------------------------------------
// Destructor
// ---------------------------------------------------------------------------
//__fastcall TmainForm::~TmainForm(void)
void __fastcall TmainForm::FormDestroy(TObject *Sender)
{
	//disable all timers
	tmMeterUpdate->Enabled = false;
	tmPlotter->Enabled = false;
	tmPlotter_NRT->Enabled = false;
	tmTest->Enabled = false;

	//close PSU comport
#ifdef USB_PSU
	sdFlushPort(PSUComPort);
	sdClosePort(PSUComPort);
#endif
	//close Plotter comport
//	sdFlushPort(PlotterComPort);
//	sdClosePort(PlotterComPort);

	if (Plotter_Comport->Open)
	{
		Plotter_Comport->FlushInBuffer();
		Plotter_Comport->FlushOutBuffer();
		Plotter_Comport->Open = false;
	}
	else
	{
		// Do Nothing
	} // End If

	//todo delete threads
//Plotter_Comport->Open = false;

	//close TX comport if opened
/*	if(TXComPort != NULL){
		sdFlushPort(TXComPort);
		sdClosePort(TXComPort);
		//delete TXComPort_Rx_Thread;
	}
*/
	if(Qi_Comport->Open)
	{
		Qi_Comport->FlushInBuffer();
		Qi_Comport->FlushOutBuffer();
		Qi_Comport->Open = false;
	}
	else
	{
		// Do Nothing
	}  // End If

// Close Any 3D Plotter PSU Port
		if( PSU_Comport->Open )
		{
			PSU_Comport->FlushInBuffer();
			PSU_Comport->FlushOutBuffer();
			PSU_Comport->Open = false;

		}
		else
		{
			// Do Nothing
		} // End If

	//close DAQ interface
	btCloseUSBClick(this);

	//kill all threads
	StopUSBMM();
	//USBMM->Stop();

	//detele member objects
	//DeleteUSBMM();
	//delete USBMM;

	delete Plotter;
}

// ---------------------------------------------------------------------------
// Constructor
// ---------------------------------------------------------------------------
__fastcall TmainForm::TmainForm(TComponent* Owner) : TForm(Owner)
{
		  // Nothing Here
}

// ---------------------------------------------------------------------------
// Create Form
// ---------------------------------------------------------------------------
void __fastcall TmainForm::FormCreate(TObject *Sender)
{
	int index;


	GUIDisableCheckBox = false;

	Plotter_Number = 0;

	Big_View->Checked = true;

	SetFormSize();

//	if ( ParamCount > 0)
//		{
		for ( index = 1; index <= ParamCount(); index++ )
			{
			try
				{
				Plotter_Number = StrToInt( ParamStr( index ));
				}
			catch ( ... )
				{
				}
			}
//		}
//	 else
//		{
//		ShowMessage("Plesae pass the Plotter number as a parameter");
//		Application->ShowMainForm = false;
//		Application->Terminate( );
//		}

	if (!Plotter_Number)
	{
		ShowMessage("Please pass the Plotter number as a parameter");
//		Application->ShowMainForm = false;
//		Application->Terminate( );
	}


	//***
	// Registry Name
	// ***
	//	sREG = ("Software\\PbP\\FODA_Plotter_" + IntToStr((int)Plotter_Number) );
	sREG = ("Software\\PbP\\" + edRegistryName->Text + "_" + IntToStr((int)Plotter_Number) );

	//***
	// Ends Registry Name
	// ***



	//disable all timers
	tmMeterUpdate->Enabled = false;
	tmPlotter->Enabled = false;
	tmTest->Enabled = false;

	measurementRunning = false;

//	DAQStartedMeasurement = false;

	//initialize member variables
	testAdmin.testState = TEST_IDLE_STATE;

	//Initialise PSU Control State
	cbEnablePSUControl->Checked = true;

	// Init Tx Manual Mode
	// cbManual_Mode->Checked = true;

	Init_cbDAQTypes();
	Init_cbPlotAreaOptions();

   Init_cbPSUOptions();
   Init_cbeLoadOptions();

   Init_cbFoDArduinoCodes();
   Init_cbFoDArduinoCommandsOptions();

/*
#ifdef USE_NI6009
	USBMM = new NI6009_USB_Interface(memoUSBLog);
#endif

#ifdef USE_NI6210
	USBMM = new NI6210_USB_Interface(memoUSBLog);
	sDQA_Address_Name = Reg_ReadString(sREG, "DAQ", "NI6210", "dev1");
	USBMM->SetDQA_Address_Name(sDQA_Address_Name);

#endif

#ifdef USE_AGILENT

	USBMM1 = new AGILENT_Interface(memoUSBLog);

#endif

#ifdef USE_DMM
	USBMM = new USBMM_Controller(memoUSBLog);
#endif

#ifdef USE_MOCK_DMM
	USBMM = new MOCK_Interface(memoUSBLog);
#endif


*/




	//acquire the required comports
#ifdef USB_PSU
	PSUComPort = UserSelectComport("Select PSU Com-Port for Plotter " + IntToStr((int)Plotter_Number), "PortPSU", 9600);
	if( PSUComPort == NULL) {
		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
	} else {
		sdFlushPort(PSUComPort);
		PSUComPort_Rx_Thread= new Rx_Thread( PSUComPort, RX_NODE_PSU_COMPORT );
	}
#endif


//27/8/15	UserSelectComport(Plotter_Comport,"Select Plotter Com-Port for Plotter " + IntToStr((int)Plotter_Number) , "PortPlotter", 57600);

//Serial Port At Open program Option
//	PlotterComPort = UserSelectComport("Select Plotter Com-Port for Plotter " + IntToStr((int)Plotter_Number) , "PortPlotter", 57600);

	// if com-port could not be opened close application
//	if(  PlotterComPort == NULL) {
//		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
//	} else {
//		sdFlushPort(PlotterComPort);
//		PlotterComPort_Rx_Thread = new Rx_Thread( PlotterComPort, RX_NODE_PLOTTER_COMPORT );
//	}


	//initialize plotter interface
//	Plotter = new XYPlotter(Plotter_Comport, memoPlotterLog,tmPlotter_NRT);

	mainForm->Caption = mainForm->Caption + " - CONNECTED to Plotter No. " + IntToStr((int)Plotter_Number)  ;


	//start plotter timer
//	tmPlotter->Enabled = true;

	//-----------------------
	//get register defaults
	//-----------------------
	ebOffTime2->Text = Reg_ReadString(sREG, "Power", "Off_Time", "100");
	ebPowerUpWait2->Text = Reg_ReadString(sREG, "Power", "On_Time", "12000");
	ebXSteps2->Text = Reg_ReadString(sREG, "Cords", "X_Steps", "20");
	ebStepSize2->Text = Reg_ReadString(sREG, "Cords", "Step_Size", "5");
	ebYSteps2->Text = Reg_ReadString(sREG, "Cords", "Y_Steps", "20");
	ebXOffset2->Text = Reg_ReadString(sREG, "Cords", "X_Offset", "0");
	ebYOffset2->Text = Reg_ReadString(sREG, "Cords", "Y_Offset", "0");

	ebXSetpointStart2->Text = Reg_ReadString(sREG, "Cords", "X_SetPoint", "0");
	ebYSetpointStart2->Text = Reg_ReadString(sREG, "Cords", "Y_SetPoint", "0");
	ed3DZHeight2->Text = Reg_ReadString(sREG, "Cords", "Z_SetPoint", "280");

	AnsiString defaultFilename = "EVK2_Tx#x_vx_x_x_19V_EVK2_Rx#x_vx_x_x_3mm_VERT_5W_resLoad_12sec_5mmStep";
	ebFilename->Text = Reg_ReadString(sREG, "Directories", "TestFileName", defaultFilename);
	ebFolderName->Text = Reg_ReadString(sREG, "Directories", "TestFolderName", "C:\\temp\\");

	//------
	// DAQ Cal
	//------
	//Bypass and use ini file as NOT PC specific!!!!!!!

//	switch(MessageDlg("Yes = Read from Registry PC Specific\nNo = Read from 'ini' File same name as Exe file", mtInformation, mbYesNoCancel, 0))
//	 {
//		case mrYes:
//		 {
			ebVinMultiplier->Text = Reg_ReadString(sREG, "Multipliers", "Vin", "1.0");
			ebIinMultiplier->Text =  Reg_ReadString(sREG, "Multipliers", "Iin", "1.0");
			ebVoutMultiplier->Text = Reg_ReadString(sREG, "Multipliers", "Vout", "1.0");
			ebIoutMultiplier->Text = Reg_ReadString(sREG, "Multipliers", "Iout", "1.0");

			ebVinOffset->Text = Reg_ReadString(sREG, "Offsets", "Vin", "0.0");
			ebIinOffset->Text = Reg_ReadString(sREG, "Offsets", "Iin", "0.0");
			ebVoutOffset->Text = Reg_ReadString(sREG, "Offsets", "Vout", "0.0");
			ebIoutOffset->Text = Reg_ReadString(sREG, "Offsets", "Iout", "0.0");
//			break;
//		 }
//		case mrNo:
//		 {
//			ReadEffPlotIniFileData();
//			break;
//		 }
//		default:
//			break;
//	 }  // End Switch

	//------
	// Ends DAQ Cal
	//------

	// Read Retry
	edRxVoltReScanLimit->Text = Reg_ReadString(sREG, "Retry", "Rx_VDC_Retry", "4.7");
	edSigStrenReScanLimit->Text = Reg_ReadString(sREG, "Retry", "Sig_Stren", "150");
	edRxVoltFinalTestLimit->Text = Reg_ReadString(sREG, "Retry", "Rx_VDC_Final", "4.7");

	// Plotter Load Unload
	edLoadXmm2->Text = Reg_ReadString(sREG, "Load_Unload", "Xmm", "280");
	edLoadYmm2->Text = Reg_ReadString(sREG, "Load_Unload", "Ymm", "120");

//	rgLoad->ItemIndex =  Reg_ReadInteger(sREG, "Load", "Radio_Index",1 );

	//-----------------------
	// Ends get register defaults
	//-----------------------



	//update plotter offset
//	ebXOffsetChange(this);

	InitPlotGrid();

	ShowTabPanel(0);

	SelectedGrid = false;

	StringGrid1->Enabled = true;
	// Title Eff Grid
	StringGrid1->Cells[0][0] = "PLOT";

	// Title Grid2
	StringGrid2->Cells[0][0] = "SIG";

	InitTxAndRxMakeOptions();

	// Init FSK Mod Depth Options
	SetModDepthOptions();

	int intTimeSecs = StrToInt(mainForm->ebPowerUpWait2->Text)/1000;
	AnsiString TimeSecs = IntToStr(intTimeSecs);
//	mainForm->edTestWait->Text = mainForm->ebPowerUpWait->Text;
	mainForm->edTestWait->Text = TimeSecs;

	mainForm->edRxStep->Text = mainForm->ebStepSize2->Text;

	mainForm->lbControlMessage->Caption = "";

	cbPlotterType2->Clear();
	cbPlotterType2->Text = "Select Plotter";
	cbPlotterType2->Items->Add("2D Plotter MakeBlock");
	cbPlotterType2->Items->Add("3D Plotter PbP");
	//cbPlotterType->Items->Add("3D Plotter for MOA");

	cbOrienZRotation2->Clear();
	cbOrienZRotation2->Text = "Orient";
	cbOrienZRotation2->Items->Add("0 deg");
	cbOrienZRotation2->Items->Add("-45 deg");
	cbOrienZRotation2->Items->Add("-90 deg");

	InitGridDataDisplayOption();


	if (FileListBox1->Items->Count == 0)
	{
//		ShowMessage("Blank1");
		UpdateSeqRow();


	   	FileListBox1->Update();
		SaveIniFileData(true);

		FileListBox1->Update();

		//FileListIniRead();
		ReadIniFile();

	}
	else
	{
  //		ShowMessage("Blank");

	} // End If



	cbTxMakeOptions->ItemIndex = 0;
	cbRxMakeOptions->ItemIndex = 0;

}

// ---------------------------------------------------------------------------
void __fastcall TmainForm::InitGridDataDisplayOption(void)
{
/*
Index
0	cbGridTypeDisplay->AddItem("Efficiency", NULL);
1	cbGridTypeDisplay->AddItem("Sig Strength", NULL);
2	cbGridTypeDisplay->AddItem("Vin", NULL);
3	cbGridTypeDisplay->AddItem("Iin", NULL);
4	cbGridTypeDisplay->AddItem("Pin", NULL);
5	cbGridTypeDisplay->AddItem("Vout", NULL);
6	cbGridTypeDisplay->AddItem("Iout", NULL);
7	cbGridTypeDisplay->AddItem("Pout", NULL);

8  LCR Freq 1 Data 1
9  LCR Freq 2 Data 1
10 LCR Freq 3 Data 1
11 LCR Freq 4 Data 1
12 LCR Freq 5 Data 1
13 LCR Freq 6 Data 1
14 LCR Freq 7 Data 1
15 LCR Freq 8 Data 1
16 LCR Freq 9 Data 1
17 LCR Freq 10 Data 1

18  LCR Freq 1 Data 2
19  LCR Freq 2 Data 2
20 LCR Freq 3 Data 2
21 LCR Freq 4 Data 2
22 LCR Freq 5 Data 2
23 LCR Freq 6 Data 2
24 LCR Freq 7 Data 2
25 LCR Freq 8 Data 2
26 LCR Freq 9 Data 2
27 LCR Freq 10 Data 2

*/
	cbGridTypeDisplay->Clear();
	cbGridTypeDisplay->Text = "Grid Data Display";
	cbGridTypeDisplay->AddItem("Efficiency", NULL);
	cbGridTypeDisplay->AddItem("Sig Strength", NULL);
	cbGridTypeDisplay->AddItem("Vin", NULL);
	cbGridTypeDisplay->AddItem("Iin", NULL);
	cbGridTypeDisplay->AddItem("Pin", NULL);
	cbGridTypeDisplay->AddItem("Vout", NULL);
	cbGridTypeDisplay->AddItem("Iout", NULL);
	cbGridTypeDisplay->AddItem("Pout", NULL);

	cbGridTypeDisplay->ItemIndex = 0;


//	ShowMessage(cbMetersConnected->Text);
//	cbMetersConnected->ItemIndex

}

// ---------------------------------------------------------------------------
void __fastcall TmainForm::AppendGridDataDisplayOption(void)
{

	// Clear first
	InitGridDataDisplayOption();

//===============
// Check GW LCR Meter in Title
//===============
	// Check if Misc Port for LCR set
AnsiString test = SeqGridKeyword[36][0];
if (SeqSeqKeywordLookup(SeqGridKeyword[36][0]))
{
		// Found Do Nothing
		//break;
		// *************************************
		// Append Grid Display for any Misc Data
		// *************************************

	// GW LCR
	switch (cbMiscInstrumentType->ItemIndex)
		{
		case 0:  // GW LCR
			{
			//ShowMessage("dff");
			if (shpMISDataConnected->Brush->Color == clLime)
			{
				// Now Append
				for (int i = 1; i < 11; i++)
				{
					cbGridTypeDisplay->AddItem("Freq " + IntToStr(i) + " : Data 1", NULL);
				} // End For

				for (int i = 1; i < 11; i++)
				{
					cbGridTypeDisplay->AddItem("Freq " + IntToStr(i) + " : Data 2", NULL);
				} // End For
			}
			else
			{
				InitGridDataDisplayOption();
				// Do Nothing
			} // End If
			break;
			}
		case 1:  // spare
			{
				// Do Nothing
			break;
			}
		default:
			break;
	} // End Switch

}
else
{
		// Do Nothing
} // End If

//===============
// Check Ti EVM in Title
//===============
if (SeqSeqKeywordLookup(SeqGridKeyword[82][0]))
{
	// Append any other non 8 more items
	if (cbTiEvmLDC_on_Off->Checked)
	{
		// Add L
		for (int i = 1; i < 5; i++)
		{
			cbGridTypeDisplay->AddItem("TiEVM L" + IntToStr(i), NULL);
		}  // End For

		// Add L freq
		for (int i = 1; i < 5; i++)
		{
			cbGridTypeDisplay->AddItem("TiEVM Lfreq" + IntToStr(i), NULL);
		}  // End For

	}
	else
	{
		// Do Nothing
	} // End If

}
else
{
	// Do Nothing
} // End If


}
// ---------------------------------------------------------------------------

void __fastcall TmainForm::logit(AnsiString FileName, AnsiString Data)
{
//bool New_File = true;

 if (cbSaveLog->Checked)
 {
	FileName = "Plotter_" + IntToStr((int)Plotter_Number) + "_" + FileName;

	Data = Data + "\n";

	//if( (_access( FileName.c_str(), 0 )) != -1 )
	//	{
	//	 New_File = false;
	//	}

	  FILE *log = fopen(FileName.c_str(), "at");
	  if(!log)
		{
		log = fopen(FileName.c_str(), "wt");
		}
	  if(!log)
	  {
		printf("can not open logfile.txt for writing.\n");
		return; // bail out if we can't log
	  }


	  fputs(Data.c_str(), log);
	  // Insert whatever you want logged here

	  fclose(log);
 }
 else
 {
	// Do Nothing
 }// End If
}

// ---------------------------------------------------------------------------
// Open a com-port select dialog, to prompt the user to select a com-port
// ---------------------------------------------------------------------------
void __fastcall TmainForm::InitPlotGrid(void)
{

int ColMax;
int RowMax;

	ColMax = StrToInt(ebXSteps2->Text);
	RowMax = StrToInt(ebYSteps2->Text);

	//***
	// Set Grid Limits
	//***
	StringGrid1->ColCount = ColMax + 2;     // Include Title Col
	StringGrid1->RowCount = RowMax + 2;     // Includes Title Row

	StringGrid2->ColCount = ColMax + 2;     // Include Title Col
	StringGrid2->RowCount = RowMax + 2;     // Includes Title Row

	if (cbDoNotMoveXY->Checked)
	{

		if (MessageDlg("Please Enter Option \n Yes = Clear Grid, No = Do Not clear Grid", mtInformation, mbYesNo, 0) == mrYes)
		 {
			//ShowMessage("Yes");
			// ***
			// Clear Grid
			// ***
			for (int iRow = 0; iRow < RowMax + 2; iRow++)
			{
				StringGrid1->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
				StringGrid2->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
			}  // End For
		 }
		 else
		 {
			 // ShowMessage("No");
			 // Do Nothing
		 }

	}
	else
	{
		// ***
		// Clear Grid
		// ***
		for (int iRow = 0; iRow < RowMax + 2; iRow++)
		{
			StringGrid1->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
			StringGrid2->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
		}  // End For

	} // End If


	//***
	// Column Width
	//***
	StringGrid1->DefaultColWidth = 100;
	StringGrid2->DefaultColWidth = 100;
	for (int i = 0; i < ColMax + 2; i++)
	{
		StringGrid1->ColWidths[i]  = 32;
		StringGrid2->ColWidths[i]  = 32;
	} // End for
	//***
	// End Column Width
	//***

	// Number Title Columns
	for (int iCol = 0; iCol < ColMax + 2; iCol++)
	{
		StringGrid1->Cells[iCol + 1][0] = iCol;
		StringGrid2->Cells[iCol + 1][0] = iCol;
	}  // End For

	// Number Row Titles
	for (int iRow = 0; iRow < RowMax + 2; iRow++)
	{
		if (iRow == 0)
		{
			StringGrid1->Cells[0][iRow] = "";
			StringGrid2->Cells[0][iRow] = "";
		}
		else
		{
			StringGrid1->Cells[0][iRow] = (RowMax + 1) - iRow;
			StringGrid2->Cells[0][iRow] = (RowMax + 1) - iRow;
		} // End If

	}  // End For

	//***
	// Set Grid Titles
	//***
	StringGrid1->Cells[0][0] = "PLOT";
	// Title Grid2
	StringGrid2->Cells[0][0] = "SIG";

}

// ---------------------------------------------------------------------------
// Open a com-port select dialog, to prompt the user to select a com-port
// ---------------------------------------------------------------------------
void __fastcall TmainForm::InitDataResultsGrid(void)
{

int ColMax;
int RowMax;

	ColMax = 30;
	RowMax = 2;

	//***
	// Set Grid Limits
	//***
	StringGrid3->ColCount = ColMax + 2;     // Include Title Col
	StringGrid3->RowCount = RowMax + 2;     // Includes Title Row


	if (cbDoNotMoveXY->Checked)
	{

		if (MessageDlg("Please Enter Option \n Yes = Clear Grid, No = Do Not clear Grid", mtInformation, mbYesNo, 0) == mrYes)
		 {
			//ShowMessage("Yes");
			// ***
			// Clear Grid
			// ***
			for (int iRow = 0; iRow < RowMax + 2; iRow++)
			{
				StringGrid3->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
			}  // End For
		 }
		 else
		 {
			 // ShowMessage("No");
			 // Do Nothing
		 }

	}
	else
	{
		// ***
		// Clear Grid
		// ***
		for (int iRow = 0; iRow < RowMax + 2; iRow++)
		{
			StringGrid3->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];

		}  // End For

	} // End If


	//***
	// Column Width
	//***
	StringGrid3->DefaultColWidth = 100;

	for (int i = 0; i < ColMax + 2; i++)
	{
		StringGrid3->ColWidths[i]  = 32;

	} // End for
	//***
	// End Column Width
	//***


	// Number Title Columns
	for (int iCol = 0; iCol < ColMax + 2; iCol++)
	{
		StringGrid3->Cells[iCol + 1][0] = iCol;
	}  // End For

	// Number Row Titles
	for (int iRow = 0; iRow < RowMax + 2; iRow++)
	{
		if (iRow == 0)
		{
			StringGrid3->Cells[0][iRow] = "";
		}
		else
		{
			StringGrid3->Cells[0][iRow] = (RowMax + 1) - iRow;

		} // End If

	}  // End For

	//***
	// Set Grid Titles
	//***
	// Results Grid  Also in Sub [RunPlotterSequence2]
	StringGrid3->Cells[0][0] = "DATA";
	StringGrid3->Cells[1][0] = "Step No";
	StringGrid3->Cells[2][0] = "X Step";
	StringGrid3->Cells[3][0] = "Y Step";
	StringGrid3->Cells[4][0] = "Z Height";
	StringGrid3->Cells[5][0] = "Rot";
	StringGrid3->Cells[6][0] = "Ang";
	StringGrid3->Cells[7][0] = "Vin";
	StringGrid3->Cells[8][0] = "Iin";
	StringGrid3->Cells[9][0] = "Pin";
	StringGrid3->Cells[10][0] = "Vout";
	StringGrid3->Cells[11][0] = "Iout";
	StringGrid3->Cells[12][0] = "Pout";
	StringGrid3->Cells[13][0] = "Eff";
	StringGrid3->Cells[14][0] = "Qi";
	StringGrid3->Cells[15][0] = "Coil1";
	StringGrid3->Cells[16][0] = "Coil2";
/*	StringGrid3->Cells[17][0] = "";
	StringGrid3->Cells[18][0] = "";
	StringGrid3->Cells[19][0] = "";
	StringGrid3->Cells[20][0] = "";
	StringGrid3->Cells[21][0] = "";
	StringGrid3->Cells[22][0] = "";
	StringGrid3->Cells[23][0] = "";
	StringGrid3->Cells[24][0] = "";
	StringGrid3->Cells[25][0] = "";
	StringGrid3->Cells[26][0] = "";
	StringGrid3->Cells[27][0] = "";
	StringGrid3->Cells[28][0] = "";
	StringGrid3->Cells[29][0] = "";
*/

}


// ---------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------
void __fastcall TmainForm::InitTxAndRxMakeOptions()
{
	mainForm->cbTxMakeOptions->Clear();
	mainForm->cbTxMakeOptions->Items->Add("HB");
	mainForm->cbTxMakeOptions->Items->Add("EVK2");
	mainForm->cbTxMakeOptions->Items->Add("SAM");
	mainForm->cbTxMakeOptions->Items->Add("TI1c");
	mainForm->cbTxMakeOptions->Items->Add("TI3c");
	mainForm->cbTxMakeOptions->Items->Add("NOK");

	mainForm->cbRxMakeOptions->Clear();
	mainForm->cbRxMakeOptions->Items->Add("HB");
	mainForm->cbRxMakeOptions->Items->Add("EVK2");
	mainForm->cbRxMakeOptions->Items->Add("TI");
	mainForm->cbRxMakeOptions->Items->Add("AV1x");
	mainForm->cbRxMakeOptions->Items->Add("SAM");


	mainForm->cbRxOrientation->Clear();
	mainForm->cbRxOrientation->Items->Add("VER");
	mainForm->cbRxOrientation->Items->Add("HOR");
	mainForm->cbRxOrientation->Items->Add("45plus");
	mainForm->cbRxOrientation->Items->Add("45min");

	mainForm->cbRxZHeight->Clear();
	mainForm->cbRxZHeight->Items->Add("3");
	mainForm->cbRxZHeight->Items->Add("5");
	mainForm->cbRxZHeight->Items->Add("6");

}


// ---------------------------------------------------------------------------
// Open a com-port select dialog, to prompt the user to select a com-port
// ---------------------------------------------------------------------------
//28/7/15
//HANDLE __fastcall TmainForm::UserSelectComport(AnsiString dialogLabel, AnsiString regGroup, uint32_t baudrate)
void __fastcall TmainForm::UserSelectComport(TApdComPort *pComport, AnsiString dialogLabel, AnsiString regGroup, uint32_t baudrate)
{

char BaudRateStr[10];

//	HANDLE comHandle = NULL;

	// create user dialog asking for com-port selection
	TDeviceSelectionForm * SelDialog;
	SelDialog = new TDeviceSelectionForm(this, dialogLabel);

	// set default selection
	SelDialog->ComNumber = Reg_ReadInteger(sREG, regGroup, "Comm Number", 0);
		sprintf( BaudRateStr, "%I32u", baudrate );
		AnsiString BaudRate = BaudRateStr;
		SelDialog->edBaudRateSet->Text = BaudRate;

	// if selection is complete, store result and test validity
	if (SelDialog->ShowModal() == mrOk)
	{

		pComport->ComNumber = SelDialog->ComNumber;
		pComport->Baud = baudrate;
//		TXComPortNo = pComport->ComNumber;

		try
		{
			pComport->Open = true;
			Reg_WriteInteger(sREG, regGroup, "Comm Number", SelDialog->ComNumber);

			//shpTxConnected->Brush->Color = clLime;
			//edTxComportNumber->Text = IntToStr(TXComPortNo);

		}
		catch(...)
		{
		MessageDlg(	"Error Opening ComPort " + String(SelDialog->ComNumber) + " @ " + String(baudrate) +
						" -> Error : " + String(GetLastError()), mtError, TMsgDlgButtons() << mbOK, 0);

			//edTxComportNumber->Text = "Error Opening -> " + IntToStr(TXComPortNo);
			//shpTxConnected->Brush->Color = clSilver;
		}  // End Try

/* ************************************************
	//open comport with given baudrate, selected comport nr, and no handshaking
		comHandle =  sdOpenSerialPort(SelDialog->ComNumber, (int)baudrate, 0, 10, 10);
		if(comHandle != NULL)
		{
			Reg_WriteInteger(sREG, regGroup, "Comm Number", SelDialog->ComNumber);
		} else {
			// if com-port could not be openen close application
			MessageDlg(	"Error Opening ComPort " + String(SelDialog->ComNumber) + " @ " + String(baudrate) +
						" -> Error : " + String(GetLastError()), mtError, TMsgDlgButtons() << mbOK, 0);
//			delete SelDialog;
//			Application->ShowMainForm = false;
//			Application->Terminate();
			return NULL;
		}

************************************************ */
	}
	else
	{
		// if cancel was pressed, do no open application
//		delete SelDialog;
//		Application->ShowMainForm = false;
//		Application->Terminate();

//		shpTxConnected->Brush->Color = clSilver;

	  //	return NULL;
	} // End If

	//clear form and return result
	delete SelDialog;
//	return comHandle;
}


// ---------------------------------------------------------------------------
// Open a com-port select dialog, to prompt the user to select a com-port
// ---------------------------------------------------------------------------
//28/7/15
int __fastcall TmainForm::UserSelectComport2(AnsiString dialogLabel, AnsiString regGroup, uint32_t baudrate)
//void __fastcall TmainForm::UserSelectComport(TApdComPort *pComport,AnsiString dialogLabel, AnsiString regGroup, uint32_t baudrate)

{

char BaudRateStr[10];

 //	HANDLE comHandle = NULL;


//	// Open Comport Selection if non Selecte
//	if (edTxComportNumber->Text == "0")
//	{
		// create user dialog asking for com-port selection
		TDeviceSelectionForm * SelDialog;
		SelDialog = new TDeviceSelectionForm(this, dialogLabel);
		// set default selection
		SelDialog->ComNumber = Reg_ReadInteger(sREG, regGroup, "Comm Number", 0);
		sprintf( BaudRateStr, "%I32u", baudrate );
		AnsiString BaudRate = BaudRateStr;
		SelDialog->edBaudRateSet->Text = BaudRate;
//	}
//	else
//	{
		// Do Nothing
//	} // End If

	// if selection is complete, store result and test validity
	if (SelDialog->ShowModal() == mrOk) {
		//open comport with given baudrate, selected comport nr, and no handshaking

//		comHandle =  sdOpenSerialPort(SelDialog->ComNumber, (int)baudrate, 0, 10, 10);

//		if(comHandle != NULL)
		if(SelDialog->ComNumber != 0)
		{
			Reg_WriteInteger(sREG, regGroup, "Comm Number", SelDialog->ComNumber);
		} else {
			// if com-port could not be openen close application
			MessageDlg(	"Error Opening ComPort " + String(SelDialog->ComNumber) + " @ " + String(baudrate) +
						" -> Error : " + String(GetLastError()), mtError, TMsgDlgButtons() << mbOK, 0);
//			delete SelDialog;
//			Application->ShowMainForm = false;
//			Application->Terminate();
			return 0;
		}
	}
	else {
		// if cancel was pressed, do no open application
//		delete SelDialog;
//		Application->ShowMainForm = false;
//		Application->Terminate();
		return NULL;
	}

//	ShowMessage(IntToStr(SelDialog->ComNumber));

	//clear form and return result
	delete SelDialog;
	return 	SelDialog->ComNumber;
}

// ---------------------------------------------------------------------------
// Enable extern power supply
// ---------------------------------------------------------------------------
void __fastcall TmainForm::btEnablePSUClick(TObject *Sender)
{
//const int SIZE = 1000;
//char c_string[SIZE];

  //	if (cbEVK->Checked)
	//if (cbPSUOptions->ItemIndex == 0)

/*
	if (!cbEVK->Checked)
	{
		PSU_ON();
	}
	else
	{
*/
		if (MessageDlg("Note Double Click on Volts or Amps to Set Value\nYes= Turn ON\nNo= Exit", mtInformation, mbYesNo, 0) == mrYes)
		 {
			//ShowMessage("Yes");
//			PSU_ComportGW->PutString("OUTP:STAT ON");
//			PSU_ComportGW->PutString("\n");
			PSU_TurnON();
		 }
		 else
		 {
			//ShowMessage("No");
			// Do Nothing
		 } // End If

//		 AnsiString SetVoltCmd = "APPLY ";
//		 AnsiString Volts = 19;
//		 AnsiString Volts = StrToInt(edPSUSetVoltage->Text);
//		 AnsiString Current = 3;
//		 AnsiString Current = StrToInt(edPSUAmps->Text);

//		 AnsiString SetVoltage = SetVoltCmd + Volts + "," + Current;
/*
		for (int index = 0; index < SetVoltage.Length(); index++)
		{
			//c_string[index] = SetVoltage[index];
			PSU_ComportGW->PutChar(SetVoltage[index]);
		}
	   //	c_string[SetVoltage.Length()] = '\n'; // add null terminator
		PSU_ComportGW->PutChar('\n');
*/
//if (PSU_ComportGW->Open)
//{
//	ShowMessage("Open");
//}
//		PSU_ComportGW->PutString(SetVoltage);
		//PSU_ComportGW->PutString("APPLY 15,3");
//		PSU_ComportGW->PutString("\n");
//		PSU_ComportGW->PutString("\n");


/*		)  PutChar('A');
		PSU_ComportGW->PutChar('P');
		PSU_ComportGW->PutChar('P');
		PSU_ComportGW->PutChar('L');
		PSU_ComportGW->PutChar('Y');
	PSU_ComportGW->PutChar(' ');
		PSU_ComportGW->PutChar('1');
		PSU_ComportGW->PutChar('1');
//		PSU_ComportGW->PutChar(',');
//		PSU_ComportGW->PutChar('3');
*/
//		PSU_ComportGW->PutChar(DCR);
//		PSU_ComportGW->PutChar(DLE);

//		PSU_ComportGW->PutChar(DCR);
//		PSU_ComportGW->PutChar(DLE);


//		   sdWriteSerial(PSU_ComportGW,"APPLY 9.00,3.00"); // Power on
//		   Sleep(100);
//		   sdWriteSerial(PSU_ComportGW,"\n"); // Power on
//		   Sleep(100);

		//   sdWriteSerial(PSU_ComportGW,"OUTP:STAT ON"); // Power on
		//   sdWriteSerial(PSU_ComportGW,"\n"); // Power on

//		   ShowMessage("Done");

		btEnablePSU->Enabled = false;
		btDisablePSU->Enabled = true;
/*
	} // End If
*/

}

// ---------------------------------------------------------------------------
void __fastcall TmainForm::PSU_TurnON()
{

if (shpPSUConnected->Brush->Color == clSilver)
{
	// Do Nothing
}
else
{
	// OK
	switch (cbPSUOptions->ItemIndex)
	{
		case 0:  // Hantek
			{
			//ShowMessage("Yes");
			PSU_ComportGW->PutString("o1");
			//PSU_ComportGW->PutString("OUTP:STAT ON");
			PSU_ComportGW->PutString("\n");

			shpPSUConnected->Brush->Color = clLime;  // Red

			}
			break;
		case 1:  //
			{

			}
			break;
		case 2:  //
			{

			}
			break;
		default:
			{

			}
			break;
	} // End Switch

} // End If



}

// ---------------------------------------------------------------------------
void __fastcall TmainForm::PSU_TurnOFF()
{

if (shpPSUConnected->Brush->Color == clSilver)
{
	// Do Nothing
}
else
{
	// OK
	switch (cbPSUOptions->ItemIndex)
	{
		case 0:  // Hantek
			{

			//ShowMessage("Yes");
			//PSU_ComportGW->PutString("OUTP:STAT OFF");
			PSU_ComportGW->PutString("o0");
			PSU_ComportGW->PutString("\n");

			shpPSUConnected->Brush->Color = clGreen;


			}
			break;
		case 1:  //
			{

			}
			break;
		case 2:  //
			{

			}
			break;
		default:
			{

			}
			break;
	} // End Switch

} // End If


}

// ---------------------------------------------------------------------------
void __fastcall TmainForm::PSU_ReadDaQ(bool V_else_I)
{

if (shpPSUConnected->Brush->Color == clSilver)
{
	// Do Nothing
}
else
{
	// OK
	switch (cbPSUOptions->ItemIndex)
	{
		case 0:  // Hantek
			{
			//ShowMessage("Yes");

			if (V_else_I)
			{
				edPSUDAQV->Text = "ReadV";
/*				// Make sure only read V
				if (edPSUDAQI->Text == "ReadI")
				{
					edPSUDAQI->Text = "ReadIWaitV";
				}
				else
				{
					// Do Nothing
				} // End If
*/
				PSU_ComportGW->PutString("rv");
				PSU_ComportGW->PutString("\n");
			}
			else
			{
				edPSUDAQI->Text = "ReadI";
/*				// Make sure only read I
				if (edPSUDAQV->Text == "ReadV")
				{
					edPSUDAQV->Text = "ReadVaitI";
				}
				else
				{
					// Do Nothing
				} // End If
*/
				PSU_ComportGW->PutString("ra");
				PSU_ComportGW->PutString("\n");
			} // End If

			//PSU_ComportGW->PutString("OUTP:STAT ON");

			shpPSUConnected->Brush->Color = clLime;  // Red

			}
			break;
		case 1:  //
			{

			}
			break;
		case 2:  //
			{

			}
			break;
		default:
			{

			}
			break;
	} // End Switch

} // End If

}


// ---------------------------------------------------------------------------
void __fastcall TmainForm::PSU_ON()
{

	if (cbPSUOptions->ItemIndex == 0)
	{
//#ifdef USB_PSU

//	sdWriteSerial(PSUComPort,"o1\n"); // Power on
	sdWriteSerial(PSU_ComportGW,"o1\n"); // Power on

	//sdWriteSerial(PSUComPort,"APPLY 1.00,3.00"); // Power on

	}
	else
	{
		//#else
			//send relay command

			if (cbPlotterType2->ItemIndex == 0)
			{
				Plotter->enPowerRelay(true);
			}
			else
			{
				enPowerRelay3D(true);
			} // End If

		//#endif

	} // End If



	lbPSUPower->Caption = "PSU Power : ON";
	TestLogAdd("Power ON");
	//update buttons
	btEnablePSU->Enabled = false;
	btDisablePSU->Enabled = true;


	shpPSUConnected->Brush->Color = clGreen;


  //	cbPowerSupplyStatus->Checked = true;

}

// ---------------------------------------------------------------------------
// Disable external power supply  (store in register)
// ---------------------------------------------------------------------------
void __fastcall TmainForm::btDisablePSUClick(TObject *Sender)
{
   //	if (cbEVK->Checked)
	//if (cbPSUOptions->ItemIndex == 1)
/*
	if (!cbEVK->Checked)
	{
		PSU_OFF();
	}
	else
	{
*/
//		   sdWriteSerial(PSU_ComportGW,"APPLY 19.00,3.00"); // Power on
//		   sdWriteSerial(PSU_ComportGW,"\n"); // Power on

//	   sdWriteSerial(PSU_ComportGW,"OUTP:STAT OFF"); // Power on
//	   sdWriteSerial(PSU_ComportGW,"\n"); // Power on


		if (MessageDlg("Note Double Click on Volts or Amps to Set Value\nYes= Turn OFF\nNo= Exit", mtInformation, mbYesNo, 0) == mrYes)
		 {
			//ShowMessage("Yes");
//			PSU_ComportGW->PutString("OUTP:STAT OFF");
//			PSU_ComportGW->PutString("\n");
			PSU_TurnOFF();
		 }
		 else
		 {
			//ShowMessage("No");
			// Do Nothing
		 } // End If

		//update buttons
		btEnablePSU->Enabled = true;
		btDisablePSU->Enabled = false;
/*
	} // End If
*/

}

// ---------------------------------------------------------------------------
void __fastcall TmainForm::PSU_OFF()
{
#ifdef USB_PSU
	sdWriteSerial(PSUComPort,"o0\n"); // Power off
#else
	if (cbPlotterType2->ItemIndex == 0)
	{
		Plotter->enPowerRelay(false);
	}
	else
	{
		enPowerRelay3D(false);
	} // End If

#endif

	lbPSUPower->Caption = "PSU Power : OFF";
	TestLogAdd("Power OFF");
	//update buttons
	btEnablePSU->Enabled = true;
	btDisablePSU->Enabled = false;


	shpPSUConnected->Brush->Color = clGreen;

  //	cbPowerSupplyStatus->Checked = false;

}

// ---------------------------------------------------------------------------
// Update X number of steps  (store in register)
// ---------------------------------------------------------------------------
void __fastcall TmainForm::ebXStepsChange(TObject *Sender)
{

if (ebXSteps2->Text == "-")
{
	// Do Nothing
}
else
{

if (StrToInt(ebXSteps2->Text) < 0 )
{
	// Do Nothing
	ebXSteps2->Text = "0";
}
else
{
   if (ebXSteps2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebXSteps2->Text = "5";
   }
   else
   {

int StepSizemm = StrToInt(ebStepSize2->Text);
	edXStepmm2->Text = IntToStr(StrToInt(ebXSteps2->Text) * StepSizemm);

	Reg_WriteString(sREG, "Cords", "X_Steps", ebXSteps2->Text);

	TestLogAdd("X Steps : " + ebXSteps2->Text);

	InitPlotGrid();
	} //End If

}

}  // End If


}

// ---------------------------------------------------------------------------
// Update U number of steps  (store in register)
// ---------------------------------------------------------------------------
void __fastcall TmainForm::ebYStepsChange(TObject *Sender)
{

if (ebYSteps2->Text == "-")
{
	// Do Nothing
}
else
{

if (StrToInt(ebYSteps2->Text) < 0 )
{
	// Do Nothing
	ebYSteps2->Text = "0";
}
else
{

   if (ebYSteps2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebYSteps2->Text = "5";
   }
   else
   {
	int StepSizemm = StrToInt(ebStepSize2->Text);
	edYStepmm2->Text = IntToStr(StrToInt(ebYSteps2->Text) * StepSizemm);

	Reg_WriteString(sREG, "Cords", "Y_Steps", ebYSteps2->Text);

	TestLogAdd("Y Steps : " + ebYSteps2->Text);

	InitPlotGrid();
   } // End If

} // End If

} // End If

}

// ---------------------------------------------------------------------------
// Update step-size per step (store in register)
// ---------------------------------------------------------------------------
void __fastcall TmainForm::ebStepSizeChange(TObject *Sender)
{
   if (ebStepSize2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebStepSize2->Text = "5";
   }
   else
   {
		int StepSizemm = StrToInt(ebStepSize2->Text);
		edXStepmm2->Text = IntToStr(StrToInt(ebXSteps2->Text) * StepSizemm);
		edYStepmm2->Text = IntToStr(StrToInt(ebYSteps2->Text) * StepSizemm);

		Reg_WriteString(sREG, "Cords", "Step_Size", ebStepSize2->Text);

		TestLogAdd("Step Size : " + ebStepSize2->Text);
		mainForm->edRxStep->Text = mainForm->ebStepSize2->Text;

   } // End If
}

//---------------------------------------------------------------------------
// Update file to save too, and foldername registry entry
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebFolderNameChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Directories", "TestFolderName", ebFolderName->Text);
}

//---------------------------------------------------------------------------
// Update file to save too, and filename registry entry
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebFilenameChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Directories", "TestFileName", ebFilename->Text);
}

//---------------------------------------------------------------------------
// Write Power-up wait time to registry
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebPowerUpWaitChange(TObject *Sender)
{
 if (ebPowerUpWait2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebPowerUpWait2->Text = "12000";
   }
   else
   {
	Reg_WriteString(sREG, "Power", "On_Time", ebPowerUpWait2->Text);
	TestLogAdd("New [On Time] : " + ebPowerUpWait2->Text + "ms");
	int intTimeSecs = StrToInt(mainForm->ebPowerUpWait2->Text)/1000;
	AnsiString TimeSecs = IntToStr(intTimeSecs);
//	mainForm->edTestWait->Text = mainForm->ebPowerUpWait->Text;
	mainForm->edTestWait->Text = TimeSecs;

   }  // End If}
}

//---------------------------------------------------------------------------
// Write Power-off wait time to registry
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebOffTimeChange(TObject *Sender)
{

	Reg_WriteString(sREG, "Power", "Off_Time", ebOffTime2->Text);
	TestLogAdd("New [Off Time] : " + ebOffTime2->Text + "ms");
}

// ---------------------------------------------------------------------------
// Update plotter setpoint to 0,0 (home)
// ---------------------------------------------------------------------------
void __fastcall TmainForm::btHomePlotterClick(TObject *Sender)
{
	InitAndHomePlotter();

	//shPlotterConnected->Brush->Color = clLime;

}

// ---------------------------------------------------------------------------
void __fastcall TmainForm::InitAndHomePlotter()
{
//	ebXSetpointStart->Text = "0";
//	ebYSetpointStart->Text = "0";
	// Set metric
	Plotter->SendCommand("G21");
	Sleep(100);
	// Set abs co-ords
	Plotter->SendCommand("G90");
	Sleep(100);
	Plotter->Home();

//	Plotter->SendCommand("G28");

	shPlotterConnected->Brush->Color = clLime;

	ShowMessage("NOTE: Allow Time for Plotter to respond");

	if (lbControlMessage->Caption == "Plotter Runing in Simulation\nThis is set in Maintenance Tab for Demo")
	{
		lbControlMessage->Caption = "";
	}
	else
	{
		// Do Nothing
	} // End If

}


// ---------------------------------------------------------------------------
// Update plotter setpoint to user input X,Y
// ---------------------------------------------------------------------------
void __fastcall TmainForm::btGotoXYClick(TObject *Sender)
{

	int ColSteps = 1 + StrToInt(ebXOffset2->Text) / StrToInt(ebStepSize2->Text);
	int RowSteps = 1 + StrToInt(ebYOffset2->Text) / StrToInt(ebStepSize2->Text);

	MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(ed3DZHeight2->Text), 1);
/*
	XYPlotter_setpoint_t tmpSetpoint;
//	tmpSetpoint.XPos = StrToInt(ebXSetpointStart->Text);
//	tmpSetpoint.YPos = StrToInt(ebYSetpointStart->Text);
//	tmpSetpoint.XPos = StrToInt(ebXOffset->Text) + StrToInt(ebXSetpoint->Text);
//	tmpSetpoint.YPos = StrToInt(ebYOffset->Text) + StrToInt(ebYSetpoint->Text);
	tmpSetpoint.XPos = StrToInt(ebXSetpointStart->Text) + StrToInt(ebXOffset->Text);
	tmpSetpoint.YPos = StrToInt(ebYSetpointStart->Text) + StrToInt(ebYOffset->Text);

//	tmpSetpoint.XPos = StrToInt(ebXOffset->Text);
// 	tmpSetpoint.YPos = StrToInt(ebYOffset->Text);

	//update plotter setpoint
	Plotter->UpdateSetpoint(tmpSetpoint);
	//Update user labels
	updatePosLabels(this);

	Plotter->Set3dZHeight(StrToFloat(ed3DZHeight->Text));
*/

}

// ---------------------------------------------------------------------------
// Update labels that indicate position and distance
// ---------------------------------------------------------------------------
void __fastcall TmainForm::updatePosLabels(TObject *Sender)
{
	//get Plotter position and setpoint
	XYPlotter_setpoint_t curSetPoint = Plotter->GetSetpoint();
	XYPlotter_setpoint_t curDistance = Plotter->GetCurDistance();
	//update labels
	lbCords->Caption = "X: " + String(curSetPoint.XPos) + " Y: " + String(curSetPoint.YPos);
	lbDistance->Caption = "X: " + String(curDistance.XPos) + " Y: " + String(curDistance.YPos);
}

//---------------------------------------------------------------------------
// Calls the USBMM object and tells it to connect all devices
//---------------------------------------------------------------------------
void __fastcall TmainForm::btOpenUSBClick(TObject *Sender)
{

//USBMM->DMM_Task();
//return;

//Start USB Threads
StartUSBMM();
//USBMM->Start();


// Ensure this is Set as Var is used in timer below
strRetryCountSet = ebHoleRetryNo->Text;
tmMeterUpdate->Enabled = true;

//enable appropriate buttons
btOpenUSB->Enabled = false;
btCloseUSB->Enabled = true;

//todo remove this
//USBMM->StartNewMeasurement();

}

//---------------------------------------------------------------------------
// Calls the USBMM object and tells it to disconnect all devices
//---------------------------------------------------------------------------
void __fastcall TmainForm::btCloseUSBClick(TObject *Sender)
{
//	TestLogAdd("STOP USB Button PRESSED");

//if (TestStarted)
//	{
//		//Do nothing
//	}
//	else
//	{
	 //	ShowMessage("Stop Pressed");
//		USBMM->RESETManualControlFlag();

		 //Start USB Threads
	   	StopUSBMM();
		//USBMM->Stop();
		tmMeterUpdate->Enabled = false;

		measurementRunning = false;
		//enable appropriate buttons
		btOpenUSB->Enabled = true;
		btCloseUSB->Enabled = false;
		//update indicators
		shInputVoltage->Brush->Color = clSilver;
		shInputCurrent->Brush->Color = clSilver;
		shOutputVoltage->Brush->Color = clSilver;
		shOutputCurrent->Brush->Color = clSilver;

//	} //End If

}


//---------------------------------------------------------------------------
// Start the test
//---------------------------------------------------------------------------
void __fastcall TmainForm::btStartTestClick(TObject *Sender)
{

	// Send FSK Mod Depth
	if (FSKDepthCmd == NULL)
	{
		ShowMessage("WARNING - No FSK Depth Set");
	}
	else
	{
		// Do Nothing
	} // End If

	if (lblRxLoadName->Caption == "?W")
	{
		ShowMessage("WARNING: Check Rx Load Setting -> ??W");
	}
	else
	{
		// Do Nothing
	} // End If

	// Check if Comport Opened
	if (!btnPlotComportOpen2->Enabled)
	{
		if (cbDoNotMoveXY->Checked)
		{
		   StringGrid1->Enabled = false;
		}
		else
		{
			// Do Nothing
		   StringGrid1->Cells[0][0] = "PLOT";
		   StringGrid1->Enabled = true;
		   StringGrid1->Visible = true;
		   StringGrid2->Visible = false;

		}  // End If

		TestStarted = true;

		cbPowerSupplyStatus->Enabled = false;

		InitPlotGrid();

		//indicate start in log
		TestLogAdd("Starting Test...");

		//set state to init state
		testAdmin.testState = TEST_INIT_STATE;

		//update buttons
		btStartTest->Enabled = false;
		rgLoad->Enabled = false;
		btStopTest->Enabled = true;
		btnPause->Enabled = true;

		//set the load
		// turn off all load relays

	if (cbPlotterType2->ItemIndex == 0)
	{
		Plotter->enLoadRelay(false,0);
		Plotter->enLoadRelay(false,1);
		//Plotter->enLoadRelay(false,2);

		Plotter->enLoadRelay(true,rgLoad->ItemIndex);
	}
	else
	{
		enLoadRelay3D(false, 0);
		enLoadRelay3D(false, 1);
		//enLoadRelay3D(false, 2);

		enLoadRelay3D(true,rgLoad->ItemIndex);

	} // End If

		//create test file name
		testAdmin.testFile = CreateFileName();

		//set the relay state to measure TX / RX
		if (cbPlotterType2->ItemIndex == 0)
		{
			Plotter->enRefRelay(true);
		}
		else
		{
			enRefRelay3D(true);
		} // End If

		// USBMM->RESETManualControlFlag();

		// ***
		// System TIMERS
		// ***
		//enable test-system timer
		tmTest->Enabled = true;

		//disable automated meter timer
		tmMeterUpdate->Enabled = false;

		//Enable Plotter Timer
		tmPlotter->Enabled = true;

		strRetryCountSet = ebHoleRetryNo->Text;
		mainForm->lbRetryNotice->Caption = "Retry = Ready";

	}
	else
	{
		ShowMessage("Please Check if Plotter Comport Opened in Set Up Sheet");
	} // End If


}


//---------------------------------------------------------------------------
// Stop the test
//---------------------------------------------------------------------------
void __fastcall TmainForm::btStopTestClick(TObject *Sender)
{
	TestStarted = false;

	cbPowerSupplyStatus->Enabled = true;

	SelectedGrid = false;
	StringGrid1->Enabled = true;

	RetryCount = 0;

	//indicate test stop in log
	TestLogAdd("****************");
	TestLogAdd("Test Stopped...");
	TestLogAdd("****************");

	//change to idle state
	//testAdmin.testState = TEST_IDLE_STATE;

	//disable test-system timer
	tmTest->Enabled = false;
	//possibly enable automated meter timer
	tmMeterUpdate->Enabled = false;

	//Kill Plotter Timer
	tmPlotter->Enabled=false;

	//disable power supply
	btDisablePSUClick(this);

	//update buttons
	btStartTest->Enabled = true;

	//rgLoad->Enabled = true;
	rgLoad->Enabled = false;

	btStopTest->Enabled = false;
	btnPause->Enabled = false;

//	Same as btCloseUSB procedure;

   	StopUSBMM();
	//USBMM->Stop();
	tmMeterUpdate->Enabled = false;

	//Set Flag
	measurementRunning = false;

	//enable appropriate buttons
	btOpenUSB->Enabled = true;
	btCloseUSB->Enabled = false;
	//update indicators
	shInputVoltage->Brush->Color = clSilver;
	shInputCurrent->Brush->Color = clSilver;
	shOutputVoltage->Brush->Color = clSilver;
	shOutputCurrent->Brush->Color = clSilver;

//*******************************
	//change to idle state
	testAdmin.testState = TEST_IDLE_STATE;

//	ShowMessage("Stopped Pressed");

//	CloseTxPort();

}

//---------------------------------------------------------------------------
// Update Plotter X offset to home
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebXOffsetChange(TObject *Sender)
{

if (ebXOffset2->Text == "-")
{
	//Ignore - symbol

}
else
{
	// OK
	if (ebXOffset2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebXOffset2->Text = "5";
   }
   else
   {
	Reg_WriteString(sREG, "Cords", "X_Offset", ebXOffset2->Text);
	TestLogAdd("New [X Offset] : " + ebXOffset2->Text);

//	Reg_WriteString(sREG, "Cords", "X_SetPoint", ebXSetpoint->Text);
//	TestLogAdd("New [X SetPoint] : " + ebXSetpoint->Text);

	//update plotter settings
	XYPlotter_setpoint_t tmpOffset;
	tmpOffset.XPos=StrToInt(ebXOffset2->Text);
	tmpOffset.YPos=StrToInt(ebYOffset2->Text);
 //	Plotter->SetOffset(tmpOffset);
   //	Plotter->SetPlotStart(tmpOffset);
   } // End If

} // End If

}

//---------------------------------------------------------------------------
// Update Plotter X offset to home
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebYOffsetChange(TObject *Sender)
{

if (ebYOffset2->Text == "-")
{
	//Ignore - symbol

}
else
{
	// OK
	if (ebYOffset2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebYOffset2->Text = "5";
   }
   else
   {
	Reg_WriteString(sREG, "Cords", "Y_Offset", ebYOffset2->Text);
	TestLogAdd("New [Y Offset] : " + ebYOffset2->Text);

//	Reg_WriteString(sREG, "Cords", "Y_SetPoint", ebYSetpoint->Text);
//	TestLogAdd("New [Y SetPoint] : " + ebYSetpoint->Text);

	//update plotter settings
	XYPlotter_setpoint_t tmpOffset;
	tmpOffset.XPos=StrToInt(ebXOffset2->Text);
	tmpOffset.YPos=StrToInt(ebYOffset2->Text);
	//Plotter->SetPlotStart(tmpOffset);
   } // End If

} // End If

}

//---------------------------------------------------------------------------
// Update Efficiency Calculation
//---------------------------------------------------------------------------
void __fastcall TmainForm::updateEfficiency(void)
{
   // Warning Also update updateEfficiency2

	char inputPowerStr[15];
	char outputPowerStr[15];
	char effStr[15];
	static double nrCalls = 0;
	nrCalls++;

	//calculate latest results
	curMeas.inputPower = curMeas.values[INPUT_VOLTAGE_INDEX] * curMeas.values[INPUT_CURRENT_INDEX];
	curMeas.outputPower = curMeas.values[OUTPUT_VOLTAGE_INDEX] * curMeas.values[OUTPUT_CURRENT_INDEX];
	//capture devision by 0
	curMeas.efficiency = (curMeas.inputPower > 0.0) ? ( curMeas.outputPower * 100.0 / curMeas.inputPower ) : 0.0;

	// Get Retry Count
	if (strRetryCountSet == "")
	{
		strRetryCountSet = "1";
	}
	else
	{
	   // Do Nothing
	} // End If
	curMeas.RetryCount = StrToInt(strRetryCountSet) - Re_Scan_Count;

	//build input power string
	sprintf(&inputPowerStr[0],"%.2f W",curMeas.inputPower);
	//build output power string
	sprintf(&outputPowerStr[0],"%.2f W",curMeas.outputPower);
	//build efficiency string
	sprintf(&effStr[0],"%.2f %",curMeas.efficiency);

	lbEffCalcManual->Caption = &effStr[0];

	//calculate time remaining in the test
	//number of Y lines still to test
	int stepsRemaining = ( StrToInt(ebYSteps2->Text) - testAdmin.ySteps - 1) * StrToInt(ebXSteps2->Text);

	if (stepsRemaining < 0) {
		stepsRemaining = 0;
	}
	//plus number of X points to go in current Y line
	stepsRemaining += StrToInt(ebXSteps2->Text) - testAdmin.xSteps;
	//multiply to find total time in seconds
	int timeRemaining = 2 * stepsRemaining * ( ( MEASUREMENT_TIME + StrToInt(ebPowerUpWait2->Text) + StrToInt(ebOffTime2->Text) ) / 1000 );
	int tempSecs = timeRemaining % 60;
	int tempMins = ( ( timeRemaining - tempSecs ) / 60 ) % 60;
	int tempHours = ( timeRemaining - tempMins * 60 - tempSecs ) / 3600;

	//display results to user
	lbInputPower->Caption = inputPowerStr;
	lbOutputPower->Caption = outputPowerStr;
	lbEff->Caption = effStr;

	if (cbShowProgChart->Checked)
	{
		Series1->Add(curMeas.efficiency);
	}
	else
	{
		// Do Nothing
	} // End If


	lblTimeRemaining->Caption = "Time Remaining for Plot";
	timeRemainlb->Caption = IntToStr(tempHours) + ":" + IntToStr(tempMins) + ":" + IntToStr(tempSecs);

	// ***
	// Update Grid lbEff
	// ***
	// Check Plot Direction
	if (TestStarted )
	{
		if (testAdmin.sign == 1)
		{
			//+ Direction
			StringGrid1->Cells[1 + testAdmin.xSteps][(StrToInt(ebYSteps2->Text) + 1) - testAdmin.ySteps] = effStr;
			StringGrid2->Cells[1 + testAdmin.xSteps][(StrToInt(ebYSteps2->Text) + 1) - testAdmin.ySteps] = Qi_Signal_Strength;
			// colour Cell if > 50%
//			if (curMeas.efficiency > 50.0)
//			{
//			   ColourGridCellGrn(1 + testAdmin.xSteps,(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps);
//			}
//			else
//			{

//			} // End If
	//		StringGrid1->Cells[SelGridColNumber][SelGridRowNumber] = effStr;

		}
		else
		{
			//- Direction
			StringGrid1->Cells[(StrToInt(ebXSteps2->Text) + 2) - (1 + testAdmin.xSteps)][(StrToInt(ebYSteps2->Text) + 1) - testAdmin.ySteps] = effStr;
			StringGrid2->Cells[(StrToInt(ebXSteps2->Text) + 2) - (1 + testAdmin.xSteps)][(StrToInt(ebYSteps2->Text) + 1) - testAdmin.ySteps] = Qi_Signal_Strength;
			// colour Cell if > 50%
//			if (curMeas.efficiency > 50.0)
//			{
//			   ColourGridCellGrn(1 + testAdmin.xSteps,(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps);
//			}
//			else
//			{

//			} // End If

	//		StringGrid1->Cells[SelGridColNumber][SelGridRowNumber] = effStr;
		} // End If


	}
	else
	{
//		SelGridRowNumber = StringGrid1->RowCount - StringGrid1->Row;
		if (SelGridColNumber == 0)
		{
			// Do Nothing
		}
		else
		{
			StringGrid1->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = effStr;
			StringGrid2->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = Qi_Signal_Strength;
			// colour Cell if > 50%
//			if (curMeas.efficiency > 50.0)
//			{
//			   ColourGridCellGrn(1 + testAdmin.xSteps,(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps);
//			}
//			else
//			{

//			} // End If

		} // End If

	} // End If

}

//---------------------------------------------------------------------------
// Update Efficiency Calculation
//---------------------------------------------------------------------------
void __fastcall TmainForm::updateEfficiency2(void)
{
	char inputPowerStr[15];
	char outputPowerStr[15];
	char effStr[15];
	static double nrCalls = 0;
	nrCalls++;

	//calculate latest results
	curMeas.inputPower = curMeas.values[INPUT_VOLTAGE_INDEX] * curMeas.values[INPUT_CURRENT_INDEX];
	curMeas.outputPower = curMeas.values[OUTPUT_VOLTAGE_INDEX] * curMeas.values[OUTPUT_CURRENT_INDEX];
	//capture devision by 0
	curMeas.efficiency = (curMeas.inputPower > 0.0) ? ( curMeas.outputPower * 100.0 / curMeas.inputPower ) : 0.0;

	// Get Retry Count
	// Get Retry Count
	if (strRetryCountSet == "")
	{
		strRetryCountSet = "1";
	}
	else
	{
	   // Do Nothing
	} // End If
	curMeas.RetryCount = StrToInt(strRetryCountSet) - Re_Scan_Count;

	//build input power string
	sprintf(&inputPowerStr[0],"%.2f W",curMeas.inputPower);
	//build output power string
	sprintf(&outputPowerStr[0],"%.2f W",curMeas.outputPower);
	//build efficiency string
	sprintf(&effStr[0],"%.2f %",curMeas.efficiency);
	//calculate time remaining in the test
	//number of Y lines still to test
	int stepsRemaining = ( StrToInt(ebYSteps2->Text) - testAdmin.ySteps - 0) * StrToInt(ebXSteps2->Text);

	if (stepsRemaining < 0) {
		stepsRemaining = 0;
	}
	//plus number of X points to go in current Y line
	stepsRemaining += StrToInt(ebXSteps2->Text) - 0 ; //testAdmin.xSteps;
	//multiply to find total time in seconds
	int timeRemaining = 2 * stepsRemaining * ( ( MEASUREMENT_TIME + StrToInt(ebPowerUpWait2->Text) + StrToInt(ebOffTime2->Text) ) / 1000 );
	int tempSecs = timeRemaining % 60;
	int tempMins = ( ( timeRemaining - tempSecs ) / 60 ) % 60;
	int tempHours = ( timeRemaining - tempMins * 60 - tempSecs ) / 3600;

	//display results to user
	lbInputPower->Caption = inputPowerStr;
	lbOutputPower->Caption = outputPowerStr;
	lbEff->Caption = effStr;

	if (cbShowProgChart->Checked)
	{
		Series1->Add(curMeas.efficiency);
	}
	else
	{
		// Do Nothing
	} // End If

//	timeRemainlb->Caption = IntToStr(tempHours) + ":" + IntToStr(tempMins) + ":" + IntToStr(tempSecs);
	lblTimeRemaining->Caption = "Repeated Point Count";
	RetryCount = RetryCount + 1;
	timeRemainlb->Caption = IntToStr(RetryCount);

	// ***
	// Update Grid lbEff
	// ***
	// Check Plot Direction
	if (TestStarted )
	{
		if (testAdmin.sign == 1)
		{

			//+ Direction
			StringGrid1->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = effStr;
			StringGrid2->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = Qi_Signal_Strength;
	//		StringGrid1->Cells[1 + testAdmin.xSteps][(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps] = effStr;
		}
		else
		{
			//- Direction
			StringGrid1->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = effStr;
			StringGrid2->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = Qi_Signal_Strength;
	//		StringGrid1->Cells[(StrToInt(ebXSteps->Text) + 2) - (1 + testAdmin.xSteps)][(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps] = effStr;
		} // End If
	}
	else
	{
		if (testAdmin.sign == 1)
		{
			//+ Direction
	//		StringGrid1->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = effStr;
			StringGrid1->Cells[1 + testAdmin.xSteps][(StrToInt(ebYSteps2->Text) + 1) - testAdmin.ySteps] = effStr;
			StringGrid2->Cells[1 + testAdmin.xSteps][(StrToInt(ebYSteps2->Text) + 1) - testAdmin.ySteps] = Qi_Signal_Strength;
		}
		else
		{
			//- Direction
	//		StringGrid1->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = effStr;
			StringGrid1->Cells[(StrToInt(ebXSteps2->Text) + 2) - (1 + testAdmin.xSteps)][(StrToInt(ebYSteps2->Text) + 1) - testAdmin.ySteps] = effStr;
			StringGrid2->Cells[(StrToInt(ebXSteps2->Text) + 2) - (1 + testAdmin.xSteps)][(StrToInt(ebYSteps2->Text) + 1) - testAdmin.ySteps] = Qi_Signal_Strength;
		} // End If

	} // End If

//	SelGridRowNumber = StringGrid1->RowCount - StringGrid1->Row;
//	SelGridColNumber = StringGrid1->Col; // StringGrid1->ColCount - StringGrid1->Col;

}

//---------------------------------------------------------------------------
// Update Efficiency Calculation
//---------------------------------------------------------------------------
void __fastcall TmainForm::updateEfficiencySeqGrid(int SeqGridRow)
{
   // Warning Also update updateEfficiency2
   try
   {

	char inputPowerStr[15];
	char outputPowerStr[15];
	char effStr[15];
	static double nrCalls = 0;
	nrCalls++;

	//calculate latest results
	curMeas.inputPower = curMeas.values[INPUT_VOLTAGE_INDEX] * curMeas.values[INPUT_CURRENT_INDEX];
	curMeas.outputPower = curMeas.values[OUTPUT_VOLTAGE_INDEX] * curMeas.values[OUTPUT_CURRENT_INDEX];

	if (curMeas.values[INPUT_VOLTAGE_INDEX] < 0.1)
	{
		//capture Zero Tx voltage
		curMeas.efficiency = 0,0;
	}
	else
	{
		//capture devision by 0
		curMeas.efficiency = (curMeas.inputPower > 0.0) ? ( curMeas.outputPower * 100.0 / curMeas.inputPower ) : 0.0;
	} // end If


	// Get Retry Count
//	curMeas.RetryCount = StrToInt(strRetryCountSet) - Re_Scan_Count;

	//build input power string
	sprintf(&inputPowerStr[0],"%.2f W",curMeas.inputPower);
	//build output power string
	sprintf(&outputPowerStr[0],"%.2f W",curMeas.outputPower);
	//build efficiency string
	sprintf(&effStr[0],"%.2f %",curMeas.efficiency);

	lbEffCalcManual->Caption = &effStr[0];

/*
	//calculate time remaining in the test
	//number of Y lines still to test
	int stepsRemaining = ( StrToInt(ebYSteps->Text) - testAdmin.ySteps - 1) * StrToInt(ebXSteps->Text);

	if (stepsRemaining < 0) {
		stepsRemaining = 0;
	}
	//plus number of X points to go in current Y line
	stepsRemaining += StrToInt(ebXSteps->Text) - testAdmin.xSteps;
	//multiply to find total time in seconds
	int timeRemaining = 2 * stepsRemaining * ( ( MEASUREMENT_TIME + StrToInt(ebPowerUpWait->Text) + StrToInt(ebOffTime->Text) ) / 1000 );
	int tempSecs = timeRemaining % 60;
	int tempMins = ( ( timeRemaining - tempSecs ) / 60 ) % 60;
	int tempHours = ( timeRemaining - tempMins * 60 - tempSecs ) / 3600;

	//display results to user
	lbInputPower->Caption = inputPowerStr;
	lbOutputPower->Caption = outputPowerStr;
	lbEff->Caption = effStr;
*/
	if (cbShowProgChart->Checked)
	{
		Series1->Add(curMeas.efficiency);
	}
	else
	{
		// Do Nothing
	} // End If


//	lblTimeRemaining->Caption = "Time Remaining";
//	timeRemainlb->Caption = IntToStr(tempHours) + ":" + IntToStr(tempMins) + ":" + IntToStr(tempSecs);

	// ***
	// Update Grid lbEff
	// ***
	// Check Plot Direction
//	if (TestStarted )
//	{

			// Timestamp
			StrGridSeqTest->Cells[14][SeqGridRow] = DateTimeToStr(Now());

			// ** Vin
			StrGridSeqTest->Cells[15][SeqGridRow] = FloatToStr(curMeas.values[INPUT_VOLTAGE_INDEX]);
			StrGridSeqTest->Cells[16][SeqGridRow] = FloatToStr(curMeas.values[INPUT_CURRENT_INDEX]);
			StrGridSeqTest->Cells[17][SeqGridRow] = FloatToStr(curMeas.values[OUTPUT_VOLTAGE_INDEX]);
			StrGridSeqTest->Cells[18][SeqGridRow] = FloatToStr(curMeas.values[OUTPUT_CURRENT_INDEX]);

			StrGridSeqTest->Cells[19][SeqGridRow] = FloatToStr(curMeas.efficiency);

			//ShowMessage(StrGridSeqTest->Cells[14][SeqGridRow]);
//			StrGridSeqTest->Cells[SeqGridRow][14] = effStr;
//			StrGridSeqTest->Cells[ ][  ] = Qi_Signal_Strength;
			// colour Cell if > 50%
//			if (curMeas.efficiency > 50.0)
//			{
//			   ColourGridCellGrn(1 + testAdmin.xSteps,(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps);
//			}
//			else
//			{

//			} // End If

//	}
//	else
//	{



//	} // End If
	}
	catch  ( ... )
	{
		   ShowMessage("Error Sub -> updateEfficiencySeqGrid");
	}
}

//---------------------------------------------------------------------------
// Update Efficiency Calculation
//---------------------------------------------------------------------------
void __fastcall TmainForm::updateEfficiencySeqGrid2()
{
   //int SeqGridRow

   // Warning Also update updateEfficiency2
   try
   {

	char inputPowerStr[15];
	char outputPowerStr[15];
	char effStr[15];
	static double nrCalls = 0;
	nrCalls++;

	//calculate latest results
	curMeas.inputPower = curMeas.values[INPUT_VOLTAGE_INDEX] * curMeas.values[INPUT_CURRENT_INDEX];
	curMeas.outputPower = curMeas.values[OUTPUT_VOLTAGE_INDEX] * curMeas.values[OUTPUT_CURRENT_INDEX];

	if (curMeas.values[INPUT_VOLTAGE_INDEX] < 0.1)
	{
		//capture Zero Tx voltage
		curMeas.efficiency = 0,0;
	}
	else
	{
		//capture devision by 0
		curMeas.efficiency = (curMeas.inputPower > 0.0) ? ( curMeas.outputPower * 100.0 / curMeas.inputPower ) : 0.0;
	} // end If


	// Get Retry Count
//	curMeas.RetryCount = StrToInt(strRetryCountSet) - Re_Scan_Count;

	//build input power string
	sprintf(&inputPowerStr[0],"%.2f W",curMeas.inputPower);
	//build output power string
	sprintf(&outputPowerStr[0],"%.2f W",curMeas.outputPower);
	//build efficiency string
	sprintf(&effStr[0],"%.2f %",curMeas.efficiency);

	lbEffCalcManual->Caption = &effStr[0];


	//display results to user
	lbInputPower->Caption = inputPowerStr;
	lbOutputPower->Caption = outputPowerStr;
	lbEff->Caption = effStr;



	if (cbShowProgChart->Checked)
	{
		Series1->Add(curMeas.efficiency);
	}
	else
	{
		// Do Nothing
	} // End If

/*
			// Timestamp
			StrGridSeqTest->Cells[14][SeqGridRow] = DateTimeToStr(Now());

			// ** Vin
			StrGridSeqTest->Cells[15][SeqGridRow] = FloatToStr(curMeas.values[INPUT_VOLTAGE_INDEX]);
			StrGridSeqTest->Cells[16][SeqGridRow] = FloatToStr(curMeas.values[INPUT_CURRENT_INDEX]);
			StrGridSeqTest->Cells[17][SeqGridRow] = FloatToStr(curMeas.values[OUTPUT_VOLTAGE_INDEX]);
			StrGridSeqTest->Cells[18][SeqGridRow] = FloatToStr(curMeas.values[OUTPUT_CURRENT_INDEX]);

			StrGridSeqTest->Cells[19][SeqGridRow] = FloatToStr(curMeas.efficiency);

*/

	}
	catch  ( ... )
	{
		   ShowMessage("Error Sub -> updateEfficiencySeqGrid2");
	}
}



//---------------------------------------------------------------------------
// Update voltage, current and position measurements
//---------------------------------------------------------------------------
void __fastcall TmainForm::tmMeterUpdateTimer(TObject *Sender)
{

	tmMeterUpdate->Enabled = false;

	ReadDaQDataValuesTimer();
/*
	StartMeasUSBMM();
	//USBMM->StartNewMeasurement();

	//if (USBMM->DataAvailable())
	if (DataAvaiUSBMM())
	{

//		USBMM->Start();

//		USBLogAdd("Data Available");
		UpdateDAQFormDisplay();
//		tmMeterUpdate->Enabled = false;

//		USBLogAdd("Ends - Update Data Available");

		updateEfficiency();

///		USBLogAdd("Ends - Data Available");

	}
	else
	{
		USBLogAdd("Error Reading DAQ");
	} // End If

*/

	tmMeterUpdate->Enabled = true;
//	USBLogAdd("Ends Manual Data Read");
}

//---------------------------------------------------------------------------
// Update voltage, current and position measurements
//---------------------------------------------------------------------------
void __fastcall TmainForm::ReadDaQDataValuesTimer()
{

	StartMeasUSBMM();
	//USBMM->StartNewMeasurement();

	//if (USBMM->DataAvailable())
	if (DataAvaiUSBMM())
	{

//		USBMM->Start();

//		USBLogAdd("Data Available");
		UpdateDAQFormDisplay();
//		tmMeterUpdate->Enabled = false;

//		USBLogAdd("Ends - Update Data Available");

//		updateEfficiency();
		updateEfficiencySeqGrid2();

///		USBLogAdd("Ends - Data Available");

	}
	else
	{
		USBLogAdd("Error Reading DAQ");
	} // End If

}

//---------------------------------------------------------------------------
// Update voltage, current and position measurements
//---------------------------------------------------------------------------
void __fastcall TmainForm::ReadDaQDataValuesSeq()
{
/*
	StartMeasUSBMM();
	//USBMM->StartNewMeasurement();

	//if (USBMM->DataAvailable())
	if (DataAvaiUSBMM())
	{

//		USBMM->Start();

//		USBLogAdd("Data Available");
		UpdateDAQFormDisplay();
//		tmMeterUpdate->Enabled = false;

//		USBLogAdd("Ends - Update Data Available");

//		updateEfficiency();
		updateEfficiencySeqGrid2();

///		USBLogAdd("Ends - Data Available");

	}
	else
	{
		USBLogAdd("Error Reading DAQ");
	} // End If

*/

	if (shpPSUConnected->Brush->Color == clLime)
	{
		// OK

			//***
			// Get DAQ Data
			//***
			StartMeasUSBMM();

			//temporary measurement storage
		   //	MAIN_measurement_t tmpMeas;

			//USBMM->DMM_Task();
			DMM_TaskUSBMM();

			//wait till data is available
			//if(USBMM->DataAvailable())
			if(DataAvaiUSBMM())
			{

				// ***
				// Process DATA
				// ***
				TestLogAdd("STATE -> Test_Process_Data State");
				try
					{
					//process and backup data

					UpdateDAQFormDisplay();

					//ShowMessage("done");

					updateEfficiencySeqGrid2();

/*
					if (cbDoNotMoveXY->Checked)
					{
						// Do Not Move
						updateEfficiency2();
					}
					else
					{
						// Move
						updateEfficiency();
					} // Ends If

*/

					//tmpMeas = curMeas;
					//display data to user

					// ***
					// Add the Qi Stuff now
					// ***
					curMeas.Qi_Signal_Strength = Qi_Signal_Strength;
					curMeas.Qi_Coil_Num_1 = Qi_Coil_Num_1;
					curMeas.Qi_Coil_Num_2 = Qi_Coil_Num_2;

//					cbCoilStatus->Text = "Final Result" + IntToStr(Qi_Coil_Num_1) + IntToStr(Qi_Coil_Num_2);

					lbQiSignal_strength->Caption = IntToStr(Qi_Signal_Strength);

//				 ShowMessage("Test2");

					memoUSBLog->Lines->Add("******");
					String M, Vin, Iin, Vout, Iout;
					M= "Measurement [" + String(testAdmin.curSetpoint.XPos) + "],[" + String(testAdmin.curSetpoint.YPos) + "]";
					Vin = "V-in [" + FloatToStr(curMeas.values[INPUT_VOLTAGE_INDEX]) + "]";
					Iin = "I-in [" + FloatToStr(curMeas.values[INPUT_CURRENT_INDEX]) + "]";
					Vout = "V-out [" + FloatToStr(curMeas.values[OUTPUT_VOLTAGE_INDEX]) + "]";
					Iout = "I-out [" + FloatToStr(curMeas.values[OUTPUT_CURRENT_INDEX]) + "]";

/*
					M= "Measurement [" + String(testAdmin.curSetpoint.XPos) + "],[" + String(testAdmin.curSetpoint.YPos) + "]";
					Vin = "V-in [" + FloatToStr((float)tmpMeas.values[INPUT_VOLTAGE_INDEX]) + "]";
					Iin = "I-in [" + FloatToStr((float)tmpMeas.values[INPUT_CURRENT_INDEX]) + "]";
					Vout = "V-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_VOLTAGE_INDEX]) + "]";
					Iout = "I-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_CURRENT_INDEX]) + "]";
*/

					memoUSBLog->Lines->Add(M);
					memoUSBLog->Lines->Add(Vin);
					memoUSBLog->Lines->Add(Iin);
					memoUSBLog->Lines->Add(Vout);
					memoUSBLog->Lines->Add(Iout);

					memoUSBLog->Lines->Add("*******");

				   } catch (...)
				   {
						TestLogAdd("ERROR in Data available State");
				   }  // End Try


				   // ***
				   // Stop Scanning
				   // ***

			  }
			  else
			  {
//					ShowMessage("PSU Control Manual Control");
					USBLogAdd("Error Reading DAQ");
					//Do nothing
			  } // End If

					// ***
					// SAVE DATA
					// ***
/*
					for (int i = 0; i < 1; i++)
					{
						if (WriteToCSV(testAdmin.testFile, GenerateCSVLine(tmpMeas)))
						{
						   tmTest->Enabled = true;
							break;
						}
						else
						{
						   tmTest->Enabled = false;
						   ShowMessage("Retry CSV" + IntToStr(i) + " of 1 max");
						} // End If
					} // End For
*/
					// ***
					// ENDS SAVE DATA
					// ***

			// ***
			// ENDS Process DATA
			// ***

		  // } // End If


	}
	else
	{
		// Do Nothing
		//Check if Alt DaQ







	} // End If



}


//---------------------------------------------------------------------------
// Update DAQ Form Display for Data Measurement Results
//---------------------------------------------------------------------------
void __fastcall TmainForm::UpdateDAQFormDisplay()
{

	//list pointers to meter data displays
	TEdit *ebPointers[4] = { ebInputVoltage,ebInputCurrent,ebOutputVoltage,ebOutputCurrent };
	TShape *shPointers[4] = { shInputVoltage,shInputCurrent,shOutputVoltage,shOutputCurrent };
	TEdit *mulPointers[4] = { ebVinMultiplier,ebIinMultiplier,ebVoutMultiplier,ebIoutMultiplier };
	TEdit *offsetPointers[4] = { ebVinOffset,ebIinOffset,ebVoutOffset,ebIoutOffset };

	//loop through all meters
	uint8_t curMeter;
	AnsiString curUnit;
	//if data is available, retreive it and update all meters

try
	{

		// Update Form Data Display---
	   //	TestLogAdd("---MeterUpdateTimer Run Data Avai---");

	  // if (cbDAQType->ItemIndex == 5)
	//   {
			// Using PSU and ELoad DAQ
//	   }
  //	   else
	//   {
//	   } // End If

			for(curMeter=0; curMeter < 4; curMeter++)
			{

				//if( USBMM->MeterConnected(curMeter) ) {
				if(MeterConnUSBMM(curMeter))
				{
					//indicate presence of meter
					shPointers[curMeter]->Brush->Color = clLime;
					//get latest measurement from meter
					//curMeas.values[curMeter] = USBMM->GetOutput(curMeter);
					curMeas.values[curMeter] = GetOutputUSBMM(curMeter);
					curMeas.values[curMeter] += StrToFloat(offsetPointers[curMeter]->Text);
					curMeas.values[curMeter] *= StrToFloat(mulPointers[curMeter]->Text);
					//update measurement indicator to display
					if( curMeas.values[curMeter] < 1.0 )
					{
						curUnit = ((curMeter % 2) == 0) ? " mV" : " mA";
						ebPointers[curMeter]->Text = FloatToDigitString(curMeas.values[curMeter]*1000.0) + curUnit;
					}
					else
					{
						curUnit = ((curMeter % 2) == 0) ? " V" : " A";
						ebPointers[curMeter]->Text = FloatToDigitString(curMeas.values[curMeter]) + curUnit;
					}  // End If

				} else
				{
					//indicate missing meter
					shPointers[curMeter]->Brush->Color = clSilver;
					//clear meter value textbox
					ebPointers[curMeter]->Text = "";

				}  // End If
			}  // End For
			// ENDS Update Form Data Display---















	} catch (...)
	{
		ShowMessage("Error " + FloatToStr(curMeas.values[curMeter]));
		lbErrorStatus->Caption = "Error in tmMeterUpdateTimer";
	}  // End Try

}

//---------------------------------------------------------------------------
// Add a line to the Test Log Memo
//---------------------------------------------------------------------------
void __fastcall TmainForm::TestLogAdd(AnsiString line)
{
	logit("Log.txt",line);
	memoTestLog->Lines->Add(line);
}

//---------------------------------------------------------------------------
// Add a line to the USB Log Memo
//---------------------------------------------------------------------------
void __fastcall TmainForm::USBLogAdd(AnsiString line)
{
	logit("Log.txt",line);
	memoUSBLog->Lines->Add(line);
}

//---------------------------------------------------------------------------
// Calculate the next setpoint for the test, and update testAdmin
//---------------------------------------------------------------------------
void __fastcall TmainForm::NextSetpoint()
{

//	TestLogAdd("Move to NEXT Plotter TEST POINT");

	//if more steps are to be taken in the x direction
	if(testAdmin.xSteps < StrToInt(ebXSteps2->Text) )
	{   //update setpoint
		testAdmin.curSetpoint.XPos += testAdmin.sign*StrToInt(ebStepSize2->Text);
		testAdmin.xSteps++;
	//if more steps are to be taken into the Y direction
	} else if (testAdmin.ySteps < StrToInt(ebYSteps2->Text)) {
		//update sign
		testAdmin.sign = -testAdmin.sign;
		//update setpoint
		testAdmin.curSetpoint.YPos += StrToInt(ebStepSize2->Text);
		testAdmin.xSteps=0;
		testAdmin.ySteps++;
	}

}

//---------------------------------------------------------------------------
// clear the test coordinates in testAdmin
//---------------------------------------------------------------------------
void __fastcall TmainForm::ResetTestCoordinates()
{
	testAdmin.xSteps = 0;
	testAdmin.ySteps = 0;

//	XYPlotter_setpoint_t tmpSetpoint;
//	testAdmin.xSteps = StrToInt(ebXSteps->Text);
//	testAdmin.ySteps = StrToInt(ebYSteps->Text);

	testAdmin.sign = 1;
	testAdmin.curIteration = 0;
//	testAdmin.curSetpoint.XPos = 0;
//	testAdmin.curSetpoint.YPos = 0;
	testAdmin.curSetpoint.XPos = StrToInt(ebXSetpointStart2->Text);
	testAdmin.curSetpoint.YPos = StrToInt(ebYSetpointStart2->Text);

//	testAdmin.curSetpoint.XPos = StrToInt(ebXOffset->Text);
//	testAdmin.curSetpoint.YPos = StrToInt(ebYOffset->Text);

	testAdmin.curDistance = Plotter->GetCurDistance();
}

//---------------------------------------------------------------------------
// clear the test coordinates in testAdmin
//---------------------------------------------------------------------------
void __fastcall TmainForm::ResetTestCoordinates2()
{
	testAdmin.xSteps = 0;
	testAdmin.ySteps = 0;

//	XYPlotter_setpoint_t tmpSetpoint;
//	testAdmin.xSteps = StrToInt(ebXSteps->Text);
//	testAdmin.ySteps = StrToInt(ebYSteps->Text);

	testAdmin.sign = 1;
	testAdmin.curIteration = 0;
//	testAdmin.curSetpoint.XPos = 0;
//	testAdmin.curSetpoint.YPos = 0;

//	testAdmin.curSetpoint.XPos = StrToInt(ebXSetpointStart->Text);
//	testAdmin.curSetpoint.YPos = StrToInt(ebYSetpointStart->Text);
	testAdmin.curSetpoint.XPos = ((SelGridColNumber - 1) *  StrToInt(ebStepSize2->Text)) + StrToInt(ebXSetpointStart2->Text);
	testAdmin.curSetpoint.YPos = ((SelGridRowNumber - 1)) * StrToInt(ebStepSize2->Text) + StrToInt(ebYSetpointStart2->Text);
	//tmpSetpoint.XPos = ((SelGridColNumber - 1) *  StrToInt(ebStepSize->Text)) + StrToInt(ebXSetpointStart2->Text);
	//tmpSetpoint.YPos = ((SelGridRowNumber - 1)) * StrToInt(ebStepSize->Text) + StrToInt(ebYSetpointStart->Text);

//	testAdmin.curSetpoint.XPos = StrToInt(ebXOffset->Text);
//	testAdmin.curSetpoint.YPos = StrToInt(ebYOffset->Text);

	testAdmin.curDistance = Plotter->GetCurDistance();
}

//---------------------------------------------------------------------------
// Generate filename based on user input, and already existing files
//---------------------------------------------------------------------------
AnsiString __fastcall TmainForm::CreateFileName(void)
{

	//get folder name
	if (AnsiString(ebFolderName->Text[1]).c_str() != "\\") {
		//ebFolderName->Text +=("\\");
	}
	AnsiString sBaseName = ebFolderName->Text + "Plotter_" + IntToStr((int)Plotter_Number) + "_" + ebFilename->Text + ebFilenameSeqAppend->Text + "_";
	AnsiString sFileName = "";

	//increase filename index until not file is found with that name/index
	uint8_t cnt = 0;
	do
	{
		sFileName = sBaseName + IntToStr(cnt) + ".csv";
		cnt++;
	}

	while (FileExists(sFileName));
	//return result
	return sFileName;
}

//---------------------------------------------------------------------------
// write single line to CSV file
//---------------------------------------------------------------------------
bool __fastcall TmainForm::WriteToCSV(AnsiString FileName, AnsiString Data)
{

try
{
	bool newFile = (FileExists(FileName)) ? false : true;
	//try and open existing file to append
	FILE *fileHandle = fopen(FileName.c_str(), "at");

	//if failed, try and open file to write
	if (fileHandle == NULL)
	{
		fileHandle = fopen(FileName.c_str(), "wt");
	}  // End If

	//if file was opened
	if (fileHandle != NULL){
		//write header if desired
		if(newFile){
			fputs("X_Index,Y_Index,X_Distance,Y_Distance,Input Voltage,Input Current,Input Power,Output Voltage,Output Current,Output Power,Efficiency,Qi Signal Strength,Coil 1,Coil 2,Retry\n"
					,fileHandle);
		}
		//write CSV line
		fputs(Data.c_str(), fileHandle);
		//close file
		fclose(fileHandle);
		//inform user
		TestLogAdd("====");
//		TestLogAdd("Wrote line to CSV file" + ebFilename->Text);
		TestLogAdd("Wrote line to CSV file ->");
		TestLogAdd(ebFilename->Text);
		TestLogAdd("====");
		return true;
	}
	else
	{
		TestLogAdd("Error opening " + ebFilename->Text);
		ShowMessage("WARNING Error Writing to CSV File");
//		btnPauseClick(this);
//		tmTest->Enabled = false;
		return false;
	}  // End If
}
catch (...)
{
	// Do Nothing
	return false;
	//	ShowMessage("Error Saving to File - Check if it is Open");

}

}

//---------------------------------------------------------------------------
// Write single line to CSV file
//---------------------------------------------------------------------------
bool __fastcall TmainForm::WriteToCSVSeqGrid(AnsiString FileName, AnsiString Data)
{

try
{
	bool newFile = (FileExists(FileName)) ? false : true;
	//try and open existing file to append
	FILE *fileHandle = fopen(FileName.c_str(), "at");

	//if failed, try and open file to write
	if (fileHandle == NULL)
	{
		fileHandle = fopen(FileName.c_str(), "wt");
	}  // End If

	//if file was opened
	if (fileHandle != NULL){
		//write header if desired
		AnsiString TestSeqHeader = "";
		for (int i = 1; i < StrGridSeqTest->ColCount; i++)
		{
			TestSeqHeader = TestSeqHeader + StrGridSeqTest->Cells[i][0] + ",";
		}
		TestSeqHeader = TestSeqHeader + "\n";
		if(newFile){
//			fputs("etry\n",fileHandle);
			fputs(TestSeqHeader.c_str(), fileHandle);
		}
		//write CSV line
		fputs(Data.c_str(), fileHandle);
		//close file
		fclose(fileHandle);
		//inform user
		TestLogAdd("====");
//		TestLogAdd("Wrote line to CSV file" + ebFilename->Text);
		TestLogAdd("Wrote line to CSV file ->");
		TestLogAdd(ebFilename->Text);
		TestLogAdd("====");
		return true;
	}
	else
	{
		TestLogAdd("Error opening " + ebFilename->Text);
		ShowMessage("WARNING Error Writing to CSV File");
//		btnPauseClick(this);
//		tmTest->Enabled = false;
		return false;
	}  // End If
}
catch (...)
{
	// Do Nothing
	return false;
	//	ShowMessage("Error Saving to File - Check if it is Open");

}

}

//---------------------------------------------------------------------------
// Write single line to Data CSV file
//---------------------------------------------------------------------------
bool __fastcall TmainForm::WriteToCSVData(AnsiString FileName, AnsiString Data, bool ShowErrorMessage)
//bool __fastcall TmainForm::WriteToCSVEffGrid(AnsiString FileName, AnsiString Data)
{

try
{
	bool newFile = (FileExists(FileName)) ? false : true;
	//try and open existing file to append
	FILE *fileHandle = fopen(FileName.c_str(), "at");

	//if failed, try and open file to write
	if (fileHandle == NULL)
	{
		fileHandle = fopen(FileName.c_str(), "wt");
	}  // End If

	//if file was opened
	if (fileHandle != NULL)
	{
/*
		//write header if desired
		AnsiString TestSeqHeader = "";
		for (int i = 1; i < 20; i++)
		{
			TestSeqHeader = TestSeqHeader + StrGridSeqTest->Cells[i][0] + ",";
		}
		TestSeqHeader = TestSeqHeader + "\n";
*/
//		if(newFile){
//			fputs("etry\n",fileHandle);
//			fputs(TestSeqHeader.c_str(), fileHandle);
//		}
		//write CSV line
		fputs(Data.c_str(), fileHandle);
		//close file
		fclose(fileHandle);
		//inform user
		TestLogAdd("====");
//		TestLogAdd("Wrote line to CSV file" + ebFilename->Text);
		TestLogAdd("Wrote line to CSV file ->");
		TestLogAdd(ebFilename->Text);
		TestLogAdd("====");
		return true;
	}
	else
	{
		TestLogAdd("Error opening " + ebFilename->Text);

		if (ShowErrorMessage)
		{
			ShowMessage("WARNING Error Writing to CSV File");
		}
		else
		{
			// Do Nothing
		} // End If


//		btnPauseClick(this);
//		tmTest->Enabled = false;
		return false;
	}  // End If
}
catch (...)
{
	// Do Nothing
	lbControlMessage->Caption = "Error Saving to File - Check if it is Open";
	return false;
	//	ShowMessage("Error Saving to File - Check if it is Open");

}

}

//---------------------------------------------------------------------------
// build CSV line, and write it to output file
//---------------------------------------------------------------------------
AnsiString __fastcall TmainForm::GenerateCSVLine2(MAIN_measurement_t meas)
{

AnsiString csvLine = "";
try
{
/*
	//write test indices
	if (testAdmin.sign == 1) {
		csvLine += String(testAdmin.xSteps) + ",";
	} else {
		csvLine += String(StrToInt(ebXSteps->Text) - testAdmin.xSteps) + ",";
	}
*/
//	csvLine += String(testAdmin.ySteps) + ",";

	//get Plotter position and setpoint

	//XYPlotter_setpoint_t curSetPoint = Plotter->GetSetpoint();
	//XYPlotter_setpoint_t curDistance = Plotter->GetCurDistance();

	//write distances
	//csvLine += String(curDistance.XPos) + ",";
	//csvLine += String(curDistance.YPos) + ",";

   //	csvLine += String(curDistance.XPos) + ",";
	//csvLine += String(curDistance.YPos) + ",";


	//write input voltage, current and power

	String Vin, Iin, Vout, Iout, Pin, Pout, Eff;
//	M= "Measurement [" + String(testAdmin.curSetpoint.XPos) + "],[" + String(testAdmin.curSetpoint.YPos) + "]";
	Vin = FloatToStr((float)meas.values[INPUT_VOLTAGE_INDEX]);
	Iin = FloatToStr((float)meas.values[INPUT_CURRENT_INDEX]);
	Pin = FloatToStr((float)meas.inputPower);
//	Vout = "V-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_VOLTAGE_INDEX]) + "]";
//	Iout = "I-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_CURRENT_INDEX]) + "]";
	csvLine = 	csvLine + Vin + "," + Iin + "," + Pin + ",";

	//csvLine += FloatToStr(meas.values[INPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";
	//csvLine += FloatToStr(meas.values[INPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";
	//csvLine += FloatToStr(meas.inputPower).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";

	//write output voltage, current and power
	Vout = FloatToStr((float)meas.values[OUTPUT_VOLTAGE_INDEX]);
	Iout = FloatToStr((float)meas.values[OUTPUT_CURRENT_INDEX]);
	Pout = FloatToStr((float)meas.outputPower);
	csvLine = csvLine + Vout + "," + Iout + "," + Pout + ",";

//	csvLine += FloatToStr(meas.values[OUTPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";
//	csvLine += FloatToStr(meas.values[OUTPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";
//	csvLine += FloatToStr(meas.outputPower).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";

	//write efficiency
//	csvLine += FloatToStr(meas.efficiency).SubString(0,CSV_SIGNIFICANT_DIGITS) + "\n";
	Eff = FloatToStr((float)meas.efficiency);
	csvLine = csvLine + Eff + "," ;

	//write Qi Stuff
	csvLine = csvLine + IntToStr(meas.Qi_Signal_Strength) + "," ;
	csvLine = csvLine + IntToStr(meas.Qi_Coil_Num_1) + "," ;
	csvLine = csvLine + IntToStr(meas.Qi_Coil_Num_2) + "," ;

	csvLine = csvLine + IntToStr(meas.RetryCount);

	csvLine = csvLine + "\n";
	return csvLine;

}
catch (...)
{
	ShowMessage("Error in GenerateCSVLine");
	lbErrorStatus->Caption = "Error in GenerateCSVLine";

}
	return csvLine;
}



//---------------------------------------------------------------------------
// build CSV line, and write it to output file
//---------------------------------------------------------------------------
AnsiString __fastcall TmainForm::GenerateCSVLine(MAIN_measurement_t meas)
{

AnsiString csvLine = "";
try
{
	//write test indices
	if (testAdmin.sign == 1) {
		csvLine += String(testAdmin.xSteps) + ",";
	} else {
		csvLine += String(StrToInt(ebXSteps2->Text) - testAdmin.xSteps) + ",";
	}

	csvLine += String(testAdmin.ySteps) + ",";
	//get Plotter position and setpoint
	XYPlotter_setpoint_t curSetPoint = Plotter->GetSetpoint();
	XYPlotter_setpoint_t curDistance = Plotter->GetCurDistance();
	//write distances
	csvLine += String(curDistance.XPos) + ",";
	csvLine += String(curDistance.YPos) + ",";
	//write input voltage, current and power

	String Vin, Iin, Vout, Iout, Pin, Pout, Eff;
//	M= "Measurement [" + String(testAdmin.curSetpoint.XPos) + "],[" + String(testAdmin.curSetpoint.YPos) + "]";
	Vin = FloatToStr((float)meas.values[INPUT_VOLTAGE_INDEX]);
	Iin = FloatToStr((float)meas.values[INPUT_CURRENT_INDEX]);
	Pin = FloatToStr((float)meas.inputPower);
//	Vout = "V-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_VOLTAGE_INDEX]) + "]";
//	Iout = "I-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_CURRENT_INDEX]) + "]";
	csvLine = 	csvLine + Vin + "," + Iin + "," + Pin + ",";

	//csvLine += FloatToStr(meas.values[INPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";
	//csvLine += FloatToStr(meas.values[INPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";
	//csvLine += FloatToStr(meas.inputPower).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";

	//write output voltage, current and power
	Vout = FloatToStr((float)meas.values[OUTPUT_VOLTAGE_INDEX]);
	Iout = FloatToStr((float)meas.values[OUTPUT_CURRENT_INDEX]);
	Pout = FloatToStr((float)meas.outputPower);
	csvLine = csvLine + Vout + "," + Iout + "," + Pout + ",";

//	csvLine += FloatToStr(meas.values[OUTPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";
//	csvLine += FloatToStr(meas.values[OUTPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";
//	csvLine += FloatToStr(meas.outputPower).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";

	//write efficiency
//	csvLine += FloatToStr(meas.efficiency).SubString(0,CSV_SIGNIFICANT_DIGITS) + "\n";
	Eff = FloatToStr((float)meas.efficiency);
	csvLine = csvLine + Eff + "," ;

	//write Qi Stuff
	csvLine = csvLine + IntToStr(meas.Qi_Signal_Strength) + "," ;
	csvLine = csvLine + IntToStr(meas.Qi_Coil_Num_1) + "," ;
	csvLine = csvLine + IntToStr(meas.Qi_Coil_Num_2) + "," ;

	csvLine = csvLine + IntToStr(meas.RetryCount);

/* COMMENT OUT*************
	 AnsiString sLine = "";
	 for (int i = 0; i < 29; i++)
		 {
		 sLine = sLine + IntToStr( ( int )meas.FODA_0_Phase_Buff[i] ) + ",";
		 }
	 for (int i = 0; i < 29; i++)
		 {
		 sLine = sLine +IntToStr( ( int )meas.FODA_90_Phase_Buff[i] );
	 if (i != 28)
		 {
		 sLine = sLine + "," ;
		 }
		 }

		csvLine = csvLine + sLine;
********************************** */

	csvLine = csvLine + "\n";
	return csvLine;

}
catch (...)
{
	ShowMessage("Error in GenerateCSVLine");
	lbErrorStatus->Caption = "Error in GenerateCSVLine";

}
	return csvLine;
}

//---------------------------------------------------------------------------
// Main test-application state machine
//---------------------------------------------------------------------------
void __fastcall TmainForm::TestStateMachine1()
{

static float WaitTimerTicks;
static float WaitTimerTicksMax;

static float WaitOffTimerTicks;
static float WaitOffTimerTicksMax;

int pCoilStatus[10][2];
// ScanAndPowerON

static bool TxPollDone;

static int NumberOfCoilPowered;
static int CoilNumberPowered[10];


// ***
// Update Tx feedback
// ***
//	ReportTxReadData
/*		AnsiString TxRead = ReportTxReadData();
	if (TxRead == NULL)
	{
		// Do Nothing
	}
	else
	{
		TXCommMemo->Lines->Add(TxRead);
	}
*/

try
{
	switch (testAdmin.testState)
	{
	   // ````````````````````````````````````````````````````````````````````````````````
	   case TEST_IDLE_STATE:
			lbErrorStatus->Caption = "STATE -> Test_Idle State";

			break;

	   // ````````````````````````````````````````````````````````````````````````````````
		case TEST_INIT_STATE:
			//Indicate start of test
			lbErrorStatus->Caption = "STATE -> Test_Init State";

			Series1->Clear();

			WaitTimerTicks = 0;
			WaitOffTimerTicks = 0;

			WaitTimerTicksMax = StrToFloat(ebPowerUpWait2->Text)/200;
			WaitOffTimerTicksMax = StrToFloat(ebOffTime2->Text)/200;

			TestLogAdd("Starting Test : " + ebXSteps2->Text + "," + ebYSteps2->Text + "," + ebStepSize2->Text);
			//disable PSU
			btDisablePSUClick(this);

#ifndef USB_PSU
			tmPlotter_NRT->Enabled = true;
			testAdmin.NRT_Goto = true;
			testAdmin.NRTState =TEST_INIT_STATE;
#endif

		if (cbDoNotMoveXY->Checked)
		{
			// Do Not Move
			ResetTestCoordinates2();
			WaitOffTimerTicks = 0.0;
			testAdmin.testState = TEST_MOVE_STATE;

		}
		else
		{

			//initialize test
			ResetTestCoordinates();
			testAdmin.curIteration = 0;

		   strRetryCountSet = ebHoleRetryNo->Text;

//			if (cbSimPlotting->Checked)
//			{
//				// Do Nothing
//			}
//			else
//			{
				//Set Initial Setpoint
				Plotter->UpdateSetpoint(testAdmin.curSetpoint);
				tmPlotter_NRT->Enabled = true;
				testAdmin.NRT_Goto = true;

//			} // End If

			testAdmin.NRTState =TEST_MOVE_NRT;

		} // End If

			//add log entry to let user validate positions..
			TestLogAdd("Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
						+ String(testAdmin.curSetpoint.YPos) + "]");
			//if plotter has reached destination
			WaitOffTimerTicks = 0.0;
			testAdmin.testState = TEST_MOVE_STATE;
			//start measurement equipment

			StartUSBMM();
			//USBMM->Start();

			btOpenUSB->Enabled = false;
			btCloseUSB->Enabled = true;
			// Init State
			TxFlgRunning = false;
			TxReScan = false;

			// Init Re_Scan Count
			Re_Scan_Count =  StrToInt(strRetryCountSet); // Add one to allow for 0 retry
		//	ShowMessage("Scan count reset" + IntToStr(Re_Scan_Count));

			break;

	   // ````````````````````````````````````````````````````````````````````````````````
		case TEST_MOVE_NRT:
			//Indicate start of test
			lbErrorStatus->Caption = "STATE -> Test_Move_NRT";

			Plotter->UpdateSetpoint(testAdmin.curSetpoint);

			if (cbSimPlotting->Checked)
			{
				// Do Nothing
			}
			else
			{
				tmPlotter_NRT->Enabled = true;
			}  // End If


			testAdmin.NRT_Goto = true;
			testAdmin.NRTState =TEST_MOVE_NRT;

			TestLogAdd("NRT Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
						+ String(testAdmin.curSetpoint.YPos) + "]");

			// Init Re_Scan Count
			Re_Scan_Count =  StrToInt(strRetryCountSet); // Add one to allow for 0 retry
		 //	ShowMessage("Scan count reset" + IntToStr(Re_Scan_Count));

			WaitOffTimerTicks = 0.0;
			testAdmin.testState = TEST_MOVE_STATE;

			break;

	   // ````````````````````````````````````````````````````````````````````````````````
		case TEST_MOVE_STATE:
		//Indicate start of test

		// OFF STATE

		if (WaitOffTimerTicks == 0.0)
		{
			// ***
			// Run Once only
			// ***

			WaitOffTimerTicks = WaitOffTimerTicks + 1.0;

			lbErrorStatus->Caption = "STATE -> Test_Move State";

			tmTest->Interval = 	200; // StrToInt(ebPowerUpWait->Text); //200;  // Set the state timer to default time todo make define

/*			if (mainForm->edTxOpenedVersion->Text == "")
			{
				// Lost Tx Comms
				ShowMessage("Error with Tx Comms - Test Paused");
				 //	btnPauseClick(this);
				break;
			}
			else
			{
				// Do Nothing
				break;
			} // End If

*/

/*

			if (mainForm->edTxOpenedVersion->Text == "")
			{
				memoTestLog->Lines->Add("=================");
				memoTestLog->Lines->Add("** CHECK Tx Still OK by Asking for Verison**");
				memoTestLog->Lines->Add("=================");

				mainForm->edTxOpenedVersion->Text = "Tx Not Connected!";
				mainForm->lbTxConnectedStatus->Caption = "Tx NOT Connected!";
			}
			else
			{
				mainForm->lbTxConnectedStatus->Caption = "Tx Connected";
			} // End If
*/

			PSU_OFF();

			memoTestLog->Lines->Add("Power OFF\n");

			memoTestLog->Lines->Add("1 MOVE State Loop Run ONCE\n");

			if (cbSimPlotting->Checked)
			{
					// ***
					//  Off Time
					// ***
			   //		tmTest->Interval = 	StrToInt(ebOffTime->Text);  // Set the state timer to power off time

					Qi_Signal_Strength = 0;
					First_Qi_Coil = true;
					Qi_Coil_Num_1 = 0;
					Qi_Coil_Num_2 = 0;

					// SIMULATE Plotting
					// Init Re_Scan Count
				   //	Re_Scan_Count =  StrToInt(strRetryCountSet); // Add one to allow for 0 retry
					WaitTimerTicks = 0.0;  // Ensure Timer is reset
					testAdmin.testState = TEST_ENABLE_PSU_STATE;

			}
			else
			{
				if( Plotter->SetpointReached() )
				{   //setup timeout

					// ***
					// Off Time
					// ***
				   //	tmTest->Interval = 	StrToInt(ebOffTime->Text);  // Set the state timer to power off time

					Qi_Signal_Strength = 0;
					First_Qi_Coil = true;
					Qi_Coil_Num_1 = 0;
					Qi_Coil_Num_2 = 0;

				   //	WaitTimerTicks = 0.0;  // Ensure Timer is reset
				   //	testAdmin.testState = TEST_ENABLE_PSU_STATE;

				} // End If

			}  // End If

		// ***
		// ENDS Run Once only
		// ***
		}
		else
		{
			// ***
			// Slow Timer down to get wait time
			// ***

			if (cbDebug->Checked)
			{
				AnsiString Countdown = FloatToStr(WaitOffTimerTicks);
				memoTestLog->Lines->Add("2. MOVE Wait Time Polling - " + Countdown);
			}
			else
			{
				// Do Nothing
			} // End If

			// ***
			//  Wait Count
			// ***
			WaitOffTimerTicks = WaitOffTimerTicks + 1.0;
			if (WaitOffTimerTicks > WaitOffTimerTicksMax + 1.0)
			{
			   // ***
			   // Off State Done
			   // ***

				lbErrorStatus->Caption = "STATE -> Test_Move State";

				if (cbSimPlotting->Checked)
				{
					// SIMULATE Plotting
					// Init Re_Scan Count
				   //	Re_Scan_Count =  StrToInt(strRetryCountSet); // Add one to allow for 0 retry
					WaitTimerTicks = 0.0;  // Ensure Timer is reset
					testAdmin.testState = TEST_ENABLE_PSU_STATE;
					// ***
					//  Off Time
					// ***
				   //	tmTest->Interval = 	StrToInt(ebOffTime->Text);  // Set the state timer to power off time

					Qi_Signal_Strength = 0;
					First_Qi_Coil = true;
					Qi_Coil_Num_1 = 0;
					Qi_Coil_Num_2 = 0;

				}
				else
				{
					if( Plotter->SetpointReached() )
					{   //setup timeout

						// Init Re_Scan Count
					   //	Re_Scan_Count =  StrToInt(strRetryCountSet); // Add one to allow for 0 retry
						WaitTimerTicks = 0.0;  // Ensure Timer is reset
						testAdmin.testState = TEST_ENABLE_PSU_STATE;

						// ***
						// Off Time
						// ***
						//tmTest->Interval = 	StrToInt(ebOffTime->Text);  // Set the state timer to power off time

						Qi_Signal_Strength = 0;
						First_Qi_Coil = true;
						Qi_Coil_Num_1 = 0;
						Qi_Coil_Num_2 = 0;

					} // End If

				}  // End If

			   // ***
			   // ENDS Off State Done
			   // ***

			   // Reset Timer
			   WaitOffTimerTicks = 0.0;
			}
			else
			{
				// ***
				// Poling Wait State
				// ***
				lbCountDownState->Caption = "OFF Countdown Status";
				lblWaitCountdown->Caption = FloatToStr(WaitOffTimerTicksMax + 1.0 - WaitOffTimerTicks);

			} // End If

		} // End If

		break;

	   // ````````````````````````````````````````````````````````````````````````````````
		case TEST_ENABLE_PSU_STATE:
		//Indicate start of test


		//		tmTest->Enabled = false;

		tmTest->Interval = 	200;

		// ***
		// ON Time
		// ***
	   //	memoTestLog->Lines->Add("PSU State Loop\n");

		if (WaitTimerTicks == 0.0)
		{
		// ***
		// Run Once only
		// ***
			lbErrorStatus->Caption = "STATE -> Test_Enable_PSU State";

			WaitTimerTicks = WaitTimerTicks + 1.0;

			memoTestLog->Lines->Add("1 PSU ON State Loop Run ONCE\n");

			// ***
			//enable power supply
			// ***
//			btEnablePSUClick(this);
			PSU_ON();

			// Check if Tx Still Connected Just before power off State Previous - Use Tx Version as Check
//			mainForm->edTxOpenedVersion->Text = "";

/*			tmTest->Enabled = false;

			mainForm->edTxOpenedVersion->Text = "";
			memoTestLog->Lines->Add("** CHECK Tx Still OK by Asking for Verison**");
			SendGetVersion();

			for (int i = 0; i < 10000; i++)
			{
				if (mainForm->edTxOpenedVersion->Text == "")
				{
					// Lost Tx Comms
					ShowMessage("Error with Tx Comms - Test Paused");
					 //	btnPauseClick(this);
					break;
				}
				else
				{
					// Do Nothing
					break;
				} // End If
				Sleep(1);
			}  // End For

			tmTest->Enabled = true;
*/


#ifndef USB_PSU
			if (cbSimPlotting->Checked)
			{
				// Do Nothing
			}
			else
			{
				tmPlotter_NRT->Enabled = true;
			} // End If

			testAdmin.NRT_Goto = true;
#endif

			//WaitTimerTicks = 0.0;  // Ensure Timer is reset
			testAdmin.NRTState =TEST_ENABLE_PSU_STATE;

//			tmTest->Interval = 	200; // StrToInt(ebPowerUpWait->Text); //200;  // Set the state timer to default time todo make define

			//setup timeout

//			testAdmin.testState = TEST_GET_DATA_STATE;

	 //		SetTxManualMode();

//			ShowMessage("START Mod Depth");
//			TestLogAdd("----");
//			TestLogAdd("Set Modulation Depth");
	   //		SetModDepth();
	   //		ShowMessage("Mod Depth");

			// Re-set to confirm Tx Operating Modes
//			TestLogAdd("----");
//			TestLogAdd("Set Tx Operating Mode");
//			SetTxOpMode();



/* *****COMMENTED OUT************************

			// ***
			// START SCANNING
			// ***
			//ShowMessage("fere");
			if(TxFlgRunning)
			{
			  // Do Nothing
			   // ShowMessage("Running Loop");
				memoTestLog->Lines->Add("Tx Flag Running Loop\n");
			}
			else
			{
//				ShowMessage("Scan Loop");
				memoTestLog->Lines->Add("2 Start Scan");

				TxFlgRunning = true;
//				ShowMessage("Run Once");
				// ShowMessage("Scan Start");
				if (flgTxPortOpen)
				{
					//ShowMessage("Tx Port Open");
					memoTestLog->Lines->Add("2b Check Comport Closed");
				   //	CloseTxPort();

					TxPollDone = false;

//					TXComPortNo = UserSelectComport2("Select TX Com-Port", "PortTX", 115200);

				//	bool result = SetManualMode2();

					memoTestLog->Lines->Add("2c Open Comport");
//					if (cbManual_Mode->Checked)
					if (OpenTxPort(TXComPortNo))
					{

					   if (cbManual_Mode->Checked)
					   {
							bool result = SetManualMode2();
					   }
					   else
					   {
							// Do Nothing
							//SetAutoMode2
					   } // End If

	//					tmTest->Enabled = false;

						if (FSKDepthCmd == NULL)
						{
							ShowMessage("WARNING - No FSK Depth Set");
						}
						else
						{
							SetFSKDepth(FSKDepthCmd);
						} // End If

						cbCoilStatus->Clear();
						cbCoilStatus->Text = "Start Coil Scan";

						memoTestLog->Lines->Add("3 Send Scan & Power Cmd");
	//					ScanAndPowerON(80,8,0,0,pCoilStatus);

						if (cbSimPlotting->Checked)
						{
							// Do Nothing
						}
						else
						{
							ScanAndPowerONOnly(10,8,0,0);
						} // End If

	/*
						// Build report of Coil status
						for (int i = 0; i < 10; i++)
						{
							AnsiString TempResult = IntToStr(i) + ". " + IntToStr(pCoilStatus[i][0]) + " - " + IntToStr(pCoilStatus[i][1]);
							cbCoilStatus->Items->Add(TempResult);

						}  // End For
						cbCoilStatus->Text = "Coil Status Updated";
	*/
	//					ShowMessage(pCoil_Status);


	//					ScanAndPowerONPlotter(20,8,0,0);
	//					ShowMessage("Scan Started");
	//					ScanAndPowerONPlotter(20,8,0,0);

	//					tmTest->Enabled = true;


/*
					}
					else
					{
						// Do Nothing
					} // End If


				}
				else
				{
		   //			ShowMessage("Tx Port NOT Open");

				} // End If

			}   // End If

				// ***
				// ENDS START SCANNING
				// ***

*****ENDS COMMENTED OUT*********************** */

			if (cbHoleRetry->Checked)
			{

				DMM_TaskUSBMM();
				//USBMM->DMM_Task();
				// Init Count
				TxReScan = false;
//				ShowMessage("Init");

			}
			else
			{
				// Do Nothing
			}  // End If

//			mainForm->ebOutputVoltage->Text = FloatToStr(WaitTimerTicks) + "_" + FloatToDigitString(USBMM->GetOutput(2));
//			// GetOutput 1 = Tx I in, 2 = Rx V , 3= Rx I, 4 = TxVin


		// ***
		// ENDS Run Once only
		// ***
		}
		else
		{
		  // ***
		// Poll Tx STatus
		// ***

//				mainForm->edTxOpenedVersion->Text = "";

			// Check if Tx still Connected

/*
			if (mainForm->edTxOpenedVersion->Text == "")
			{
				memoTestLog->Lines->Add("** CHECK Tx Still OK by Asking for Verison**");
				mainForm->edTxOpenedVersion->Text = "Tx Not Connected?";
			}
			else
			{
			   // Do Nothing
			} // End If

			SendGetVersion();   // <- Will "hange" here if Tx not connected in this loop!!!!!!!
			mainForm->edTxOpenedVersion->Text = "";

*/
/*
			// Check if Tx Still Connected
			if (WaitTimerTicks == 10.0)
			{
				mainForm->edTxOpenedVersion->Text = "";
				memoTestLog->Lines->Add("** CHECK Tx Still OK by Asking for Verison**");
				SendGetVersion();
			}
			else
			{
				// Do Nothing
			} // End If
*/
/*
			// Check if Tx Still Connected
			if (WaitTimerTicks == 15.0)
			{
				if (mainForm->edTxOpenedVersion->Text == "")
				{
					// Lost Tx
					//ShowMessage("Error with Tx Comms - Test Paused");
					btnPauseClick(this);

				} // End If

			} // End If
*/

/*
// Old TX Mike

			if(TxFlgRunning)
			{
				if (cbDebug->Checked)
				{
					AnsiString Countdown = FloatToStr(WaitTimerTicks);
					memoTestLog->Lines->Add("Polling Tx - " + Countdown);
				}
				else
				{
					// Do Nothing
				} // End If


			//***
			// SIMULATION OPTION
			// ***
			if (cbSimPlotting->Checked)
			{
				// Do Nothing
			}
			else
			{
				// ***
				// Read Tx Messages
				// ***

				TxPollDone = false;
				if (TxPollDone)
				{
					// Read Tx Done

				}
				else
				{


					if (PollTxMessages())
					{
//						TxPollDone = true;

						ReadTxMessages(pCoilStatus);

						// Build report of Coil status
						AnsiString TempResult = "";
						NumberOfCoilPowered = 0;
						for (int i = 0; i < 10; i++)
						{
							AnsiString TempResult = IntToStr(i) + ". " + IntToStr(pCoilStatus[i][0]) + " - " + IntToStr(pCoilStatus[i][1]);
							if (cbDebug->Checked)
							{
								cbCoilStatus->Items->Add(TempResult);

								TXCommMemo->Lines->Add(TempResult);

							}  // End If
							switch (pCoilStatus[i][0])
							{
								case 4:
									NumberOfCoilPowered++;
									CoilNumberPowered[NumberOfCoilPowered] = i;
									break;
								default:
									break;
							} // End switch

						}  // End For

					}
					else
					{
						// Do Nothing
						//TxPollDone = false;
					} // End If


				} // End If



			} // End If
			//***
			// SIMULATION OPTION
			// ***




//				cbCoilStatus->Text = "Coil Status Updated";

				// ***
				// Poll to see if Tx Done
				// ***
				//ShowMessage(TxStatus());
				if (TxStatusDone())
				{
					// Done
					// ShowMessage("Scan Done");

				 //	TxFlgRunning = false;

				/*	Qi_Signal_Strength = 0;
					First_Qi_Coil = true;
					Qi_Coil_Num_1 = 0;
					Qi_Coil_Num_2 = 0;
				 */
//					tmpMeas.Qi_Signal_Strength = Qi_Signal_Strength;
//					tmpMeas.Qi_Coil_Num_1 = Qi_Coil_Num_1;
//					tmpMeas.Qi_Coil_Num_2 = Qi_Coil_Num_2;

/*

					Qi_Signal_Strength = 0;
//					First_Qi_Coil = true;

//					cbCoilStatus->Clear();
					cbCoilStatus->Text = "Final Result";
					switch (NumberOfCoilPowered)
					{
						case 0:
							break;
						case 1:
							Qi_Coil_Num_1 = CoilNumberPowered[1];
							Qi_Signal_Strength = pCoilStatus[CoilNumberPowered[1]][1];
							cbCoilStatus->Items->Add(FloatToStr(WaitTimerTicks) + ". " + IntToStr(Qi_Coil_Num_1) + "," + IntToStr(Qi_Signal_Strength));
							break;
						case 2:
							Qi_Coil_Num_1 = CoilNumberPowered[1];
							Qi_Coil_Num_2 = CoilNumberPowered[2];
							Qi_Signal_Strength = pCoilStatus[CoilNumberPowered[1]][1];
							cbCoilStatus->Items->Add(FloatToStr(WaitTimerTicks) + ". " + IntToStr(Qi_Coil_Num_1) + "," + IntToStr(Qi_Coil_Num_2) + "," + IntToStr(Qi_Signal_Strength));
							break;
						default:
							break;
					} // End switch

					if (cbDebug->Checked)
					{
						memoTestLog->Lines->Add("Tx Done ");
					} // End If


/*					GetTxSigStrength(Qi_Signal_Strength);
//					GetTxStatus(Qi_Signal_Strength);
					GetTxCoil1(Qi_Coil_Num_1);
					GetTxCoil2(Qi_Coil_Num_2);
*/
					//ShowMessage("Sig = " ); //& IntToStr(Qi_Signal_Strength));
					//ShowMessage("Sig = " + IntToStr(Qi_Signal_Strength));
					//ShowMessage("Coil1 = " + IntToStr(Qi_Coil_Num_1));
					//ShowMessage("Coil2 = " + IntToStr(Qi_Coil_Num_2));
/*
				}
				else
				{
					// Tx Not Done

				} // End If
			}
			else
			{
				// TxFlgRunning = true;
//				ShowMessage("Scan Start");
				//ShowMessage("Scan");

//				ScanAndPowerON(20,8,0,0);
				//ShowMessage("Scan End");

			} // End If
		// ***
		// ENDS Poll Tx STatus
		// ***
// Ends old tx Mike

*/

		} // End If


		// ***
		// Retry ON/Off
		// ***
		if (cbHoleRetry->Checked)
		{
		  // RetryCount = StrToInt(ebHoleRetryNo->Text);

			// ***
			// Re_Scan_Count is a countdown integer
			// ***
//			if (Re_Scan_Count > 0)
//			{
//				RetryCount = RetryCount - 1;
//				ebHoleRetryNo->Text = IntToStr(RetryCount);
				// ***
				// Check Rx Voltage
				// ***
				//wait till data is available

				// ***
				// Test Signal Strength
				// ***

				lbQiSignal_strength->Caption = IntToStr(Qi_Signal_Strength);

				StartMeasUSBMM();
				//USBMM->StartNewMeasurement();

				//if(USBMM->DataAvailable())
				if(DataAvaiUSBMM())
				{

					//curMeas.values[OUTPUT_VOLTAGE_INDEX] = USBMM->GetOutput(OUTPUT_VOLTAGE_INDEX);
					curMeas.values[OUTPUT_VOLTAGE_INDEX] = GetOutputUSBMM(OUTPUT_VOLTAGE_INDEX);
					// GetOutput 1 = Tx I in, 2 = Rx V , 3= Rx I, 4 = TxVin
				}
				else
				{
					// Do Nothing
				} // End If

				mainForm->ebOutputVoltage->Text = FloatToStr(WaitTimerTicks) + "_" + FloatToDigitString(curMeas.values[OUTPUT_VOLTAGE_INDEX]);

				// Test if Rx Voltage ever rises above threshold
				if ((curMeas.values[OUTPUT_VOLTAGE_INDEX] > StrToFloat(edRxVoltReScanLimit->Text)) || (StrToFloat(edRxVoltReScanLimit->Text) == 0))
				{
					// Flag re-scan subject to Sig Strength
					if (Qi_Signal_Strength > StrToInt(edSigStrenReScanLimit->Text))
					{
						// Valid Plot for retry testing
						if (TxReScan)
						{
							// Do Nothing
						}
						else
						{
							mainForm->lbRetryNotice->Caption = mainForm->lbRetryNotice->Caption + "_Wait";
						} // End If

						// Set Re-scan ON
						TxReScan = true;

					}
					else
					{
						// Located in Zero coil pickup range?
						// Do Nothing
//						if (mainForm->lbRetryNotice->Caption.TrimRight(2) == "_V")
//						{
//							mainForm->lbRetryNotice->Caption = mainForm->lbRetryNotice->Caption + "_V";
//						}
//						else
//						{
//							mainForm->lbRetryNotice->Caption = mainForm->lbRetryNotice->Caption + "_V";
//						} // End If

					} // End If

				}
				else
				{
					//TxReScan = true;
				} // End If


//			}
//			else
//			{
//			   // Do Not Re-Scan
//			   TxReScan = false;
//			} // End If

		}
		else
		{
			// Do Nothing
		}  // End If

		// ***
		// Timer countdown to get wait time
		// ***
		WaitTimerTicks = WaitTimerTicks + 1.0;
		if (WaitTimerTicks > WaitTimerTicksMax + 1.0)
		{
			// Scan and Power Done
			memoTestLog->Lines->Add("SCAN DONE");
			memoTestLog->Lines->Add("************************");

			WaitTimerTicks = 0.0;
//			WaitOffTimerTicks = 0.0;

			TxFlgRunning = false;

			// If Retry set
			if (cbHoleRetry->Checked)
			{
				// ***
				// Check if Rx Voltage still present
				// ***

				//wait till data is available
				//USBMM->StartNewMeasurement();
				StartMeasUSBMM();

//				int retryCount = 0;
//				while (retryCount < 50)
//				{
					//if(USBMM->DataAvailable())
					if(DataAvaiUSBMM())
					{
//						retryCount = 0;
						//curMeas.values[OUTPUT_VOLTAGE_INDEX] = USBMM->GetOutput(OUTPUT_VOLTAGE_INDEX);
						curMeas.values[OUTPUT_VOLTAGE_INDEX] = GetOutputUSBMM(OUTPUT_VOLTAGE_INDEX);
						// GetOutput 1 = Tx I in, 2 = Rx V , 3= Rx I, 4 = TxVin
					}
					else
					{
						// Do Nothing
//						Sleep(1);
//						retryCount = retryCount + 1;
					} // End If
//				} // End While

				mainForm->ebOutputVoltage->Text = "Last_" + FloatToStr(WaitTimerTicks) + "_" + FloatToDigitString(curMeas.values[OUTPUT_VOLTAGE_INDEX]);

				// Final Test if Rx Voltage ever rises above set threshold
//				if ((curMeas.values[OUTPUT_VOLTAGE_INDEX] > StrToFloat(edRxVoltReScanLimit->Text)) && (StrToFloat(edRxVoltReScanLimit->Text) > 0))
//				if (curMeas.values[OUTPUT_VOLTAGE_INDEX] > StrToFloat(edRxVoltReScanLimit->Text))
				if (curMeas.values[OUTPUT_VOLTAGE_INDEX] > StrToFloat(edRxVoltFinalTestLimit->Text))
				{
					// Done - No Retry required
					mainForm->lbRetryNotice->Caption = mainForm->lbRetryNotice->Caption + "_OK";
					// Set Re-scan OFF
					TxReScan = false;
				}
				else
				{
					// Do Nothing for default below
					//TxReScan = true;  // Whatever was last set

				} // End If
				// ***
				// ENDS Check if Rx Voltage still present
				// ***

				// Change State Decision
				if (TxReScan && (Re_Scan_Count > 0))
				{
					// Stay in Current State
					// Re-Scan Go to Power Off State
	  //				ebHoleRetryNo->Text = IntToStr(Re_Scan_Count);
	//				ShowMessage("Count1 = " + ebHoleRetryNo->Text);

					Re_Scan_Count = Re_Scan_Count - 1;
				//	ShowMessage("Scan countdown = " + IntToStr(Re_Scan_Count));
	//				ebHoleRetryNo->Text = IntToStr(Re_Scan_Count);
	//				ShowMessage("Count2 = " + ebHoleRetryNo->Text);

					// Init State
					//TxReScan = false;
					mainForm->lbRetryNotice->Caption = "Retry = " + IntToStr(StrToInt(strRetryCountSet) - Re_Scan_Count);

					WaitOffTimerTicks = 0.0; // Ensure Off timer is reset
					testAdmin.testState = TEST_MOVE_STATE;
				}
				else
				{
					// Move State
					// Restore retry Cuont
	//				ebHoleRetryNo->Text = strRetryCountSet;

					// Init Re_Scan Count
				  //	Re_Scan_Count =  StrToInt(strRetryCountSet);  // DO NOT re-init here as used to update CSV file later!!!
				  //	ShowMessage("Scan count reset" + IntToStr(Re_Scan_Count));

				   //	mainForm->lbRetryNotice->Visible = false;
					mainForm->lbRetryNotice->Caption = "Retry = Ready";
					// Go to Next State
					testAdmin.testState = TEST_GET_DATA_STATE;
	//				ShowMessage("ll");

				} // End If
			}
			else
			{
				// Go to Next State
				testAdmin.testState = TEST_GET_DATA_STATE;
			} // End If


//			mainForm->edTxOpenedVersion->Text = "";
//			SendGetVersion();   // <- Will "hang" here if Tx not connected in this loop!!!!!!!
/*			if (mainForm->edTxOpenedVersion->Text == "")
			{
				memoTestLog->Lines->Add("** CHECK Tx Still OK by Asking for Verison**");
				mainForm->edTxOpenedVersion->Text = "Tx Not Connected?";
			}
			else
			{

			} // End If
*/
		}
		else
		{
			// Stay in Current State
			// WaitTimerTicks = 0.0;  // Ensure Timer is reset DO NOT RESET!!!
			testAdmin.testState = TEST_ENABLE_PSU_STATE;

			lbCountDownState->Caption = "ON Countdown Status";
			lblWaitCountdown->Caption = FloatToStr(WaitTimerTicksMax + 1.0 - WaitTimerTicks);

		} // End If
		//			tmTest->Interval = 	StrToInt(ebPowerUpWait->Text);       // Set the state timer to power On time


		   //	Application->ProcessMessages();


		//tmTest->Enabled = true;


			break;

	   // ````````````````````````````````````````````````````````````````````````````````
		case TEST_GET_DATA_STATE:
			lbErrorStatus->Caption = "STATE -> Test_Get_Data State";

		//	Application->ProcessMessages();


			tmTest->Interval = 	200;  // Set the state timer to default time todo make define

			TestLogAdd("----");
			TestLogAdd("Start New Measurement Test State");

			if (cbSimPlotting->Checked)
			{
				// Do Nothing
			}
			else
			{
				//USBMM->StartNewMeasurement();
				StartMeasUSBMM();
			} // End If

			//Change STATE
			testAdmin.testState = TEST_PROCESS_DATA_STATE;
			break;

	   // ````````````````````````````````````````````````````````````````````````````````
		case TEST_PROCESS_DATA_STATE:
			lbErrorStatus->Caption = "STATE -> Test_Process_Data State";

			Application->ProcessMessages();

			//temporary measurement storage
			MAIN_measurement_t tmpMeas;

			//USBMM->DMM_Task();
			DMM_TaskUSBMM();

			//wait till data is available
			//if(USBMM->DataAvailable())
			if(DataAvaiUSBMM())
			{
				// ***
				// Process DATA
				// ***
				TestLogAdd("STATE -> Test_Process_Data State");
				try
					{
					//process and backup data

					UpdateDAQFormDisplay();


					//ShowMessage("done");

					if (cbDoNotMoveXY->Checked)
					{
						updateEfficiency2();
					}
					else
					{
						updateEfficiency();
					} // Ends If


					tmpMeas = curMeas;
					//display data to user

					// ***
					// Add the Qi Stuff now
					// ***
					tmpMeas.Qi_Signal_Strength = Qi_Signal_Strength;
					tmpMeas.Qi_Coil_Num_1 = Qi_Coil_Num_1;
					tmpMeas.Qi_Coil_Num_2 = Qi_Coil_Num_2;

//					cbCoilStatus->Text = "Final Result" + IntToStr(Qi_Coil_Num_1) + IntToStr(Qi_Coil_Num_2);

					lbQiSignal_strength->Caption = IntToStr(Qi_Signal_Strength);

//				 ShowMessage("Test2");

					memoUSBLog->Lines->Add("******");
					String M, Vin, Iin, Vout, Iout;
					M= "Measurement [" + String(testAdmin.curSetpoint.XPos) + "],[" + String(testAdmin.curSetpoint.YPos) + "]";
					Vin = "V-in [" + FloatToStr((float)tmpMeas.values[INPUT_VOLTAGE_INDEX]) + "]";
					Iin = "I-in [" + FloatToStr((float)tmpMeas.values[INPUT_CURRENT_INDEX]) + "]";
					Vout = "V-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_VOLTAGE_INDEX]) + "]";
					Iout = "I-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_CURRENT_INDEX]) + "]";

					memoUSBLog->Lines->Add(M);
					memoUSBLog->Lines->Add(Vin);
					memoUSBLog->Lines->Add(Iin);
					memoUSBLog->Lines->Add(Vout);
					memoUSBLog->Lines->Add(Iout);

					memoUSBLog->Lines->Add("*******");

/*					memoUSBLog->Lines->Add("Measurement [" + String(testAdmin.curSetpoint.XPos) + "],[" + String(testAdmin.curSetpoint.YPos) + "]");
					memoUSBLog->Lines->Add("V-in [" + FloatToStr((float)tmpMeas.values[INPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + "]");
					memoUSBLog->Lines->Add("I-in [" + FloatToStr((float)tmpMeas.values[INPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + "]");
					memoUSBLog->Lines->Add("V-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + "]");
					memoUSBLog->Lines->Add("I-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + "]");
*/
				   } catch (...)
				   {
						TestLogAdd("ERROR in Data available State");
//				   		ShowMessage(FloatToStr(tmpMeas.values[INPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS));
//				   		ShowMessage(FloatToStr(tmpMeas.values[INPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS));
//				   		ShowMessage(FloatToStr(tmpMeas.values[OUTPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS));
//				   		ShowMessage(FloatToStr(tmpMeas.values[OUTPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS));
				   }  // End Try

				   // If Retry set
				   if (cbHoleRetry->Checked)
				   {
					   // ***
					   // After data updated can reset Rescan
					   // ***
						Re_Scan_Count =  StrToInt(strRetryCountSet); // Add one to allow for 0 retry
				   }
				   else
				   {
						// Do Nothing
				   } // End If

				   // ***
				   // Stop Scanning
				   // ***
//					tmTest->Enabled = false;
//					while(!StartScanPowerOFFMain())
//					{
//						//Wait
//					}
//					tmTest->Enabled = true;

				  // ***
				  // Disable PSU
				  // ***
				  if (cbEnablePSUControl->Checked)
				  {
//					ShowMessage("PSU Control Auto ON/OFF");
					btDisablePSUClick(this);
#ifndef USB_PSU
					tmPlotter_NRT->Enabled = true;
					testAdmin.NRT_Goto = true;
					testAdmin.NRTState =TEST_PROCESS_DATA_STATE;
#endif

				  }
				  else
				  {
//					ShowMessage("PSU Control Manual Control");
					//Do nothing
				  } // End If

					// ***
					// SAVE DATA
					// ***
					for (int i = 0; i < 1; i++)
					{
						if (WriteToCSV(testAdmin.testFile, GenerateCSVLine(tmpMeas)))
						{
						   tmTest->Enabled = true;
							break;
						}
						else
						{
						   tmTest->Enabled = false;
						   ShowMessage("Retry CSV" + IntToStr(i) + " of 1 max");
						} // End If
					} // End For

					// ***
					// ENDS SAVE DATA
					// ***

					//if last step has been taken already, stop test
					if(	testAdmin.ySteps == StrToInt(ebYSteps2->Text) &&
						testAdmin.xSteps == StrToInt(ebXSteps2->Text)   )
					{
						TestLogAdd("Reached Test Steps Max -> Y Steps= " + ebYSteps2->Text + " X Steps =  " + ebXSteps2->Text);

					   //*******************************
					   // DEBUG AREA

						int count = StrToInt(ebRepeat->Text);
						if(count != 0 )
							{
							 count--;
							 ebRepeat->Text= IntToStr(count);
							}
						if (count)
							{
							btStartTestClick(this);
							}
						else
							{
							btStopTestClick(this);

							TestLogAdd("Test Finished");
							ShowMessage("Test Finished");
							 }
						//For LOOP Testing!!!!!!!!!!
//						btStartTestClick(this);
//						TestLogAdd("Loop Test*********");
					   //*******************************

					}
					else
					{

						if (cbDoNotMoveXY->Checked)
						{
							// Do Not Move Plotter
							//add log entry to let user validate positions..
							TestLogAdd("----");
							TestLogAdd("Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
									+ String(testAdmin.curSetpoint.YPos) + "]");
							//if plotter has reached destination
							WaitOffTimerTicks = 0.0;
							testAdmin.testState = TEST_MOVE_STATE;
						}
						else
						{
							// ***
							// Move to Next Position
							// ***
							//goto new setpoint
							//Prepare next iteration or stop
							NextSetpoint();

							Plotter->UpdateSetpoint(testAdmin.curSetpoint);
							tmPlotter_NRT->Enabled = true;
							testAdmin.NRT_Goto = true;
							testAdmin.NRTState =TEST_MOVE_NRT;

							//add log entry to let user validate positions..
							TestLogAdd("----");
							TestLogAdd("Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
									+ String(testAdmin.curSetpoint.YPos) + "]");
							//if plotter has reached destination
							WaitOffTimerTicks = 0.0;
							testAdmin.testState = TEST_MOVE_STATE;
						} // End If

						if (cbDoNotMoveXY->Checked)
						{
							// Do Not Move Plotter
						}
						else
						{
							// Move to Next Position


						} // End If
					}  // End If

			// ***
			// ENDS Process DATA
			// ***



			}
			else
			{

				if (cbSimPlotting->Checked)
				{
				  // ***
				  // SIMULATION
				  // ***

					if (cbDoNotMoveXY->Checked)
					{
						updateEfficiency2();
					}
					else
					{
						updateEfficiency();
					} // Ends If

					tmpMeas = curMeas;
					//display data to user

					// ***
					// Update Grid   lbEff
					// ***
					// Check Plot Direction
/*					if (testAdmin.sign == 1)
					{
						//+ Direction
						StringGrid1->Cells[1 + testAdmin.xSteps][(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps] = 0;
					}
					else
					{
						//- Direction
						StringGrid1->Cells[(StrToInt(ebXSteps->Text) + 2) - (1 + testAdmin.xSteps)][(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps] = 0;
					} // End If
*/

		//			StringGrid1->Cells[testAdmin.curSetpoint.XPos][testAdmin.curSetpoint.YPos] = 0;

				  // ***
				  // Disable PSU
				  // ***
				  if (cbEnablePSUControl->Checked)
				  {
					// ShowMessage("PSU Control Auto ON/OFF");
					btDisablePSUClick(this);
					#ifndef USB_PSU
					tmPlotter_NRT->Enabled = true;
					testAdmin.NRT_Goto = true;
					testAdmin.NRTState =TEST_PROCESS_DATA_STATE;
					#endif

				  }
				  else
				  {
					//Do nothing
				  } // End If

					// ***
					// Store DATA
					// ***
//					WriteToCSV(testAdmin.testFile, GenerateCSVLine(tmpMeas));
					for (int i = 0; i < 1; i++)
					{
						if (WriteToCSV(testAdmin.testFile, GenerateCSVLine(tmpMeas)))
						{
						   tmTest->Enabled = true;
							break;
						}
						else
						{
						   tmTest->Enabled = false;
						   ShowMessage("Retry CSV" + IntToStr(i) + " of 1 max");
						} // End If
					} // End For

					//if last step has been taken already, stop test
					if(	testAdmin.ySteps == StrToInt(ebYSteps2->Text) &&
						testAdmin.xSteps == StrToInt(ebXSteps2->Text)   )
					{

						TestLogAdd("Reached Test Steps Max -> Y Steps= " + ebYSteps2->Text + " X Steps =  " + ebXSteps2->Text);

					   //*******************************
					   // DEBUG AREA
						int count = StrToInt(ebRepeat->Text);
						if(count != 0 )
							{
							 count--;
							 ebRepeat->Text= IntToStr(count);
							}
						if (count)
							{
							btStartTestClick(this);
							}
						else
							{
							btStopTestClick(this);

							TestLogAdd("Test Finished");
							ShowMessage("Test Finished");
							 }
						//For LOOP Testing!!!!!!!!!!
						//btStartTestClick(this);
						//TestLogAdd("Loop Test*********");
					   //*******************************

					}
					else
					{

						if (cbDoNotMoveXY->Checked)
						{
							// Do Not Move Plotter
							//add log entry to let user validate positions..
							TestLogAdd("----");
							TestLogAdd("Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
									+ String(testAdmin.curSetpoint.YPos) + "]");
							//if plotter has reached destination

							// CHANGE STATE
							WaitOffTimerTicks = 0.0;
							testAdmin.testState = TEST_MOVE_STATE;
							// ENDS CHANGE STATE
						}
						else
						{
							// Move to Next Position
							//Prepare next iteration or stop

//                  			Series1->Clear();

							// ***
							// Go to New Set Point
							// ***
							NextSetpoint();

							Plotter->UpdateSetpoint(testAdmin.curSetpoint);
							tmPlotter_NRT->Enabled = true;
							testAdmin.NRT_Goto = true;
							// CHANGE STATE
							testAdmin.NRTState =TEST_MOVE_NRT;
							// ENDS CHANGE STATE

							//add log entry to let user validate positions..
							TestLogAdd("----");
							TestLogAdd("Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
									+ String(testAdmin.curSetpoint.YPos) + "]");
							//if plotter has reached destination

							// CHANGE STATE
							WaitOffTimerTicks = 0.0;
							testAdmin.testState = TEST_MOVE_STATE;
							// ENDS CHANGE STATE

						} // End If

					}  // End If

				  // ***
				  // ENDS SIMULATION
				  // ***

				}
				else
				{
					//*************************
					// Handle comms failure here to stop "apparent lock up" loop
					TestLogAdd("USB DAQ Data NOT available");
					mainForm->lbControlMessage->Caption = "WARNING: UAB DAQ NOT connected (Click to Clear)";
					//*************************
				}  // End If

			} // End If



			break;

	   // ````````````````````````````````````````````````````````````````````````````````
		case TEST_PAUSE_STATE:
			lbErrorStatus->Caption = "STATE -> Test_pause_state";
			// do nothing.  The button click will bring us out of the pause state
			break;

	   // ````````````````````````````````````````````````````````````````````````````````
		default:
			//something weird happened, stop test.
			lbErrorStatus->Caption = "STATE -> Undefined State";
			TestLogAdd("Oops : invalid state");
			btStopTestClick(this);

	}  // End Switch

}
catch (...)
//catch (Exception &E)
{
  //	ShowMessage("Error in State Machine -> " & E.Message);
//	TestLogAdd("Oops : invalid state");

	lbErrorStatus->Caption = "Error in State Machine";

//	ShowMessage(testAdmin.testState);

}
//return ;

}






















//---------------------------------------------------------------------------
void __fastcall TmainForm::EffTest_TEST_INIT_STATE()
{
			//Indicate start of test
			//lbErrorStatus->Caption = "STATE -> Test_Init State";

			Series1->Clear();

			WaitTimerTicks = 0;
			WaitOffTimerTicks = 0;

			WaitTimerTicksMax = StrToFloat(ebPowerUpWait2->Text)/200;
			WaitOffTimerTicksMax = StrToFloat(ebOffTime2->Text)/200;

			TestLogAdd("Starting Test : " + ebXSteps2->Text + "," + ebYSteps2->Text + "," + ebStepSize2->Text);
			//disable PSU
			btDisablePSUClick(this);

#ifndef USB_PSU
			tmPlotter_NRT->Enabled = true;
			testAdmin.NRT_Goto = true;
			testAdmin.NRTState =TEST_INIT_STATE;
#endif

		if (cbDoNotMoveXY->Checked)
		{
			// Do Not Move
			ResetTestCoordinates2();
			WaitOffTimerTicks = 0.0;
			testAdmin.testState = TEST_MOVE_STATE;

		}
		else
		{
			//initialize test
			ResetTestCoordinates();
			testAdmin.curIteration = 0;

			strRetryCountSet = ebHoleRetryNo->Text;

			//Set Initial Setpoint
			Plotter->UpdateSetpoint(testAdmin.curSetpoint);
			tmPlotter_NRT->Enabled = true;
			testAdmin.NRT_Goto = true;

			testAdmin.NRTState =TEST_MOVE_NRT;

			//add log entry to let user validate positions..
			TestLogAdd("Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
						+ String(testAdmin.curSetpoint.YPos) + "]");

			//if plotter has reached destination
			WaitOffTimerTicks = 0.0;
			testAdmin.testState = TEST_MOVE_STATE;
			//start measurement equipment

			//USBMM->Start();
			StartUSBMM();

			btOpenUSB->Enabled = false;
			btCloseUSB->Enabled = true;
			// Init State
			TxFlgRunning = false;
			TxReScan = false;

			// Init Re_Scan Count
			Re_Scan_Count =  StrToInt(strRetryCountSet); // Add one to allow for 0 retry
		//	ShowMessage("Scan count reset" + IntToStr(Re_Scan_Count));

		}  // End If
}

//---------------------------------------------------------------------------
// Main test-application state machine
//---------------------------------------------------------------------------
void __fastcall TmainForm::TestStateMachine2()
{

/*
static float WaitTimerTicks;
static float WaitTimerTicksMax;

static float WaitOffTimerTicks;
static float WaitOffTimerTicksMax;
*/
int pCoilStatus[10][2];
// ScanAndPowerON

static bool TxPollDone;

static int NumberOfCoilPowered;
static int CoilNumberPowered[10];

try
{

	//============================================================================================
	//***
	// Eff Plot TEST CODE
	//***
	if (SeqTestRunning)
	{
		// Run Test Cases
		tmTest->Enabled = false;

		lbStatusGridSeq->Caption = "Run Seq Step Number ->" + IntToStr(SeqGridRowNumber);
		// Form Status Ind
		ebRepeat->Text= IntToStr(StrToInt(ebRepeat->Text) + 1);

		ShowMessage("Run Plot Current \nCurrent Seq Grid Row = " + IntToStr(SeqGridStepNumber));

/*		// Check if repeated Plotting
		if (SeqRepeatCount > 0)
		{
			// REPEAT Plot
			// Form Status Ind
			lbStatusGridSeq->Caption = "Run Seq Step Number ->" + IntToStr(SeqRowRunCounter);
			// Form Status Ind
			ebRepeat->Text= IntToStr(StrToInt(ebRepeat->Text) + 1);

			ShowMessage("Run Plot Current \nCurrent Seq Grid Row = " + IntToStr(SeqStepCounter));
		}
		else
		{
			// NO REPEAT Plot
			// Form Status Ind
			lbStatusGridSeq->Caption = "Run Seq Step Number ->" + IntToStr(SeqRowRunCounter - 1);
			// Form Status Ind
			ebRepeat->Text= IntToStr(StrToInt(ebRepeat->Text) + 1);

			ShowMessage("Run Plot Current \nCurrent Seq Grid Row = " + IntToStr(SeqStepCounter - 1));

		} // End If
*/

	//	ShowMessage("Run Plot Current \nCurrent Seq Grid Row = " + IntToStr(SeqStepCounter));

//		ebRepeat->Text= IntToStr(SeqRepeatCount);

		SeqTestRunning = false;
		tmTest->Enabled = true;
	}
	else
	{
		// Make sure timer is OFF when no Test Running
		tmTest->Enabled = true;
	} // End If
	//***
	// Ends Eff Plot TEST CODE
	//***
	//============================================================================================

	switch (testAdmin.testState)
	{
	   // 1 ````````````````````````````````````````````````````````````````````````````````
	   case TEST_IDLE_STATE:
			lbErrorStatus->Caption = "STATE -> Test_Idle State";

			//Change STATE

			break;

	   // 2 ````````````````````````````````````````````````````````````````````````````````
		case TEST_INIT_STATE:
			//Indicate start of test
			lbErrorStatus->Caption = "STATE -> Test_Init State";

			SetSeqData2Form(SeqGridStepNumber);

			//EffTest_TEST_INIT_STATE();

			//Change STATE
			testAdmin.testState = TEST_MOVE_STATE;
			break;

	   // 3 ````````````````````````````````````````````````````````````````````````````````
		case TEST_MOVE_NRT:
			//Indicate start of test
			lbErrorStatus->Caption = "STATE -> Test_Move_NRT";


			//Change STATE
			testAdmin.testState = TEST_MOVE_STATE;
			break;

	   // 4 ````````````````````````````````````````````````````````````````````````````````
		case TEST_MOVE_STATE:
			//Indicate start of test
			lbErrorStatus->Caption = "STATE -> Test_Move State";

			//Change STATE
		break;

	   // 5 ````````````````````````````````````````````````````````````````````````````````
		case TEST_ENABLE_PSU_STATE:
			//Indicate start of test
			lbErrorStatus->Caption = "STATE -> Test_Enable_PSU State";


			//Change STATE
			testAdmin.NRTState =TEST_ENABLE_PSU_STATE;

			break;

	   // 6 ````````````````````````````````````````````````````````````````````````````````
		case TEST_GET_DATA_STATE:
			//Indicate start of test
			lbErrorStatus->Caption = "STATE -> Test_Get_Data State";



			//Change STATE
			testAdmin.testState = TEST_PROCESS_DATA_STATE;
			break;

	   // 7 ````````````````````````````````````````````````````````````````````````````````
		case TEST_PROCESS_DATA_STATE:
			//Indicate start of test
			lbErrorStatus->Caption = "STATE -> Test_Process_Data State";

			Application->ProcessMessages();


			//Change STATE
			break;

	   // 8 ````````````````````````````````````````````````````````````````````````````````
		case TEST_PAUSE_STATE:
			//Indicate start of test
			lbErrorStatus->Caption = "STATE -> Test_pause_state";
			// do nothing.  The button click will bring us out of the pause state
			//Change STATE
			break;

	   // 9 ````````````````````````````````````````````````````````````````````````````````
		default:
			//something weird happened, stop test.
			//Indicate start of test
			lbErrorStatus->Caption = "STATE -> Undefined State";
			TestLogAdd("Oops : invalid state");
			btStopTestClick(this);

	}  // End Switch

}
catch (...)
{
	lbErrorStatus->Caption = "Error in State Machine";
//	ShowMessage(testAdmin.testState);

}

} // End Function

//---------------------------------------------------------------------------
// Main test-application state machine
//---------------------------------------------------------------------------
void __fastcall TmainForm::tmTestTimer(TObject *Sender)
{
	//TestStateMachine1(); // For 2D Plotter

	TestStateMachine2();  // For 3D Plotter
}

//---------------------------------------------------------------------------
// Tell plotter to request new position,
//---------------------------------------------------------------------------
void __fastcall TmainForm::tmPlotterTimer(TObject *Sender)
{

//	TestLogAdd("Plotter Timer Fired****");

	//update plotter connection indicator
	shPlotterConnected->Brush->Color = (Plotter->UpdateConnectionState()) ? clLime :clSilver;
	//get relay connected state
	if(Plotter->relayState == true){
		btRelayOn->Enabled = false;
		btRelayOff->Enabled = true;
	} else {
		btRelayOn->Enabled = true;
		btRelayOff->Enabled = false;
	}
	//instruct plotter to process any received comms
//	Plotter->ProcessRXData();
	//instruct plotter to retreive new data, and print latest

//	if (cbSimPlotting->Checked)
//	{
		// Do Nothing
//	}
//	else
//	{
		Plotter->RequestDistance();
		//update plotter state to latest known
		testAdmin.curDistance = Plotter->GetCurDistance();
		updatePosLabels(this);

//	} // End If

}


//---------------------------------------------------------------------------
// Store Callibration settings when edited
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebVinMultiplierChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Multipliers", "Vin", ebVinMultiplier->Text);
	TestLogAdd("New [Vin *] : " + ebVinMultiplier->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebIinMultiplierChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Multipliers", "Iin", ebIinMultiplier->Text);
	TestLogAdd("New [Iin *] : " + ebIinMultiplier->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebVoutMultiplierChange(TObject *Sender)
{
   Reg_WriteString(sREG, "Multipliers", "Vout", ebVoutMultiplier->Text);
   TestLogAdd("New [Vout *] : " + ebVoutMultiplier->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebIoutMultiplierChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Multipliers", "Iout", ebIoutMultiplier->Text);
	TestLogAdd("New [Iout *] : " + ebIoutMultiplier->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebVinOffsetChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Offsets", "Vin", ebVinOffset->Text);
	TestLogAdd("New [Vin +] : " + ebVinOffset->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebIinOffsetChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Offsets", "Iin", ebIinOffset->Text);
	TestLogAdd("New [Iin +] : " + ebIinOffset->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebVoutOffsetChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Offsets", "Vout", ebVoutOffset->Text);
	TestLogAdd("New [Vout +] : " + ebVoutOffset->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebIoutOffsetChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Offsets", "Iout", ebIoutOffset->Text);
	TestLogAdd("New [Iout +] : " + ebIoutOffset->Text);
}
//---------------------------------------------------------------------------


void __fastcall TmainForm::btTXOpenCommsClick(TObject *Sender)
{
	// Wait in case just powered Tx
	Sleep(10);

/* COMMENT OUT **********************

	//acquire the required comports
	TXComPortNo = UserSelectComport2("Select TX Com-Port", "PortTX", 115200);

	// if com-port could not be opened close application

	if( TXComPortNo == 0 )
	{
		// ***
		// NO Comport
		// ***

		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
		shpTxConnected->Brush->Color = clSilver;

	}
	else
	{
		// ***
		// Comport OK
		// ***

	//		sdFlushPort(TXComPort);
		btTXOpenComms->Enabled = false;
		btTXCloseComms->Enabled = true;
		//start comms timer

//		TXComPort_Rx_Thread = new Rx_Thread(TXComPort,RX_NODE_TX_COMPORT );


		edTxComportNumber->Text = IntToStr(TXComPortNo);

		shpTxConnected->Brush->Color = clLime;
	}  // End If
/*

	 // ShowMessage(IntToStr(SelDialog->ComNumber));

************************ */
  /*
 OpenTxPort(TXComPortNo);
// OpenTxPort(TXComPortNo);

 AnsiString TxVers = GetTxVersion();
 mainForm->edTxOpenedVersion->Text = TxVers;

//	 ShowMessage("Version = " + xx);

CloseTxPort();

*/
/*
// FOR COMMS TESTING ***************************
for (int i = 0; i < 10000; i++) {

 OpenTxPort(TXComPortNo);

 Sleep(100);
// AnsiString TxVers = GetTxVersion();
//  TXCommMemo->Lines->Add(IntToStr(i) + ". " + TxVers);
  TXCommMemo->Lines->Add(IntToStr(i) + ". ");

CloseTxPort();

 Sleep(100);
}
// ***************************
*/

			   //28/7/15
//acquire the required comports
	UserSelectComport(Qi_Comport,"Select TX Com-Port", "PortTX", 115200);

	edTxComportNumber->Text = IntToStr(Qi_Comport->ComNumber);
//	edTxVersion->Text = edTxComportNumber->Text;

/*
// *****************************
	Qi_Comport->FlushInBuffer();
	Qi_Comport->FlushOutBuffer();
	Qi_Comport->Open = false;


OpenTxPort(StrToInt(edTxComportNumber->Text));
// OpenTxPort(TXComPortNo);

 AnsiString TxVers = GetTxVersion();
 mainForm->edTxOpenedVersion->Text = TxVers;

//	 ShowMessage("Version = " + xx);

CloseTxPort();

		Qi_Comport->Open = true;
// *****************************
*/

	// if com-port could not be opened close application
	if( Qi_Comport->Open == false )
	{
		// Error
		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
		TXCommMemo->Lines->Add("Tx Comport Error");
		mainForm->btScanPower->Enabled = false;
		mainForm->cbModDepth->Enabled = false;

	}
	else
	{
		Qi_Comport->FlushInBuffer();
		Qi_Comport->FlushOutBuffer();
		btTXOpenComms->Enabled = false;
		btTXCloseComms->Enabled = true;

		TXCommMemo->Lines->Add("Tx Comport Opened");

		//start comms timer

		// Get Version
	//		SendGetVersion();
		Tx_Init_State = Tx_Init_CheckTx;
	//	Tx_Init_State = Tx_Init_Manual_Mode;

		// ***
		// Enable Tx comms timer
		// ***
		tmInit_SM->Enabled = true;
		// ShowMessage("Scan Timer Tx Open Enabled");

		Sleep(10);

		mainForm->btScanPower->Enabled = true;
		mainForm->cbModDepth->Enabled = true;

		cbManual_Mode->Checked = true;

	} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btTXCloseCommsClick(TObject *Sender)
{

//	CloseTxPort();

	mainForm->edTxOpenedVersion->Text = "Tx Version";


	shpTxConnected->Brush->Color = clSilver;
/* ********************************************************
	//stop timer
	sdFlushPort(TXComPort);
	sdClosePort(TXComPort);

	TXComPort_Rx_Thread->FreeOnTerminate = false;
	TXComPort_Rx_Thread->Terminate();
	TXComPort_Rx_Thread->WaitFor() ;

	delete TXComPort_Rx_Thread;
//set buttons
	btTXOpenComms->Enabled = true;
	btTXCloseComms->Enabled = false;

************************************* */
//	sdFlushPort(TXComPort);
//	sdClosePort(TXComPort);

//stop timer
	if (Qi_Comport->Open == true)
	{
		Qi_Comport->FlushInBuffer();
		Qi_Comport->FlushOutBuffer();
		Qi_Comport->Open = false;
	}
	else
	{
		// Do Nothing
	} // End If

	//set buttons

	btTXOpenComms->Enabled = true;
	btTXCloseComms->Enabled = false;

	btScanPower->Enabled = false;

	mainForm->cbModDepth->Enabled = false;

}
//---------------------------------------------------------------------------



// ---------------------------------------------------------------------------
//void __fastcall TmainForm::Qi_ComportRxChar(int Count, uint8_t * pData)
void __fastcall TmainForm::Qi_ComportTriggerAvail(TObject *CP, WORD Count)
{
enum states {
			WAIT_SOM, SEQ, HEADER, DATA, CHECKSUM, DONE
		};

static char RxBuffer[400];
static int State = WAIT_SOM;
char input;
static char LRC;
static int i;
static int Ri_Ptr;

while (Count != 0)
	{
	if (Recived_Message == false)
		{
		input = Qi_Comport->GetChar();

		if (input == STX)	//if STX found in data resync
			{
			State = WAIT_SOM;
			}

		switch (State)
			{
			case WAIT_SOM:
				if (input == STX)
					{
					Ri_Ptr = 0;
					memset(RxBuffer,0xFF,sizeof(RxBuffer));
					RxBuffer[Ri_Ptr++] = input;
					LRC = 0;
					State = SEQ;
					}
				break;

			case SEQ:
				RxBuffer[Ri_Ptr++] = input;
				State = DATA;
				break;

			case DATA:
				if (input != ETX)
					{
					if (Ri_Ptr < (sizeof(RxBuffer)-2))
						{
						RxBuffer[Ri_Ptr++] = input;
						}
					else
						{  //buffer overflow, Start looking for STX again
						State = WAIT_SOM;
						TestLogAdd("Overflow Error");
						break;
						}
					}
				else
					{
					// got message do check sum
					for (i = 1; i <= Ri_Ptr - 2; ++i)
						{
						LRC += RxBuffer[i];
						}

					LRC = ((LRC + ((LRC & 0xC0) >> 6)) & 0x3F) + 0x20;
					if (LRC == RxBuffer[Ri_Ptr - 1])  // Checksum ok. Copy to working RxBuffer.
						{
						RxBuffer[Ri_Ptr - 1] = 0;
						// (* Null terminated *)

						Recived_Message = TRUE;
						memcpy(RiBuf, RxBuffer, sizeof(RxBuffer));

						PostMessage(mainForm->Handle, UM_RxMESSAGE, 0, 0);
						AnsiString StartTxMag;
						StartTxMag = AnsiString(RxBuffer[1]) + AnsiString(RxBuffer[2]);
						if (StartTxMag == "1Q")
						{
							TXCommMemo->Lines->Add("---------");
						}
						else
						{
							// Do Nothing
						} // End If



						Application->ProcessMessages();
						}
					else
						{
						TestLogAdd("Checksum Error");
						}
					State = WAIT_SOM;
					}
				break;

				} // end of case
			Count--;
			} // end of (Recived_Message == false)
		Application->ProcessMessages();
		}
}

// ---------------------------------------------------------------------------

void __fastcall TmainForm::RxMessage( TMessage & Message )
	{

	if ( Message.Msg == UM_RxMESSAGE )
		{
		if ( Recived_Message )
			{
			ProcessRxMessage( );
			Recived_Message = FALSE;
			}
		}
	}

// ---------------------------------------------------------------------------
void __fastcall TmainForm::ProcessRxMessage( void )
	{
	unsigned char i;
	unsigned char This_Seq = RiBuf[ SEQ ];

	AnsiString sNum;
	char * Ptr;

	Ptr = &RiBuf[ DATA ];

	if (NackCount == 1)
	{
		// Rec msg Start
		TXCommMemo->Lines->Add("---------");
	}
	else
	{
		// Do Nothing
	} // End If

	TXCommMemo->Lines->Add( RiBuf );
	logit("Log.txt","--coms--" + AnsiString(RiBuf));

	switch ( RiBuf[ HDR ] ) // switch header
		{

	case 'Q':
		if ( ( This_Seq != RxSeqNum ) || ( This_Seq == '0' ) )
			{
			RxSeqNum = This_Seq;
			// do stuff here
			OutPutQ( AnsiString( & RiBuf[ DATA ] ) );
			}
		break;

	case 'n':

		Waiting_On_Manual_Mode = false;
		if ( ( This_Seq == TxSeqNum ) && ( Tx_State == WAITING_FOR_ACK ) )
			{
			Tx_State = TX_BUFF_EMPTY;
			}
		else
			{
			NackCount++ ;
			Tx_State = SENDING_DATA;
			StartTransmitMessage( ); // resend message
			}
		break;

	case 'j': // Do board scan
		if ( ( This_Seq == TxSeqNum ) && ( Tx_State == WAITING_FOR_ACK ) )
			{
			Tx_State = TX_BUFF_EMPTY;
			}
		else
			{
			NackCount++ ;
			Tx_State = SENDING_DATA;
			StartTransmitMessage( ); // resend message
			}
		break;

		//28/7/15
	case 'k': // Scan and power
		Waiting_On_Scan_n_Power = false;
		if ( ( This_Seq == TxSeqNum ) && ( Tx_State == WAITING_FOR_ACK ) )
			{
			Tx_State = TX_BUFF_EMPTY;
			}
		else
			{
			NackCount++ ;
			Tx_State = SENDING_DATA;
			StartTransmitMessage( ); // resend message
			}
		break;


	case 'p':
		Waiting_On_Q_Message_Enable = false;
		if ( ( This_Seq == TxSeqNum ) && ( Tx_State == WAITING_FOR_ACK ) )
			{
			Tx_State = TX_BUFF_EMPTY;
			}
		else
			{
			NackCount++ ;
			Tx_State = SENDING_DATA;
			StartTransmitMessage( ); // resend message
			}
		break;

	case 'v':

		int iAPI_Version;
		int iDevice_Type;
		Waiting_On_Version = false;

		if ( ( This_Seq == TxSeqNum ) && ( Tx_State == WAITING_FOR_ACK ) )
			{
			Tx_State = TX_BUFF_EMPTY;

			try
				{
				sNum = AnsiString( Ptr, 2 );
				Ptr += 2;
				iAPI_Version = StrToInt( "0x" + sNum );

				sNum = AnsiString( Ptr, 2 );
				Ptr += 2;
				iDevice_Type = StrToInt( "0x" + sNum );

				}
			catch ( const EConvertError & error )
				{
				ShowMessage( "Return Parameter Error: Not a valid hex number" );
				mainForm->edTxModDepthView->Text = "";
				break;
				}


//			edTxOpenedVersion->Caption = "API Version: " + IntToStr(iAPI_Version);
//			edTxOpenedVersion->Text = AnsiString( Ptr);
			 mainForm->edTxOpenedVersion->Text = AnsiString( Ptr); //IntToStr(iAPI_Version);
			 mainForm->edTxVersion->Text = mainForm->edTxOpenedVersion->Text;

/*			if(iAPI_Version != API_VERSION)
				{
				ShowMessage( "Error: API Version mismatch. Please update" );
				}

			if(iDevice_Type != 0x01)
				{
				ShowMessage( "Error: You are not conected to a Tx Device. Please check your comport settings" );
				}
*/

			}
		else
			{
			NackCount++ ;
			Tx_State = SENDING_DATA;
			StartTransmitMessage( ); // resend message
			}

		break;


	case 'w':  // Update FSK pars ie Mode Depth etc
		if ( ( This_Seq == TxSeqNum ) && ( Tx_State == WAITING_FOR_ACK ) )
			{
			Tx_State = TX_BUFF_EMPTY;
			}
		else
			{
			NackCount++ ;
			Tx_State = SENDING_DATA;
			StartTransmitMessage( ); // resend message
			}
		break;


		} // end of switch
	}

// ---------------------------------------------------------------------------

void __fastcall TmainForm::OutPutQ( AnsiString sOutput )
	{


	AnsiString sTemp;
	int16_t iTemp;
	double dTemp, dTemp2;
	unsigned char TempBuffer[ 255 ];
	AnsiString sNum;
	char * Ptr;
	int LoopCount;

	char Flag = sOutput[ 1 ];

	AnsiString sOut = sOutput.SubString( 2, sOutput.Length( ) - 1 );

	memcpy( TempBuffer, sOut.c_str( ), sOut.Length( ) );
	Ptr = TempBuffer;


	if ( Flag == '~' )
		{// the Tx was reset.  Lets clear the GUI
		Reset_GUI();
		}

	if ( Flag == 'G' )
		{ // Qi message
		qiMsg_t message;
		// first byte says how many bytes will follow.
		sNum = AnsiString( Ptr, 2 );
		Ptr += 2;
		message.num_bytes = StrToInt( "0x" + sNum );

		if ( message.num_bytes > 0 ) // is there a header?  no error?
			{
			sNum = AnsiString( Ptr, 2 );
			Ptr += 2;
			message.header = StrToInt( "0x" + sNum );
			}

		if ( message.num_bytes > 2 ) // is there data?
			{
			uint8_t i;
			for ( i = 0; i < message.num_bytes - 2;
			i++ ) // go through all data bytes (not header or checksum)
				{
				sNum = AnsiString( Ptr, 2 );
				Ptr += 2;
				message.data[ i ] = StrToInt( "0x" + sNum );
				}
			}

		if ( message.num_bytes > 1 ) // is there a checksum?
			{
			sNum = AnsiString( Ptr, 2 );
			Ptr += 2;
			message.checksum = StrToInt( "0x" + sNum );
			}

		message.received_time = message.received_time.CurrentTime( );
		// store the time when this was received.

		message.config.configured = 0;
		// flag that this message does not have an associated Qi configuration.
		message.checked_for_errors = 0; // not yet checked for errors.
		message.errors = 0; // no errors yet

		message.isStatus = 0; // not a status update but a packet

		//qiViewer->Add_Qi_Message( message ); // add the message to the display

		if (message.header == QI_H_SIG_STRENGTH )
			{
			//Qi_Signal_Strength = message.data[0];
			//lbQiSignal_strength->Caption = IntToStr(message.data[0]);
			}
		}

	if ( Flag == 'A' )
		{
		unsigned int Index, Status, Result;
		bool success = true;

		try
			{
			sNum = AnsiString( Ptr, 2 ); // index
			Ptr += 2;
			Index = StrToInt( "0x" + sNum );
			sNum = AnsiString( Ptr, 2 ); // status
			Ptr += 2;
			Status = StrToInt( "0x" + sNum );
			sNum = AnsiString( Ptr, 2); // result
			Ptr += 2;
			Result = StrToInt( "0x" + sNum );
			}
		catch ( const EConvertError & error )
			{
			success = false;
			}

		if ( success )
			{

			if (Status == TCS_Powered)
				{
				lbQiSignal_strength->Caption =  IntToStr((int)Result);
				Qi_Signal_Strength = Result;

				if (First_Qi_Coil)
					{
					Qi_Coil_Num_1 = Index +1;
					First_Qi_Coil = false;
					}
				else
					{
					Qi_Coil_Num_2 = Index +1;
					}
				}
			}
		}

}

// ---------------------------------------------------------------------------


void __fastcall TmainForm::btRelayOnClick(TObject *Sender)
{
	//send relay command

	if (cbPlotterType2->ItemIndex == 0)
	{
		Plotter->enRefRelay(true);
	}
	else
	{
		enRefRelay3D(true);
	} // End If

	//set button state accordingly
	btRelayOn->Enabled = false;
	btRelayOff->Enabled = true;
	Plotter->relayState = true;

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btRelayOffClick(TObject *Sender)
{
	//send relay command
	if (cbPlotterType2->ItemIndex == 0)
	{
		Plotter->enRefRelay(false);
	}
	else
	{
		enRefRelay3D(true);
	} // End If

	//set button state accordingly
	btRelayOn->Enabled = true;
	btRelayOff->Enabled = false;
	Plotter->relayState = false;
}

//---------------------------------------------------------------------------

void __fastcall TmainForm::btnPauseClick(TObject *Sender)
{
// save all parameters and enter pause state
   if (btnPause->Caption == "PAUSED")
   {
	btnPause->Caption = "Pause" ;
	//enable or go to the previous state and previous set parameters.
	PauseState = false;
	testAdmin.testState = testAdmin.LastState;  //restore the last state
   }
   else
   {
	btnPause->Caption = "PAUSED" ;
	testAdmin.LastState = testAdmin.testState;  // save the current state so we can restore it later
	testAdmin.testState = TEST_PAUSE_STATE;
	PauseState = true;
//	ShowMessage("Paused");
   }  // End If
}
//---------------------------------------------------------------------------


void __fastcall TmainForm::CloseProgram1Click(TObject *Sender)
{

	//USBMM->Stop();
   	StopUSBMM();

	tmTest->Enabled = false;
	mainForm->Close();
}
//---------------------------------------------------------------------------



void __fastcall TmainForm::cbEnablePSUControlClick(TObject *Sender)
{
/*	if (cbEnablePSUControl->Checked == true")
	{
		cbEnablePSUControl->Caption = "Enable PSU ON/OFF";
	}
	else
	{
		cbEnablePSUControl->Caption = "NO";
	} //End If

*/
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::tmInit_SMTimer(TObject *Sender)
{

//	TestLogAdd("tmInit_SMTimer Timer Fired****");

  switch (Tx_Init_State)
  {
	case Tx_Init_Idle: //nothing to do
		tmInit_SM->Enabled = false;

//	case Tx_Init_Version: //Ask for Version
//		Waiting_On_Version = true;
//		btVersionClick(this);
//		Tx_Init_State = Tx_Init_Version_Wait;
//	  break;
//
//	case Tx_Init_Version_Wait: //waiting for Version
//		if (Waiting_On_Version == false)
//			{
//			Tx_Init_State = Tx_Init_Manual_Mode;
//			}
//	  break;
//

	case Tx_Init_ModDepth_Mode: //Set Run Mode
		Waiting_On_Manual_Mode = true;

//			ShowMessage("START Mod Depth");
//			TestLogAdd("----");
		  //	TestLogAdd("Set Modulation Depth");
			SetModDepth();
	   //		ShowMessage("Mod Depth");

		Tx_Init_State = Tx_Init_Manual_Mode;
	  break;


	case Tx_Init_Manual_Mode: //Set Run Mode
		Waiting_On_Manual_Mode = true;

		SetTxOpMode();
	   //	cbManual_ModeClick(this);

		Tx_Init_State = Tx_Init_Manual_Mode_Wait;
	  break;

	case Tx_Init_Manual_Mode_Wait: //waiting for Run mode
		if (Waiting_On_Manual_Mode == false)
			{
				Tx_Init_State = Tx_Init_Q_Message_Enable;
			}
	  break;

	case 	Tx_Init_Q_Message_Enable: //Set Run Mode
		Waiting_On_Q_Message_Enable = true;
		Update_Q_Message_Config();
		Tx_Init_State = Tx_Init_Q_Message_Enable_Wait;
	  break;

	case Tx_Init_Q_Message_Enable_Wait: //waiting for Run mode
		if (Waiting_On_Q_Message_Enable == false)
			{
				//28/7/15
				if (cbManual_Mode->Checked)
				{
					Tx_Init_State = Tx_Init_Start_Scan_Power;
				}
				else
				{
					Tx_Init_State = Tx_Init_Idle;
				}
			}
	  break;

	case Tx_Init_Start_Scan_Power: //Start a scan and power
		Waiting_On_Scan_n_Power = true;

		//28/7/15
		Start_Scan_n_Power();

		Tx_Init_State = Tx_Init_Start_Scan_Power_Wait;
	  break;

	case Tx_Init_Start_Scan_Power_Wait: //waiting for Scan and Power
		if (Waiting_On_Scan_n_Power == false)
			{
			Tx_Init_State = Tx_Init_Idle;
			}
	  break;

	case Tx_Init_CheckTx: //Set Run Mode
		Waiting_On_Manual_Mode = true;

		tmInit_SM->Enabled = false;
		// Get Version
		SendGetVersion();

		Sleep (500);
		tmInit_SM->Enabled = true;

		Tx_Init_State = Tx_Init_Idle;
	  break;

  }

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Reset_GUI(void )
{

	TXCommMemo->Clear();
	//Log_Coms("Tx Reset");

	Tx_Init_State = Tx_Init_ModDepth_Mode;
//	Tx_Init_State = Tx_Init_Manual_Mode;
	tmInit_SM->Enabled = true;
	//ShowMessage("Scan Timer Reset Enabled");

 }

//---------------------------------------------------------------------------

void __fastcall TmainForm::Update_Q_Message_Config(void)
{

	AnsiString sPeram;
	uint32_t Q_Message_BM = 0x00;

//if (coilViewer->cb_Coil_Info_Enable->Checked)
	{
	Q_Message_BM |= Q_Message_BM_All_Detection;
	}

//if (qiViewer->cb_Qi_Message_Enable->Checked)
	{
	Q_Message_BM |= Q_Message_BM_Decoded_Qi_Messages;
	}


	sPeram = "P01" + IntToHex((int)Q_Message_BM,8) + IntToHex(0x00,8);
	Transmit_Message( sPeram.c_str( ) );


}

//---------------------------------------------------------------------------

void __fastcall TmainForm::Transmit_Message_N( char * MesPtr, unsigned int Len )
{

	unsigned int TiPtr = 0;
	char LRC = 0;

	TxBuffer2[ TiPtr++ ] = STX;

	if ( ( *MesPtr & 0x20 ) == 0 ) // (* Originator message *)
		{
		TxSeqNum++ ;
		if ( TxSeqNum > '9' )
			{
			TxSeqNum = '1';
			}

		// if (Set_Seq_Zero)
		// {
		// TxSeqNum = '0';
		// Set_Seq_Zero = false;
		// }

		TxBuffer2[ TiPtr++ ] = TxSeqNum;
		LRC += TxSeqNum;

		Tx_State = SENDING_DATA;
		NackCount = 0;
		}
	else // (* Response message *)
		{
		TxBuffer2[ TiPtr++ ] = RxSeqNum;
		LRC += RxSeqNum;
		Tx_State = SENDING_RESPONSE;
		}

	TxBuffer2[ TiPtr++ ] = *MesPtr; // Header
	LRC += *MesPtr;
	Len-- ;
	MesPtr++ ;

	while ( Len != 0 )
		{
		if ( ( *MesPtr == DLE ) || ( *MesPtr == STX ) || ( *MesPtr == ETX ) )
			{
			TxBuffer2[ TiPtr++ ] = DLE;
			LRC += DLE;
			}
		TxBuffer2[ TiPtr++ ] = *MesPtr;
		LRC += *MesPtr;
		MesPtr++ ;
		Len-- ;
		}

	LRC = ( ( ( LRC + ( ( LRC & 0xC0 ) >> 6 ) ) & 0x3F ) + 0x20 );
	TxBuffer2[ TiPtr++ ] = LRC;
	TxBuffer2[ TiPtr++ ] = ETX;
	TxBuffer2[ TiPtr ] = 0x00;

	Binary_Len = TiPtr;
	StartTransmitMessage( );

}

// ---------------------------------------------------------------------------------

void __fastcall TmainForm::Transmit_Message( char * MesPtr )
	/* This procedure is called with a pointer to a null terminated message . The message should include the header */
	{
	Transmit_Message_N( MesPtr, StrLen( MesPtr ) );
	}

// ---------------------------------------------------------------------------------

void __fastcall TmainForm::StartTransmitMessage( void )
	{

	char Temp[ 30 ];

	if ( ( Tx_State == SENDING_RESPONSE ) || ( Tx_State == SENDING_DATA ) )
		{
		//sdWriteSerial( TXComPort, TxBuffer2, Binary_Len );
if (Qi_Comport->Open)
			{
			Qi_Comport->PutBlock( TxBuffer2, Binary_Len );
			}
		 else
		 	{
			TXCommMemo->Lines->Add( "Comport Not Open" );
			mainForm->edTxModDepthView->Text = "";
			}
		}
	TXCommMemo->Lines->Add( "** Send Msg -->" );
	TXCommMemo->Lines->Add(TxBuffer2);
	TXCommMemo->Lines->Add( "<-- End Send Msg **" );

	logit("Log.txt","Txcoms--" + AnsiString(TxBuffer2));


	if ( Tx_State == SENDING_RESPONSE )
	{
		Tx_State = TX_BUFF_EMPTY;
	}
	else
	if ( Tx_State == SENDING_DATA )
	{
		tmAcknowledge->Enabled = false;
		tmAcknowledge->Enabled = true;
		Tx_State = WAITING_FOR_ACK;
	} // End If

}

// ---------------------------------------------------------------------------------

void __fastcall TmainForm::tmAcknowledgeTimer(TObject *Sender)
{
//	TestLogAdd("tmAcknowledgeTimer Timer Fired****");


	if ( Tx_State == WAITING_FOR_ACK )
	{
		NackCount++ ;
		Tx_State = SENDING_DATA;
		StartTransmitMessage( );
	} // End If

	if ( NackCount > 5 )
	{
		// off line code here.
		NackCount = 0;
		Tx_State = TX_BUFF_EMPTY;
		Tx_Init_State = Tx_Init_Idle;
//		ShowMessage("Tx is Offline");
		mainForm->lbControlMessage->Caption = "WARNING: Tx is Offline (Click to Clear)";
		//todo  clear dispaly.
	}  // End If

}

//---------------------------------------------------------------------------
//	AnsiString sPeram;
//
//	if ( cbManual_Mode->Checked )
//		{
//		sPeram = "N0100000001";
//		btScan_Board->Enabled = true;
//		bt_Scan_n_Power->Enabled = true;
//		btPower_Selected->Enabled = true;
//		btTurn_Off_Coils->Enabled = true;
//
//		Enable_Coil_Picker( );
//		}
//	else
//		{
//		sPeram = "N0100000000";
//		btScan_Board->Enabled = false;
//		bt_Scan_n_Power->Enabled = false;
//		btPower_Selected->Enabled = false;
//		btTurn_Off_Coils->Enabled = false;
//		Disable_Coil_Picker( );
//
//		}
//
//	Transmit_Message( sPeram.c_str( ) );
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TmainForm::cbManual_ModeClick(TObject *Sender)
{

	SetTxOpMode();

}

void __fastcall TmainForm::SetTxOpMode()
{
//	AnsiString sPeram;
	//OpenTxPort(TXComPortNo);

	if ( cbManual_Mode->Checked )
		{
  //			sPeram = "N0100000001";
			//SetManualMode2;
			SetTxManualMode();
		}
	else
		{
	//		sPeram = "N0100000000";
			//SetAutoMode2;
			SetTxAutoMode();
		}
//	CloseTxPort();
//	Transmit_Message( sPeram.c_str( ) );

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SetTxManualMode()
{
	AnsiString sPeram;

	sPeram = "N0100000001";
	Transmit_Message( sPeram.c_str( ) );

}
//---------------------------------------------------------------------------
void __fastcall TmainForm::SetTxAutoMode()
{
	AnsiString sPeram;
	sPeram = "N0100000000";
	Transmit_Message( sPeram.c_str( ) );

}

//---------------------------------------------------------------------------

void __fastcall TmainForm::memoPlotterLogChange(TObject *Sender)
{
if (memoPlotterLog->Lines->Count)
	{
	logit("Log.txt",memoPlotterLog->Lines->Strings[memoPlotterLog->Lines->Count -1]);
	}
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::memoUSBLogChange(TObject *Sender)
{
if (memoUSBLog->Lines->Count)
	{
	logit("Log.txt",memoUSBLog->Lines->Strings[memoUSBLog->Lines->Count -1]);
	}
}
//---------------------------------------------------------------------------


void __fastcall TmainForm::btnPlotComportOpenClick(TObject *Sender)
{
//USBMM->DMM_Task();

//	PlotterComPort = UserSelectComport("Select Plotter Com-Port for Plotter " + IntToStr((int)Plotter_Number) , "PortPlotter", 57600);

	if (cbPlotterType2->ItemIndex == 0)
	{
		// 2D Plotter
		UserSelectComport(Plotter_Comport,"Select Plotter Com-Port for Plotter " + IntToStr((int)Plotter_Number) , "PortPlotter", 57600);
	}
	else
	{
		// 3D Plotter
		UserSelectComport(Plotter_Comport,"Select Plotter Com-Port for Plotter " + IntToStr((int)Plotter_Number) , "PortPlotter", 115200);

	} // End If


	//initialize plotter interface
	Plotter = new XYPlotter(Plotter_Comport, memoPlotterLog,tmPlotter_NRT);
//	mainForm->Caption = mainForm->Caption + " - CONNECTED to Plotter No. " + IntToStr((int)Plotter_Number)  ;
//	mainForm->Caption = mainForm->Caption + " Plotter " + IntToStr((int)Plotter_Number)  ;


	// if com-port could not be opened close application
	if(Plotter == NULL)
	{
		// Plotter NOT CONNECTED

		shPlotterConnected->Brush->Color = clSilver;
		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
	}
	else
	{
		// Plotter CONNECTED
		//		sdFlushPort(PlotterComPort);
//		PlotterComPort_Rx_Thread = new Rx_Thread( PlotterComPort, RX_NODE_PLOTTER_COMPORT );

//		shPlotterConnected->Brush->Color = clLime;

		if( Plotter_Comport->Open )
//	  if (Plotter_Comport->ComNumber == NULL)
		{
			//ShowMessage("yes");
			Plotter_Comport->FlushInBuffer();
			Plotter_Comport->FlushOutBuffer();

			pnlManualPlotterCmd->Enabled = true;

		edPlotterComportNo2->Text = IntToStr(Plotter_Comport->ComNumber);

		//set buttons
		btnPlotComportOpen2->Enabled = false;
		btnPlotComportClose2->Enabled = true;

		btnManGoToTestStart2->Enabled = true;
		btnHomePlotterSet2->Enabled = true;
		btHomePlotter->Enabled = true;

		btGotoXY2->Enabled = true;
		//btnGoToOffset2->Enabled = true;
		btnManGoToTestStart2->Enabled = true;

		btEnablePSU->Enabled = true;

		//rgLoad->Enabled = true;

		cbPSU_ONOff->Enabled = true;

		btnGoToStepEnd2->Enabled = true;

		btnLoadUnload2->Enabled = true;

		//pnlPlotterControl->Visible = true;
		//pnlPlotterControl->Enabled = true;

	   ShowMessage("NOTE: Remember to Home Plotter FIRST before Running Test");

	  }
	 else
	 {
			//ShowMessage("no");
			edPlotterComportNo2->Text = "";


	 } // End If

	}  // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::rgLoadClick(TObject *Sender)
{

/*

 Reg_WriteInteger(sREG, "Load", "Radio_Index",rgLoad->ItemIndex );

	//set the load
	// turn off all load relays
	if (cbPlotterType->ItemIndex == 0)
	{
		// 2D Plotter
		Plotter->enLoadRelay(false,0);
		Plotter->enLoadRelay(false,1);
		//Plotter->enLoadRelay(false,2);

		// Turn ON selected Load
		Plotter->enLoadRelay(true,rgLoad->ItemIndex);
	}
	else
	{
		// 3D Plotter
		enLoadRelay3D(false, 0);
		enLoadRelay3D(false, 1);
		enLoadRelay3D(false, 2);
		// Turn ON selected Load
		enLoadRelay3D(true,rgLoad->ItemIndex);

	} // End If


	switch (rgLoad->ItemIndex)
	{
		case 0:
			lblRxLoadName->Caption = "3.6W";
			mainForm->edRxLoad->Text = "3_6W";
			break;
		case 1:
			lblRxLoadName->Caption = "5W";
			mainForm->edRxLoad->Text = "5W";
			break;
		case 2:
			lblRxLoadName->Caption = "7.5W";
			mainForm->edRxLoad->Text = "7_5W";
			break;
		default:
			lblRxLoadName->Caption = "??W";
			mainForm->edRxLoad->Text = "?W";

		break;
	}

	//set the relay state to measure TX / RX
	if (cbPlotterType->ItemIndex == 0)
	{
		// 2D Plotter
		Plotter->enRefRelay(true);
	}
	else
	{
		// 2D Plotter
		enRefRelay3D(true);
	} // End If

*/
}
//---------------------------------------------------------------------------


void __fastcall TmainForm::tmPlotter_NRTTimer(TObject *Sender)
{
memoPlotterLog->Lines->Add("NRT");
tmPlotter_NRT->Enabled = false;

if (testAdmin.NRT_Goto)
	{
	testAdmin.testState = testAdmin.NRTState;
	}
testAdmin.NRT_Goto = false;

}

//28/7/15
//---------------------------------------------------------------------------
void __fastcall TmainForm::Start_Scan_n_Power(void)
	{
	AnsiString sPeram;
	sPeram = "K";

	Transmit_Message( sPeram.c_str( ) );

	}

//---------------------------------------------------------------------------

void __fastcall TmainForm::TabControl1Change(TObject *Sender)
{

	//ShowMessage("tab");

	char buf[3];

	if (TabControl1->TabIndex == -1)
	{
//		ShowMessage("tab1");
	}
	else
	{
//        StatusBar1->SimpleText = "Tab index: " +
  //      String(itoa(TabControl1->TabIndex, buf, 10));

		switch (TabControl1->TabIndex)
		{
			case 0:
			   //		ShowMessage("tab0");
			  //	Panel0->Visible = true;
				ShowTabPanel(0);
				//cbIgnoreMessages->Visible = false;
				break;
			case 1:
		//		ShowMessage("tab1");
				ShowTabPanel(1);
				//cbIgnoreMessages->Visible = true;
				break;
			case 2:
		//		ShowMessage("tab2");
				ShowTabPanel(2);
				//bIgnoreMessages->Visible = true;
				break;
			case 3:
		//				ShowMessage("tab3");
				ShowTabPanel(3);
				break;
			case 4:
		//				ShowMessage("tab4");
				break;
			default:
				break;
		} // End Switch
	}  // End If


}  // End Procedure
//---------------------------------------------------------------------------
void __fastcall TmainForm::ShowTabPanel(int PanelNumb)
{
	switch (PanelNumb)
		{
			case 0:
				Panel2->Visible = true;
				Panel3->Visible = false;
				Panel4->Visible = false;
				Panel5->Visible = false;
				break;
			case 1:
				Panel2->Visible = false;
				Panel3->Visible = true;
				Panel4->Visible = false;
				Panel5->Visible = false;
				break;
			case 2:
				Panel2->Visible = false;
				Panel3->Visible = false;
				Panel4->Visible = true;
				Panel5->Visible = false;
				break;
			case 3:
				Panel2->Visible = false;
				Panel3->Visible = false;
				Panel4->Visible = false;
				Panel5->Visible = true;
				break;
			default:
				break;
		}

}
//---------------------------------------------------------------------------
void __fastcall TmainForm::About1Click(TObject *Sender)
{
	ShowMessage("Developed for Efficiency Plotter with Embarcadero� C++Builder� XE3\n(April 2016)");
}
//---------------------------------------------------------------------------

/*
void __fastcall TmainForm::dsgsgs1Click(TObject *Sender)
{
	PlotterComPort = UserSelectComport("Select Plotter Com-Port for Plotter " + IntToStr((int)Plotter_Number) , "PortPlotter", 57600);

	// if com-port could not be opened close application
	if(  PlotterComPort == NULL) {
		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
	} else {
		sdFlushPort(PlotterComPort);
		PlotterComPort_Rx_Thread = new Rx_Thread( PlotterComPort, RX_NODE_PLOTTER_COMPORT );
	}

}

*/
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnPlotComportCloseClick(TObject *Sender)
{

	//stop timer
//	sdFlushPort(PlotterComPort);
//	sdClosePort(PlotterComPort);

//	PlotterComPort_Rx_Thread->FreeOnTerminate = false;
//	PlotterComPort_Rx_Thread->Terminate();
//	PlotterComPort_Rx_Thread->WaitFor() ;

//	delete PlotterComPort_Rx_Thread;


	//set buttons
	btnPlotComportOpen2->Enabled = false;

	btnPlotComportClose2->Enabled = false;
	btnManGoToTestStart2->Enabled = false;
	btnHomePlotterSet2->Enabled = false;

	btHomePlotter->Enabled = false;

  	btGotoXY2->Enabled = false;
   //	btnGoToOffset->enabled = false;
   //	btnGoToOffset2->Enabled = false;
	btnManGoToTestStart2->Enabled = false;

	btEnablePSU->Enabled = false;

	rgLoad->Enabled = false;

	cbPSU_ONOff->Enabled = false;

	shPlotterConnected->Brush->Color = clSilver;

	btnGoToStepEnd2->Enabled = false;

	btnLoadUnload2->Enabled = false;

	//pnlPlotterControl->Enabled = false;
	//pnlPlotterControl->Visible = false;

// Close Any 3D Plotter PSU Port
		if( PSU_Comport->Open )
		{
			PSU_Comport->FlushInBuffer();
			PSU_Comport->FlushOutBuffer();
			PSU_Comport->Open = false;

		}
		else
		{
			// Do Nothing
		} // End If

// Close any Plotter PSU Port
		if( Plotter_Comport->Open )
		{
			Plotter_Comport->FlushInBuffer();
			Plotter_Comport->FlushOutBuffer();
			Plotter_Comport->Open = false;

		}
		else
		{
			// Do Nothing
			edPlotterComportNo2->Text = "";
		} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnProgramCloseClick(TObject *Sender)
{

	CloseGUI();

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::CloseGUI()
{

if (MessageDlg("Please confirm you want to CLOSE \nYES = CLOSE \nNO = Cancel", mtInformation, mbYesNo, 0) == mrYes)
	 {
		//ShowMessage("Yes");
	   //Run Shutdown Sequence here

		if (flgTxPortOpen)
		{
			//ShowMessage("Tx Port Open");
		   //	CloseTxPort();
		}
		else
		{
		   // Do Nothing
		} // End If

		StopALLTimers();

		SeqTestSTOP();
		cbSeqIniFilenames->Enabled = true;

		CloseAllOpenComports();

		//USBMM->Stop();
		StopUSBMM();

		mainForm->Close();

	 }
	 else
	 {
		 //ShowMessage("No");
	 } // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::StopALLTimers()
{
		// Stop ALL Timers
		tmTest->Enabled = false;
		tmSeqTest2->Enabled = false;
		tmPlotter->Enabled = false;
		tmMeterUpdate->Enabled = false;
		tmAcknowledge->Enabled = false;
		tmInit_SM->Enabled = false;
		tmPlotter_NRT->Enabled = false;
		tmSeqTestWait->Enabled = false;

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::CloseAllOpenComports()
{


		if(Plotter_Comport->Open)
		{

//			for (int i = 0; i < 100; i++)
//			{
//				Wait_mS(100);
//				lbStatusGridSeq->Caption = "STATUS: Closing Plotter Port";
//			}  // End For

			Plotter_Comport->FlushInBuffer();
			Plotter_Comport->FlushOutBuffer();
			Plotter_Comport->Open = false;
//			lbStatusGridSeq->Caption = "STATUS: Plotter Port CLOSED";
		}
		else
		{
			// Do Nothing
		} // End If

		if(Qi_Comport->Open)
		{
			Qi_Comport->FlushInBuffer();
			Qi_Comport->FlushOutBuffer();
			Qi_Comport->Open = false;
//		lbStatusGridSeq->Caption = "STATUS: Tx Port CLOSED";
//			break;
		}
		else
		{
			// Do Nothing
		} // End If
//		lbStatusGridSeq->Caption = "STATUS: Closing Tx Port";

		if(PSU_Comport->Open)
		{
			PSU_Comport->FlushInBuffer();
			PSU_Comport->FlushOutBuffer();
			PSU_Comport->Open = false;
//		lbStatusGridSeq->Caption = "STATUS: PSU Port CLOSED";
//			break;
		}
		else
		{
			// Do Nothing
		} // End If
//		lbStatusGridSeq->Caption = "STATUS: Closing PSU Port";

		if(PSU_ComportGW->Open)
		{
			PSU_ComportGW->FlushInBuffer();
			PSU_ComportGW->FlushOutBuffer();
			PSU_ComportGW->Open = false;
//		lbStatusGridSeq->Caption = "STATUS: PSU Generic Port CLOSED";
//			break;
		}
		else
		{
			// Do Nothing
		} // End If
//		lbStatusGridSeq->Caption = "STATUS: Closing PSU Generic Port";

		if(eLoad_Comport->Open)
		{
			eLoad_Comport->FlushInBuffer();
			eLoad_Comport->FlushOutBuffer();
			eLoad_Comport->Open = false;
//		lbStatusGridSeq->Caption = "STATUS: e-Load Port CLOSED";
//			break;
		}
		else
		{
			// Do Nothing
		} // End If
//		lbStatusGridSeq->Caption = "STATUS: Closing e-Load Port";

		if(Gen_Comport->Open)
		{
			flgRunGenComport = false;
		/*
			if (RunningComport)
			{
				// Wait to finish
				for (int i = 0; i < 1000; i++)
				{
					Wait_mS(100);
					lbStatusGridSeq->Caption = "STATUS: Closing Gen Port: " + IntToStr(i);
					if ((Gen_Comport->Open) || (RunningComport))
					{
						// Do Nothing
					}
					else
					{
						break;
					} // End If
				}  // End For
			}
			else
			{

			} // End If
		*/

			Gen_Comport->FlushInBuffer();
			Gen_Comport->FlushOutBuffer();
			Gen_Comport->Open = false;
//		lbStatusGridSeq->Caption = "STATUS: Gen Misc Port CLOSED";
//			break;
		}
		else
		{
			// Do Nothing
		} // End If
//		lbStatusGridSeq->Caption = "STATUS: Closing Gen Misc Port";



	if(Arduino_FOD->Open)
		{
///			flgRunGenComport = false;

			Arduino_FOD->FlushInBuffer();
			Arduino_FOD->FlushOutBuffer();
			Arduino_FOD->Open = false;
//		lbStatusGridSeq->Caption = "STATUS: Gen Misc Port CLOSED";
//			break;
		}
		else
		{
			// Do Nothing
		} // End If

}

//---------------------------------------------------------------------------

void __fastcall TmainForm::btnManGoToTestStartClick(TObject *Sender)
{


	MovePlotterToPointMAIN(1, 1, 1, StrToFloat(ed3DZHeight2->Text), 1);

/*
	XYPlotter_setpoint_t tmpSetpoint;
//	tmpSetpoint.XPos = StrToInt(ebXOffset->Text);
//	tmpSetpoint.YPos = StrToInt(ebYOffset->Text);
	tmpSetpoint.XPos = StrToInt(ebXSetpointStart->Text); // - StrToInt(ebXOffset->Text);
	tmpSetpoint.YPos = StrToInt(ebYSetpointStart->Text); // - StrToInt(ebYOffset->Text);
//	tmpSetpoint.XPos = 0;
//	tmpSetpoint.YPos = 0;

	Plotter->Set3dZHeight(StrToFloat(ed3DZHeight->Text));

	//update plotter setpoint
	Plotter->UpdateSetpoint(tmpSetpoint);
	//Update user labels
	updatePosLabels(this);
*/
}

//---------------------------------------------------------------------------
void __fastcall TmainForm::RunPlotterSequence()
{

	 //	TestStarted = true;
//		SeqRowStarted = true;

		InitPlotGrid();

		//indicate start in log
		TestLogAdd("Starting Test...");

		//set state to init state
		testAdmin.testState = TEST_INIT_STATE;

		//update buttons
		btStartTest->Enabled = false;

		rgLoad->Enabled = false;

		btStopTest->Enabled = true;
		btnPause->Enabled = true;

		//set the load
		// turn off all load relays

		//create test file name
		testAdmin.testFile = CreateFileName();

		//set the relay state to measure TX / RX
//		if (cbPlotterType->ItemIndex == 0)
//		{
//			Plotter->enRefRelay(true);
//		}
//		else
//		{
//			enRefRelay3D(true);
//		} // End If

		// USBMM->RESETManualControlFlag();

		// ***
		// System TIMERS
		// ***
		//enable test-system timer
		tmTest->Enabled = true;

		//disable automated meter timer
	   //	tmMeterUpdate->Enabled = false;

		//Enable Plotter Timer
		tmPlotter->Enabled = true;

		strRetryCountSet = ebHoleRetryNo->Text;
		mainForm->lbRetryNotice->Caption = "Retry = Ready";

//	}
//	else
//	{
//		ShowMessage("Please Check if Plotter Comport Opened in Set Up Sheet");
//	} // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::StartSeqTest()
{

	// IniFilename = ChangeFileExt(Application->ExeName, ".INI");

	// Reset Grid. If already run Col 1 is numbered
	// ***
	// Check if File Exists
	// ***
/*
	if (PlotterON())
	{
		// OK

*/

		//	if (FileExists(FileListBox1->FileName))
			if (StrGridSeqTest->Cells[0][0] != "")
			{

				if (MessageDlg("Plotter Rx at Start of Test?\nYes = OK\nNo = Exit Test", mtInformation, mbYesNo, 0) == mrYes)
				 {

					//ShowMessage("File Exists");
					ReadIniFile();

					//    StrGridSeqTest->Cells[0][0] = "SEQ GRID";

					if (MessageDlg("Yes = Run Sequence Grid at Bottom\n(Note: Rows with 'r' in 1st Column)\nNo = Exit", mtInformation, mbYesNo, 0) == mrYes)
					 {

						cbSeqIniFilenames->Enabled = false;

					  //	USBMM->Start();

						// Initialise
						ebRepeat->Text = "0";
						TestStarted = true;
						SeqTestDone = false;
						SeqStepCount = 0;
						SeqRepeatCount = 0;
						SeqStepCounter = 1; // Start at position 1
						SeqTestRunning = false;
						SeqRowRunCounter = 1;

				//	   lbRepeatCountStatus->Caption = "Repeat Count = " + IntToStr(SeqRepeatCount);

						OFFSeqGridTimeDone = false;
						ONSeqGridTimeDone = false;

						btnStartSeqGrid->Enabled = false;
						btnPause->Enabled = true;
						btnStopSeqGrid->Enabled = true;

						//Create CSV Filename
				//		testFile_Name = CreateFileName2(ebFolderName->Text, ebFilename->Text);

						//Create test file name
		 //				testAdmin.testFile = CreateFileName();

					   // Initialise Plotter
					   // 1 Home
					   // 2 Got to Start Position


					   //***
					   // Start DAQ in background
					   //***


						//Create NEW test file name
					   //	testAdmin.testFile = CreateFileName();
						//Create test file Set 0
					   //	SaveGridData2CSV(0);


						//==============================
						// Check if Misc Port for LCR set
						//==============================
//						if (SeqSeqKeywordLookup(SeqGridKeyword[41][0]))
//						{
							// *************************************
							// Append Grid Display for any Misc Data
							// *************************************
							AppendGridDataDisplayOption();
//						}
//						else
//						{
							// Do Nothing
//						} // End If



					   //***
					   // Start Test timer
					   //***
					   //SeqTestRunning = true;

					   tmSeqTest2->Enabled = true;

						//***
						// GO TO START LOCATION
						//***
						// Allow time for Plotter to Home
	/*
						// Create mm co-ord ref to grid point location
						AnsiString xCoord = IntToStr(StrToInt(ebXSetpointStart->Text) + ((0) * StrToInt(ebStepSize->Text)));
						AnsiString yCoord = IntToStr(StrToInt(ebYSetpointStart->Text) + ((0) * StrToInt(ebStepSize->Text)));
						AnsiString ZHeight = ed3DZHeight->Text;
						AnsiString Speed = "3000";
						// Move Plotter
						if (cbSimPlotting->Checked)
						{
							MessagesGlobal(2);
						}
						else
						{
							MovePlotterToPoint(xCoord, yCoord, ZHeight, Speed);
						}  // End If


					   lbCountDownState->Caption = "Wait Countdown: HOME";
					   // Allow time to get there
					   TimerSequential(5000);
					   SeqTestRunning = false;

					   lbCountDownState->Caption = "Wait Countdown: Ready";
						//***
						// Ends GO TO START LOCATION
						//***

	*/
					   //***
					   // Ends Start Test timer
					   //***
					 }
					 else
					 {
						 //ShowMessage("No");
						// Do Nothing
						TestStarted = false;
					 } // Ends Message

				 }
				 else
				 {
					 //ShowMessage("No");
				 } // End If

			}
			else
			{
				// Do Nothing
		//		SaveIniFileData(true);
		  //		ReadIniFile();
				ShowMessage("No Ini Sequence File Selected at Bottom of the Form Grid");

			}  // End If


/*
	}
	else
	{

		ShowMessage("Plotter Not Open or Homed.\nNOTE: You can Run Simulation set in Maintenance Tab where ALL Outputs Disabled for Demo");
	} // End If

*/

} // End Function


//---------------------------------------------------------------------------
void __fastcall TmainForm::btnStartSeqGridClick(TObject *Sender)
{

bool TitleCheckOK;


// Make sure changes are saved in Seq???

	SaveIniFileData(false);
	ReadIniFile();


		PlotOnSelectedPoint = false;

		//ShowMessage("Yes");

	if (OverallSeqSeqKeywordCheck())
	{
		 // OK
		TitleCheckOK = true;
	}
	else
	{
		// Title Not Found
		if (MessageDlg("Yes = Ignore Title Error\nNo = Exit", mtInformation, mbYesNo, 0) == mrYes)
		 {
  //			ShowMessage("Yes");
			TitleCheckOK = true;
		 }
		 else
		 {
//			 ShowMessage("No");
			TitleCheckOK = false;

		 } // End If
	 } // End If         }


	 if (TitleCheckOK)
	 {
		// Do Nothing
		  InitDataResultsGrid();

			// Clear Warning Message
			lbControlMessage->Caption = "";

			if (cbSimPlotting->Checked)
			{
				ShowMessage("Simulation Checked in 3. Maintenance Tab. ALL Outputs Disabled for Demo");
				if (SeqTestRunning)
				{
					cbSimPlotting->Enabled = false;
				}
				else
				{
					// Do Nothing

				} // End If

			}
			else
			{
				// Do Nothing
				cbSimPlotting->Enabled = true;

				//***
				// Check Settings
				//***
				// Plotter
				if (shPlotterConnected->Brush->Color == clSilver)
				{
//					ShowMessage("WARNING: Plotter Not Set up");
				}
				else
				{
					// Do Nothing
				} // End If

					// PSU
					if (shpPSUConnected->Brush->Color == clSilver)
					{
//						ShowMessage("WARNING: PSU Not Set up");
					}
					else
					{
						// Do Nothing
					} // End If

					// DAQ
					if (shpDAQConnected->Brush->Color == clSilver)
					{
//						ShowMessage("WARNING: DAQ Not Set up");
					}
					else
					{
						// Do Nothing
					} // End If

					// e-load
					if (shpELoadConnected->Brush->Color == clSilver)
					{
//						ShowMessage("WARNING: e-load Not Set up");
					}
					else
					{
						// Do Nothing
					} // End If


					// TX
					if (shpTxConnected->Brush->Color == clSilver)
					{
						//ShowMessage("WARNING: Tx Not Set up");
					}
					else
					{
						// Do Nothing
					} // End If


			} // End If


			StartSeqTest();

	 }
	 else
	 {
		// Exit

	 } // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SeqStepGrid()
{
int Seqrow;
AnsiString ResultCSV;

	   Seqrow = 1;
	   int RowMax = StrGridSeqTest->RowCount;


	//Create CSV Filename
  //	testFile_Name = CreateFileName2(ebFolderName->Text, ebFilename->Text);

	//***
	// Write CSV Timestamp
	//***
   //	ResultCSV = WriteCSV(testFile_Name, "test", false);  // timestamp

//	ResultCSV = WriteToCSV(testFile_Name,"\n", false);


	   for (Seqrow; Seqrow < RowMax; Seqrow++)
	   {

			   //	AnsiString gridtext = StrGridSeqTest->Cells[Seqrow][1];
			StrGridSeqTest->Cells[1][Seqrow] = IntToStr(Seqrow);

			tmSeqTest2->Enabled = true;
			//Sleep(1000);



	   } // End For


} // End Function

//---------------------------------------------------------------------------

void __fastcall TmainForm::ColourGridCellGrn(int cellx, int celly, int GridNo)
{


	if (GridNo = 1)
	{
		TRect Recto = StringGrid1->CellRect(cellx , celly);
	//	int Area = Recto.Width() * Recto.Height();

	//		ShowMessage(IntToStr(Area));

	//	StringGrid1->Canvas->Brush->Color = clRed;
		StringGrid1->Canvas->Brush->Color = clGreen;
	//	StringGrid1->Canvas->Brush->Color = clWhite;
	//	StringGrid1->Canvas->Font->Color  = clGreen;
		StringGrid1->Canvas->FillRect(Recto);

	//	ShowMessage("df");

	}
	else
	{
		TRect Recto = StringGrid2->CellRect(cellx , celly);
	//	int Area = Recto.Width() * Recto.Height();

	//		ShowMessage(IntToStr(Area));

	//	StringGrid1->Canvas->Brush->Color = clRed;
		StringGrid2->Canvas->Brush->Color = clGreen;
	//	StringGrid1->Canvas->Brush->Color = clWhite;
	//	StringGrid1->Canvas->Font->Color  = clGreen;
		StringGrid2->Canvas->FillRect(Recto);

	//	ShowMessage("df");

	} // End If

}

//---------------------------------------------------------------------------


void __fastcall TmainForm::ebXSetpointStartChange(TObject *Sender)
{

if (ebXSetpointStart2->Text == "-")
{
	//Ignore - symbol
}
else
{
	// OK
 if (ebXSetpointStart2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebXSetpointStart2->Text = "5";
   }
   else
   {

//	Reg_WriteString(sREG, "Cords", "X_Offset", ebXOffset->Text);
//	TestLogAdd("New [X Offset] : " + ebXOffset->Text);

	Reg_WriteString(sREG, "Cords", "X_SetPoint", ebXSetpointStart2->Text);
	TestLogAdd("New [X SetPoint] : " + ebXSetpointStart2->Text);

	//update plotter settings
	XYPlotter_setpoint_t tmpOffset;
	tmpOffset.XPos=StrToInt(ebXSetpointStart2->Text);
	tmpOffset.YPos=StrToInt(ebYSetpointStart2->Text);
	//Plotter->SetPlotStart(tmpOffset);
   }  // End If


} // End If


}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebYSetpointStartChange(TObject *Sender)
{

if (ebYSetpointStart2->Text == "-")
{
	//Ignore - symbol

}
else
{
	// OK
 if (ebYSetpointStart2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebYSetpointStart2->Text = "5";
   }
   else
   {
//	Reg_WriteString(sREG, "Cords", "Y_Offset", ebYOffset->Text);
//	TestLogAdd("New [Y Offset] : " + ebYOffset->Text);

	Reg_WriteString(sREG, "Cords", "Y_SetPoint", ebYSetpointStart2->Text);
	TestLogAdd("New [Y SetPoint] : " + ebYSetpointStart2->Text);

	//update plotter settings
	XYPlotter_setpoint_t tmpOffset;
	tmpOffset.XPos=StrToInt(ebXSetpointStart2->Text);
	tmpOffset.YPos=StrToInt(ebYSetpointStart2->Text);
	//Plotter->SetPlotStart(tmpOffset);

   } // End If

} // End If


}
//---------------------------------------------------------------------------

void __fastcall TmainForm::cbShowProgChartClick(TObject *Sender)
{
	if (cbShowProgChart->Checked)
	{
	   // With Chart
	   btnClearChart->Visible = true;
		if (Big_View->Checked)
		{
			StringGrid1->Height = 234 * 1.8;
			StringGrid2->Height = 234 * 1.8;
			Chart1->Top = 570;
		}
		else
		{
			StringGrid1->Height = 234;
			StringGrid2->Height = 234;
			Chart1->Top = 406;
		} // End If


		Chart1->Visible = true;
	}
	else
	{
		// No Chart
	   btnClearChart->Visible = false;
		if (Big_View->Checked)
		{
			StringGrid1->Height = 388 * 1.45;
			StringGrid2->Height = 388 * 1.45;
			Chart1->Top = 570;
		}
		else
		{
			StringGrid1->Height = 388;
			StringGrid2->Height = 388;
			Chart1->Top = 406;
		} // End If

		Chart1->Visible = false;

	} // End If
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnClearChartClick(TObject *Sender)
{


//TDateTime today = Date();
//ShowMessage(today);

//TDateTime time = Time();
//ShowMessage(time);
//int yy = today.CurrentTime();

   time_t firsttime, secondtime;
   firsttime = time(NULL);  // Get start time
   ShowMessage("delay");
   secondtime = time(NULL); // Get stop time
	float xx = difftime(secondtime, firsttime);



//ReadEffPlotIniFileData();
//SaveEffPlotIniFileData();
//ShowMessage("Done");


//SaveSeqIniFileData();
//ReadSeqIniFileData();


//AnsiString ResultCSV = WriteCSV("c:\\temp\\data2.csv", ",test", false);  // timestamp


 if (WriteToCSVData("c:\\temp\\data2.csv", "TestResultData", false))
 {
	ShowMessage("OK");
 }
 else
 {
	ShowMessage("Not OK");
 }


/*
	switch(StrToInt(InputBox("Please Enter Menu Option Number",
	"Menu Number\n"
	"0 = Exit\n"
	"1 = ??\n",
	0)))
	 {
		case 0:
		 {

			break;
		 }
		case 1:
		 {
			break;
		 }

		default:
			break;
	 }  // End Switch

	 */

//OverallSeqSeqKeywordCheck();

//CheckSeqKeywords();
//SaveSeqIniFileData();
//ReadSeqIniFileData();


//shpPSUConnected->Brush->Color = clRed;

/*
	//***
	// Get Seq Grid Title Column Number
	//***
	int ser = GetSeqGridColNofText("Plot");
	if ((ser == -1) || (ser > StrGridSeqTest->ColCount))
	{
		// No Grid opened?
		// Do Nothing
		ShowMessage("Not Found");
	}
	else
	{
		ShowMessage("Found Grid Col No = " + IntToStr(ser));
	} // End If
	//***
	// Ends Get Seq Grid Title Column Number
	//***

*/



//	shpDAQConnected->Brush->Color = clLime;
//	ClearEff_Sig_Grids();

//	Series1->Clear();

//	   rteIniFile->Lines->LoadFromFile(FileListBox1->FileName);

//rteIniFile->SelStart = 1;
//  rteIniFile->SelLength = 21;
//  Char = rteIniFile->SelText;

  //	   ShowMessage(rteIniFile->SelText);
		// rteIniFile->

/*
int iFileHandle;
  int iFileLength;
  int iBytesRead;
  try
  {
	iFileHandle = FileOpen(Edit3->Text, fmOpenRead | fmOpenWrite | fmShareDenyNone);
	if (iFileHandle > 0) {
	  iFileLength = FileSeek(iFileHandle, 0, 2);
	  FileSeek(iFileHandle,0,0);
	  std::auto_ptr<AnsiChar> pszBuffer(new AnsiChar[iFileLength+1]);
	  iBytesRead = FileRead(iFileHandle, pszBuffer.get(), iFileLength);
	  FileClose(iFileHandle);

	  for (int i = 0; i < iBytesRead; i++)
	  {
		StringGrid1->RowCount += 1;
		StringGrid1->Cells[1][i+1] = pszBuffer.get()[i];
		StringGrid1->Cells[2][i+1] = IntToStr((int)pszBuffer.get()[i]);
	  }
	}
	else
	{
	  Application->MessageBox(
		L"Can't perform one of the following file operations: Open, Seek, Read, Close.",
		L"File Error", IDOK);
	}
  }
  catch(...)
  {
	Application->MessageBox(
	  L"Can't perform one of the following file operations: Open, Seek, Read, Close.",
	  L"File Error", IDOK);
  }


*/


}




//---------------------------------------------------------------------------
void __fastcall TmainForm::cbPowerSupplyStatusClick(TObject *Sender)
{

	if (cbPowerSupplyStatus->Checked)
	{
//		btEnablePSUClick(this);
		PSU_ON();
	}
	else
	{
//		btDisablePSUClick(this);
		PSU_OFF();

	} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Plotter_ComportTriggerAvail(TObject *CP, WORD Count)
{
/*
char Temp;

	while(Count--)
	{

	Temp = Plotter_Comport->GetChar();

	if (Plotter != NULL)
		{
		Plotter->ProcessRXData(1,&Temp);
		}
	 }

*/
//enum states {
//			WAIT_SOM, SEQ, HEADER, DATA, CHECKSUM, DONE
//		};

//static char RxBuffer[400];

static char RxBuffer2[400];

//static int State = WAIT_SOM;
char input;
//static char LRC;
static int i;
static int Ri_Ptr;

while (Count != 0)
	{
	if (Recived_Message == false)
	{
		input = Plotter_Comport->GetChar();

  //		if ((input == '\0') || (input == '\n') || (input == '\r'))	//if STX found in data resync
		if ((input == '\n') || (input == '\r'))	//if STX found in data resync
		{
			AnsiString RX_Plotter;
			for (i = 0; i < sizeof(RxBuffer2); i++)
			{
				RX_Plotter = RX_Plotter + RxBuffer2[i];
			}  // End For

			if ((RX_Plotter == "") || (RX_Plotter == "wait"))
			{
				// Do nothing
			}
			else
			{
				TestLogAdd("Plotter msg : " + RX_Plotter);
			} // End If

			// Clear Buffer
			Ri_Ptr = 0;
			memset(RxBuffer2, 0, sizeof(RxBuffer2));
		}
		else
		{
			RxBuffer2[Ri_Ptr++] = input;
		} // End If




/*
		if (input == STX)	//if STX found in data resync
			{
			State = WAIT_SOM;
			}

		switch (State)
			{
			case WAIT_SOM:
				if (input == STX)
					{
					Ri_Ptr = 0;
					memset(RxBuffer,0xFF,sizeof(RxBuffer));
					RxBuffer[Ri_Ptr++] = input;
					LRC = 0;
					State = SEQ;
					}
				break;

			case SEQ:
				RxBuffer[Ri_Ptr++] = input;
				State = DATA;
				break;

			case DATA:
				if (input != ETX)
					{
					if (Ri_Ptr < (sizeof(RxBuffer)-2))
						{
						RxBuffer[Ri_Ptr++] = input;
						}
					else
						{  //buffer overflow, Start looking for STX again
						State = WAIT_SOM;
						TestLogAdd("Overflow Error");
						break;
						}
					}
				else
					{
					// got message do check sum
					for (i = 1; i <= Ri_Ptr - 2; ++i)
						{
						LRC += RxBuffer[i];
						}

					LRC = ((LRC + ((LRC & 0xC0) >> 6)) & 0x3F) + 0x20;
					if (LRC == RxBuffer[Ri_Ptr - 1])  // Checksum ok. Copy to working RxBuffer.
						{
						RxBuffer[Ri_Ptr - 1] = 0;
						// (* Null terminated *)

						Recived_Message = TRUE;
						memcpy(RiBuf, RxBuffer, sizeof(RxBuffer));

						Application->ProcessMessages();
						}
					else
						{
						TestLogAdd("Checksum Error");
						}
					State = WAIT_SOM;
					}
				break;

				} // end of case

*/

			Count--;

			} // end of (Recived_Message == false)
		Application->ProcessMessages();

		}  // End While

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::cbPSU_ONOffClick(TObject *Sender)
{
	if (cbPSU_ONOff->Checked)
	{
		PSU_ON();
	}
	else
	{
		PSU_OFF();
	} // End If
}
//---------------------------------------------------------------------------


void __fastcall TmainForm::btScanPowerClick(TObject *Sender)
{

	SetTxManualMode();
	SendScanAndPowerCmd();
//	SetTxAutoMode();

//  bool WaitTx;
//	 ShowMessage(IntToStr(TXComPortNo));
//TXComPortNo = 22;
//	 OpenTxPort(TXComPortNo);
//	 OpenTxPort(22);


//	 AnsiString xx = GetTxVersion();

//	 ShowMessage("Version = " + xx);


//	CloseTxPort();



//OpenTxPort(22);

	//txOpenFile("testgc.txt");
//
//	if (SetManualMode())
//	{
//		while (!ScanAndPowerON(20,4,0,0))

//		ScanAndPowerON(50,8,0,0,0);

//		{
//			Application->ProcessMessages();
//		}
//		ShowMessage("Done");
//	}
//	else
//	{
		// Do Nothing
//		 ShowMessage("Not Done");
//	} // End If

//	txCloseFile();

//	CloseTxPort();
//WaitTx = false;
//while(!StartScanPowerONMain())
//{
//	// Wait
//}

//ScanAndPowerONOnceInit();

//ShowMessage("Done");

//while(!WaitTx)
//{
//	if(StartScanPowerONMain())
//	{
//		WaitTx = true;
//	}
//	else
//	{
//		 WaitTx = false;
//	}  // End If
	//Wait
//}


}
//---------------------------------------------------------------------------
bool __fastcall TmainForm::StartScanPowerONMain()
{

	if( TXComPortNo == 0 )
	{
	  ShowMessage("No Tx Comport Set");
	}
	else
	{
		if (SetManualMode())
		{

/*
			while (!ScanAndPowerON(20,4,0,0,0))
			{
				Application->ProcessMessages();
			}
*/

	//		ShowMessage("Done");
			return true;
		}
		else
		{
			// Do Nothing
	//		 ShowMessage("Not Done");
			return false;
		} // End If

	} // End If
	return false;
}

//---------------------------------------------------------------------------
bool __fastcall TmainForm::StartScanPowerOFFMain()
{

	if( TXComPortNo == 0 )
	{
	  ShowMessage("No Tx Compoert Set");
	}
	else
	{
		if (SetManualMode())
		{

			while (!ScanAndPowerOFF(50,8,0,0))
			{
				Application->ProcessMessages();
			}
	//		 ShowMessage("Done");
			return true;
		}
		else
		{
			// Do Nothing
	//		 ShowMessage("Not Done");
			return false;
		} // End If
	} // End If

	return false;
}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Button3Click(TObject *Sender)
{


//ebFilename->Text = ebFilename->Text + "xx";

//	bool test = SetUpForm2SeqData(1);

//cbELoadCommands->ItemIndex = 2;
/*
	 if (PlotterON())
	 {
		ShowMessage("green");
	 }
	 else
	 {
		ShowMessage("Not ON");
	 }
*/
/*
bool TxBusy;
TxBusy = ScanAndPowerONOnce();

if (TxBusy)
{
	 ShowMessage("no");
}
else
{
	 ShowMessage("yes");
} //End If

//	while(!StartScanPowerOFFMain())
//	{
//		//Wait
//  	}
  //	 ShowMessage("Done");

*/

}

//---------------------------------------------------------------------------
bool __fastcall TmainForm::PlotterON()
{
	switch (shPlotterConnected->Brush->Color)
	{
		case clLime:
		{
			//ShowMessage("green");
			return true;
			break;
		}
		case clSilver:
		{
			//ShowMessage("Silver");
			return false;
			break;
		}
	default:
			//ShowMessage("Unknown");
			return false;
			break;
		;
	} // End Switch

}

//---------------------------------------------------------------------------
bool __fastcall TmainForm::PSUON()
{
	switch (shpPSUConnected->Brush->Color)
	{
		case clLime:
		{
			//ShowMessage("green");
			return true;
			break;
		}
		case clSilver:
		{
			//ShowMessage("Silver");
			return false;
			break;
		}
	default:
			//ShowMessage("Unknown");
			return false;
			break;
		;
	} // End Switch

}


//---------------------------------------------------------------------------
bool __fastcall TmainForm::ELoadON()
{
	switch (shpELoadConnected->Brush->Color)
	{
		case clLime:
		{
			//ShowMessage("green");
			return true;
			break;
		}
		case clSilver:
		{
			//ShowMessage("Silver");
			return false;
			break;
		}
	default:
			//ShowMessage("Unknown");
			return false;
			break;
		;
	} // End Switch

}


//---------------------------------------------------------------------------
bool __fastcall TmainForm::TxON()
{
	switch (shpTxConnected->Brush->Color)
	{
		case clLime:
		{
			//ShowMessage("green");
			return true;
			break;
		}
		case clSilver:
		{
			//ShowMessage("Silver");
			return false;
			break;
		}
	default:
			//ShowMessage("Unknown");
			return false;
			break;
		;
	} // End Switch

}


//---------------------------------------------------------------------------
bool __fastcall TmainForm::DAQON()
{
	switch (shpDAQConnected->Brush->Color)
	{
		case clLime:
		{
			//ShowMessage("green");
			return true;
			break;
		}
		case clSilver:
		{
			//ShowMessage("Silver");
			return false;
			break;
		}
	default:
			//ShowMessage("Unknown");
			return false;
			break;
		;
	} // End Switch

}


//---------------------------------------------------------------------------
void __fastcall TmainForm::SetModDepthOptions()
{

	cbModDepth->Clear();
	cbModDepth->Text = "FSK Mod Depth";
//	cbModDepth->AddItem("FSK Depth 0",NULL);
//	cbModDepth->AddItem("FSK Depth 1",NULL);
//	cbModDepth->AddItem("FSK Depth 2",NULL);
//	cbModDepth->AddItem("FSK Depth 3",NULL);
	cbModDepth->Items->Add("FSK Depth 0 (min)");
	cbModDepth->Items->Add("FSK Depth 1");
	cbModDepth->Items->Add("FSK Depth 2");
	cbModDepth->Items->Add("FSK Depth 3 (max)");

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::cbModDepthChange(TObject *Sender)
{

	SetModDepth();

}

//---------------------------------------------------------------------------

void __fastcall TmainForm::SetModDepth()
{

	AnsiString sPeram;

//	ShowMessage(IntToStr(cbModDepth->ItemIndex));
	switch (cbModDepth->ItemIndex)
		{
			case 0:
				FSKDepthCmd = "W6E0000";  // FSK depth = 0
				mainForm->edTxModDepth->Text = "Mod0";
				break;
			case 1:
				FSKDepthCmd = "W6E0100"; // FSK depth = 1
				mainForm->edTxModDepth->Text = "Mod1";
				break;
			case 2:
				FSKDepthCmd = "W6E0200"; // FSK depth = 2
				mainForm->edTxModDepth->Text = "Mod2";
				break;
			case 3:
				FSKDepthCmd = "W6E0300"; // FSK depth = 3
				mainForm->edTxModDepth->Text = "Mod3";
				break;
			default:
				FSKDepthCmd = "W6E0300"; // FSK depth = 3
				mainForm->edTxModDepth->Text = "Mod3";
				break;
		} // End Switch

	mainForm->edTxModDepthView->Text = mainForm->edTxModDepth->Text;

	sPeram = FSKDepthCmd;
	Transmit_Message( sPeram.c_str( ) );
	//ShowMessage(IntToStr(cbModDepth->ItemIndex));

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SendGetVersion()
{

	AnsiString sPeram;

	sPeram = "V";
	Transmit_Message( sPeram.c_str( ) );
   //	ShowMessage(IntToStr(cbModDepth->ItemIndex));

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SendScanAndPowerCmd()
{

	AnsiString sPeram;

	sPeram = "K";

	Transmit_Message( sPeram.c_str( ) );
   //	ShowMessage(IntToStr(cbModDepth->ItemIndex));

}

//---------------------------------------------------------------------------

void __fastcall TmainForm::StringGrid1DrawCell(TObject *Sender, int ACol, int ARow,
		  TRect &Rect, TGridDrawState State)
{

	if( State.Contains(gdFixed) )
	{
//		StringGrid1->Canvas->Brush->Color = static_cast<TColor>(RGB(155, 155, 100));
//		StringGrid1->Canvas->Font->Style = TFontStyles() << fsBold;
//		StringGrid1->Canvas->Font->Color = static_cast<TColor>(RGB(250, 245, 135));
//		StringGrid1->Canvas->Rectangle(Rect);
	}
	else if( State.Contains(gdSelected) )
	{
//		StringGrid1->Canvas->Brush->Color = static_cast<TColor>(RGB(255, 205, 155));
//		StringGrid1->Canvas->Font->Style = TFontStyles() >> fsBold;
//		StringGrid1->Canvas->Font->Color = clNavy;
//		StringGrid1->Canvas->FillRect(Rect);
	}
	else
	{
//		StringGrid1->Canvas->Brush->Color = clWhite;
//		String  Grid1->Canvas->Font->Color = clBlue;
//		StringGrid1->Canvas->FillRect(Rect);
	} // End If


//	StringGrid1->ColWidths[0] = 15;
//	StringGrid1->ColWidths[1] = 75;
//	StringGrid1->RowHeights[1] = 16;
//	StringGrid1->RowHeights[2] = 16;

	AnsiString text = StringGrid1->Cells[ACol][ARow];

	if (( ACol > 0 && ARow > 0 ) && (StringGrid1->Cells[ACol][ARow] != ""))
	{
		if (isNumber(StringGrid1->Cells[ACol][ARow]))
		{
			if (StrToFloat(StringGrid1->Cells[ACol][ARow]) >= StrToFloat(edGridColourThreshold->Text))
			{
		//		StringGrid1->Canvas->Brush->Color = clGreen;
				StringGrid1->Canvas->Brush->Color = static_cast<TColor>(RGB(105, 155, 100));
				StringGrid1->Canvas->Font->Color = clBlue;
				StringGrid1->Canvas->FillRect(Rect);

			} // End If
		}
		else
		{
			// Do Nothing
		}

	}  // End If

	StringGrid1->Canvas->TextRect(Rect, Rect.Left, Rect.Top, text);

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::edGridColourThresholdChange(TObject *Sender)
{
	StringGrid1->OnDrawCell;
}

//---------------------------------------------------------------------------
void __fastcall TmainForm::RunMiscInstrumentTest(bool manControl)
{

try
{

	if (((shpMISDataConnected->Brush->Color == clLime) && ( Gen_Comport->Open )) || manControl)
	{

		// Clear Data
		edLCRfreq1_Data1->Text = "";
		edLCRfreq2_Data1->Text = "";
		edLCRfreq3_Data1->Text = "";
		edLCRfreq4_Data1->Text = "";
		edLCRfreq5_Data1->Text = "";
		edLCRfreq6_Data1->Text = "";
		edLCRfreq7_Data1->Text = "";
		edLCRfreq8_Data1->Text = "";
		edLCRfreq9_Data1->Text = "";
		edLCRfreq10_Data1->Text = "";

		edLCRfreq1_Data2->Text = "";
		edLCRfreq2_Data2->Text = "";
		edLCRfreq3_Data2->Text = "";
		edLCRfreq4_Data2->Text = "";
		edLCRfreq5_Data2->Text = "";
		edLCRfreq6_Data2->Text = "";
		edLCRfreq7_Data2->Text = "";
		edLCRfreq8_Data2->Text = "";
		edLCRfreq9_Data2->Text = "";
		edLCRfreq10_Data2->Text = "";


	//  ShowMessage("St");
	//		Wait_mS(1000);
	//	ShowMessage("St");

		// Set Flag
	//	RunningComport = true;
		flgRunGenComport = true;

	   //		cbMiscInstrumentCmds->Items->Add(GWLCR_meas_test + "ac");

	   if (UpperCase(edGWLCR_Mode->Text) == "MAN")
	   {
			// Manual Mode
			// Set ac or r meas
			Gen_Comport->PutString(GWLCR_meas_test + edGWLCR_Meas->Text + "\n");
			Wait_mS(200);

			// Set ser or par
			Gen_Comport->PutString(GWLCR_meas_cct + edGWLCR_cct->Text + "\n");
			Wait_mS(100);

			// Set level
			Gen_Comport->PutString(GWLCR_meas_lev + edGWLCR_Level->Text + "\n");
			Wait_mS(100);

			// Set meas pars to get
			Gen_Comport->PutString(GWLCR_meas_func + edGWLCR_Comp->Text + "\n");
			Wait_mS(100);

			// Set Speed
			Gen_Comport->PutString(GWLCR_meas_speed + edGWLCR_Speed->Text + "\n");
			Wait_mS(100);

			//*************************************
			// Read All Freqs
			//*************************************
			MiscLCRFreqVal1Val2Result[1][0][0] = edTestFreqHz1of10->Text;
			MiscLCRFreqVal1Val2Result[2][0][0] = edTestFreqHz2of10->Text;
			MiscLCRFreqVal1Val2Result[3][0][0] = edTestFreqHz3of10->Text;
			MiscLCRFreqVal1Val2Result[4][0][0] = edTestFreqHz4of10->Text;
			MiscLCRFreqVal1Val2Result[5][0][0] = edTestFreqHz5of10->Text;
			MiscLCRFreqVal1Val2Result[6][0][0] = edTestFreqHz6of10->Text;
			MiscLCRFreqVal1Val2Result[7][0][0] = edTestFreqHz7of10->Text;
			MiscLCRFreqVal1Val2Result[8][0][0] = edTestFreqHz8of10->Text;
			MiscLCRFreqVal1Val2Result[9][0][0] = edTestFreqHz9of10->Text;
			MiscLCRFreqVal1Val2Result[10][0][0] = edTestFreqHz10of10->Text;

/*
	// Add Misc LCR Freq Data
	for (int i = 0; i < 10; i++)
	{
		if (MiscLCRFreqVal1Val2Result[i][0][0] == "")
		{
			// Do Nothing
		}
		else
		{
			cbGridTypeDisplay->AddItem(MiscLCRFreqVal1Val2Result[i][0][0], NULL);
		} // End If

	} // End For

*/

		if ((Arduino_FOD->Open) && (GetSeqGridColNofText(SeqGridKeyword[96][0]) < 0))
		{
			// OK
			for (int freqi = 1; freqi < 10; freqi++)
			{
				// Allow stopping of test
				if (flgRunGenComport)
				{
					// Do Nothing
				   LCR_ReadData();
				   SetFoDCoilResults(freqi);
				}
				else
				{
					break;
				} // End If

			}  // End For

		}
		else
		{
			LCR_ReadData();
		}  // End If

			// =================
			// Show On Misc Tab
			// =================

/*
			 edLCRfreq2_Data1->Text = MiscLCRFreqVal1Val2Result[2][1][0];
			 edLCRfreq3_Data1->Text = MiscLCRFreqVal1Val2Result[3][1][0];
			 edLCRfreq4_Data1->Text = MiscLCRFreqVal1Val2Result[4][1][0];
			 edLCRfreq5_Data1->Text = MiscLCRFreqVal1Val2Result[5][1][0];
			 edLCRfreq6_Data1->Text = MiscLCRFreqVal1Val2Result[6][1][0];
			 edLCRfreq7_Data1->Text = MiscLCRFreqVal1Val2Result[7][1][0];
			 edLCRfreq8_Data1->Text = MiscLCRFreqVal1Val2Result[8][1][0];
			 edLCRfreq9_Data1->Text = MiscLCRFreqVal1Val2Result[9][1][0];
			 edLCRfreq10_Data1->Text = MiscLCRFreqVal1Val2Result[10][1][0];

			 edLCRfreq1_Data2->Text = MiscLCRFreqVal1Val2Result[1][1][1];
			 edLCRfreq2_Data2->Text = MiscLCRFreqVal1Val2Result[2][1][1];
			 edLCRfreq3_Data2->Text = MiscLCRFreqVal1Val2Result[3][1][1];
			 edLCRfreq4_Data2->Text = MiscLCRFreqVal1Val2Result[4][1][1];
			 edLCRfreq5_Data2->Text = MiscLCRFreqVal1Val2Result[5][1][1];
			 edLCRfreq6_Data2->Text = MiscLCRFreqVal1Val2Result[6][1][1];
			 edLCRfreq7_Data2->Text = MiscLCRFreqVal1Val2Result[7][1][1];
			 edLCRfreq8_Data2->Text = MiscLCRFreqVal1Val2Result[8][1][1];
			 edLCRfreq9_Data2->Text = MiscLCRFreqVal1Val2Result[9][1][1];
			 edLCRfreq10_Data2->Text = MiscLCRFreqVal1Val2Result[10][1][1];

*/


//			ShowMessage("Done");

	   }
	   else
	   {
			//
		   if (UpperCase(edGWLCR_Mode->Text) == "GRAPH")
		   {
				// Graphic Mode
				ShowMessage("Graph Option not coded\nNot enough hours in the day to do this!");
		   }
		   else
		   {
				//
				ShowMessage("Unknown LCR Test Configuration Set (options = 'man' or 'graph')");
				//RunningComport = false;
		   } // End If

	   } // End If

	}
	else
	{
		  // Do Nothing
	} // End If

	//RunningComport = false;
}
catch (...)
{
//	ShowMessage("Error in Sub RunMiscInstrumentTest");
	MessageDlg(	"Error in Sub xx -> Error : " + String(GetLastError()), mtError, TMsgDlgButtons() << mbOK, 0);
}  // End Try

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::LCR_ReadData(void)
{

			//***
			//	Run 10 LCR Freq Measurements
			//***
			for (int i = 1; i < 11; i++)
			{
				if (flgRunGenComport)
				{
					// Do Nothing
				}
				else
				{
					// Stop Test
					break;
				} // End if

				if (MiscLCRFreqVal1Val2Result[i][0][0] == "")
				{
					// Do Nothing
				}
				else
				{
					// OK to Process

					AnsiString Freqi = MiscLCRFreqVal1Val2Result[i][0][0];

					//*************************************
					// Set freq i
					//*************************************
					Gen_Comport->PutString(GWLCR_meas_freq + Freqi + "\n");
					Wait_mS(100);

					// Clear serial receive buffer first
					edMiscInstruMsg->Text = "";
					Gen_Comport->PutString(GWLCR_meas_trig + "\n");
					Wait_mS(100);

					int WaitTimeLCRMeas = 0;

					 if ((UpperCase(edGWLCR_Speed->Text) == "MED") || (UpperCase(edGWLCR_Speed->Text) == "SLOW"))
					 {
						// Allow extra reading time
						WaitTimeLCRMeas = 800;
					 }
					 else
					 {
					   // Do Nothing
						WaitTimeLCRMeas = 400;

					 } // End If

						// Wait for reply
						for (int i = 0; i < WaitTimeLCRMeas; i++)
						{
							// Exit Waiting for reply if Stopped
							if (flgRunGenComport)
							{
								// Do Nothing
							}
							else
							{
								// Stop Test
								break;
							} // End if

							Wait_mS(10);

							if (edMiscInstruMsg->Text == "")
							{
								// Do Nothing
							}
							else
							{
								// msg received
								//RunningComport = false;
								break;
							} // End If
						} // End For

					if (edMiscInstruMsg->Text == "")
					{
						// NO result timed out!!!!!!!
						//RunningComport = false;
					}
					else
					{
						//ShowMessage(edMiscInstruMsg->Text);
						// Freq
						//ShowMessage(edTestFreqHz1of10->Text);
						MiscLCRFreqVal1Val2Result[i][0][0] = Freqi;
		//				TestDataType
						// First Par
						AnsiString FreqVar1Result = SplitStringFrom0(edMiscInstruMsg->Text,0,',');
		//				ShowMessage(FreqVar1Result);
						MiscLCRFreqVal1Val2Result[i][i][0] = FreqVar1Result;
						// second par
						AnsiString FreqVar2Result = SplitStringFrom0(edMiscInstruMsg->Text,1,',');
		//				ShowMessage(FreqVar2Result);
						MiscLCRFreqVal1Val2Result[i][i][i] = FreqVar2Result;

						switch(i)
						 {
						case 1:
						 {
							 edLCRfreq1_Data1->Text = MiscLCRFreqVal1Val2Result[1][1][0];
							 edLCRfreq1_Data2->Text = MiscLCRFreqVal1Val2Result[1][1][1];
							break;
						 }
						case 2:
						 {
							 edLCRfreq2_Data1->Text = MiscLCRFreqVal1Val2Result[2][2][0];
							 edLCRfreq2_Data2->Text = MiscLCRFreqVal1Val2Result[2][2][2];
							break;
						 }
						case 3:
						 {
							 edLCRfreq3_Data1->Text = MiscLCRFreqVal1Val2Result[3][3][0];
							 edLCRfreq3_Data2->Text = MiscLCRFreqVal1Val2Result[3][3][3];
							break;
						 }
						case 4:
						 {
							 edLCRfreq4_Data1->Text = MiscLCRFreqVal1Val2Result[4][4][0];
							 edLCRfreq4_Data2->Text = MiscLCRFreqVal1Val2Result[4][4][4];
							break;
						 }
						case 5:
						 {
							 edLCRfreq5_Data1->Text = MiscLCRFreqVal1Val2Result[5][5][0];
							 edLCRfreq5_Data2->Text = MiscLCRFreqVal1Val2Result[5][5][5];
							break;
						 }
						case 6:
						 {
							 edLCRfreq6_Data1->Text = MiscLCRFreqVal1Val2Result[6][6][0];
							 edLCRfreq6_Data2->Text = MiscLCRFreqVal1Val2Result[6][6][6];
							break;
						 }
						case 7:
						 {
							 edLCRfreq7_Data1->Text = MiscLCRFreqVal1Val2Result[7][7][0];
							 edLCRfreq7_Data2->Text = MiscLCRFreqVal1Val2Result[7][7][7];
							break;
						 }
						case 8:
						 {
							 edLCRfreq8_Data1->Text = MiscLCRFreqVal1Val2Result[8][8][0];
							 edLCRfreq8_Data2->Text = MiscLCRFreqVal1Val2Result[8][8][8];
							break;
						 }
						case 9:
						 {
							 edLCRfreq9_Data1->Text = MiscLCRFreqVal1Val2Result[9][9][0];
							 edLCRfreq9_Data2->Text = MiscLCRFreqVal1Val2Result[9][9][9];
							break;
						 }
						case 10:
						 {
							 edLCRfreq10_Data1->Text = MiscLCRFreqVal1Val2Result[10][10][10];
							 edLCRfreq10_Data2->Text = MiscLCRFreqVal1Val2Result[10][10][10];
							break;
						 }
						default:
							break;
						 }  // End Switch


					} // End If
					//*************************************
					// Ends freq i
					//*************************************

				} // End If

			}  // End For



}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SetFoDCoilResults(int CoilNumber)
{
int Offsetfor_r;

	for (int freqi = 1; freqi < 11; freqi++)
	{
		Offsetfor_r = freqi + 10;
		// Get L for 10 freq
		FoDCoilResult[CoilNumber][freqi] = MiscLCRFreqVal1Val2Result[freqi][freqi][0];
		// Get r for 10 freq
		FoDCoilResult[CoilNumber][Offsetfor_r] = MiscLCRFreqVal1Val2Result[freqi][freqi][freqi];

	}  // End For

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::RunMiscInstrumentTest_Ti_LDC(void)
{


if (cbTiEvmLDC_on_Off->Checked)
{
	// Run


   lbTiEVMStatus->Caption = "About to Read Values";
	AnsiString LastNumber = SplitStringFrom0(ReadCSV(edTiLCDEvmFileAddress->Text, true),0,',');

	for (int i = 0; i < 5; i++)
	{
		Wait_mS(200);

	}  // End For

	AnsiString NewNumber = SplitStringFrom0(ReadCSV(edTiLCDEvmFileAddress->Text, true),0,',');


		AnsiString TiEVMData = ReadCSV(edTiLCDEvmFileAddress->Text, true);

		MiscTiEVM_LDCResult[1] = SplitStringFrom0(TiEVMData,3,',');
		MiscTiEVM_LDCResult[2] = SplitStringFrom0(TiEVMData,4,',');
		MiscTiEVM_LDCResult[3] = SplitStringFrom0(TiEVMData,5,',');
		MiscTiEVM_LDCResult[4] = SplitStringFrom0(TiEVMData,6,',');
		MiscTiEVM_LDCResult[5] = SplitStringFrom0(TiEVMData,11,',');
		MiscTiEVM_LDCResult[6] = SplitStringFrom0(TiEVMData,12,',');
		MiscTiEVM_LDCResult[7] = SplitStringFrom0(TiEVMData,13,',');
		MiscTiEVM_LDCResult[8] = SplitStringFrom0(TiEVMData,14,',');


		edTiEVM_L0->Text = "";
		edTiEVM_L1->Text = "";
		edTiEVM_L2->Text = "";
		edTiEVM_L3->Text = "";

		edTiEVM_LFreq0->Text = "";
		edTiEVM_LFreq1->Text = "";
		edTiEVM_LFreq2->Text = "";
		edTiEVM_LFreq3->Text = "";

		edTiEVM_L0->Text = MiscTiEVM_LDCResult[1];
		edTiEVM_L1->Text = MiscTiEVM_LDCResult[2];
		edTiEVM_L2->Text = MiscTiEVM_LDCResult[3];
		edTiEVM_L3->Text = MiscTiEVM_LDCResult[4];

		edTiEVM_LFreq0->Text = MiscTiEVM_LDCResult[5];
		edTiEVM_LFreq1->Text = MiscTiEVM_LDCResult[6];
		edTiEVM_LFreq2->Text = MiscTiEVM_LDCResult[7];
		edTiEVM_LFreq3->Text = MiscTiEVM_LDCResult[8];


	   lbTiEVMStatus->Caption = "Values Read";


	if (LastNumber == NewNumber)
	{
		// No change
		FailureCount = FailureCount + 1;
		if (FailureCount > 10)
		{
			FailureCount = 0;
			ShowMessage("CSV file Data has not changed in 1 second.\nCheck file update rate < 1 second!");
		}
		else
		{
			// Do Nothing
		}  // End If

	}
	else
	{
		// OK
	} // End If


}
else
{
	// Do Nothing
} // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::btRunMiscInstruTtestClick(TObject *Sender)
{


	lbStatusGWLCR->Caption = "STATUS: START LCR";

	 RunMiscInstrumentTest(true);

	 lbStatusGWLCR->Caption = "STATUS: LCR DONE";
//	ShowMessage("Do not press me fool!");


}
//---------------------------------------------------------------------------


void __fastcall TmainForm::Button2Click(TObject *Sender)
{

int RXComPortNo = UserSelectComport2("Select RX Com-Port", "PortRX", 115200);
//TXComPortNo = 26;
mainForm->edRxComport->Text = IntToStr(RXComPortNo);

//Qi_Comport->Open = false;

 OpenTxPort(RXComPortNo);

 AnsiString RxVers = GetTxVersion();
 mainForm->edRxVersion->Text = RxVers;
 mainForm->edRxVers->Text = RxVers;

//	 ShowMessage("Version = " + xx);

CloseTxPort();

//Qi_Comport->Open = true;

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btUpdateFilenameClick(TObject *Sender)
{

	AnsiString TxText;
	AnsiString RxText;
	AnsiString PlotSettings;
	AnsiString CSVFilename;

	//ShowMessage(ReplaceChar("testedsdggvdffegh", 'e', '_')); // replace the space with

	TxText = cbTxMakeOptions->Text + "_Tx#" + mainForm->edTxNumber->Text + "_" + mainForm->edTxVersion->Text + "_b" + mainForm->edTxBuild->Text + "_" + mainForm->edTxVDC->Text + "VDC";
	RxText = cbRxMakeOptions->Text + "_Rx#" + mainForm->edRxNumber->Text + "_" + mainForm->edRxVers->Text + "_b" + mainForm->edRxBuild->Text;
	PlotSettings = "z" + cbRxZHeight->Text + "_" + mainForm->cbRxOrientation->Text + "_" + mainForm->edRxLoad->Text + "_" + mainForm->edTestWait->Text + "S_" + mainForm->edRxStep->Text + "step_" + mainForm->edTxModDepth->Text;

	CSVFilename = TxText + "_" + RxText + "_" + PlotSettings;

	mainForm->ebFilename->Text = (ReplaceChar(CSVFilename, '.', '_'));


	switch(MessageDlg("Yes = Save New File Now (needs Open Seq File)\nNo = Manually Append to Last File\nCANCEL = Exit (Seq will use this filename)", mtInformation, mbYesNoCancel, 0))
	 {
		case mrYes:
		 {
			//ShowMessage("Yes");

			//Create NEW test file name
			testAdmin.testFile = CreateFileName();

			//Create test file name
			SaveGridData2CSV(0, true);  // Write Titles
			SaveGridData2CSV(1, true);  // Write Titles

			break;
		 }
		case mrNo:
		 {

			//Create test file name
			SaveGridData2CSV(1, false);

			break;
		 }
		default:

			break;
	 }  // End Switch


//ShowMessage();

}
//---------------------------------------------------------------------------


void __fastcall TmainForm::Button5Click(TObject *Sender)
{
	mainForm->ebFilename->Text = "HB_Tx#xx_vx_x_x_19V_HB_Rx#xx_vx_xx_x_3mm_VERT_5W_res_12sec_5mmStep_Mod0";
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Button6Click(TObject *Sender)
{

//FileListBox1->Update();
SaveIniFileData(true);

/* cbSeqIniFilenames->Clear();
//cbSeqIniFilenames->Items->Add("");

for (int i = 0; i < FileListBox1->Items->Count; i++)
  {
 //		ShowMessage(FileListBox1->FileName);
		cbSeqIniFilenames->Items->Add(FileListBox1->FileName);
  }

*/
//cbInifilenames->Clear();
//cbInifilenames->Items->Add("erer");


//ShowMessage(FileListBox1->FileName);
//ShowMessage(ChangeFileExt(Application->ExeName, ".INI"));


//	StringGrid1

//	SeqColourCEllGREEN(1, 1);

//	StrGridSeqTest->Cells[1][1] = IntToStr(1);

//	TRect Recto = StrGridSeqTest->CellRect(0 , 1);
//	int Area = Recto.Width() * Recto.Height();
//	StrGridSeqTest->Canvas->Brush->Color = clGreen;
//	StrGridSeqTest->Canvas->FillRect(Recto);

}
//---------------------------------------------------------------------------


void __fastcall TmainForm::cbHoleRetryClick(TObject *Sender)
{
	if (cbHoleRetry->Checked)
	{
		// Do Nothing
		mainForm->lbRetryNotice->Visible = true;
	}	
	else
	{
	 //	ebHoleRetryNo->Text = strRetryCountSet;
		mainForm->lbRetryNotice->Visible = false;
	} // End If
	
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnGoToStepEndClick(TObject *Sender)
{

	int ColSteps = 1 + StrToInt(ebXSteps2->Text);
	int RowSteps = 1 + StrToInt(ebYSteps2->Text);

	MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(ed3DZHeight2->Text), 1);

/*

int StepSizemm = StrToInt(ebStepSize->Text);

	XYPlotter_setpoint_t tmpSetpoint;
	tmpSetpoint.XPos = StrToInt(ebXSetpointStart->Text) + StrToInt(ebXSteps->Text) * StepSizemm;
	tmpSetpoint.YPos = StrToInt(ebYSetpointStart->Text) + StrToInt(ebYSteps->Text) * StepSizemm;

	Plotter->UpdateSetpoint(tmpSetpoint);
	//Update user labels
	updatePosLabels(this);

	Plotter->Set3dZHeight(StrToFloat(ed3DZHeight->Text));
*/

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::StringGrid2DrawCell(TObject *Sender, int ACol, int ARow,
		  TRect &Rect, TGridDrawState State)
{

if( State.Contains(gdFixed) )
	{
//		StringGrid1->Canvas->Brush->Color = static_cast<TColor>(RGB(155, 155, 100));
//		StringGrid1->Canvas->Font->Style = TFontStyles() << fsBold;
//		StringGrid1->Canvas->Font->Color = static_cast<TColor>(RGB(250, 245, 135));
//		StringGrid1->Canvas->Rectangle(Rect);
	}
	else if( State.Contains(gdSelected) )
	{
//		StringGrid1->Canvas->Brush->Color = static_cast<TColor>(RGB(255, 205, 155));
//		StringGrid1->Canvas->Font->Style = TFontStyles() >> fsBold;
//		StringGrid1->Canvas->Font->Color = clNavy;
//		StringGrid1->Canvas->FillRect(Rect);
	}
	else
	{
//		StringGrid1->Canvas->Brush->Color = clWhite;
//		String  Grid1->Canvas->Font->Color = clBlue;
//		StringGrid1->Canvas->FillRect(Rect);
	} // End If

//	StringGrid1->ColWidths[0] = 15;
//	StringGrid1->ColWidths[1] = 75;
//	StringGrid1->RowHeights[1] = 16;
//	StringGrid1->RowHeights[2] = 16;

	AnsiString text = StringGrid2->Cells[ACol][ARow];

	if( ACol > 0 && ARow > 0  )
	{
		if (StringGrid1->Cells[ACol][ARow] > StrToInt(edGridColourThreshold->Text))
		{
	//		StringGrid1->Canvas->Brush->Color = clGreen;
			StringGrid2->Canvas->Brush->Color = static_cast<TColor>(RGB(105, 105, 100));
			StringGrid2->Canvas->Font->Color = clBlue;
			StringGrid2->Canvas->FillRect(Rect);

		} // End If
	}  // End If
	StringGrid2->Canvas->TextRect(Rect, Rect.Left, Rect.Top, text);

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Big_ViewClick(TObject *Sender)
{

	//Big_View->Checked = false;
	SetFormSize();

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SetFormSize()
{
	float ScaleWidth, ScaleWidth2, ScaleHeight, ScaleHeight2;
	ScaleWidth = 1.8;
	ScaleWidth2 = 1.85;
	ScaleHeight = 1.3;
	ScaleHeight2 = 1.4;


	StringGrid3->Visible = false;

	if (Big_View->Checked)
	{
		// Change Small Size


		//mainForm->Width = 652;
		//mainForm->Height = 716;
		mainForm->Width = 652; //mainForm->Width / ScaleWidth;
//		mainForm->Height = 716; // mainForm->Height / ScaleHeight;
		mainForm->Height = 820; // mainForm->Height / ScaleHeight;

		TabControl1->Width = 625; //TabControl1->Width / ScaleWidth2;
//		TabControl1->Height = 641; //TabControl1->Height / ScaleHeight;
		TabControl1->Height = 746; //TabControl1->Height / ScaleHeight;

		Panel3->Width = 620; //Panel3->Width / ScaleWidth2;
//		Panel3->Height = 601; //Panel3->Height / ScaleHeight;
		Panel3->Height = 699; //Panel3->Height / ScaleHeight;

		StringGrid1->Width = 600; //StringGrid1->Width / ScaleWidth2;
		StringGrid1->Height = 388; //StringGrid1->Height / ScaleHeight;

		StringGrid2->Width = 600; //StringGrid2->Width / ScaleWidth2;
		StringGrid2->Height = 388; //StringGrid2->Height / ScaleHeight;

		Chart1->Width = 600; // Chart1->Width / ScaleWidth2;
		Chart1->Top = 406; //Chart1->Top / ScaleHeight;

		//btnProgramClose->Left = btnProgramClose->Left / (ScaleWidth2 * 1.05);
		btnProgramClose->Top = 720; //btnProgramClose->Top / ScaleHeight;
		btnProgramClose->Left = 523;

		// Grid Colour
		Label21->Top = 148;
		edGridColourThreshold->Top = 144;

		Big_View->Checked = false;

	   // Seq Grid
		StrGridSeqTest->Top = 559;
		StrGridSeqTest->Width = 600;
//		StrGridSeqTest->Height = 335;
		StrGridSeqTest->Height = 97;
		StrGridSeqTest->ScrollBars = ssBoth;

		FileListBox1->Top = 648;
		cbSeqIniFilenames->Top = 648;

		lbIniFilename->Top=672;

		//rteIniFile->Visible = true;

	}
	else
	{
		// Change BIG
		mainForm->Width = mainForm->Width * ScaleWidth; //1.84;
		mainForm->Height = mainForm->Height * ScaleHeight * .92;

		TabControl1->Width = TabControl1->Width * ScaleWidth2;
		TabControl1->Height = TabControl1->Height * ScaleHeight * .93;

		Panel3->Width = Panel3->Width * ScaleWidth2;
		Panel3->Height = Panel3->Height * ScaleHeight * .93;

		StringGrid1->Width = StringGrid1->Width * ScaleWidth2;
		StringGrid1->Height = 388 * 1.4; //StringGrid1->Height * ScaleHeight;
//		StringGrid1->Height = 388 * 1.45; //StringGrid1->Height * ScaleHeight;

		StringGrid2->Width = StringGrid2->Width * ScaleWidth2;
		StringGrid2->Height = 388 * 1.4; // StringGrid2->Height * ScaleHeight;

		Chart1->Width = 589 * 1.84; //Chart1->Width * ScaleWidth;
		Chart1->Top = Chart1->Top * ScaleHeight;

		//btnProgramClose->Top = btnProgramClose->Top * ScaleHeight;
		btnProgramClose->Top = btnProgramClose->Top * ScaleWidth2 * .66; //713 + 60;
		btnProgramClose->Left = btnProgramClose->Left * (ScaleWidth2 * 1.08);

		// Grid Colour
		//Label21->Top = Label21->Top * ScaleHeight;
		//edGridColourThreshold->Top = edGridColourThreshold->Top * ScaleHeight;

		Big_View->Checked = true;

//		StrGridSeqTest->Top = StrGridSeqTest->Top * .98;
		StrGridSeqTest->Top =  StrGridSeqTest->Top * ScaleHeight;
		StrGridSeqTest->Width = StrGridSeqTest->Width * ScaleWidth2; //1059;
		//StrGridSeqTest->Height = 455;
		//StrGridSeqTest->Height = 97;

		FileListBox1->Top = FileListBox1->Top * ScaleHeight * .96;
		cbSeqIniFilenames->Top = cbSeqIniFilenames->Top * ScaleHeight * .96;

		lbIniFilename->Top = lbIniFilename->Top * ScaleHeight * .95; //672;

		//rteIniFile->Visible = false;

	} // End If

}


//---------------------------------------------------------------------------
void __fastcall TmainForm::SeqGrid_Init()
{

	//ColMax = StrToInt(ebXSteps->Text);
	//RowMax = StrToInt(ebYSteps->Text);

	SeqColMax = 20;
	SeqRowMax = StrToInt(edSeqRowCount->Text);

	//***
	// Set Grid Limits
	//***
	StrGridSeqTest->ColCount = SeqColMax + 2;     // Include Title Col
  //	StrGridSeqTest->RowCount = SeqRowMax + 2;     // Includes Title Row
	StrGridSeqTest->RowCount = 3;     // Includes Title Row
   //	StrGridSeqTest->ColCount = SeqColMax + 2;     // Include Title Col
//	StrGridSeqTest->RowCount = SeqRowMax + 2;     // Includes Title Row

	// ***
	// Clear Grid
	// ***
	for (int iRow = 0; iRow < StrGridSeqTest->RowCount ; iRow++)
	{
		StrGridSeqTest->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
	   //	StrGridSeqTest->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
	}  // End For

	//***
	// Column Width
	//***
	StrGridSeqTest->DefaultColWidth = 100;
   //	StrGridSeqTest->DefaultColWidth = 100;
	for (int i = 0; i < SeqColMax + 2; i++)
	{
		StrGridSeqTest->ColWidths[i]  = 62;
	  //	StrGridSeqTest->ColWidths[i]  = 32;
	} // End for
	//***
	// End Column Width
	//***
	// NOTE do NOT put commas in title!!!!
	AnsiString ColTitles[20];
	ColTitles[0] = "1_No.";
	ColTitles[1] = "2_Seq Ctrl.";
	ColTitles[2] = "3_Xmm";
	ColTitles[3] = "4_Ymm";
	ColTitles[4] = "5_Zmm";
	ColTitles[5] = "6_Rot 1_0 45 90";
	ColTitles[6] = "7_Rot 2_";
	ColTitles[7] = "8_PSU_VDC";
	ColTitles[8] = "9_PSU_Imax_Amps";
	ColTitles[9] = "10_eload Type_CC or Res";
	ColTitles[10] = "11_eload Value_eg 5";
	ColTitles[11] = "12_OFF Time_mS";
	ColTitles[12] = "13_ON Time_mS";
	ColTitles[13] = "14_Timestamp";
	ColTitles[14] = "15_V in";
	ColTitles[15] = "16_I In";
	ColTitles[16] = "17_V out";
	ColTitles[17] = "18_I out";
	ColTitles[18] = "19_Efficiency";
	ColTitles[19] = "20_";

	// Number Title Columns
	for (int iCol = 0; iCol < SeqColMax; iCol++)
	{
		StrGridSeqTest->Cells[iCol + 1][0] = ColTitles[iCol];
		//StrGridSeqTest->Cells[iCol + 1][0] = iCol;
	}  // End For

	NumberSeqGridRows();

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::ClearEff_Sig_Grids()
{
	// ***
	// Clear Grid
	// ***
	for (int iRow = 1; iRow < StringGrid1->RowCount ; iRow++)
	{
		//StrGridSeqTest->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
		AnsiString Col1Text = StringGrid1->Cells[0][iRow];
		StringGrid1->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
		StringGrid1->Cells[0][iRow] = Col1Text;

		AnsiString Col1Text2 = StringGrid2->Cells[0][iRow];
		StringGrid2->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
		StringGrid2->Cells[0][iRow] = Col1Text2;

	   //	StrGridSeqTest->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
	}  // End For

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::NumberSeqGridRows()
{


	// ***
	// Number Rows
	// ***
	int LastRow;
	for (int iRow = 0; iRow < StrGridSeqTest->RowCount; iRow++)
	{
		if (iRow == 0)
		{
			//StrGridSeqTest->Cells[1][iRow] = "";
		}
		else
		{
			StrGridSeqTest->Cells[0][iRow] = IntToStr(iRow);

		} // End If
		LastRow = iRow;
	}  // End For
	// Last Row end

	StrGridSeqTest->Cells[2][LastRow] = "end";

}
//---------------------------------------------------------------------------
void __fastcall TmainForm::lbControlMessageClick(TObject *Sender)
{
	lbControlMessage->Caption = "";
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnLoadUnloadClick(TObject *Sender)
{

if ( StrToInt(edLoadZmm2->Text) < StrToInt(ed3DZHeight2->Text))
{
	ShowMessage("WARNING: Check Z height as < Test Z height");
}
else
{
	int ColSteps = 1 + StrToInt(edLoadXmm2->Text) / StrToInt(ebStepSize2->Text);
	int RowSteps = 1 + StrToInt(edLoadYmm2->Text) / StrToInt(ebStepSize2->Text);

	MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(edLoadZmm2->Text), 1);

} // Edn If


/*
int StepSizemm = StrToInt(ebStepSize->Text);

if (MessageDlg("This is for Loading Unloading Rx. Location above buton = Xmm, Ymm, Zmm\nYES = OK\nNO= Exit", mtInformation, mbYesNo, 0) == mrYes)
	 {
		//ShowMessage("Yes");
		XYPlotter_setpoint_t tmpSetpoint;
		tmpSetpoint.XPos = StrToInt(edLoadXmm->Text);
		tmpSetpoint.YPos = StrToInt(edLoadYmm->Text);

		Plotter->UpdateSetpoint(tmpSetpoint);
		//Update user labels
		updatePosLabels(this);

		Plotter->Set3dZHeight(StrToFloat(edLoadZmm->Text));

	 }
	 else
	 {
		 //ShowMessage("No");
	 } // End If

*/

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::edRxVoltReScanLimitChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Retry", "Rx_VDC_Retry", edRxVoltReScanLimit->Text);
	TestLogAdd("Re_try Rx volt min " + edRxVoltReScanLimit->Text + "VDC");

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::edSigStrenReScanLimitChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Retry", "Sig_Stren", edSigStrenReScanLimit->Text);
	TestLogAdd("Re_try Sig Strength" + edSigStrenReScanLimit->Text);

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::edRxVoltFinalTestLimitChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Retry", "Rx_VDC_Final", edRxVoltFinalTestLimit->Text);
	TestLogAdd("End Re_try Rx volt min " + edRxVoltFinalTestLimit->Text + "VDC");

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::edLoadXmmChange(TObject *Sender)
{

   if (edLoadXmm2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		edLoadXmm2->Text = "280";
   }
   else
   {
	Reg_WriteString(sREG, "Load_Unload", "Xmm", edLoadXmm2->Text);
	TestLogAdd("Load_Unload Xmm Location " + edLoadXmm2->Text + "mm");
   } // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::edLoadYmmChange(TObject *Sender)
{
   if (edLoadYmm2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		edLoadYmm2->Text = "120";
   }
   else
   {

	Reg_WriteString(sREG, "Load_Unload", "Ymm", edLoadYmm2->Text);
	TestLogAdd("Load_Unload Ymm Location " + edLoadYmm2->Text + "mm");
	}  // End If
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Help_RegEditClick(TObject *Sender)
{
		  ShowMessage("Use RegEdit to edit Settings in HKEY_CURRENT_USERS/Software/PbP/");
}
//---------------------------------------------------------------------------


void __fastcall TmainForm::cbPlotterTypeChange(TObject *Sender)
{

	if (cbPlotterType->ItemIndex == 0)
	{
		// ***
		// 2d Plotter Selected
		// ***

/*		if( PSU_Comport->Open == true )
		{
			PSU_Comport->FlushInBuffer();
			PSU_Comport->FlushOutBuffer();
			PSU_Comport->Open = false;

		}
		else
		{

		} // End If
*/
		Plotter->SetPlotterType2D(true);
		edPSU3DPlot2->Visible = false;
//		lb3DPSU->Visible = false;
		pnl3DPlot->Visible = false;


		btnPlotComportOpen2->Enabled = true;
		//pnlPlotterControl->Visible = true;

	}
	else if (cbPlotterType->ItemIndex == 1)
	{

		btnPlotComportOpen2->Enabled = true;

		pnl3DPlot->Visible = true;
		cbEVK->Checked = false;
		//cbe_LoadSelect->Checked = true;
//		pnlPlotterControl->Visible = true;

 //		ShowMessage("NOTES:\n1. You Need Set 3D Plotter comport (You may need to WAIT)\n2. You Need to go to Maint Sheet and Set PSU comport\n4. You Need to go to Maint Sheet and Set e-load comport");

/*
		// ***
		// 3d plotter Selected
		// ***
		Plotter->SetPlotterType2D(false);
		// Power supply uses Plotter 2D controller

//		int PSUComPort = UserSelectComport2("Select PSU Com-Port for Plotter ", "PortPSU", 57600);

		UserSelectComport(PSU_Comport,"Select PSU Comport", "PortPSU", 57600);

		if( PSU_Comport->Open == false )
		{
			PSU_Comport->FlushInBuffer();
			PSU_Comport->FlushOutBuffer();
			PSU_Comport->Open = false;
//			edPSU3DPlot->Visible = false;
//			lb3DPSU->Visible = false;
			pnl3DPlot->Visible = false;
		}
		else
		{
			pnl3DPlot->Visible = true;
//			lb3DPSU->Visible = true;
//			edPSU3DPlot->Visible = true;
			edPSU3DPlot->Text = IntToStr(PSU_Comport->ComNumber);

//			PSU_Comport->PutString("");

/*			enLoadRelay3D(false, 0);
			enLoadRelay3D(false, 1);
			enLoadRelay3D(false, 2);
Sleep(500);

			enLoadRelay3D(true, 0);
			enLoadRelay3D(true, 1);
			enLoadRelay3D(true, 2);
Sleep(500);
			enLoadRelay3D(false, 0);
			enLoadRelay3D(false, 1);
			enLoadRelay3D(false, 2);
*/
//		} // End If

	}
	else
	{
			// ***
			// MOA Selected cbPlotterType->ItemIndex == 1
			// ***
			pnl3DPlot->Visible = true;
			cbEVK->Checked = false;
			//cbe_LoadSelect->Checked = true;


			ShowMessage("NOTES:\n1. You Need Set 3D Plotter comport (You may need to WAIT)\n2. You Need to go to Maint Sheet and Set PSU comport\n4. You Need to go to Maint Sheet and Set e-load comport");

	} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::enLoadRelay3D(bool state,int iLoad)
{

//enPowerRelay
char cmdBuff[5];
	cmdBuff[0] = (char)XYPLOTTER_CMD_SET_RELAY;
	cmdBuff[1] = (state)? '1' : '0';

	switch (iLoad)
		{
			case 0x00:
			 cmdBuff[2] = XYPLOTTER_6R8_LOAD_RELAY;
			break;

			case 0x01:
			 cmdBuff[2] = XYPLOTTER_5R0_LOAD_RELAY;
			break;

			case 0x02:
			 cmdBuff[2] = XYPLOTTER_3R3_LOAD_RELAY;
			break;

			default:
				  //Opps this is an error. Do nothing.
				  return;

		}

	IntToHexByteBuff(xorCheckSum(&cmdBuff[0], 3),&cmdBuff[3], 2);

	PSU_Comport->PutChar(STX);
	PSU_Comport->PutChar(cmdBuff[0]);
	PSU_Comport->PutChar(cmdBuff[1]);
	PSU_Comport->PutChar(cmdBuff[2]);
	PSU_Comport->PutChar(cmdBuff[3]);
	PSU_Comport->PutChar(cmdBuff[4]);
	PSU_Comport->PutChar(ETX);

}
// ---------------------------------------------------------------------------
// Send a relay command to the plotter, based on the bool intput
// ---------------------------------------------------------------------------
void __fastcall TmainForm::enRefRelay3D(bool state) //true = on
{

	char cmdBuff[5];
	cmdBuff[0] = (char)XYPLOTTER_CMD_SET_RELAY;
	cmdBuff[1] = (state)? '1' : '0';
	cmdBuff[2] = XYPLOTTER_REF_RELAY;
	IntToHexByteBuff(xorCheckSum(&cmdBuff[0], 3),&cmdBuff[3], 2);

	PSU_Comport->PutChar(STX);
	PSU_Comport->PutChar(cmdBuff[1]);
	PSU_Comport->PutChar(cmdBuff[2]);
	PSU_Comport->PutChar(cmdBuff[3]);
	PSU_Comport->PutChar(cmdBuff[4]);
	PSU_Comport->PutChar(ETX);

}
// ---------------------------------------------------------------------------
// Send a relay command to the plotter, based on the bool intput
// To power the PSU
// ---------------------------------------------------------------------------
void __fastcall TmainForm::enPowerRelay3D(bool state) //true = on
{
	char cmdBuff[5];
	cmdBuff[0] = (char)XYPLOTTER_CMD_SET_RELAY;
	cmdBuff[1] = (state)? '1' : '0';
	cmdBuff[2] = XYPLOTTER_POWER_RELAY;
	IntToHexByteBuff(xorCheckSum(&cmdBuff[0], 3),&cmdBuff[3], 2);

	PSU_Comport->PutChar(STX);
	PSU_Comport->PutChar(cmdBuff[0]);
	PSU_Comport->PutChar(cmdBuff[1]);
	PSU_Comport->PutChar(cmdBuff[2]);
	PSU_Comport->PutChar(cmdBuff[3]);
	PSU_Comport->PutChar(cmdBuff[4]);
	PSU_Comport->PutChar(ETX);

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::ed3DZHeightChange(TObject *Sender)
{
//	Plotter->Set3dZHeight(StrToInt(ed3DZHeight->Text));
	Reg_WriteString(sREG, "Cords", "Z_SetPoint", ed3DZHeight2->Text);
	TestLogAdd("New [Z SetPoint] : " + ed3DZHeight2->Text);

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::cbOrienZRotationChange(TObject *Sender)
{
		//Plotter->SendCommand("M340 P0 S1450");
		//ShowMessage("Done");

	   switch (cbOrienZRotation2->ItemIndex)
	   {
		 case 0:
			//ShowMessage("0");
			Plotter->SendCommand("M340 P0 S1050");
			break;
		 case 1:
			//ShowMessage("-45");
			Plotter->SendCommand("M340 P0 S1400");
			break;
		 case 2:
			//ShowMessage("-90");
			Plotter->SendCommand("M340 P0 S1950");
			break;
	   default:
		   ;
	   } // End Switch

}
//---------------------------------------------------------------------------
void __fastcall TmainForm::Button7Click(TObject *Sender)
{

	if (cbe_LoadSelect->Checked)
	{
		eLoad_SetLoadOFF();
	}
	else
	{
		if (MessageDlg("Please Confirm you wish to Turn OFF all RX Load relays\nNOTE: You MUST Set again Manually", mtInformation, mbYesNo, 0) == mrYes)
		 {
	//		ShowMessage("Yes");
			// 2D Plotter
			Plotter->enLoadRelay(false,0);
			Plotter->enLoadRelay(false,1);
			Plotter->enLoadRelay(false,2);

			//rgLoad-> = false;
			rgLoad->ItemIndex = -1;

		 }
		 else
		 {
	  //		 ShowMessage("No");
		 } // End If

	} // End If


}
//---------------------------------------------------------------------------

// *****************************************************************
// MOA
// *****************************************************************

//---------------------------------------------------------------------------
void __fastcall TmainForm::SaveGridToFile()
{
	wchar_t szFileName[4];
	int iFileHandle;
	int iLength;
	int countCol;
	int countRow;

	AnsiString Folder_Filename = "c:\temp\test.txt";

/*	if (FileExists(Folder_Filename)
	{
	  ShowMessage("File Exists");
	  //_wfnsplit(SaveDialog1->FileName.c_str(), 0, 0, szFileName, 0);

//	  wcscat(szFileName, L".BAK");
	  //Removing previously created .BAK files, if found
  //	  if (FileExists(szFileName)) {
	//	  DeleteFileW(szFileName);
	 // }
	  //RenameFile(SaveDialog1->FileName, szFileName);
	}
	else
	{

	}  // End If
*/

	iFileHandle = FileCreate(Folder_Filename);

//	ShowMessage("create file");
	// Write out the number of rows and columns in the grid.
	countCol = StrGridSeqTest->ColCount;

	//FileWrite(iFileHandle, &count, sizeof(count));

	countRow = StrGridSeqTest->RowCount;

	//FileWrite(iFileHandle, &count, sizeof(count));

	for (int x=0; x < countRow ; x++)
	{
	  for (int y=0; y < countCol ; y++)
	  {
		// Write out the length of each string, followed by the string itself.
		iLength = StrGridSeqTest->Cells[x][y].Length();
		FileWrite(iFileHandle, "StrGridSeqTest->Cells[x][y].c_str()", (iLength)*sizeof(wchar_t));
	  }  // End For

	}  // End For

	FileClose(iFileHandle);

	ShowMessage("Save done");
}

//void __fastcall TForm1::Button8Click(TObject *Sender)
//{
//	SaveGridToFile();
//}

//---------------------------------------------------------------------------
void __fastcall TmainForm::ShowPlotGrid()
{
/*
	StrGridSeqTest->Visible = false;
	StringGrid1->Visible = true;
	StringGrid2->Visible = false;

	GridPanel1->Visible = false;

	FileListBox1->Visible = false;
	cbSeqIniFilenames->Visible = false;
*/
}

void __fastcall TmainForm::ShowSeqGrid()
{

/*	StrGridSeqTest->Visible = true;
	StringGrid1->Visible = false;
	StringGrid2->Visible = false;

	GridPanel1->Visible = false;

	FileListBox1->Visible = true;
	cbSeqIniFilenames->Visible = true;
*/
}



//---------------------------------------------------------------------------
void __fastcall TmainForm::ShowGridKeywordTitleDataOptions(int ColNumber)
{

	//SeqGridKeyword[34][0] ="PSU on/off";

/*
		AnsiString GridTitleOptions =
		"NOTE: Use Excel file in same folder as this file for MAJOR Seq edits\n"
		"1 Col Title: 'No.' Options -> r = Run Row, Leave blank to skip Row\n"
		"2 Col Title: 'e_Load' Options -> res or cc\n"
		"3 Col Title: 'PSU on/off' Options -> on or on/off\n"
		;
*/
		ShowMessage("Column Title = " + StrGridSeqTest->Cells[ColNumber][0]);


}

//---------------------------------------------------------------------------
void __fastcall TmainForm::StrGridSeqTestDblClick(TObject *Sender)
{
	//ShowMessage("Clciked");

/*if (MessageDlg("Yes= Save Grid\nNo = Exit", mtInformation, mbYesNoCancel, 0) == mrYes)
	 {
		//ShowMessage("Yes");
//		SaveGridToFile();
		SaveIniFileData(false);
		ReadIniFile();

	 }
	 else
	 {
		 //ShowMessage("No");
	 }
*/


//ShowMessage(IntToStr(StrGridSeqTest->Col));

//int RowNumberSelected = StrGridSeqTest->Row;

//ShowMessage(IntToStr(RowNumberSelected));

	if (OverallSeqSeqKeywordCheck())
	{
		// OK
	}
	else
	{
		if (MessageDlg("Yes = Edit Title (Keyword Options given in Edit Menu)\nNo = Continue", mtInformation, mbYesNo, 0) == mrYes)
		 {
	//		ShowMessage("Yes");

			CheckSeqKeywords();
		 }
		 else
		 {
	  //		 ShowMessage("No");
		 } // End If
	} // End If

	if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
	{


		switch(StrToInt(InputBox("Please Enter Menu Option Number",
		"Menu Number\n"
		"0 = EXIT\n"
		"1 = SAVE -> Seq Grid\n"
		"2 = SET Seq Row <-> Set-Up Tab\n"
		"3 = SHOW Seq Grid Title KEYWORDS\n"
		"4 = SHOW Selected Column TITLE full text view\n"
		"5 = EDIT Seq Grid Titles\n",
		0)))
		 {
			case 0:
			 {
				// Do Nothing
				break;
			 }
			case 1:
			 {
				SaveIniFileData(false);
				ReadIniFile();
				break;
			 }
			case 2:
			 {
					int RowNumberSelected = StrGridSeqTest->Row;

					switch(MessageDlg("Please Choose Option\nYes = Seq Grid -> Set Up FORM\nNo = Set-up Form -> Seq Grid\nCancel = Exit", mtInformation, mbYesNoCancel, 0))
					 {
						case mrYes:
						 {
							SetSeqData2Form(RowNumberSelected);
							break;
						 }
						case mrNo:
						 {
							SetUpForm2SeqData(RowNumberSelected);
							break;
						 }
						 default:
						 {
							break;
						 }
					 }  // End Switch

				break;
			 }
			case 3:
			 {
					AnsiString KeywordStringOptions = "Column Title = " + StrGridSeqTest->Cells[StrGridSeqTest->Col][0] + "-> KEYWORD OPTIONS:" + GenerateKeywordTextListStr();
					ShowMessage(KeywordStringOptions);
				break;
			 }
			case 4:
			 {

					ShowGridKeywordTitleDataOptions(StrGridSeqTest->Col);

				break;
			 }
			case 5: // Edit titles
			 {



/*				switch(StrToInt(InputBox("Please Enter Menu Option Number",
					"Menu Number\n"
					"0 = Exit\n"
					"1 = Edit Title\n"
					"2 = Insert Column Title\n"
					"3 = Delete Column Title\n",
					0)))
					 {
					case 0:
					 {
						// Do Nothing
						break;
					 }
					case 1:
					 {
*/
						AnsiString ColNumbOfTitle = InputBox("Edit Column Title","Please enter Column Number of Title (>0)", "1");
						int ColNumbTitleInt = StrToInt(ColNumbOfTitle);
						if ( ColNumbTitleInt > 0)
						{
							AnsiString ColTitle = StrGridSeqTest->Cells[ColNumbTitleInt][0];
							AnsiString ColTitleNew = InputBox("Edit Column Title","Please enter Title", ColTitle);
							if (ColTitleNew == "")
							{
								// Do Nothing
							}
							else
							{
								StrGridSeqTest->Cells[ColNumbTitleInt][0] = ColTitleNew;
								CheckSeqKeywords();
							} // End If

						}
						else
						{
							// Do Nothing
						} // End If
/*
						break;
					 }
					default:
					 {
						break;
					 }

					} // End Switch

*/




				break;
			 }
			default:

				break;
		 }  // End Switch



/*
	switch(MessageDlg("Yes= Save Grid\nNo=Show more menus\nCancel = Show Results Grid or Exit", mtInformation, mbYesNoCancel, 0))
	 {
		case mrYes:
		 {
			//ShowMessage("Yes");
//			SaveGridToFile();
			SaveIniFileData(false);
			ReadIniFile();
			break;
		 }
		case mrNo:
		 {

			switch(MessageDlg("Yes = Set Selected Grid Row Data to Form\nNo=Show menu Options for selected Column\nCancel = Exit", mtInformation, mbYesNoCancel, 0))
			 {
				case mrYes:
				 {
					// Show Results Grid
					//	ShowPlotGrid();
					int RowNumberSelected = StrGridSeqTest->Row;
					SetSeqData2Form(RowNumberSelected);

					break;
				 }
				case mrNo:
				 {

					switch(MessageDlg("Yes = Show Seq Grid Title Keywords\nNo = Column\nCancel = Exit", mtInformation, mbYesNoCancel, 0))
					 {
						case mrYes:
						 {
							AnsiString KeywordStringOptions = GenerateKeywordTextListStr();
							ShowMessage(KeywordStringOptions);
							break;
						 }
						case mrNo:
						 {

							AnsiString GridTitleOptions =
							"Col Title: 'No.' Options -> r = Run Row, Leave blank to skip Row"
							"Col Title: 'xx' Options";

							ShowMessage("Title Options are: " + GridTitleOptions);

							break;
						 }
						default:
						 {
							break;
						 }
					 } // End Switch


				 /*
					int ColNumberSelected = StrGridSeqTest->Col;
					switch (ColNumberSelected)
					{
						case 1:
							ShowMessage("Column Options:\n1. end\n2. r = Run Row\nLeave blank to skip Row");
							break;
						case 2:
							ShowMessage("No Column Options");
							break;
						case 3:
							ShowMessage("No Column Options");
							break;
						case 4:
							ShowMessage("No Column Options");
							break;
						case 5:
							ShowMessage("No Column Options");
							break;
						case 6:
							ShowMessage("Column Options:\n1. 0\n2. 45\n3. 90");
							break;
						case 7:
							ShowMessage("No Column Options");
							break;
						case 8:
							ShowMessage("Column Options:\n1. eg 19 for 19VDC");
							break;
						case 9:
							ShowMessage("Column Options:\n1. eg 1 for 1A");
							break;
						case 10:
							ShowMessage("Column Options:\n1. cc");
							break;
						case 11:
							ShowMessage("Column Options:\n1. eg 2 for 2A");
							break;
						case 12:
							ShowMessage("Column Options:\n1. eg 1000 for 1000mS");
							break;
						case 13:
							ShowMessage("Column Options:\n1. eg 1000 for 1000mS");
							break;
						case 14:
							ShowMessage("No Column Options");
							break;
						case 15:
							ShowMessage("No Column Options");
							break;
						case 16:
							ShowMessage("No Column Options");
							break;
						case 17:
							ShowMessage("No Column Options");
							break;
						case 18:
							ShowMessage("No Column Options");
							break;
						case 19:
							ShowMessage("No Column Options");
							break;
						case 20:
							ShowMessage("No Column Options");
							break;
						default:
							ShowMessage("No Column Options");
							break;
					} // end Switch

					*/

/*
					break;
				 }
				default:
				 {
					break;
				 }
			 } // End Switch

//			break;
		 }
		default:

/*
					switch(MessageDlg("Yes = Show Results Grid\nNo = Exit", mtInformation, mbYesNo, 0))
					{
						case mrYes:
						 {
							  StringGrid3->Visible = true;
							  StrGridSeqTest->Visible = false;
							break;
						 }
						default:
						 {
							break;
						 }
					 } // End Switch

*/


/*
			break;
	 }  // End Switch



*/

	}
	else
	{
	   //	ShowMessage("No GRid file loaded");
	} // End If


}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SaveIniFileData(bool DefaultTitles)
{
uint8_t i;
AnsiString Title;
TCustomIniFile* SettingsFile;

	/* Open an instance. */
	if (cbSeqIniFilenames->Text == "")
	{
		// Create Default name
//AnsiString INIFolderFilename = ChangeFileExt(Application->ExeName, ".INI");
		SettingsFile = OpenIniFileInstance();
  //SettingsFile = new TIniFile(ChangeFileExt(Application->ExeName, ".INI"));

//			return new TIniFile(ChangeFileExt(Application->ExeName, ".INI"));
	}
	else
	{
		SettingsFile = new TIniFile(cbSeqIniFilenames->Text);
	} // End If

if (SettingsFile == NULL)
{
	ShowMessage("Error Cannot Create ini File handle");
}
else
{
	try
	{
			//***
			// Ends Default CVS Folder
			//***
			const unsigned long maxDir = 260;
			char currentDir[maxDir];
			GetCurrentDirectory(maxDir, currentDir);
  //			ShowMessage(currentDir);
			StrCat(currentDir,"\\");

			//***
			// Ends Default CVS Folder
			//***
	if (DefaultTitles)
	{
		SeqGrid_Init();
	}
	else
	{
		// Do Nothing
	} // End If

	// ***
	// Check if File Exists
	// ***

/*	AnsiString INIFolderFilename = ChangeFileExt(Application->ExeName, ".INI");
	if (FileExists(INIFolderFilename))
	{
		//ShowMessage("File Exists");
		//DeleteFile(INIFolderFilename);
	}
	else
	{
		// Do Nothing
	}  // End If

*/
	   // ***
	   // Save Grid titles
	   // ***
	   //StrGridSeqTest->ColCount
	   //StrGridSeqTest->RowCount
	   for (int iCol = 0; iCol < StrGridSeqTest->ColCount ; iCol++)
	   {
			SettingsFile->WriteString("Main", "Grid Title" + IntToStr(iCol), StrGridSeqTest->Cells[iCol + 1][0]);
	   }  // End For


	   // ***
	   // Save Grid Data
	   // ***
	   for (int iRow = 1; iRow < StrGridSeqTest->RowCount; iRow++)
	   {
		   for (int iCol = 0; iCol < StrGridSeqTest->ColCount ; iCol++)
		   {

//			// General Grid Save
//			SettingsFile->WriteString  ("Main", "Grid Data:" + IntToStr(iRow) + "_" + IntToStr(iCol), StrGridSeqTest->Cells[iCol][iRow]);


		   //***
		   // Special case for Column 1. States = 'number' or 'r' or 'end' or ""
		   //***
			if ((iCol == 1) && (StrGridSeqTest->Cells[iCol][iRow] != ""))
			{

			SettingsFile->WriteString  ("Main", "Grid Data:" + IntToStr(iRow) + "_" + IntToStr(iCol), "");

/*   		if (UpperCase(StrGridSeqTest->Cells[iCol][iRow]) == "END")
				{
					SettingsFile->WriteString  ("Main", "Grid Data:" + IntToStr(iRow) + "_" + IntToStr(iCol), StrGridSeqTest->Cells[iCol][iRow]);
				}
				else
				{
					// Mark r as numbered means it should be run. Blank = do not run Row
					SettingsFile->WriteString  ("Main", "Grid Data:" + IntToStr(iRow) + "_" + IntToStr(iCol), "r");
				} // End If
*/

			}
			else
			{
				// General Grid Save
				SettingsFile->WriteString  ("Main", "Grid Data:" + IntToStr(iRow) + "_" + IntToStr(iCol), StrGridSeqTest->Cells[iCol][iRow]);
			} // End If

			//***
			// ENDS Special case for Column 1. States = 'number' or 'r' or 'end' or ""
			//***

//				SettingsFile->WriteString  ("Main", "Grid Data:" + IntToStr(iRow) + "_" + IntToStr(iCol), StrGridSeqTest->Cells[iCol][iRow]);
/*
				// Ignore Column 1 data
				if (iCol == 1)
				{
					// Blank Column Seq Progress Count Col
					SettingsFile->WriteString  ("Main", "Grid Data:" + IntToStr(iRow) + "_" + IntToStr(iCol), "");
				}
				else
				{
					// Do Nothing
					SettingsFile->WriteString  ("Main", "Grid Data:" + IntToStr(iRow) + "_" + IntToStr(iCol), StrGridSeqTest->Cells[iCol][iRow]);
				} // End If
*/
		   }  // End For

	   }  // End For

	}
	catch(Exception* e)
	{
		delete SettingsFile;
		ShowMessage("Error in sub -> SaveIniFileData");
	} // End Catch

	delete SettingsFile;

} // End If


}  // End Function

//---------------------------------------------------------------------------
void __fastcall TmainForm::SaveEffPlotIniFileData()
{

AnsiString INIFolderFilename;
//TCustomIniFile* SettingsFile;

	/* Open an instance. */

	INIFolderFilename = ChangeFileExt(Application->ExeName, ".INI");

 try
 {

	   if (FileExists(INIFolderFilename))
		{
//			MessageDlg("File already exists", mtError,
//				TMsgDlgButtons() << mbOK, 0);


		}
		else
		{
			  SaveDEFAULTEffPlotIniFileData();
		}  // End If

/*
//		AnsiString INIFolderFilename = ChangeFileExt(Application->ExeName, "SeqGrid.INI");
	if (FileExists(INIFolderFilename))
	{
		ShowMessage("File Exists");

		//DeleteFile(INIFolderFilename);

		//***
		// Save Ini Here
		//***

				//***
				// Save ini File
				//***


			  /*
				AnsiString DefaultText = "No text Found";
				for (int i = 0; i < 1000; i++)
				{
					AnsiString Data = SettingsFile->WriteString("Keywords", "Key_" + IntToStr(i), DefaultText);
					if (Data == DefaultText)
					{
						// Not found
						break;
					}
					else
					{
						// Found
						meSeqIniFile->Lines->Add(Data);

					} // End If

				}  // End For

  */

/*
	}
	else
	{
		// Create file handle
		SettingsFile = new TIniFile(INIFolderFilename);
		if (SettingsFile == NULL)
		{
			ShowMessage("Error Cannot Create ini File handle");
		}
		else
		{
			SettingsFile->WriteString  ("Name", "Caption", "Caption");
		}  // End If

		delete SettingsFile;

	}  // End If
*/
	}
	catch(Exception* e)
	{
	   //	delete SettingsFile;
		ShowMessage("Error in sub -> SaveSeqIniFileData");
	} // End Catch

}  // End Function


//---------------------------------------------------------------------------
void __fastcall TmainForm::SaveSeqIniFileData()
{

AnsiString INIFolderFilename;
//TCustomIniFile* SettingsFile;

	/* Open an instance. */

	INIFolderFilename = ChangeFileExt(Application->ExeName, "SeqGrid.INI");

 try
 {


//	   if (FileExists(INIFolderFilename))
//		{
//			MessageDlg("File already exists", mtError,
//				TMsgDlgButtons() << mbOK, 0);
//		}
//		else
//		{
			// saves to file based on the selected encoding.
			meSeqIniFile->Lines->SaveToFile(INIFolderFilename);
//		}

/*

//		AnsiString INIFolderFilename = ChangeFileExt(Application->ExeName, "SeqGrid.INI");
	if (FileExists(INIFolderFilename))
	{
		ShowMessage("File Exists");

		//DeleteFile(INIFolderFilename);

		//***
		// Save Ini Here
		//***

				//***
				// Save ini File
				//***


			  /*
				AnsiString DefaultText = "No text Found";
				for (int i = 0; i < 1000; i++)
				{
					AnsiString Data = SettingsFile->WriteString("Keywords", "Key_" + IntToStr(i), DefaultText);
					if (Data == DefaultText)
					{
						// Not found
						break;
					}
					else
					{
						// Found
						meSeqIniFile->Lines->Add(Data);

					} // End If

				}  // End For

  */

/*
	}
	else
	{
		// Create file handle
		SettingsFile = new TIniFile(INIFolderFilename);
		if (SettingsFile == NULL)
		{
			ShowMessage("Error Cannot Create ini File handle");
		}
		else
		{
			SettingsFile->WriteString  ("Name", "Caption", "Caption");
		}  // End If

		delete SettingsFile;

	}  // End If
*/
	}
	catch(Exception* e)
	{
	   //	delete SettingsFile;
		ShowMessage("Error in sub -> SaveSeqIniFileData");
	} // End Catch

}  // End Function


//---------------------------------------------------------------------------
void __fastcall TmainForm::SaveDEFAULTEffPlotIniFileData()
{

AnsiString INIFolderFilename;

TCustomIniFile* SettingsFile;

	/* Open an instance. */

	INIFolderFilename = ChangeFileExt(Application->ExeName, ".INI");

// Create file handle

		SettingsFile = new TIniFile(INIFolderFilename);
		if (SettingsFile == NULL)
		{
			ShowMessage("Error Cannot Create ini File handle");
		}
		else
		{
			SettingsFile->WriteString  ("DAQ Tx Cal", "ebVinOffset", "1.0");
			SettingsFile->WriteString  ("DAQ Tx Cal", "ebVinMultiplier", "1.0");
			SettingsFile->WriteString  ("DAQ Tx Cal", "ebIinOffset", "1.0");
			SettingsFile->WriteString  ("DAQ Tx Cal", "ebIinMultiplier", "1.0");

			SettingsFile->WriteString  ("DAQ Rx Cal", "ebVoutOffset", "1.0");
			SettingsFile->WriteString  ("DAQ Rx Cal", "ebVoutMultiplier", "1.0");
			SettingsFile->WriteString  ("DAQ Rx Cal", "ebIoutOffset", "1.0");
			SettingsFile->WriteString  ("DAQ Rx Cal", "ebIoutMultiplier", "1.0");

		}  // End If

		delete SettingsFile;

}  // End Function


//---------------------------------------------------------------------------
void __fastcall TmainForm::SaveDEFAULTSeqIniFileData()
{

AnsiString INIFolderFilename;
//TCustomIniFile* SettingsFile;

	/* Open an instance. */

	INIFolderFilename = ChangeFileExt(Application->ExeName, "SeqGrid.INI");

//	   if (FileExists(INIFolderFilename))
//		{
//			MessageDlg("File already exists", mtError,
//				TMsgDlgButtons() << mbOK, 0);
//		}
//		else
//		{
			// saves to file based on the selected encoding.

			Memo1->Lines->Add("[Keywords]");
			Memo1->Lines->Add("Key_0=No.");
			Memo1->Lines->Add("Key_1=Stepmm");



			Memo1->Lines->SaveToFile(INIFolderFilename);
//		}


}  // End Function


//---------------------------------------------------------------------------
void __fastcall TmainForm::ReadSeqIniFileData()
{

AnsiString INIFolderFilename;
TCustomIniFile* SettingsFile;

	/* Open an instance. */

	INIFolderFilename = ChangeFileExt(Application->ExeName, "SeqGrid.INI");

/*
		 if (FileExists(INIFolderFilename))
		{
			// display the contents in a memo based on the selected encoding
			meSeqIniFile->Lines->LoadFromFile(INIFolderFilename);
		}
		else
		{
			//MessageDlg("File does not exist", mtError,
				//TMsgDlgButtons() << mbOK, 0);
		}  // End If
*/



		// Create file handle
		SettingsFile = new TIniFile(INIFolderFilename);
		if (SettingsFile == NULL)
		{
			ShowMessage("Error Cannot Create ini File handle");
		}
		else
		{
			//SettingsFile->WriteString  ("Name", "Caption", "Caption");


			try
			 {

			//		AnsiString INIFolderFilename = ChangeFileExt(Application->ExeName, "SeqGrid.INI");
				if (FileExists(INIFolderFilename))
				{
					//ShowMessage("File Exists");
			//		DeleteFile(INIFolderFilename);

				//***
				// Read ini File
				//***

				AnsiString DefaultText = "No text Found";

				meSeqIniFile->Lines->Add("[Keywords]");

				for (int i = 0; i < SEQ_COL_MAX; i++)
				{
					AnsiString Data = SettingsFile->ReadString("Keywords", "Key_" + IntToStr(i), DefaultText);
					if (Data == DefaultText)
					{
						// Not found
						break;
					}
					else
					{
						// Found
						meSeqIniFile->Lines->Add("Key_" + IntToStr(i) + "=" + Data);
						SeqGridKeyword[i][0] = Data;

					} // End If

				}  // End For

					// Save
				}
				else
				{
					// Create default
					SettingsFile = new TIniFile(INIFolderFilename);
					if (SettingsFile == NULL)
					{
						ShowMessage("Error Cannot Create ini File handle");
					}
					else
					{
						SettingsFile->WriteString  ("Name", "Caption", "Caption");
					}  // End If

					delete SettingsFile;

				}  // End If

				}
				catch(Exception* e)
				{
					delete SettingsFile;
					ShowMessage("Error in sub -> ReadSeqIniFileData");
				} // End Catch

		}  // End If


}  // End Function

//---------------------------------------------------------------------------
void __fastcall TmainForm::ReadEffPlotIniFileData()
{

AnsiString INIFolderFilename;
TCustomIniFile* SettingsFile;

	/* Open an instance. */

	INIFolderFilename = ChangeFileExt(Application->ExeName, ".INI");

/*
		 if (FileExists(INIFolderFilename))
		{
			// display the contents in a memo based on the selected encoding
			meSeqIniFile->Lines->LoadFromFile(INIFolderFilename);
		}
		else
		{
			//MessageDlg("File does not exist", mtError,
				//TMsgDlgButtons() << mbOK, 0);
		}  // End If
*/



		// Create file handle
		SettingsFile = new TIniFile(INIFolderFilename);
		if (SettingsFile == NULL)
		{
			ShowMessage("Error Cannot Create ini File handle");
		}
		else
		{
			//SettingsFile->WriteString  ("Name", "Caption", "Caption");


			try
			 {

			//		AnsiString INIFolderFilename = ChangeFileExt(Application->ExeName, "SeqGrid.INI");
				if (FileExists(INIFolderFilename))
				{
					//ShowMessage("File Exists");
			//		DeleteFile(INIFolderFilename);

				//***
				// Read ini File
				//***

				ebVinOffset->Text = SettingsFile->ReadString  ("DAQ Tx Cal", "ebVinOffset", "1.0");
				ebVinMultiplier->Text = SettingsFile->ReadString  ("DAQ Tx Cal", "ebVinMultiplier", "1.0");
				ebIinOffset->Text = SettingsFile->ReadString  ("DAQ Tx Cal", "ebIinOffset", "1.0");
				ebIinMultiplier->Text = SettingsFile->ReadString  ("DAQ Tx Cal", "ebIinMultiplier", "1.0");

				ebVoutOffset->Text = SettingsFile->ReadString  ("DAQ Rx Cal", "ebVoutOffset", "1.0");
				ebVoutMultiplier->Text = SettingsFile->ReadString  ("DAQ Rx Cal", "ebVoutMultiplier", "1.0");
				ebIoutOffset->Text = SettingsFile->ReadString  ("DAQ Rx Cal", "ebIoutOffset", "1.0");
				ebIoutMultiplier->Text = SettingsFile->ReadString  ("DAQ Rx Cal", "ebIoutMultiplier", "1.0");

					// Save
				}
				else
				{
					// Create default

				   SaveDEFAULTEffPlotIniFileData();

				   // Read Back values
				ebVinOffset->Text = SettingsFile->ReadString  ("DAQ Tx Cal", "ebVinOffset", "1.0");
				ebVinMultiplier->Text = SettingsFile->ReadString  ("DAQ Tx Cal", "ebVinMultiplier", "1.0");
				ebIinOffset->Text = SettingsFile->ReadString  ("DAQ Tx Cal", "ebIinOffset", "1.0");
				ebIinMultiplier->Text = SettingsFile->ReadString  ("DAQ Tx Cal", "ebIinMultiplier", "1.0");

				ebVoutOffset->Text = SettingsFile->ReadString  ("DAQ Rx Cal", "ebVoutOffset", "1.0");
				ebVoutMultiplier->Text = SettingsFile->ReadString  ("DAQ Rx Cal", "ebVoutMultiplier", "1.0");
				ebIoutOffset->Text = SettingsFile->ReadString  ("DAQ Rx Cal", "ebIoutOffset", "1.0");
				ebIoutMultiplier->Text = SettingsFile->ReadString  ("DAQ Rx Cal", "ebIoutMultiplier", "1.0");


				}  // End If

				}
				catch(Exception* e)
				{
					delete SettingsFile;
					ShowMessage("Error in sub -> ReadSeqIniFileData");
				} // End Catch

		}  // End If


}  // End Function

//---------------------------------------------------------------------------
void __fastcall TmainForm::ReadIniFile()
{
uint8_t i;
int ColMax;
int RowMax;
TCustomIniFile* SettingsFile;
int RowSelect;
int ColSelect;

	//ColMax = StrToInt(ebXSteps->Text);
	//RowMax = StrToInt(ebYSteps->Text);

	ColMax = SEQ_COL_MAX + 1;
//	RowMax = SeqRowMax; //20;
	RowMax = 2000; //StrGridSeqTest->RowCount;    // Includes Title Row

	//***
	// Set Grid Limits
	//***
	StrGridSeqTest->ColCount = ColMax;     // Include Title Col
//	StrGridSeqTest->RowCount = RowMax;     // Includes Title Row

	// ***
	// Clear Grid
	// ***
	for (int iRow = 0; iRow < RowMax; iRow++)
	{
		StrGridSeqTest->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
	   //	StrGridSeqTest->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
	}  // End For

	/* Open an instance */


//	TCustomIniFile* SettingsFile = OpenIniFileInstance();

	if (cbSeqIniFilenames->Text == "")
	{
		// Create Default name
		SettingsFile = OpenIniFileInstance();
	}
	else
	{
		SettingsFile = new TIniFile(cbSeqIniFilenames->Text);
	} // End If

	try
	{

		// ***
		// Save Titles
		// ***

		StrGridSeqTest->Cells[0][0] = "SEQ GRID";

		for (int iCol = 0; iCol < ColMax; iCol++)
		{
			//
			AnsiString Data = SettingsFile->ReadString("Main", "Grid Title" + IntToStr(iCol),"default");
			if ((Data == "default") || (Data == ""))
			{
				//End of Cols
				// Upddate Grid Columns based on non blank titles + a few more

				StrGridSeqTest->ColCount = iCol + 1;     // Include Title Col

				break;
			}
			else
			{
				// Do Nothing
				StrGridSeqTest->Cells[iCol + 1][0] = SettingsFile->ReadString("Main", "Grid Title" + IntToStr(iCol),"default");
			} // End If

		}  // End For

		// ***
		// End Save Titles
		// ***


		// ***
		// Data
		// ***
		//StrGridSeqTest->RowCount
//		for (int iRow = 1; iRow < SeqRowMax - 1 ; iRow++)

  //	StrGridSeqTest->Cells[13][19] = SettingsFile->ReadString("Main", "Grid Data:" + IntToStr(19) + "_" + IntToStr(13),"default");
  //	AnsiString test1 = SettingsFile->ReadString("Main", "Grid Data:" + IntToStr(19) + "_" + IntToStr(13),"default");
  //	AnsiString test = SettingsFile->ReadString("Main", "Grid Data:" + IntToStr(19) + "_" + IntToStr(14),"default");
  //	StrGridSeqTest->Cells[14][19] = test;

//		StrGridSeqTest->RowCount = StrToInt(edSeqRowCount->Text);
		bool RowDone = false;
		for (RowSelect = 1; RowSelect < RowMax ; RowSelect++)
		{
			if (RowDone)
			{
				break;
			}
			else
			{
				// Do Nothing
			} // End If

//			for (ColSelect = 0; ColSelect < (StrGridSeqTest->ColCount - 6); ColSelect++)
			for (ColSelect = 0; ColSelect < (StrGridSeqTest->ColCount); ColSelect++)
			{

//				AnsiString IniData = SettingsFile->ReadString("Main", "Grid Data:" + IntToStr(iRow) + "_" + IntToStr(iCol),"default");
				// Test for any special commands in Seq
//				if (UppserCase(IniData) == "END))
//				{
//					StrGridSeqTest->Cells[iCol][iRow]
//				}
//				else
//				{
//					StrGridSeqTest->Cells[iCol][iRow] = IntToStr(IniData);

					AnsiString Data = SettingsFile->ReadString("Main", "Grid Data:" + IntToStr(RowSelect) + "_" + IntToStr(ColSelect),"default");
					// Check if Row is numbered else end of rows
					if (ColSelect == 0)
					{
					  if ((Data == "") || (Data == "default"))
					  {
						// ***
						// No more row numbers
						// ***
						RowDone = true;
						break;
					  }
					  else
					  {
						 // Do Nothing
						 // Check if Data is Seq Done Marker
						if (Data == "S")
						{
							// Overwrite Seq marker
							Data = IntToStr(RowSelect);
						}
						else
						{
						   // Do Nothing
						} // End If
						StrGridSeqTest->RowCount = RowSelect + 1;
						StrGridSeqTest->Cells[0][RowSelect] = Data;
					  }  // End If
					}
					else
					{


						// Check Special Case of Column 1
						if (ColSelect == 1)
						{

								Data = "";
/*
							// Should be Blank as Seq writes to this col
							if ((UpperCase(Data) == "END") ||(UpperCase(Data) == "R") ||(UpperCase(Data) == "P"))
							{
								// Do Nothing
							}
							else
							{
								// Blank - Only end in this column
								Data = "";
							} // End If
*/
						}
						else
						{
							// Do Nothing
						} // End If

						StrGridSeqTest->RowCount = RowSelect + 1;
						StrGridSeqTest->Cells[ColSelect][RowSelect] = Data;

					} // End If

//					StrGridSeqTest->Cells[ColSelect][RowSelect] = SettingsFile->ReadString("Main", "Grid Data:" + IntToStr(RowSelect) + "_" + IntToStr(ColSelect),"default");//					StrGridSeqTest->Cells[ColSelect][RowSelect] = SettingsFile->ReadString("Main", "Grid Data:" + IntToStr(RowSelect) + "_" + IntToStr(ColSelect),"default");
	//				SettingsFile->WriteString  ("Main", "Grid Data:" + IntToStr(iRow) + "_" + IntToStr(iCol), StrGridSeqTest->Cells[iCol][iRow]);

//				}


			} // End For

		}  // End For

		// ***
		// End Data
		// ***

	  // ShowMessage(IntToStr(RowSelect));

		// Load Text Edit box
//   rteIniFile->Lines->LoadFromFile(cbSeqIniFilenames->Text);

	}
	catch(...)
	{
		// NO Ini File exists
		//SaIniFileData(true);
		delete SettingsFile;
		ShowMessage("Error in Sub -> ReadIniFile\nRow = " + IntToStr(RowSelect) + "\nCol= " + IntToStr(ColSelect));
	} // End Catch

	delete SettingsFile;

} // End Function


//---------------------------------------------------------------------------
TCustomIniFile* __fastcall TmainForm::OpenIniFileInstance()
{
	/*
	Open/create a new INI file that has the same name as
	your executable, only with the INI extension.
	*/

	switch (1)
	{
		case 0:
			/* Registry mode selected: in HKEY_CURRENT_USER\Software\... */
			//return new TRegistryIniFile(String("Software\\") + Application->Title);
			ShowMessage("hi");
		case 1:
			/* Ini file mode selected */
			if (cbSeqIniFilenames->Text == "")
			{
				// Create Default name
				return new TIniFile(ChangeFileExt(Application->ExeName, ".INI"));
			}
			else
			{
				return new TIniFile(cbSeqIniFilenames->Text);
			} // End If

			  //	ShowMessage("Return ini");
			//return new TIniFile("inifile.txt");
		case 2:
			/* Memory based Ini file mode selected */
			//return new TMemIniFile(Change*FileExt(Application->ExeName, ".INI"));
			ShowMessage("hi");
	}  // End Switch

} // End Function

//---------------------------------------------------------------------------
void __fastcall TmainForm::rteIniFileDblClick(TObject *Sender)
{
	rteIniFile->Lines->SaveToFile(ChangeFileExt(Application->ExeName, ".INI"));
	//ReadIniFile();
}

//---------------------------------------------------------------------------
void __fastcall TmainForm::RunSequenceSteps()
{
AnsiString PlotCmd[10];
//int PlotIndex = 0;
static loopCount;
bool SeqDone;

try
{

	if (TestStarted)
	{
		Application->ProcessMessages();

		// Stop Clock to get data
		tmSeqTest2->Enabled = false;

		// ***
		// Run Seq
		// ***
		AnsiString test = StrGridSeqTest->Cells[2][SeqStepCounter];

//		if (SeqStepCounter < StrGridSeqTest->RowCount)

		if (UpperCase(test) == "END")
		{
			SeqDone = true;
		}
		else
		{
			SeqDone = false;
		} // End If

//		if ((SeqStepCounter < StrGridSeqTest->RowCount) && (UpperCase(test) != "END"))
		if ((SeqStepCounter < StrGridSeqTest->RowCount) && (!SeqDone))
		{
			// Number Col
			StrGridSeqTest->Cells[1][SeqStepCounter] = IntToStr(SeqStepCounter);
			SeqColourCEllGREEN(SeqStepCounter, 0);
//			SeqColourCEllGREEN(2, 2);

			// ***
			// Plotter
			// ***
			PlotCmd[0] = "G1 X" + StrGridSeqTest->Cells[3][SeqStepCounter] + " Y" + StrGridSeqTest->Cells[4][SeqStepCounter] + " Z" + StrGridSeqTest->Cells[5][SeqStepCounter] + " F3000";
	//		PlotIndex = PlotIndex + 1;
		   //	ShowMessage("Plot cmd = " + PlotCmd);
			lbStatusGridSeq->Caption = "Plot cmd = " + PlotCmd[0];

			Plotter->SendCommand(PlotCmd[0]);
//			Plotter->Set3dZHeight(StrToFloat(ed3DZHeight->Text));

			// ***
			// Rot 1
			// ***
			PlotCmd[1] = StrGridSeqTest->Cells[6][SeqStepCounter];
			if (PlotCmd[1] == "0")
			{
				//ShowMessage("0");
				Plotter->SendCommand("M340 P0 S1050");
			}
			else if ((PlotCmd[1] == "45"))
			{
				//ShowMessage("-45");
				Plotter->SendCommand("M340 P0 S1400");
			}
			else if ((PlotCmd[1] == "90"))
			{
				//ShowMessage("-90");
				Plotter->SendCommand("M340 P0 S1950");
			}
			else
			{
			   //	ShowMessage("ERROR: Unknown Rot 1 Command");
				//ShowMessage("0");
				Plotter->SendCommand("M340 P0 S1050");
			} // End If
  //	   }// End If

	//		PlotIndex = PlotIndex + 1;

			// ***
			// Rot 2
			// ***
			PlotCmd[2] = StrGridSeqTest->Cells[7][SeqStepCounter];
	//		PlotIndex = PlotIndex + 1;

			// ***
			// PSU Volts
			// ***
			PlotCmd[3] = StrGridSeqTest->Cells[8][SeqStepCounter];
			lbStatusGridSeq->Caption = "PSU cmd = " + PlotCmd[3];
	//		PlotIndex = PlotIndex + 1;

			// ***
			// PSU Amps
			// ***
			PlotCmd[4] = StrGridSeqTest->Cells[9][SeqStepCounter];
			lbStatusGridSeq->Caption = "PSU cmd = " + PlotCmd[4];
			PSUGWSetVDCandAMPS(StrGridSeqTest->Cells[7][SeqStepCounter], StrGridSeqTest->Cells[8][SeqStepCounter]);
	//		ShowMessage("Set Power Supply");
	//		PlotIndex = PlotIndex + 1;

			// ***
			// e-load type
			// ***
			PlotCmd[5] = StrGridSeqTest->Cells[10][SeqStepCounter];
			lbStatusGridSeq->Caption = "e-load Type = " + PlotCmd[5];
	//		PlotIndex = PlotIndex + 1;
			eLoad_SetRemote();
			if (UpperCase(PlotCmd[5]) == "CC")
			{
				eLoad_SetCCMode();
			}
			else
			{
				// Do Nothing
			} // End If

			// ***
			// e-load value
			// ***
			PlotCmd[6] = StrGridSeqTest->Cells[11][SeqStepCounter];
			lbStatusGridSeq->Caption = "e-load Value = " + PlotCmd[6];
	//		PlotIndex = PlotIndex + 1;

			if (StrToInt(PlotCmd[6]) == 0)
			{
				// Do Nothing
				eLoad_SetCCValue("0");
				eLoad_SetLoadOFF();
			}
			else
			{
				eLoad_SetCCValue(PlotCmd[6]);

			} // End If

			// ***
			// OFF Time
			// ***
			PlotCmd[7] = StrGridSeqTest->Cells[12][SeqStepCounter];
			lbStatusGridSeq->Caption = "OFF Time = " + PlotCmd[7];
		//		PlotIndex = PlotIndex + 1;
			CountDownProgressWait = StrToInt(PlotCmd[7]);

			// ***
			// ON Time
			// ***
			PlotCmd[8] = StrGridSeqTest->Cells[13][SeqStepCounter];
			lbStatusGridSeq->Caption = "ON Time = " + PlotCmd[8];
			CountDownProgressOnWait = StrToInt(PlotCmd[8]);
	//		PlotIndex = PlotIndex + 1;

			// ***
			// TimeStamp
			// ***
			PlotCmd[9] = StrGridSeqTest->Cells[14][SeqStepCounter];
			lbStatusGridSeq->Caption = "ON Time = " + PlotCmd[8];

		  // Start Clock after get data
			tmSeqTest2->Enabled = true;

			// ***
			// OFF time
			// ***
			if (OFFSeqGridTimeDone)
			{
				// ***
				// Now move on to ON time
				// ***
				if (ONSeqGridTimeDone)
				{
					lbStatusGridSeq->Caption = "ON Time Done for -> " + PlotCmd[8];
				}
				else
				{
					if (tmSeqTestWait->Enabled)
					{
						// Do Nothing
						lbStatusGridSeq->Caption = "ON Time WAIT";

					}
					else
					{
						lbStatusGridSeq->Caption = "START WAIT ON Time";
						// *********************************
						// ON State
						// *********************************
						Application->ProcessMessages();
						CountDownProgressOnWait = CountDownProgressOnWait - 1;
						lblWaitCountdown->Caption = IntToStr(CountDownProgressOnWait);

						PSU_TurnON();
						eLoad_SetLoadON();

		//				PlotCmd[9] = StrGridSeqTest->Cells[13][SeqStepCounter];
						if (PlotCmd[8] == "")
						{
							PlotCmd[8] = "1";
						} // End If
						tmSeqTestWait->Interval = StrToInt(PlotCmd[8]);
						tmSeqTestWait->Enabled = true;

					}  // End If

				} // End If

			}
			else
			{
				if (tmSeqTestWait->Enabled)
				{
					// Do Nothing

//					lblWaitCountdown->Caption =
					lbStatusGridSeq->Caption = "OFF Time WAIT";
				}
				else
				{
					// *********************************
					// OFF State
					// *********************************
					Application->ProcessMessages();
					CountDownProgressWait = CountDownProgressWait - 1;
					lblWaitCountdown->Caption = IntToStr(CountDownProgressWait);

					PSU_TurnOFF();
					eLoad_SetLoadOFF();

					if (PlotCmd[7] == "")
					{
						PlotCmd[7] = "1";
					} // End If
					tmSeqTestWait->Interval = StrToInt(PlotCmd[7]);
					tmSeqTestWait->Enabled = true;
					lbStatusGridSeq->Caption = "START WAIT OFF Time";
	//				ONSeqGridTimeDone = false;

				} // End If

			} // End If

			// ****************************************
			// Check if ON/Off times Done. Now ready to Measure
			// ****************************************
			if (OFFSeqGridTimeDone && ONSeqGridTimeDone)
			{
			  // ShowMessage("Off and On done");
				tmSeqTest2->Enabled = false;

				lbStatusGridSeq->Caption = "START MEAS";

				// ***
				// RUN MESUREMENT
				// ***
				//ShowMessage("Ready to take mesurement");

				//USBMM->StartNewMeasurement();
				StartMeasUSBMM();

				//if (USBMM->DataAvailable())
				if (DataAvaiUSBMM())
				{

			//		USBMM->Start();

			//		USBLogAdd("Data Available");
					UpdateDAQFormDisplay();
			//		tmMeterUpdate->Enabled = false;

			//		USBLogAdd("Ends - Update Data Available");

	//				updateEfficiency();
					updateEfficiencySeqGrid(SeqStepCounter);

			///		USBLogAdd("Ends - Data Available");

					// ***
					// SAVE DATA
					// ***
					// Create Data Row String from Grid
					AnsiString TestResultData; // = StrGridSeqTest->Cells[12][SeqStepCounter] + "," + StrGridSeqTest->Cells[13][SeqStepCounter] + "," + StrGridSeqTest->Cells[14][SeqStepCounter] + "," + StrGridSeqTest->Cells[15][SeqStepCounter];
					for (int i = 1; i < 20; i++)
					{
						TestResultData = TestResultData + StrGridSeqTest->Cells[i][SeqStepCounter] + ",";
					}
					TestResultData = TestResultData + "\n";
					// Now save data
					if (WriteToCSVSeqGrid(testAdmin.testFile, TestResultData))
					{
						tmSeqTest2->Enabled = true;
					}
					else
					{
						tmSeqTest2->Enabled = false;
						ShowMessage("Error saving CSV file");
						tmSeqTest2->Enabled = true;
					} // End If

					// ***
					// ENDS SAVE DATA
					// ***


					// Reset
					OFFSeqGridTimeDone = false;
					ONSeqGridTimeDone = false;

				}
				else
				{
					USBLogAdd("Error Reading DAQ");
				} // End If

				// ***
				// ENDS RUN MEASUREMENT
				// ***
	//			lbStatusGridSeq->Caption = "OFF Time WAIT";
				SeqStepCounter = SeqStepCounter + 1;
//				lbRepeatCountStatus->Caption = "Repeat Count = " + IntToStr(SeqStepCounter);
				tmSeqTest2->Enabled = true;

			}
			else
			{
				// Do Nothing
			} // Ends If

		}
		else
		{
			// End

//			ShowMessage("Done");
			lbStatusGridSeq->Caption = "Seq Done";
//			SeqRepeatCount = 1;
			if (SeqDone)
			{
				// ====
				// Check for Repeat
				// ====
//				if ((SeqRepeatCount > (StrToInt(ebRepeat->Text) - 1)) || (SeqRepeatCount == 0))
				if (SeqRepeatCount > (StrToInt(ebRepeat->Text) - 1))
				{
					lbStatusGridSeq->Caption = "Seq Repeat Completed";
					tmSeqTest2->Enabled = false;
					SeqTestDone = true;
					SeqTestSTOP();

					//ShowMessage("Test Sequence Done");

				}
				else
				{
				   lbStatusGridSeq->Caption = "Seq Repeat";
				   // ***
				   // Repeat Test
				   // ***
	//			   SeqStepCounter = 1;

				   ReadIniFile();

				   SeqStepCount = 0;
				   TestStarted = true;
				   SeqDone = false;
				   SeqStepCounter = 1;
				   SeqRepeatCount = SeqRepeatCount + 1;
				   tmSeqTest2->Enabled = true;
				   lbRepeatCountStatus->Caption = "Repeat Count = " + IntToStr(SeqRepeatCount);
				   // Repeat Test
				} // End If
			}
			else
			{
				lbStatusGridSeq->Caption = "Seq Next Step";
				SeqStepCount = SeqStepCount + 1;
			} // End If

		}  /// End If

		// ***
		// Ends Run Seq
		// ***

	}
	else
	{
		// Do Nothing
		tmSeqTest2->Enabled = false;
	} // End If


}
catch (...)
{
	ShowMessage("Error in Sub -> tmSeqTest2Timer : Seq Error. \nCHECK:\n1. All Comms Set\n2. eLoad is set for Remote Control \n3. Other problem");
}  // End Try


}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SeqColourCEllGREEN(int rowcell, int colcell)
{
	//NOTE:This MUST be last edit on Cell as once an cell changes colour is reset

	TRect Recto = StrGridSeqTest->CellRect(colcell , rowcell);
	int Area = Recto.Width() * Recto.Height();
	StrGridSeqTest->Canvas->Brush->Color = clGreen;
	StrGridSeqTest->Canvas->FillRect(Recto);
}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SeqColourCEllRED(int rowcell, int colcell)
{
	TRect Recto = StrGridSeqTest->CellRect(colcell , rowcell);
	int Area = Recto.Width() * Recto.Height();
	StrGridSeqTest->Canvas->Brush->Color = clRed;
	StrGridSeqTest->Canvas->FillRect(Recto);
}

void __fastcall TmainForm::btnStopSeqGridClick(TObject *Sender)
{
if (MessageDlg("Please confirm you want to STOP\nYES = STOP \nNO = Cancel", mtInformation, mbYesNo, 0) == mrYes)
	 {
		//ShowMessage("Yes");
		SeqTestSTOP();
		cbSeqIniFilenames->Enabled = true;

		//For Generic comport
		flgRunGenComport = false;
		PlotOnSelectedPoint = false;

	 }
	 else
	 {
		 //ShowMessage("No");
	 } // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SeqTestSTOP()
{

	StopALLTimers();

	TestStarted = false;

	SeqStepCounter = 1;
	SeqRowRunCounter = 1;
	SeqTestRunning = false;

//	tmTest->Enabled = false;
//	tmSeqTest2->Enabled = false;
//	tmSeqTestWait->Enabled = false;

	PauseState = false;

//	ShowMessage("Sequence Test Stopped");

	btnStartSeqGrid->Enabled = true;
	btnPause->Enabled = false;
	btnStopSeqGrid->Enabled = false;

	//USBMM->Stop();
	// StopUSBMM();

   	flgRunGenComport = false;

	lbCountDownState->Caption = "Wait Countdown:";
	lblWaitCountdown->Caption = "0";
}

//---------------------------------------------------------------------------

void __fastcall TmainForm::tmSeqTestWaitTimer(TObject *Sender)
{

if (TestStarted)
{
	// ***
	// Off Time
	// ***
//	Application->ProcessMessages();
//	CountDownProgressWait = CountDownProgressWait - 1;
//	lblWaitCountdown->Caption = IntToStr(CountDownProgressWait);

	if (OFFSeqGridTimeDone)
	{
		// Do Nothing
	   //	OFFSeqGridTimeDone = false;

		// ***
		// ON time
		// ***
		if (ONSeqGridTimeDone)
		{
			// Do Nothing
		   //	ONSeqGridTimeDone = false;
		}
		else
		{
			ONSeqGridTimeDone = true;
		} // End If

//		Application->ProcessMessages();
//		CountDownProgressOnWait = CountDownProgressOnWait - 1;
//		lblWaitCountdown->Caption = IntToStr(CountDownProgressOnWait);

	}
	else
	{
		// Wait time Done
		OFFSeqGridTimeDone = true;
	} // End If

	tmSeqTestWait->Enabled = false;
}
else
{
	tmSeqTestWait->Enabled = false;
} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnOpenPSUGWPortClick(TObject *Sender)
{

	//if (cbPSUOptions->ItemIndex == 1)
	if (!cbEVK->Checked)
	{
		   //	ShowMessage("Other comport type NOT seleted");
	}
	else
	{
//		   PSU_ComportGW->ComNumber = UserSelectComport("Select PSU Com-Port", "PortPSU", 115200);
		   UserSelectComport(PSU_ComportGW,"Select GW PSU Comport", "PortPSU", 9600);

			if( PSU_ComportGW == NULL)
			{
				MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
			}
			else
			{
					//PSU_ComportGW->Open;

					edPSUComport->Text = IntToStr(PSU_ComportGW->ComNumber);

					PSU_ComportGW->FlushInBuffer();
					PSU_ComportGW->FlushOutBuffer();

					cbRunPSU_OnOff->Enabled =true;

					btEnablePSU->Enabled = true;

					shpPSUConnected->Brush->Color = clGreen;

			}  // End If

	} // End If

}
//---------------------------------------------------------------------------


void __fastcall TmainForm::cbEVKClick(TObject *Sender)
{
	if (cbEVK->Checked)
	{
//		ShowMessage("Check");

		cbPSUOptions->Enabled = true;

		btnOpenPSUGWPort->Enabled = false;
		edPSUComport->Enabled = false;
		edPSUSetVoltage->Enabled = false;

		edPSUComport->Enabled = true;
		edPSUSetVoltage->Enabled = true;
		edPSUAmps->Enabled = true;

		btDisablePSU->Enabled = false;

		btEnablePSU->Enabled = false;

	}
	else
	{
//		ShowMessage("NOT Check");
		btnOpenPSUGWPort->Enabled = false;
		edPSUComport->Enabled = true;
		edPSUSetVoltage->Enabled = true;

		cbPSUOptions->Enabled = false;

		edPSUComport->Enabled = false;
		edPSUSetVoltage->Enabled = false;
		edPSUAmps->Enabled = false;

		btDisablePSU->Enabled = false;

		btEnablePSU->Enabled = false;

		cbRunPSU_OnOff->Enabled =false;

		shpPSUConnected->Brush->Color = clSilver;

		PSU_ComportGW->FlushInBuffer();
		PSU_ComportGW->FlushOutBuffer();


	} // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::PSUGWSetVDCandAMPS(AnsiString VDCSet, AnsiString AMPSSet)
{
/*
		//AnsiString SetVoltCmd = "APPLY ";
		AnsiString SetVoltCmd = "su";

//		 AnsiString Volts = 19;
//		 AnsiString Volts = StrToInt(edPSUSetVoltage->Text);
//		 AnsiString Current = 3;
//		 AnsiString Current = StrToInt(edPSUAmps->Text);

		 //AnsiString SetVoltage = SetVoltCmd + VDCSet + "," + AMPSSet;

		 AnsiString SetVoltage = SetVoltCmd + IntToStr(StrToInt(VDCSet) * 100);

		PSU_ComportGW->PutString(SetVoltage);
//		PSU_ComportGW->PutString("APPLY 15,3");
		PSU_ComportGW->PutString("\n");
*/
		AnsiString SetVoltCmd = "su";
	   //	AnsiString SetVoltage = SetVoltCmd; // + IntToStr(StrToInt(VDCSet) * 100);
		AnsiString SetVoltage = SetVoltCmd + VDCSet;

		PSU_ComportGW->PutString(SetVoltage);
		PSU_ComportGW->PutString("\n");

		AnsiString SetAmpCmd = "si";
//		AnsiString SetAmp = SetAmpCmd + FloatToStr(StrToFloat(AMPSSet) * 100);
		AnsiString SetAmp = SetAmpCmd + AMPSSet;

		PSU_ComportGW->PutString(SetAmp);
		PSU_ComportGW->PutString("\n");

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::edPSUAmpsDblClick(TObject *Sender)
{

	SetPSUVoltAndAmps();
/*
AnsiString Volts;
AnsiString Current;

	switch (cbPSUOptions->ItemIndex)
	{
		case 0:  // Hantek
			{
				Volts = Float2NoDecimalString(StrToFloat(edPSUSetVoltage->Text) * 100);
				Current = Float2NoDecimalString(StrToFloat(edPSUAmps->Text) * 1000);
			}
			break;
		case 1:  //
			{

			}
			break;
		case 2:  //
			{

			}
			break;
		default:
			{

			}
			break;
	} // End Switch

//	 AnsiString Current = edPSUAmps->Text;
//	PSU_ComportGW->PutString("APPLY 19,3");
//	PSU_ComportGW->PutString("\n");

	PSUGWSetVDCandAMPS(Volts, Current);
	ShowMessage("Voltage and Current Set");

//	PSUGWSetVDCandAMPS(edPSUSetVoltage->Text, edPSUAmps->Text);
*/

/*
	 AnsiString Volts = StrToInt(edPSUSetVoltage->Text);
	 AnsiString Current = StrToInt(edPSUAmps->Text);

	PSUGWSetVDCandAMPS(Volts, Current);
	ShowMessage("Voltage and Current Set");
*/
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::cbe_LoadSelectClick(TObject *Sender)
{
	if (cbe_LoadSelect->Checked)
	{
		//eload ON
/*
		btne_Load->Enabled = true;
		cbELoadCommands->Clear();
		cbELoadCommands->Text = "Select Mode";
		cbELoadCommands->Items->Add("Set Remote Mode");
		cbELoadCommands->Items->Add("Set Local Mode");
		cbELoadCommands->Items->Add("Set CC Mode");
		cbELoadCommands->Items->Add("Set Res Mode");
		cbELoadCommands->Items->Add("Set CC Value");
		cbELoadCommands->Items->Add("Set Res Value");

		Button8->Enabled = true;
		Button7->Enabled = true;

		cbELoadCommands->Enabled = true;
*/
		cbeLoadOptions->Enabled = true;

		//shpELoadConnected->Brush->Color = clGreen;
	}
	else
	{
		//eload OFF

		cbeLoadOptions->Enabled = false;

		btne_Load->Enabled = false;

		Button8->Enabled = false;
		Button7->Enabled = false;

		cbELoadCommands->Enabled = false;

		rgLoad->Enabled = false;

		shpELoadConnected->Brush->Color = clSilver;

	} // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Init_cbDAQTypes()
{

		cbDAQType->Clear();
		cbDAQType->Text = "Select DAQ";
		cbDAQType->Items->Add("NI USB 6009");  // 0
		cbDAQType->Items->Add("NI USB 6210");  // 1
		cbDAQType->Items->Add("Keysight 34972A");  // 2
		cbDAQType->Items->Add("DMM");  // 3
		cbDAQType->Items->Add("USE_MOCK_DMM");  // 4
		cbDAQType->Items->Add("USE_PSU_E_LOAD_DAQ");  // 5

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Init_cbPlotAreaOptions()
{

		cbPlotAreaOptions->Clear();
		cbPlotAreaOptions->Text = "Set Plot Area";
		cbPlotAreaOptions->Items->Add("Full Plot");  // 0
		cbPlotAreaOptions->Items->Add("User Set-Up");  // 1
		cbPlotAreaOptions->Items->Add("1/2 Plot Top");  // 2
		cbPlotAreaOptions->Items->Add("1/2 Plot Bot");  // 3
		cbPlotAreaOptions->Items->Add("1/4 Top LHS");  // 4
		cbPlotAreaOptions->Items->Add("1/4 Top RHS");  // 5
		cbPlotAreaOptions->Items->Add("1/4 Bot LHS");  // 6
		cbPlotAreaOptions->Items->Add("1/4 Bot RHS");  // 7

		cbPlotAreaOptions->ItemIndex = 0;

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Init_cbPSUOptions()
{

		cbPSUOptions->Clear();
		cbPSUOptions->Text = "PSU Type";
		cbPSUOptions->Items->Add("Hantek PPS2116A");  // 0
		cbPSUOptions->Items->Add("GW Instek");  // 1
		cbPSUOptions->Items->Add("Keysight");  // 2

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Init_cbFoDArduinoCodes()
{

	FoDArduinoCmds[1] = "x";  // all Off

	FoDArduinoCmds[2] = "a";  // c1 on
	FoDArduinoCmds[3] = "b";  // c1 off

	FoDArduinoCmds[4] = "c";  // c2 on
	FoDArduinoCmds[5] = "d";  // c2 off

	FoDArduinoCmds[6] = "e";  // c3 on
	FoDArduinoCmds[7] = "f";  // c3 off

	FoDArduinoCmds[8] = "g";  // c4 on
	FoDArduinoCmds[9] = "h";  // c4 off

	FoDArduinoCmds[10] = "i";  // c5 on
	FoDArduinoCmds[11] = "j";  // c5 off

	FoDArduinoCmds[12] = "k";  // c6 on
	FoDArduinoCmds[13] = "l";  // c6 off

	FoDArduinoCmds[14] = "m";  // c7 on
	FoDArduinoCmds[15] = "n";  // c7 off

	FoDArduinoCmds[16] = "o";  // Short Cal on
	FoDArduinoCmds[17] = "p";  // Short Cal off

/*	FoDArduinoCmds[18] = "g";  // c4 on
	FoDArduinoCmds[19] = "h";  // c4 off

	FoDArduinoCmds[20] = "i";  // c5 on
	FoDArduinoCmds[21] = "j";  // c5 off
*/

}


//---------------------------------------------------------------------------
void __fastcall TmainForm::Init_cbFoDArduinoCommandsOptions()
{

		cbFoDArduinoCommands->Clear();

		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[1] + " All off <-delete this text before send");

		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[2] + " C1 on <-delete this text before send");
		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[3] + " C1 off <-delete this text before send");

		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[4] + " C2 on <-delete this text before send");
		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[5] + " C2 off <-delete this text before send");

		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[6] + " C3 on <-delete this text before send");
		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[7] + " C3 off <-delete this text before send");

		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[8] + " C4 on <-delete this text before send");
		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[9] + " C4 off <-delete this text before send");

		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[10] + " C5 on <-delete this text before send");
		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[11] + " C5 off <-delete this text before send");

		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[12] + " C6 on <-delete this text before send");
		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[13] + " C6 off <-delete this text before send");

		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[14] + " C7 on <-delete this text before send");
		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[15] + " C7 off <-delete this text before send");

		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[16] + " Short CAL on <-delete this text before send");
		cbFoDArduinoCommands->Items->Add(FoDArduinoCmds[17] + " Short CAL off <-delete this text before send");

}



//---------------------------------------------------------------------------
void __fastcall TmainForm::Init_cbeLoadOptions()
{

		cbeLoadOptions->Clear();
		cbeLoadOptions->Text = "Rx load Type";
		cbeLoadOptions->Items->Add("Resistor");  // 0
		cbeLoadOptions->Items->Add("eLoad BK 8502");  // 1
//		cbeLoadOptions->Items->Add("other");  // 2

}

//---------------------------------------------------------------------------

void __fastcall TmainForm::btne_LoadClick(TObject *Sender)
{
	if (cbe_LoadSelect->Checked)
	{
//		   PSU_ComportGW->ComNumber = UserSelectComport("Select PSU Com-Port", "PortPSU", 115200);
//		   UserSelectComport(eLoad_Comport,"Select BK e_load Comport", "Port_e-Load", 38400);
		   UserSelectComport(eLoad_Comport,"Select BK e_load Comport", "Port_e-Load", 9600);

			if( eLoad_Comport == NULL)
			{
				MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
				cbELoadCommands->Enabled = false;
			}
			else
			{
					//PSU_ComportGW->Open;
					edeLoadPort->Text = IntToStr(eLoad_Comport->ComNumber);

					eLoad_Comport->FlushInBuffer();
					eLoad_Comport->FlushOutBuffer();

					cbELoadCommands->Enabled = true;


          			eLoad_SetRemote();
                    shpELoadConnected->Brush->Color = clGreen;


			} // End If

	}
	else
	{
		   //	ShowMessage("Other comport type NOT seleted");
	} // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Button8Click(TObject *Sender)
{
//	char cmdBuff[2];
//	cmdBuff[0] = 0xAA; //0
//	cmdBuff[1] = 0x00; //0
//	cmdBuff[2] = 0x00; //0

//	AnsiString HexDecodedString = IntToHex(30000,8);
//	AnsiString temp = HexDecodedString.SubString(5, 2);

//char *hexstring = temp; //"75";
//int x = atoi("75");
//unsigned int x = strtoul(temp.c_str(), NULL, 16);
//int number = (int)strtol(hexstring, NULL, 16);

//		int tre = HexStrToUint(&hexstring, 2, false);


	//int buff = (uint8_t)(cmdBuff[0]); // & 0xFF);

//AnsiString HexDecodedString = IntToHex(30000,4);
/*	int checksum = 0;
	for (int i = 0; i < 3; i++)
	{
		int test = (uint8_t)cmdBuff[i];
		checksum = checksum + test;

	}  // End For
*/

//	int val = HexCharToChkSum(&cmdBuff[0],3);
//	char test = (uint8_t)val;

//AnsiString HexDecodedString = IntToHex(30000,4);

//	for (int ib = 0; ib < 4; ib++)
//	{
//		AnsiString sdd = HexDecodedString.SubString(2*ib+1, 2);
//		cmdBuff[0] = StrToInt(sdd);
//	}  // End For

//	ShowMessage(IntToStr(int8(cmdBuff[0])));



//long tre = strtol(cmdBuff[0], 2, 8);

//  char hex_string[] = "0xbeef";
//	unsigned long hex_value	= strtol(hex_string, 0, 16);


//int x = strtol(hex.c_str(), NULL, 16);

   //	unsigned char serNum [] = "DS0008D-0111A";
//   unsigned short checksum;

//   checksum = CalcChksum(cmdBuff[0], 1);



//	eLoad_SetRemote();
//	eLoad_SetCCMode();
	eLoad_SetLoadON();

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::eLoad_SetRemote()
{
if (shpELoadConnected->Brush->Color == clSilver)
{
	// Not Set

}
else
{
	// OK


	char cmdBuff[26];
	cmdBuff[0] = 0xAA; //0
	cmdBuff[1] = 0x00; //1
	cmdBuff[2] = 0x20; //2  Remote
	cmdBuff[3] = 0x01; //3  ON
	cmdBuff[4] = 0x00; //4
	cmdBuff[5] = 0x00; //5
	cmdBuff[6] = 0x00; //6
	cmdBuff[7] = 0x00; //7
	cmdBuff[8] = 0x00; //8
	cmdBuff[9] = 0x00; //9
	cmdBuff[10] = 0x00; //10
	cmdBuff[11] = 0x00; //11
	cmdBuff[12] = 0x00; //12
	cmdBuff[13] = 0x00; //13
	cmdBuff[14] = 0x00; //14
	cmdBuff[15] = 0x00; //15
	cmdBuff[16] = 0x00; //16
	cmdBuff[17] = 0x00; //17
	cmdBuff[18] = 0x00; //18
	cmdBuff[19] = 0x00; //19
	cmdBuff[20] = 0x00; //20
	cmdBuff[21] = 0x00; //21
	cmdBuff[22] = 0x00; //22
	cmdBuff[23] = 0x00; //23
	cmdBuff[24] = 0x00; //24
	cmdBuff[25] = 0xCB; //25

	//int val = HexCharToChkSum(&cmdBuff[0],25);
	//char test = (uint8_t)val;

	for (int i = 0; i < 26; i++)
	{
		eLoad_Comport->PutChar(cmdBuff[i]);
	}  // End For

	eLoad_Comport->PutChar(DCR);
	eLoad_Comport->PutChar(DLE);

	for (int i = 0; i < 30; i++)
	{
		lbStatusGridSeq->Caption = "Comms eload Wait " + IntToStr(30 - i);
		Sleep(100);
		Application->ProcessMessages();
	} // End For

} // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::eLoad_SetLocal()
{
if (shpELoadConnected->Brush->Color == clSilver)
{
	// Not Set

}
else
{
	// OK

	char cmdBuff[26];
	cmdBuff[0] = 0xAA; //0
	cmdBuff[1] = 0x00; //1
	cmdBuff[2] = 0x20; //2  Remote
	cmdBuff[3] = 0x00; //3  OFF
	cmdBuff[4] = 0x00; //4
	cmdBuff[5] = 0x00; //5
	cmdBuff[6] = 0x00; //6
	cmdBuff[7] = 0x00; //7
	cmdBuff[8] = 0x00; //8
	cmdBuff[9] = 0x00; //9
	cmdBuff[10] = 0x00; //10
	cmdBuff[11] = 0x00; //11
	cmdBuff[12] = 0x00; //12
	cmdBuff[13] = 0x00; //13
	cmdBuff[14] = 0x00; //14
	cmdBuff[15] = 0x00; //15
	cmdBuff[16] = 0x00; //16
	cmdBuff[17] = 0x00; //17
	cmdBuff[18] = 0x00; //18
	cmdBuff[19] = 0x00; //19
	cmdBuff[20] = 0x00; //20
	cmdBuff[21] = 0x00; //21
	cmdBuff[22] = 0x00; //22
	cmdBuff[23] = 0x00; //23
	cmdBuff[24] = 0x00; //24
	cmdBuff[25] = 0xCA; //25

	for (int i = 0; i < 26; i++)
	{
		eLoad_Comport->PutChar(cmdBuff[i]);
	}  // End For

	eLoad_Comport->PutChar(DCR);
	eLoad_Comport->PutChar(DLE);

	for (int i = 0; i < 30; i++)
	{
		lbStatusGridSeq->Caption = "Comms eload Wait " + IntToStr(30 - i);
		Sleep(100);
		Application->ProcessMessages();
	} // End For

} // End If

}


//---------------------------------------------------------------------------
void __fastcall TmainForm::eLoad_SetLoadON()
{

if (shpELoadConnected->Brush->Color == clSilver)
{
	// Not Set

}
else
{
	// OK


	char cmdBuff[26];
	cmdBuff[0] = 0xAA; //0
	cmdBuff[1] = 0x00; //1
	cmdBuff[2] = 0x21; //2 Load
	cmdBuff[3] = 0x01; //3 ON
	cmdBuff[4] = 0x00; //4
	cmdBuff[5] = 0x00; //5
	cmdBuff[6] = 0x00; //6
	cmdBuff[7] = 0x00; //7
	cmdBuff[8] = 0x00; //8
	cmdBuff[9] = 0x00; //9
	cmdBuff[10] = 0x00; //10
	cmdBuff[11] = 0x00; //11
	cmdBuff[12] = 0x00; //12
	cmdBuff[13] = 0x00; //13
	cmdBuff[14] = 0x00; //14
	cmdBuff[15] = 0x00; //15
	cmdBuff[16] = 0x00; //16
	cmdBuff[17] = 0x00; //17
	cmdBuff[18] = 0x00; //18
	cmdBuff[19] = 0x00; //19
	cmdBuff[20] = 0x00; //20
	cmdBuff[21] = 0x00; //21
	cmdBuff[22] = 0x00; //22
	cmdBuff[23] = 0x00; //23
	cmdBuff[24] = 0x00; //24
	cmdBuff[25] = 0xCC; //25

	for (int i = 0; i < 26; i++)
	{
		eLoad_Comport->PutChar(cmdBuff[i]);
	}  // End For

   //	eLoad_Comport->PutChar(DCR);
   //	eLoad_Comport->PutChar(DLE);

	for (int i = 0; i < 30; i++)
	{
		lbStatusGridSeq->Caption = "Comms eload Wait " + IntToStr(30 - i);
		Sleep(100);
		Application->ProcessMessages();
	} // End For

   shpELoadConnected->Brush->Color = clLime;    //Red

} // End If
}



//---------------------------------------------------------------------------
void __fastcall TmainForm::eLoad_SetLoadOFF()
{

if (shpELoadConnected->Brush->Color == clSilver)
{
	// Not Set

}
else
{
	// OK

	char cmdBuff[26];
	cmdBuff[0] = 0xAA; //0
	cmdBuff[1] = 0x00; //1
	cmdBuff[2] = 0x21; //2 Load
	cmdBuff[3] = 0x00; //3 Off/
	cmdBuff[4] = 0x00; //4
	cmdBuff[5] = 0x00; //5
	cmdBuff[6] = 0x00; //6
	cmdBuff[7] = 0x00; //7
	cmdBuff[8] = 0x00; //8
	cmdBuff[9] = 0x00; //9
	cmdBuff[10] = 0x00; //10
	cmdBuff[11] = 0x00; //11
	cmdBuff[12] = 0x00; //12
	cmdBuff[13] = 0x00; //13
	cmdBuff[14] = 0x00; //14
	cmdBuff[15] = 0x00; //15
	cmdBuff[16] = 0x00; //16
	cmdBuff[17] = 0x00; //17
	cmdBuff[18] = 0x00; //18
	cmdBuff[19] = 0x00; //19
	cmdBuff[20] = 0x00; //20
	cmdBuff[21] = 0x00; //21
	cmdBuff[22] = 0x00; //22
	cmdBuff[23] = 0x00; //23
	cmdBuff[24] = 0x00; //24
	cmdBuff[25] = 0xCB; //25

	for (int i = 0; i < 26; i++)
	{
		eLoad_Comport->PutChar(cmdBuff[i]);
	}  // End For

//	eLoad_Comport->PutChar(DCR);
//	eLoad_Comport->PutChar(DLE);

	for (int i = 0; i < 30; i++)
	{
		lbStatusGridSeq->Caption = "Comms eload Wait " + IntToStr(30 - i);
		Sleep(100);
		Application->ProcessMessages();
	} // End For

   shpELoadConnected->Brush->Color = clGreen;

} // End If
}

//---------------------------------------------------------------------------
void __fastcall TmainForm::eLoad_SetCCMode()
{

if (shpELoadConnected->Brush->Color == clSilver)
{
	// Not Set

}
else
{
	// OK

	char cmdBuff[26];
	cmdBuff[0] = 0xAA; //0
	cmdBuff[1] = 0x00; //1
	cmdBuff[2] = 0x28; //2 Set CC, CV, CW, or CR mode
	cmdBuff[3] = 0x00; //3 CC
	cmdBuff[4] = 0x00; //4
	cmdBuff[5] = 0x00; //5
	cmdBuff[6] = 0x00; //6
	cmdBuff[7] = 0x00; //7
	cmdBuff[8] = 0x00; //8
	cmdBuff[9] = 0x00; //9
	cmdBuff[10] = 0x00; //10
	cmdBuff[11] = 0x00; //11
	cmdBuff[12] = 0x00; //12
	cmdBuff[13] = 0x00; //13
	cmdBuff[14] = 0x00; //14
	cmdBuff[15] = 0x00; //15
	cmdBuff[16] = 0x00; //16
	cmdBuff[17] = 0x00; //17
	cmdBuff[18] = 0x00; //18
	cmdBuff[19] = 0x00; //19
	cmdBuff[20] = 0x00; //20
	cmdBuff[21] = 0x00; //21
	cmdBuff[22] = 0x00; //22
	cmdBuff[23] = 0x00; //23
	cmdBuff[24] = 0x00; //24
//	cmdBuff[25] = 0xD2; //25
	int val = HexCharToChkSum(&cmdBuff[0],25);
	char CheckSum = (uint8_t)val;
	cmdBuff[25] = CheckSum; //25

	for (int i = 0; i < 26; i++)
	{
		eLoad_Comport->PutChar(cmdBuff[i]);
	}  // End For

//	eLoad_Comport->PutChar(DCR);
//	eLoad_Comport->PutChar(DLE);

	for (int i = 0; i < 30; i++)
	{
		lbStatusGridSeq->Caption = "Comms eload Wait " + IntToStr(30 - i);
		Sleep(100);
		Application->ProcessMessages();
	} // End For

} // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::eLoad_SetResMode()
{

if (shpELoadConnected->Brush->Color == clSilver)
{
	// Not Set

}
else
{
	// OK


	char cmdBuff[26];
	cmdBuff[0] = 0xAA; //0
	cmdBuff[1] = 0x00; //1
	cmdBuff[2] = 0x28; //2 Set CC, CV, CW, or CR mode
	cmdBuff[3] = 0x03; //3 Res
	cmdBuff[4] = 0x00; //4
	cmdBuff[5] = 0x00; //5
	cmdBuff[6] = 0x00; //6
	cmdBuff[7] = 0x00; //7
	cmdBuff[8] = 0x00; //8
	cmdBuff[9] = 0x00; //9
	cmdBuff[10] = 0x00; //10
	cmdBuff[11] = 0x00; //11
	cmdBuff[12] = 0x00; //12
	cmdBuff[13] = 0x00; //13
	cmdBuff[14] = 0x00; //14
	cmdBuff[15] = 0x00; //15
	cmdBuff[16] = 0x00; //16
	cmdBuff[17] = 0x00; //17
	cmdBuff[18] = 0x00; //18
	cmdBuff[19] = 0x00; //19
	cmdBuff[20] = 0x00; //20
	cmdBuff[21] = 0x00; //21
	cmdBuff[22] = 0x00; //22
	cmdBuff[23] = 0x00; //23
	cmdBuff[24] = 0x00; //24
//	cmdBuff[25] = 0xD2; //25
	int val = HexCharToChkSum(&cmdBuff[0],25);
	char CheckSum = (uint8_t)val;
	cmdBuff[25] = CheckSum; //25


	for (int i = 0; i < 26; i++)
	{
		eLoad_Comport->PutChar(cmdBuff[i]);
	}  // End For

//	eLoad_Comport->PutChar(DCR);
//	eLoad_Comport->PutChar(DLE);

	for (int i = 0; i < 30; i++)
	{
		lbStatusGridSeq->Caption = "Comms eload Wait " + IntToStr(30 - i);
		Sleep(100);
		Application->ProcessMessages();
	} // End For

} // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::eLoad_SetCCValue(AnsiString CCValue)
{

if (shpELoadConnected->Brush->Color == clSilver)
{
	// Not Set

}
else
{
	// OK

	char cmdBuffVal[4];
	cmdBuffVal[0] = 0x01; //0 MSB
	cmdBuffVal[1] = 0x01; //1
	cmdBuffVal[2] = 0x01; //2
	cmdBuffVal[3] = 0x01; //3 LSB

//	IntToHexByteBuff2(StrToInt(edELoadValue->Text), &cmdBuffVal[0], 4);

switch (cbeLoadOptions->ItemIndex)
		{
		case 0:  // res
			{

			break;
			}
		case 1:  // USE_BK
			{
				IntToHexByteBuff2(((int)(StrToFloat(CCValue) * 10000)), &cmdBuffVal[0], 4);
			break;
			}
		case 2:  //
			{

			break;
			}
		case 3:  // USE_DMM
			{

			break;
			}
		default:
			break;
	} // End Switch


	char cmdBuff[26];
	cmdBuff[0] = 0xAA; //0
	cmdBuff[1] = 0x00; //1
	cmdBuff[2] = 0x2A; //2 Set CC, CV, CW, or CR mode
	cmdBuff[3] = cmdBuffVal[3]; //3 CC LSB (mA Units)
	cmdBuff[4] = cmdBuffVal[2]; //4
	cmdBuff[5] = cmdBuffVal[1]; //5
	cmdBuff[6] = cmdBuffVal[0]; //6 CC MSB
	cmdBuff[7] = 0x00; //7
	cmdBuff[8] = 0x00; //8
	cmdBuff[9] = 0x00; //9
	cmdBuff[10] = 0x00; //10
	cmdBuff[11] = 0x00; //11
	cmdBuff[12] = 0x00; //12
	cmdBuff[13] = 0x00; //13
	cmdBuff[14] = 0x00; //14
	cmdBuff[15] = 0x00; //15
	cmdBuff[16] = 0x00; //16
	cmdBuff[17] = 0x00; //17
	cmdBuff[18] = 0x00; //18
	cmdBuff[19] = 0x00; //19
	cmdBuff[20] = 0x00; //20
	cmdBuff[21] = 0x00; //21
	cmdBuff[22] = 0x00; //22
	cmdBuff[23] = 0x00; //23
	cmdBuff[24] = 0x00; //24
	int val = HexCharToChkSum(&cmdBuff[0],25);
	char CheckSum = (uint8_t)val;
	cmdBuff[25] = CheckSum; //25

	for (int i = 0; i < 26; i++)
	{
		eLoad_Comport->PutChar(cmdBuff[i]);
	}  // End For

//	eLoad_Comport->PutChar(DCR);
//	eLoad_Comport->PutChar(DLE);

	for (int i = 0; i < 30; i++)
	{
		lbStatusGridSeq->Caption = "Comms eload Wait " + IntToStr(30 - i);
		Sleep(100);
		Application->ProcessMessages();
	} // End For

} // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::eLoad_SetResValue(AnsiString CCValue)
{

if (shpELoadConnected->Brush->Color == clSilver)
{
	// Not Set

}
else
{
	// OK

	char cmdBuffVal[4];
	cmdBuffVal[0] = 0x01; //0 MSB
	cmdBuffVal[1] = 0x01; //1
	cmdBuffVal[2] = 0x01; //2
	cmdBuffVal[3] = 0x01; //3 LSB

//	IntToHexByteBuff2(StrToInt(edELoadValue->Text), &cmdBuffVal[0], 4);

switch (cbeLoadOptions->ItemIndex)
		{
		case 0:  // res
			{

			break;
			}
		case 1:  // USE_BK
			{
			IntToHexByteBuff2(((int)(StrToFloat(CCValue) * 1000)), &cmdBuffVal[0], 4);  // Scale = 1^4
			break;
			}
		case 2:  //
			{

			break;
			}
		case 3:  // USE_DMM
			{

			break;
			}
		default:
			break;
	} // End Switch

	char cmdBuff[26];
	cmdBuff[0] = 0xAA; //0
	cmdBuff[1] = 0x00; //1
	cmdBuff[2] = 0x30; //2 Set CC, CV, CW, or CR mode
	cmdBuff[3] = cmdBuffVal[3]; //3 CC LSB (mA Units)
	cmdBuff[4] = cmdBuffVal[2]; //4
	cmdBuff[5] = cmdBuffVal[1]; //5
	cmdBuff[6] = cmdBuffVal[0]; //6 CC MSB
	cmdBuff[7] = 0x00; //7
	cmdBuff[8] = 0x00; //8
	cmdBuff[9] = 0x00; //9
	cmdBuff[10] = 0x00; //10
	cmdBuff[11] = 0x00; //11
	cmdBuff[12] = 0x00; //12
	cmdBuff[13] = 0x00; //13
	cmdBuff[14] = 0x00; //14
	cmdBuff[15] = 0x00; //15
	cmdBuff[16] = 0x00; //16
	cmdBuff[17] = 0x00; //17
	cmdBuff[18] = 0x00; //18
	cmdBuff[19] = 0x00; //19
	cmdBuff[20] = 0x00; //20
	cmdBuff[21] = 0x00; //21
	cmdBuff[22] = 0x00; //22
	cmdBuff[23] = 0x00; //23
	cmdBuff[24] = 0x00; //24
	int val = HexCharToChkSum(&cmdBuff[0],25);
	char CheckSum = (uint8_t)val;
	cmdBuff[25] = CheckSum; //25

	for (int i = 0; i < 26; i++)
	{
		eLoad_Comport->PutChar(cmdBuff[i]);
	}  // End For

//	eLoad_Comport->PutChar(DCR);
//	eLoad_Comport->PutChar(DLE);

	for (int i = 0; i < 30; i++)
	{
		lbStatusGridSeq->Caption = "Comms eload Wait " + IntToStr(30 - i);
		Sleep(100);
		Application->ProcessMessages();
	} // End For


} // End If



}

//---------------------------------------------------------------------------
void __fastcall TmainForm::eLoad_GetVIPower()
{

if (shpELoadConnected->Brush->Color == clSilver)
{
	// Not Set

}
else
{
	// OK


	char cmdBuff[26];
	cmdBuff[0] = 0xAA; //0
	cmdBuff[1] = 0x00; //1
	cmdBuff[2] = 0x5F; //2 Read V I and Power
	cmdBuff[3] = 0x00; //3
	cmdBuff[4] = 0x00; //4
	cmdBuff[5] = 0x00; //5
	cmdBuff[6] = 0x00; //6
	cmdBuff[7] = 0x00; //7
	cmdBuff[8] = 0x00; //8
	cmdBuff[9] = 0x00; //9
	cmdBuff[10] = 0x00; //10
	cmdBuff[11] = 0x00; //11
	cmdBuff[12] = 0x00; //12
	cmdBuff[13] = 0x00; //13
	cmdBuff[14] = 0x00; //14
	cmdBuff[15] = 0x00; //15
	cmdBuff[16] = 0x00; //16
	cmdBuff[17] = 0x00; //17
	cmdBuff[18] = 0x00; //18
	cmdBuff[19] = 0x00; //19
	cmdBuff[20] = 0x00; //20
	cmdBuff[21] = 0x00; //21
	cmdBuff[22] = 0x00; //22
	cmdBuff[23] = 0x00; //23
	cmdBuff[24] = 0x00; //24
	//cmdBuff[25] = 0x09; //25

	int val = HexCharToChkSum(&cmdBuff[0],25);
	char CheckSum = (uint8_t)val;
	cmdBuff[25] = CheckSum; //25

	for (int i = 0; i < 26; i++)
	{
		eLoad_Comport->PutChar(cmdBuff[i]);
	}  // End For

   //	eLoad_Comport->PutChar(DCR);
   //	eLoad_Comport->PutChar(DLE);

	for (int i = 0; i < 30; i++)
	{
		lbStatusGridSeq->Caption = "Comms eload Wait " + IntToStr(30 - i);
		Sleep(100);
		Application->ProcessMessages();
	} // End For

   shpELoadConnected->Brush->Color = clLime;    //Red

} // End If
}

//---------------------------------------------------------------------------
void __fastcall TmainForm::eLoad_ComportTriggerAvail(TObject *CP, WORD Count)
{

static char RxBuffer3[26];

char input;
static int i2;
static int Ri_Ptr2;

//char tempdata[4];

AnsiString RX_String;

	while (Count != 0)
	{
			input = eLoad_Comport->GetChar();

			if (input == char(0xAA))
			{
				// Data Start Found
				// Clear Buffer
				Ri_Ptr2 = 0;
				memset(RxBuffer3, 0, sizeof(RxBuffer3));
			}
			else
			{
				// Do Nothing
			} // End If

			//***
			// Accum data received
			//***
			RxBuffer3[Ri_Ptr2++] = input;
			//int test = (int)RxBuffer3[26];
			if ((int)RxBuffer3[25] == 0)
			{
				// Do nothing. Data End Checksum Not Found
			}
			else
			{
				//***
				//End of Data Checksum Found
				//***
				for (i2 = 0; i2 < sizeof(RxBuffer3); i2++)
				{
					RX_String = RX_String + RxBuffer3[i2];
				}  // End For

			   //	ShowMessage("ans = " + RX_String);

			/*********
			  Process Data
			********/

				//Check if Data = Status Reply
				if (RxBuffer3[2] == char(0x12))
				{
					// Data is Status Reply
					//ShowMessage("Status Reply eload msg received");
				}
				else
				{
					// Do Nothing
				}  // End If

				//***
				// VOLTAGE
				//***
				if (edeLoadDAQV->Text == "Read")
				{
					edeLoadDAQV->Text == "";
					// Check if data OK
					if (RxBuffer3[0] == char(0xAA))
					{
						// Check not status packet reply
						if (RxBuffer3[2] == char(0x12))
						{
							// Do Nothing
						}
						else
						{
							// ***
							// Data OK
							//***
							// Get mV 4 byte value
							//for (int i = 0; i < 4; i++)
							//{
							//	tempdata[i] = RxBuffer3[i + 3];
							//}  // End For

							//int eloadDaQVal = (uint8_t)(tempdata[3] << 24) | (uint8_t)(tempdata[2] << 16) | (uint8_t)(tempdata[1] << 8) | (uint8_t)(tempdata[0]);
							//int eloadDaQVal = ((uint8_t)tempdata[3] * 24) +  ((uint8_t)tempdata[2] * 16) + ((uint8_t)tempdata[1] * 8) + (uint8_t)tempdata[0];
							//int eloadDaQVal = (uint8_t)(tempdata[3] << 24) | (uint8_t)(tempdata[2] << 16) | (uint8_t)tempdata[1] << 8  | (uint8_t)(tempdata[0]);
							int eloadDaQVal = (uint8_t)(RxBuffer3[6] << 24) | (uint8_t)(RxBuffer3[5] << 16) | (uint8_t)RxBuffer3[4] << 8  | (uint8_t)(RxBuffer3[3]);

							char FormatDaqeLoad[10];

							float temp = eloadDaQVal/1000.0f;

							sprintf(FormatDaqeLoad,"%.3f", temp);
							AnsiString temp2 = FormatDaqeLoad;

							//	int x;
							//	x = *(int *)tempdata;

							//ShowMessage("Volts = " + IntToStr(x));
							edeLoadDAQV->Text = temp2;

						}  // End If
					}
					else
					{
						// Do Nothing
					}  // End If

				}
				else
				{

				} // End If

				//***
				// Current
				//***
				if (edeLoadDAQI->Text == "Read")
				{
					edeLoadDAQI->Text == "";
					// Check if data OK
					if (RxBuffer3[0] == char(0xAA))
					{
						// Check not status packet reply
						if (RxBuffer3[2] == char(0x12))
						{
							// Do Nothing
						}
						else
						{
							// ***
							// Data OK
							//***
							int eloadDaQValI = (uint8_t)(RxBuffer3[10] << 24) | (uint8_t)(RxBuffer3[9] << 16) | (uint8_t)RxBuffer3[8] << 8  | (uint8_t)(RxBuffer3[7]);

							char FormatDaqeLoadI[10];

							float temp3 = eloadDaQValI/10000.0f;

							sprintf(FormatDaqeLoadI,"%.3f", temp3);
							AnsiString temp4 = FormatDaqeLoadI;

							edeLoadDAQI->Text = temp4;

						}  // End If
					}
					else
					{
						// Do Nothing
					}  // End If

				}
				else
				{
					// Do Nothing
				} // End If


			} // End If






/*
			if (input == char(0xAA))
			if ((input == '\n') || (input == '\r'))	//if STX found in data resync
			{

				for (i2 = 0; i2 < sizeof(RxBuffer3); i2++)
				{
					RX_String = RX_String + RxBuffer3[i2];
				}  // End For

				if ((RX_String == "") || (RX_String == "wait"))
				{
					// Do nothing
				}
				else
				{
					//***
					// Message Received
					//***

					if (edPSUDAQV->Text == "ReadV")
					{
						edPSUDAQV->Text = RX_String;
					}
					else
					{
						if (edPSUDAQI->Text == "ReadI")
						{
							edPSUDAQI->Text = RX_String;
						}
						else
						{
							// Do Nothing
						} // End If
					} // End If

					//***
					// Ends Message Received
					//***

				} // End If

				// Clear Buffer
				Ri_Ptr2 = 0;
				memset(RxBuffer3, 0, sizeof(RxBuffer3));

				//edMiscInstruMsg->Text = RX_String;
			}
			else
			{
				RxBuffer3[Ri_Ptr2++] = input;
			} // End If
*/




	Count--;

//		} // end of (Recived_Message == false)
		Application->ProcessMessages();

	}  // End While


   //	ShowMessage(String(RxBuffer2[1]));
	//	Memo1->Lines->Add("Plotter msg : " + RX_String);

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::cbELoadCommandsClick(TObject *Sender)
{

if (GUIDisableCheckBox)
{
	// Do NOT run CB

}
else
{

//	ShowMessage("NOTE: Double click to Run");
//	ShowMessage("clscked");
/*
		cbELoadCommands->Items->Add("Set Remote Mode"); 0
		cbELoadCommands->Items->Add("Set Local Mode");   1
		cbELoadCommands->Items->Add("Set CC Mode");      2
		cbELoadCommands->Items->Add("Set Res Mode");     3
		cbELoadCommands->Items->Add("Set CC Value");     4
		cbELoadCommands->Items->Add("Set Res Value");    5

*/
	switch (cbELoadCommands->ItemIndex)
	{
		case 0:  //
//			ShowMessage("dff");
			eLoad_SetRemote();
			break;
		case 1:  //
			eLoad_SetLocal();
			break;
		case 2:  //
			eLoad_SetCCMode();
			break;
		case 3:  //
			//;
			eLoad_SetResMode();
			break;
		case 4:  //
			eLoad_SetCCValue(edELoadValue->Text);
			break;
		case 5:  //
			eLoad_SetResValue(edELoadValue->Text);
			break;
		default:
			break;
	} // End Switch

} // End If


}

//---------------------------------------------------------------------------

void __fastcall TmainForm::StrGridSeqTestClick(TObject *Sender)
{
		//SaveIniFileData(false);
	  //	ReadIniFile();
	  //ShowMessage("NOTE: Double Click any Cell to Save");

}

//---------------------------------------------------------------------------

void __fastcall TmainForm::StrGridSeqTestDrawCell(TObject *Sender, int ACol, int ARow,
          TRect &Rect, TGridDrawState State)
{
try
{

if( State.Contains(gdFixed) )
	{
//		StringGrid1->Canvas->Brush->Color = static_cast<TColor>(RGB(155, 155, 100));
//		StringGrid1->Canvas->Font->Style = TFontStyles() << fsBold;
//		StringGrid1->Canvas->Font->Color = static_cast<TColor>(RGB(250, 245, 135));
//		StringGrid1->Canvas->Rectangle(Rect);
	}
	else if( State.Contains(gdSelected) )
	{
//		StringGrid1->Canvas->Brush->Color = static_cast<TColor>(RGB(255, 205, 155));
//		StringGrid1->Canvas->Font->Style = TFontStyles() >> fsBold;
//		StringGrid1->Canvas->Font->Color = clNavy;
//		StringGrid1->Canvas->FillRect(Rect);
	}
	else
	{
//		StringGrid1->Canvas->Brush->Color = clWhite;
//		String  Grid1->Canvas->Font->Color = clBlue;
//		StringGrid1->Canvas->FillRect(Rect);
	}

	AnsiString text = StrGridSeqTest->Cells[ACol][ARow];

	if( ACol == 1 && ARow > 0 )
	{
		if (text == "")
		{
			// Do Nothing
		}
		else
		{
			if (isNumber(text))
			{
					if (StrToInt(text) > 0)
					{
						StrGridSeqTest->Canvas->Brush->Color = clGreen;
			//			StrGridSeqTest->Canvas->Brush->Color = static_cast<TColor>(RGB(105, 155, 100));
						StrGridSeqTest->Canvas->Font->Color = clBlue;
						StrGridSeqTest->Canvas->FillRect(Rect);
					} // End If
			}
			else
			{
				// Do Nothing
			} // End If

		} // End If

	}  // End If

	StrGridSeqTest->Canvas->TextRect(Rect, Rect.Left, Rect.Top, text);

}
catch (...)
{

} // End Try

}
//---------------------------------------------------------------------------


void __fastcall TmainForm::edSeqRowCountExit(TObject *Sender)
{
	UpdateSeqRow();
	NumberSeqGridRows();
}

//---------------------------------------------------------------------------
void __fastcall TmainForm::UpdateSeqRow()
{
//	ShowMessage("Exit");
	if (StrToInt(edSeqRowCount->Text) < 20)
	 {
		ShowMessage("Min number of rows = 20");
		edSeqRowCount->Text = 20;
//		StrGridSeqTest->RowCount = 20;
//		SeqGrid_Init
	 }
	 else
	 {
		StrGridSeqTest->RowCount = StrToInt(edSeqRowCount->Text);



//		SeqGrid_Init();
	 }// End if
}


//---------------------------------------------------------------------------

void __fastcall TmainForm::FileListBox1DblClick(TObject *Sender)
{

	if (TestStarted)
	{
		ShowMessage("Cannot Run as Test Started");
	}
	else
	{
	   FileListIniRead();
	   ReadIniFile();

//	   CheckSeqGridTitles();
	   CheckSeqKeywords();

		// Check if Misc Port for LCR set
//		if (SeqSeqKeywordLookup(SeqGridKeyword[41][0]))
//		{
			// Found Do Nothing
			//break;
			// *************************************
			// Append Grid Display for any Misc Data
			// *************************************
			AppendGridDataDisplayOption();
//		}
//		else
//		{
			// Do Nothing
//		} // End If


	TestTimeRemaining(-1);

	} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::FileListIniRead()
{

	// AnsiString INIFolderFilename = ChangeFileExt(Application->ExeName, "SeqGrid.INI");

	//		ShowMessage(FileListBox1->FileName);
	cbSeqIniFilenames->Clear();

	cbSeqIniFilenames->Text = FileListBox1->FileName;

	for (int i = 0; i < FileListBox1->Items->Count; i++)
	  {
	 //		ShowMessage(FileListBox1->FileName);

			// Ignore Ini file Seq



			cbSeqIniFilenames->Items->Add(FileListBox1->FileName);


	  } // end For

}
//---------------------------------------------------------------------------



void __fastcall TmainForm::cbSeqIniFilenamesDblClick(TObject *Sender)
{
		//Save file

	if (MessageDlg("Save Ini file?\nYes = SAVE\nNo = Exit", mtInformation, mbYesNo, 0) == mrYes)
	 {
		//ShowMessage("Yes");
		SaveIniFileData(false);
	 }
	 else
	 {
		 //ShowMessage("No");

	 }

	 //FileListBox1->Mask = "*.ini":
	 FileListBox1->Update();

}
//---------------------------------------------------------------------------


void __fastcall TmainForm::StringGrid1DblClick(TObject *Sender)
{
	//ShowMessage("sele");

	SelGridRowNumber = StringGrid1->RowCount - StringGrid1->Row;
	SelGridColNumber = StringGrid1->Col; // StringGrid1->ColCount - StringGrid1->Col;

//	if (TestStarted || SelectedGrid)
	if (TestStarted)
	{
	   // Do Nothing
//	   ShowMessage("Running Repeat test - Please Stop if finished");
		int UserAns = MessageDlg("Please Enter Option for 'Eff' Grid: \nYes = Show 'SIG STRENGTH' Grid \nNo = Show Seq Grid\nCancel = Exit", mtInformation, mbYesNoCancel, 0);
//		if (MessageDlg("Please Enter Option: Yes = Move Plotter to Selected Cell, No = Exit"  , mtInformation, mbYesNoCancel, 0) == mrYes)
		switch (UserAns)
		{
			case mrYes:
			{
				//ShowMessage("yes");
				StringGrid1->Visible = false;
				StringGrid2->Cells[0][0] = "SIG";
				StringGrid2->Visible = true;
//				StringGrid2->Cells[StringGrid1->Row][StringGrid1->Col] = StringGrid2->Cells[StringGrid1->Row][StringGrid1->Col];
//				ColourGridCellGrn(StringGrid1->Row, StringGrid1->Col, 2);
//				StringGrid2->Row = StringGrid1->RowCount;
//				StringGrid2->Col = StringGrid1->Col;
//				StringGrid2-> SelectCell(StringGrid1->RowCount, StringGrid1->Col);
//				StringGrid2.SetFocus;

				break;
			}
			case mrNo:
			{
				//Show Seq Grid
				ShowSeqGrid();
				break;
			}

		default:
		{
			SelectedGrid = false;
		   break;
		}
	}  // End Switch

	}
	else
	{
		int UserAns = MessageDlg("Test Started:\nPlease Enter Option for 'Eff' Grid: \nYes = Move Plotter to Selected Cell \nNo = Show 'SIG STRENGTH' Grid \nCancel = Exit", mtInformation, mbYesNoCancel, 0);
//		if (MessageDlg("Please Enter Option: Yes = Move Plotter to Selected Cell, No = Exit"  , mtInformation, mbYesNoCancel, 0) == mrYes)
		switch (UserAns)
		{
			case mrYes:
			{
				// Move to SELECTED POINT

				//ShowMessage("Row = " + IntToStr(StringGrid1->Row) + " Col = " + IntToStr(StringGrid1->Col));

				int ColSteps = StringGrid1->Col;
				int RowSteps = StringGrid1->RowCount - StringGrid1->Row;
				int SelRow = StringGrid1->Row;

				MovePlotterToPointMAIN(ColSteps, RowSteps, SelRow, StrToFloat(ed3DZHeight2->Text), 1);
				//RunPlotterSequence2(ColNumber, RowNumber, iRow, StrToInt(ed3DZHeight->Text), SeqRowRunCounter);

/*

				//	ShowMessage("Yes");
				//	ebStepSize->Text;

				SelectedGrid = true;

	//			StringGrid1->Enabled = false;

				XYPlotter_setpoint_t tmpSetpoint;
				//	tmpSetpoint.XPos = StrToInt(ebXOffset->Text);
				//	tmpSetpoint.YPos = StrToInt(ebYOffset->Text);

				tmpSetpoint.XPos = ((SelGridColNumber - 1) *  StrToInt(ebStepSize->Text)) + StrToInt(ebXSetpointStart->Text);
				tmpSetpoint.YPos = ((SelGridRowNumber - 1)) * StrToInt(ebStepSize->Text) + StrToInt(ebYSetpointStart->Text);

				lbCords->Caption = "X: " + String(tmpSetpoint.XPos) + " Y: " + String(tmpSetpoint.YPos);

	//			Series1->Add()
		  //		ShowMessage("NOTE: Refer to 'Set Coordinates' for mm X-Y location");
		//		lbDistance->Caption = "X: " + String(curDistance.XPos) + " Y: " + String(curDistance.YPo

				if (cbSimPlotting->Checked)
				{
					// Do Nothing
				}
				else
				{
					//update plotter setpoint
					Plotter->UpdateSetpoint(tmpSetpoint);
					//Update user labels
					updatePosLabels(this);
				}  // End If


*/

					//=====================
					int UserAns2 = MessageDlg("Please Enter Option:\nYes = Plot on Selected Point\nNo = Clear Grid\nCancel = EXIT", mtInformation, mbYesNoCancel, 0);
					switch (UserAns2)
					{
						case mrYes:
						{

							// PLOT on SELECTED POINT
							PlotOnSelectedPoint = true;

							SeqTestRunning = true;

							cbDoNotMoveXY->Checked = true;

							//RunPlotSequencer();

							btnStopSeqGrid->Enabled = true;
							btnStartSeqGrid->Enabled = false;

							for (int i = 0; i < 1000000; i++)
							{

								Application->ProcessMessages();
								if (!SeqTestRunning) {break;}

								//RunPlotterSequence2(ColSteps, RowSteps, 1, StrToInt(ed3DZHeight->Text), 0);

								//***
								// Read DAQ
								// ***

								//Start USB Threads
								StartUSBMM();

								// Ensure this is Set as Var is used in timer below
								strRetryCountSet = ebHoleRetryNo->Text;
								tmMeterUpdate->Enabled = true;

								lbStatusGridSeq->Caption = "Read DAQ count (1e^60 max)= " + IntToStr(i);

								updateEfficiencySeqGrid2();


								char inputPowerStr[15];
								char outputPowerStr[15];
								char effStr[15];

								//build input power string
								sprintf(&inputPowerStr[0],"%.2f W",curMeas.inputPower);
								lbInputPower->Caption = &inputPowerStr[0];

								//build output power string
								sprintf(&outputPowerStr[0],"%.2f W",curMeas.outputPower);
								lbOutputPower->Caption = &outputPowerStr[0];

								//build efficiency string
								sprintf(&effStr[0],"%.2f %",curMeas.efficiency);
								lbEff->Caption = &effStr[0];


								//========================
								// Display Data to Grid
								//========================


								// Update Grid Cell Eff Value
//								StringGrid1->Cells[ColSteps][SelRow] = FloatToStr(curMeas.efficiency);
								StringGrid1->Cells[ColSteps][SelRow] = &effStr[0];
								// Update Grid Cell Sig Value
//								StringGrid2->Cells[ColSteps][SelRow] = FloatToStr(curMeas.efficiency);
								StringGrid2->Cells[ColSteps][SelRow] = &effStr[0];

							//***
							// Option to Read LCR Meter
							//***

							 // RunMiscInstrumentTest();

							//***
							// Ends Option to REad LCR Meter
							//***

								//========================
								// Ends Display Data to Grid
								//========================


								Sleep(10);

							} // End For

							lbStatusGridSeq->Caption = "Ends Read DAQ";
							SeqTestRunning = false;
							cbSimPlotting->Checked = false;

							cbDoNotMoveXY->Checked = false;

							break;
						}
						case mrNo:
						{

							lbStatusGridSeq->Caption = "Ends Read DAQ";
							SeqTestRunning = false;
							cbSimPlotting->Checked = false;

							cbDoNotMoveXY->Checked = false;

							ClearEff_Sig_Grids();

							PlotOnSelectedPoint = false;

							break;
						}
						default:
						{
							// Do Nothing
							PlotOnSelectedPoint = false;
							break;
						}
					} // End Switch
					//=====================

				break;
			}
			case mrNo:
			{

				// Sig Strength
				StringGrid1->Visible = false;
				StringGrid2->Cells[0][0] = "SIG";
				StringGrid2->Visible = true;

/*				int UserAns = MessageDlg("Please Enter Option: \nYes = Show 'SIG STRENGTH' Grid \nNo = Show Seq Grid \nCancel = Exit", mtInformation, mbYesNoCancel, 0);
					switch (UserAns)
					{
						case mrYes:
						{
							// Sig Strength
							StringGrid1->Visible = false;
							StringGrid2->Visible = true;
							break;
						}
						case mrNo:
						{
							ShowSeqGrid();
							break;
						}
						default:
						{

							break;
						}

					}  // End Switch

*/

//				StringGrid2->Cells[StringGrid1->Row][StringGrid1->Col] = StringGrid2->Cells[StringGrid1->Row][StringGrid1->Col];
//				ColourGridCellGrn(StringGrid1->Row, StringGrid1->Col, 2);
//				StringGrid2->Row = StringGrid1->RowCount;
//				StringGrid2->Col = StringGrid1->Col;
//				StringGrid2-> SelectCell(StringGrid1->RowCount, StringGrid1->Col);
//				StringGrid2.SetFocus;


				break;
			}

		default:
		{
			// 	ShowMessage("No");
			SelectedGrid = false;
		   break;
		}
	}  // End Switch

  }  // End If


}
//---------------------------------------------------------------------------

void __fastcall TmainForm::StringGrid2DblClick(TObject *Sender)
{
	SelGridRowNumber = StringGrid2->RowCount - StringGrid2->Row;
	SelGridColNumber = StringGrid2->Col; // StringGrid1->ColCount - StringGrid1->Col;

	int UserAns = MessageDlg("Please Enter Option for 'Sig Strength' Grid: \nYes = Show 'Efficiency' Grid \nNo = Exit", mtInformation, mbYesNo, 0);
		switch (UserAns)
		{
			case mrYes:
			{
				StringGrid2->Visible = false;
				StringGrid1->Cells[0][0] = "PLOT";
				StringGrid1->Visible = true;
				ColourGridCellGrn(StringGrid2->Row, StringGrid2->Col, 1);
//				StringGrid1->Cells[StringGrid2->Row][StringGrid2->Col] = StringGrid1->Cells[StringGrid2->Row][StringGrid2->Col];
//				StringGrid1->Row = StringGrid2->RowCount;
//				StringGrid1->Col = StringGrid2->Col;
//				StringGrid1SelectCell(self, StringGrid2->RowCount, StringGrid2->Col, Cancel);
//				StringGrid1->SetFocus;

				break;
			}
			case mrNo:
			{
				// Do Nothing
				break;
			}

		default:
		{
			// 	ShowMessage("No");
		   break;
		}
	}  // End Switch
}

//---------------------------------------------------------------------------
void __fastcall TmainForm::RunSequencer2()
{
AnsiString PlotCmd[10];
//int PlotIndex = 0;
static loopCount;
bool SeqDone;

try
{
	if (TestStarted)
	{
		Application->ProcessMessages();

		// Stop Clock to get data
		tmSeqTest2->Enabled = false;

		// ***
		// Run Seq
		// ***
		AnsiString test = StrGridSeqTest->Cells[1][SeqStepCounter];

//		if (SeqStepCounter < StrGridSeqTest->RowCount)
		SeqColourCEllGREEN(SeqStepCounter, 0);

		if ((UpperCase(test) == "END") || (SeqStepCounter == StrGridSeqTest->RowCount))
		{
			SeqDone = true;
		}
		else
		{
			SeqDone = false;
			if ((SeqStepCounter < StrGridSeqTest->RowCount) && (!SeqDone))
			{
				// Number Column
				StrGridSeqTest->Cells[1][SeqStepCounter] = IntToStr(SeqStepCounter);

				// ***
				// ACTION SEQ STEP
				// ***
				lbStatusGridSeq->Caption = "Run Seq Step";
			   //	ShowMessage("Run STEP");

			   // 1. Load Settings



			   // 2. Run Plotter Test

				//Enable Plotter Timer
			   //	tmPlotter->Enabled = true;

				RunPlotterSequence();

				// ***
				// ENDS ACTION SEQ STEP
				// ***
			}
			else
			{
				// Exceed Steps
			} // End If

		} // End If

//			SeqRepeatCount = 1;
			if (SeqDone)
			{
	//			ShowMessage("Done");
				lbStatusGridSeq->Caption = "Seq Done";
				// ====
				// Check for Repeat
				// ====
				if (SeqRepeatCount > (StrToInt(ebRepeat->Text) - 1))
				{
					lbStatusGridSeq->Caption = "Seq Repeat Completed";
					tmSeqTest2->Enabled = false;
					SeqTestDone = true;
					SeqTestSTOP();

					//ShowMessage("Test Sequence Done");

				}
				else
				{
				   lbStatusGridSeq->Caption = "Seq Repeat";
				   // ***
				   // Repeat Test
				   // ***

				   SeqStepCount = 0;
				   TestStarted = true;
				   SeqDone = false;
				   SeqStepCounter = 1;
				   SeqRepeatCount = SeqRepeatCount + 1;
				   tmSeqTest2->Enabled = true;
				   lbRepeatCountStatus->Caption = "Repeat Count = " + IntToStr(SeqRepeatCount);
				   // Repeat Test
				} // End If
			}
			else
			{
				lbStatusGridSeq->Caption = "Seq Next Step";
				SeqStepCount = SeqStepCount + 1;
				SeqStepCounter = SeqStepCounter + 1;
			} // End If

		// Stop Clock to get data
	   //	Only enable once plotter done
//		tmSeqTest2->Enabled = true;
	}
	else
	{
		// Do Nothing
		tmSeqTest2->Enabled = false;
	} // End If

}
catch (...)
{
	ShowMessage("Error in Sub -> RunSequencer2 : Seq Error. \nCHECK:\n1. All Comms Set\n2. eLoad is set for Remote Control \n3. Other problem");
}  // End Try

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::RunSequencer3()
{
//AnsiString PlotCmd[10];
//int PlotIndex = 0;
//static loopCount;
//bool SeqDone;

try
{

/*		TestStarted = true;
		SeqTestDone = false;
		SeqStepCount = 0;
		SeqRepeatCount = 0;
		SeqStepCounter = 0;
		SeqTestRunning = false;
*/

	if (SeqTestRunning)
	{
		// 1. Waiting on Timer to Finish Curent Seq Row Test

		// While in Waiting State use this Area as a timer.
		// Can use this area as a Heartbeat SEQUENTIAL (ie only 1 timer at a time!) timer 10ms.

		if (PauseState)
		{
			// Do Nothing
		}
		else
		{
			// Not Paused
			if (CountDownProgressWait > 0)
			{
				CountDownProgressWait = CountDownProgressWait - 1;
				lblWaitCountdown->Caption = IntToStr(CountDownProgressWait);
			}
			else
			{
				CountDownProgressWait = 0;
			} // End If

		} // End If

		//TestTimeRemaining();

		Application->ProcessMessages();

	}
	else
	{
		// ***
		// 2. Read Seq Row Data
		// ***

		// SeqColourCEllGREEN(SeqStepCounter, 0);
		// Read Seq data
		AnsiString SeqCtrl = StrGridSeqTest->Cells[2][SeqStepCounter];

			tmSeqTest2->Enabled = true;
			PauseState = false;
			testAdmin.testState = testAdmin.LastState;  //restore the last state

		if ((UpperCase(SeqCtrl) == "END") || (SeqStepCounter >= StrGridSeqTest->RowCount))
		{
			//***
			// 3. SEQUENCE END
			//***

			SeqTestSTOP();
			cbSeqIniFilenames->Enabled = true;

			//tmSeqTest2->Enabled = false;
			//tmTest->Enabled = false;

			ShowMessage("Ends Sequence");
			lbStatusGridSeq->Caption = "Ends Sequence";

			//***
			// End SEQUENCE END
			//***

		}
		else
		{
			//***
			//4. RUN SEQ
			//***
/*
			if (cbSimPlotting->Checked)
			{
				// Do Nothing as Simulating

			}
			else
			{
				//Create test file name

				//Create NEW test file name
				//testAdmin.testFile = CreateFileName();
				//Create test file Set 0
				//SaveGridData2CSV(0);

			} // End If
*/

			// Run Row marked or Repeating Row
//			if ((UpperCase(SeqCtrl) == "R") || ((SeqRepeatCount > 0) && (UpperCase(SeqCtrl) != NULL)))
			if ((UpperCase(SeqCtrl) == "R") || (UpperCase(SeqCtrl) == "P") || (SeqRepeatCount > 0))
			{
				// 5. RUN SEQUENCE ROW
				// 1 Set Form
				// 2 Set Next State
				// 3 Start Seq Timer
				// Inc actual sequence Row Run

				//SeqRowRunCounter = SeqRowRunCounter + 1;

				//***
				// Set Form Data from Seq Grid
				//***
				//int RowNumberSelected = StrGridSeqTest->Row;
				SetSeqData2Form(SeqStepCounter);

				// Number SEQ Row
				//RepeatSeqNoStr = StrGridSeqTest->Cells[16][SeqStepCounter];
				RepeatSeqNoStr = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[16][0]) ][SeqStepCounter];  // 16

				//***
				// Number seq step and Repeat Plot Control
				//***
				if (SeqRepeatCount > 0)
				{
					// Repeating Row
					// Do Nothing
				}
				else
				{
					// NUMBER GRID.  CELL AUTO COLOURED IF NUMBERED
					StrGridSeqTest->Cells[1][SeqStepCounter] = IntToStr(SeqRowRunCounter);
					lbRepeatCountStatus->Caption = "Repeat Count = " + IntToStr(SeqRepeatCount);


					//***
					// Check if Pause set
					//***
					if ((UpperCase(SeqCtrl) == "P"))
					{
						// Paused

						testAdmin.LastState = testAdmin.testState;  // save the current state so we can restore it later
						testAdmin.testState = TEST_PAUSE_STATE;
						PauseState = true;
						tmSeqTest2->Enabled = false;

						ShowMessage("Sequence HALTED due to letter p in Seq Col\nPress OK to CONTINUE\nNote: You can Press PAUSE Button on Form after OK button");

						tmSeqTest2->Enabled = true;
						PauseState = false;
						testAdmin.testState = testAdmin.LastState;  //restore the last state
					}
					else
					{
						// Do Nothing
					} // End If

				} // End If


				//****
				//Create NEW test file name
				//****
				// Modify name for any Seq changes done buy set Form function
				//ebFilename->Text = ebFilename->Text + "xx";

				testAdmin.testFile = CreateFileName();

				//Create test file Set 0
				SaveGridData2CSV(0, true);
				//SaveGridData2CSV(1, true);


				//***
				// ENDS Set Form Data from Seq Grid
				//***

//				SetSeqNextState();

				//=========START PLOT================
				//***
				// 5.1
				//***
				// Indicate running Seq
				SeqTestRunning = true;
				// Set timer State
				btnPlotComportOpen2->Enabled = false;

				RunPlotSequencer();  // This is alternative to timer below

				btnPlotComportOpen2->Enabled = true;
				SetSeqNextState();

				//testAdmin.testState = TEST_INIT_STATE;
				//tmTest->Enabled = true;

				//==================================
			}
			else
			{
				// 6. Skip Seq Row
				SeqStepCounter = SeqStepCounter + 1;
				SeqRepeatCount = 0;

			} // End If



			// Set Current Row States
			// Check if repeated Plotting
			if (SeqRepeatCount > 0)
			{
				// REPEAT Plot
				SeqGridRowNumber = SeqRowRunCounter;
				SeqGridStepNumber = SeqStepCounter;
			}
			else
			{
				// NO REPEAT Plot
				SeqGridRowNumber = SeqRowRunCounter - 1;
				SeqGridStepNumber = SeqStepCounter - 1;
			} // End If

		} // End If

	} // End If

}
catch (...)
{
//	ShowMessage("Error in Sub -> RunSequencer3 : Seq Error. \nCHECK:\n1. All Comms Set\n2. eLoad is set for Remote Control \n3. Other problem");
lbControlMessage->Caption = "Error in Sub -> RunSequencer3\nCheck if Sequencer Title exists etc";

}  // End Try

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SetSeqNextState()
{

			//RepeatSeqNoStr = StrGridSeqTest->Cells[16][SeqStepCounter];
			RepeatSeqNoStr = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[16][0]) ][SeqStepCounter];  // 16
			// Number seq step
			// Mark Col 0 with letter "S" for Colouring Green ref ONDraw Function of Grid
		//StrGridSeqTest->Cells[0][SeqStepCounter] = IntToStr(SeqStepCounter);
			//StrGridSeqTest->Cells[0][SeqStepCounter] = "S"; //IntToStr(SeqStepCounter);
/*
			if (SeqRepeatCount > 0)
			{
				// Repeating Row
				// Do Nothing
			}
			else
			{
				// NUMBER GRID.  CELL AUTO COLOURED IF NUMBERED
				StrGridSeqTest->Cells[1][SeqStepCounter] = IntToStr(SeqRowRunCounter);
			} // End If
*/
			//StrGridSeqTest->Cells[1][SeqStepCounter] = IntToStr(SeqStepCounter);

			//***
			// Set Next State
			//***
			// Check if repeat is empty
			if (RepeatSeqNoStr == "")
			{
				RepeatSeqNoStr = "0";

			}
			else
			{
				// Do Nothing
			} // End If

			//***
			// Repeat Seq
			//***
			int RepeatSeqNo = StrToInt(RepeatSeqNoStr);

			// Check if repeat
			if (RepeatSeqNo == 0)
			{
				// No Repeat
				//ebRepeat->Text= IntToStr(1);

				// Mark Actual seq done
				SeqRowRunCounter = SeqRowRunCounter + 1;
				// Number Grid Seq done
				//StrGridSeqTest->Cells[1][SeqStepCounter] = IntToStr(SeqRowRunCounter);

				SeqStepCounter = SeqStepCounter + 1;
				SeqRepeatCount = 0;
				lbRepeatCountStatus->Caption = "Repeat Count = " + IntToStr(SeqRepeatCount);
			}
			else
			{
				//***
				// Repeat
				//***

				// Compare
				if (SeqRepeatCount >= RepeatSeqNo)
				{
					// Ends Repeat. Next Step

					// Mark Actual seq done
					SeqRowRunCounter = SeqRowRunCounter + 1;  // Do Not increment Seq Row as repeating same Row

					// Number Grid Seq done
					//StrGridSeqTest->Cells[1][SeqStepCounter] = IntToStr(SeqRowRunCounter);

					SeqStepCounter = SeqStepCounter + 1;  // Move to next Row
					SeqRepeatCount = 0;
					// Test if No Repeats set
				}
				else
				{
					// REPEAT ROW
					SeqRepeatCount = SeqRepeatCount + 1;
				} // End If

				lbRepeatCountStatus->Caption = "Repeat Count = " + IntToStr(SeqRepeatCount);

				//***
				// Ends Repeat
				//***
				//ebRepeat->Text= IntToStr(SeqRepeatCount);
			} // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::tmSeqTest2Timer(TObject *Sender)
{

//	  RunSequencer2();
	  RunSequencer3();
	  Application->ProcessMessages();
	  //RunSequenceSteps
}

//---------------------------------------------------------------------------
void __fastcall TmainForm::CheckSeqKeywords(void)
{
AnsiString ColTitle;
AnsiString TitleResult;
bool ExitTitleCheck;
AnsiString KeywordStringOptions;

	//SeqGridKeyword[]

	if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
	{
		TitleResult = "";
		ExitTitleCheck = false;

		for (int i = 1; i < StrGridSeqTest->ColCount + 1; i++)
		{
			if (ExitTitleCheck)
			{
				break;
			}
			else
			{
				// Do Nothing
			} // End If

			ColTitle = StrGridSeqTest->Cells[i][0];

			if (ColTitle == "")
			{
				 break;
			}
			else
			{
				// Do Nothing
			} // End If


			if (SeqSeqKeywordLookup(ColTitle))
			{
				// Found Do Nothing
				//break;
			}
			else
			{
				// Not Found

//		TitleResult = TitleResult + ColTitle + "\n";

				// Edit Title??
				switch(MessageDlg("SEQUENCE GRID Title does NOT have Keyword inside Title text\n(NOTE use excel file in same folder for MAJOR edits) : ** " + ColTitle + " **\nYes= Edit\nNo = Check Next Title\nCANCEL = Exit", mtInformation, mbYesNoCancel, 0))
				 {
				case mrYes:
				 {

					while (!SeqSeqKeywordLookup(ColTitle))
					{
					 // Edit
						KeywordStringOptions = GenerateKeywordTextListStr();
						AnsiString NewTitle = InputBox("Keyword NOT Found",
						"Found Seq Grid Title not containing Keyword inside Title\nNOTE: Title Col for Data readings have prefix '>' eg >Vin = Voltage reading\nKeywords options separated by spaces are listed for Title not found: " + ColTitle, KeywordStringOptions);
						if (NewTitle == "")
						{
							//NewTitle == "??"; // Cannot have blank text

						}
						else
						{
							if (NewTitle == KeywordStringOptions)
							{
								 // Do Nothing
							}
							else
							{
								StrGridSeqTest->Cells[i][0] = NewTitle;
							} // End If

						} // End If

						ColTitle = StrGridSeqTest->Cells[i][0];


						if (MessageDlg("Yes = Continue Checking Titles\nNo= Exit Title Check", mtInformation, mbYesNo, 0) == mrYes)
						 {
						//	ShowMessage("Yes");
						 }
						 else
						 {
						  //	 ShowMessage("No");
								break;
								ExitTitleCheck = true;
						 } // End If


					}  // End While

/*					  if (SeqSeqKeywordLookup(ColTitle))
					  {
						// OK

					  }
					  else
					  {
					  // Not Found

					  } // End If
*/
					break;
				 }
				case mrNo:
				 {
					// Exit
//                    ExitTitleCheck
					break;
				 }
				default:
					ExitTitleCheck = true;
					break;
				 }  // End Switch

			} // End If


		}  // End For

/*
		if (TitleResult = "")
		{

		}
		else
		{

		} // End If

		ShowMessage("Seq Titles Not Found\n" + TitleResult);
*/

		//***
		// Final Check
		//***
		/*
		if (OverallSeqSeqKeywordCheck())
		{
			// OK
		}
		else
		{
			// Not OK
		} // End If
		*/

		// ***
		// Check any MISSING KEYWORDS
		//***
		for (int i = 1; i < 34; i++)
		{
			if (GetSeqGridColNofText(SeqGridKeyword[i][0]) < 0)
			{
				ShowMessage("Keyword MISSING -> " + SeqGridKeyword[i][0]);
			}
			else
			{
				// Do Nothing
			} // End If

		} // End For

	}
	else
	{
		ShowMessage("No Sequence Loaded to check titles");
	} // End If

}

//---------------------------------------------------------------------------
bool __fastcall TmainForm::OverallSeqSeqKeywordCheck(void)
{

AnsiString ColTitle;
AnsiString TitleResult;

		TitleResult = "";

	if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
	{

		for (int i = 1; i < StrGridSeqTest->ColCount + 1; i++)
		{

			ColTitle = StrGridSeqTest->Cells[i][0];

			if (ColTitle == "")
			{
				 break;
			}
			else
			{
				// Do Nothing
			} // End If

			if (SeqSeqKeywordLookup(ColTitle))
			{
				// Found
//				break;
			}
			else
			{
				// Not Found
				TitleResult = TitleResult + ColTitle + "\n";

			}

		}  // End For

		if (TitleResult == "")
		{
			// OK
			return true;
		}
		else
		{
			// Error
			TitleResult = TitleResult + ColTitle + "\n";
			ShowMessage("Seq Titles Not Found\n" + TitleResult);

			return false;

		}  // End If


	}
	else
	{
			ShowMessage("No Sequence Loaded to check titles");
			return true;  // Ignore Grid
	} // End If


}

//---------------------------------------------------------------------------
void __fastcall TmainForm::CreateSeqSeqKeywordList(void)
{


/* ========================================
 NOTE
	When you update this list also:-
	1. Update Excel ini Editor File


========================================= */

//int KeywordMax;

//***
// Keyword List
//***
// DATA ENTRY
	SeqGridKeyword[1][0] ="Seq Step";
	SeqGridKeyword[2][0] ="Stepmm";
	SeqGridKeyword[3][0] ="XStartmm";
	SeqGridKeyword[4][0] ="XNoSteps";
	SeqGridKeyword[5][0] ="YStartmm";
	SeqGridKeyword[6][0] ="YNoSteps";
	SeqGridKeyword[7][0] ="Zmm";
	SeqGridKeyword[8][0] ="AngDeg";
	SeqGridKeyword[9][0] ="PlanDeg";
	SeqGridKeyword[10][0] ="PSUVDC";
	SeqGridKeyword[11][0] ="PSUAmp";
	SeqGridKeyword[12][0] ="eload cc_res";
	SeqGridKeyword[13][0] ="eload Val";
	SeqGridKeyword[14][0] ="OFFmS";
	SeqGridKeyword[15][0] ="ONmS";
	SeqGridKeyword[16][0] ="RepeatNo";

// SPARE COLS
	SeqGridKeyword[17][0] ="r_s_p RunSkipPause";

// RESULTS COLS Prefix with '>'
	SeqGridKeyword[18][0] =">Plot Index";
	SeqGridKeyword[19][0] =">x Step";
	SeqGridKeyword[20][0] =">y Step";
	SeqGridKeyword[21][0] =">Z height";
	SeqGridKeyword[22][0] =">Rotate";
	SeqGridKeyword[23][0] =">Angular";
	SeqGridKeyword[24][0] =">Vin";
	SeqGridKeyword[25][0] =">Iin";
	SeqGridKeyword[26][0] =">Pin";
	SeqGridKeyword[27][0] =">Vout";
	SeqGridKeyword[28][0] =">Iout";
	SeqGridKeyword[29][0] =">Pout";
	SeqGridKeyword[30][0] =">Eff";
	SeqGridKeyword[31][0] =">Sig Stren";
	SeqGridKeyword[32][0] =">Coil 1";
	SeqGridKeyword[33][0] =">Coil 2";

// EXTRA
	SeqGridKeyword[34][0] ="PSU on_on/off";
	SeqGridKeyword[35][0] =">Results";

	SeqGridKeyword[36][0] ="man_graph GWLCR";
	SeqGridKeyword[37][0] ="ac_rdc GWLCR";
	SeqGridKeyword[38][0] ="cct ser_par GWLC";
	SeqGridKeyword[39][0] ="lev eg 200m GWLCR";

	SeqGridKeyword[40][0] ="l;c;r;q;d GWLCR";
	SeqGridKeyword[41][0] ="freqHz 1 GWLCR";
	SeqGridKeyword[42][0] ="freqHz 2 GWLCR";
	SeqGridKeyword[43][0] ="freqHz 3 GWLCR";
	SeqGridKeyword[44][0] ="freqHz 4 GWLCR";
	SeqGridKeyword[45][0] ="freqHz 5 GWLCR";
	SeqGridKeyword[46][0] ="freqHz 6 GWLCR";
	SeqGridKeyword[47][0] ="freqHz 7 GWLCR";
	SeqGridKeyword[48][0] ="freqHz 8 GWLCR";
	SeqGridKeyword[49][0] ="freqHz 9 GWLCR";

	SeqGridKeyword[50][0] ="freqHz 10 GWLCR";
	SeqGridKeyword[51][0] ="Misc";
	SeqGridKeyword[52][0] =">freqHz 1 GWLCR";
	SeqGridKeyword[53][0] =">freqHz 2 GWLCR";
	SeqGridKeyword[54][0] =">freqHz 3 GWLCR";
	SeqGridKeyword[55][0] =">freqHz 4 GWLCR";
	SeqGridKeyword[56][0] =">freqHz 5 GWLCR";
	SeqGridKeyword[57][0] =">freqHz 6 GWLCR";
	SeqGridKeyword[58][0] =">freqHz 7 GWLCR";
	SeqGridKeyword[59][0] =">freqHz 8 GWLCR";

	SeqGridKeyword[60][0] =">freqHz 9 GWLCR";
	SeqGridKeyword[61][0] =">freqHz 10 GWLCR";
	SeqGridKeyword[62][0] =">freqHz 1_1 GWLCR";
	SeqGridKeyword[63][0] =">freqHz 2_1 GWLCR";
	SeqGridKeyword[64][0] =">freqHz 3_1 GWLCR";
	SeqGridKeyword[65][0] =">freqHz 4_1 GWLCR";
	SeqGridKeyword[66][0] =">freqHz 5_1 GWLCR";
	SeqGridKeyword[67][0] =">freqHz 6_1 GWLCR";
	SeqGridKeyword[68][0] =">freqHz 7_1 GWLCR";
	SeqGridKeyword[69][0] =">freqHz 8_1 GWLCR";

	SeqGridKeyword[70][0] =">freqHz 9_1 GWLCR";
	SeqGridKeyword[71][0] =">freqHz 10_1 GWLCR";
	SeqGridKeyword[72][0] =">freqHz 1_2 GWLCR";
	SeqGridKeyword[73][0] =">freqHz 2_2 GWLCR";
	SeqGridKeyword[74][0] =">freqHz 3_2 GWLCR";
	SeqGridKeyword[75][0] =">freqHz 4_2 GWLCR";
	SeqGridKeyword[76][0] =">freqHz 5_2 GWLCR";
	SeqGridKeyword[77][0] =">freqHz 6_2 GWLCR";
	SeqGridKeyword[78][0] =">freqHz 7_2 GWLCR";
	SeqGridKeyword[79][0] =">freqHz 8_2 GWLCR";

	SeqGridKeyword[80][0] =">freqHz 9_2 GWLCR";
	SeqGridKeyword[81][0] =">freqHz 10_2 GWLCR";

	// Ti EVM LDC1614
	SeqGridKeyword[82][0] ="TiLDC on_off";
	SeqGridKeyword[83][0] =">TiLDC_L1";
	SeqGridKeyword[84][0] =">TiLDC_L2";
	SeqGridKeyword[85][0] =">TiLDC_L3";
	SeqGridKeyword[86][0] =">TiLDC_L4";
	SeqGridKeyword[87][0] =">TiLDC_L1freq";
	SeqGridKeyword[88][0] =">TiLDC_L2freq";
	SeqGridKeyword[89][0] =">TiLDC_L3freq";

	SeqGridKeyword[90][0] =">TiLDC_L4freq";

	// GW LCR extra
	SeqGridKeyword[91][0] ="Speed max_fast_med_slow";
	// GW LCR extra

	// Misc
	SeqGridKeyword[92][0] ="Append_Filename";

	// TimeStamp
	SeqGridKeyword[93][0] ="TestDayStamp";
	SeqGridKeyword[94][0] ="TestTimeStamp";

	// Spare
	SeqGridKeyword[95][0] ="New CSV yes_no";
	SeqGridKeyword[96][0] ="FoD Coil nos 1 2 3 20max";
	SeqGridKeyword[97][0] ="Wait FoD mS eg 100";
	SeqGridKeyword[98][0] ="FoD xmm";
	SeqGridKeyword[99][0] ="FoD ymm";

	SeqGridKeyword[100][0] ="";
	SeqGridKeyword[101][0] ="";
	SeqGridKeyword[102][0] ="";
	SeqGridKeyword[103][0] ="";
	SeqGridKeyword[104][0] ="";
	SeqGridKeyword[105][0] ="";
	SeqGridKeyword[106][0] ="";
	SeqGridKeyword[107][0] ="";
	SeqGridKeyword[108][0] ="";
	SeqGridKeyword[109][0] ="";

	SeqGridKeyword[110][0] ="";
	SeqGridKeyword[111][0] ="";
	SeqGridKeyword[112][0] ="";
	SeqGridKeyword[113][0] ="";
	SeqGridKeyword[114][0] ="";
	SeqGridKeyword[115][0] ="";
	SeqGridKeyword[116][0] ="";
	SeqGridKeyword[117][0] ="";
	SeqGridKeyword[118][0] ="";
	SeqGridKeyword[119][0] ="";

	SeqGridKeyword[120][0] ="";
	SeqGridKeyword[121][0] ="";
	SeqGridKeyword[122][0] ="";
	SeqGridKeyword[123][0] ="";
	SeqGridKeyword[124][0] ="";
	SeqGridKeyword[125][0] ="";
	SeqGridKeyword[126][0] ="";
	SeqGridKeyword[127][0] ="";
	SeqGridKeyword[128][0] ="";
	SeqGridKeyword[129][0] ="";

	SeqGridKeyword[130][0] ="";
	SeqGridKeyword[131][0] ="";
	SeqGridKeyword[132][0] ="";
	SeqGridKeyword[133][0] ="";
	SeqGridKeyword[134][0] ="";
	SeqGridKeyword[135][0] ="";
	SeqGridKeyword[136][0] ="";
	SeqGridKeyword[137][0] ="";
	SeqGridKeyword[138][0] ="";
	SeqGridKeyword[139][0] ="";

	SeqGridKeyword[140][0] ="";
	SeqGridKeyword[141][0] ="";
	SeqGridKeyword[142][0] ="";
	SeqGridKeyword[143][0] ="";
	SeqGridKeyword[144][0] ="";
	SeqGridKeyword[145][0] ="";
	SeqGridKeyword[146][0] ="";
	SeqGridKeyword[147][0] ="";
	SeqGridKeyword[148][0] ="";
	SeqGridKeyword[149][0] ="";

	SeqGridKeyword[150][0] ="";
	// max value set!!!!!


//***
// Ends Keyword List
//***


}

//---------------------------------------------------------------------------
AnsiString __fastcall TmainForm::GenerateKeywordTextListStr()
{
AnsiString KeywordListString;

int EveryTenItems;

	KeywordListString = "";

	EveryTenItems = 1;

	  for (int i = 1; i < 100; i++)
	  {
		  if (SeqGridKeyword[i][0] == "")
		  {
				break;
		  }
		  else
		  {


			  EveryTenItems = EveryTenItems + 1;
			  if (EveryTenItems > 9)
			  {
				  KeywordListString = KeywordListString + "\n " + SeqGridKeyword[i][0];
				  EveryTenItems = 1;
			  }
			  else
			  {
				  KeywordListString = KeywordListString + "| " + SeqGridKeyword[i][0];
			  } // End If


		  } // Enbd If

	  }  // End For

	  return KeywordListString;

}

//---------------------------------------------------------------------------
boolean __fastcall TmainForm::SeqSeqKeywordLookup(AnsiString WordtoCheck)
{
bool FoundWord;

	CreateSeqSeqKeywordList();

	//SeqGridKeyword[]
	FoundWord = false;

	for (int i = 1; i < 100; i++)
	{

		int test = AnsiPos(SeqGridKeyword[i][0], WordtoCheck);  // returns 0 is not in string
		if (!(test == 0))
//		if (SeqGridKeyword[i] == WordtoCheck)
		{
			// Found
			FoundWord = true;
			break;
		}
		else
		{
			// Not Found
			if (SeqGridKeyword[i][0] == "")
			{
				break;
			}
			else
			{
				// Do Nothing
			} // End If

		} // End If

//		ColTitle = StrGridSeqTest->Cells[i][0];
	}  // End For

	if (FoundWord)
	{
		return true;
	}
	else
	{
		return false;
	} // End If

}

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
bool __fastcall TmainForm::SetSeqData2Form(int SeqGridRowNo)
{

/*
Refer to [CreateSeqSeqKeywordList]

	Copy as follows--
//***
// Keyword List
//***
// DATA ENTRY
	SeqGridKeyword[1][0] ="No.";
	SeqGridKeyword[2][0] ="Stepmm";
	SeqGridKeyword[3][0] ="XStartmm";
	SeqGridKeyword[4][0] ="XNoSteps";
	SeqGridKeyword[5][0] ="YStartmm";
	SeqGridKeyword[6][0] ="YNoSteps";
	SeqGridKeyword[7][0] ="Zmm";
	SeqGridKeyword[8][0] ="AngDeg";
	SeqGridKeyword[9][0] ="PlanDeg";
	SeqGridKeyword[10][0] ="PSUVDC";
	SeqGridKeyword[11][0] ="PSUAmp";
	SeqGridKeyword[12][0] ="eload Type";
	SeqGridKeyword[13][0] ="eload Val";
	SeqGridKeyword[14][0] ="OFFmS";
	SeqGridKeyword[15][0] ="ONmS";
	SeqGridKeyword[16][0] ="RepeatNo";

// SPARE COLS
	SeqGridKeyword[17][0] ="Spare";

// RESULTS COLS
	SeqGridKeyword[18][0] ="Plot Index";
	SeqGridKeyword[19][0] ="x Step";
	SeqGridKeyword[20][0] ="y Step";
	SeqGridKeyword[21][0] ="Z height";
	SeqGridKeyword[22][0] ="Rot";
	SeqGridKeyword[23][0] ="Ang";
	SeqGridKeyword[24][0] ="Vin";
	SeqGridKeyword[25][0] ="Iin";
	SeqGridKeyword[26][0] ="Pin";
	SeqGridKeyword[27][0] ="Vout";
	SeqGridKeyword[28][0] ="Iout";
	SeqGridKeyword[29][0] ="Pout";
	SeqGridKeyword[30][0] ="Eff";
	SeqGridKeyword[31][0] ="Qi Sig Stren";
	SeqGridKeyword[32][0] ="Coil 1";
	SeqGridKeyword[33][0] ="Coil 2";


// EXTRA
	SeqGridKeyword[34][0] ="PSU on/off";
	SeqGridKeyword[35][0] ="Coil 2";

	SeqGridKeyword[36][0] ="GWLCR man_graph";
	SeqGridKeyword[37][0] ="GWLCR ac_rdc";
	SeqGridKeyword[38][0] ="GWLC cct ser_par";
	SeqGridKeyword[39][0] ="GWLCR lev eg 200m";

	SeqGridKeyword[40][0] ="GWLCR l;c;r;q;d";
	SeqGridKeyword[41][0] ="GWLCR freqHz 1";
	SeqGridKeyword[42][0] ="GWLCR freqHz 2";
	SeqGridKeyword[43][0] ="GWLCR freqHz 3";
	SeqGridKeyword[44][0] ="GWLCR freqHz 4";
	SeqGridKeyword[45][0] ="GWLCR freqHz 5";
	SeqGridKeyword[46][0] ="GWLCR freqHz 6";
	SeqGridKeyword[47][0] ="GWLCR freqHz 7";
	SeqGridKeyword[48][0] ="GWLCR freqHz 8";
	SeqGridKeyword[49][0] ="GWLCR freqHz 9";

	SeqGridKeyword[50][0] ="GWLCR freqHz 10";
	SeqGridKeyword[51][0] ="Misc";
	SeqGridKeyword[52][0] ="";
	SeqGridKeyword[53][0] ="";
	SeqGridKeyword[54][0] ="";
	SeqGridKeyword[55][0] ="";
	SeqGridKeyword[56][0] ="";
	SeqGridKeyword[57][0] ="";


//***
// Ends Keyword List
//***



*/

	//***
	// DATA
	//***


/*
   AnsiString SeqColTitlesNotFound = "";

	//***
	// Get Seq Grid Title Column Number
	//***
	int SeqGridCol = GetSeqGridColNofText("Stepmm");
	if ((SeqGridCol == -1) || (SeqGridCol > StrGridSeqTest->ColCount))
	{
		// No Grid opened?
		// Do Nothing
		//ShowMessage("Grid Title 'Stepmm' -> Not Found");
		SeqColTitlesNotFound = SeqColTitlesNotFound + "/n," + "Stepmm" ;
	}
	else
	{
		//ShowMessage("Found Grid Col No = " + IntToStr(ser));
	   ebStepSize->Text = StrGridSeqTest->Cells[SeqGridCol][SeqGridRowNo];  // 2
	} // End If
	//***
	// Ends Get Seq Grid Title Column Number
	//***
*/

   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[0][SeqGridRowNo];  // 0
   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[1][SeqGridRowNo];  // 1

	//int SeqGridCol = GetSeqGridColNofText(SeqGridKeyword[2])

/*
	if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
	{
		// Add Column Number
	   for (int i = 1; i < StrGridSeqTest->ColCount + 1; i++)
	   {
		 SeqGridKeyword[33][1] = GetSeqGridColNofText(SeqGridKeyword[2][1]);
	   }  // End For

	}
	else
	{
	// Do Nothing
	} // End If

*/

	// Append CSV file Title
	AnsiString SeqAppendTitle = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[92][0]) ][SeqGridRowNo];
	if (SeqAppendTitle == "")
	{
		// Do Nothing
	}
	else
	{
		//AnsiString FileSeqAppend =  " " + StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[92][0]) ][SeqGridRowNo];
		//AnsiString INIFolderFilename = ChangeFileExt(ebFilename->Text, FileSeqAppend);

		//ebFilename->Text = ebFilename->Text + ebFilenameSeqAppend->Text;
		//ebFilename->Text = INIFolderFilename + FileSeqAppend;

		ebFilenameSeqAppend->Text = "_" + SeqAppendTitle;

	} // End If

   // Other
   ebStepSize2->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[2][0]) ][SeqGridRowNo];  // 2

   ebXSetpointStart2->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[3][0]) ][SeqGridRowNo];  // 3
   ebXSteps2->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[4][0]) ][SeqGridRowNo];  // 4
   ebYSetpointStart2->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[5][0]) ][SeqGridRowNo];  // 5
   ebYSteps2->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[6][0]) ][SeqGridRowNo];  // 6
   ed3DZHeight2->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[7][0]) ][SeqGridRowNo];  // 7
   // cbOrienZRotation->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[8][0]) ][SeqGridRowNo];  // 8
   // cbAngleYRotation->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[9][0]) ][SeqGridRowNo];  // 9
   edPSUSetVoltage->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[10][0]) ][SeqGridRowNo];  // 10 edTxVDC
   edPSUAmps->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[11][0]) ][SeqGridRowNo];  // 11


   //***
   // eLoad
   //***
/*
		cbELoadCommands->Items->Add("Set Remote Mode"); 0
		cbELoadCommands->Items->Add("Set Local Mode");   1
		cbELoadCommands->Items->Add("Set CC Mode");      2
		cbELoadCommands->Items->Add("Set Res Mode");     3
		cbELoadCommands->Items->Add("Set CC Value");     4
		cbELoadCommands->Items->Add("Set Res Value");    5

*/

	if (UpperCase(StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[12][0]) ][SeqGridRowNo]) == "CC")
	{
		   GUIDisableCheckBox = true;
		   cbELoadCommands->ItemIndex = 2;
		   GUIDisableCheckBox = false;
	}
	else if (UpperCase(StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[12][0]) ][SeqGridRowNo]) == "RES")
	{
		   GUIDisableCheckBox = true;
		   cbELoadCommands->ItemIndex = 3;
		   GUIDisableCheckBox = false;
	}
	else if (UpperCase(StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[12][0]) ][SeqGridRowNo]) == "PASS")
	{
		   GUIDisableCheckBox = true;
		   cbELoadCommands->ItemIndex = 5;
		   GUIDisableCheckBox = false;
	}
	else
	{

	} // End If



   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[12][SeqGridRowNo];  // 12
   edELoadValue->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[13][0]) ][SeqGridRowNo];  // 13

   ebOffTime2->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[14][0]) ][SeqGridRowNo];  // 14

   ebPowerUpWait2->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[15][0]) ][SeqGridRowNo];  // 15

   // Repeat see also RepeatSeqNoStr (RepeatSeqNo  SeqRepeatCount) distributed in code!!!!!!!!!!!!!!!!!!!!!
   RepeatSeqNoStr = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[16][0]) ][SeqGridRowNo];  // 16

   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[17][0]) ][SeqGridRowNo];  // 17
   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[18][0]) ][SeqGridRowNo];  // 18
   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[19][0]) ][SeqGridRowNo];  // 19
   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[20][0]) ][SeqGridRowNo];  // 20


	if (StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[34][0]) ][SeqGridRowNo] == "on")
	{
		cbRunPSU_OnOff->Checked = false;
	}
	else
	{
		cbRunPSU_OnOff->Checked = true;
	} // End If


	//***
	// Misc Instrument GW LCR
	//***
	// Test if Title Exists for GW LCR
	if (GetSeqGridColNofText(SeqGridKeyword[36][0]) < 0)
	{
		// No GW LCR titles exist
		// Do Nothing
		if (shpMISDataConnected->Brush->Color == clLime)
		{
		   lbControlMessage->Caption = "WARNING: Misc Data ON but No Titles in Grid for control";
		}
		else
		{
			// Do Nothing

		} // End If

	}
	else
	{
		edGWLCR_Mode->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[36][0]) ][SeqGridRowNo];
		edGWLCR_Meas->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[37][0]) ][SeqGridRowNo];
		edGWLCR_cct->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[38][0]) ][SeqGridRowNo];
		edGWLCR_Level->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[39][0]) ][SeqGridRowNo];
		edGWLCR_Comp->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[40][0]) ][SeqGridRowNo];

		edGWLCR_Speed->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[91][0]) ][SeqGridRowNo];

		edTestFreqHz1of10->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[41][0]) ][SeqGridRowNo];
		edTestFreqHz2of10->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[42][0]) ][SeqGridRowNo];
		edTestFreqHz3of10->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[43][0]) ][SeqGridRowNo];
		edTestFreqHz4of10->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[44][0]) ][SeqGridRowNo];
		edTestFreqHz5of10->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[45][0]) ][SeqGridRowNo];
		edTestFreqHz6of10->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[46][0]) ][SeqGridRowNo];
		edTestFreqHz7of10->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[47][0]) ][SeqGridRowNo];
		edTestFreqHz8of10->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[48][0]) ][SeqGridRowNo];
		edTestFreqHz9of10->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[49][0]) ][SeqGridRowNo];
		edTestFreqHz10of10->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[50][0]) ][SeqGridRowNo];

	} // End If

	//***
	// Ti EVM
	//***
	if (GetSeqGridColNofText(SeqGridKeyword[82][0]) < 0)
	{
	   // Do Nothing
		cbTiEvmLDC_on_Off->Checked = false;
	}
	else
	{
		cbTiEvmLDC_on_Off->Checked = true;
	} // End If


	//***
	// Ends Misc Instrument GW LCR
	//***

	// FoD Wait after relay ON
	if (GetSeqGridColNofText(SeqGridKeyword[97][0]) < 0)
	{
	   // Do Nothing
	}
	else
	{
		AnsiString WaitBeforLCRRead = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[97][0]) ][SeqGridRowNo];
		if (WaitBeforLCRRead == "")
		{
		   ShowMessage("'Wait FoD mS eg 100' - Title NOT Found or Blank\nWill set default value = 100mS");
		   EdWaitBeforeLCRRead->Text = "100";
		}
		else
		{
		   EdWaitBeforeLCRRead->Text = WaitBeforLCRRead;
		} // End If

	} // End If

	//***
	// RESULTS
	//***

/*
	if (SeqColTitlesNotFound == "")
	{
		// OK
		// Do Nothing
	}
	else
	{
		ShowMessage("WARNING Column Title Keywords Missing as follows:/n" + SeqColTitlesNotFound);
	} // End If
*/

}


//---------------------------------------------------------------------------
bool __fastcall TmainForm::SetUpForm2SeqData(int SeqGridRowNo)
{

/*
Refer to [CreateSeqSeqKeywordList]

	Copy as follows--
//***
// Keyword List
//***
// DATA ENTRY
	SeqGridKeyword[1][0] ="No.";
	SeqGridKeyword[2][0] ="Stepmm";
	SeqGridKeyword[3][0] ="XStartmm";
	SeqGridKeyword[4][0] ="XNoSteps";
	SeqGridKeyword[5][0] ="YStartmm";
	SeqGridKeyword[6][0] ="YNoSteps";
	SeqGridKeyword[7][0] ="Zmm";
	SeqGridKeyword[8][0] ="AngDeg";
	SeqGridKeyword[9][0] ="PlanDeg";
	SeqGridKeyword[10][0] ="PSUVDC";
	SeqGridKeyword[11][0] ="PSUAmp";
	SeqGridKeyword[12][0] ="eload Type";
	SeqGridKeyword[13][0] ="eload Val";
	SeqGridKeyword[14][0] ="OFFmS";
	SeqGridKeyword[15][0] ="ONmS";
	SeqGridKeyword[16][0] ="RepeatNo";

// SPARE COLS
	SeqGridKeyword[17][0] ="Spare";

// RESULTS COLS
	SeqGridKeyword[18][0] ="Plot Index";
	SeqGridKeyword[19][0] ="x Step";
	SeqGridKeyword[20][0] ="y Step";
	SeqGridKeyword[21][0] ="Z height";
	SeqGridKeyword[22][0] ="Rot";
	SeqGridKeyword[23][0] ="Ang";
	SeqGridKeyword[24][0] ="Vin";
	SeqGridKeyword[25][0] ="Iin";
	SeqGridKeyword[26][0] ="Pin";
	SeqGridKeyword[27][0] ="Vout";
	SeqGridKeyword[28][0] ="Iout";
	SeqGridKeyword[29][0] ="Pout";
	SeqGridKeyword[30][0] ="Eff";
	SeqGridKeyword[31][0] ="Qi Sig Stren";
	SeqGridKeyword[32][0] ="Coil 1";
	SeqGridKeyword[33][0] ="Coil 2";


// EXTRA
	SeqGridKeyword[34][0] ="PSU on/off";
	SeqGridKeyword[35][0] ="Coil 2";

	SeqGridKeyword[36][0] ="GWLCR man_graph";
	SeqGridKeyword[37][0] ="GWLCR ac_rdc";
	SeqGridKeyword[38][0] ="GWLC cct ser_par";
	SeqGridKeyword[39][0] ="GWLCR lev eg 200m";

	SeqGridKeyword[40][0] ="GWLCR l;c;r;q;d";
	SeqGridKeyword[41][0] ="GWLCR freqHz 1";
	SeqGridKeyword[42][0] ="GWLCR freqHz 2";
	SeqGridKeyword[43][0] ="GWLCR freqHz 3";
	SeqGridKeyword[44][0] ="GWLCR freqHz 4";
	SeqGridKeyword[45][0] ="GWLCR freqHz 5";
	SeqGridKeyword[46][0] ="GWLCR freqHz 6";
	SeqGridKeyword[47][0] ="GWLCR freqHz 7";
	SeqGridKeyword[48][0] ="GWLCR freqHz 8";
	SeqGridKeyword[49][0] ="GWLCR freqHz 9";

	SeqGridKeyword[50][0] ="GWLCR freqHz 10";
	SeqGridKeyword[51][0] ="Misc";
	SeqGridKeyword[52][0] ="";
	SeqGridKeyword[53][0] ="";
	SeqGridKeyword[54][0] ="";
	SeqGridKeyword[55][0] ="";
	SeqGridKeyword[56][0] ="";
	SeqGridKeyword[57][0] ="";


//***
// Ends Keyword List
//***



*/

	//***
	// DATA
	//***


/*
   AnsiString SeqColTitlesNotFound = "";

	//***
	// Get Seq Grid Title Column Number
	//***
	int SeqGridCol = GetSeqGridColNofText("Stepmm");
	if ((SeqGridCol == -1) || (SeqGridCol > StrGridSeqTest->ColCount))
	{
		// No Grid opened?
		// Do Nothing
		//ShowMessage("Grid Title 'Stepmm' -> Not Found");
		SeqColTitlesNotFound = SeqColTitlesNotFound + "/n," + "Stepmm" ;
	}
	else
	{
		//ShowMessage("Found Grid Col No = " + IntToStr(ser));
	   ebStepSize->Text = StrGridSeqTest->Cells[SeqGridCol][SeqGridRowNo];  // 2
	} // End If
	//***
	// Ends Get Seq Grid Title Column Number
	//***
*/

   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[0][SeqGridRowNo];  // 0
   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[1][SeqGridRowNo];  // 1

	//int SeqGridCol = GetSeqGridColNofText(SeqGridKeyword[2])

/*
	if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
	{
		// Add Column Number
	   for (int i = 1; i < StrGridSeqTest->ColCount + 1; i++)
	   {
		 SeqGridKeyword[33][1] = GetSeqGridColNofText(SeqGridKeyword[2][1]);
	   }  // End For

	}
	else
	{
	// Do Nothing
	} // End If

*/
   StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[2][0]) ][SeqGridRowNo] = ebStepSize2->Text;  // 2

   StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[3][0]) ][SeqGridRowNo] = ebXSetpointStart2->Text;  // 3
   StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[4][0]) ][SeqGridRowNo] = ebXSteps2->Text;  // 4
   StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[5][0]) ][SeqGridRowNo] = ebYSetpointStart2->Text;  // 5
   StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[6][0]) ][SeqGridRowNo] = ebYSteps2->Text;  // 6
   StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[7][0]) ][SeqGridRowNo] = ed3DZHeight2->Text;  // 7
   // cbOrienZRotation->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[8][0]) ][SeqGridRowNo];  // 8
   // cbAngleYRotation->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[9][0]) ][SeqGridRowNo];  // 9
   StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[10][0]) ][SeqGridRowNo] = edPSUSetVoltage->Text;  // 10 edTxVDC
   StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[11][0]) ][SeqGridRowNo] = edPSUAmps->Text;  // 11


   //***
   // eLoad
   //***
/*
		cbELoadCommands->Items->Add("Set Remote Mode"); 0
		cbELoadCommands->Items->Add("Set Local Mode");   1
		cbELoadCommands->Items->Add("Set CC Mode");      2
		cbELoadCommands->Items->Add("Set Res Mode");     3
		cbELoadCommands->Items->Add("Set CC Value");     4
		cbELoadCommands->Items->Add("Set Res Value");    5

*/

/*
	if (UpperCase(StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[12][0]) ][SeqGridRowNo]) == "CC")
	{
		   GUIDisableCheckBox = true;
		   cbELoadCommands->ItemIndex = 2;
		   GUIDisableCheckBox = false;
	}
	else if (UpperCase(StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[12][0]) ][SeqGridRowNo]) == "RES")
	{
		   GUIDisableCheckBox = true;
		   cbELoadCommands->ItemIndex = 3;
		   GUIDisableCheckBox = false;
	}
	else if (UpperCase(StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[12][0]) ][SeqGridRowNo]) == "PASS")
	{
		   GUIDisableCheckBox = true;
		   cbELoadCommands->ItemIndex = 5;
		   GUIDisableCheckBox = false;
	}
	else
	{

	} // End If

*/

   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[12][SeqGridRowNo];  // 12
   StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[13][0]) ][SeqGridRowNo] = edELoadValue->Text;  // 13

   StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[14][0]) ][SeqGridRowNo] = ebOffTime2->Text;  // 14
   StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[15][0]) ][SeqGridRowNo] = ebPowerUpWait2->Text;  // 15
   //ebPowerUpWait->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[16][0]) ][SeqGridRowNo];  // 16
   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[17][0]) ][SeqGridRowNo];  // 17
   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[18][0]) ][SeqGridRowNo];  // 18
   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[19][0]) ][SeqGridRowNo];  // 19
   //	ebPowerUpWait->Text = StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[20][0]) ][SeqGridRowNo];  // 20

/*
	if (StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[34][0]) ][SeqGridRowNo] == "on")
	{
		cbRunPSU_OnOff->Checked = false;
	}
	else
	{
		cbRunPSU_OnOff->Checked = true;
	} // End If

*/
	//***
	// Misc Instrument GW LCR
	//***
	// Test if Title Exists for GW LCR
	if (GetSeqGridColNofText(SeqGridKeyword[36][0]) < 0)
	{
		// No GW LCR titles exist
		// Do Nothing
		if (shpMISDataConnected->Brush->Color == clLime)
		{
		   lbControlMessage->Caption = "WARNING: Misc Data ON but No Titles in Grid for control";
		}
		else
		{
			// Do Nothing

		} // End If

	}
	else
	{
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[36][0]) ][SeqGridRowNo] = edGWLCR_Mode->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[37][0]) ][SeqGridRowNo] = edGWLCR_Meas->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[38][0]) ][SeqGridRowNo] = edGWLCR_cct->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[39][0]) ][SeqGridRowNo] = edGWLCR_Level->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[40][0]) ][SeqGridRowNo] = edGWLCR_Comp->Text;

		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[91][0]) ][SeqGridRowNo] = edGWLCR_Speed->Text;

		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[41][0]) ][SeqGridRowNo] = edTestFreqHz1of10->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[42][0]) ][SeqGridRowNo] = edTestFreqHz2of10->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[43][0]) ][SeqGridRowNo] = edTestFreqHz3of10->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[44][0]) ][SeqGridRowNo] = edTestFreqHz4of10->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[45][0]) ][SeqGridRowNo] = edTestFreqHz5of10->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[46][0]) ][SeqGridRowNo] = edTestFreqHz6of10->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[47][0]) ][SeqGridRowNo] = edTestFreqHz7of10->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[48][0]) ][SeqGridRowNo] = edTestFreqHz8of10->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[49][0]) ][SeqGridRowNo] = edTestFreqHz9of10->Text;
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[50][0]) ][SeqGridRowNo] = edTestFreqHz10of10->Text;

	} // End If

/*
	//***
	// Ti EVM
	//***
	if (GetSeqGridColNofText(SeqGridKeyword[82][0]) < 0)
	{
	   // Do Nothing
		cbTiEvmLDC_on_Off->Checked = false;
	}
	else
	{
		cbTiEvmLDC_on_Off->Checked = true;
	} // End If

*/
	//***
	// Ends Misc Instrument GW LCR
	//***



	//***
	// RESULTS
	//***

/*
	if (SeqColTitlesNotFound == "")
	{
		// OK
		// Do Nothing
	}
	else
	{
		ShowMessage("WARNING Column Title Keywords Missing as follows:/n" + SeqColTitlesNotFound);
	} // End If
*/

}


//---------------------------------------------------------------------------
void __fastcall TmainForm::CheckSeqGridTitles()
{

/*
Stepmm
XStartmm
XNoSteps
YStartmm
YNoSteps
Zmm
AngDeg
PlanDeg
PSUVDC
PSUAmp
eload Type
eload Val
OFFmS
ONmS
RepeatNo
Spare
Plot Index
x Step
y Step
Z height
Rot
Ang
Vin
Iin
Pin
Vout
Iout
Pout
Eff
Qi Sig Stren
Coil 1
Coil 2
*/

/*

   AnsiString SeqColTitlesNotFound = "";

	//***
	// Get Seq Grid Title Column Number
	//***
	int SeqGridCol = GetSeqGridColNofText("Stepmm");
	if ((SeqGridCol == -1) || (SeqGridCol > StrGridSeqTest->ColCount))
	{
		// No Grid opened?
		// Do Nothing
		//ShowMessage("Grid Title 'Stepmm' -> Not Found");
		SeqColTitlesNotFound = SeqColTitlesNotFound + "/n," + "Stepmm" ;
	}
	else
	{
		//ShowMessage("Found Grid Col No = " + IntToStr(ser));

	} // End If
	//***
	// Ends Get Seq Grid Title Column Number
	//***








	SeqColTitlesNotFound = SeqColTitlesNotFound + "/n," + "Stepmm" ;
	SeqColTitlesNotFound = SeqColTitlesNotFound + "/n," + "Stepmm" ;







	if (SeqColTitlesNotFound == "")
	{
		// OK
		// Do Nothing
	}
	else
	{
		ShowMessage("WARNING Column Title Keywords Missing as follows:/n" + SeqColTitlesNotFound);
	} // End If

*/

}

//---------------------------------------------------------------------------
// Main test-application state machine
//---------------------------------------------------------------------------
void __fastcall TmainForm::ClearDataGridArray()
{

	// TestDataType[][][]  ** CHECK LIMITS!!!!

	for (int x = 0; x < 60; x++)
	{
		for (int y = 0; y < 100; y++)
		{
			for (int z = 0; z < 100; z++)
			{

				TestDataType[x][y][z] = "";

			} // End For
		} // End For
	} // End For


}

//---------------------------------------------------------------------------
// Main test-application state machine
//---------------------------------------------------------------------------
void __fastcall TmainForm::RunPlotSequencer()
{
bool PlotLeft2Right;
int ColNumber;
int RowNumber;

int PlotStartROWCount;
bool PlotStartROWCountOK;

int PlotEndROWCount;
bool PlotEndROWCountOK;

int PlotStartCOLCount;
bool PlotStartCOLCountOK;

int PlotEndCOLCount;
bool PlotEndCOLCountOK;

//int gblTotalPlotStepCount;

//       StringGrid1

//		StringGrid2->Visible = false;
//				StringGrid1->Cells[0][0] = "EFF";
//				StringGrid1->Visible = true;
//				ColourGridCellGrn(StringGrid2->Row, StringGrid2->Col, 1);
//				StringGrid1->Cells[StringGrid2->Row][StringGrid2->Col] = StringGrid1->Cells[StringGrid2->Row][StringGrid2->Col];
//				StringGrid1->Row = StringGrid2->RowCount;
//				StringGrid1->Col = StringGrid2->Col;
//				StringGrid1SelectCell(self, StringGrid2->RowCount, StringGrid2->Col, Cancel);
//				StringGrid1->SetFocus;

	//***
	// Init
	//***

	// Clear Temp Data Arrays
	ClearDataGridArray();
	UpdateDataGrid(0);

	PlotLeft2Right = true;
	ClearEff_Sig_Grids();

	gblTotalPlotStepCount = 0;

	//***
	// Set Power Supply
	//***
	 // AnsiString Volts = StrToInt(edPSUSetVoltage->Text);
	 // AnsiString Current = StrToInt(edPSUAmps->Text);
	 if (cbSimPlotting->Checked)
	 {
			// Simulate
	 }
	 else
	 {
		 //PSUGWSetVDCandAMPS(Volts, Current);
		 if (shpPSUConnected->Brush->Color == clSilver)
		 {
			// Do Nothing
		 }
		 else
		 {
			 SetPSUVoltAndAmps();
			// PSU_TurnON();
		 } // End If

	 } // End If

	//***
	// Set eload
	//***
/*
		cbELoadCommands->Items->Add("Set Remote Mode"); 0
		cbELoadCommands->Items->Add("Set Local Mode");  1
		cbELoadCommands->Items->Add("Set CC Mode");     2
		cbELoadCommands->Items->Add("Set Res Mode");    3
		cbELoadCommands->Items->Add("Set CC Value");    4
		cbELoadCommands->Items->Add("Set Res Value");   5
*/

	 if (cbSimPlotting->Checked)
	 {
			// Simulate
	 }
	 else
	 {
	   if (shpELoadConnected->Brush->Color == clSilver)
	   {
			// Do Nothing
	   }
	   else
	   {
			Set_eLoadValue();
			eLoad_SetLoadON();  // Leave ON
	   }  // End If

	 } // End If

	//***
	// ENDS Init
	//***

	if (SeqTestRunning)
	{
		// Indicate Current Sequencer Row Number
		//lbStatusGridSeq->Caption = "Run Seq Step Number ->" + IntToStr(SeqGridRowNumber);
		lbStatusGridSeq->Caption = "Run Seq Step Number ->" + IntToStr(SeqRowRunCounter);

		// Form Status Ind
		ebRepeat->Text= IntToStr(StrToInt(ebRepeat->Text) + 1);

		//ShowMessage("Run Plot Current \nCurrent Seq Grid Row = " + IntToStr(SeqGridStepNumber));

/*		// Check if repeated Plotting
		if (SeqRepeatCount > 0)
		{
			// REPEAT Plot
			// Form Status Ind
			lbStatusGridSeq->Caption = "Run Seq Step Number ->" + IntToStr(SeqRowRunCounter);
			// Form Status Ind
			ebRepeat->Text= IntToStr(StrToInt(ebRepeat->Text) + 1);

			ShowMessage("Run Plot Current \nCurrent Seq Grid Row = " + IntToStr(SeqStepCounter));
		}
		else
		{
			// NO REPEAT Plot
			// Form Status Ind
			lbStatusGridSeq->Caption = "Run Seq Step Number ->" + IntToStr(SeqRowRunCounter - 1);
			// Form Status Ind
			ebRepeat->Text= IntToStr(StrToInt(ebRepeat->Text) + 1);

			ShowMessage("Run Plot Current \nCurrent Seq Grid Row = " + IntToStr(SeqStepCounter - 1));

		} // End If
*/

	//	ShowMessage("Run Plot Current \nCurrent Seq Grid Row = " + IntToStr(SeqStepCounter));

//		ebRepeat->Text= IntToStr(SeqRepeatCount);

	//***
	//One Point Selected Plot only
	//***
	if (cbDoNotMoveXY->Checked)
	{
		//ShowMessage(StringGrid1->Row);
		PlotStartROWCount = StrToInt(StringGrid1->Row);
		PlotEndROWCount = StrToInt(StringGrid1->Row);
		PlotStartCOLCount = StrToInt(StringGrid1->Col);
		PlotEndCOLCount = StrToInt(StringGrid1->Col);

	}
	else
	{

		// Check Max Value and one > than other
		PlotStartROWCountOK = true;
		if (StrToInt(edXOffsetStart2->Text))
		{
		   // Do Nothing
		}
		else
		{
		   // Do Nothing
		} // End If

		if (PlotStartROWCountOK)
		{
		   // Do Nothing
		}
		else
		{
		   // Do Nothing
		} // End If

		//=============================

/*
		cbPlotAreaOptions->Items->Add("Full Plot");  // 0
		cbPlotAreaOptions->Items->Add("User Set-Up");  // 1
		cbPlotAreaOptions->Items->Add("1/2 Plot Top");  // 2
		cbPlotAreaOptions->Items->Add("1/2 Plot Bot");  // 3
		cbPlotAreaOptions->Items->Add("1/4 Top LHS");  // 4
		cbPlotAreaOptions->Items->Add("1/4 Top RHS");  // 5
		cbPlotAreaOptions->Items->Add("1/4 Bot LHS");  // 6
		cbPlotAreaOptions->Items->Add("1/4 Bot RHS");  // 7
*/

		switch (cbPlotAreaOptions->ItemIndex)
		{
		case 0 :
		{
			//ShowMessage("0 Selected");
			edXOffsetStart2->Text = "0"; // Start Row (x) Bot
			edXOffsetSteps2->Text = "0"; // End Row (x) Top

			edyOffsetStart2->Text = "0"; // Start Col (y) Left
			edyOffsetSteps2->Text = "0"; // End Col (y) Right
			break;
		}
		case 1 :
		{
			// Do Nothing
			break;
		}
		case 2 :
		{

			edXOffsetStart2->Text = IntToStr((StringGrid1->ColCount - 1) / 2);  // Start 1/2 Row down
			edXOffsetSteps2->Text = "0"; // End Row (x) Top

			edyOffsetStart2->Text = "0"; // Start Col (y) Left
			edyOffsetSteps2->Text = "0"; // End Col (y) Right

			break;
		}
		case 3 :
		{
			edXOffsetStart2->Text = "0"; // Start Row (x) Bot
			edXOffsetSteps2->Text = IntToStr((StringGrid1->ColCount - 1) / 2); // End 1/2 Row up

			edyOffsetStart2->Text = "0"; // Start Col (y) Left
			edyOffsetSteps2->Text = "0"; // End Col (y) Right

			break;
		}
		case 4 :
		{

			edXOffsetStart2->Text = IntToStr((StringGrid1->ColCount - 1) / 2);  // Start 1/2 Row down
			edXOffsetSteps2->Text = "0"; // End Row (x) Top

			edyOffsetStart2->Text = "0"; // Start Col (y) Left
			edyOffsetSteps2->Text = IntToStr((StringGrid1->RowCount - 1) / 2);  // End 1/2 Col Right

			break;
		}
		case 5 :
		{

			edXOffsetStart2->Text = IntToStr((StringGrid1->ColCount - 1) / 2);  // Start 1/2 Row down
			edXOffsetSteps2->Text = "0"; // End Row (x) Top

			edyOffsetStart2->Text = IntToStr((StringGrid1->RowCount - 1) / 2);  // Start 1/2 Col Right
			edyOffsetSteps2->Text = "0"; // End Col (y) Right

			break;
		}
		case 6 :
		{

			edXOffsetStart2->Text = "0"; // Start Row (x) Bot
			edXOffsetSteps2->Text = IntToStr((StringGrid1->ColCount - 1) / 2); // End 1/2 Row up;

			edyOffsetStart2->Text = "0"; // Start Col (y) Left
			edyOffsetSteps2->Text = IntToStr((StringGrid1->RowCount - 1) / 2);  // End 1/2 Col Right

			break;
		}

		case 7 :
		{

			edXOffsetStart2->Text =  "0"; // Start Row (x) Bot
			edXOffsetSteps2->Text = IntToStr((StringGrid1->ColCount - 1) / 2); // End 1/2 Row up;

			edyOffsetStart2->Text = IntToStr((StringGrid1->RowCount - 1) / 2);  // Start 1/2 Col Right
			edyOffsetSteps2->Text = "0"; // End Col (y) Right

			break;
		}
		case 8 :
		{
			// Spare
			break;
		}
		default:
			{
				//ShowMessage("Item Not Found");
				//ShowMessage("0 Selected");
/*
				edXOffsetStart->Text = "0";
				edXOffsetSteps->Text = "0";

				edyOffsetStart->Text = "0";
				edyOffsetSteps->Text = "0";
*/
				break;
			}
		} // End Switch

		//***
		// Set ROW START
		//***
		PlotStartROWCount = (StringGrid1->RowCount - 1) - StrToInt(edXOffsetStart2->Text);

		//***
		// Set ROW END
		//***
		if (StrToInt(edXOffsetSteps2->Text) == 0)
		{
			// END FULL PLOT
			PlotEndROWCount = 0;     // Default = 0
		}
		else
		{
			// END OFFSET PLOT
			PlotEndROWCount = PlotStartROWCount - StrToInt(edXOffsetSteps2->Text);     // Default = 0
		} // End If

		//=============================
		//***
		// Set COL START
		//***
		PlotStartCOLCount = StrToInt(edyOffsetStart2->Text) + 1;  // Default = 1

		//***
		// Set COL END
		//***
		if (StrToInt(edyOffsetSteps2->Text) == 0)
		{
			// END FULL PLOT
			PlotEndCOLCount =  StringGrid1->ColCount - 1;
		}
		else
		{
			// END OFFSET PLOT
			PlotEndCOLCount =  PlotStartCOLCount + StrToInt(edyOffsetSteps2->Text) - 1;  //  (StringGrid1->ColCount - 1) - (StrToInt(edyOffsetSteps->Text)); //(StringGrid1->ColCount - 1);
		} // End If
		//=============================

	} // End If


		//================================================================
		// Run Plot
		//================================================================
		//***
		// Scroll Along COLUMNS for each Row
		//***

		if (MeterConnUSBMM(1))
		{
			//ShowMessage("DAQ OK");
		}
		else
		{
			if (shpDAQConnected->Brush->Color == clSilver)
			{
				// Do  Nothing
			}
			else
			{
				//ShowMessage("DAQ NOT OK");
				StartUSBMM();
			} // End If

		} // End If

	 //	int TOTALPLOTPoints = (PlotEndROWCount - PlotStartROWCount) * (PlotEndCOLCount - PlotStartCOLCount)

		//		for (int iRow = StringGrid1->RowCount - 1; iRow > 0; iRow--)
		for (int iRow = PlotStartROWCount; iRow > PlotEndROWCount; iRow--)
		{
				Application->ProcessMessages();
				if (SeqTestRunning)
				{
					// Do Nothing
				}
				else
				{
					break;
				} // End If

				//***
				// Shift ROWS
				//***
//				for (int iCol = 1; iCol <= StringGrid1->ColCount - 1; iCol++)
				for (int iCol = PlotStartCOLCount; iCol <= PlotEndCOLCount; iCol++)
				{
					Application->ProcessMessages();
					if (SeqTestRunning)
					{
						// Do Nothing
					}
					else
					{
						break;
					} // End If

					// Check Direction
					if (PlotLeft2Right)
					{
						//Left to Right
						ColNumber = iCol;
					}
					else
					{
						// Right to Left
						//ColNumber = StringGrid1->ColCount - iCol;
						ColNumber = PlotEndCOLCount - iCol + PlotStartCOLCount;

					} // End If

					//===================================
					// Plot Data Point
					//===================================

					// Current Grid Index = iRow, ColNumber
					// Current Plotter Point = RowNumber, ColNumber
					// Plotter Grid Point starts at 1,1
					// Seq Grid Index Row number = SeqRowRunCounter

					// irow Counts Down so reverse
					RowNumber = StringGrid1->RowCount - iRow;

					//ColourGridCellGrn(ColPoint, RowPoint, 1);

					lblProgramStateIndicator->Caption = "PROG STATE: Row = " + IntToStr(RowNumber) + " Col = " + IntToStr(ColNumber);

//					if (cbSimPlotting->Checked)
//					{
						//***
						// RUN SIMULATION
						//***
//						  RunSIMULATION(ColNumber, RowNumber, iRow, SeqRowRunCounter);
//					}
//					else
//					{

						//***
						// RUN PLOTTER
						//***

/*						// Allow Time to go from end to start of test
						if (SeqRowRunCounter == 1)
						{
							// First Time Plot Allow Time to go to Start
						}
						else
						{

						} // End If
*/
					gblTotalPlotStepCount = gblTotalPlotStepCount + 1;

					//***
					// Check if First Row of Data
					//***

					// Measure Test time
					 clock_t start, end;
					 start = clock();

					//time_t firsttime, secondtime;
					//firsttime = time(NULL);  // Get start time

					// Calculate time if at Start of Plot
					if ((ColNumber == 1) && (RowNumber == 1))
					{
						// Do not include Wait time for Moving Plotter to Start
						 TestTimeRemaining(-1);
					}
					else
					{
						// OK to measure Plot Point time
						// Do Nothing  TestTimeRemaining(TestTimePerPlot);
					} // End If

					if ((iRow == PlotStartROWCount) && (iCol == PlotStartCOLCount))
					{
					  // First Row of Data

						RunPlotterSequence2(ColNumber, RowNumber, iRow, StrToFloat(ed3DZHeight2->Text), SeqRowRunCounter, true);

					}
					else
					{
						RunPlotterSequence2(ColNumber, RowNumber, iRow, StrToFloat(ed3DZHeight2->Text), SeqRowRunCounter, false);
					} // End If

					//secondtime = time(NULL); // Get stop time
					//float TestTimePerPlot = difftime(secondtime, firsttime);

				   end = clock();
				   float TestTimePerPlot = ((end - start) / CLK_TCK);
				   //ShowMessage("The time was: ");

					//ShowMessage(FloatToStr(TestTimePerPlot));

					 //(xx / TOTALPLOTPoints) * time per point
					 //timeRemainlb

					if ((ColNumber == 1) && (RowNumber == 1))
					{
						// Do not include Wait time for Moving Plotter to Start
						 TestTimeRemaining(-1);
					}
					else
					{
						// OK to measure Plot Point time
						 TestTimeRemaining(TestTimePerPlot);
					} // End If


//					} // End If

					//===================================
					// ENDS Plot Data Point
					//===================================

					//***
					// PAUSE PRESSED
					//***
					while (PauseState)
					{
						//Sleep(10);
						Application->ProcessMessages();
					} // End While
					//***
					// Ends PAUSE PRESSED
					//***

				}  // End For

				// Toggle Direction
				if (PlotLeft2Right)
				{
					PlotLeft2Right = false;
				}
				else
				{
					PlotLeft2Right = true;
				} // End If

		}  // End For
		//================================================================
		// ENDS Run Plot
		//================================================================

		gblTotalPlotStepCount = 0;

		//***
		// SEQ DONE
		//***

//		ShowMessage("Seq End");
		SeqTestRunning = false;
	}
	else
	{
		// Make sure timer is OFF when no Test Running

	} // End If
	//***
	// Ends Eff Plot TEST CODE
	//***
	//============================================================================================

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::RunSIMULATION(int ColPoint, int RowPoint, int RowGridPoint, int SeqPoint)
{
		// Current Grid Index = iRow, ColNumber
		// Current Plotter Point = RowNumber, ColNumber

		// SIMULATION
		// Current Grid Index = iRow, ColNumber
		// Seq Grid Index Row number = SeqRowRunCounter

		//lbControlMessage->Caption = "Plotter Runing in Simulation\n This is set in Maintenance Tab for Demo";
		MessagesGlobal(3);

		ColourGridCellGrn(ColPoint, RowGridPoint, 1);

		// Update Grid Cell Eff Value
		StringGrid1->Cells[ColPoint][RowGridPoint] = IntToStr(SeqPoint);
		// Update Grid Cell Sig Value
		StringGrid2->Cells[ColPoint][RowGridPoint] = SeqPoint;
		//ShowMessage("Plot Point = Col = " + IntToStr(ColNumber) + " Row = " + IntToStr(iRow));
		Sleep(100);

/*
	   lbCountDownState->Caption = "Wait Countdown: ON";
	   TimerSequential(StrToInt(ebPowerUpWait->Text));

	   lbCountDownState->Caption = "Wait Countdown: OFF";
	   TimerSequential(StrToInt(ebOffTime->Text));
*/

		//ReadDaQDataValuesTimer();
		ReadDaQDataValuesSeq();
		//tmMeterUpdate->Enabled = true;

		//WriteResults2Grid(ColPoint, RowPoint, RowGridPoint, 1, SeqPoint);
		WriteResults2SeqGrid(SeqRowRunCounter, ColPoint, RowPoint, RowGridPoint, 1, SeqPoint);

		SaveGridData2CSV(SeqRowRunCounter, false);


}

//---------------------------------------------------------------------------
void __fastcall TmainForm::WriteResult2PlotGrid(int ColPoint, int RowPoint, int RowGridPoint, int SeqPoint)
{

	  //cbGridTypeDisplay
	  //switch (cbDAQType->ItemIndex)

		// Update Grid Cell Eff Value
		//StringGrid1->Cells[ColPoint][RowGridPoint] = IntToStr(SeqPoint);
//StringGrid1->Cells[ColPoint][RowGridPoint] = FloatToStr(curMeas.efficiency);

		// Update Grid Cell Sig Value
StringGrid2->Cells[ColPoint][RowGridPoint] = SeqPoint;
/*
  From [InitGridDataDisplayOption]

Index
0	cbGridTypeDisplay->AddItem("Efficiency", NULL);
1	cbGridTypeDisplay->AddItem("Sig Strength", NULL);
2	cbGridTypeDisplay->AddItem("Vin", NULL);
3	cbGridTypeDisplay->AddItem("Iin", NULL);
4	cbGridTypeDisplay->AddItem("Pin", NULL);
5	cbGridTypeDisplay->AddItem("Vout", NULL);
6	cbGridTypeDisplay->AddItem("Iout", NULL);
7	cbGridTypeDisplay->AddItem("Pout", NULL);

8  LCR Freq 1 Data 1
9  LCR Freq 2 Data 1
10 LCR Freq 3 Data 1
11 LCR Freq 4 Data 1
12 LCR Freq 5 Data 1
13 LCR Freq 6 Data 1
14 LCR Freq 7 Data 1
15 LCR Freq 8 Data 1
16 LCR Freq 9 Data 1
17 LCR Freq 10 Data 1

18  LCR Freq 1 Data 2
19  LCR Freq 2 Data 2
20 LCR Freq 3 Data 2
21 LCR Freq 4 Data 2
22 LCR Freq 5 Data 2
23 LCR Freq 6 Data 2
24 LCR Freq 7 Data 2
25 LCR Freq 8 Data 2
26 LCR Freq 9 Data 2
27 LCR Freq 10 Data 2

*/

/*
AnsiString TestDataType [80][100][100];

TestData format [Type as below][x (ie col)][(y ie Row)]
0 Eff
1 Sig
2 Vin
3 Iin
4 Pin
5 Vout
6 Iout
7 Pout

Data LCR
8 LCRFreq1
9 LCRFreqVal1
10 LCRFreqVal2

11 LCRFreq2
12 LCRFreqVal1
13 LCRFreqVal2

14 LCRFreq3
15 LCRFreqVal1
16 LCRFreqVal2

17 LCRFreq4
18 LCRFreqVal1
19 LCRFreqVal2

20 LCRFreq5
21 LCRFreqVal1
22 LCRFreqVal2

23 LCRFreq6
24 LCRFreqVal1
25 LCRFreqVal2

26 LCRFreq7
27 LCRFreqVal1
28 LCRFreqVal2

29 LCRFreq8
30 LCRFreqVal1
31 LCRFreqVal2

32 LCRFreq9
33 LCRFreqVal1
34 LCRFreqVal2

35 LCRFreq10
36 LCRFreqVal1
37 LCRFreqVal2

38 TiLDC_L1
39 TiLDC_L2
40 TiLDC_L3
41 TiLDC_L4

42 TiLDC_L1freq
43 TiLDC_L2freq
44 TiLDC_L3freq
45 TiLDC_L4freq

*/

	if ( cbGridTypeDisplay->ItemIndex == -1 )
	{
		//AnsiString test = TestDataType[0][ColPoint-1][RowPoint-1];
		// Default to Eff
		StringGrid1->Cells[ColPoint][RowGridPoint] = TestDataType[0][ColPoint-1][RowPoint-1];
	}
	else
	{
		if ( cbGridTypeDisplay->ItemIndex < 8 )
		{
			// Efficiency Plotter
			//AnsiString test = TestDataType[cbGridTypeDisplay->ItemIndex][ColPoint-1][RowPoint-1];
			StringGrid1->Cells[ColPoint][RowGridPoint] = TestDataType[cbGridTypeDisplay->ItemIndex][ColPoint-1][RowPoint-1];
		}
		else
		{

			//---------
			// GW LCR Meter
			//---------
			 WriteOneGridPointGWLCR(StringGrid1->RowCount, (ColPoint - 1), (RowPoint - 1));

			//---------
			// Ti LDC EVM Meter
			//---------
			 WriteOneGridPointTiLDC(StringGrid1->RowCount, (ColPoint - 1), (RowPoint - 1));


		} // End If

	} // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::AppendMiscResult2PlotGrid(int ColPoint, int RowPoint, int RowGridPoint, int SeqPoint)
{


		// Update Grid Cell Sig Value
StringGrid2->Cells[ColPoint][RowGridPoint] = SeqPoint;
/*
  From [InitGridDataDisplayOption]

*/

/*
AnsiString TestDataType [60][100][100];

TestData format [Type as below][x (ie col)][(y ie Row)]
0 Eff
1 Sig
2 Vin
3 Iin
4 Pin
5 Vout
6 Iout
7 Pout

Data LCR
8 LCRFreq1
9 LCRFreqVal1
10 LCRFreqVal2

11 LCRFreq2
12 LCRFreqVal1
13 LCRFreqVal2

14 LCRFreq3
15 LCRFreqVal1
16 LCRFreqVal2

17 LCRFreq4
18 LCRFreqVal1
19 LCRFreqVal2

20 LCRFreq5
21 LCRFreqVal1
22 LCRFreqVal2

23 LCRFreq6
24 LCRFreqVal1
25 LCRFreqVal2

26 LCRFreq7
27 LCRFreqVal1
28 LCRFreqVal2

29 LCRFreq8
30 LCRFreqVal1
31 LCRFreqVal2

32 LCRFreq9
33 LCRFreqVal1
34 LCRFreqVal2

35 LCRFreq10
36 LCRFreqVal1
37 LCRFreqVal2

38 TiLDC_L1
39 TiLDC_L2
40 TiLDC_L3
41 TiLDC_L4

42 TiLDC_L1freq
43 TiLDC_L2freq
44 TiLDC_L3freq
45 TiLDC_L4freq

*/

	if ( cbGridTypeDisplay->ItemIndex == -1 )
	{
		//AnsiString test = TestDataType[0][ColPoint-1][RowPoint-1];
		// Default to Eff
		StringGrid1->Cells[ColPoint][RowGridPoint] = TestDataType[0][ColPoint-1][RowPoint-1];
	}
	else
	{
		//AnsiString test = TestDataType[cbGridTypeDisplay->ItemIndex][ColPoint-1][RowPoint-1];
		StringGrid1->Cells[ColPoint][RowGridPoint] = TestDataType[cbGridTypeDisplay->ItemIndex][ColPoint-1][RowPoint-1];

	} // End If

}



//---------------------------------------------------------------------------
void __fastcall TmainForm::RunPlotterSequence2(int ColPoint, int RowPoint, int RowGridPoint, float zHeight,  int SeqPoint, bool FirstRowData)
{
		// Current Grid Index = iRow, ColNumber
		// Current Plotter Point = RowNumber, ColNumber

		// SIMULATION
		// Current Grid Index = iRow, ColNumber
		// Seq Grid Index Row number Global Var = SeqRowRunCounter = SeqPoint

/*		STEPS
		1 Move Plotter to Test Point
		2 Power ON
		3 Wait ON Time
		4 Read Value from DAQ
		5 Save data to CSV File
		6 Power OFF
		7 Wait OFF Time
		8 If Not end of Test Go to 1.
*/

		//********************************************************
		// Step 1  Move Plotter
		//********************************************************
		lbStatusGridSeq->Caption = "State: MOVE PLOTTER";

		ColourGridCellGrn(ColPoint, RowGridPoint, 1);

		if (cbSimPlotting->Checked)
		{
			//***
			// RUN SIMULATION
			//***
			// Do Nothing
		}
		else
		{
			MovePlotterToPointMAIN(ColPoint, RowPoint, RowGridPoint, zHeight, SeqPoint);

		}  // End If

		//********************************************************
		// Step 2   Power ON
		//********************************************************
		lbStatusGridSeq->Caption = "State: PSU ON";
		if (shpPSUConnected->Brush->Color == clSilver)
		{
			// Do Nothing
		}
		else
		{
			// Power ON
			PSU_TurnON();
		} // End If

		//eload ON
//		eLoad_SetLoadON();

		//********************************************************
		// Step 3  Wait ON
		//********************************************************
		lbStatusGridSeq->Caption = "State: WAIT ON";
		lbCountDownState->Caption = "Wait Countdown: ON";
		TimerSequential(StrToInt(ebPowerUpWait2->Text));

		//********************************************************
	   // Step 4  Read DAQ
		//********************************************************
		lbStatusGridSeq->Caption = "State: Read DAQ";
		if (cbSimPlotting->Checked)
		{
			//***
			// RUN SIMULATION
			//***
			// Do Nothing
		}
		else
		{
			//ReadDaQDataValuesTimer();
			ReadDaQDataValuesSeq();
			//tmMeterUpdate->Enabled = true;

		}  // End If

		//---------------------------------------
		// EXTRA Misc results here
		//---------------------------------------

		RunMiscInstrumentTest(false);

		RunFoDCoilTest();

		RunMiscInstrumentTest_Ti_LDC();

/*		if (cbTiEvmLDC_on_Off->Checked)
		{
			if (Title in Grid)
			{
			   // OK to Run


			}
			else
			{
				// Do Nothing
			} // End If
		}
		else
		{
			// Do Nothing
		} // End If
*/

		//---------------------------------------
		// Ends EXTRA Misc results here
		//---------------------------------------

		//===
		// Put New Results on SEQUENCE GRID and Append to Total Data Array used for later Plot Grid GUI
		//===
		//WriteResults2Grid(ColPoint, RowPoint, RowGridPoint, zHeight, SeqPoint);
//		SeqStepCounter
		//WriteResults2SeqGrid(SeqRowRunCounter, ColPoint, RowPoint, RowGridPoint, zHeight, SeqPoint);
		WriteResults2SeqGrid(SeqStepCounter, ColPoint, RowPoint, RowGridPoint, zHeight, SeqPoint);

		//===
		// Display results on Plot GRID GUI
		//===

		WriteResult2PlotGrid(ColPoint, RowPoint, RowGridPoint, SeqPoint);


	/*
		//write Qi Stuff
		csvLine = csvLine + IntToStr(meas.Qi_Signal_Strength) + "," ;
		csvLine = csvLine + IntToStr(meas.Qi_Coil_Num_1) + "," ;
		csvLine = csvLine + IntToStr(meas.Qi_Coil_Num_2) + "," ;

		csvLine = csvLine + IntToStr(meas.RetryCount);

		csvLine = csvLine + "\n";
	*/

		//===
		// Ends Put results ot GRID
		//===


		//********************************************************
	   // Step 5 Save CSV
		//********************************************************
		lbStatusGridSeq->Caption = "State: Save CSV";
		//Append test file name
		// For first Row save Title settings and results else just results
		if (FirstRowData)
		{
			// First Row of data
			//SaveGridData2CSV(SeqRowRunCounter, true);
			SaveGridData2CSV(SeqStepCounter, true);
		}
		else
		{
			// After First Row of data
			//SaveGridData2CSV(SeqRowRunCounter, false);
			SaveGridData2CSV(SeqStepCounter, false);
		} // End If


		//********************************************************
	   // Step 6  Power OFF
		//********************************************************

		if (cbRunPSU_OnOff->Checked)
		{
			// on off
			lbStatusGridSeq->Caption = "State: PSU OFF";
			// Power OFF
			PSU_TurnOFF();
			//eload OFF

		}
		else
		{
			// PS On always
			lbStatusGridSeq->Caption = "State: PSU ON";
		} // End If

		//********************************************************
	   // Step 7 Wait OFF
		//********************************************************
		lbStatusGridSeq->Caption = "State: WAIT OFF";
		// Off time
	   lbCountDownState->Caption = "Wait Countdown: OFF";
	   TimerSequential(StrToInt(ebOffTime2->Text));

}


//---------------------------------------------------------------------------
void __fastcall TmainForm::WriteResults2SeqGrid(int SeqRowNo, int ColPoint, int RowPoint, int RowGridPoint, int zHeight,  int SeqPoint)
{
int ColNumber_Results_text;

try
{

//	MAIN_measurement_t meas;

/*
	for (ColNumber_Results_text = 0; ColNumber_Results_text < StrGridSeqTest->ColCount ; ColNumber_Results_text++)
	{
		if (UpperCase(StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo]) == "RESULT")
		{
		// Found Result Start Col
			  break;
		}
		else
		{

		} // End If

		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo];
	}  // End For

	 ColNumber_Results_text = ColNumber_Results_text + 1;

*/

		// Results Grid located in sub [InitDataResultsGrid]
		//StrGridSeqTest->Cells[0][SeqRowNo] = "";

/*
  From [CreateSeqSeqKeywordList]
// RESULTS COLS
	SeqGridKeyword[18][0] ="Plot Index";
	SeqGridKeyword[19][0] ="x Step";
	SeqGridKeyword[20][0] ="y Step";
	SeqGridKeyword[21][0] ="Z height";
	SeqGridKeyword[22][0] ="Rot";
	SeqGridKeyword[23][0] ="Ang";
	SeqGridKeyword[24][0] ="Vin";
	SeqGridKeyword[25][0] ="Iin";
	SeqGridKeyword[26][0] ="Pin";
	SeqGridKeyword[27][0] ="Vout";
	SeqGridKeyword[28][0] ="Iout";
	SeqGridKeyword[29][0] ="Pout";
	SeqGridKeyword[30][0] ="Eff";
	SeqGridKeyword[31][0] ="Qi Sig Stren";
	SeqGridKeyword[32][0] ="Coil 1";
	SeqGridKeyword[33][0] ="Coil 2";

// EXTRA
	SeqGridKeyword[34][0] ="PSU on/off";
	SeqGridKeyword[35][0] ="Results";
*/



		// Record Timestamp Option
		TDateTime today = Date();
		//ShowMessage(today);

		//TDateTime time = Time();
		//ShowMessage(time);
	if (GetSeqGridColNofText(SeqGridKeyword[93][0]) < 0)
	{
		// Do Nothing
	}
	else
	{
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[93][0]) ][SeqRowNo] = today.CurrentDate();  // Timestamp Option
	} // End If

	if (GetSeqGridColNofText(SeqGridKeyword[94][0]) < 0)
	{
		// Do Nothing
	}
	else
	{
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[94][0]) ][SeqRowNo] = today.CurrentTime();  // Timestamp Option
	} // End If

		//***
		// Other
		//***
		if (GetSeqGridColNofText(SeqGridKeyword[18][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[18][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[18][0]) ][SeqRowNo] = IntToStr(gblTotalPlotStepCount);  // 18 index

		if (GetSeqGridColNofText(SeqGridKeyword[19][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[19][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[19][0]) ][SeqRowNo] = IntToStr(ColPoint - 1); // 19 s step Less one as Starts at 1 but display as point 0

		if (GetSeqGridColNofText(SeqGridKeyword[20][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[20][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[20][0]) ][SeqRowNo] = IntToStr(RowPoint -1); // 20 y step Less one as Starts at 1 but display as point 0

		if (GetSeqGridColNofText(SeqGridKeyword[21][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[21][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[21][0]) ][SeqRowNo] = IntToStr(zHeight); // 21 z

		if (GetSeqGridColNofText(SeqGridKeyword[22][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[22][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[22][0]) ][SeqRowNo] = "Rot";  // 22 rot

		if (GetSeqGridColNofText(SeqGridKeyword[23][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[23][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[23][0]) ][SeqRowNo] = "Ang"; // 23 ang

		if (GetSeqGridColNofText(SeqGridKeyword[24][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[24][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[24][0]) ][SeqRowNo] = FloatToStr(curMeas.values[INPUT_VOLTAGE_INDEX]); //24 vin

		int ss = GetSeqGridColNofText(SeqGridKeyword[25][0]);

		if (GetSeqGridColNofText(SeqGridKeyword[25][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[25][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[25][0]) ][SeqRowNo] = FloatToStr(curMeas.values[INPUT_CURRENT_INDEX]);  //25 iin

		if (GetSeqGridColNofText(SeqGridKeyword[26][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[26][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[26][0]) ][SeqRowNo] = FloatToStr(curMeas.inputPower); // 26 pin

		if (GetSeqGridColNofText(SeqGridKeyword[27][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[27][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[27][0]) ][SeqRowNo] = FloatToStr(curMeas.values[OUTPUT_VOLTAGE_INDEX]);  // 27 vout

		if (GetSeqGridColNofText(SeqGridKeyword[28][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[28][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[28][0]) ][SeqRowNo] = FloatToStr(curMeas.values[OUTPUT_CURRENT_INDEX]); // 28 iout

		if (GetSeqGridColNofText(SeqGridKeyword[29][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[29][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[29][0]) ][SeqRowNo] = FloatToStr(curMeas.outputPower);  // 29 pout

		if (GetSeqGridColNofText(SeqGridKeyword[30][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[30][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[30][0]) ][SeqRowNo] = FloatToStr(curMeas.efficiency);  // 30 eff

		if (GetSeqGridColNofText(SeqGridKeyword[31][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[31][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[31][0]) ][SeqRowNo] = "Qi"; // 31 qi

		if (GetSeqGridColNofText(SeqGridKeyword[32][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[32][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[32][0]) ][SeqRowNo] = "Coil1";  // 32 coil 1

		if (GetSeqGridColNofText(SeqGridKeyword[33][0]) < 0)
		{
			ShowMessage("KeyWord MISSING -> " + SeqGridKeyword[33][0]);
		}
		else
		{
			// Do Nothing
		} // End If
		StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[33][0]) ][SeqRowNo] = "Coil2";  // 33  coil 2


	// For Display Limit is 100 x 100 data points
	//
/*
	TestDataType format [Type as below][x (ie col)][(y ie Row)]
	0 Eff
	1 Sig
	2 Vin
	3 Iin
	4 Pin
	5 Vout
	6 Iout
	7 Pout

Data LCR
8 LCRFreq1
9 LCRFreqVal1
10 LCRFreqVal2

11 LCRFreq2
12 LCRFreqVal1
13 LCRFreqVal2

14 LCRFreq3
15 LCRFreqVal1
16 LCRFreqVal2

17 LCRFreq4
18 LCRFreqVal1
19 LCRFreqVal2

20 LCRFreq5
21 LCRFreqVal1
22 LCRFreqVal2

23 LCRFreq6
24 LCRFreqVal1
25 LCRFreqVal2

26 LCRFreq7
27 LCRFreqVal1
28 LCRFreqVal2

29 LCRFreq8
30 LCRFreqVal1
31 LCRFreqVal2

32 LCRFreq9
33 LCRFreqVal1
34 LCRFreqVal2

35 LCRFreq10
36 LCRFreqVal1
37 LCRFreqVal2

38 TiLDC_L1
39 TiLDC_L2
40 TiLDC_L3
41 TiLDC_L4

42 TiLDC_L1freq
43 TiLDC_L2freq
44 TiLDC_L3freq
45 TiLDC_L4freq

*/

	TestDataType[0][(ColPoint - 1)][(RowPoint - 1)] = FloatToStr(curMeas.efficiency); // Eff
	TestDataType[1][(ColPoint - 1)][(RowPoint - 1)] = "Qi";  //Qi
	TestDataType[2][(ColPoint - 1)][(RowPoint - 1)] = FloatToStr(curMeas.values[INPUT_VOLTAGE_INDEX]); //24 vin
	TestDataType[3][(ColPoint - 1)][(RowPoint - 1)] = FloatToStr(curMeas.values[INPUT_CURRENT_INDEX]);  //25 iin
	TestDataType[4][(ColPoint - 1)][(RowPoint - 1)] = FloatToStr(curMeas.inputPower); // 26 pin
	TestDataType[5][(ColPoint - 1)][(RowPoint - 1)] = FloatToStr(curMeas.values[OUTPUT_VOLTAGE_INDEX]);  // 27 vout
	TestDataType[6][(ColPoint - 1)][(RowPoint - 1)] = FloatToStr(curMeas.values[OUTPUT_CURRENT_INDEX]); // 28 iout
	TestDataType[7][(ColPoint - 1)][(RowPoint - 1)] = FloatToStr(curMeas.outputPower);  // 29 pout


	//*************************************
	// GW LCR MeterRead All Freqs
	//*************************************
	if (GetSeqGridColNofText(SeqGridKeyword[36][0]) < 0)
	{
	   // Do Nothing
	}
	else
	{

		MiscLCRFreqVal1Val2Result[1][0][0] = edTestFreqHz1of10->Text;
		MiscLCRFreqVal1Val2Result[2][0][0] = edTestFreqHz2of10->Text;
		MiscLCRFreqVal1Val2Result[3][0][0] = edTestFreqHz3of10->Text;
		MiscLCRFreqVal1Val2Result[4][0][0] = edTestFreqHz4of10->Text;
		MiscLCRFreqVal1Val2Result[5][0][0] = edTestFreqHz5of10->Text;
		MiscLCRFreqVal1Val2Result[6][0][0] = edTestFreqHz6of10->Text;
		MiscLCRFreqVal1Val2Result[7][0][0] = edTestFreqHz7of10->Text;
		MiscLCRFreqVal1Val2Result[8][0][0] = edTestFreqHz8of10->Text;
		MiscLCRFreqVal1Val2Result[9][0][0] = edTestFreqHz9of10->Text;
		MiscLCRFreqVal1Val2Result[10][0][0] = edTestFreqHz10of10->Text;


		for (int i = 1; i < 11; i++)
		{
			// Misc results
			int indexStart = (3 * i + 5);

			// Freq 1
			if (MiscLCRFreqVal1Val2Result[i][0][0] == "")
			{
				TestDataType[indexStart][(ColPoint - 1)][(RowPoint - 1)] = "";  //
				TestDataType[indexStart + 1][(ColPoint - 1)][(RowPoint - 1)] = "";  //
				TestDataType[indexStart + 2][(ColPoint - 1)][(RowPoint - 1)] = "";  //

				StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[(52 + (i-1))][0]) ][SeqRowNo] = "";  //
				StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[(62 + (i-1))][0]) ][SeqRowNo] = "";  //
				StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[(72 + (i-1))][0]) ][SeqRowNo] = "";  //
			}
			else
			{
				// Data
				// Index data

				TestDataType[indexStart][(ColPoint - 1)][(RowPoint - 1)] = MiscLCRFreqVal1Val2Result[i][0][0];  //
				//AnsiString test1 = MiscLCRFreqVal1Val2Result[i][0][0];
				TestDataType[indexStart + 1][(ColPoint - 1)][(RowPoint - 1)] = MiscLCRFreqVal1Val2Result[i][i][0];  //
				//AnsiString test2 = MiscLCRFreqVal1Val2Result[i][i][0];
				TestDataType[indexStart + 2][(ColPoint - 1)][(RowPoint - 1)] = MiscLCRFreqVal1Val2Result[i][i][i];  //
				//AnsiString test3 = MiscLCRFreqVal1Val2Result[i][i][i];

				StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[(52 + (i-1))][0]) ][SeqRowNo] = MiscLCRFreqVal1Val2Result[i][0][0];  //
				StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[(62 + (i-1))][0]) ][SeqRowNo] = MiscLCRFreqVal1Val2Result[i][i][0];  //
				StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[(72 + (i-1))][0]) ][SeqRowNo] = MiscLCRFreqVal1Val2Result[i][i][i];  //

			} // End If

		}  // End For

	} // End If


	//*************************************
	// Ends GW LCR MeterRead All Freqs
	//*************************************


	//******
	// Ti LCD EVM
	//******

	if (GetSeqGridColNofText(SeqGridKeyword[82][0]) < 0)
	{
	   // Not Set??
	}
	else
	{
		//AnsiString shds = SeqGridKeyword[82][0];
		//AnsiString sds = UpperCase(GetSeqGridColNofText(SeqGridKeyword[82][SeqRowNo]));
		if ((UpperCase(	StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[82][0]) ][SeqRowNo] ) == "ON") && cbTiEvmLDC_on_Off->Checked)
		{

		  // AnsiString TiEVMData = ReadCSV(edTiLCDEvmFileAddress->Text, true);

		/*   //ShowMessage(xx);
		   ShowMessage("L1 = " + SplitStringFrom0(TiEVMData,3,','));
		   ShowMessage("L2 = " + SplitStringFrom0(TiEVMData,4,','));
		   ShowMessage("L3 = " + SplitStringFrom0(TiEVMData,5,','));
		   ShowMessage("L4 = " + SplitStringFrom0(TiEVMData,6,','));

		   ShowMessage("L1 freq = " + SplitStringFrom0(TiEVMData,11,','));
		   ShowMessage("L2 freq = " + SplitStringFrom0(TiEVMData,12,','));
		   ShowMessage("L3 freq = " + SplitStringFrom0(TiEVMData,13,','));
		   ShowMessage("L4 freq = " + SplitStringFrom0(TiEVMData,14,','));
		*/

			TestDataType[38][(ColPoint - 1)][(RowPoint - 1)] = MiscTiEVM_LDCResult[1];
			TestDataType[39][(ColPoint - 1)][(RowPoint - 1)] = MiscTiEVM_LDCResult[2];
			TestDataType[40][(ColPoint - 1)][(RowPoint - 1)] = MiscTiEVM_LDCResult[3];
			TestDataType[41][(ColPoint - 1)][(RowPoint - 1)] = MiscTiEVM_LDCResult[4];
			TestDataType[42][(ColPoint - 1)][(RowPoint - 1)] = MiscTiEVM_LDCResult[5];
			TestDataType[43][(ColPoint - 1)][(RowPoint - 1)] = MiscTiEVM_LDCResult[6];
			TestDataType[44][(ColPoint - 1)][(RowPoint - 1)] = MiscTiEVM_LDCResult[7];
			TestDataType[45][(ColPoint - 1)][(RowPoint - 1)] = MiscTiEVM_LDCResult[8];

			//***
			// Put Data to Seq Grid
			//***
			for (int i = 1; i < 9; i++)
			{
				if (i > StrGridSeqTest->ColCount)
				{
					// Error
					lbControlMessage->Caption = "ERROR: TiEVM data exceeds Column Count";
				}
				else
				{
					StrGridSeqTest->Cells[ GetSeqGridColNofText(SeqGridKeyword[82 + i][0]) ][SeqRowNo] = MiscTiEVM_LDCResult[i];  //
				}  // End If

			} // End For
			//***
			// Ends Put Data to Seq GRid
			//***

		}
		else
		{
			// Do Nothing

		} // End If


	} // End If

	//******
	// Ends Ti LCD EVM
	//******


	/*
	Eff [(ColPoint - 1)][(RowPoint - 1)] = FloatToStr(curMeas.efficiency); // Eff
	Sig [(ColPoint - 1)][(RowPoint - 1)] =  "Qi";  //Qi

	Vin[(ColPoint - 1)][(RowPoint - 1)] = FloatToStr(curMeas.values[INPUT_VOLTAGE_INDEX]); //24 vin
	Iin [(ColPoint - 1)][(RowPoint - 1)] =  FloatToStr(curMeas.values[INPUT_CURRENT_INDEX]);  //25 iin
	Pin [(ColPoint) - 1][(RowPoint) - 1] = FloatToStr(curMeas.inputPower); // 26 pin
	Vout [(ColPoint) - 1][(RowPoint) - 1] = FloatToStr(curMeas.values[OUTPUT_VOLTAGE_INDEX]);  // 27 vout
	Iout [(ColPoint) - 1][(RowPoint) - 1] = FloatToStr(curMeas.values[OUTPUT_CURRENT_INDEX]); // 28 iout
	Pout [(ColPoint) - 1][(RowPoint) - 1] = FloatToStr(curMeas.outputPower);  // 29 pout

   */
/*
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = IntToStr(gblTotalPlotStepCount);
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = IntToStr(ColPoint - 1); // Less one as Starts at 1 but display as point 0
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = IntToStr(RowPoint -1); // Less one as Starts at 1 but display as point 0
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = IntToStr(zHeight);
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = "Rot";
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = "Ang";
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = FloatToStr(curMeas.values[INPUT_VOLTAGE_INDEX]);
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = FloatToStr(curMeas.values[INPUT_CURRENT_INDEX]);
		ColNumber_Results_text = ColNumber_Results_text +1;

		StrGridSeqTest->Cells[ColNumberResults_text][SeqRowNo] = FloatToStr(curMeas.inputPower);
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = FloatToStr(curMeas.values[OUTPUT_VOLTAGE_INDEX]);
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = FloatToStr(curMeas.values[OUTPUT_CURRENT_INDEX]);
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = FloatToStr(curMeas.outputPower);
		ColNumber_Results_text = ColNumber_Results_text +1;

		// Option for Efficiency in 2 decimal places
//		char effStr[15];
//		sprintf(&effStr[0],"%.2f %",curMeas.efficiency);
//		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = &effStr[0];

		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = FloatToStr(curMeas.efficiency);
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = "Qi";
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = "Coil1";
		ColNumber_Results_text = ColNumber_Results_text +1;
		StrGridSeqTest->Cells[ColNumber_Results_text][SeqRowNo] = "Coil2";
		ColNumber_Results_text = ColNumber_Results_text +1;

*/


	//*	StrGridSeqTest->Cells[17][0] = "";

		//display results to user

		//lbInputPower->Caption = FloatToStr(curMeas.inputPower);
		//float rounded = ((int)(curMeas.inputPower * 100 + .5) / 100.0);
		//lbInputPower->Caption = FloatToStr(rounded);

		//lbOutputPower->Caption = FloatToStr(curMeas.outputPower);
		//lbEff->Caption = FloatToStr(curMeas.efficiency);

		/*
		done in efficiency sub

		char inputPowerStr[15];
		char outputPowerStr[15];
		char effStr[15];

		//build input power string
		sprintf(&inputPowerStr[0],"%.2f W",curMeas.inputPower);
		lbInputPower->Caption = &inputPowerStr[0];

		//build output power string
		sprintf(&outputPowerStr[0],"%.2f W",curMeas.outputPower);
		lbOutputPower->Caption = &outputPowerStr[0];

		//build efficiency string
		sprintf(&effStr[0],"%.2f %",curMeas.efficiency);
		lbEff->Caption = &effStr[0];

		//lbEffCalcManual->Caption = &effStr[0];
*/
}
catch (...)
{
	ShowMessage("Error in Sub WriteResults2SeqGrid");
} // End Try

}

//---------------------------------------------------------------------------
int __fastcall TmainForm::GetGridTypeDropDownIndex(AnsiString DropDownLookUpString)
{
//        cbGridTypeDisplay

//AnsiString sx = cbGridTypeDisplay->Text;
//int ssx = cbGridTypeDisplay->ItemIndex;


	for (int i = 0; i < 12; i++)
	{
		cbGridTypeDisplay->ItemIndex = i;
		AnsiString DropText = cbGridTypeDisplay->Text;
		if (DropText == DropDownLookUpString)
		{
		   return i;
		}
		else
		{
			// Do Nothing
		}  // End If

	} // End For

	return -1;

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::UpdateDataGrid(int DataDropTypeNumb)
{
int GridTotalRow_y;
int GridTotalCol_x;

// NOTE: Need to redo Grid co-ords as 0,0 at bottom Left NOT top left

GridTotalRow_y = StringGrid1->RowCount;
GridTotalCol_x = StringGrid1->ColCount;

	for (int Coli = 0; Coli < StringGrid1->ColCount; Coli++)
	{
		for (int Rowi = 0; Rowi < StringGrid1->RowCount - 1; Rowi++)
		{
			// Check Data type
			if (DataDropTypeNumb < 8)
			{
				// Normal Eff Plotter
				StringGrid1->Cells[Coli + 1][GridTotalRow_y - Rowi - 1] = TestDataType[DataDropTypeNumb][Coli][Rowi];
			}
			else
			{

/*

			  // Drop Down Misc Items

			  //----------------
			  // Check if drop down exists for GW LCR Meter
			  //----------------
			  bool foundText = false;
			  int GWLCRDataNumber;
			  for (int GWLCRDataNumber= 1; GWLCRDataNumber < 11; GWLCRDataNumber++)
			  {
				if (cbGridTypeDisplay->Text == "Freq " + IntToStr(GWLCRDataNumber) + " : Data 1")
				{
					foundText = true;
					break;
				}
				else
				{
					// Do Nothing
				} // End If

			  }  // End For

			 if (foundText)
			 {
				  // OK
*/
				//----------------
				// LCR Meter
				//----------------

				// Extra data delected
				/*
				Calculate data from drop down index

				Data 1
				Count       1  2   3   4  etc
				Drop Down   8  9   10  11 etc
				Data1 array 9  12  15

				Formula for data 1 is:
				drop No + (([Drop No -7] * 2) -1)

				Formula for data 2 is (offset by 10 in drop down less -1:
				(drop No - 10) + ([(Drop No - 10) -7] * 2)

				*/

				// LCR Data 1
/*			   int TestDataTypeIndex = (7 + GWLCRDataNumber) + (((GWLCRDataNumber) * 2) - 1)  ;
			   StringGrid1->Cells[Coli + 1][GridTotalRow_y - Rowi - 1] = TestDataType[TestDataTypeIndex][Coli][Rowi];
*/
//				if ((DataDropTypeNumb > 7) && (DataDropTypeNumb < 18))
//				{
//				   int TestDataTypeIndex = DataDropTypeNumb + (((DataDropTypeNumb - 7) * 2) - 1)  ;
//				   StringGrid1->Cells[Coli + 1][GridTotalRow_y - Rowi - 1] = TestDataType[TestDataTypeIndex][Coli][Rowi];
//				   AnsiString test1 = TestDataType[TestDataTypeIndex][Coli][Rowi];
/*
				   AnsiString testx1 = TestDataType[8][0][0];
				   AnsiString testy1 = TestDataType[9][0][0];
				   AnsiString testz1 = TestDataType[10][0][0];
*/
//				}
//				else
//				{
				  // Do Nothing
//				} // End If
/*
			 }
			 else
			 {
				// Do Nothing
			 } // End If

				// LCR Data 2
				if ((DataDropTypeNumb > 17) && (DataDropTypeNumb < 28))
				{
				   int TestDataTypeIndex = (DataDropTypeNumb - 10) + (((DataDropTypeNumb - 10) - 7) * 2)  ;
				   StringGrid1->Cells[Coli + 1][GridTotalRow_y - Rowi - 1] = TestDataType[TestDataTypeIndex][Coli][Rowi];
				}
				else
				{
				  // Do Nothing
				} // End If

				//----------------
				// Ends LCR Meter
				//----------------

*/

				//----------------
				// GW LCR Meter
				//----------------
				WriteOneGridPointGWLCR(GridTotalRow_y, Coli, Rowi);

				//----------------
				// TiLDC EVM Meter
				//----------------
				WriteOneGridPointTiLDC(GridTotalRow_y, Coli, Rowi);

			} // End If

		} // End For

	}  // End For


}

//---------------------------------------------------------------------------
void __fastcall TmainForm::WriteOneGridPointGWLCR(int GridTotalRow_y, int Coli, int Rowi)
{

			//----------------
			// LCR Meter
			//----------------

			// Extra data delected
			/*
			Calculate data from drop down index

			Data 1
			Count       1  2   3   4  etc
			Drop Down   ?  ?   ?  ? etc
			Data1 array 9  12  15
			Data2 array 10 13  16

			Formula for data 1 is:
			//drop No + (([Drop No -7] * 2) -1)
			(3 * Count) + 6

			Formula for data 2 is (offset by 10 in drop down less -1:
			//(drop No - 10) + ([(Drop No - 10) -7] * 2)
			(3 * Count) + 7

			*/

			   //----------------
			   // Check if drop down exists for Ti LDC
			   //----------------
			  //bool foundText = false;
			  int foundData = 0; //o = none, 1 = data 1 , 2 = data2
			  int DataNumber;
			  int TestDataTypeIndex2;

			  for (int DataNumber = 1; DataNumber < 11; DataNumber++)
			  {
				if (cbGridTypeDisplay->Text == "Freq " + IntToStr(DataNumber) + " : Data 1")
				{
					foundData = 1;
//				   int TestDataTypeIndex = cbGridTypeDisplay->ItemIndex + (((cbGridTypeDisplay->ItemIndex - 7) * 2) - 1)  ;
//				   StringGrid1->Cells[ColPoint][RowGridPoint] = TestDataType[TestDataTypeIndex][ColPoint-1][RowPoint-1];

				   TestDataTypeIndex2 = (3 * DataNumber) + 6;
				   StringGrid1->Cells[Coli + 1][GridTotalRow_y - Rowi - 1] = TestDataType[TestDataTypeIndex2][Coli][Rowi];
					break;
				}
				else
				{
					if (cbGridTypeDisplay->Text == "Freq " + IntToStr(DataNumber) + " : Data 2")
					{
						foundData = 2;
//						 int TestDataTypeIndex =
//						   StringGrid1->Cells[ColPoint][RowGridPoint] = TestDataType[TestDataTypeIndex][ColPoint-1][RowPoint-1];

					   TestDataTypeIndex2 = (3 * DataNumber) + 7;
					   StringGrid1->Cells[Coli + 1][GridTotalRow_y - Rowi - 1] = TestDataType[TestDataTypeIndex2][Coli][Rowi];

						break;
					}
					else
					{
						// Do Nothing
					} // End If
				} // End If

			  }  // End For


/*
			// GW LCR Meter NOT GENERIC Index range so FIX LATER ****
			// Data 1
			if (( cbGridTypeDisplay->ItemIndex > 7 ) && (cbGridTypeDisplay->ItemIndex < 18))
			{
				// GW LCR meter or misc data
				//AnsiString test = TestDataType[cbGridTypeDisplay->ItemIndex][ColPoint-1][RowPoint-1];
			   int TestDataTypeIndex = cbGridTypeDisplay->ItemIndex + (((cbGridTypeDisplay->ItemIndex - 7) * 2) - 1)  ;
			   StringGrid1->Cells[ColPoint][RowGridPoint] = TestDataType[TestDataTypeIndex][ColPoint-1][RowPoint-1];
			   //AnsiString test1 = TestDataType[TestDataTypeIndex][ColPoint-1][RowPoint-1];
//			   StringGrid1->Cells[ColPoint][RowGridPoint] = TestDataType[cbGridTypeDisplay->ItemIndex][ColPoint-1][RowPoint-1];
			}
			else
			{
				// Do Nothing
			} // End If

			// Data 2
			if (( cbGridTypeDisplay->ItemIndex > 17 ) && (cbGridTypeDisplay->ItemIndex < 28))
			{
			   int TestDataTypeIndex = (cbGridTypeDisplay->ItemIndex - 10) + (((cbGridTypeDisplay->ItemIndex - 10) - 7) * 2)  ;
			   StringGrid1->Cells[ColPoint][RowGridPoint] = TestDataType[TestDataTypeIndex][ColPoint-1][RowPoint-1];
			}
			else
			{
				// Do Nothing
			} // End If
*/

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::WriteOneGridPointTiLDC(int GridTotalRow_y, int Coli, int Rowi)
{

			   //----------------
			   // Check if drop down exists for Ti LDC
			   //----------------
			  //bool foundText = false;
			  int foundData = 0; //o = none, 1 = data 1 , 2 = data2
			  int DataNumber;
			  int TestDataTypeIndex2;

			  for (int DataNumber = 1; DataNumber < 5; DataNumber++)
			  {
				if (cbGridTypeDisplay->Text == "TiEVM L" + IntToStr(DataNumber))
				{
					foundData = 1;
				   TestDataTypeIndex2 = (37) + DataNumber;
				   StringGrid1->Cells[Coli + 1][GridTotalRow_y - Rowi - 1] = TestDataType[TestDataTypeIndex2][Coli][Rowi];
					break;
				}
				else
				{
					if (cbGridTypeDisplay->Text == "TiEVM Lfreq" + IntToStr(DataNumber))
					{
						foundData = 2;
					   TestDataTypeIndex2 = (41) + DataNumber;
					   StringGrid1->Cells[Coli + 1][GridTotalRow_y - Rowi - 1] = TestDataType[TestDataTypeIndex2][Coli][Rowi];

						break;
					}
					else
					{
						// Do Nothing
					} // End If
				} // End If

			  }  // End For


			//----------------
				// Ti LDC Meter
				//----------------
				// Ti EVM Data L

//				if ((DataDropTypeNumb > 27) && (DataDropTypeNumb < 39))
//				{
//				   int TestDataTypeIndex = (DataDropTypeNumb - 10) + (((DataDropTypeNumb - 10) - 7) * 2)  ;
//				   int TestDataTypeIndex = (DataDropTypeNumb - 10) + (((DataDropTypeNumb - 10) - 7) * 2)  ;
//				   StringGrid1->Cells[Coli + 1][GridTotalRow_y - Rowi - 1] = TestDataType[TestDataTypeIndex][Coli][Rowi];
//				}
//				else
//				{
				  // Do Nothing
//				} // End If

				//----------------
				// Ends Ti LDC Meter
				//----------------

//			 }
//			 else
//			 {
				// Do Nothing
//			 } // End If



}

//---------------------------------------------------------------------------
void __fastcall TmainForm::WriteResults2Grid(int ColPoint, int RowPoint, int RowGridPoint, int zHeight,  int SeqPoint)
{
	MAIN_measurement_t meas;

		// Results Grid located in sub [InitDataResultsGrid]
		StringGrid3->Cells[0][1] = "";
		StringGrid3->Cells[1][1] = IntToStr(SeqPoint);
		StringGrid3->Cells[2][1] = IntToStr(ColPoint);
		StringGrid3->Cells[3][1] = IntToStr(RowPoint);
		StringGrid3->Cells[4][1] = IntToStr(zHeight);
		StringGrid3->Cells[5][1] = "Rot";
		StringGrid3->Cells[6][1] = "Ang";
		StringGrid3->Cells[7][1] = FloatToStr((float)meas.values[INPUT_VOLTAGE_INDEX]);
		StringGrid3->Cells[8][1] = FloatToStr((float)meas.values[INPUT_CURRENT_INDEX]);
		StringGrid3->Cells[9][1] = FloatToStr((float)meas.inputPower);
		StringGrid3->Cells[10][1] = FloatToStr((float)meas.values[OUTPUT_VOLTAGE_INDEX]);
		StringGrid3->Cells[11][1] = FloatToStr((float)meas.values[OUTPUT_CURRENT_INDEX]);
		StringGrid3->Cells[12][1] = FloatToStr((float)meas.outputPower);
		StringGrid3->Cells[13][1] = FloatToStr((float)meas.efficiency);
		StringGrid3->Cells[14][1] = "Qi";
		StringGrid3->Cells[15][1] = "Coil1";
		StringGrid3->Cells[16][1] = "Coil2";
	//*	StringGrid3->Cells[17][0] = "";



}


//---------------------------------------------------------------------------
void __fastcall TmainForm::MovePlotterToPointMAIN(int ColPoint, int RowPoint, int RowGridPoint, float zHeight,  int SeqPoint)
{
		// Current Grid Index = iRow, ColNumber
		// Current Plotter Point = RowNumber, ColNumber

		// SIMULATION
		// Current Grid Index = iRow, ColNumber
		// Seq Grid Index Row number = SeqRowRunCounter

		// Step 1
		ColourGridCellGrn(ColPoint, RowGridPoint, 1);
		// Create mm co-ord ref to grid point location
		AnsiString xCoord = FloatToStr(StrToFloat(ebXSetpointStart2->Text) + ((ColPoint - 1) * StrToFloat(ebStepSize2->Text)));
		AnsiString yCoord = FloatToStr(StrToFloat(ebYSetpointStart2->Text) + ((RowPoint - 1) * StrToFloat(ebStepSize2->Text)));
		AnsiString ZHeight = FloatToStr(zHeight); //ed3DZHeight->Text;
		AnsiString Speed = "3000";
		// Move Plotter
		if (cbSimPlotting->Checked)
		{
			MessagesGlobal(2);
		}
		else
		{
			MovePlotterToPoint(xCoord, yCoord, ZHeight, Speed);

			// Allow Time to go from end to start of test
			if ((ColPoint == 1) && (RowPoint == 1))
			{
				// For second Time Plot, Allow Time to go to Start from End of Plot
				// ShowMessage("Moving to Start of Plot");

				if (SeqTestRunning)
				{
					// Allow time for Plotter to Home
				   lbCountDownState->Caption = "Wait Countdown: Go Start";
				   // Allow time to get there


				   TimerSequential(5000);


				   lbCountDownState->Caption = "Wait Countdown: Ready";

				}
				else
				{
					// Do Nothing
				} // End If

			}
			else
			{
				// Do Nothing

			} // End If

		}

//		ShowMessage("Done");

}



//---------------------------------------------------------------------------
void __fastcall TmainForm::MovePlotterToPoint(AnsiString xpoint, AnsiString ypoint, AnsiString zpoint, AnsiString speed)
{
		if (PlotterON())
		{
			AnsiString PlotCmd = "G1 X" + xpoint + "Y" + ypoint + "Z" + zpoint + "F" + speed;
			Plotter->SendCommand(PlotCmd);
		}
		else
		{
			MessagesGlobal(1);
			//lblProgramStateIndicator->Caption = "Plotter Not Open or Homed. You can Run Simulation set in Maintenance Tab where ALL Outputs Disabled for Demo";
			//lbStatusGridSeq->Caption = "Plotter Not Open or Homed\nYou can Run Simulation set in Maintenance Tab where ALL Outputs Disabled for Demo";
//			lbControlMessage->Caption = "Plotter Not Open or Homed!\nYou can Run Simulation set in Maintenance Tab for Demo";
		} // End If

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::TimerSequential(int timedelay)
{
	   // This uses tmSeqTest2 Timer

		CountDownProgressWait = timedelay/10;
		while (CountDownProgressWait > 0 )
		{
		   //Sleep(10);
		   if ((!(tmSeqTest2->Enabled)) || !(SeqTestRunning))
		   {
			break;
		   }
		   Application->ProcessMessages();
		} //End While

}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
void __fastcall TmainForm::SaveGridData2CSV(int GridRow, bool TitleOnly)
{
bool Found_Result_StartCol;
		//GridRow = -1 disable else Seq Grid row

			// ***
			// SAVE DATA
			// ***

			// Create Data Row String from Grid
			AnsiString TestResultData; // = StrGridSeqTest->Cells[12][SeqStepCounter] + "," + StrGridSeqTest->Cells[13][SeqStepCounter] + "," + StrGridSeqTest->Cells[14][SeqStepCounter] + "," + StrGridSeqTest->Cells[15][SeqStepCounter];
			TestResultData = "";

			if (TitleOnly)
			{
				Found_Result_StartCol = false;

				// Write Titles from Seq Grid
				for (int i = 1; i < StrGridSeqTest->ColCount; i++)
				{
					if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
					{

						TestResultData = TestResultData + StrGridSeqTest->Cells[i][GridRow] + ",";
/*(
						//***
						// Option for ignoring result
						//***
						if (Found_Result_StartCol)
						{
							// Blank results
							TestResultData = TestResultData + ",";
						}
						else
						{
						   // Save data
							TestResultData = TestResultData + StrGridSeqTest->Cells[i][GridRow] + ",";

							if (UpperCase(StrGridSeqTest->Cells[i][GridRow]) == "RESULT")
							{
								Found_Result_StartCol = true;
							}
							else
							{
								// Do Nothing
							} // End If

						}  // End If
						//***
						// Ends Option for ignoring result
						//***
*/

					}
					else
					{
						// Do Nothing
						ShowMessage("ALERT: No 'Sequence File' loaded in Sequence Grid!");
						TestResultData = "";
						break;
					} // End If

				} // End For

			}
			else
			{
				 // Save Data

				 Found_Result_StartCol = false;
				// Read data exclude Titles from Seq GRid
				for (int i = 1; i < StrGridSeqTest->ColCount; i++)
				{
					if (Found_Result_StartCol)
					{
					   // Save data
						TestResultData = TestResultData + StrGridSeqTest->Cells[i][GridRow] + ",";
					}
					else
					{
						// Save blank ie ignore title
						TestResultData = TestResultData + ",";

						if (UpperCase(StrGridSeqTest->Cells[i][GridRow]) == "RESULT")
						{
							// Found Result Start Col
							//TestResultData = TestResultData + StrGridSeqTest->Cells[i][GridRow] + ",";
							Found_Result_StartCol = true;
						}
						else
						{
							// Do Nothing
							//TestResultData = TestResultData + ",";
						} // End If

					}  // End If

				} // End For
		  } // End If

/*
				//temporary measurement storage
				MAIN_measurement_t tmpMeas;

				tmpMeas = curMeas;
				TestResultData = GenerateCSVLine2(tmpMeas);

*/

/*
				// Titles from Seq GRid
				for (int i = 1; i < StringGrid3->ColCount; i++)
				{
					TestResultData = TestResultData + StringGrid3->Cells[i][1] + ",";
				} // End For
*/

			  if (TestResultData == "")
			  {
					// Do Nothing
			  }
			  else
			  {
				// OK to save CSV

				//int test = GetSeqGridColNofText(SeqGridKeyword[96][0]);
				 if ((Arduino_FOD->Open) && (GetSeqGridColNofText(SeqGridKeyword[96][0]) > 0))
				 {

					if (GridRow == 0 )
					{
						// Write Title
						TestResultData = TestResultData + "," + AppendCSVFileFoDData(true) + "\n";
					}
					else
					{
						// Write Data
						TestResultData = TestResultData + "," + AppendCSVFileFoDData(false) + "\n";
					}
				 }
				 else
				 {
					TestResultData = TestResultData + "\n";
				 } // End If

				// Now save data
				if (WriteToCSVData(testAdmin.testFile, TestResultData, true))
				{
					//tmSeqTest2->Enabled = true;
					// CSV OK
				}
				else
				{
					tmSeqTest2->Enabled = false;
					ShowMessage("Error saving CSV file");
					tmSeqTest2->Enabled = true;
				} // End If

				// ***
				// ENDS SAVE DATA
				// ***

			  } // End If


}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
AnsiString __fastcall TmainForm::AppendCSVFileFoDData(bool PutTitles)
{

AnsiString TestResultData; // = StrGridSeqTest->Cells[12][SeqStepCounter] + "," + StrGridSeqTest->Cells[13][SeqStepCounter] + "," + StrGridSeqTest->Cells[14][SeqStepCounter] + "," + StrGridSeqTest->Cells[15][SeqStepCounter];
TestResultData = "";

//	int Offset_for_r;
//L     REMEMBER TO ALLOW MAX COILS of 20!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for (int coili = 1; coili < 9; coili++)
	{
		 for (int freqi = 1; freqi < 11; freqi++)
		 {

			 if (PutTitles)
			 {
				// Titles
				TestResultData = TestResultData + "," + "Coil" + IntToStr(coili) + " L_freq " + IntToStr(freqi);
//				TestResultData = TestResultData + "," + "Coil" + IntToStr(coili) + " r_freq " + IntToStr(freqi);
			 }
			 else
			 {
				// Data
//				 Offset_for_r = freqi + 10;
				 TestResultData = TestResultData + "," + FoDCoilResult[coili][freqi];
//				 TestResultData = TestResultData + "," + FoDCoilResult[coili][Offset_for_r];
			 }  // End If

		 }  // End For

	}  // End For

// r
	int Offset_for_r;
	for (int coilw = 1; coilw < 9; coilw++)
	{
		 for (int freqi = 1; freqi < 11; freqi++)
		 {

			 if (PutTitles)
			 {
				// Titles
//				TestResultData = TestResultData + "," + "Coil" + IntToStr(coili) + " L_freq " + IntToStr(freqi);
				TestResultData = TestResultData + "," + "Coil" + IntToStr(coilw) + " r_freq " + IntToStr(freqi);
			 }
			 else
			 {
				// Data
				 Offset_for_r = freqi + 10;
//				 TestResultData = TestResultData + "," + FoDCoilResult[coili][freqi];
				 TestResultData = TestResultData + "," + FoDCoilResult[coilw][Offset_for_r];
			 }  // End If

		 }  // End For

	}  // End For












		return TestResultData;

	// Now save data

/*				if (WriteToCSVData(testAdmin.testFile, TestResultData, true))
				{
					//tmSeqTest2->Enabled = true;
					// CSV OK
				}
				else
				{
					ShowMessage("Error saving CSV file");
				} // End If
*/
}


//---------------------------------------------------------------------------
void __fastcall TmainForm::Button9Click(TObject *Sender)
{
		Plotter->SendCommand(cbPlotterSendCmd->Text);
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void __fastcall TmainForm::MessagesGlobal(int MessNo)
{
			// Status bottom of Form
			//lblProgramStateIndicator->Caption = "Plotter Not Open or Homed. You can Run Simulation set in Maintenance Tab where ALL Outputs Disabled for Demo";

			// Status Ind Top right of Grid
			//lbStatusGridSeq->Caption = "Plotter Not Open or Homed\nYou can Run Simulation set in Maintenance Tab where ALL Outputs Disabled for Demo";

			// WARNING Message Top Left of Grid
			//lbControlMessage->Caption = "Plotter Not Open or Homed!\nYou can Run Simulation set in Maintenance Tab for Demo";


	switch (MessNo)
	{
		case 0:
		{
			//**
			// CLEAR ALL Messages
			//***
			// Status bottom of Form
			lblProgramStateIndicator->Caption = "";

			// Status Ind Top right of Grid
			lbStatusGridSeq->Caption = "";

			// WARNING Message Top Left of Grid
			lbControlMessage->Caption = "";
			break;
		}
		case 1:
		{
			//**
			// Plotter Not homed
			//***
			// WARNING Message Top Left of Grid
			lbControlMessage->Caption = "Plotter Not Open or Homed!\nYou can Run Simulation set in Maintenance Tab for Demo";
			break;
		}
		case 2:
		{
			//**
			// Plotter set in Sim
			//***
			// WARNING Message Top Left of Grid
			lbControlMessage->Caption = "Plotter Running in Simulation set in Maintenance Tab for Demo";
			break;
		}
		case 3:
		{
			//**
			// Plotter Not homed
			//***
			// WARNING Message Top Left of Grid
			lbControlMessage->Caption = "Plotter Runing in Simulation\nThis is set in Maintenance Tab for Demo";
			break;
		}
		case 4:
		{
			//**
			// Plotter Not homed
			//***
			// WARNING Message Top Left of Grid

			break;
		}
		case 5:
		{
			//**
			// Plotter Not homed
			//***
			// WARNING Message Top Left of Grid

			break;
		}
		case 6:
		{
			//**
			// Plotter Not homed
			//***
			// WARNING Message Top Left of Grid

			break;
		}
		case 7:
		{
			//**
			// Plotter Not homed
			//***
			// WARNING Message Top Left of Grid

			break;
		}
		case 8:
		{
			//**
			// Plotter Not homed
			//***
			// WARNING Message Top Left of Grid

			break;
		}

	default:
		break;
		;
	}  // End Switch

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::btnGotToPlotOffsetClick(TObject *Sender)
{

	if (MessageDlg("Yes = Got To Start\nNo = Go to End", mtInformation, mbYesNo, 0) == mrYes)
	 {
		//ShowMessage("Yes");
		int ColSteps = 1 + StrToInt(edXOffsetStart2->Text);
		int RowSteps = 1 + StrToInt(edyOffsetStart2->Text);

		MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(ed3DZHeight2->Text), 1);

	 }
	 else
	 {
		 //ShowMessage("No");
		int ColSteps = 1 + StrToInt(edXOffsetStart2->Text) + StrToInt(edXOffsetSteps2->Text);
		int RowSteps = 1 + StrToInt(edyOffsetStart2->Text) + StrToInt(edyOffsetSteps2->Text);

		MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(ed3DZHeight2->Text), 1);

	 } // End If

}

//---------------------------------------------------------------------------


void __fastcall TmainForm::StringGrid3DblClick(TObject *Sender)
{
   switch(MessageDlg("Yes = Show Seq Grid\nNo = Exit", mtInformation, mbYesNo, 0))
	{
		case mrYes:
		 {
			  StringGrid3->Visible = false;
			  StrGridSeqTest->Visible = true;
				break;
		 }
		default:
		 {
			break;
		 }
	} // End Switch

}

//---------------------------------------------------------------------------

void __fastcall TmainForm::cbDAQTypeChange(TObject *Sender)
{

shpDAQConnected->Brush->Color = clSilver;

switch (cbDAQType->ItemIndex)
		{
		case 0:  // USE_NI6009
			{
			//ShowMessage("dff");
			USBMM0 = new NI6009_USB_Interface(memoUSBLog);
//			sDQA_Address_Name = Reg_ReadString(sREG, "DAQ", "NI6009", "dev1");
//			USBMM0->SetDQA_Address_Name(sDQA_Address_Name);

		   //	shpDAQConnected->Brush->Color = clLime;
		   btOpenUSB->Enabled = true;

			// From GetAnalogInputs
			edVinDaqAddr->Text = "0";
			edIinDaqAddr->Text = "1";
			edVoutDaqAddr->Text = "2";
			edIoutDaqAddr->Text = "3";

			ShowMessage("NOTE: Remember to check Maintenance Tab DAQ Calibration Numbers");

			break;
			}
		case 1:  // USE_NI6210
			{
			USBMM1 = new NI6210_USB_Interface(memoUSBLog);
			sDQA_Address_Name = Reg_ReadString(sREG, "DAQ", "NI6210", "dev1");
			USBMM1->SetDQA_Address_Name(sDQA_Address_Name);

			// From GetAnalogInputs
			edVinDaqAddr->Text = "0";
			edIinDaqAddr->Text = "1";
			edVoutDaqAddr->Text = "2";
			edIoutDaqAddr->Text = "3";

		   btOpenUSB->Enabled = true;

		   //	shpDAQConnected->Brush->Color = clLime;
/*
			for(int curMeter = 0; curMeter < 4; curMeter++)
			{
				if(MeterConnUSBMM(curMeter))
				{
				  // Do Nothing
				}
				else
				{
					// If Any Meter NOT connected reset
					shpDAQConnected->Brush->Color = clSilver;
				} // End If
			} // End For
*/

			ShowMessage("NOTE: Remember to check Maintenance Tab DAQ Calibration Numbers");

			break;
			}
		case 2:  // USE_AGILENT
			{
			USBMM2 = new AGILENT_Interface(memoUSBLog);

		   btOpenUSB->Enabled = true;
			//shpDAQConnected->Brush->Color = clLime;

			// From GetAnalogInputs
			edVinDaqAddr->Text = "101";
			edIinDaqAddr->Text = "102";
			edVoutDaqAddr->Text = "103";
			edIoutDaqAddr->Text = "104";

			ShowMessage("NOTE: Remember to check Maintenance Tab DAQ Calibration Numbers");

			break;
			}
		case 3:  // USE_DMM
			{
			USBMM3 = new USBMM_Controller(memoUSBLog);
			//shpDAQConnected->Brush->Color = clLime;

		   btOpenUSB->Enabled = true;

			// From GetAnalogInputs
			edVinDaqAddr->Text = "";
			edIinDaqAddr->Text = "";
			edVoutDaqAddr->Text = "";
			edIoutDaqAddr->Text = "";

			ShowMessage("NOTE: Remember to check Maintenance Tab DAQ Calibration Numbers");

			break;
			}
		case 4:  // USE_MOCK_DMM
			{
			USBMM4 = new MOCK_Interface(memoUSBLog);
			//shpDAQConnected->Brush->Color = clLime;

		   btOpenUSB->Enabled = true;

			// From GetAnalogInputs
			edVinDaqAddr->Text = "";
			edIinDaqAddr->Text = "";
			edVoutDaqAddr->Text = "";
			edIoutDaqAddr->Text = "";

			ShowMessage("NOTE: Remember to check Maintenance Tab DAQ Calibration Numbers");

			break;
			}
		case 5:  // USE_PSU and eLoad DAQ
			{

				if ((cb_E_LoadDaQ->Checked) && (cbDAQType->ItemIndex == 5))
				{
					shpDAQConnected->Brush->Color = clLime;
				}
				else
				{
					shpDAQConnected->Brush->Color = clSilver;
				} // End If

				 if ((cbPSU_DaQ->Checked) && (cbDAQType->ItemIndex == 5))
				{
					shpDAQConnected->Brush->Color = clLime;
				}
				else
				{
					shpDAQConnected->Brush->Color = clSilver;
				} // End If

				ShowMessage("NOTE: Remember to check Maintenance Tab DAQ Calibration Numbers");

			break;
			}

		default:
			{
			  //	shpDAQConnected->Brush->Color = clSilver;
			}
			break;
	} // End Switch


/*

#ifdef USE_NI6009
	USBMM = new NI6009_USB_Interface(memoUSBLog);
#endif

#ifdef USE_NI6210
	USBMM = new NI6210_USB_Interface(memoUSBLog);
	sDQA_Address_Name = Reg_ReadString(sREG, "DAQ", "NI6210", "dev1");
	USBMM->SetDQA_Address_Name(sDQA_Address_Name);

#endif

#ifdef USE_AGILENT
	USBMM = new AGILENT_Interface(memoUSBLog);
#endif

#ifdef USE_DMM
	USBMM = new USBMM_Controller(memoUSBLog);
#endif

#ifdef USE_MOCK_DMM
	USBMM = new MOCK_Interface(memoUSBLog);
#endif
*/

}


//---------------------------------------------------------------------------
void __fastcall TmainForm::StopUSBMM()
{

	switch (cbDAQType->ItemIndex)
		{
		case 0:  // USE_NI6009
			{
			//ShowMessage("dff");
			USBMM0->Stop();
			shpDAQConnected->Brush->Color = clSilver;
			break;
			}
		case 1:  // USE_NI6210
			{
			USBMM1->Stop();
			shpDAQConnected->Brush->Color = clSilver;
			break;
			}
		case 2:  // USE_AGILENT
			{
			USBMM2->Stop();
			shpDAQConnected->Brush->Color = clSilver;
			break;
			}
		case 3:  // USE_DMM
			{
			USBMM3->Stop();
			shpDAQConnected->Brush->Color = clSilver;
			break;
			}
		case 4:  // USE_MOCK_DMM
			{
			USBMM4->Stop();
			shpDAQConnected->Brush->Color = clSilver;
			break;
			}
		default:
			break;
	} // End Switch

}

// ---------------------------------------------------------------------------

void __fastcall TmainForm::StartUSBMM()
{

	switch (cbDAQType->ItemIndex)
		{
		case 0:  // USE_NI6009
			{
			//ShowMessage("dff");
			USBMM0->Start();
			shpDAQConnected->Brush->Color = clLime;
			break;
			}
		case 1:  // USE_NI6210
			 {
			if (USBMM1->Start())
			{
//				ShowMessage("DAQ OK");
				shpDAQConnected->Brush->Color = clLime;
			}
			else
			{
//				ShowMessage("DAQ Not OK");
				shpDAQConnected->Brush->Color = clSilver;
			} // End If

			break;
			 }
		case 2:  // USE_AGILENT
			{
			USBMM2->Start();
			shpDAQConnected->Brush->Color = clLime;
			break;
			}
		case 3:  // USE_DMM
			{
			USBMM3->Start();
			shpDAQConnected->Brush->Color = clLime;
			break;
			}
		case 4:  // USE_MOCK_DMM
			{
			USBMM4->Start();
			shpDAQConnected->Brush->Color = clLime;
			break;
			}
		default:
			break;
	} // End Switch

}

// ---------------------------------------------------------------------------
void __fastcall TmainForm::StartMeasUSBMM()
{
	switch (cbDAQType->ItemIndex)
		{
		case 0:  // USE_NI6009
			{
				//ShowMessage("dff");
				USBMM0->StartNewMeasurement();
				break;
			}
		case 1:  // USE_NI6210
			{
				USBMM1->StartNewMeasurement();
				break;
			}
		case 2:  // USE_AGILENT
			{
				USBMM2->StartNewMeasurement();
				break;
			}
		case 3:  // USE_DMM
			{
				USBMM3->StartNewMeasurement();
				break;
			}
		case 4:  // USE_MOCK_DMM
			{
				USBMM4->StartNewMeasurement();
				break;
			}
		case 5:  // PSU and Eload DAQ
			{

				if (cbPSU_DaQ->Checked)
				{
				   Read_PSU_DAQ();
				   //Sleep(100);
				   //Read_PSU_DAQ();
				}
				else
				{
				   //Do Nothing
				} // End If

				if (cb_E_LoadDaQ->Checked)
				{
					Read_eLoad_DAQ();
					//Sleep(100);
					//Read_eLoad_DAQ();
				}
				else
				{
					// Do Nothing
				} // End If

				break;
			}
		default:
			break;
	} // End Switch

}

// ---------------------------------------------------------------------------
bool __fastcall TmainForm::DataAvaiUSBMM()
{
switch (cbDAQType->ItemIndex)
		{
		case 0:  // USE_NI6009
			{
			//ShowMessage("dff");
			if (USBMM0->DataAvailable())
			{
				return true;
			}
			else
			{
				return false;
			} // End If

			break;
			}
		case 1:  // USE_NI6210
			{

			if (USBMM1->DataAvailable())
			{
				return true;
			}
			else
			{
				return false;
			} // End If

			break;
			}
		case 2:  // USE_AGILENT
			{

			if (USBMM2->DataAvailable())
			{
				return true;
			}
			else
			{
				return false;
			} // End If

			break;
			}
		case 3:  // USE_DMM
			{

			if (USBMM3->DataAvailable())
			{
				return true;
			}
			else
			{
				return false;
			} // End If

			break;
			}
		case 4:  // USE_MOCK_DMM
			{

			if (USBMM4->DataAvailable())
			{
				return true;
			}
			else
			{
				return false;
			} // End If

			break;
			}
		case 5:  // USE_PSU ELoad DAQ
			{

			if (cbPSU_DaQ->Checked)
			{
				if (cb_E_LoadDaQ->Checked)
				{
					return true;
				}
				else
				{
					// Do Nothing
					return false;
				} // End If
			}
			else
			{
			   //Do Nothing
				return false;
			} // End If

			break;
			}
		default:

			break;
	} // End Switch

}


// ---------------------------------------------------------------------------
bool __fastcall TmainForm::MeterConnUSBMM(uint8_t curMeter)
{
switch (cbDAQType->ItemIndex)
		{
		case 0:  // USE_NI6009
			{
			//ShowMessage("dff");

			if (USBMM0->MeterConnected(curMeter))
			{
				return true;
			}
			else
			{
				return false;
			} // End If

			break;
			}
		case 1:  // USE_NI6210
			{

			if (USBMM1->MeterConnected(curMeter))
			{
				return true;
			}
			else
			{
				return false;
			} // End If

			break;
			}
		case 2:  // USE_AGILENT
			{

			if (USBMM2->MeterConnected(curMeter))
			{
				return true;
			}
			else
			{
				return false;
			} // End If

			break;
			}
		case 3:  // USE_DMM
			{

			if (USBMM3->MeterConnected(curMeter))
			{
				return true;
			}
			else
			{
				return false;
			} // End If

			break;
			}
		case 4:  // USE_MOCK_DMM
			{

			if (USBMM4->MeterConnected(curMeter))
			{
				return true;
			}
			else
			{
				return false;
			} // End If

			break;
			}
		case 5:  // USE_PSU and eLoad DAQ
			{
			return true;
			break;
			}
		default:
			break;
	} // End Switch

}


// ---------------------------------------------------------------------------
float __fastcall TmainForm::GetOutputUSBMM(uint8_t curMeter)
{
float ResultDAQ;

ResultDAQ = 0.0;

switch (cbDAQType->ItemIndex)
		{
		case 0:  // USE_NI6009
			{
			//ShowMessage("dff");
			ResultDAQ = USBMM0->GetOutput(curMeter);
			//break;
			}
		case 1:  // USE_NI6210
			{
			ResultDAQ = USBMM1->GetOutput(curMeter);
			break;
			}
		case 2:  // USE_AGILENT
			{
			ResultDAQ = USBMM2->GetOutput(curMeter);
			break;
			}
		case 3:  // USE_DMM
			{
			ResultDAQ = USBMM3->GetOutput(curMeter);
			break;
			}
		case 4:  // USE_MOCK_DMM
			{
			ResultDAQ = USBMM4->GetOutput(curMeter);
			break;
			}
		case 5:  // USE_PSU and ELoad DAQ
			{

				switch (curMeter)
				{
					case 0:
					{
						ResultDAQ = StrToFloat(edPSUDAQV->Text);
						break;
					}
					case 1:
					{
						ResultDAQ = StrToFloat(edPSUDAQI->Text);
						break;
					}
					case 2:
					{
						ResultDAQ = StrToFloat(edeLoadDAQV->Text);
						break;
					}
					case 3:
					{
						ResultDAQ = StrToFloat(edeLoadDAQI->Text);
						break;
					}
					default:
					{
						break;
					}

				} // End Switch







			break;
			}
		default:

			break;
	} // End Switch

	return ResultDAQ;
}


// ---------------------------------------------------------------------------
void __fastcall TmainForm::DMM_TaskUSBMM()
{
switch (cbDAQType->ItemIndex)
		{
		case 0:  // USE_NI6009
			{
			//ShowMessage("dff");
			USBMM0->DMM_Task();
			break;
			}
		case 1:  // USE_NI6210
			{
			USBMM1->DMM_Task();
			break;
			}
		case 2:  // USE_AGILENT
			{
			USBMM2->DMM_Task();
			break;
			}
		case 3:  // USE_DMM
			{
			//USBMM3->DMM_Task();
			break;
			}
		case 4:  // USE_MOCK_DMM
			{
			USBMM4->DMM_Task();
			break;
			}
		case 5:  // USE_PSU eload DAQ
			{
				// Do Nothing
			break;
			}		default:
			break;
	} // End Switch

}


// ---------------------------------------------------------------------------
void __fastcall TmainForm::DeleteUSBMM()
{
switch (cbDAQType->ItemIndex)
		{
		case 0:  // USE_NI6009
			{
			//ShowMessage("dff");
			delete USBMM0;
			break;
			}
		case 1:  // USE_NI6210
			{
			delete USBMM1;
			break;
			}
		case 2:  // USE_AGILENT
			{
			delete USBMM2;
			break;
			}
		case 3:  // USE_DMM
			{
			delete USBMM3;
			break;
			}
		case 4:  // USE_MOCK_DMM
			{
			delete USBMM4;
			break;
			}
		default:
			break;
	} // End Switch

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Button1Click(TObject *Sender)
{
	//Start USB Threads
	StartUSBMM();
	//USBMM->Start();
	//shpDAQConnected->Brush->Color = clLime;


}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
void __fastcall TmainForm::TestTimeRemaining(float timeperPlot)
{

int timeRemaining;

	//plus number of X points to go in current Y line
	//stepsRemaining += StrToInt(ebXSteps->Text) - testAdmin.xSteps;
	//multiply to find total time in seconds
	//int timeRemaining = 2 * stepsRemaining * ( ( MEASUREMENT_TIME + StrToInt(ebPowerUpWait->Text) + StrToInt(ebOffTime->Text) ) / 1000 );
	int TotalStepsRequired = (StrToInt(ebXSteps2->Text) + 1)  * (StrToInt(ebYSteps2->Text) + 1);

	if (TotalStepsRequired >= gblTotalPlotStepCount)
	{
	  // Do Nothing
	}
	else
	{
		TotalStepsRequired = gblTotalPlotStepCount;
	} // End If


  if (timeperPlot == -1)
  {

	timeRemaining = (TotalStepsRequired - gblTotalPlotStepCount) * ( ( MEASUREMENT_TIME + StrToInt(ebPowerUpWait2->Text) + StrToInt(ebOffTime2->Text) ) / 1000 );
	// Add Misc Test Time

	if (cbTiEvmLDC_on_Off->Checked)
	{
		timeRemaining = timeRemaining + (TotalStepsRequired - gblTotalPlotStepCount) * ((StrToInt(edTiEVM_EstTestTime->Text))/1000);
	}
	else
	{
		// Do Nothing
	} // End If


	// Add GW LCR
	if(Gen_Comport->Open)
	{
		timeRemaining =	timeRemaining + (TotalStepsRequired - gblTotalPlotStepCount) * ((StrToInt(edLCR_EstTestTime->Text))/1000);
	}
	else
	{

	} // End If


	// Add FoD LCR
	if(Arduino_FOD->Open)
	{
		timeRemaining =	timeRemaining + (TotalStepsRequired - gblTotalPlotStepCount) * ((StrToInt(edLCR_EstTestTime->Text) * 8)/1000);
	}
	else
	{

	} // End If

  }
  else
  {


	   timeRemaining = (TotalStepsRequired - gblTotalPlotStepCount) * timeperPlot;


  } // End If


	int tempSecs = timeRemaining % 60;
	int tempMins = ( ( timeRemaining - tempSecs ) / 60 ) % 60;
	int tempHours = ( timeRemaining - tempMins * 60 - tempSecs ) / 3600;

	lblTimeRemaining->Caption = "Time Remaining for Plot";
	timeRemainlb->Caption = IntToStr(tempHours) + ":" + IntToStr(tempMins) + ":" + IntToStr(tempSecs);

	//display results to user
//	lbInputPower->Caption = inputPowerStr;
//	lbOutputPower->Caption = outputPowerStr;
//	lbEff->Caption = effStr;



/*	if (cbShowProgChart->Checked)
	{
		Series1->Add(curMeas.efficiency);
	}
	else
	{
		// Do Nothing
	} // End If

*/

}


//---------------------------------------------------------------------------
void __fastcall TmainForm::SoftwarePCRequirements1Click(TObject *Sender)
{
	ShowMessage("This GUI also Requires Installation of:\n1. NI DAQ\n2. Aligent IO");

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::cbPSUOptionsChange(TObject *Sender)
{
	btnOpenPSUGWPort->Enabled = true;
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
int __fastcall TmainForm::GetSeqGridColNofText(AnsiString ColText2Find)
{
int ColNumber;

try
{
	if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
	{
		for (ColNumber = 1; ColNumber < StrGridSeqTest->ColCount + 1; ColNumber++)
		{
			// int aa = AnsiStrPos(ColText2Find, StrGridSeqTest->Cells[ColNumber][0]);

			AnsiString SeqGridTitle = StrGridSeqTest->Cells[ColNumber][0];

			int test = AnsiPos(ColText2Find, SeqGridTitle);  // returns 0 is not in string
			if (test == 0)
			{
				// NOT Found
				// Do Nothing
			}
			else
			{
				// Found
				return ColNumber;
				break;
			} // End If

		}  // End For

		// Not Found
		return -1;

	}
	else
	{
		ShowMessage("WARNING No Seq File Loaded!");
		return -1;
	} // End If
}
catch(...)
{

  ShowMessage("Error in sub GetSeqGridColNofText");
} // End Try

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::cbeLoadOptionsChange(TObject *Sender)
{

switch (cbeLoadOptions->ItemIndex)
		{
		case 0:  // res
			{
				rgLoad->Enabled = true;
				rgLoad->Visible =true;
			break;
			}
		case 1:  // USE_BK
			{
				rgLoad->Enabled = false;
				rgLoad->Visible = false;

				btne_Load->Enabled = true;
				cbELoadCommands->Clear();
				cbELoadCommands->Text = "Select Mode";
				cbELoadCommands->Items->Add("Set Remote Mode");
				cbELoadCommands->Items->Add("Set Local Mode");
				cbELoadCommands->Items->Add("Set CC Mode");
				cbELoadCommands->Items->Add("Set Res Mode");
				cbELoadCommands->Items->Add("Set CC Value");
				cbELoadCommands->Items->Add("Set Res Value");

				Button8->Enabled = true;
				Button7->Enabled = true;

				cbELoadCommands->Enabled = true;

			break;
			}
		case 2:  //
			{

			break;
			}
		case 3:  // USE_DMM
			{

			break;
			}
		default:
			break;
	} // End Switch


}

//---------------------------------------------------------------------------
void __fastcall TmainForm::edPSUSetVoltageDblClick(TObject *Sender)
{
	SetPSUVoltAndAmps();

}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void __fastcall TmainForm::SetPSUVoltAndAmps()
{

AnsiString Volts;
AnsiString Current;

	if (shpPSUConnected->Brush->Color == clSilver)
	{
		// PSU NOT selected
		// Do Nothing
	}
	else
	{

		switch (cbPSUOptions->ItemIndex)
		{
		case 0:  // Hantek
			{
				Volts = Float2NoDecimalString(StrToFloat(edPSUSetVoltage->Text) * 100);
				Current = Float2NoDecimalString(StrToFloat(edPSUAmps->Text) * 1000);
			}
			break;
		case 1:  // GW Instek
			{

			}
			break;
		case 2:  //  Keysight
			{

			}
			break;
		default:
			{

			}
			break;
		} // End Switch


	//	 AnsiString Current = edPSUAmps->Text;
	//	PSU_ComportGW->PutString("APPLY 19,3");
	//	PSU_ComportGW->PutString("\n");

		PSUGWSetVDCandAMPS(Volts, Current);
		//ShowMessage("Voltage and Current Set");

	} // End If


}

//---------------------------------------------------------------------------
void __fastcall TmainForm::edELoadValueDblClick(TObject *Sender)
{
	Set_eLoadValue();
}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Set_eLoadValue()
{

	switch (cbELoadCommands->ItemIndex)
	{
		case 0:  //
//			ShowMessage("dff");
			ShowMessage("Combo Box Not Selected CC or Res Mode");
//			eLoad_SetRemote();
			break;
		case 1:  //
//			eLoad_SetLocal();
			ShowMessage("Combo Box Not Selected CC or Res Mode");
			break;
		case 2:  //
//			eLoad_SetCCMode();

				eLoad_SetRemote();
				eLoad_SetCCMode();
				eLoad_SetCCValue(edELoadValue->Text);
//				ShowMessage("dONE");
			break;
		case 3:  //
			//;
//			eLoad_SetResMode();
				eLoad_SetRemote();
				eLoad_SetResMode();
				eLoad_SetResValue(edELoadValue->Text);

			break;
		case 4:  //
			{
				ShowMessage("Combo Box Not Selected CC or Res Mode");
				break;
			}
		case 5:  //
			{
				ShowMessage("Combo Box Not Selected CC or Res Mode");
				break;
			}
		default:
			break;
	} // End Switch

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::MiscLabelUpdate(void)
{

	lbMisc->Caption = "Misc";

	 // GW LCR
	if ( Gen_Comport->Open)
	{
		lbMisc->Caption = lbMisc->Caption + "_LCR";
	}
	else
	{
		// Do Nothing
	}  // End If

	// Ti
	if ( cbTiEvmLDC_on_Off->Checked )
	{
		lbMisc->Caption = lbMisc->Caption + "_Ti";
	}
	else
	{
		// Do Nothing
	}  // End If

	//Arduino FOD Coils
	if ( Arduino_FOD->Open)
	{
		lbMisc->Caption = lbMisc->Caption + "_FoD";
	}
	else
	{
		// Do Nothing
	}  // End If

}
//---------------------------------------------------------------------------
void __fastcall TmainForm::btnLCRGWClick(TObject *Sender)
{

		UserSelectComport(Gen_Comport,"Select GW PSU Comport", "PortPSU", StrToInt(edBaudGenSerial->Text));

		if( Gen_Comport == NULL)
		{
			// Error
			MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);


		}
		else
		{

				//Gen_Comport->Open;
				ShowMessage("Please allow some time for Instrument to connect");

				edMiscIntruComportNo->Text = IntToStr(Gen_Comport->ComNumber);
               MiscLabelUpdate();
/*
				if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
				{
					// Grid Open

				}
				else
				{

				} // End If


				// Test if Title Exists for GW LCR
				if (GetSeqGridColNofText(SeqGridKeyword[36][0]) < 0)
				{
					// No GW LCR titles exist
					// Do Nothing
					ShowMessage("WARNIG: No Titles exists for GW LCR in sequence File");
				}
				else
				{


				}  // End If
*/
				shpMISDataConnected->Brush->Color = clLime;

/*
				//Gen_Comport->Open;

				edMiscIntruComportNo->Text = IntToStr(Gen_Comport->ComNumber);

				Gen_Comport->PutString(edMiscInstruMessage->Text);
				Gen_Comport->PutString("\n");

				Gen_Comport->FlushInBuffer();
				Gen_Comport->FlushOutBuffer();
*/

//					cbRunPSU_OnOff->Enabled =true;
  //					btEnablePSU->Enabled = true;
	//				shpPSUConnected->Brush->Color = clGreen;

		}  // End If


/*
   int MeterComPortNo = UserSelectComport2("Select Misc Meter Com-Port", "PortMiscInstru", 115200);

	mainForm->edMiscIntruComportNo->Text = IntToStr(MeterComPortNo);

   OpenTxPort(MeterComPortNo);




	AnsiString MiscInstruData = GetTxVersion();
	mainForm->edMiscInstruMessage->Text = MiscInstruData;
	//	 ShowMessage("Version = " + xx);

	CloseTxPort();
*/




}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnReadSeqIniClick(TObject *Sender)
{

  ReadSeqIniFileData();
//  btnReadSeqIni
  //btnSaveSeqIni->Enabled = true;

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnSaveSeqIniClick(TObject *Sender)
{

	AnsiString test = meSeqIniFile->Lines->Strings[0];

	if (test == "[Keywords]")
	{

		switch(MessageDlg("Please select Option\nYes= SAVE\nNo = Save Default\nCancel = Exit", mtInformation, mbYesNoCancel, 0))
		 {
		case mrYes:
		 {
			SaveSeqIniFileData();
			break;
		 }
		case mrNo:
		 {
			SaveDEFAULTSeqIniFileData();
		    ReadSeqIniFileData();
			break;
		 }
		default:
		   // Do Nothing
			break;
		 }  // End Switch
	}
	else
	{
		//ShowMessage("No Ini file opened");
		switch(MessageDlg("No Ini File Loaded\nPlease select Option\nYes= SAVE DEFAULT\nNo = Exit", mtInformation, mbYesNo, 0))
		 {
		case mrYes:
		 {
			SaveDEFAULTSeqIniFileData();
		  ReadSeqIniFileData();

			break;
		 }
		case mrNo:
		 {
			// Do Nothing
			break;
		 }
		default:
		   // Do Nothing
			break;
		 }  // End Switch

	} // End If

}

//---------------------------------------------------------------------------


void __fastcall TmainForm::StrGridSeqTestSelectCell(TObject *Sender, int ACol, int ARow,
		  bool &CanSelect)
{
	//ShowMessage("Select Cell" + IntToStr(ACol));
}

//---------------------------------------------------------------------------

void __fastcall TmainForm::cbRunPSU_OnOffClick(TObject *Sender)
{
	if (cbRunPSU_OnOff->Checked)
	{
		cbRunPSU_OnOff->Caption = "PS on/off cycle";

	}
	else
	{
		cbRunPSU_OnOff->Caption = "PS on Always";
	} // End If
}

//---------------------------------------------------------------------------


void __fastcall TmainForm::cbGridTypeDisplayChange(TObject *Sender)
{


//int test = GetGridTypeDropDownIndex("Vin");

/*
 From [InitGridDataDisplayOption]
Index
0	cbGridTypeDisplay->AddItem("Efficiency", NULL);
1	cbGridTypeDisplay->AddItem("Sig Strength", NULL);
2	cbGridTypeDisplay->AddItem("Vin", NULL);
3	cbGridTypeDisplay->AddItem("Iin", NULL);
4	cbGridTypeDisplay->AddItem("Pin", NULL);
5	cbGridTypeDisplay->AddItem("Vout", NULL);
6	cbGridTypeDisplay->AddItem("Iout", NULL);
7	cbGridTypeDisplay->AddItem("Pout", NULL);

8  LCR Freq 1 Data 1
9  LCR Freq 2 Data 1
10 LCR Freq 3 Data 1
11 LCR Freq 4 Data 1
12 LCR Freq 5 Data 1
13 LCR Freq 6 Data 1
14 LCR Freq 7 Data 1
15 LCR Freq 8 Data 1
16 LCR Freq 9 Data 1
17 LCR Freq 10 Data 1

18  LCR Freq 1 Data 2
19  LCR Freq 2 Data 2
20 LCR Freq 3 Data 2
21 LCR Freq 4 Data 2
22 LCR Freq 5 Data 2
23 LCR Freq 6 Data 2
24 LCR Freq 7 Data 2
25 LCR Freq 8 Data 2
26 LCR Freq 9 Data 2
27 LCR Freq 10 Data 2

TestData format [Type as below][x (ie col)][(y ie Row)]
0 Eff
1 Sig
2 Vin
3 Iin
4 Pin
5 Vout
6 Iout
7 Pout
8 Eff
9 Sig
10 Vin
11 Iin
12 Pin
13 Vout
14 Iout
15 Pout
16 ?
17 ?
18 ?
19 ?


*/
	if (PlotOnSelectedPoint)
	{
		// Plotting on ONE Point ONLY
		cbGridTypeDisplay->ItemIndex = 0;
		ShowMessage("Only Efficiency Data Shown for Plotting on ONE Point\nNeed more money if you want more features!");
	}
	else
	{
		// Normal Plotting
		UpdateDataGrid(cbGridTypeDisplay->ItemIndex);
	} // End If


}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SerialSend(AnsiString msg2Send)
{

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Button10Click(TObject *Sender)
{
//		sdWriteSerial( TXComPort, TxBuffer2, Binary_Len );
//		sdWriteSerial(PSUComPort,"o0\n"); // Power off


				//Gen_Comport->Open;

				//edMiscIntruComportNo->Text = IntToStr(Gen_Comport->ComNumber);

//				Gen_Comport->PutString(edMiscInstruMessage->Text);
				Gen_Comport->PutString(cbMiscInstrumentCmds->Text);
				Gen_Comport->PutString("\n");

//				Gen_Comport->FlushInBuffer();
//				Gen_Comport->FlushOutBuffer();


}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Gen_ComportTriggerAvail(TObject *CP, WORD Count)
{

static char RxBuffer2[400];

//static int State = WAIT_SOM;
char input;
//static char LRC;
static int i;
static int Ri_Ptr;

AnsiString RX_String;

	while (Count != 0)
	{
//		if (Recived_Message == false)
//		{
			input = Gen_Comport->GetChar();

	  //		if ((input == '\0') || (input == '\n') || (input == '\r'))	//if STX found in data resync
			if ((input == '\n') || (input == '\r'))	//if STX found in data resync
			{

				for (i = 0; i < sizeof(RxBuffer2); i++)
				{
					RX_String = RX_String + RxBuffer2[i];
				}  // End For

				if ((RX_String == "") || (RX_String == "wait"))
				{
					// Do nothing
				}
				else
				{
					//***
					// Message Received
					//***

					//TestLogAdd("Plotter msg : " + RX_String);
					//ShowMessage("Plotter msg : " + RX_String);
					if (Memo1->Lines->Count > 50)
					{
						Memo1->Clear();
					}
					else
					{
						Memo1->Lines->Add("Received msg : " + RX_String);
					} // End If

					edMiscInstruMsg->Text = RX_String;

					//***
					// Ends Message Received
					//***

				} // End If

				// Clear Buffer
				Ri_Ptr = 0;
				memset(RxBuffer2, 0, sizeof(RxBuffer2));

				//edMiscInstruMsg->Text = RX_String;
			}
			else
			{
				RxBuffer2[Ri_Ptr++] = input;
			} // End If

			Count--;

//		} // end of (Recived_Message == false)
		Application->ProcessMessages();

	}  // End While

   //	ShowMessage(String(RxBuffer2[1]));

		//			Memo1->Lines->Add("Plotter msg : " + RX_String);

}
//---------------------------------------------------------------------------


void __fastcall TmainForm::Button11Click(TObject *Sender)
{
		if(Gen_Comport->Open)
		{
			Gen_Comport->FlushInBuffer();
			Gen_Comport->FlushOutBuffer();
			Gen_Comport->Open = false;

			shpMISDataConnected->Brush->Color = clSilver;
			flgRunGenComport = false;
			InitGridDataDisplayOption();

		   btnLCRGW->Enabled = false;
		   grMiscInstruments->Enabled = false;

		   edMiscIntruComportNo->Text = 0;

		}
		else
		{
			// Do Nothing
		} // End If


}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Gen_ComportPortClose(TObject *Sender)
{
	//ShowMessage("Port Closed");
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::cbMiscInstrumentCmdsDblClick(TObject *Sender)
{

	//edMiscInstruMessage->Text = cbMiscInstrumentCmds->Text;

}

//---------------------------------------------------------------------------

void __fastcall TmainForm::cbMiscInstrumentTypeChange(TObject *Sender)
{


	switch (cbMiscInstrumentType->ItemIndex)
		{
		case 0:  // GW lcr
			{
			//ShowMessage("dff");

			cbMiscInstrumentCmds->Clear();
			cbMiscInstrumentCmds->Items->Add(GWLCR_id);
			cbMiscInstrumentCmds->Items->Add(GWLCR_rst);
			cbMiscInstrumentCmds->Items->Add(GWLCR_hwoptions);

			cbMiscInstrumentCmds->Items->Add("---Cal--");
			cbMiscInstrumentCmds->Items->Add(GWLCR_runCal_oc);
			cbMiscInstrumentCmds->Items->Add(GWLCR_runCal_sc);
			cbMiscInstrumentCmds->Items->Add(GWLCR_getCalresult);

			cbMiscInstrumentCmds->Items->Add("---Meas Cmds--");
			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_test + "ac");
			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_test + "rdc");
			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_speed + "max");
			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_speed + "fast");
			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_speed + "med");
			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_lev + "200m");
			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_freq +  "1e3");
			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_cct + "ser");
			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_cct + "par");

			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_func + "l;r");
			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_func + "l;q");
			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_func + "c;r");

			cbMiscInstrumentCmds->Items->Add("---Trig Cmds--");
			cbMiscInstrumentCmds->Items->Add(GWLCR_meas_trig);
			cbMiscInstrumentCmds->Items->Add(GWLCR_trig);

			cbMiscInstrumentCmds->Items->Add("---Multi Cmds--");
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_set);

			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_loadFile + "test");
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_save);

			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_test + "1");
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_func + "lp");
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_func + "ls");
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_freq + "1e3");
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_lev + "200m");
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_speed + "max");
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_speed + "fast");
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_speed + "med");
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_speed + "slow");


			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_delay + "1.0m");
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_HiLim + "1.0e-3");
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_LoLim + "0.1e-3");

			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_run);
			cbMiscInstrumentCmds->Items->Add(GWLCR_mult_trig);

			cbMiscInstrumentCmds->Items->Add("---Graph Cmds--");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph);
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_func + "lp");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_func + "ls");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_func + "rp");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_func + "rs");

			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_sweep +  "freq");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_startFreq + "100e3");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_stopFreq + "300e3");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_level + "100m");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_speed + "fast");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_speed + "med");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_speed + "slow");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_step + "1");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_step + "2");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_step + "3");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_step + "4");

			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_trig);

			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_autoFit + "on");

			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_mkFreq + "1e3");
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_mkPeak);
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_mkVal);
			cbMiscInstrumentCmds->Items->Add(GWLCR_graph_mkValFreq);

			cbMiscInstrumentCmds->Text = "Command List";

		   grMiscInstruments->Enabled = true;
		   btnLCRGW->Enabled = true;

/*
			cbMiscInstrumentCmds->Text = "Command List";
			cbMiscInstrumentCmds->Clear();
			cbMiscInstrumentCmds->Items->Add("*idn?");
			cbMiscInstrumentCmds->Items->Add("*rst");
			cbMiscInstrumentCmds->Items->Add("*opt?");

			cbMiscInstrumentCmds->Items->Add("---Cal--");
			cbMiscInstrumentCmds->Items->Add(":cal:oc-trim 4");
			cbMiscInstrumentCmds->Items->Add(":cal:sc-trim 4");
			cbMiscInstrumentCmds->Items->Add(":cal:res?");

			cbMiscInstrumentCmds->Items->Add("---Meas Cmds--");
			cbMiscInstrumentCmds->Items->Add(":meas:test:ac");
			cbMiscInstrumentCmds->Items->Add(":meas:test:rdc");
			cbMiscInstrumentCmds->Items->Add(":meas:speed max");
			cbMiscInstrumentCmds->Items->Add(":meas:speed fast");
			cbMiscInstrumentCmds->Items->Add(":meas:speed med");
			cbMiscInstrumentCmds->Items->Add(":meas:lev 200m");
			cbMiscInstrumentCmds->Items->Add(":meas:freq 1e3");
			cbMiscInstrumentCmds->Items->Add(":meas:equ-cct ser");
			cbMiscInstrumentCmds->Items->Add(":meas:equ-cct par");

			cbMiscInstrumentCmds->Items->Add(":meas:func:l;r");
			cbMiscInstrumentCmds->Items->Add(":meas:func:l;q");
			cbMiscInstrumentCmds->Items->Add(":meas:func:c;r");

			cbMiscInstrumentCmds->Items->Add("---Trig Cmds--");
			cbMiscInstrumentCmds->Items->Add(":meas:trig");
			cbMiscInstrumentCmds->Items->Add(":trig");

			cbMiscInstrumentCmds->Items->Add("---Multi Cmds--");
			cbMiscInstrumentCmds->Items->Add(":multi:set");

			cbMiscInstrumentCmds->Items->Add(":multi:load test");
			cbMiscInstrumentCmds->Items->Add(":multi:save");

			cbMiscInstrumentCmds->Items->Add(":multi:test 1");
			cbMiscInstrumentCmds->Items->Add(":multi:func lp");
			cbMiscInstrumentCmds->Items->Add(":multi:func ls");
			cbMiscInstrumentCmds->Items->Add(":multi:freq 1e3");
			cbMiscInstrumentCmds->Items->Add(":multi:lev 200m");
			cbMiscInstrumentCmds->Items->Add(":multi:speed max");
			cbMiscInstrumentCmds->Items->Add(":multi:speed fast");
			cbMiscInstrumentCmds->Items->Add(":multi:speed med");
			cbMiscInstrumentCmds->Items->Add(":multi:speed slow");


			cbMiscInstrumentCmds->Items->Add(":multi:delay 1.0m");
			cbMiscInstrumentCmds->Items->Add(":multi:hi-lim 1.0e-3");
			cbMiscInstrumentCmds->Items->Add(":multi:lo-lim 0.1e-3");

			cbMiscInstrumentCmds->Items->Add(":multi:run");
			cbMiscInstrumentCmds->Items->Add(":multi:trig");

			cbMiscInstrumentCmds->Items->Add("---Graph Cmds--");
			cbMiscInstrumentCmds->Items->Add(":graph");
			cbMiscInstrumentCmds->Items->Add(":graph:func lp");
			cbMiscInstrumentCmds->Items->Add(":graph:func ls");
			cbMiscInstrumentCmds->Items->Add(":graph:func rp");
			cbMiscInstrumentCmds->Items->Add(":graph:func rs");

			cbMiscInstrumentCmds->Items->Add(":graph:sweep freq");
			cbMiscInstrumentCmds->Items->Add(":graph:st 100e3");
			cbMiscInstrumentCmds->Items->Add(":graph:sp 300e3");
			cbMiscInstrumentCmds->Items->Add(":graph:lev 100m");
			cbMiscInstrumentCmds->Items->Add(":graph:speed fast");
			cbMiscInstrumentCmds->Items->Add(":graph:speed med");
			cbMiscInstrumentCmds->Items->Add(":graph:speed slow");
			cbMiscInstrumentCmds->Items->Add(":graph:step 1");
			cbMiscInstrumentCmds->Items->Add(":graph:step 2");
			cbMiscInstrumentCmds->Items->Add(":graph:step 3");
			cbMiscInstrumentCmds->Items->Add(":graph:step 4");

			cbMiscInstrumentCmds->Items->Add(":graph:trig");

			cbMiscInstrumentCmds->Items->Add(":graph:autofit on");

			cbMiscInstrumentCmds->Items->Add(":graph:mkf 1e3");
			cbMiscInstrumentCmds->Items->Add(":graph:peak");
			cbMiscInstrumentCmds->Items->Add(":graph:mk?");
			cbMiscInstrumentCmds->Items->Add(":graph:mkf?");

			cbMiscInstrumentCmds->Text = "Command List";
*/


			break;
			}
		case 1:  // ?
			{

			break;
			}
		default:
			break;
	} // End Switch


}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Button4Click(TObject *Sender)
{



AppendCSVFileFoDData(false);



/*

   FailureCount = 0;

for (int i = 0; i < 100000; i++)
{

   RunMiscInstrumentTest_Ti_LDC();
   Application->ProcessMessages();


}




//AnsiString xx = ReadCSV("c:\\temp\\data.csv", true);
if (edTiLCDEvmFileAddress->Text == "")
{
	ShowMessage("No Filelocation eg C:\\temp\\data.csv");
}
else
{
	AnsiString xx = ReadCSV(edTiLCDEvmFileAddress->Text, true);

   ShowMessage(xx);
   ShowMessage("L1 = " + SplitStringFrom0(xx,3,','));
   ShowMessage("L2 = " + SplitStringFrom0(xx,4,','));
   ShowMessage("L3 = " + SplitStringFrom0(xx,5,','));
   ShowMessage("L4 = " + SplitStringFrom0(xx,6,','));

  ShowMessage("L1 freq = " + SplitStringFrom0(xx,11,','));
  ShowMessage("L2 freq = " + SplitStringFrom0(xx,12,','));
  ShowMessage("L3 freq = " + SplitStringFrom0(xx,13,','));
  ShowMessage("L4 freq = " + SplitStringFrom0(xx,14,','));

} // End If

*/

//AnsiString no = "1.0E3";
//float ans = StrToFloat(no);
  //	ShowMessage(FloatToStr(ans));

//   ShowMessage(SplitStringFrom0("dsfdfs,gfg",1,','));

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::shPlotterConnectedMouseActivate(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y, int HitTest, TMouseActivate &MouseActivate)

{
	ShowMessage("NOTE: Plotter Needs to be Open AND HOMED");
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btMiscInstruCalClick(TObject *Sender)
{


	switch(MessageDlg("Run Cal Tests?\nYes= Run OC CAL\nNo = Run SC CAL\nCANCEL = EXIT", mtInformation, mbYesNoCancel, 0))
	 {
		case mrYes:
		 {
			ShowMessage("Ready for OPEN Circuit Cal. Please OPEN Circuit Probes");
			Gen_Comport->PutString(GWLCR_runCal_oc + "\n");
			Wait_mS(200);
			break;

/*			switch(MessageDlg("Please check if Done\nIf DONE SHORT Probes FIRST then:\nYes = Run SHORT Cal Test\nNo = Exit", mtInformation, mbYesNo, 0))
			 {
				case mrYes:
				 {
					// ShowMessage("Wait until LCR Done\nWhen Done Now Ready for SHORT Circuit Cal");
					Gen_Comport->PutString(GWLCR_runCal_sc + "\n");
					Wait_mS(200);
					break;
				 }
				 default:
				 {
					// Do Nothing
					break;
				 }
			 }  // End Switch
*/
		 }
		case mrNo:
		 {
			// Do Nothing
			// ShowMessage("Wait until LCR Done\nWhen Done Now Ready for SHORT Circuit Cal");
			Gen_Comport->PutString(GWLCR_runCal_sc + "\n");
			Wait_mS(200);
			break;
		 }
		default:
			break;
	 }  // End Switch

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::cbTiEvmLDC_on_OffClick(TObject *Sender)
{

	if (cbTiEvmLDC_on_Off->Checked)
	{
		//ShowMessage("Read CSV file saved by EVM");
		shpMISDataConnected->Brush->Color = clLime;
		MiscLabelUpdate();
	}
	else
	{
		// Do Nothing
		shpMISDataConnected->Brush->Color = clSilver;

	} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Button12Click(TObject *Sender)
{
/*
bool StopTest;
	if (Button12->Caption == "Test CSV File")
	{
		// Running
		Button12->Caption = "Run Test CSV";
		StopTest = false;

	}
	else
	{
		Button12->Caption = "Test CSV File";
		StopTest = true;
	} // End If
*/

	StopTi_EVMTest = false;

	if (edTiLCDEvmFileAddress->Text == "")
	{
		ShowMessage("No Filelocation eg C:\\temp\\data.csv");
	}
	else
	{

	   FailureCount = 0;

		for (int i = 0; i < 10000; i++)
		{

		   RunMiscInstrumentTest_Ti_LDC();
		   Application->ProcessMessages();

		   if (StopTi_EVMTest)
		   {

				break;
		   }
		   else
		   {
				// Do Nothing
		   } // End If

		}  // End For
	} // End If


//   RunMiscInstrumentTest_Ti_LDC();

/*
   AnsiString TiEVMData = ReadCSV(edTiLCDEvmFileAddress->Text, true);

   //ShowMessage(xx);
   ShowMessage("L1 = " + SplitStringFrom0(TiEVMData,3,','));
   ShowMessage("L2 = " + SplitStringFrom0(TiEVMData,4,','));
   ShowMessage("L3 = " + SplitStringFrom0(TiEVMData,5,','));
   ShowMessage("L4 = " + SplitStringFrom0(TiEVMData,6,','));

   ShowMessage("L1 freq = " + SplitStringFrom0(TiEVMData,11,','));
   ShowMessage("L2 freq = " + SplitStringFrom0(TiEVMData,12,','));
   ShowMessage("L3 freq = " + SplitStringFrom0(TiEVMData,13,','));
   ShowMessage("L4 freq = " + SplitStringFrom0(TiEVMData,14,','));
*/

/*
   ShowMessage("L1 = " + MiscTiEVM_LDCResult[1]);
   ShowMessage("L2 = " + MiscTiEVM_LDCResult[2]);
   ShowMessage("L3 = " + MiscTiEVM_LDCResult[3]);
   ShowMessage("L4 = " + MiscTiEVM_LDCResult[4]);

   ShowMessage("L1 freq = " + MiscTiEVM_LDCResult[5]);
   ShowMessage("L2 freq = " + MiscTiEVM_LDCResult[6]);
   ShowMessage("L3 freq = " + MiscTiEVM_LDCResult[7]);
   ShowMessage("L4 freq = " + MiscTiEVM_LDCResult[8]);
*/

/*		MiscTiEVM_LDCResult[1] = SplitStringFrom0(TiEVMData,3,',');
		MiscTiEVM_LDCResult[2] = SplitStringFrom0(TiEVMData,4,',');
		MiscTiEVM_LDCResult[3] = SplitStringFrom0(TiEVMData,5,',');
		MiscTiEVM_LDCResult[4] = SplitStringFrom0(TiEVMData,6,',');
		MiscTiEVM_LDCResult[5] = SplitStringFrom0(TiEVMData,11,',');
		MiscTiEVM_LDCResult[6] = SplitStringFrom0(TiEVMData,12,',');
		MiscTiEVM_LDCResult[7] = SplitStringFrom0(TiEVMData,13,',');
		MiscTiEVM_LDCResult[8] = SplitStringFrom0(TiEVMData,14,',');
*/


//} // End If

}
//---------------------------------------------------------------------------




void __fastcall TmainForm::Button13Click(TObject *Sender)
{
			lbTiEVMStatus->Caption = "Test STOPPED";
			StopTi_EVMTest = true;
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Button14Click(TObject *Sender)
{

	switch(MessageDlg("Please enter option\nYes = Read or Save to PC Registry (PC Specific)\nNo = Read from 'ini' File same name as this file (Not PC Specific)\nCancel = Exit", mtInformation, mbYesNoCancel, 0))
	 {
		case mrYes:
		 {

			if (MessageDlg("Yes = Save 'ini' File to PC Registry\nNo = Read from PC Registry", mtInformation, mbYesNo, 0) == mrYes)
			 {
				//ShowMessage("Yes");
				SaveEffPlotIniFileData();
			 }
			 else
			 {
				 //ShowMessage("No");
				 // Do Nothing

			 } // End If

			ebVinMultiplier->Text = Reg_ReadString(sREG, "Multipliers", "Vin", "1.0");
			ebIinMultiplier->Text =  Reg_ReadString(sREG, "Multipliers", "Iin", "1.0");
			ebVoutMultiplier->Text = Reg_ReadString(sREG, "Multipliers", "Vout", "1.0");
			ebIoutMultiplier->Text = Reg_ReadString(sREG, "Multipliers", "Iout", "1.0");

			ebVinOffset->Text = Reg_ReadString(sREG, "Offsets", "Vin", "0.0");
			ebIinOffset->Text = Reg_ReadString(sREG, "Offsets", "Iin", "0.0");
			ebVoutOffset->Text = Reg_ReadString(sREG, "Offsets", "Vout", "0.0");
			ebIoutOffset->Text = Reg_ReadString(sREG, "Offsets", "Iout", "0.0");

			break;
		 }
		case mrNo:
		 {
			ReadEffPlotIniFileData();
			/*
			// Match Reg
			ebVinMultiplier->Text = Reg_ReadString(sREG, "Multipliers", "Vin", "1.0");
			ebIinMultiplier->Text =  Reg_ReadString(sREG, "Multipliers", "Iin", "1.0");
			ebVoutMultiplier->Text = Reg_ReadString(sREG, "Multipliers", "Vout", "1.0");
			ebIoutMultiplier->Text = Reg_ReadString(sREG, "Multipliers", "Iout", "1.0");

			ebVinOffset->Text = Reg_ReadString(sREG, "Offsets", "Vin", "0.0");
			ebIinOffset->Text = Reg_ReadString(sREG, "Offsets", "Iin", "0.0");
			ebVoutOffset->Text = Reg_ReadString(sREG, "Offsets", "Vout", "0.0");
			ebIoutOffset->Text = Reg_ReadString(sREG, "Offsets", "Iout", "0.0");
			*/

			break;
		 }
		default:
		{
			break;
		}

	 }  // End Switch


}
//---------------------------------------------------------------------------

void __fastcall TmainForm::StrGridSeqTestExit(TObject *Sender)
{
//ShowMessage("out");
}
//---------------------------------------------------------------------------




void __fastcall TmainForm::Button16Click(TObject *Sender)
{

//lbMisc

		UserSelectComport(Arduino_FOD,"Select Arduino Comport", "PortArdFOD", StrToInt(edBaudArdFoDSerialBd->Text));

		if( Arduino_FOD == NULL)
		{
			// Error
			MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
		}
		else
		{
				//Gen_Comport->Open;

				edBaudArdFoDSerialNo->Text = IntToStr(Arduino_FOD->ComNumber);

				shpMISDataConnected->Brush->Color = clLime;
				MiscLabelUpdate();

				// Make sure all relays OFF
				SendCmdFoDArduino(FoDArduinoCmds[1]);


		}  // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::cbPlotterType2Change(TObject *Sender)
{
	if (cbPlotterType2->ItemIndex == 0)
	{
		// ***
		// 2d Plotter Selected
		// ***

/*		if( PSU_Comport->Open == true )
		{
			PSU_Comport->FlushInBuffer();
			PSU_Comport->FlushOutBuffer();
			PSU_Comport->Open = false;

		}
		else
		{

		} // End If
*/
		Plotter->SetPlotterType2D(true);
		edPSU3DPlot2->Visible = false;
//		lb3DPSU->Visible = false;
		pnl3DPlot->Visible = false;


		btnPlotComportOpen2->Enabled = true;
		//pnlPlotterControl->Visible = true;

	}
	else if (cbPlotterType2->ItemIndex == 1)
	{

		btnPlotComportOpen2->Enabled = true;

		pnl3DPlot->Visible = true;
		cbEVK->Checked = false;
		//cbe_LoadSelect->Checked = true;
//		pnlPlotterControl->Visible = true;
//		ShowMessage("NOTES:\n1. You Need Set 3D Plotter comport (You may need to WAIT)\n2. You Need to go to Maint Sheet and Set PSU comport\n4. You Need to go to Maint Sheet and Set e-load comport");

/*
		// ***
		// 3d plotter Selected
		// ***
		Plotter->SetPlotterType2D(false);
		// Power supply uses Plotter 2D controller

//		int PSUComPort = UserSelectComport2("Select PSU Com-Port for Plotter ", "PortPSU", 57600);

		UserSelectComport(PSU_Comport,"Select PSU Comport", "PortPSU", 57600);

		if( PSU_Comport->Open == false )
		{
			PSU_Comport->FlushInBuffer();
			PSU_Comport->FlushOutBuffer();
			PSU_Comport->Open = false;
//			edPSU3DPlot->Visible = false;
//			lb3DPSU->Visible = false;
			pnl3DPlot->Visible = false;
		}
		else
		{
			pnl3DPlot->Visible = true;
//			lb3DPSU->Visible = true;
//			edPSU3DPlot->Visible = true;
			edPSU3DPlot->Text = IntToStr(PSU_Comport->ComNumber);

//			PSU_Comport->PutString("");

/*			enLoadRelay3D(false, 0);
			enLoadRelay3D(false, 1);
			enLoadRelay3D(false, 2);
Sleep(500);

			enLoadRelay3D(true, 0);
			enLoadRelay3D(true, 1);
			enLoadRelay3D(true, 2);
Sleep(500);
			enLoadRelay3D(false, 0);
			enLoadRelay3D(false, 1);
			enLoadRelay3D(false, 2);
*/
//		} // End If

	}
	else
	{
			// ***
			// MOA Selected cbPlotterType->ItemIndex == 1
			// ***
			pnl3DPlot->Visible = true;
			cbEVK->Checked = false;
			//cbe_LoadSelect->Checked = true;


			ShowMessage("NOTES:\n1. You Need Set 3D Plotter comport (You may need to WAIT)\n2. You Need to go to Maint Sheet and Set PSU comport\n4. You Need to go to Maint Sheet and Set e-load comport");

	} // End If


}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Button17Click(TObject *Sender)
{
//void __fastcall TmainForm::btnPlotComportOpenClick(TObject *Sender)

	   //	Arduino_FOD->PutString(cbFoDArduinoCommands->Text);
	   //	Arduino_FOD->PutString("\n");

	   SendCmdFoDArduino(cbFoDArduinoCommands->Text);


}

//---------------------------------------------------------------------------
void __fastcall TmainForm::SendCmdFoDArduino(AnsiString Text2Send)
{
//void __fastcall TmainForm::btnPlotComportOpenClick(TObject *Sender)

		Arduino_FOD->PutString(Text2Send);
//		Arduino_FOD->PutString("\n");

}

//---------------------------------------------------------------------------

void __fastcall TmainForm::btnPlotComportOpen2Click(TObject *Sender)
{
//USBMM->DMM_Task();

//	PlotterComPort = UserSelectComport("Select Plotter Com-Port for Plotter " + IntToStr((int)Plotter_Number) , "PortPlotter", 57600);

	if (cbPlotterType2->ItemIndex == 0)
	{
		// 2D Plotter
		UserSelectComport(Plotter_Comport,"Select Plotter Com-Port for Plotter " + IntToStr((int)Plotter_Number) , "PortPlotter", 57600);
	}
	else
	{
		// 3D Plotter
		UserSelectComport(Plotter_Comport,"Select Plotter Com-Port for Plotter " + IntToStr((int)Plotter_Number) , "PortPlotter", 115200);

	} // End If


	//initialize plotter interface
	Plotter = new XYPlotter(Plotter_Comport, memoPlotterLog,tmPlotter_NRT);
//	mainForm->Caption = mainForm->Caption + " - CONNECTED to Plotter No. " + IntToStr((int)Plotter_Number)  ;
//	mainForm->Caption = mainForm->Caption + " Plotter " + IntToStr((int)Plotter_Number)  ;


	// if com-port could not be opened close application
	if(Plotter == NULL)
	{
		// Plotter NOT CONNECTED

		shPlotterConnected->Brush->Color = clSilver;
		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
	}
	else
	{
		// Plotter CONNECTED
		//		sdFlushPort(PlotterComPort);
//		PlotterComPort_Rx_Thread = new Rx_Thread( PlotterComPort, RX_NODE_PLOTTER_COMPORT );

//		shPlotterConnected->Brush->Color = clLime;

		if( Plotter_Comport->Open )
//	  if (Plotter_Comport->ComNumber == NULL)
		{
			//ShowMessage("yes");
			Plotter_Comport->FlushInBuffer();
			Plotter_Comport->FlushOutBuffer();

			pnlManualPlotterCmd->Enabled = true;


	   //	shPlotterConnected->Brush->Color = (Plotter->UpdateConnectionState()) ? clLime :clSilver;

		edPlotterComportNo2->Text = IntToStr(Plotter_Comport->ComNumber);

		//set buttons
		btnPlotComportOpen2->Enabled = false;
		btnPlotComportClose2->Enabled = true;

		btnManGoToTestStart2->Enabled = true;
		btnHomePlotterSet2->Enabled = true;
		btHomePlotter->Enabled = true;

		btGotoXY2->Enabled = true;
		//btnGoToOffset2->Enabled = true;
		btnManGoToTestStart2->Enabled = true;

		btEnablePSU->Enabled = true;

		//rgLoad->Enabled = true;

		cbPSU_ONOff->Enabled = true;

		btnGoToStepEnd2->Enabled = true;

		btnLoadUnload2->Enabled = true;

		//pnlPlotterControl->Visible = true;
		//pnlPlotterControl->Enabled = true;

	   ShowMessage("NOTE: Remember to Home Plotter FIRST before Running Test");

	  }
	 else
	 {
			//ShowMessage("no");
			edPlotterComportNo2->Text = "";


	 } // End If


	}  // End If


}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnPlotComportClose2Click(TObject *Sender)
{

	//stop timer
//	sdFlushPort(PlotterComPort);
//	sdClosePort(PlotterComPort);

//	PlotterComPort_Rx_Thread->FreeOnTerminate = false;
//	PlotterComPort_Rx_Thread->Terminate();
//	PlotterComPort_Rx_Thread->WaitFor() ;

//	delete PlotterComPort_Rx_Thread;


	//set buttons
	btnPlotComportOpen2->Enabled = false;

	btnPlotComportClose2->Enabled = false;
	btnManGoToTestStart2->Enabled = false;
	btnHomePlotterSet2->Enabled = false;

	btHomePlotter->Enabled = false;

	btGotoXY2->Enabled = false;
   //	btnGoToOffset->enabled = false;
   //	btnGoToOffset2->Enabled = false;
	btnManGoToTestStart2->Enabled = false;

	btEnablePSU->Enabled = false;

	rgLoad->Enabled = false;

	cbPSU_ONOff->Enabled = false;

	shPlotterConnected->Brush->Color = clSilver;

	btnGoToStepEnd2->Enabled = false;

	btnLoadUnload2->Enabled = false;

	//pnlPlotterControl->Enabled = false;
	//pnlPlotterControl->Visible = false;

// Close Any 3D Plotter PSU Port
		if( PSU_Comport->Open )
		{
			PSU_Comport->FlushInBuffer();
			PSU_Comport->FlushOutBuffer();
			PSU_Comport->Open = false;

		}
		else
		{
			// Do Nothing
		} // End If

// Close any Plotter PSU Port
		if( Plotter_Comport->Open )
		{
			Plotter_Comport->FlushInBuffer();
			Plotter_Comport->FlushOutBuffer();
			Plotter_Comport->Open = false;

		}
		else
		{
			// Do Nothing
			edPlotterComportNo2->Text = "";
		} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::cbOrienZRotation2Change(TObject *Sender)
{
	//Plotter->SendCommand("M340 P0 S1450");
		//ShowMessage("Done");

	   switch (cbOrienZRotation2->ItemIndex)
	   {
		 case 0:
			//ShowMessage("0");
			Plotter->SendCommand("M340 P0 S1050");
			break;
		 case 1:
			//ShowMessage("-45");
			Plotter->SendCommand("M340 P0 S1400");
			break;
		 case 2:
			//ShowMessage("-90");
			Plotter->SendCommand("M340 P0 S1950");
			break;
	   default:
		   ;
	   } // End Switch
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnHomePlotterSet2Click(TObject *Sender)
{
	InitAndHomePlotter();
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebPowerUpWait2Change(TObject *Sender)
{
if (ebPowerUpWait2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebPowerUpWait2->Text = "12000";
   }
   else
   {
	Reg_WriteString(sREG, "Power", "On_Time", ebPowerUpWait2->Text);
	TestLogAdd("New [On Time] : " + ebPowerUpWait2->Text + "ms");
	int intTimeSecs = StrToInt(mainForm->ebPowerUpWait2->Text)/1000;
	AnsiString TimeSecs = IntToStr(intTimeSecs);
//	mainForm->edTestWait->Text = mainForm->ebPowerUpWait->Text;
	mainForm->edTestWait->Text = TimeSecs;

   }  // End If}
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebOffTime2Change(TObject *Sender)
{
	Reg_WriteString(sREG, "Power", "Off_Time", ebOffTime2->Text);
	TestLogAdd("New [Off Time] : " + ebOffTime2->Text + "ms");
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebXSetpointStart2Change(TObject *Sender)
{

if (ebXSetpointStart2->Text == "-")
{
	//Ignore - symbol

}
else
{
	// OK
 if (ebXSetpointStart2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebXSetpointStart2->Text = "5";
   }
   else
   {
//	Reg_WriteString(sREG, "Cords", "Y_Offset", ebYOffset->Text);
//	TestLogAdd("New [Y Offset] : " + ebYOffset->Text);

	Reg_WriteString(sREG, "Cords", "Y_SetPoint", ebYSetpointStart2->Text);
	TestLogAdd("New [Y SetPoint] : " + ebYSetpointStart2->Text);

	//update plotter settings
	XYPlotter_setpoint_t tmpOffset;
	tmpOffset.XPos=StrToFloat(ebXSetpointStart2->Text);
	tmpOffset.YPos=StrToFloat(ebYSetpointStart2->Text);
	//Plotter->SetPlotStart(tmpOffset);

   } // End If

} // End If
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebXOffset2Change(TObject *Sender)
{

try
{
	if (ebXOffset2->Text == "-")
	{
		//Ignore - symbol

	}
	else
	{
		// OK
		if (ebXOffset2->Text == "")
	   {
			ShowMessage("Cannot leave Blank -> Will Put default number");
			ebXOffset2->Text = "5";
	   }
	   else
	   {
		Reg_WriteString(sREG, "Cords", "X_Offset", ebXOffset2->Text);
		TestLogAdd("New [X Offset] : " + ebXOffset2->Text);

	//	Reg_WriteString(sREG, "Cords", "X_SetPoint", ebXSetpoint->Text);
	//	TestLogAdd("New [X SetPoint] : " + ebXSetpoint->Text);

		//update plotter settings
		XYPlotter_setpoint_t tmpOffset;
		tmpOffset.XPos=StrToInt(ebXOffset2->Text);
		tmpOffset.YPos=StrToInt(ebYOffset2->Text);
	 //	Plotter->SetOffset(tmpOffset);
	   //	Plotter->SetPlotStart(tmpOffset);
	   } // End If

	} // End If
}
catch (...)
{
	 ShowMessage("Entry is not an Integer, Will Reset to 0");
	 ebXOffset2->Text = "0";
} // End Try

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebYSetpointStart2Change(TObject *Sender)
{

if (ebYSetpointStart2->Text == "-")
{
	//Ignore - symbol

}
else
{
	// OK
 if (ebYSetpointStart2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebYSetpointStart2->Text = "5";
   }
   else
   {
//	Reg_WriteString(sREG, "Cords", "Y_Offset", ebYOffset->Text);
//	TestLogAdd("New [Y Offset] : " + ebYOffset->Text);

	Reg_WriteString(sREG, "Cords", "Y_SetPoint", ebYSetpointStart2->Text);
	TestLogAdd("New [Y SetPoint] : " + ebYSetpointStart2->Text);

	//update plotter settings
	XYPlotter_setpoint_t tmpOffset;
	tmpOffset.XPos=StrToFloat(ebXSetpointStart2->Text);
	tmpOffset.YPos=StrToFloat(ebYSetpointStart2->Text);
	//Plotter->SetPlotStart(tmpOffset);

   } // End If

} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebYOffset2Change(TObject *Sender)
{
try
{
	if (ebYOffset2->Text == "-")
	{
		//Ignore - symbol

	}
	else
	{
		// OK
		if (ebYOffset2->Text == "")
	   {
			ShowMessage("Cannot leave Blank -> Will Put default number");
			ebYOffset2->Text = "5";
	   }
	   else
	   {
		Reg_WriteString(sREG, "Cords", "Y_Offset", ebYOffset2->Text);
		TestLogAdd("New [Y Offset] : " + ebYOffset2->Text);

	//	Reg_WriteString(sREG, "Cords", "Y_SetPoint", ebYSetpoint->Text);
	//	TestLogAdd("New [Y SetPoint] : " + ebYSetpoint->Text);

		//update plotter settings
		XYPlotter_setpoint_t tmpOffset;
		tmpOffset.XPos=StrToInt(ebXOffset2->Text);
		tmpOffset.YPos=StrToInt(ebYOffset2->Text);
		//Plotter->SetPlotStart(tmpOffset);
	   } // End If

	} // End If
}

catch (...)
{
	 ShowMessage("Entry is not an Integer, Will Reset to 0");
	 ebYOffset2->Text = "0";
} // End Try

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnManGoToTestStart2Click(TObject *Sender)
{

	if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
	{
		// OK
		MovePlotterToPointMAIN(1, 1, 1, StrToFloat(ed3DZHeight2->Text), 1);
	}

	else
	{
		switch(MessageDlg("WARNING NO Seq File Loaded. Check Co-ordinates!\nYes= Run\nNo = Exit", mtInformation, mbYesNo, 0))
		 {
			case mrYes:
			 {
				MovePlotterToPointMAIN(1, 1, 1, StrToFloat(ed3DZHeight2->Text), 1);
				break;
			 }
			case mrNo:
			 {
				break;
			 }
			default:
				break;
		 }  // End Switch

	} // End If



/*
	XYPlotter_setpoint_t tmpSetpoint;
//	tmpSetpoint.XPos = StrToInt(ebXOffset->Text);
//	tmpSetpoint.YPos = StrToInt(ebYOffset->Text);
	tmpSetpoint.XPos = StrToInt(ebXSetpointStart->Text); // - StrToInt(ebXOffset->Text);
	tmpSetpoint.YPos = StrToInt(ebYSetpointStart->Text); // - StrToInt(ebYOffset->Text);
//	tmpSetpoint.XPos = 0;
//	tmpSetpoint.YPos = 0;

	Plotter->Set3dZHeight(StrToFloat(ed3DZHeight->Text));

	//update plotter setpoint
	Plotter->UpdateSetpoint(tmpSetpoint);
	//Update user labels
	updatePosLabels(this);
*/
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebXSteps2Change(TObject *Sender)
{

if (ebXSteps2->Text == "-")
{
	// Do Nothing
}
else
{

if (StrToFloat(ebXSteps2->Text) < 0.0 )
{
	// Do Nothing
	ebXSteps2->Text = "0";
}
else
{
   if (ebXSteps2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebXSteps2->Text = "5";
   }
   else
   {

float StepSizemm = StrToFloat(ebStepSize2->Text);
	edXStepmm2->Text = FloatToStr(StrToFloat(ebXSteps2->Text) * StepSizemm);

	Reg_WriteString(sREG, "Cords", "X_Steps", ebXSteps2->Text);

	TestLogAdd("X Steps : " + ebXSteps2->Text);

	InitPlotGrid();
	} //End If

}

}  // End If


}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebYSteps2Change(TObject *Sender)
{

if (ebYSteps2->Text == "-")
{
	// Do Nothing
}
else
{

if (StrToFloat(ebYSteps2->Text) < 0.0 )
{
	// Do Nothing
	ebYSteps2->Text = "0";
}
else
{

   if (ebYSteps2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebYSteps2->Text = "5";
   }
   else
   {
	float StepSizemm = StrToFloat(ebStepSize2->Text);
	edYStepmm2->Text = FloatToStr(StrToFloat(ebYSteps2->Text) * StepSizemm);

	Reg_WriteString(sREG, "Cords", "Y_Steps", ebYSteps2->Text);

	TestLogAdd("Y Steps : " + ebYSteps2->Text);

	InitPlotGrid();
   } // End If

} // End If

} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebStepSize2Change(TObject *Sender)
{
if (ebStepSize2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		ebStepSize2->Text = "5";
   }
   else
   {
		float StepSizemm = StrToFloat(ebStepSize2->Text);
		edXStepmm2->Text = FloatToStr(StrToFloat(ebXSteps2->Text) * StepSizemm);
		edYStepmm2->Text = FloatToStr(StrToFloat(ebYSteps2->Text) * StepSizemm);

		Reg_WriteString(sREG, "Cords", "Step_Size", ebStepSize2->Text);

		TestLogAdd("Step Size : " + ebStepSize2->Text);
		mainForm->edRxStep->Text = mainForm->ebStepSize2->Text;

   } // End If
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::edLoadXmm2Change(TObject *Sender)
{
 if (edLoadXmm2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		edLoadXmm2->Text = "280";
   }
   else
   {
	Reg_WriteString(sREG, "Load_Unload", "Xmm", edLoadXmm2->Text);
	TestLogAdd("Load_Unload Xmm Location " + edLoadXmm2->Text + "mm");
   } // End If
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::edLoadYmm2Change(TObject *Sender)
{
if (edLoadYmm2->Text == "")
   {
		ShowMessage("Cannot leave Blank -> Will Put default number");
		edLoadYmm2->Text = "120";
   }
   else
   {

	Reg_WriteString(sREG, "Load_Unload", "Ymm", edLoadYmm2->Text);
	TestLogAdd("Load_Unload Ymm Location " + edLoadYmm2->Text + "mm");
	}  // End If
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnLoadUnload2Click(TObject *Sender)
{

	if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
	{

		if ( StrToFloat(edLoadZmm2->Text) > 299)
		{
			ShowMessage("WARNING: Check Z MUST be less than 300mm\nElse Too high and you my need to Home again");
		}
		else
		{
			if ( StrToFloat(edLoadZmm2->Text) < StrToFloat(ed3DZHeight2->Text))
			{
				ShowMessage("WARNING: Check Z height as < Test Z height");
			}
			else
			{
				float ColSteps = 1 + StrToFloat(edLoadXmm2->Text) / StrToFloat(ebStepSize2->Text);
				float RowSteps = 1 + StrToFloat(edLoadYmm2->Text) / StrToFloat(ebStepSize2->Text);

				MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(edLoadZmm2->Text), 1);
			} // End If

		}  // End If

	}
	else
	{

		switch(MessageDlg("WARNING NO Seq File Loaded. Check Co-ordinates!\nYes= Run\nNo = Exit", mtInformation, mbYesNo, 0))
		 {
			case mrYes:
			 {

//----------------
		if ( StrToFloat(edLoadZmm2->Text) > 299)
		{
			ShowMessage("WARNING: Check Z MUST be less than 300mm\nElse Too high and you my need to Home again");
		}
		else
		{
			if ( StrToFloat(edLoadZmm2->Text) < StrToFloat(ed3DZHeight2->Text))
			{
				ShowMessage("WARNING: Check Z height as < Test Z height");
			}
			else
			{
				float ColSteps = 1 + StrToFloat(edLoadXmm2->Text) / StrToFloat(ebStepSize2->Text);
				float RowSteps = 1 + StrToFloat(edLoadYmm2->Text) / StrToFloat(ebStepSize2->Text);

				MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(edLoadZmm2->Text), 1);
			} // End If

		}  // End If
//----------------

				break;
			 }
			case mrNo:
			 {
				break;
			 }
			default:
				break;
		 }  // End Switch


	} // End If



/*
int StepSizemm = StrToInt(ebStepSize->Text);

if (MessageDlg("This is for Loading Unloading Rx. Location above buton = Xmm, Ymm, Zmm\nYES = OK\nNO= Exit", mtInformation, mbYesNo, 0) == mrYes)
	 {
		//ShowMessage("Yes");
		XYPlotter_setpoint_t tmpSetpoint;
		tmpSetpoint.XPos = StrToInt(edLoadXmm->Text);
		tmpSetpoint.YPos = StrToInt(edLoadYmm->Text);

		Plotter->UpdateSetpoint(tmpSetpoint);
		//Update user labels
		updatePosLabels(this);

		Plotter->Set3dZHeight(StrToFloat(edLoadZmm->Text));

	 }
	 else
	 {
		 //ShowMessage("No");
	 } // End If

*/

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnGotToPlotOffset2Click(TObject *Sender)
{

	if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
	{
		if (MessageDlg("Yes = Got To Start\nNo = Go to End", mtInformation, mbYesNo, 0) == mrYes)
		 {
			//ShowMessage("Yes");
			int ColSteps = 1 + StrToInt(edXOffsetStart2->Text);
			int RowSteps = 1 + StrToInt(edyOffsetStart2->Text);

			MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(ed3DZHeight2->Text), 1);

		 }
		 else
		 {
			 //ShowMessage("No");
			int ColSteps = 1 + StrToInt(edXOffsetStart2->Text) + StrToInt(edXOffsetSteps2->Text);
			int RowSteps = 1 + StrToInt(edyOffsetStart2->Text) + StrToInt(edyOffsetSteps2->Text);

			MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(ed3DZHeight2->Text), 1);

		 } // End If

	}
	else
	{

	switch(MessageDlg("WARNING NO Seq File Loaded. Check Co-ordinates!\nYes= Run\nNo = Exit", mtInformation, mbYesNo, 0))
		 {
			case mrYes:
			 {

//---------------
		if (MessageDlg("Yes = Got To Start\nNo = Go to End", mtInformation, mbYesNo, 0) == mrYes)
		 {
			//ShowMessage("Yes");
			int ColSteps = 1 + StrToInt(edXOffsetStart2->Text);
			int RowSteps = 1 + StrToInt(edyOffsetStart2->Text);

			MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(ed3DZHeight2->Text), 1);

		 }
		 else
		 {
			 //ShowMessage("No");
			int ColSteps = 1 + StrToInt(edXOffsetStart2->Text) + StrToInt(edXOffsetSteps2->Text);
			int RowSteps = 1 + StrToInt(edyOffsetStart2->Text) + StrToInt(edyOffsetSteps2->Text);

			MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(ed3DZHeight2->Text), 1);

		 } // End If
//---------------


				break;
			 }
			case mrNo:
			 {
				break;
			 }
			default:
				break;
		 }  // End Switch


	} // End If


}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btGotoXY2Click(TObject *Sender)
{
int ColSteps = 1 + StrToInt(ebXOffset2->Text);
int RowSteps = 1 + StrToInt(ebYOffset2->Text);

	if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
	{
		MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(ed3DZHeight2->Text), 1);
	}
	else
	{

		switch(MessageDlg("WARNING NO Seq File Loaded. Check Co-ordinates!\nYes= Run\nNo = Exit", mtInformation, mbYesNo, 0))
		 {
			case mrYes:
			 {
				MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(ed3DZHeight2->Text), 1);
				break;
			 }
			case mrNo:
			 {
				break;
			 }
			default:
				break;
		 }  // End Switch

	} // End If


	//float ColSteps = 1 + StrToFloat(ebXOffset2->Text) / StrToFloat(ebStepSize2->Text);
	//float RowSteps = 1 + StrToFloat(ebYOffset2->Text) / StrToFloat(ebStepSize2->Text);


/*
	XYPlotter_setpoint_t tmpSetpoint;
//	tmpSetpoint.XPos = StrToInt(ebXSetpointStart->Text);
//	tmpSetpoint.YPos = StrToInt(ebYSetpointStart->Text);
//	tmpSetpoint.XPos = StrToInt(ebXOffset->Text) + StrToInt(ebXSetpoint->Text);
//	tmpSetpoint.YPos = StrToInt(ebYOffset->Text) + StrToInt(ebYSetpoint->Text);
	tmpSetpoint.XPos = StrToInt(ebXSetpointStart->Text) + StrToInt(ebXOffset->Text);
	tmpSetpoint.YPos = StrToInt(ebYSetpointStart->Text) + StrToInt(ebYOffset->Text);

//	tmpSetpoint.XPos = StrToInt(ebXOffset->Text);
// 	tmpSetpoint.YPos = StrToInt(ebYOffset->Text);

	//update plotter setpoint
	Plotter->UpdateSetpoint(tmpSetpoint);
	//Update user labels
	updatePosLabels(this);

	Plotter->Set3dZHeight(StrToFloat(ed3DZHeight->Text));
*/



}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnGoToStepEnd2Click(TObject *Sender)
{
int ColSteps = 1 + StrToInt(ebXSteps2->Text);
int RowSteps = 1 + StrToInt(ebYSteps2->Text);

	if (StrGridSeqTest->Cells[0][0] == "SEQ GRID")
	{
		// OK
		MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(ed3DZHeight2->Text), 1);
	}

	else
	{

		switch(MessageDlg("WARNING NO Seq File Loaded. Check Co-ordinates!\nYes= Run\nNo = Exit", mtInformation, mbYesNo, 0))
		 {
			case mrYes:
			 {
				MovePlotterToPointMAIN(ColSteps, RowSteps, 1, StrToFloat(ed3DZHeight2->Text), 1);
				break;
			 }
			case mrNo:
			 {
				break;
			 }
			default:
				break;
		 }  // End Switch


	} // End If

/*

int StepSizemm = StrToInt(ebStepSize->Text);

	XYPlotter_setpoint_t tmpSetpoint;
	tmpSetpoint.XPos = StrToInt(ebXSetpointStart->Text) + StrToInt(ebXSteps->Text) * StepSizemm;
	tmpSetpoint.YPos = StrToInt(ebYSetpointStart->Text) + StrToInt(ebYSteps->Text) * StepSizemm;

	Plotter->UpdateSetpoint(tmpSetpoint);
	//Update user labels
	updatePosLabels(this);

	Plotter->Set3dZHeight(StrToFloat(ed3DZHeight->Text));
*/

}
//---------------------------------------------------------------------------
void __fastcall TmainForm::Button18Click(TObject *Sender)
{

	lbStatusGWLCR->Caption = "STATUS: START LCR";

	//***
	// Get LC Data
	//***

	lbFoDCoilStatus->Caption = "Run Check LCR";
	 RunMiscInstrumentTest(true);

	lbFoDCoilStatus->Caption = "Now Start Reading FoD coils";

	 RunFoDCoilTest();

	 lbStatusGWLCR->Caption = "STATUS: LCR DONE";

	 cbFodResults->Clear();
	 cbFodResults->Text = "FoD Coil results";

	 int offsetfor_r;
	 for (int coili = 1; coili < 9; coili++)
	 {
			for (int freqi = 1; freqi < 11; freqi++)
			{
				offsetfor_r = freqi + 10;
				cbFodResults->Items->Add("C" + IntToStr(coili) + "L= " + FoDCoilResult[coili][freqi]) + "_Freq " + IntToStr(freqi);
				cbFodResults->Items->Add("C" + IntToStr(coili) + "r= " + FoDCoilResult[coili][offsetfor_r]) + "_Freq " + IntToStr(freqi);




			}  // End For

	 } // End For

	lbFoDCoilStatus->Caption = "STATUS: DONE";

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::RunFoDCoilTest(void)
{

int coili;

	 // ***
	 // Check if FoD selected
	 // ***
	 if ((Arduino_FOD->Open) && (GetSeqGridColNofText(SeqGridKeyword[96][0]) > 0))
		{
			// OK
			for (coili = 1; coili < 8; coili++)
			{
			   //***
			   // MUX Coil to test
			   //***

			   if (flgRunGenComport )
			   {
					// Do Nothing
			   }
			   else
			   {
					break;
			   } // End If

				// Ensure all Coils OFF
				SendCmdFoDArduino(FoDArduinoCmds[1]);
				Wait_mS(10);

				switch (coili)
				{
					case 1:
					{
						SendCmdFoDArduino(FoDArduinoCmds[2]);
						Wait_mS(10);
						lbFoDCoilStatus->Caption = "STATUS: Set Coil 1";
						break;
					}
					case 2:
					{
						SendCmdFoDArduino(FoDArduinoCmds[4]);
						Wait_mS(10);
						lbFoDCoilStatus->Caption = "STATUS: Set Coil 2";
						break;
					}
					case 3:
					{
						SendCmdFoDArduino(FoDArduinoCmds[6]);
						Wait_mS(10);
						lbFoDCoilStatus->Caption = "STATUS: Set Coil 3";
						break;
					}
					case 4:
					{
						SendCmdFoDArduino(FoDArduinoCmds[8]);
						Wait_mS(10);
						lbFoDCoilStatus->Caption = "STATUS: Set Coil 4";
						break;
					}
					case 5:
					{
						SendCmdFoDArduino(FoDArduinoCmds[10]);
						Wait_mS(10);
						lbFoDCoilStatus->Caption = "STATUS: Set Coil 5";
						break;
					}
					case 6:
					{
						SendCmdFoDArduino(FoDArduinoCmds[12]);
						Wait_mS(10);
						lbFoDCoilStatus->Caption = "STATUS: Set Coil 6";
						break;
					}
					case 7:
					{
						SendCmdFoDArduino(FoDArduinoCmds[14]);
						Wait_mS(10);
						lbFoDCoilStatus->Caption = "STATUS: Set Coil 7";
						break;
					}
				default:
					{
					   // Do Nothing
					   break;
					}
					;
				} // End Switch
			   //***
			   // Ends MUX Coil to test
			   //***


			   //***
			   // Wait before LCR Read
			   //***

				AnsiString CurrentCoil = lbFoDCoilStatus->Caption;

				lbStatusGridSeq->Caption = "State: Read DAQ: FoD Coil - " + IntToStr(coili);

				for (int i = 0; i < StrToInt(EdWaitBeforeLCRRead->Text); i++)
				{
					Wait_mS(1);
					AnsiString WaitStatus = "Read Wait " + IntToStr(i);
					lbFoDCoilStatus->Caption = CurrentCoil + "->" + WaitStatus;

					Application->ProcessMessages();
				}  // End For

				lbFoDCoilStatus->Caption = CurrentCoil;


			   //***
			   // Ends Wait before LCR Read
			   //***

			   lbStatusGridSeq->Caption = "State: Read DAQ: Begin LCR";

			   LCR_ReadData();
			   SetFoDCoilResults(coili);

			   lbStatusGridSeq->Caption = "State: Read DAQ: End LCR";

			}  // End For

		}
		else
		{
		   //	LCR_ReadData();
		}  // End If

	//***
	// Ends Get LC Data
	//***

}
//---------------------------------------------------------------------------


void __fastcall TmainForm::CloseGUI1Click(TObject *Sender)
{
	CloseGUI();
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::BClick(TObject *Sender)
{
	//Big_View->Checked = false;
	SetFormSize();
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Button19Click(TObject *Sender)
{
	if(Arduino_FOD->Open)
		{
///			flgRunGenComport = false;

			Arduino_FOD->FlushInBuffer();
			Arduino_FOD->FlushOutBuffer();
			Arduino_FOD->Open = false;
//		lbStatusGridSeq->Caption = "STATUS: Gen Misc Port CLOSED";
//			break;

		edBaudArdFoDSerialNo->Text = 0;

		}
		else
		{
			// Do Nothing
		} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ed3DZHeight2Change(TObject *Sender)
{

	if ( StrToFloat(ed3DZHeight2->Text) > 290 )
	{
		ed3DZHeight2->Text = 290;
		ShowMessage("WARNING: Check Z MUST be 290mm max\nElse Too high and you my need to Home again");
	}
	else
	{
		// Do Nothing

	}

	Reg_WriteString(sREG, "Cords", "Z_SetPoint", ed3DZHeight2->Text);
	TestLogAdd("New [Z SetPoint] : " + ed3DZHeight2->Text);

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::edLoadZmm2Change(TObject *Sender)
{
	if ( StrToFloat(edLoadZmm2->Text) > 290 )
	{
		edLoadZmm2->Text = 290;
		ShowMessage("WARNING: Check Z MUST be 290mm max\nElse Too high and you my need to Home again");
	}
	else
	{
		// Do Nothing

	}
}
//---------------------------------------------------------------------------


void __fastcall TmainForm::Button20Click(TObject *Sender)
{

	ebYSetpointStart2->Text = FloatToStr(StrToFloat(ebYSetpointStart2->Text) + StrToFloat(edNudgeAmount->Text));

	MovePlotterToPointMAIN(1, 1, 1, StrToFloat(ed3DZHeight2->Text), 1);

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Button23Click(TObject *Sender)
{
	ebXSetpointStart2->Text = FloatToStr(StrToFloat(ebXSetpointStart2->Text) - StrToFloat(edNudgeAmount->Text));

	MovePlotterToPointMAIN(1, 1, 1, StrToFloat(ed3DZHeight2->Text), 1);

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Button22Click(TObject *Sender)
{
	ebXSetpointStart2->Text = FloatToStr(StrToFloat(ebXSetpointStart2->Text) + StrToFloat(edNudgeAmount->Text));

	MovePlotterToPointMAIN(1, 1, 1, StrToFloat(ed3DZHeight2->Text), 1);

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Button21Click(TObject *Sender)
{
	ebYSetpointStart2->Text = FloatToStr(StrToFloat(ebYSetpointStart2->Text) - StrToFloat(edNudgeAmount->Text));

	MovePlotterToPointMAIN(1, 1, 1, StrToFloat(ed3DZHeight2->Text), 1);

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Button24Click(TObject *Sender)
{
	ed3DZHeight2->Text = FloatToStr(StrToFloat(ed3DZHeight2->Text) + StrToFloat(edZheightNudge->Text));

	MovePlotterToPointMAIN(1, 1, 1, StrToFloat(ed3DZHeight2->Text), 1);

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Button25Click(TObject *Sender)
{
	ed3DZHeight2->Text = FloatToStr(StrToFloat(ed3DZHeight2->Text) - StrToFloat(edZheightNudge->Text));

	MovePlotterToPointMAIN(1, 1, 1, StrToFloat(ed3DZHeight2->Text), 1);

}
//---------------------------------------------------------------------------



void __fastcall TmainForm::Button26Click(TObject *Sender)
{
	flgRunGenComport = false;
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::PSU_ComportGWTriggerAvail(TObject *CP, WORD Count)
{
static char RxBuffer2[40];

//static int State = WAIT_SOM;
char input;
//static char LRC;
static int i;
static int Ri_Ptr;

AnsiString RX_String;

	while (Count != 0)
	{
//		if (Recived_Message == false)
//		{
			input = PSU_ComportGW->GetChar();

	  //		if ((input == '\0') || (input == '\n') || (input == '\r'))	//if STX found in data resync
			if ((input == '\n') || (input == '\r'))	//if STX found in data resync
			{
				RxBuffer2[Ri_Ptr++] = input;

				for (i = 0; i < sizeof(RxBuffer2); i++)
				{
					if ((RxBuffer2[i] == '\n') || (RxBuffer2[i] == '\r'))
					{
						break;
					}
					else
					{
						RX_String = RX_String + RxBuffer2[i];
					} // End If

				}  // End For

				if ((RX_String == "") || (RX_String == "wait"))
				{
					// Do nothing
				}
				else
				{
					//***
					// Message Received
					//***

					//TestLogAdd("Plotter msg : " + RX_String);
					//ShowMessage("Plotter msg : " + RX_String);

/*					if (Memo1->Lines->Count > 50)
					{
						Memo1->Clear();
					}
					else
					{
						Memo1->Lines->Add("Received msg : " + RX_String);
					} // End If
*/

				if (edPSUDAQV->Text == "ReadV")
				{

					//float temp = roundoff(StrToFloat(RX_String)/100.00f,2);
				   //	int test = StrToInt(RX_String)/100;

					  char FormatDaqV[10];
					  float tempNo = StrToInt(RX_String)/100.00f;

					  sprintf(FormatDaqV,"%.2f", tempNo);
					  AnsiString temp = FormatDaqV;

					  //char temp2[8];
					//sprintf(&temp2[0],"%4.2d", (int)temp);
					//AnsiString NoDecString = &temp2[0];

					edPSUDAQV->Text = temp;
					//edPSUDAQV->Text = NoDecString;
				}
				else
				{
					// Do Nothing
					if (edPSUDAQI->Text == "ReadI")
					{
						char FormatDaqI[10];
						float tempNo2 = StrToInt(RX_String)/1000.0f;
						sprintf(FormatDaqI,"%.4f", tempNo2);
						AnsiString temp3 = FormatDaqI;

						edPSUDAQI->Text = temp3;
						//edPSUDAQI->Text = RX_String;
					}
					else
					{
						// Do Nothing
					} // End If

				} // End If

				//***
				// Ends Message Received
				//***

				} // End If

				// Clear Buffer
				Ri_Ptr = 0;
				memset(RxBuffer2, 0, sizeof(RxBuffer2));
				RX_String= "";

				//edMiscInstruMsg->Text = RX_String;
			}
			else
			{
				RxBuffer2[Ri_Ptr++] = input;
			} // End If

			Count--;

//		} // end of (Recived_Message == false)
		Application->ProcessMessages();

	}  // End While

   //	ShowMessage(String(RxBuffer2[1]));

		//			Memo1->Lines->Add("Plotter msg : " + RX_String);

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Button15Click(TObject *Sender)
{

   Read_PSU_DAQ();

/*	switch(StrToInt(InputBox("Please Enter Menu Option Number",
	"Menu Number\n"
	"0 = Exit\n"
	"1 = Read DaQ Volts\n"
	"2 = Read DaQ Amps\n",
	0)))
	 {
		case 0:
		 {
			// Exit
			break;
		 }
		case 1:
		 {
			PSU_ReadDaQ(true);
			break;
		 }
		case 2:
		 {
			PSU_ReadDaQ(false);
			break;
		 }

		default:
		break;
	 }  // End Switch
*/

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Read_PSU_DAQ()
{
		PSU_ReadDaQ(true);
		Sleep(100);
		// Wait for reply
/*		for (int i = 0; i < 10; i++)
		{
			Sleep(1);

			AnsiString test = edPSUDAQV->Text;
			if (edPSUDAQV->Text == "ReadV")
			{
				// Do Nothing
			}
			else
			{
				break;
			} // End If
		}  // End For
*/
		PSU_ReadDaQ(false);
		// Wait for reply
/*		for (int i = 0; i < 10; i++)
		{
			Sleep(1);
			if (edPSUDAQI->Text == "ReadI")
			{
				// Do Nothing
			}
			else
			{
				break;
			} // End If
		}  // End For
*/
}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Button27Click(TObject *Sender)
{

	Read_eLoad_DAQ();

}

//---------------------------------------------------------------------------
void __fastcall TmainForm::Read_eLoad_DAQ()
{
	edeLoadDAQV->Text = "Read";
	edeLoadDAQI->Text = "Read";
	eLoad_GetVIPower();

}
//---------------------------------------------------------------------------
void __fastcall TmainForm::cbPSU_DaQClick(TObject *Sender)
{

	if (cbPSU_DaQ->Checked)
	{
	   Read_PSU_DAQ();

	   if ((cb_E_LoadDaQ->Checked) && (cbDAQType->ItemIndex == 5))
	   {
			shpDAQConnected->Brush->Color = clLime;
	   }
	   else
	   {
			shpDAQConnected->Brush->Color = clSilver;
	   } // End If

	}
	else
	{
	   //Do Nothing
		shpDAQConnected->Brush->Color = clSilver;
	} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::cb_E_LoadDaQClick(TObject *Sender)
{
	if (cb_E_LoadDaQ->Checked)
	{
		Read_eLoad_DAQ();

	   if ((cbPSU_DaQ->Checked) && (cbDAQType->ItemIndex == 5))
	   {
			shpDAQConnected->Brush->Color = clLime;
	   }
	   else
	   {
			shpDAQConnected->Brush->Color = clSilver;
	   } // End If

	}
	else
	{
		// Do Nothing
		shpDAQConnected->Brush->Color = clSilver;
	} // End If

}
//---------------------------------------------------------------------------

