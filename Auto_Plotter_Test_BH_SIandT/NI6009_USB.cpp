//---------------------------------------------------------------------------

#pragma hdrstop

#include "NI6009_USB.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


//============================================================================
//============================================================================
//============================================================================
//		NI6009 USB CONTROLLER FUNCTION IMPLEMENTATIONS
//============================================================================
//============================================================================
//============================================================================


//---------------------------------------------------------------------------
// Add a line to the USB connection log...
//---------------------------------------------------------------------------
void __fastcall NI6009_USB_Interface::AddToLog(AnsiString line)
{
	MemoMutex->Acquire();
	Memo->Lines->Add(line);
	MemoMutex->Release();
}


NI6009_ret_t __fastcall NI6009_USB_Interface::GetAnalogInputs(double *buff)
{
	long read = 0;
	uint32_t i;
	double data[SAMPLES_TO_READ*4];

	//start the task if no configuration errors, if succesfull return true
	NI6009_ret_t result = DAQmxErrChk (DAQmxStartTask(NI6009_Handle));
	//if task was started
	if(result == NI6009_OK)
	{
		result = DAQmxErrChk (DAQmxReadAnalogF64(NI6009_Handle,-1,10.0,DAQmx_Val_GroupByChannel,&data[0],SAMPLES_TO_READ*4,&read,NULL));
		//if any samples were read.
		if(result == NI6009_OK && read > 0)
		{   //clear buffer
			buff[0] = 0;
			buff[1] = 0;
			buff[2] = 0;
			buff[3] = 0;
			//add all samples
			for(i=0; i<(unsigned long)read; i++){
				buff[0] += data[i];
				buff[1] += data[i+read];
				buff[2] += data[i+(2*read)];
				buff[3] += data[i+(3*read)];
			}
			//calculate average
			buff[0] = buff[0] / read;
        	buff[1] = buff[1] / read;
			buff[2] = buff[2] / read;
			buff[3] = buff[3] / read;
		}
     	//stop task regardless of succes
		DAQmxStopTask(NI6009_Handle);
	}

	return result;
}


//---------------------------------------------------------------------------
// Tell all threads to go and get a new measurement
//---------------------------------------------------------------------------
void __fastcall NI6009_USB_Interface::StartNewMeasurement(void){

	//if the NIDAQ task is available
	if(NI6009_Handle != 0){
		//invalidate current data
		dataReady = false;
		//get new data
		if(GetAnalogInputs(&newValues[0]) != NI6009_OK)
		{
			AddToLog("Error getting new measurement");
			Stop();
		} else {
			curValues[0] = newValues[0];
			curValues[1] = newValues[1];
			curValues[2] = newValues[2];
			curValues[3] = newValues[3];
			dataReady = true;
		}
	}

}

//---------------------------------------------------------------------------
// Ask all threads whether new data is available, if so, return true
//---------------------------------------------------------------------------
bool __fastcall NI6009_USB_Interface::DataAvailable(void){
	 return dataReady;
}

//---------------------------------------------------------------------------
// Get the latest output string of a certain Node
//---------------------------------------------------------------------------
double __fastcall NI6009_USB_Interface::GetOutput(uint8_t channel){
	return curValues[channel];
}

//---------------------------------------------------------------------------
// Return connection state for node
//---------------------------------------------------------------------------
bool __fastcall NI6009_USB_Interface::MeterConnected(uint8_t channel)
{
	   return (NI6009_Handle != 0) ? true : false;
}

//---------------------------------------------------------------------------
// Kill all threads
//---------------------------------------------------------------------------
void __fastcall NI6009_USB_Interface::Stop(void)
{
	DAQmxStopTask(NI6009_Handle);
	DAQmxClearTask(NI6009_Handle);
	NI6009_Handle = 0;
	AddToLog("NI6009 Task Stopped");
}

//---------------------------------------------------------------------------
// Attempt to start DAQ anaog in task. Return true if succesfull
//---------------------------------------------------------------------------
bool __fastcall NI6009_USB_Interface::Start(void)
{

	//create the NI daq task
	NI6009_ret_t result = DAQmxErrChk(DAQmxCreateTask("",&NI6009_Handle));
	//if succesfull configure and start
	if(result == NI6009_OK){
		AddToLog("NI6009 Task Created");
		//configre the NI daq task
		DAQmxCreateAIVoltageChan(NI6009_Handle,"dev2/ai0:3","",DAQmx_Val_Diff,-1.0,1.0,DAQmx_Val_Volts,NULL);
		DAQmxCfgSampClkTiming(NI6009_Handle,"",SAMPLE_RATE,DAQmx_Val_Rising,DAQmx_Val_FiniteSamps,SAMPLES_TO_READ);
	//if unsuccessfull, indicate to user
	} else {
		AddToLog("NI6009 TaskCreate Error");
	}

	//return result
	return (result == NI6009_OK) ? true : false ;

}

void __fastcall NI6009_USB_Interface::DMM_Task(void)
{

}
