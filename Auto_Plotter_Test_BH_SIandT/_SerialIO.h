/***************************************************************************H1*

	  Filename    : SerialIO
      Description : Handles all the serial IO
      Created     : January 1, 2003
      Last Edit   : January 1, 2003

*******************************************************************************
                    Copyright (c) 2003 - Michael Heyns
       This file contains confidential and proprietary information.
      All use, disclosure, and/or reproduction is strictly prohibited.
						   All rights reserved.
*******************************************************************************
******************************************************************************/

#include <vcl.h>
#include <stdio.h>
#include <io.h>
#include <setupapi.h>
#include "Utils.h"


#ifndef _SerialIOH
#define _SerialIOH

	int __fastcall sdWriteSerialCh(HANDLE hCom, char ch);
	int __fastcall sdWriteSerial(HANDLE hCom, char *Str);
	void __fastcall sdWriteSerial(HANDLE hCom, char *Str,int Len);
	int __fastcall sdReadSerial(HANDLE hCom, char *Str, unsigned long max);
	HANDLE __fastcall sdOpenSerialPort(int ComPort, int Baud, int Handshaking, int ReadTimeout, int WriteTimeout);
	void __fastcall sdClosePort(HANDLE hCom);
	int __fastcall sdDataWaiting(HANDLE hCom);
	void __fastcall sdFlushPort(HANDLE hCom);


#endif

/******************************************************************************
                                  End of file
******************************************************************************/

