/*
=======================================================================
 File Name         : Lib_Misc.cpp
 File Version      : 1.0
 Creation Date     : 8 Dec 2014
 Programmed By     : ??
 File Description  : USB Meters

 NOTE
 Contains Miscellaneous Code Examples for Embarcardero C++ XE3 Compiler

 Ver  Date     Name     Comment
 1.0  8/12/14  GC       Release'

=======================================================================}
*/

//---------------------------------------------------------------------------

#pragma hdrstop

#include "Lib_Misc.h"

#include <System.Classes.hpp>
#include <vcl.h>
//#include <fstream>


//---------------------------------------------------------------------------
#pragma package(smart_init)

//----
// Your Code here
//----

// *******************************
// User defined VARIABLES
// *******************************
// eg #define NODE_DEVICE_ID  "Vid_1a86&Pid_e008"
// eg uint8_t NumOfMeters;
// eg const uint8_t NumOfMetersMax = 4;
// *******************************

// *******************************
// User defined TYPES
// *******************************

// *******************************
// User defined CLASSES
// *******************************

// ---------------------------------------------------------------------------
// Test Code
// ---------------------------------------------------------------------------
void __fastcall testLib()
{
	AnsiString result = "";
	uint8_t i;

//		int aa = CompareText(StrGridSeqTest->Cells[ColNumber][0], ColText2Find);


//	TDateTime today = Date();
//	xx = today.CurrentDate();
//  yy = today.CurrentTime();


//   time_t firsttime, secondtime;
//   firsttime = time(NULL);  // Get start time
//   delay(2000);         /* Waits 2 secs */
//   secondtime = time(NULL); // Get stop time
//	double xx = difftime(second,first);
//   printf("The difference is: %f seconds\n",difftime(second,first));

//#include <time.h>
//time_t start = time(0);
//double seconds_since_start = difftime( time(0), start);

// time_t start, end;
//  double diff;
//  time (&start); //useful call

	// delay here!!!

//  time (&end); //useful call
//  diff = difftime(end,start);//this will give you time spent between those two calls.


//	memset(RxBuffer2, 0, sizeof(RxBuffer2));


//char BaudRateStr[10];
//sprintf( BaudRateStr, "%I32u", baudrate );
//sprintf(&inputPowerStr[0],"%.2f W",curMeas.inputPower);
//AnsiString BaudRate = BaudRateStr;


/* try
  {
	xx
	xc = StrToFloat(sValue) * 10000;
	xd = FloatToStr(valueNothread);

  }
  catch (...)
  {

		MessageDlg(	"Error in Sub xx -> Error : " + String(GetLastError()), mtError, TMsgDlgButtons() << mbOK, 0);

  }
*/
	Application->ProcessMessages();

//	UpperCase(PlotCmd[5])

/*
	switch ( cbMetersConnected->ItemIndex )
	{
	case 0 :
	{
		ShowMessage("0 Selected");
		break;
	}
	case 1 : ShowMessage("1 Selected"); break;
	default : ShowMessage("Item Not Found");
	} // End Switch

switch (cbDAQType->ItemIndex)
		{
		case 0:  // USE_NI6009
			{
			//ShowMessage("dff");
			delete USBMM0;
			break;
			}
		case 1:  // USE_NI6210
			{
			delete USBMM1;
			break;
			}
		case 2:  // USE_AGILENT
			{
			delete USBMM2;
			break;
			}
		case 3:  // USE_DMM
			{
			delete USBMM3;
			break;
			}
		case 4:  // USE_MOCK_DMM
			{
			delete USBMM4;
			break;
			}
		default:
			break;
	} // End Switch

/*********
INPUT BOX

	AnsiString NewTitle = InputBox("Keyword NOT Found","Seq Title", "default");

*****
Numbered Menu

	switch(StrToInt(InputBox("Please Enter Menu Option Number",
	"Menu Number\n"
	"0 = Exit\n"
	"1 = ??\n",
	0)))
	 {
		case 0:
		 {
			// Exit
			break;
		 }
		case 1:
		 {
			break;
		 }
		default:
			break;
	 }  // End Switch



	switch(MessageDlg("Yes= Save Grid\nNo = Exit", mtInformation, mbYesNoCancel, 0))
	 {
		case mrYes:
		 {
			//ShowMessage("Yes");
//			SaveGridToFile();
			SaveIniFileData(false);
			ReadIniFile();
			break;
		 }
		case mrNo:
		 {
			break;
		 }
		default:
			break;
	 }  // End Switch

*/

/*
   do
   {
	 if ((sr.Attr & iAttributes) == sr.Attr)
	 {
//       StringGrid1->RowCount = StringGrid1->RowCount + 1;
//	   StringGrid1->Cells[1][StringGrid1->RowCount-1] = sr.Name;
//       StringGrid1->Cells[2][StringGrid1->RowCount-1] = IntToStr(sr.Size);
	 }
   } while (FindNext(sr) == 0); // End Do While

*/

/*

while (I < NumPaletteEntries)
 {
	//Code here
 };  // End While

*/

	for(i = 0 ; i < 10 ; i++)
	{
		// code here

	} // End For

	// Combo Box example
	//cbMetersConnected->Clear();
	//cbMetersConnected->Text = "Meters Connected";
	//cbMetersConnected->AddItem("test", NULL);
	//ShowMessage(cbMetersConnected->Text);
	//cbMetersConnected->ItemIndex


	if (MessageDlg("Message", mtInformation, mbYesNo, 0) == mrYes)
	 {
		ShowMessage("Yes");
	 }
	 else
	 {
		 ShowMessage("No");
	 } // End If

	ShowMessage("test");

	MessageDlgPos("Message", mtConfirmation, mbYesNoCancel, 0, 200, 200, mbYes);

//	return result;
} // End function

// Generate filename based on user input, and already existing files
//---------------------------------------------------------------------------
AnsiString __fastcall CreateFileName2(AnsiString FolderName, AnsiString Filename)
{
// Need to create
// 1) ebFolderName Edit Box
// 2) ebFilename Edit Box
// 	testFile = CreateFileName2("C:\\temp\\","test_");
//	WriteToCSV(testFile,"test");

	//get folder name
	if (FolderName.c_str() != "\\") {
		//ebFolderName->Text +=("\\");
	}
	AnsiString sBaseName = FolderName + Filename + "_";
	AnsiString sFileName = "";
	//increase filename index until not file is found with that name/index
	uint8_t cnt = 0;
	do {
		sFileName = sBaseName + IntToStr(cnt) + ".csv";
		cnt++;
	}
	while (FileExists(sFileName));

	//return result
	return sFileName;
}

//---------------------------------------------------------------------------
// write single line to CSV file
//---------------------------------------------------------------------------
AnsiString __fastcall WriteCSV(AnsiString FileName, AnsiString Data, bool Show_Message)
{
bool CSVFile_OK;
   //Needs #include <stdio.h>
//AnsiString CSVTitle = "Time,Meter1 Data, Meter1 Units, Meter2 Data, Meter2 Units\n";

	bool newFile = (FileExists(FileName)) ? false : true;

	for(int i = 0 ; i < 10 ; i++)
	{
		// code here
		CSVFile_OK = false;
		//try and open existing file to append
		FILE *fileHandle = fopen(FileName.c_str(), "at");

		for (int i = 0; i < 2; i++)
		{
			// Check if Opened
			if (fileHandle == NULL)
			{
				// Failed to Open Try Again
				fclose(fileHandle);
				FILE *fileHandle = fopen(FileName.c_str(), "at");
//				fileHandle = fopen(FileName.c_str(), "wt");
			}
			else
			{
				// Do Nothing
				break;
			}  // End If

//			ShowMessage("Unsuccesful Rety to Open Save CSV File");
//			fclose(fileHandle);

		} // End For

/*		// If failed, try and open file to write
		if (fileHandle == NULL)
		{

			fileHandle = fopen(FileName.c_str(), "wt");

		}
		else
		{
			// Do Nothing
		}  // End If
*/
		//***
		// If file was opened
		//***
		if (fileHandle != NULL){
			//Write Title Option
			if(newFile)
			{
				//fputs("Time,Meter1 Data, Meter1 Units, Meter2 Data, Meter2 Units\n", fileHandle);
				//fputs("textTitle\n", fileHandle);
			} // End If

			//Write CSV line
			fputs(Data.c_str(), fileHandle);

			//close file
			fclose(fileHandle);
			//inform user
			//ShowMessage("Wrote line to CSV file ->" + FileName);
			CSVFile_OK = true;
			return FileName;
//			break;
		}
		else
		{
			//TestLogAdd("Error opening " + ebFilename->Text);
			//ShowMessage("WARNING Error Writing to CSV File");
			if (Show_Message)
			{
				ShowMessage("Error writing to CSV File - Please check if it is Opened");
			}
			else
			{
				 // Do Nothing
			} // End If
			CSVFile_OK = false;

		} // End If

		 Sleep(100);
//		 ShowMessage("Wait");
//		 break;

	} // End For
	 if (CSVFile_OK)
	 {
	 // Do Nothing
	 }
	 else
	 {
		  ShowMessage("Error writing to CSV File - Please check if it is Opened");
	 } // Enf If

	 return "Error";
}

//---------------------------------------------------------------------------
// Read single line from CSV file
//---------------------------------------------------------------------------
AnsiString __fastcall ReadCSV(AnsiString FileName, bool Show_Message)
{
bool CSVFile_OK;
   //Needs #include <stdio.h>
//AnsiString CSVTitle = "Time,Meter1 Data, Meter1 Units, Meter2 Data, Meter2 Units\n";
char CSVstring [256];
FILE * pFile;

   pFile = fopen (FileName.c_str() , "r");
   if (pFile == NULL)
   {
	 perror ("Error opening file");
		 fclose (pFile);
	 return "";
   }
   else
   {
		// Get last Row
		while(fgets(CSVstring, sizeof(CSVstring), pFile))
		{
		  //	buf[strlen(buf)-1]='\0' ;
		   //	printf("%s\n", buf) ;
		//	if(0 == strcmp(buf, "done"))


		//		break ;
		}  // End While
		AnsiString StrResult = AnsiString(CSVstring);

		 fclose (pFile);

		return StrResult;
//	 if ( fgets (mystring , 100 , pFile) != NULL )
//	 {
//		 puts (mystring);
//		 fclose (pFile);
//	 }
//	 else
//	 {
/*		// Do Nothing
		if (Show_Message)
		{
			ShowMessage("File Not Found");
		}
		else
		{
			// Do Nothing
		} // End If

	 } // End IF
*/
   } // End If
/*
pFile = fopen (FileName.c_str() , "r");
char buf[256] ;
while(fgets(buf, sizeof(buf), pFile))
{
  //	buf[strlen(buf)-1]='\0' ;
   //	printf("%s\n", buf) ;
//	if(0 == strcmp(buf, "done"))


//		break ;
}  // End While
*/


/*
char s[256];

ifstream file(FileName.c_str());

AnsiString test;
while ( !file.eof()  )
{
	getline( cin, test);
//	file.getline( FileName.c_str(), test );
		// handle input
}


AnsiString xx = AnsiString(mystring);
*/
//   return 0;



/*

	bool newFile = (FileExists(FileName)) ? false : true;

	for(int i = 0 ; i < 10 ; i++)
	{
		// code here
		CSVFile_OK = false;
		//try and open existing file to append
		FILE *fileHandle = fopen(FileName.c_str(), "at");

		for (int i = 0; i < 2; i++)
		{
			// Check if Opened
			if (fileHandle == NULL)
			{
				// Failed to Open Try Again
				fclose(fileHandle);
				FILE *fileHandle = fopen(FileName.c_str(), "at");
//				fileHandle = fopen(FileName.c_str(), "wt");
			}
			else
			{
				// Do Nothing
				break;
			}  // End If

//			ShowMessage("Unsuccesful Rety to Open Save CSV File");
//			fclose(fileHandle);

		} // End For

		//***
		// If file was opened
		//***
		if (fileHandle != NULL)
		{


			//Read CSV line
			 if ( fgets (FileName.c_str() , 100 , fileHandle) != NULL )
			 {
			   puts (CSVstring);
			 }
			 else
			 {
			   //CSVstring = "";
			 } // End If

			//close file
			fclose(fileHandle);
			//inform user
			//ShowMessage("Wrote line to CSV file ->" + FileName);
			CSVFile_OK = true;
			return FileName;
//			break;
		}
		else
		{
			//TestLogAdd("Error opening " + ebFilename->Text);
			//ShowMessage("WARNING Error Writing to CSV File");
			if (Show_Message)
			{
				ShowMessage("Error reading CSV File - Please check if it exists");
			}
			else
			{
				 // Do Nothing
			} // End If
			CSVFile_OK = false;

		} // End If

		 Sleep(100);
//		 ShowMessage("Wait");
//		 break;


	} // End For


	 if (CSVFile_OK)
	 {
		// Do Nothing
		 return "Error";
	 }
	 else
	 {
		  ShowMessage("Error reading CSV File - Please check if it exists");
		 return "Error";
	 } // Enf If

*/

}


//---------------------------------------------------------------------------
bool __fastcall isNumber( const AnsiString s )
{
  bool IsADigit;

//		int aa = ColText2Find.AnsiCompare(sds); -1 0, 1

//  int sd = AnsiCompareText("t", "s");
  //		int aa = CompareText(StrGridSeqTest->Cells[ColNumber][0], ColText2Find);

//  			int test = AnsiPos(ColText2Find, sds);  // returns 0 is not in string
//			int leng = ColText2Find.Length();

  /*	if (isNumber("232"))
	{
		ShowMessage("digit");
	}
	else
	{
		ShowMessage("non digit");
	}  // End Is
*/

//  AnsiString x("dickheadsix");
// char * f = x.c_str();

 char * f = s.c_str();
// ShowMessage(f[0]);

	// Default as digit unless any char found
	IsADigit = true;

  for (unsigned i = 0; i < strlen(f); ++i)
  {
	//ShowMessage(f[i]);
	if ((isdigit(f[i]) && f[i] != NULL) || (f[i] == '.'))
	{
		// Do Nothing
	}
	else
	{
		// Only One instance to reset
		IsADigit = false;
//	  return 0;
	} // End If

  }  // End For

  // Process result
  if (IsADigit)
  {
	  return 1;
  }
  else
  {
	  return 0;
  } // End If

/*
  for(char c : s)
  {
	if( c == '.' && !hitDecimal ) // 2 '.' in string mean invalid
	  hitDecimal = 1; // first hit here, we forgive and skip
	else if( !isdigit( c ) )
	  return 0 ; // not ., not
  }
  return 1 ;
*/
}

// ---------------------------------------------------------------------------
AnsiString __fastcall Float2NoDecimalString(double value)
{
AnsiString result;
char effStr[4];

	 //AnsiString Volts = edPSUSetVoltage->Text;

//	float VoltSet = StrToFloat(edPSUSetVoltage->Text);
	// Remove decimal Point
	for (int i = 1; i < 20; i++)
	{
		if (fmod(value,1.0) == 0.0)
		{
			break;
		}
		else
		{
			value = value * 10;
		} // End If

	} // End For

	//sprintf(&effStr[0],"%.2f %", edPSUSetVoltage->Text);

	sprintf(&effStr[0],"%04d", (int)value);
	AnsiString NoDecString = &effStr[0];

	result = NoDecString;
	//return result
	return result;
}

// ---------------------------------------------------------------------------
void __fastcall Wait_mS(int WaitTimemS)
{

	if (WaitTimemS <= 0)
	{
		// Do Nothing
	}
	else
	{
		for (int i = 0; i < WaitTimemS; i++)
		{
			Sleep(1);
			Application->ProcessMessages();
		}  // End For

	} // End If

}

// ---------------------------------------------------------------------------
AnsiString __fastcall SplitStringFrom0(AnsiString String2break, int SubtextNumbFrom0, char delimiter)
{

//   ShowMessage(SplitStringFrom0("dsfdfs,gfg",1,','));

	AnsiString splitstring;
	uint32_t lastIndex = 0;
	uint32_t curValue = 0;
	uint32_t i;


	//break up return string into seperate values
	for(int i = 0 ; i < (uint32_t)String2break.Length() + 1 ; i++)
	{

		if((String2break.c_str()[i] == ',') || (String2break.c_str()[i] == '\r') || (String2break.c_str()[i] == '\n') || (String2break.c_str()[i] == NULL))
		{
//			AnsiString subss = String2break.SubString(lastIndex, i - lastIndex);


		 if (SubtextNumbFrom0 == curValue)
		 {
		   // Change index after first comma found
		   if (curValue == 0)
		   {
				splitstring = String2break.SubString(lastIndex,i-lastIndex);
		   }
		   else
		   {
				splitstring = String2break.SubString(lastIndex+2,i-lastIndex-1);

		   } // End If

		   break;

		 }
		 else
		 {
			// Do Nothing
		 } // End If

			lastIndex = i; // New Start point
			curValue++;
		}

	}  // End For

	return splitstring;

}

// ---------------------------------------------------------------------------
float roundoff(float num,int precision)
{
	  int temp=(int )(num*pow(10,precision));
	  int num1=num*pow(10,precision+1);
	  temp*=10;
	  temp+=5;
	  if(num1>=temp)
			  num1+=10;
	  num1/=10;
	  num1*=10;
	  num=num1/pow(10,precision+1);
	  return num;
}
// ---------------------------------------------------------------------------
