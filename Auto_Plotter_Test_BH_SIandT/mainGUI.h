//---------------------------------------------------------------------------

#ifndef mainGUIH
#define mainGUIH
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <StdCtrls.hpp>
#include "AdPort.hpp"
#include "OoMisc.hpp"
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Buttons.hpp>
#include <Vcl.Grids.hpp>

#include <VCLTee.Chart.hpp>
#include <VCLTee.TeEngine.hpp>
#include <VCLTee.TeeProcs.hpp>
#include <VCLTee.Series.hpp>
#include <Vcl.FileCtrl.hpp>
#include <Vcl.Dialogs.hpp>
//---------------------------------------------------------------------------

#if (__BORLANDC__  > 0x0631)
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <Vcl.Menus.hpp>
#endif

#include "USB_meters.h"
#include "NI6009_USB.h"
#include "NI6210_USB.h"
#include "AGILENT_34970A.h"
#include "Mock_DMM.h"

// User includes
#include "XYPlotter.h"

#include "Txapp2.cpp"

// ***
// User definitions
// ***
//definitions for USB data aquisition
//#define USE_NI6009
//#define USE_NI6210
//#define USE_AGILENT
//#define USE_DMM

//#define USB_PSU

//#define USE_MOCK_DMM

#define  UM_RxMESSAGE (WM_USER + 1001)
// ***
// ENDS user definitions
// ***

//***
// TEST STATES
//***
/*
// 1 ````````````````````````````````````````````````````````````````````````````````
case TEST_IDLE_STATE:
// 2 ````````````````````````````````````````````````````````````````````````````````
case TEST_INIT_STATE:
// 3 ````````````````````````````````````````````````````````````````````````````````
case TEST_MOVE_NRT:
// 4 ````````````````````````````````````````````````````````````````````````````````
case TEST_MOVE_STATE:
// 5 ````````````````````````````````````````````````````````````````````````````````
case TEST_ENABLE_PSU_STATE:
// 6 ````````````````````````````````````````````````````````````````````````````````
case TEST_GET_DATA_STATE:
// 7 ````````````````````````````````````````````````````````````````````````````````
case TEST_PROCESS_DATA_STATE:
// 8 ````````````````````````````````````````````````````````````````````````````````
case TEST_PAUSE_STATE:
// 9 ````````````````````````````````````````````````````````````````````````````````
default:
*/

typedef enum
{
	TEST_IDLE_STATE,
	TEST_TIMEOUT_STATE,
	TEST_INIT_STATE,
	TEST_MOVE_NRT,
	TEST_MOVE_STATE,
	TEST_ENABLE_PSU_STATE,
	TEST_GET_DATA_STATE,
	TEST_PROCESS_DATA_STATE,
	TEST_PAUSE_STATE,
} MAIN_test_state_t;
//***
// ENDS TEST STATES
//***

typedef struct
{
	int32_t xSteps;
	int32_t ySteps;
	int32_t sign;
	int32_t curIteration;
	XYPlotter_setpoint_t curSetpoint;
	XYPlotter_setpoint_t curDistance;
	MAIN_test_state_t testState;
	MAIN_test_state_t LastState;			//used by the pause function

	MAIN_test_state_t NRTState;
	bool NRT_Goto;

	AnsiString testFile;
} MAIN_test_admin_t;

#define INPUT_VOLTAGE_INDEX   	(0)
#define INPUT_CURRENT_INDEX 	(1)
#define OUTPUT_VOLTAGE_INDEX 	(2)
#define OUTPUT_CURRENT_INDEX  	(3)

#define CSV_SIGNIFICANT_DIGITS  (6+1) //the +1 is for the '.' character

typedef struct
{
	double values[4]; //contains the raw measurement values (Vin, Iin, Vout, Iout)
	double inputPower;
	double outputPower;
	double efficiency;
	int Qi_Signal_Strength;
	int Qi_Coil_Num_1;
	int Qi_Coil_Num_2;
//	unsigned int	FODA_0_Phase_Buff[29];
//    unsigned int	FODA_90_Phase_Buff[29];
	int RetryCount;

 } MAIN_measurement_t;
//---------------------------------------------------------------------------
class TmainForm : public TForm
{
__published:	// IDE-managed Components

	//GUI Panels
	TPanel *plCallibration;

	//Updated labels for USER info
	TLabel *lbUSBConnected;
	TLabel *lbFilename;
	TLabel *lbDistance;
	TLabel *lbCords;
	TLabel *lbInputPower;
	TLabel *lbOutputPower;
	TLabel *lbEff;
	TLabel *lbQiSignal_strength;


	//Caption / Not updated labels


TLabel *lbQiSignalStrengthCaption;
	TLabel *lbCaptionEff;
	TLabel *lbCaptionInputPower;
	TLabel *lbCaptionOutputPower;



	//System timers
	TTimer *tmMeterUpdate;
	TTimer *tmTest;
	TTimer *tmPlotter;
	TMainMenu *MainMenu1;
	TTimer *tmInit_SM;
	TTimer *tmAcknowledge;
	TTimer *tmPlotter_NRT;
	TTabControl *TabControl1;
	TPanel *Panel2;
	TPanel *Panel3;
	TPanel *Panel4;
	TMenuItem *Help1;
	TMenuItem *About1;
	TButton *btStartTest;
	TButton *btnPause;
	TCheckBox *cbManual_Mode;
	TLabel *lbTestControl;
	TButton *btStopTest;
	TEdit *ebRepeat;
	TLabel *Label6;
	TPanel *Panel5;
	TPanel *plComms;
	TLabel *lbCaptionUSBMeters;
	TLabel *lbCaptionTestLog;
	TLabel *lbCaptionPlotterComms;
	TLabel *Label1;
	TLabel *lbErrorStatus;
	TMemo *memoTestLog;
	TMemo *memoPlotterLog;
	TMemo *memoUSBLog;
	TPanel *Panel7;
//	TLabel *lbUSBConnected;
	TLabel *lbInputVoltage;
	TLabel *lbOutputVoltage;
	TLabel *lbIputCurrent;
	TLabel *lbOutputCurrent;
	TShape *shInputVoltage;
	TShape *shInputCurrent;
	TShape *shOutputVoltage;
	TShape *shOutputCurrent;
	TLabel *Label2;
	TLabel *Label5;
	TEdit *ebInputVoltage;
	TEdit *ebInputCurrent;
	TEdit *ebOutputVoltage;
	TEdit *ebOutputCurrent;
	TButton *btCloseUSB;
	TButton *btOpenUSB;
	TPanel *Panel8;
	TLabel *lbCaptionPlotterControl;
	TLabel *lbRelayControl;
	TButton *btHomePlotter;
	TCheckBox *cbEnablePSUControl;
	TButton *btRelayOff;
	TButton *btRelayOn;
	TPanel *Panel9;
//	TLabel *lbEff;
//	TLabel *lbCaptionEff;
//	TLabel *lbOutputPower;
//	TLabel *lbCaptionOutputPower;
//	TLabel *lbInputPower;
//	TLabel *lbCaptionInputPower;
	TLabel *lblTimeRemaining;
	TLabel *timeRemainlb;
	TPanel *Panel10;
//	TLabel *lbFilename;
	TLabel *lbFolderName;
	TEdit *ebFilename;
	TEdit *ebFolderName;
	TButton *btnProgramClose;
	TListBox *ListBox1;
	TGridPanel *GridPanel1;
//	TPanel *plCallibration;
	TLabel *lbCaptionIinMultiplier;
	TLabel *lbCaptionVinMultiplier;
	TLabel *lbCaptionVoutMultiplier;
	TLabel *lbCaptionIoutMultiplier;
	TLabel *lbCaptionMultipliers;
	TEdit *ebVinMultiplier;
	TEdit *ebIinMultiplier;
	TEdit *ebIoutMultiplier;
	TEdit *ebVoutMultiplier;
	TEdit *ebVinOffset;
	TEdit *ebIinOffset;
	TEdit *ebVoutOffset;
	TEdit *ebIoutOffset;
	TStringGrid *StringGrid1;
	TBitBtn *btnStartSeqGrid;
	TEdit *edRegistryName;
	TLabel *Label10;
//	TLabel *lbQiSignal_strength;
//	TLabel *lbQiSignalStrengthCaption;
	TCheckBox *cbDoNotMoveXY;
	TCheckBox *cbHoleRetry;
	TEdit *ebHoleRetryNo;
	TLabel *Label11;
	TLabel *Label4;
	TLabel *lblRxLoadName;
	TLabel *lbCountDownState;
	TLabel *lblWaitCountdown;
	TLabel *lbCaptionDistance;
//	TLabel *lbDistance;
	TLabel *lbCaptionCords;
	TLabel *lbEffCalcManual;
	TLabel *Label18;
	TChart *Chart1;
	TLineSeries *Series1;
	TCheckBox *cbShowProgChart;
	TButton *btnClearChart;
	TCheckBox *cbPowerSupplyStatus;
	TShape *Shape1;
	TLabel *Label17;
	TCheckBox *cbSaveLog;
	TEdit *edGridColourThreshold;
	TLabel *Label21;
	TComboBox *cbCoilStatus;
	TCheckBox *cbDebug;
	TApdComPort *Qi_Comport;
	TApdComPort *Plotter_Comport;
	TGroupBox *GroupBox1;
	TButton *Button2;
	TEdit *edRxVersion;
	TEdit *edRxComport;
	TLabel *Label22;
	TLabel *Label23;
	TLabel *Label24;
	TEdit *edTxVersion;
	TEdit *edTxBuild;
	TEdit *edRxVers;
	TEdit *edRxBuild;
	TEdit *edRxLoad;
	TEdit *edTestWait;
	TEdit *edRxStep;
	TEdit *edTxModDepth;
	TLabel *Label25;
	TLabel *Label26;
	TLabel *Label27;
	TLabel *Label28;
	TLabel *Label29;
	TLabel *Label30;
	TLabel *Label31;
	TLabel *Label32;
	TLabel *Label33;
	TLabel *Label34;
	TLabel *Label35;
	TLabel *Label36;
	TButton *btUpdateFilename;
	TButton *Button5;
	TEdit *edTxModDepthView;
	TLabel *Label37;
	TLabel *Label19;
	TLabel *Label38;
	TComboBox *cbTxMakeOptions;
	TEdit *edTxNumber;
	TEdit *edRxNumber;
	TComboBox *cbRxMakeOptions;
	TComboBox *cbRxOrientation;
	TComboBox *cbRxZHeight;
	TButton *Button6;
	TLabel *lbRetryNotice;
	TStringGrid *StringGrid2;
	TMenuItem *Help_RegEdit;
	TLabel *Label44;
	TEdit *edTxVDC;
	TApdComPort *PSU_Comport;
	TStringGrid *StrGridSeqTest;
	TRichEdit *rteIniFile;
	TTimer *tmSeqTest2;
	TButton *btnStopSeqGrid;
	TLabel *lbStatusGridSeq;
	TTimer *tmSeqTestWait;
	TApdComPort *PSU_ComportGW;
	TApdComPort *eLoad_Comport;
	TLabel *lbRepeatCountStatus;
	TEdit *edSeqRowCount;
	TLabel *Label51;
	TFileListBox *FileListBox1;
	TComboBox *cbSeqIniFilenames;
	TLabel *lbIniFilename;
	TPanel *pnlManualPlotterCmd;
	TButton *Button9;
	TComboBox *cbPlotterSendCmd;
	TLabel *Label52;
	TPanel *Panel15;
	TShape *shPlotterConnected;
	TLabel *lbPlotterConnected;
	TShape *shpPSUConnected;
	TLabel *Label56;
	TShape *shpDAQConnected;
	TLabel *Label58;
	TShape *shpELoadConnected;
	TLabel *Label57;
	TPanel *Panel16;
	TPanel *Panel1;
	TLabel *TXCommMemoCaption;
	TLabel *Label7;
	TLabel *Label20;
	TMemo *TXCommMemo;
	TButton *btTXOpenComms;
	TButton *btTXCloseComms;
	TCheckBox *cbPSU_ONOff;
	TButton *btScanPower;
	TEdit *edTxOpenedVersion;
	TButton *Button3;
	TEdit *edTxComportNumber;
	TComboBox *cbModDepth;
	TPanel *pnlPlotterControl;
	TLabel *lbStepSize;
	TLabel *lbYSteps;
	TLabel *lbXSteps;
	TLabel *lblPowerUpWait;
	TLabel *lbOffTime;
	TLabel *lbYOffset;
	TLabel *lbXOffset;
	TLabel *Label8;
	TLabel *Label9;
	TLabel *Label13;
	TLabel *Label14;
	TLabel *Label15;
	TLabel *Label16;
	TLabel *Label12;
	TLabel *Label40;
	TLabel *Label41;
	TEdit *ebStepSize;
	TEdit *ebYSteps;
	TEdit *ebOffTime;
	TEdit *ebPowerUpWait;
	TEdit *ebXSteps;
	TEdit *ebYOffset;
	TEdit *ebXOffset;
	TButton *btnHomePlotterSet;
	TBitBtn *btnManGoToTestStart;
	TButton *btnGoToOffset;
	TEdit *ebXSetpointStart;
	TEdit *ebYSetpointStart;
	TEdit *edPlotterComportNo;
	TButton *btnGoToStepEnd;
	TEdit *edXStepmm;
	TEdit *edYStepmm;
	TPanel *Panel12;
	TEdit *edLoadXmm;
	TEdit *edLoadYmm;
	TButton *btnLoadUnload;
	TEdit *edLoadZmm;
	TPanel *pnl3DPlot;
	TLabel *lb3DPSU;
	TLabel *Label45;
	TEdit *edPSU3DPlot;
	TComboBox *cbOrienZRotation;
	TComboBox *cbAngleYRotation;
	TEdit *ed3DZHeight;
	TPanel *Panel14;
	TLabel *Label53;
	TLabel *Label54;
	TLabel *Label55;
	TEdit *edXOffsetStart;
	TEdit *edyOffsetStart;
	TEdit *edXOffsetSteps;
	TEdit *edyOffsetSteps;
	TLabel *Label3;
	TComboBox *cbPlotterType;
	TButton *btnPlotComportOpen;
	TButton *btnPlotComportClose;
	TLabel *Label59;
	TLabel *lblProgramStateIndicator;
	TCheckBox *cbSimPlotting;
	TButton *btnGotToPlotOffset;
	TStringGrid *StringGrid3;
	TPanel *plPSUControl;
	TLabel *lbPSUPower;
	TLabel *Label47;
	TLabel *Label48;
	TLabel *lbPSUAmps;
	TButton *btEnablePSU;
	TButton *btDisablePSU;
	TCheckBox *cbEVK;
	TButton *btnOpenPSUGWPort;
	TEdit *edPSUComport;
	TEdit *edPSUSetVoltage;
	TEdit *edPSUAmps;
	TPanel *Panel13;
	TLabel *Label46;
	TLabel *Label49;
	TLabel *Label50;
	TButton *Button7;
	TButton *btne_Load;
	TCheckBox *cbe_LoadSelect;
	TEdit *edeLoadPort;
	TButton *Button8;
	TComboBox *cbELoadCommands;
	TEdit *edELoadValue;
	TPanel *Panel6;
	TComboBox *cbDAQType;
	TLabel *Label60;
	TButton *Button1;
	TComboBox *cbPlotAreaOptions;
	TMenuItem *SoftwarePCRequirements1;
	TComboBox *cbPSUOptions;
	TComboBox *cbeLoadOptions;
	TPanel *Panel17;
	TLabel *lbTxConnectedStatus;
	TShape *shpTxConnected;
	TRadioGroup *rgLoad;
	TGroupBox *GroupBox4;
	TMemo *meSeqIniFile;
	TButton *btnReadSeqIni;
	TButton *btnSaveSeqIni;
	TCheckBox *cbRunPSU_OnOff;
	TComboBox *cbGridTypeDisplay;
	TLabel *lbControlMessage;
	TApdComPort *Gen_Comport;
	TEdit *edVinDaqAddr;
	TEdit *edIinDaqAddr;
	TEdit *edVoutDaqAddr;
	TEdit *edIoutDaqAddr;
	TLabel *Label63;
	TLabel *Label64;
	TLabel *Label65;
	TLabel *Label66;
	TLabel *Label67;
	TShape *shpMISDataConnected;
	TLabel *lbMisc;
	TGroupBox *GroupBox7;
	TGroupBox *grMiscInstruments;
	TLabel *Label61;
	TLabel *Label62;
	TLabel *Label68;
	TLabel *Label85;
	TButton *btnLCRGW;
	TEdit *edMiscIntruComportNo;
	TButton *Button10;
	TMemo *Memo1;
	TEdit *edBaudGenSerial;
	TButton *Button11;
	TComboBox *cbMiscInstrumentCmds;
	TGroupBox *GroupBox5;
	TLabel *Label74;
	TLabel *Label75;
	TLabel *Label76;
	TLabel *Label77;
	TLabel *Label78;
	TLabel *Label79;
	TLabel *Label80;
	TLabel *Label81;
	TLabel *Label82;
	TLabel *Label83;
	TEdit *edTestFreqHz1of10;
	TEdit *edTestFreqHz2of10;
	TEdit *edTestFreqHz3of10;
	TEdit *edTestFreqHz4of10;
	TEdit *edTestFreqHz5of10;
	TEdit *edTestFreqHz6of10;
	TEdit *edTestFreqHz7of10;
	TEdit *edTestFreqHz8of10;
	TEdit *edTestFreqHz9of10;
	TEdit *edTestFreqHz10of10;
	TGroupBox *GroupBox6;
	TLabel *Label70;
	TLabel *Label71;
	TLabel *Label73;
	TLabel *Label72;
	TLabel *Label84;
	TEdit *edGWLCR_Mode;
	TEdit *edGWLCR_Meas;
	TEdit *edGWLCR_Level;
	TEdit *edGWLCR_Comp;
	TEdit *edGWLCR_cct;
	TButton *btRunMiscInstruTtest;
	TButton *btMiscInstruCal;
	TEdit *edMiscInstruMsg;
	TComboBox *cbMiscInstrumentType;
	TLabel *lbStatusGWLCR;
	TEdit *edLCRfreq1_Data1;
	TEdit *edLCRfreq1_Data2;
	TLabel *Label86;
	TLabel *Label87;
	TEdit *edLCRfreq2_Data1;
	TEdit *edLCRfreq2_Data2;
	TEdit *edLCRfreq3_Data1;
	TEdit *edLCRfreq4_Data1;
	TEdit *edLCRfreq5_Data1;
	TEdit *edLCRfreq6_Data1;
	TEdit *edLCRfreq7_Data1;
	TEdit *edLCRfreq8_Data1;
	TEdit *edLCRfreq9_Data1;
	TEdit *edLCRfreq10_Data1;
	TEdit *edLCRfreq3_Data2;
	TEdit *edLCRfreq4_Data2;
	TEdit *edLCRfreq5_Data2;
	TEdit *edLCRfreq6_Data2;
	TEdit *edLCRfreq7_Data2;
	TEdit *edLCRfreq8_Data2;
	TEdit *edLCRfreq9_Data2;
	TEdit *edLCRfreq10_Data2;
	TGroupBox *TiEvmLDC1614;
	TCheckBox *cbTiEvmLDC_on_Off;
	TLabel *Label88;
	TButton *Button4;
	TEdit *edTiLCDEvmFileAddress;
	TButton *Button12;
	TLabel *Label89;
	TEdit *edTiEVM_L0;
	TEdit *edTiEVM_L1;
	TEdit *edTiEVM_L2;
	TEdit *edTiEVM_L3;
	TEdit *edTiEVM_LFreq0;
	TEdit *edTiEVM_LFreq1;
	TEdit *edTiEVM_LFreq2;
	TEdit *edTiEVM_LFreq3;
	TLabel *Label90;
	TLabel *Label91;
	TLabel *Label92;
	TLabel *Label93;
	TLabel *Label94;
	TLabel *Label95;
	TLabel *lbTiEVMStatus;
	TButton *Button13;
	TEdit *edGWLCR_Speed;
	TLabel *Label96;
	TButton *Button14;
	TEdit *edLCR_EstTestTime;
	TLabel *Label97;
	TLabel *Label98;
	TEdit *edTiEVM_EstTestTime;
	TLabel *Label99;
	TEdit *ebFilenameSeqAppend;
	TButton *Button15;
	TApdComPort *Arduino_FOD;
	TGroupBox *GroupBox8;
	TButton *Button16;
	TLabel *Label69;
	TEdit *edBaudArdFoDSerialNo;
	TLabel *Label100;
	TEdit *edBaudArdFoDSerialBd;
	TPanel *Panel18;
	TComboBox *cbPlotterType2;
	TButton *btnPlotComportOpen2;
	TButton *btnPlotComportClose2;
	TPanel *Panel19;
	TLabel *Label101;
	TLabel *Label102;
	TEdit *edPlotterComportNo2;
	TButton *btnHomePlotterSet2;
	TLabel *Label103;
	TEdit *edPSU3DPlot2;
	TComboBox *cbOrienZRotation2;
	TComboBox *cbAngleYRotation2;
	TLabel *Label104;
	TEdit *ed3DZHeight2;
	TLabel *Label105;
	TLabel *Label106;
	TEdit *ebOffTime2;
	TLabel *Label107;
	TEdit *ebXSetpointStart2;
	TLabel *Label108;
	TEdit *ebXOffset2;
	TLabel *Label109;
	TEdit *ebYSetpointStart2;
	TLabel *Label110;
	TEdit *ebYOffset2;
	TButton *btnManGoToTestStart2;
	TButton *btGotoXY2;
	TLabel *Label111;
	TLabel *Label112;
	TLabel *Label113;
	TEdit *ebXSteps2;
	TEdit *ebYSteps2;
	TEdit *ebStepSize2;
	TEdit *edXStepmm2;
	TLabel *Label114;
	TEdit *edYStepmm2;
	TLabel *Label115;
	TPanel *Panel20;
	TEdit *edLoadXmm2;
	TLabel *Label118;
	TLabel *Label117;
	TLabel *Label116;
	TEdit *edLoadYmm2;
	TEdit *edLoadZmm2;
	TButton *btnLoadUnload2;
	TPanel *Panel21;
	TBitBtn *btnGoToStepEnd2;
	TLabel *Label119;
	TEdit *edXOffsetStart2;
	TEdit *edXOffsetSteps2;
	TEdit *edyOffsetStart2;
	TEdit *edyOffsetSteps2;
	TLabel *Label120;
	TLabel *Label121;
	TButton *btnGotToPlotOffset2;
	TEdit *ebPowerUpWait2;
	TLabel *Label122;
	TLabel *Label123;
	TLabel *Label124;
	TButton *Button17;
	TComboBox *cbFoDArduinoCommands;
	TButton *Button18;
	TComboBox *cbFodResults;
	TLabel *lbFoDCoilStatus;
	TMenuItem *GUI1;
	TMenuItem *CloseGUI1;
	TMenuItem *FormSize1;
	TMenuItem *Big_View;
	TEdit *EdWaitBeforeLCRRead;
	TLabel *Label125;
	TButton *Button19;
	TLabel *Label126;
	TLabel *Label127;
	TLabel *Label128;
	TGroupBox *GroupBox9;
	TButton *Button20;
	TEdit *edNudgeAmount;
	TLabel *Label129;
	TButton *Button21;
	TButton *Button22;
	TButton *Button23;
	TButton *Button24;
	TButton *Button25;
	TEdit *edZheightNudge;
	TLabel *Label130;
	TEdit *edPSUDAQV;
	TButton *Button26;
	TGroupBox *GroupBox2;
	TLabel *Label39;
	TLabel *Label43;
	TLabel *Label42;
	TEdit *edRxVoltReScanLimit;
	TEdit *edSigStrenReScanLimit;
	TEdit *edRxVoltFinalTestLimit;
	TButton *Button27;
	TEdit *edeLoadDAQV;
	TEdit *edPSUDAQI;
	TEdit *edeLoadDAQI;
	TLabel *Label131;
	TLabel *Label132;
	TCheckBox *cbPSU_DaQ;
	TCheckBox *cb_E_LoadDaQ; //28/7/15

	//GUI function prototypes
	void __fastcall btEnablePSUClick(TObject *Sender);
	void __fastcall btDisablePSUClick(TObject *Sender);
	void __fastcall ebXStepsChange(TObject *Sender);
	void __fastcall ebYStepsChange(TObject *Sender);
	void __fastcall ebStepSizeChange(TObject *Sender);
	void __fastcall btHomePlotterClick(TObject *Sender);
	void __fastcall btOpenUSBClick(TObject *Sender);
	void __fastcall btCloseUSBClick(TObject *Sender);
	void __fastcall btStartTestClick(TObject *Sender);
	void __fastcall btStopTestClick(TObject *Sender);
	void __fastcall ebXOffsetChange(TObject *Sender);
	void __fastcall ebYOffsetChange(TObject *Sender);
	void __fastcall btGotoXYClick(TObject *Sender);
	void __fastcall tmMeterUpdateTimer(TObject *Sender);
	void __fastcall tmTestTimer(TObject *Sender);
	void __fastcall ebFolderNameChange(TObject *Sender);
	void __fastcall ebFilenameChange(TObject *Sender);
	void __fastcall ebPowerUpWaitChange(TObject *Sender);
	void __fastcall ebOffTimeChange(TObject *Sender);
	void __fastcall tmPlotterTimer(TObject *Sender);
	void __fastcall ebVinMultiplierChange(TObject *Sender);
	void __fastcall ebIinMultiplierChange(TObject *Sender);
	void __fastcall ebVoutMultiplierChange(TObject *Sender);
	void __fastcall ebIoutMultiplierChange(TObject *Sender);
	void __fastcall ebVinOffsetChange(TObject *Sender);
	void __fastcall ebIinOffsetChange(TObject *Sender);
	void __fastcall ebVoutOffsetChange(TObject *Sender);
	void __fastcall ebIoutOffsetChange(TObject *Sender);
	void __fastcall btTXOpenCommsClick(TObject *Sender);
	void __fastcall btTXCloseCommsClick(TObject *Sender);
	void __fastcall btRelayOnClick(TObject *Sender);
	void __fastcall btRelayOffClick(TObject *Sender);
	void __fastcall btnPauseClick(TObject *Sender);
	void __fastcall CloseProgram1Click(TObject *Sender);
	void __fastcall cbEnablePSUControlClick(TObject *Sender);
	void __fastcall tmInit_SMTimer(TObject *Sender);
	void __fastcall tmAcknowledgeTimer(TObject *Sender);
	void __fastcall cbManual_ModeClick(TObject *Sender);
	void __fastcall memoPlotterLogChange(TObject *Sender);
	void __fastcall memoUSBLogChange(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall btnPlotComportOpenClick(TObject *Sender);
	void __fastcall rgLoadClick(TObject *Sender);
	void __fastcall tmPlotter_NRTTimer(TObject *Sender);
	void __fastcall TabControl1Change(TObject *Sender);
	void __fastcall About1Click(TObject *Sender);
 //	void __fastcall dsgsgs1Click(TObject *Sender);
	void __fastcall btnPlotComportCloseClick(TObject *Sender);
	void __fastcall btnProgramCloseClick(TObject *Sender);
	void __fastcall btnManGoToTestStartClick(TObject *Sender);
	void __fastcall btnStartSeqGridClick(TObject *Sender);
	void __fastcall ebXSetpointStartChange(TObject *Sender);
	void __fastcall ebYSetpointStartChange(TObject *Sender);
//	void __fastcall btnChartProgClick(TObject *Sender);
	void __fastcall cbShowProgChartClick(TObject *Sender);
	void __fastcall btnClearChartClick(TObject *Sender);
	void __fastcall cbPowerSupplyStatusClick(TObject *Sender);

	void __fastcall Qi_ComportTriggerAvail(TObject *CP, WORD Count);         //28/7/15 Same as ->Qi_ComportRxChar
	void __fastcall Plotter_ComportTriggerAvail(TObject *CP, WORD Count);

	void __fastcall cbPSU_ONOffClick(TObject *Sender);
	void __fastcall btScanPowerClick(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall cbModDepthChange(TObject *Sender);
	void __fastcall StringGrid1DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State);
	void __fastcall edGridColourThresholdChange(TObject *Sender);
	void __fastcall btRunMiscInstruTtestClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall btUpdateFilenameClick(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall cbHoleRetryClick(TObject *Sender);
	void __fastcall btnGoToStepEndClick(TObject *Sender);
	void __fastcall StringGrid2DrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
		  TGridDrawState State);
	void __fastcall Big_ViewClick(TObject *Sender);
	void __fastcall lbControlMessageClick(TObject *Sender);
	void __fastcall btnLoadUnloadClick(TObject *Sender);
	void __fastcall edRxVoltReScanLimitChange(TObject *Sender);
	void __fastcall edSigStrenReScanLimitChange(TObject *Sender);
	void __fastcall edRxVoltFinalTestLimitChange(TObject *Sender);
	void __fastcall edLoadXmmChange(TObject *Sender);
	void __fastcall edLoadYmmChange(TObject *Sender);
	void __fastcall Help_RegEditClick(TObject *Sender);
	void __fastcall cbPlotterTypeChange(TObject *Sender);
	void __fastcall ed3DZHeightChange(TObject *Sender);
	void __fastcall cbOrienZRotationChange(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall StrGridSeqTestDblClick(TObject *Sender);
	void __fastcall rteIniFileDblClick(TObject *Sender);
	void __fastcall btnStopSeqGridClick(TObject *Sender);
	void __fastcall tmSeqTestWaitTimer(TObject *Sender);
	void __fastcall btnOpenPSUGWPortClick(TObject *Sender);
	void __fastcall cbEVKClick(TObject *Sender);
	void __fastcall edPSUAmpsDblClick(TObject *Sender);
	void __fastcall cbe_LoadSelectClick(TObject *Sender);
	void __fastcall btne_LoadClick(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall eLoad_ComportTriggerAvail(TObject *CP, WORD Count);
	void __fastcall cbELoadCommandsClick(TObject *Sender);
	void __fastcall StrGridSeqTestClick(TObject *Sender);
	void __fastcall StrGridSeqTestDrawCell(TObject *Sender, int ACol, int ARow, TRect &Rect,
          TGridDrawState State);
	void __fastcall edSeqRowCountExit(TObject *Sender);
	void __fastcall FileListBox1DblClick(TObject *Sender);
	void __fastcall cbSeqIniFilenamesDblClick(TObject *Sender);
	void __fastcall StringGrid1DblClick(TObject *Sender);
	void __fastcall StringGrid2DblClick(TObject *Sender);
	void __fastcall tmSeqTest2Timer(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall btnGotToPlotOffsetClick(TObject *Sender);
	void __fastcall StringGrid3DblClick(TObject *Sender);
	void __fastcall cbDAQTypeChange(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall SoftwarePCRequirements1Click(TObject *Sender);
	void __fastcall cbPSUOptionsChange(TObject *Sender);
	void __fastcall cbeLoadOptionsChange(TObject *Sender);
	void __fastcall edPSUSetVoltageDblClick(TObject *Sender);
	void __fastcall edELoadValueDblClick(TObject *Sender);
	void __fastcall btnLCRGWClick(TObject *Sender);
	void __fastcall btnReadSeqIniClick(TObject *Sender);
	void __fastcall btnSaveSeqIniClick(TObject *Sender);
	void __fastcall StrGridSeqTestSelectCell(TObject *Sender, int ACol, int ARow, bool &CanSelect);
	void __fastcall cbRunPSU_OnOffClick(TObject *Sender);
	void __fastcall cbGridTypeDisplayChange(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Gen_ComportTriggerAvail(TObject *CP, WORD Count);
	void __fastcall Button11Click(TObject *Sender);
	void __fastcall Gen_ComportPortClose(TObject *Sender);
	void __fastcall cbMiscInstrumentCmdsDblClick(TObject *Sender);
	void __fastcall cbMiscInstrumentTypeChange(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall shPlotterConnectedMouseActivate(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y, int HitTest, TMouseActivate &MouseActivate);
	void __fastcall btMiscInstruCalClick(TObject *Sender);
	void __fastcall cbTiEvmLDC_on_OffClick(TObject *Sender);
	void __fastcall Button12Click(TObject *Sender);
	void __fastcall Button13Click(TObject *Sender);
	void __fastcall Button14Click(TObject *Sender);
	void __fastcall StrGridSeqTestExit(TObject *Sender);
	void __fastcall Button16Click(TObject *Sender);
	void __fastcall cbPlotterType2Change(TObject *Sender);
	void __fastcall Button17Click(TObject *Sender);
	void __fastcall btnPlotComportOpen2Click(TObject *Sender);
	void __fastcall btnPlotComportClose2Click(TObject *Sender);
	void __fastcall cbOrienZRotation2Change(TObject *Sender);
	void __fastcall btnHomePlotterSet2Click(TObject *Sender);
	void __fastcall ebPowerUpWait2Change(TObject *Sender);
	void __fastcall ebOffTime2Change(TObject *Sender);
	void __fastcall ebXSetpointStart2Change(TObject *Sender);
	void __fastcall ebXOffset2Change(TObject *Sender);
	void __fastcall ebYSetpointStart2Change(TObject *Sender);
	void __fastcall ebYOffset2Change(TObject *Sender);
	void __fastcall btnManGoToTestStart2Click(TObject *Sender);
	void __fastcall ebXSteps2Change(TObject *Sender);
	void __fastcall ebYSteps2Change(TObject *Sender);
	void __fastcall ebStepSize2Change(TObject *Sender);
	void __fastcall edLoadXmm2Change(TObject *Sender);
	void __fastcall edLoadYmm2Change(TObject *Sender);
	void __fastcall btnLoadUnload2Click(TObject *Sender);
	void __fastcall btnGotToPlotOffset2Click(TObject *Sender);
	void __fastcall btGotoXY2Click(TObject *Sender);
	void __fastcall btnGoToStepEnd2Click(TObject *Sender);
	void __fastcall Button18Click(TObject *Sender);
	void __fastcall CloseGUI1Click(TObject *Sender);
	void __fastcall BClick(TObject *Sender);
	void __fastcall Button19Click(TObject *Sender);
	void __fastcall ed3DZHeight2Change(TObject *Sender);
	void __fastcall edLoadZmm2Change(TObject *Sender);
	void __fastcall Button20Click(TObject *Sender);
	void __fastcall Button23Click(TObject *Sender);
	void __fastcall Button22Click(TObject *Sender);
	void __fastcall Button21Click(TObject *Sender);
	void __fastcall Button24Click(TObject *Sender);
	void __fastcall Button25Click(TObject *Sender);
	void __fastcall Button26Click(TObject *Sender);
	void __fastcall PSU_ComportGWTriggerAvail(TObject *CP, WORD Count);
	void __fastcall Button15Click(TObject *Sender);
	void __fastcall Button27Click(TObject *Sender);
	void __fastcall cbPSU_DaQClick(TObject *Sender);
	void __fastcall cb_E_LoadDaQClick(TObject *Sender);



//	void __fastcall FileListBox1Change(TObject *Sender);
//	void __fastcall Panel10Click(TObject *Sender);    //28/7/15  Not appear to be called


private:	// User declarations


	unsigned int Plotter_Number;
    AnsiString sREG;

	//member variables
	XYPlotter *Plotter;
	MAIN_test_admin_t testAdmin;




//#ifdef USE_NI6009
	NI6009_USB_Interface *USBMM0;
//#endif

//#ifdef USE_NI6210
	NI6210_USB_Interface *USBMM1;
//#endif

//#ifdef USE_AGILENT
	AGILENT_Interface *USBMM2;
//#endif

//#ifdef USE_DMM
	USBMM_Controller *USBMM3;
//#endif

//#ifdef USE_MOCK_DMM
	MOCK_Interface *USBMM4;
//#endif



	MAIN_measurement_t curMeas;
//#ifdef USB_PSU
	HANDLE PSUComPort;
//#endif
//	HANDLE PlotterComPort;
//	HANDLE TXComPort;
	bool measurementRunning;
	bool PauseState;
	AnsiString	sDQA_Address_Name;


	//opens a GUI to allow user to select com-port
	HANDLE __fastcall UserSelectComport(AnsiString dialogLabel, AnsiString regGroup, uint32_t baudrate);  //28/7/15
	void __fastcall UserSelectComport(TApdComPort *pComport,AnsiString dialogLabel, AnsiString regGroup, uint32_t baudrate);
	int __fastcall UserSelectComport2(AnsiString dialogLabel, AnsiString regGroup, uint32_t baudrate);

	void __fastcall updatePosLabels(TObject *Sender);
	void __fastcall TestLogAdd(AnsiString line);
	void __fastcall USBLogAdd(AnsiString line);
	void __fastcall NextSetpoint();
	void __fastcall ResetTestCoordinates();
	void __fastcall ResetTestCoordinates2();

	AnsiString __fastcall CreateFileName(void);

	bool __fastcall WriteToCSV(AnsiString FileName, AnsiString Data);
	AnsiString __fastcall GenerateCSVLine(MAIN_measurement_t meas);

	void __fastcall updateEfficiency(void);
	void __fastcall updateEfficiency2(void);

	void __fastcall TmainForm::Qi_ComportRxChar(int Count, uint8_t * pData);
	void __fastcall TmainForm::ProcessRxMessage( void );
	void __fastcall TmainForm::OutPutQ( AnsiString sOutput );
	void __fastcall TmainForm::Reset_GUI(void );
	void __fastcall TmainForm::Update_Q_Message_Config(void);
	void __fastcall TmainForm::Transmit_Message_N( char * MesPtr, unsigned int Len );
	void __fastcall TmainForm::Transmit_Message( char * MesPtr );
	void __fastcall TmainForm::StartTransmitMessage( void );

	void __fastcall TmainForm::UpdateDAQFormDisplay();

	void __fastcall TmainForm::ShowTabPanel(int PanelNumb);

	void __fastcall TmainForm::InitPlotGrid(void);
//28/7/15
	void __fastcall TmainForm::Start_Scan_n_Power(void);

	void __fastcall TmainForm::RxMessage( TMessage & Message );

	 void __fastcall TmainForm::PSU_ON();
	 void __fastcall TmainForm::PSU_OFF();

	BEGIN_MESSAGE_MAP
		MESSAGE_HANDLER(UM_RxMESSAGE,TMessage,RxMessage)
	END_MESSAGE_MAP(TForm)
//28/7/15 end

	bool __fastcall TmainForm::StartScanPowerONMain();
	bool __fastcall TmainForm::StartScanPowerOFFMain();

	void __fastcall TmainForm::SetModDepthOptions();

	void __fastcall TmainForm::ColourGridCellGrn(int cellx, int celly, int GridNo);

	void __fastcall TmainForm::SetTxOpMode();
	void __fastcall TmainForm::SetModDepth();
	void __fastcall TmainForm::SendGetVersion();
	void __fastcall TmainForm::SendScanAndPowerCmd();

	void __fastcall TmainForm::SetTxManualMode();
	void __fastcall TmainForm::SetTxAutoMode();
	void __fastcall TmainForm::InitTxAndRxMakeOptions();

	void __fastcall TmainForm::enLoadRelay3D(bool state,int iLoad);
	void __fastcall TmainForm::enRefRelay3D(bool state); //true = on
	void __fastcall TmainForm::enPowerRelay3D(bool state); //true = on

	void __fastcall TmainForm::CloseAllOpenComports();

	void __fastcall TmainForm::ShowPlotGrid();
	void __fastcall TmainForm::ShowSeqGrid();

	void __fastcall TmainForm::RunSequenceSteps();
	void __fastcall TmainForm::RunSequencer2();
	void __fastcall TmainForm::RunSequencer3();

	void __fastcall TmainForm::RunPlotterSequence();

	void __fastcall TmainForm::TestStateMachine1();
	void __fastcall TmainForm::TestStateMachine2();

	void __fastcall TmainForm::StartSeqTest();

	void __fastcall TmainForm::SetFormSize();

	bool __fastcall TmainForm::SetSeqData2Form(int SeqGridRowNo);
	void __fastcall TmainForm::SetSeqNextState();

	void __fastcall TmainForm::EffTest_TEST_INIT_STATE();

	void __fastcall TmainForm::SetPSUVoltAndAmps();



	//***
	// 3D Plotter Code
	//***
	void __fastcall TmainForm::RunPlotSequencer();
	void __fastcall TmainForm::ClearEff_Sig_Grids();

	void __fastcall TmainForm::RunSIMULATION(int ColPoint, int RowPoint, int RowGridPoint, int SeqPoint);
	//void __fastcall TmainForm::RunPlotterSequence2(int ColPoint, int RowPoint, int RowGridPoint, int SeqPoint);
	void __fastcall TmainForm::RunPlotterSequence2(int ColPoint, int RowPoint, int RowGridPoint, float zHeight,  int SeqPoint, bool FirstRowData);

	void __fastcall TmainForm::MovePlotterToPointMAIN(int ColPoint, int RowPoint, int RowGridPoint, float zHeight,  int SeqPoint);
	void __fastcall TmainForm::MovePlotterToPoint(AnsiString xpoint, AnsiString ypoint, AnsiString zpoint, AnsiString speed);

	bool __fastcall TmainForm::PlotterON();
	bool __fastcall TmainForm::PSUON();
	bool __fastcall TmainForm::ELoadON();
	bool __fastcall TmainForm::TxON();
	bool __fastcall TmainForm::DAQON();

	void __fastcall TmainForm::InitAndHomePlotter();

	// Seq Timer ONE at a time only Uses Seq Timer ticks
	void __fastcall TmainForm::TimerSequential(int timedelay);

	// Global Messages
	void __fastcall TmainForm::MessagesGlobal(int MessNo);

	void __fastcall TmainForm::StopALLTimers();

	void __fastcall TmainForm::SaveGridData2CSV(int GridRow, bool TitleOnly);

	bool __fastcall TmainForm::WriteToCSVData(AnsiString FileName, AnsiString Data, bool ShowErrorMessage);

	AnsiString __fastcall TmainForm::GenerateCSVLine2(MAIN_measurement_t meas);

	void __fastcall TmainForm::InitDataResultsGrid(void);

	void __fastcall TmainForm::WriteResults2Grid(int ColPoint, int RowPoint, int RowGridPoint, int zHeight,  int SeqPoint);
	//void __fastcall TmainForm::WriteResults2SeqGrid(int ColPoint, int RowPoint, int RowGridPoint, int zHeight,  int SeqPoint);
	void __fastcall TmainForm::WriteResults2SeqGrid(int SeqRowNo, int ColPoint, int RowPoint, int RowGridPoint, int zHeight,  int SeqPoint);

	void __fastcall TmainForm::Init_cbDAQTypes();
	//void __fastcall TmainForm::ReadDaQDataValues();
	void __fastcall TmainForm::ReadDaQDataValuesTimer();
	void __fastcall TmainForm::ReadDaQDataValuesSeq();


	void __fastcall TmainForm::StopUSBMM();
	void __fastcall TmainForm::DeleteUSBMM();
	void __fastcall TmainForm::StartUSBMM();
	void __fastcall TmainForm::StartMeasUSBMM();
	bool __fastcall TmainForm::DataAvaiUSBMM();
	bool __fastcall TmainForm::MeterConnUSBMM(uint8_t curMeter);
	float __fastcall TmainForm::GetOutputUSBMM(uint8_t curMeter);
	void __fastcall TmainForm::DMM_TaskUSBMM();

	void __fastcall TmainForm::updateEfficiencySeqGrid2();

	//void __fastcall TmainForm::TestTimeRemaining();
	void __fastcall TmainForm::TestTimeRemaining(float timeperPlot);


	void __fastcall TmainForm::Init_cbPlotAreaOptions();

	void __fastcall TmainForm::Init_cbPSUOptions();
	void __fastcall TmainForm::Init_cbeLoadOptions();

	int __fastcall TmainForm::GetSeqGridColNofText(AnsiString ColText2Find);

	void __fastcall TmainForm::SaveSeqIniFileData();
	void __fastcall TmainForm::ReadSeqIniFileData();

	void __fastcall TmainForm::CheckSeqGridTitles();

    void __fastcall TmainForm::Set_eLoadValue();
	void __fastcall TmainForm::SaveDEFAULTSeqIniFileData();

	void __fastcall TmainForm::CheckSeqKeywords(void);
	boolean __fastcall TmainForm::SeqSeqKeywordLookup(AnsiString WordtoCheck);
	void __fastcall TmainForm::CreateSeqSeqKeywordList(void);
	AnsiString __fastcall TmainForm::GenerateKeywordTextListStr();

	bool __fastcall TmainForm::OverallSeqSeqKeywordCheck(void);

	void __fastcall TmainForm::ShowGridKeywordTitleDataOptions(int ColNumber);
	void __fastcall TmainForm::InitGridDataDisplayOption(void);
	void __fastcall TmainForm::UpdateDataGrid(int DataDropTypeNumb);

	void __fastcall TmainForm::ClearDataGridArray();
	void __fastcall TmainForm::WriteResult2PlotGrid(int ColPoint, int RowPoint, int RowGridPoint, int SeqPoint);

	void __fastcall TmainForm::SerialSend(AnsiString msg2Send);

	void __fastcall TmainForm::RunMiscInstrumentTest(bool manControl);
	void __fastcall TmainForm::RunMiscInstrumentTest_Ti_LDC(void);

	bool __fastcall TmainForm::SetUpForm2SeqData(int SeqGridRowNo);

	void __fastcall TmainForm::WriteOneGridPointTiLDC(int GridTotalRow_y, int Coli, int Rowi);
	void __fastcall TmainForm::WriteOneGridPointGWLCR(int GridTotalRow_y, int Coli, int Rowi);


	void __fastcall TmainForm::AppendMiscResult2PlotGrid(int ColPoint, int RowPoint, int RowGridPoint, int SeqPoint);

	void __fastcall TmainForm::AppendGridDataDisplayOption(void);

	int __fastcall TmainForm::GetGridTypeDropDownIndex(AnsiString DropDownLookUpString);

	void __fastcall TmainForm::SaveEffPlotIniFileData();
	void __fastcall TmainForm::SaveDEFAULTEffPlotIniFileData();
	void __fastcall TmainForm::ReadEffPlotIniFileData();

	void __fastcall TmainForm::MiscLabelUpdate(void);
	void __fastcall TmainForm::Init_cbFoDArduinoCommandsOptions();
	void __fastcall TmainForm::Init_cbFoDArduinoCodes();
	void __fastcall TmainForm::LCR_ReadData(void);
	void __fastcall TmainForm::SetFoDCoilResults(int CoilNumber);

	AnsiString __fastcall TmainForm::AppendCSVFileFoDData(bool PutTitles);

	void __fastcall TmainForm::SendCmdFoDArduino(AnsiString Text2Send);
	void __fastcall TmainForm::RunFoDCoilTest(void);

	void __fastcall TmainForm::CloseGUI();

	void __fastcall TmainForm::PSU_ReadDaQ(bool V_else_I);

	void __fastcall TmainForm::eLoad_GetVIPower();

	void __fastcall TmainForm::Read_PSU_DAQ();
	void __fastcall TmainForm::Read_eLoad_DAQ();


	//***
	// Ends 3D Plotter Code
	//***

	// ***
	// MOA
	// ***
TCustomIniFile* __fastcall TmainForm::OpenIniFileInstance();
	void __fastcall TmainForm::SeqGrid_Init();
	void __fastcall TmainForm::SaveGridToFile();

	void __fastcall TmainForm::ReadIniFile();
	void __fastcall TmainForm::SaveIniFileData(bool DefaultTitles);
	void __fastcall TmainForm::SeqStepGrid();
	void __fastcall TmainForm::SeqColourCEllGREEN(int rowcell, int colcell);
	void __fastcall TmainForm::SeqColourCEllRED(int rowcell, int colcell);
	void __fastcall TmainForm::updateEfficiencySeqGrid(int SeqGridRow);
	bool __fastcall TmainForm::WriteToCSVSeqGrid(AnsiString FileName, AnsiString Data);
	void __fastcall TmainForm::PSUGWSetVDCandAMPS(AnsiString VDCSet, AnsiString AMPSSet);
	void __fastcall TmainForm::PSU_TurnON();
	void __fastcall TmainForm::PSU_TurnOFF();

	void __fastcall TmainForm::eLoad_SetRemote();
	void __fastcall TmainForm::eLoad_SetCCMode();
	void __fastcall TmainForm::eLoad_SetResMode();
	void __fastcall TmainForm::eLoad_SetLoadON();
	void __fastcall TmainForm::eLoad_SetLoadOFF();
	void __fastcall TmainForm::eLoad_SetLocal();
	void __fastcall TmainForm::eLoad_SetCCValue(AnsiString CCValue);
	void __fastcall TmainForm::eLoad_SetResValue(AnsiString CCValue);
	void __fastcall TmainForm::SeqTestSTOP();
	void __fastcall TmainForm::UpdateSeqRow();
	void __fastcall TmainForm::FileListIniRead();
	void __fastcall TmainForm::NumberSeqGridRows();


public:		// User declarations
	__fastcall TmainForm(TComponent* Owner);
	void __fastcall TmainForm::logit(AnsiString FileName, AnsiString Data);
	void __fastcall TmainForm::Comport_Rx_Data( int Count, uint8_t * pData,	int Node );

};
//---------------------------------------------------------------------------
extern PACKAGE TmainForm *mainForm;
//---------------------------------------------------------------------------
#endif
