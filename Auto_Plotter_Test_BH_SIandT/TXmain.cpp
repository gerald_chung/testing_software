/***************************************************************************H1*

	  Filename    : TXmain.cpp
      Description : TX application main entry
      Created     : July 2015

*******************************************************************************
					Copyright (c) 2015 - Michael Heyns
	   This file contains confidential and proprietary information.
	  All use, disclosure, and/or reproduction is strictly prohibited.
						   All rights reserved.
*******************************************************************************
******************************************************************************/

#define MAIN_MODULE		1
#include "Txmain.h"

int RunAsServer(void);
void __cdecl cleanup(void);

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")

static char recvbuf[DEFAULT_BUFLEN];
static char sendbuf[DEFAULT_BUFLEN];

/*----------------------------------------------------------------------
 * Client
 *----------------------------------------------------------------------*/
bool RunAsClient(void)
{
	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
					*ptr = NULL,
					hints;
	int iResult;
    int recvbuflen = DEFAULT_BUFLEN;

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed with error: %d\n", iResult);
        return false;
    }

    ZeroMemory( &hints, sizeof(hints) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
    iResult = getaddrinfo(LOCALHOST, DEFAULT_PORT, &hints, &result);
    if ( iResult != 0 ) {
        printf("getaddrinfo failed with error: %d\n", iResult);
        WSACleanup();
        return false;
    }

	// Attempt to connect to an address until one succeeds
	for(ptr=result; ptr != NULL ;ptr=ptr->ai_next) {

        // Create a SOCKET for connecting to server
        ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
            ptr->ai_protocol);
        if (ConnectSocket == INVALID_SOCKET) {
            printf("socket failed with error: %ld\n", WSAGetLastError());
			freeaddrinfo(result);
			WSACleanup();
            return false;
        }

        // Connect to server.
        iResult = connect( ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
        if (iResult == SOCKET_ERROR) {
            closesocket(ConnectSocket);
            ConnectSocket = INVALID_SOCKET;
			continue;
        }
        break;
    }

    freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("The TX server is not runing!\n");
        WSACleanup();
        return false;
    }

	// Send an initial buffer
    iResult = send( ConnectSocket, sendbuf, (int)strlen(sendbuf), 0 );
	if (iResult == SOCKET_ERROR) {
        printf("send failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return false;
    }

	// shutdown the connection since no more data will be sent
	iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
        WSACleanup();
        return false;
    }

    // Receive until the peer closes the connection
	iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);

	// cleanup
	closesocket(ConnectSocket);
	WSACleanup();

	if (iResult < 0)
		printf("recv failed with error: %d\n", WSAGetLastError());

	if (iResult > 0)
		return true;
	return false;
}

/*----------------------------------------------------------------------
 * Program entry point
 *----------------------------------------------------------------------*/
int __cdecl main(int argc, char* argv[])
{
	atexit(cleanup);

	if (argc == 1)
	{
		while (RunAsServer() == 0);
		return 1;
	}

	// catch local requests
	if (argc == 2 && stricmp(argv[1], "HELP") == 0)
	{
		printf("Syntax:  TX [parameters]\n");
		printf("No parameters starts the server - typically with: 'START /MIN TX'\n");
		printf("With parameters requests the server to perform a specific action\n");
		printf("Parameters:\n");
		printf("   HELP                       Prints this help\n");
		printf("   CLIENT VERSION             Prints the version of the client application\n");
		printf("   SERVER VERSION             Prints the version of the server\n");
		printf("   FIRMWARE VERSION           Prints the version of the TX firmware\n");
		printf("   SLEEP ms                   Sleeps for 'ms' milliseconds\n");
		printf("   DEBUG ON                   Turns debug messages ON\n");
		printf("   DEBUG OFF                  Turns debug messages OFF (default)\n");
		printf("   EXIT                       Stops the server\n");
		printf("   FILE OPEN filename         Opens 'filename' for data logging\n");
		printf("   FILE CLOSE                 Closes the log file\n");
		printf("   PORT OPEN number           Opens serial port 'number'\n");
		printf("   PORT CLOSE                 Closes the serial port\n");
		printf("   MSG text-string            Writes the text string (and timestamp)\n");
		printf("   FODA SCAN count            Does 'count' FODA scans\n");
		printf("   POWER SCAN timeout x y     Does a power scan with a 'timeout' period\n");
		printf("                              'x' and 'y' are for logging purposes only\n");
        return 0;
	}
	else if (argc == 3 && stricmp(argv[1], "SLEEP") == 0)
	{
		Sleep(atoi(argv[2]));
		return 0;
	}
	else if (argc == 3 && stricmp(argv[1], "CLIENT") == 0 && stricmp(argv[2], "VERSION") == 0)
	{
		printf("%s - Client Version %d.%d\n", NAME, MAJOR, MINOR);
		return 0;
	}

	// send the server the parameters
	sendbuf[0] = '\0';
	for (int i = 1; i < argc; i++)
	{
		strcat(sendbuf, argv[i]);
		if (i < argc - 1)
			strcat(sendbuf, " ");
	}

	bool rc = RunAsClient();

	// perform special work on returned data
	if (rc && argc == 3 && stricmp(argv[1], "SERVER") == 0 && stricmp(argv[2], "VERSION") == 0)
	{
		printf("%s\n", recvbuf);
		return 0;
	}
	else if (rc && argc == 3 && stricmp(argv[1], "FIRMWARE") == 0 && stricmp(argv[2], "VERSION") == 0)
	{
		printf("%s\n", recvbuf);
		return 0;
	}
	else if (rc && argc > 2 && stricmp(argv[1], "MSG") == 0)
	{
		printf("%s\n", recvbuf);
		return 0;
	}


	if (!rc || strncmp(recvbuf, "FAIL", 4) == 0)
	{
		printf("\n*** Syntax Error ***: %s\n", recvbuf);
		return -1;
	}
	printf("OK\n");
	return 0;
}


