#ifndef SerialIO2H
#define SerialIO2H

int __fastcall sndWriteSerial(HANDLE hCom, char *Str);
int __fastcall sndWriteSerial_N(HANDLE hCom, char *Str, uint32_t len);
int __fastcall sndReadSerial(HANDLE hCom, char *Str, unsigned long max);
HANDLE __fastcall sndOpenSerialPort(int Port, int Baud, bool Handshaking, int ReadTimeout, int WriteTimeout);
void __fastcall sndClosePort(HANDLE hCom);
void __fastcall sndFlushPort(HANDLE hCom);
int __fastcall sndDataWaiting(HANDLE hCom);

#endif
