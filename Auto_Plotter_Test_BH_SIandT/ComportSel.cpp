/*
  Example of Use

In Unit
#include "ComportSel.h"
#include "Utils.h" // Include Utils to Project also

	int ComportNo = UserSelectComport2("Select TX Com-Port", "PortTX", 115200);
	edTxComportNumber->Text = IntToStr(ComportNo);
	if( ComportNo == 0)
	{
		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
	}
	else
	{

	} // End If

int __fastcall xxx::UserSelectComport2(AnsiString dialogLabel, AnsiString regGroup, uint32_t baudrate)
{

		// create user dialog asking for com-port selection
		TDeviceSelectionForm * SelDialog;
		SelDialog = new TDeviceSelectionForm(this, dialogLabel);
		// set default selection
		//SelDialog->ComNumber = Reg_ReadInteger(sREG, regGroup, "Comm Number", 0);

	// if selection is complete, store result and test validity
	if (SelDialog->ShowModal() == mrOk)
	{

		if(SelDialog->ComNumber != 0)
		{
//			Reg_WriteInteger(sREG, regGroup, "Comm Number", SelDialog->ComNumber);
		} else {
			// if com-port could not be openen close application
			MessageDlg(	"Error Opening ComPort " + String(SelDialog->ComNumber) + " @ " + String(baudrate) +
						" -> Error : " + String(GetLastError()), mtError, TMsgDlgButtons() << mbOK, 0);
//			delete SelDialog;
//			Application->ShowMainForm = false;
//			Application->Terminate();
			return 0;
		}
	}
	else {
		// if cancel was pressed, do no open application
//		delete SelDialog;
//		Application->ShowMainForm = false;
//		Application->Terminate();
		return NULL;
	}

//	ShowMessage(IntToStr(SelDialog->ComNumber));

	//clear form and return result
	delete SelDialog;
	return 	SelDialog->ComNumber;

}

*/

//---------------------------------------------------------------------
#include <vcl.h>
#include <stdio.h>
#include <windows.h>
#include <winbase.h>
#include <tchar.h>
#pragma hdrstop



#include "ComportSel.h"


#define MaxComHandles 100
#define DirectTo AnsiString("Direct to COM")

//---------------------------------------------------------------------
#pragma resource "*.dfm"
TDeviceSelectionForm *DeviceSelectionForm;
//---------------------------------------------------------------------
__fastcall TDeviceSelectionForm::TDeviceSelectionForm(TComponent* AOwner, AnsiString windowLabel)
	: TForm(AOwner)
{
	//set window caption
	Label1->Caption = windowLabel.c_str();

}

//---------------------------------------------------------------------
BOOL __fastcall TDeviceSelectionForm::COM_exists( int port)
{
	char buffer[7];
	COMMCONFIG CommConfig;
	DWORD size;

	if (! (1 <= port && port <= 255))
	{
		return FALSE;
	}

	snprintf( buffer, sizeof buffer, "COM%d", port);
	size = sizeof CommConfig;

	// COM port exists if GetDefaultCommConfig returns TRUE
	// or changes <size> to indicate COMMCONFIG buffer too small.
	return (GetDefaultCommConfig( buffer, &CommConfig, &size)
													|| size > sizeof CommConfig);
}

//---------------------------------------------------------------------
void __fastcall TDeviceSelectionForm::dsfCancelBitBtnClick(TObject *Sender)
{

  DeviceName = dsfComboBox->Items->Strings[dsfComboBox->ItemIndex];
  if (DeviceName.Pos(DirectTo) > 0)
  {
  ComNumber = StrToInt(DeviceName.SubString(DirectTo.Length()+1,DeviceName.Length()));
  }

  ModalResult = mrCancel;

}

//---------------------------------------------------------------------------
bool __fastcall TDeviceSelectionForm::IsPortAvailable( int port)
{
	char buffer[7];
	COMMCONFIG CommConfig;
	DWORD size;

	if (! (1 <= port && port <= 255))
	{
		return FALSE;
	}

	snprintf( buffer, sizeof buffer, "COM%d", port);
	size = sizeof CommConfig;

	// COM port exists if GetDefaultCommConfig returns TRUE
	// or changes <size> to indicate COMMCONFIG buffer too small.
	return (GetDefaultCommConfig( buffer, &CommConfig, &size)
													|| size > sizeof CommConfig);
}


//---------------------------------------------------------------------
void __fastcall TDeviceSelectionForm::dsfOkBitBtnClick(TObject *Sender)
{

  DeviceName = dsfComboBox->Items->Strings[dsfComboBox->ItemIndex];
  if (DeviceName.Pos(DirectTo) > 0)
  {
  ComNumber = StrToInt(DeviceName.SubString(DirectTo.Length()+1,DeviceName.Length()));
  }

  ModalResult = mrOk;

}

//---------------------------------------------------------------------------

void __fastcall TDeviceSelectionForm::EnumComPorts(void)
{
int   Loop;

  for (Loop = 0; Loop < MaxComHandles; Loop++)
	{
	if (IsPortAvailable(Loop))
		{
	  FPortItemList->Add(DirectTo+IntToStr(Loop));
		}
	Application->ProcessMessages();

	}
}

//---------------------------------------------------------------------------

void __fastcall TDeviceSelectionForm::EnumAllPorts(void)
{
//  {-Collect the TAPI devices and comport numbers}

  Screen->Cursor = crHourGlass;


//  {Show ports only if requested}
//  if (ShowPorts) then                                               {!!.50}
	EnumComPorts();

  Screen->Cursor = crDefault;
}



void __fastcall TDeviceSelectionForm::FormShow(TObject *Sender)
{
  if (!FEnumerated)
  {
	EnumAllPorts();
	dsfComboBox->Items = FPortItemList;

  // { Highlite the active device in the list }
	dsfComboBox->ItemIndex = dsfComboBox->Items->IndexOf(DirectTo+IntToStr(ComNumber));

	FEnumerated = True;
	if (dsfComboBox->ItemIndex < 0)
		{
		dsfComboBox->ItemIndex = 0;
        }
	}

}
//---------------------------------------------------------------------------

void __fastcall TDeviceSelectionForm::FormCreate(TObject *Sender)
{
FEnumerated   = false;
FPortItemList = new TStringList();
}
//---------------------------------------------------------------------------

void __fastcall TDeviceSelectionForm::FormDestroy(TObject *Sender)
{
delete FPortItemList;
}
//---------------------------------------------------------------------------

