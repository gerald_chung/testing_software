object DeviceSelectionForm: TDeviceSelectionForm
  Left = 227
  Top = 108
  ActiveControl = dsfOkBitBtn
  BorderStyle = bsDialog
  Caption = 'Comport Selection Menu'
  ClientHeight = 113
  ClientWidth = 236
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 22
    Top = 8
    Width = 167
    Height = 16
    Alignment = taCenter
    Caption = 'Please choose a serial port.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 24
    Top = 56
    Width = 50
    Height = 13
    Caption = 'Baud Rate'
  end
  object dsfOkBitBtn: TBitBtn
    Left = 22
    Top = 78
    Width = 75
    Height = 27
    Caption = 'OK'
    Default = True
    ModalResult = 1
    NumGlyphs = 2
    TabOrder = 0
    OnClick = dsfOkBitBtnClick
  end
  object dsfCancelBitBtn: TBitBtn
    Left = 140
    Top = 78
    Width = 75
    Height = 27
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    NumGlyphs = 2
    TabOrder = 1
    OnClick = dsfCancelBitBtnClick
  end
  object dsfComboBox: TComboBox
    Left = 22
    Top = 29
    Width = 193
    Height = 21
    Style = csDropDownList
    TabOrder = 2
  end
  object edBaudRateSet: TEdit
    Left = 118
    Top = 51
    Width = 97
    Height = 21
    Enabled = False
    TabOrder = 3
    Text = 'Baud Rate'
  end
end
