//---------------------------------------------------------------------------
#ifndef UtilsH
#define UtilsH
//---------------------------------------------------------------------------

int __fastcall Reg_ReadInteger(AnsiString sAppRegPath,AnsiString sSection,AnsiString sValue,int iDefault);
void __fastcall Reg_WriteInteger(AnsiString sAppRegPath,AnsiString sSection,AnsiString sValue,int iData);
AnsiString __fastcall Reg_ReadString(AnsiString sAppRegPath,AnsiString sSection,AnsiString sValue,AnsiString sDefault);
void __fastcall Reg_WriteString(AnsiString sAppRegPath,AnsiString sSection,AnsiString sValue,AnsiString sData);

//int / string conversion utilities
//-----------------------------------------------------------------
unsigned int __fastcall MakeChecksum(AnsiString sData);
uint8_t __fastcall HexCharToNibble(uint8_t hexChar);
uint32_t __fastcall HexStrToUint(char *phexChars, uint8_t len, bool bigEndian);
uint8_t __fastcall xorCheckSum(uint8_t *buff, uint8_t len);
void __fastcall IntToHexByteBuff(int input, uint8_t *buff, uint8_t len);
void __fastcall IntToHexByteBuff2(int input, uint8_t *buff, uint8_t NoOfBytes);
AnsiString __fastcall ByteArrayToStr(uint8_t *buff, uint8_t len);
AnsiString __fastcall FloatToDigitString(double value);

unsigned short CalcChksum(unsigned char *start_addr, int bytecnt);
int __fastcall HexCharToChkSum(char *buff, int len);

#endif
