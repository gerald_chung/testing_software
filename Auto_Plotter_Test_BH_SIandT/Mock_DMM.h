//---------------------------------------------------------------------------

#ifndef MOCK_INTERFACE_H
#define MOCK_INTERFACE_H

#include <vcl.h>
#include <stdio.h>
#include  <io.h>
#include <setupapi.h>
#include "Utils.h"
#include "visa.h"


//-----------------------------------------------------------------------
// definitions
//-----------------------------------------------------------------------

#define AGILENT_SAMPLES_TO_READ (2)
#define AGILENT_GPIB_ADDRESS ("USB0::0x0957::0x2007::MY49013702::0::INSTR")
//-----------------------------------------------------------------------
// types
//-----------------------------------------------------------------------

typedef enum {
	MOCK_OK = 0x00,
	MOCK_ERR,
	MOCK_OK_HIB_LIB_LOAD_ERROR,
	MOCK_OK_CONNECT_ERROR
} MOCK_ret_t;


//simple controller class to control USBMM_Threads
class MOCK_Interface {

	 public:

		// Start the thread suspended (Constructor)
		__fastcall MOCK_Interface(TMemo *USBLogMemo)
		{
			Memo = USBLogMemo;
			MemoMutex = new TMutex(false);
			AGILENT_Session = 0;
			dataReady = false;
		}

		//Destructor
		__fastcall ~MOCK_Interface(void) {
		}

		bool __fastcall Start(void);
		void __fastcall Stop(void);
		bool __fastcall MeterConnected(uint8_t meter);
		double __fastcall GetOutput(uint8_t meter);
		void __fastcall StartNewMeasurement(void);
		bool __fastcall DataAvailable(void);
		void __fastcall AddToLog(AnsiString line);
		void __fastcall StopTimer(void);
		void __fastcall BeginTimer(void);
		void __fastcall DMM_Task(void);
//		void __fastcall RESETManualControlFlag(void);
//		void __fastcall SETManualControlFlag(void);

	private:

		//Memo for user messages
		TMemo *Memo;
		//Locks for shared resources
		TMutex *MemoMutex;
		//session for AGILENT daq
		ViSession defaultRM;
		ViSession AGILENT_Session;
		//keep track of the latest data
		bool dataReady;
		//latest measurements
		double curValues[4];
		double newValues[4];

		MOCK_ret_t __fastcall GetAnalogInputs(double *buff);
		void __fastcall DecodeRetString(char *buff, double* retValues);

};


//---------------------------------------------------------------------------
#endif
