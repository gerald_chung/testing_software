//---------------------------------------------------------------------------

#pragma hdrstop

#include "USB_meters.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


//============================================================================
//============================================================================
//============================================================================
//		USB CONTROLLER FUNCTION IMPLEMENTATIONS
//============================================================================
//============================================================================
//============================================================================


//---------------------------------------------------------------------------
// Add a line to the USB connection log...
//---------------------------------------------------------------------------
void __fastcall USBMM_Controller::AddToLog(AnsiString line)
{
	MemoMutex->Acquire();
	Memo->Lines->Add(line);
	MemoMutex->Release();
}

//---------------------------------------------------------------------------
// Tell all threads to go and get a new measurement
//---------------------------------------------------------------------------
void __fastcall USBMM_Controller::StartNewMeasurement(void){

	uint8_t i;
	//start a new measurement for each node, if all nodes have new data
	for(i=0; i<4 && this->DataAvailable(); i++){
		threads[i]->GetNewMeasurement();
	}
}

//---------------------------------------------------------------------------
// Ask all threads whether new data is available, if so, return true
//---------------------------------------------------------------------------
bool __fastcall USBMM_Controller::DataAvailable(void){

	uint8_t i;
	bool result = true;
	//check new measurement for each node
	for(i=0; i<4 && result; i++){
		result = (devices[i].connected == true && threads[i]->DataAvailable()) ? true : false;
	}

	//return result
	return result;
}


//---------------------------------------------------------------------------
// Get the latest output string of a certain Node
//---------------------------------------------------------------------------
double __fastcall USBMM_Controller::GetOutput(uint8_t thread){

	if(devices[thread].connected == true)
	{
		return threads[thread]->GetCurData().value;
	} else {
        return 0.0;
    }
}

//---------------------------------------------------------------------------
// Return connection state for node
//---------------------------------------------------------------------------
bool __fastcall USBMM_Controller::MeterConnected(uint8_t meter)
{
	return devices[meter].connected;
}

//---------------------------------------------------------------------------
// Kill all threads
//---------------------------------------------------------------------------
void __fastcall USBMM_Controller::Stop(void)
{
	uint8_t curThread;
	for (curThread = 0; curThread < 4; curThread++) {
		//only kill threads that are connected
		if( devices[curThread].connected )
		{
			AddToLog("Killing Thread " + String(curThread));
			threads[curThread]->Terminate();  //FreeOnTerminate is set to true
			devices[curThread].readhandle = NULL;
			devices[curThread].writehandle = NULL;
			devices[curThread].connected = false;
		}
	}
	//inform user
	AddToLog("USB Meters Stopped");
}

//---------------------------------------------------------------------------
// Attempt to start all threads, if not all can be started, return error
//---------------------------------------------------------------------------
bool __fastcall USBMM_Controller::Start(void)
{
	USBMM_ret_t result;
	uint8_t curDevice;

	AddToLog("Connecting...");

	//start by initializing the HID_DLL
	if( (result = Init_HID_DLL()) == USBMM_OK)
	{
		//connect each node, and start a thread
		for(curDevice = 0; curDevice<4; curDevice ++)
		{   //if node is not already started
			if( !devices[curDevice].connected ){
				//start by attempting to connect node
				devices[curDevice]=Connect_Device(curDevice);
				//if node has connected start thread!
				if(devices[curDevice].connected) {
					threads[curDevice] = new USBMM_Thread(devices[curDevice],MemoMutex,Memo);
					threads[curDevice]->FreeOnTerminate = true;
					threads[curDevice]->Start();
					AddToLog("Thread " + String(curDevice) + " Connected");
				} else {
					AddToLog("Thread " + String(curDevice) + " Failed");
					//indicate connection error
					result =  USBMM_USBMM_CONNECT_ERROR;
					break;
				}
			}
		}
	} else {
    	AddToLog("HID-Lib init failed");
	}

	//return result
	return result;
}

//---------------------------------------------------------------------------
// Initialize USB Hid library, used to get read/write handles for threads
//---------------------------------------------------------------------------
USBMM_ret_t __fastcall USBMM_Controller::Init_HID_DLL(void)
{

	//assume success
	USBMM_ret_t result = USBMM_OK;

	// Load the library:
	hHID = LoadLibrary(TEXT("HID.DLL"));
	if (!hHID) {
		ShowMessage("Failed to load HID.DLL.");
	}

	// Update the SetFeature call pointer:
	HidD_SetFeature = (PHidD_SetFeature) GetProcAddress(hHID, "HidD_SetFeature");
	// if ponter is invalid, return false, and free up library
	if (!HidD_SetFeature)
	{
		result = USBMM_HIB_LIB_LOAD_ERROR;
		ShowMessage("Couldn�t find one or more HID entry points.");
		FreeLibrary(hHID);
	}

	//return result
	return result;

}


//---------------------------------------------------------------------------
// Attempt to connect to node X, and if succesfull return device handles
//---------------------------------------------------------------------------
USBMM_device_t __fastcall USBMM_Controller::Connect_Device(uint8_t device) {
	// -------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------BEGIN CUT AND PASTE BLOCK-----------------------------------------------------------------------------------
	/*
	 Before we can "connect" our application to our USB embedded device, we must first find the device.
	 A USB bus can have many devices simultaneously connected, so somehow we have to find our device, and only
	 our device.  This is done with the Vendor ID (VID) and Product ID (PID).  Each USB product line should have
	 a unique combination of VID and PID.

	 Microsoft has created a number of functions which are useful for finding plug and play devices.  Documentation
	 for each function used can be found in the MSDN library.  We will be using the following functions:

	 SetupDiGetClassDevs()					//provided by setupapi.dll, which comes with Windows
	 SetupDiEnumDeviceInterfaces()			//provided by setupapi.dll, which comes with Windows
	 GetLastError()							//provided by kernel32.dll, which comes with Windows
	 SetupDiDestroyDeviceInfoList()			//provided by setupapi.dll, which comes with Windows
	 SetupDiGetDeviceInterfaceDetail()		//provided by setupapi.dll, which comes with Windows
	 SetupDiGetDeviceRegistryProperty()		//provided by setupapi.dll, which comes with Windows
	 malloc()								//part of C runtime library, msvcrt.dll?
	 CreateFile()							//provided by kernel32.dll, which comes with Windows

	 We will also be using the following unusual data types and structures.  Documentation can also be found in
	 the MSDN library:

	 PSP_DEVICE_INTERFACE_DATA
	 PSP_DEVICE_INTERFACE_DETAIL_DATA
	 SP_DEVINFO_DATA
	 HDEVINFO
	 HANDLE
	 GUID

	 The ultimate objective of the following code is to call CreateFile(), which opens a communications
	 pipe to a specific device (such as a HID class USB device endpoint).  CreateFile() returns a "handle"
	 which is needed later when calling ReadFile() or WriteFile().  These functions are used to actually
	 send and receive application related data to/from the USB peripheral device.

	 However, in order to call CreateFile(), we first need to get the device path for the USB device
	 with the correct VID and PID.  Getting the device path is a multi-step round about process, which
	 requires calling several of the SetupDixxx() functions provided by setupapi.dll.
	 */

	// Globally Unique Identifier (GUID) for HID class devices.  Windows uses GUIDs to identify things.
	GUID InterfaceClassGuid = {
		0x4d1e55b2, 0xf16f, 0x11cf, 0x88, 0xcb, 0x00, 0x11, 0x11, 0x00,
		0x00, 0x30};

	HDEVINFO DeviceInfoTable;
	PSP_DEVICE_INTERFACE_DATA InterfaceDataStructure =
		new SP_DEVICE_INTERFACE_DATA;
	PSP_DEVICE_INTERFACE_DETAIL_DATA DetailedInterfaceDataStructure =
		new SP_DEVICE_INTERFACE_DETAIL_DATA;
	SP_DEVINFO_DATA DevInfoData;


	DWORD InterfaceIndex = 0;
	DWORD dwRegType;
	DWORD dwRegSize;
	DWORD StructureSize = 0;
	PBYTE PropertyValueBuffer;
	DWORD ErrorStatus;
	uint8_t Match_Count = 0;

	USBMM_device_t usbmm_dev;
	usbmm_dev.connected = false;
	HANDLE writehandle = INVALID_HANDLE_VALUE;
	HANDLE readhandle = INVALID_HANDLE_VALUE;


	AnsiString DeviceIDToFind = NODE_DEVICE_ID;

	// First populate a list of plugged in devices (by specifying "DIGCF_PRESENT"), which are of the specified class GUID.
	DeviceInfoTable = SetupDiGetClassDevs(&InterfaceClassGuid, NULL, NULL,
		DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);

	// Now look through the list we just populated.  We are trying to see if any of them match our device.
	while (true) {
		InterfaceDataStructure->cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		if (SetupDiEnumDeviceInterfaces(DeviceInfoTable, NULL,
			&InterfaceClassGuid, InterfaceIndex, InterfaceDataStructure)) {
			ErrorStatus = GetLastError();
			if (ERROR_NO_MORE_ITEMS == ErrorStatus)
				// Did we reach the end of the list of matching devices in the DeviceInfoTable?
			{ // Cound not find the device.  Must not have been attached.
				SetupDiDestroyDeviceInfoList(DeviceInfoTable);
				// Clean up the old structure we no longer need.
				break;
			}
		}
		else // Else some other kind of unknown error ocurred...
		{
			GetLastError();
			SetupDiDestroyDeviceInfoList(DeviceInfoTable);
			// Clean up the old structure we no longer need.
			break;
		}

		// Now retrieve the hardware ID from the registry.  The hardware ID contains the VID and PID, which we will then
		// check to see if it is the correct device or not.

		// Initialize an appropriate SP_DEVINFO_DATA structure.  We need this structure for SetupDiGetDeviceRegistryProperty().
		DevInfoData.cbSize = sizeof(SP_DEVINFO_DATA);
		SetupDiEnumDeviceInfo(DeviceInfoTable, InterfaceIndex, &DevInfoData);

		// First query for the size of the hardware ID, so we can know how big a buffer to allocate for the data.
		SetupDiGetDeviceRegistryProperty(DeviceInfoTable, &DevInfoData,
			SPDRP_HARDWAREID, &dwRegType, NULL, 0, &dwRegSize);

		// Allocate a buffer for the hardware ID.
		PropertyValueBuffer = (BYTE*) malloc(dwRegSize);
		if (PropertyValueBuffer == NULL)
			// if null, error, couldn't allocate enough memory
		{ // Can't really recover from this situation, just exit instead.
			SetupDiDestroyDeviceInfoList(DeviceInfoTable);
			// Clean up the old structure we no longer need.
			break;
		}

		// Retrieve the hardware IDs for the current device we are looking at.  PropertyValueBuffer gets filled with a
		// REG_MULTI_SZ (array of null terminated strings).  To find a device, we only care about the very first string in the
		// buffer, which will be the "device ID".  The device ID is a string which contains the VID and PID, in the example
		// format "Vid_04d8&Pid_003f".
		SetupDiGetDeviceRegistryProperty(DeviceInfoTable, &DevInfoData,
			SPDRP_HARDWAREID, &dwRegType, PropertyValueBuffer, dwRegSize, NULL);

		// Now check if the first string in the hardware ID matches the device ID of my USB device.
#ifdef UNICODE
		AnsiString DeviceIDFromRegistry =
			AnsiString((wchar_t *)PropertyValueBuffer);
#else
		AnsiString DeviceIDFromRegistry =
			AnsiString((char *) PropertyValueBuffer);
#endif

		free(PropertyValueBuffer);
		// No longer need the PropertyValueBuffer, free the memory to prevent potential memory leaks

		// Convert both strings to lower case.  This makes the code more robust/portable accross OS Versions
		DeviceIDFromRegistry = DeviceIDFromRegistry.LowerCase();
		DeviceIDToFind = DeviceIDToFind.LowerCase();

		// Now check if the hardware ID we are looking at contains the correct VID/PID
		if ( DeviceIDFromRegistry.Pos(DeviceIDToFind) ) {
			Match_Count++;  //update match count
			if (Match_Count-1 == device) {

				// Device must have been found.  Open read and write handles.  In order to do this, we will need the actual device path first.
				// We can get the path by calling SetupDiGetDeviceInterfaceDetail(), however, we have to call this function twice:  The first
				// time to get the size of the required structure/buffer to hold the detailed interface data, then a second time to actually
				// get the structure (after we have allocated enough memory for the structure.)
				DetailedInterfaceDataStructure->cbSize =
					sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
				// First call populates "StructureSize" with the correct value
				SetupDiGetDeviceInterfaceDetail(DeviceInfoTable,
					InterfaceDataStructure, NULL, NULL, &StructureSize, NULL);
				DetailedInterfaceDataStructure =
					(PSP_DEVICE_INTERFACE_DETAIL_DATA)
					(malloc(StructureSize)); // Allocate enough memory
				if (DetailedInterfaceDataStructure == NULL)
					// if null, error, couldn't allocate enough memory
				{ // Can't really recover from this situation, just exit instead.
					SetupDiDestroyDeviceInfoList(DeviceInfoTable);
					// Clean up the old structure we no longer need.
					break;
				}
				DetailedInterfaceDataStructure->cbSize =
					sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
				// Now call SetupDiGetDeviceInterfaceDetail() a second time to receive the goods.
				SetupDiGetDeviceInterfaceDetail(DeviceInfoTable,
					InterfaceDataStructure, DetailedInterfaceDataStructure,
					StructureSize, NULL, NULL);

				// We now have the proper device path, and we can finally open read and write handles to the device.
				// We store the handles in the global variables "writehandle" and "readhandle", which we will use later to actually communicate.
				writehandle =
					CreateFile((DetailedInterfaceDataStructure->DevicePath),
					GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
					OPEN_EXISTING, 0, 0);

				ErrorStatus = GetLastError();
				if (ErrorStatus == ERROR_SUCCESS) {
					// ToggleLED_btn->Enabled = true;
				} // Make button no longer greyed out
				readhandle =
					CreateFile((DetailedInterfaceDataStructure->DevicePath),
					GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
					OPEN_EXISTING, 0, 0);
				ErrorStatus = GetLastError();
				if (ErrorStatus == ERROR_SUCCESS) {
					// GetPushbuttonState_btn->Enabled = true;
					// Make button no longer greyed out
					// StateLabel->Enabled = true;					//Make label no longer greyed out
				}

				reportBuf.reportID = 0x00;
				reportBuf.report[0] = 96;
				reportBuf.report[1] = 9;
				reportBuf.report[2] = 0;
				reportBuf.report[3] = 0;
				reportBuf.report[4] = 3;

				if (!(HidD_SetFeature)(writehandle, &reportBuf,	sizeof(USBMM_report_t)) )
				{
					ErrorStatus = GetLastError();
					ShowMessage("Error reading CMD_READ reply.");
					// LeaveCriticalSection(&g_crSection);
					break;
				}

				SetupDiDestroyDeviceInfoList(DeviceInfoTable);

				//return found read and write handles
				usbmm_dev.connected = true;
				break;

			}
		}

		InterfaceIndex++;
		// Keep looping until we either find a device with matching VID and PID, or until we run out of items.

	} // end of while(true)

	//store the result and return it.
	usbmm_dev.readhandle = readhandle;
	usbmm_dev.writehandle = writehandle;
	return usbmm_dev;

} //end of Connect()




//============================================================================
//============================================================================
//============================================================================
//		WORKER THREAD FUNCTION IMPLEMENTATIONS
//============================================================================
//============================================================================
//============================================================================


//---------------------------------------------------------------------------
// Log a string to the parent memo
//---------------------------------------------------------------------------
void __fastcall USBMM_Thread::Send_Message_To_Parent(AnsiString message)
{
	memoMux->Acquire();
	memo->Lines->Add(message);
	memoMux->Release();
}

//---------------------------------------------------------------------------
// return the latest available data
//---------------------------------------------------------------------------
DMM_Data __fastcall USBMM_Thread::GetCurData()
{
	dataMux->Acquire();
	DMM_Data result = data;
	dataMux->Release();
	return result;
}

//---------------------------------------------------------------------------
// Clear the NewData flag, this will be set automatically if data is available
//---------------------------------------------------------------------------
void __fastcall	USBMM_Thread::GetNewMeasurement(void){
	dataMux->Acquire();
	data.NewData = false;
	dataMux->Release();
	//new measurement will automatically set this again
}

//---------------------------------------------------------------------------
// Return the NewData flag
//---------------------------------------------------------------------------
bool __fastcall USBMM_Thread::DataAvailable(void){
	dataMux->Acquire();
	bool result = data.NewData;
	dataMux->Release();
	return result;
}

//---------------------------------------------------------------------------
// Decode the data received over USB and store the result
//---------------------------------------------------------------------------
void __fastcall USBMM_Thread::Decode_USB_Data(uint8_t *buffer, DWORD Len)
{
  unsigned char PacketBuffer[9];

	while (Len > 0)
	{
		//TODO: check for actual data availability?
		//copy next 9 bytes
		memcpy(PacketBuffer,buffer,9);
		Len -= 9;
		buffer += 9;
		Build_DMM_Message(PacketBuffer);
  	}
}


//---------------------------------------------------------------------------
// Helper for decoding USB data
//---------------------------------------------------------------------------
void __fastcall USBMM_Thread::Build_DMM_Message(uint8_t *pData)
{

	static unsigned char Message_Buffer[128];
	static unsigned char Index = 0;

	unsigned char Len = *pData & 0x07;
	pData++; // move past Len

	while (Len) {

		if (Index < sizeof(Message_Buffer)) {
			// SendToMemo(IntToStr(*pData &0x7f));

			Message_Buffer[Index++] = *pData & 0x7f;
			if ((*pData & 0x7f) == 10) {
				Decode_DMM(Message_Buffer);
				Index = 0;
			}
			pData++;
		}
		Len--;
	}

}


//---------------------------------------------------------------------------
// Decode the data received over USB and store the result
//---------------------------------------------------------------------------
void __fastcall USBMM_Thread::Decode_DMM(uint8_t *pData) {
    //Meaning of each byte
	//Byte 0:4  = ...
	//Byte 5    = decimal place
	//Byte 6    = Unit.  eg VDC  or mV
	//Byte 7    = mA AC - DC Flag;
	//Byte 8    = (Byte 8 & 0x04) =  Sign
	//Byte 9    = CR
	//Byte 10   = LF

	unsigned char Decimal_Place = pData[5] - '0';
	double result;
	char sign;
	AnsiString sUnit;

	//determine the sign
	sign = (pData[8] & 0x04) ? '-' : ' ';

	//determine the Unit and decimal places
	switch (pData[6]) {
	case '?':
		sUnit = "4-20mA %";
		break;

	case '1':
		sUnit = "VDC";
		break;

	case '2':
		sUnit = "VAC";
		break;

	case '7':
		sUnit = "uA";
		Decimal_Place += 3;
		if (pData[7] != '0') { 	sUnit += " AC";	}
		break;

	case '8':
		sUnit = "mA";
		Decimal_Place += 2;
		if (pData[7] != '0') { 	sUnit += " AC";	}
		break;

	case '9':
		sUnit = "A";
		Decimal_Place += 1;
		if (pData[7] != '0') {  sUnit += " AC";	}
		break;

	default:
		sUnit = "Unknown  " + StringOfChar((char) pData[6], 1);
	}

	//build result
	AnsiString sValue = AnsiString(sign);
	for (int i = 0; i < 5; i++) {
		if (i == Decimal_Place) {
			sValue += StringOfChar((char)'.', 1);
		}
		sValue += StringOfChar((char) pData[i], 1);
	}

	//Todo: TEST with actual hardware
	dataMux->Acquire();
	data.sUnit = sUnit;
	data.value = StrToFloat(sValue);
	data.NewData = true;
	dataMux->Release();

	//notify user (in parten window) of new data
	Send_Message_To_Parent("DMM : " + StringOfChar((char) pData[0], 1) + " " +
		StringOfChar((char) pData[1], 1) + " " + StringOfChar((char) pData[2],
		1) + " " + StringOfChar((char) pData[3], 1) + " " +
		StringOfChar((char) pData[4], 1) + " " + StringOfChar((char) pData[5],
		1) + " " + StringOfChar((char) pData[6], 1) + " " +
		StringOfChar((char) pData[7], 1) + " " + StringOfChar
		((char) pData[8], 1));
	//notify user of decoding result
	Send_Message_To_Parent("Sign : " + AnsiString(sign) + " DP " + IntToStr(Decimal_Place) +
		" Unit " + sUnit + "     " + sValue);

}

//---------------------------------------------------------------------------
// This is what gets run when the thread starts
//---------------------------------------------------------------------------
void __fastcall USBMM_Thread::Execute(void)
{

	while (!Terminated) {
		//read new data if available
		ReadFile( device.readhandle, &InputPacketBuffer, 65, &BytesRead, 0);
		//threadsafe USB data decoding
		Decode_USB_Data(&InputPacketBuffer[1], BytesRead);
		memset( InputPacketBuffer, 0x00, sizeof(InputPacketBuffer));
	}

}





