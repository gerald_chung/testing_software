/***************************************************************************H1*

	  Filename    : TXapp.cpp
      Description : TX application
      Created     : July 2015

*******************************************************************************
					Copyright (c) 2015 - Michael Heyns
	   This file contains confidential and proprietary information.
	  All use, disclosure, and/or reproduction is strictly prohibited.
						   All rights reserved.
*******************************************************************************
******************************************************************************/

/*

  [OpenTxPort]
  [CloseTxPort]

  [ScanAndPowerON]
  [TxStatus]
  [ProcessPowerPacket2]

  [FetchOneMessages2]

*/


#include "TXmain.h"
#include "_SerialIO2.h"
//#include "mainGUI.h"

#define SOM 0x02    // Start of Message
#define EOM 0x03    // End Of Message
#define DLE 0x10    //
#define Expected(x)		(x + 4)

//extern bool Debug;
bool Debug;

static HANDLE hCom = NULL;
static bool PortOpen = false;
FILE *fp;

static bool MustPrintHeading = true;

static int16_t cal00[29];
static int16_t cal90[29];
static int16_t plot00[29];
static int16_t plot90[29];
static int16_t pcoil_signal_strength[10];
static int16_t pcoil_status[10];
static int16_t pcoil_Coil1;
static int16_t pcoil_Coil2;

static bool TxScanDone;

#define RXBUFLEN		5000
static char rxbuf[RXBUFLEN+1];
static char rxbufReport[RXBUFLEN+1];
static int rxbufReportCnt;
static char TxBuffer[400];
static unsigned char TxSeqNum2 = '0' - 1;
static unsigned char RxSeqNum2 = 'X';

static void Transmit_Message(char *message);
static void Transmit_Message_N(char *message, uint16_t Len);
static bool SetManualMode(void);

int GetTxSigStrength(void);

/*----------------------------------------------------------------------
 * This procedure is called with a pointer to a null terminated message .
 * The message should include the header
 *----------------------------------------------------------------------*/
static void Transmit_Message(char *message)
{
	Transmit_Message_N(message, strlen(message));
}

/*----------------------------------------------------------------------
 * Sends a message
 *----------------------------------------------------------------------*/
static void Transmit_Message_N(char *message, uint16_t Len)
{

	uint32_t index = 0;
	char LRC = 0;

	TxBuffer[index++] = SOM;

	if ((*message & 0x20) == 0) // (* Originator message *)
	{
		TxSeqNum2++;
		if (TxSeqNum2 > '9') {
			TxSeqNum2 = '1';
		}

		TxBuffer[index++] = TxSeqNum2;
		LRC += TxSeqNum2;

	}
	else // (* Response message *)
	{
		TxBuffer[index++] = RxSeqNum2;
		LRC += RxSeqNum2;
	}

	TxBuffer[index++] = *message; // Header
	LRC += *message;
	Len--;
	message++;

	while (Len != 0) {
		if ((*message == DLE) || (*message == SOM) || (*message == EOM))
		{
			TxBuffer[index++] = DLE;
			LRC += DLE;
		}
		TxBuffer[index++] = *message;
		LRC += *message;
		message++;
		Len--;
	}

	LRC = (((LRC + ((LRC & 0xC0) >> 6)) & 0x3F) + 0x20);
	TxBuffer[index++] = LRC;
	TxBuffer[index++] = EOM;
	TxBuffer[index] = 0x00;

	sndFlushPort(hCom);
	sndWriteSerial_N(hCom, TxBuffer, index);
	Sleep(100);

}

/*----------------------------------------------------------------------
 * Fetches 1 message (only)
 *----------------------------------------------------------------------*/
static int FetchOneMessages(int timeout)
{

	int len = 0;
	bool message_started = false;

	// fetch SOM
	while (timeout-- > 0)
	{
		if (sndDataWaiting(hCom) > 0)
		{
			// Read one character
			if (sndReadSerial(hCom, &rxbuf[0], 1) == 1)
			{
				// if it's the start, then proceed
				if (rxbuf[0] == SOM)
				{
					message_started = true;
					break;
				} // End If
			}
			else
			{
				timeout++;			// reset timeout
			}  // End If
		}
		Sleep(1);
	}  // End While


	// if timed out, exit
	if (!message_started)
	{

		rxbuf[0] = '\0';
		return 0;

	} // End If



	// fetch the rest of the message
	len++;
	while (timeout-- > 0)
	{
		while (sndDataWaiting(hCom) > 0)
		{
			// read one character
			if (sndReadSerial(hCom, &rxbuf[len], 1) == 1)
			{
				// if it's the End, then proceed
				if (rxbuf[len] == EOM)
				{
					if (len > 0)
					{
 						len--;				// move back to the LRC
// ***********
// Above was found to overright Signal Strength byte B7 so commented out Gerald Chung
// ***********
						rxbuf[len] = '\0';

					} // End If
/*					 AnsiString test;
					for (int i = 0; i < 40; i++) {
					  test = test + rxbuf[i];
					}

					ShowMessage(test);
*/
					return len;
				}
				else
					timeout++;		// reset timeout
			}
			len++;
		} // End While
		Sleep(1);
	}  // End While

	rxbuf[0] = '\0';

	return 0;
}


/*----------------------------------------------------------------------
 * Fetches 1 message (only)
 *----------------------------------------------------------------------*/
static int FetchOneMessages2(int timeout)
{

// Read All data first Option
//		if (sndDataWaiting(hCom) > 0)
//		{
//			bool rxbufReport22 = sndReadSerial(hCom, &rxbufReport[0], 500);
//			bool rxbufReport22 = sndReadSerial(hCom, &rxbuf[0], 500);
 //			AnsiString test = rxbufReport[0];
//			ShowMessage("dfd");
//		}
//			ShowMessage("dfd");


}


/*----------------------------------------------------------------------
 * Sets the TX into MANUAL mode
 *----------------------------------------------------------------------*/
static bool SetManualMode(void)
{
	Transmit_Message("N0100000001");
	int len = FetchOneMessages(500);
	if (len > 0)
	{
		Transmit_Message("P010008B02100000000");			// LOG on + Enable power coil info + send Qi Control Error
		int len = FetchOneMessages(500);
		if (len > 0)
		{
			sndFlushPort(hCom);
			return true;
		}
	}
	return false;
}

/*----------------------------------------------------------------------
 * Sets the TX into MANUAL mode
 *----------------------------------------------------------------------*/
bool SetManualMode2(void)
{
	Transmit_Message("N0100000001");

	int len = FetchOneMessages(100);
	if (len > 0)
	{
		Transmit_Message("P010008B02100000000");			// LOG on + Enable power coil info + send Qi Control Error
		int len = FetchOneMessages(100);
		if (len > 0)
		{
			sndFlushPort(hCom);
			return true;
		}
	}
	return false;

}

/*----------------------------------------------------------------------
 * Sets the TX into MANUAL mode
 *----------------------------------------------------------------------*/
bool SetAutoMode2(void)
{
	Transmit_Message("N0100000000");

	int len = FetchOneMessages(100);
	if (len > 0)
	{
		Transmit_Message("P010008B02100000000");			// LOG on + Enable power coil info + send Qi Control Error
		int len = FetchOneMessages(100);
		if (len > 0)
		{
			sndFlushPort(hCom);
			return true;
		}
	}
	return false;
}

/*----------------------------------------------------------------------
 * Wait for 'N' Bytes in T milliseconds
 *----------------------------------------------------------------------*/
static bool WaitFor(uint16_t number, uint16_t timeout)
{
	while (timeout-- > 0)
	{
		if (sndDataWaiting(hCom) >= number)
			return true;
		Sleep(1);
	}
	return false;
}

// ###########################################################################################################################

/*----------------------------------------------------------------------
 * Opens the CSV file
 *----------------------------------------------------------------------*/
bool txOpenFile(char *fname)
{
	fp = fopen(fname, "wt");
	if (fp != NULL)
	{
		if (Debug)
			printf("File '%s' now open\n", fname);
		return true;
	}
	if (Debug)
		printf("***ERROR: Could not open file '%s'\n", fname);
	return false;
}

/*----------------------------------------------------------------------
 * Closes the CSV file
 *----------------------------------------------------------------------*/
void txCloseFile(void)
{
	if (fp != NULL)
	{
		if (Debug)
			printf("Closing file\n");
		MustPrintHeading = true;
		fclose(fp);
		fp = NULL;
	}
	else if (Debug)
		printf("File was already closed\n");
}

/*----------------------------------------------------------------------
 * Reads the VERSION string from the TX
 *----------------------------------------------------------------------*/
char * GetTxVersion(void)
{
	if (PortOpen)
	{
		Transmit_Message("V");
		int len = FetchOneMessages(500);
		int RetryCount = 0;
		while (len > 0)
		{
			if (rxbuf[2] == 'v')
			{
				return &rxbuf[7];
			}
			else
			{
				len = FetchOneMessages(500);
				if (RetryCount > 20)
				{
					RetryCount = 0;
					return "***ERROR: No valid string received from the TX";
//					ShowMessage("WARNING: No Valid data received after 20 retries");
				}
				else
				{
					RetryCount = RetryCount + 1;
				} // End If
			} // End If
		}  // End While

		return "***ERROR: No valid string received from the TX";
	}
	return "***ERROR: Port not open";
}

/*----------------------------------------------------------------------
 * Opens the serial port to the TX
 *----------------------------------------------------------------------*/
bool OpenTxPort(int port)
{
	hCom = sndOpenSerialPort(port, 115200, false, 10, 10);
	if (hCom != NULL)
	{
			PortOpen = true;
			rxbufReportCnt = 0;
			return true;

	}
	if (Debug)
		printf("***ERROR: Could not open serial port %d\n", port);
		PortOpen = false;
	return false;
}

/*----------------------------------------------------------------------
 * Opens the general serial port
 *----------------------------------------------------------------------*/
bool OpenSerialPort(int port, int Baudrate)
{
	hCom = sndOpenSerialPort(port, Baudrate, false, 10, 10);
	if (hCom != NULL)
	{
			return true;
	}
	if (Debug)
		printf("***ERROR: Could not open serial port %d\n", port);
	PortOpen = false;
	return false;
}

/*----------------------------------------------------------------------
 * Opens the serial port to the TX
 *----------------------------------------------------------------------*/
bool flgTxPortOpen(void)
{
	if (PortOpen)
	{
		return true;
	}
	else
	{
		return false;
	} // End If

}

/*----------------------------------------------------------------------
 * Closes the port
 *----------------------------------------------------------------------*/
void CloseTxPort(void)
{
//	if (Debug)
//		printf("Closing comms port\n");

	if (PortOpen)
	{
		sndClosePort(hCom);
		hCom = NULL;
		PortOpen = false;
	}
	else
	{
	   //	ShowMessage("Port Not open");
	}
	// End If
}

/*----------------------------------------------------------------------
 * Extract the coil values from the message
 *----------------------------------------------------------------------*/
static int ProcessQMpacket(char *packet)
{
	int coil, _00, _90;

	if (strncmp(packet, "QM00", 4) == 0 || strncmp(packet, "QM01", 4) == 0)
	{
		sscanf(&packet[10],"%X", &_90);
		packet[10] = '\0';
		sscanf(&packet[6],"%X", &_00);
		packet[6] = '\0';
		sscanf(&packet[4],"%X", &coil);

		plot00[coil] = _00;
		plot90[coil] = _90;

		return 1;
	}
	return 0;
}

/*----------------------------------------------------------------------
 * Scan the Tx and put the data into the LOG file
 *----------------------------------------------------------------------*/
int ScanFODA(int N)
{
	if (Debug)
		printf("Scanning %d times\n", N);

	if (fp == NULL)
	{
		if (Debug)
			printf("***ERROR: Log file is not open (Use TX FILE OPEN filename)\n");
		return 0;
	}

	if (!PortOpen)
	{
		if (Debug)
			printf("***ERROR: Serial port not open (Use TX PORT OPEN port)\n");
		return 0;
	}

	uint64_t total = 0;

	if (MustPrintHeading)
	{
		MustPrintHeading = false;
		for (int c = 0; c < 28; c++)
			fprintf(fp, "%d,", c);
		fprintf(fp, "EX-00,");
		for (int c = 0; c < 28; c++)
			fprintf(fp, "%d,", c);
		fprintf(fp, "EX-90\n");
	}

	while (N > 0)
	{
		// do one manual scan
		Transmit_Message("S00");

		// fetch the results
		int coil_count = 0;
		int len = FetchOneMessages(500);
		while (len > 0)
		{
			char *qm = strstr(rxbuf, "QM0");
			if (qm != NULL)
			{
				coil_count += ProcessQMpacket(qm);
				if (coil_count == 28)
					break;					// last coil
			}
			len = FetchOneMessages(500);
		}

		// save it
		if (coil_count == 28)
		{
			for (int c = 0; c < 29; c++)
				fprintf(fp, "%d,", plot00[c]);
			for (int c = 0; c < 28; c++)
				fprintf(fp, "%d,", plot90[c]);
			fprintf(fp, "%d\n", plot90[28]);
		}
		else
			fprintf(fp, "Invalid packets received\n");

		total++;

		N--;
	}

	if (Debug)
		printf("%d scans done and saved\n", total);
	return total;
}

/*----------------------------------------------------------------------
 * Extract the coil values from the message
 *----------------------------------------------------------------------*/
static bool ProcessPowerPacket(char *packet)
{
	int coil, ss, status;

	sscanf(&packet[6],"%X", &ss);		// Qi Signal Strength
	packet[6] = '\0';
	sscanf(&packet[4],"%X", &status);
	packet[4] = '\0';
	sscanf(&packet[2],"%X", &coil);

/*	if( status == 0x01 )
	{
		ss++;
	}
*/
	pcoil_status[coil] = status;
	pcoil_signal_strength[coil] = ss;

	return true;
}

/*----------------------------------------------------------------------
 * Extract the coil values from the message
 *----------------------------------------------------------------------*/
static bool ProcessPowerPacket2(char *packet)
{
	int coil1, ss, ss2, status, coil2;

	sscanf(&packet[6],"%X", &ss);		// Qi Signal Strength
	packet[6] = '\0';

	sscanf(&packet[4],"%X", &status);   // Tx Status
	packet[4] = '\0';

	sscanf(&packet[3],"%X", &coil1);  // Coil 1 Info
	packet[3] = '\0';

	sscanf(&packet[2],"%X", &coil2);   // Coil 2 Info
	packet[2] = '\0';

	pcoil_status[11] = status;
	pcoil_signal_strength[1] = ss;

	pcoil_Coil1 = coil1;
	pcoil_Coil2 = coil2;

	return true;
}

/*----------------------------------------------------------------------
 * Report Status Tx to Caller
 *----------------------------------------------------------------------*/
bool TxStatusDone(void)
{

  if (TxScanDone)
  {
	//return TxData;
	TxScanDone = false;
	return true;
  }
  else
  {
	return false;
  } // End If

}

/*----------------------------------------------------------------------
 * Report Status Tx to Caller
 *----------------------------------------------------------------------*/
int GetTxSigStrength(int &ptrQi_Signal_Strength)
{

	ptrQi_Signal_Strength = pcoil_signal_strength[1];
	return 1; //pcoil_signal_strength[11];

}

/*----------------------------------------------------------------------
 * Report Status Tx to Caller
 *----------------------------------------------------------------------*/
int GetTxStatus(int &ptrxx)
{

	ptrxx = pcoil_status[1];
	return 1; //pcoil_signal_strength[11];

}


/*----------------------------------------------------------------------
 * Report Status Tx to Caller
 *----------------------------------------------------------------------*/
int GetTxCoil1(int &ptrQi_Coil_Num_1)
{

	ptrQi_Coil_Num_1 = pcoil_Coil1;
	return 1; //pcoil_signal_strength[11];

}


/*----------------------------------------------------------------------
 * Report Status Tx to Caller
 *----------------------------------------------------------------------*/
int GetTxCoil2(int &ptrQi_Coil_Num_2)
{

	ptrQi_Coil_Num_2 = pcoil_Coil2;
	return 1; //pcoil_signal_strength[11];

}

// ***
// Globals
// ***
int old_demod_error_packet_count2 = 0;
int old_demod_correct_packet_count = 0;
int iq_demod_error_packet_count = 0;
int iq_demod_correct_packet_count = 0;
int count_of_packets_correctly_demodulated = 0;
// ***
// Ends Globals
// ***

/*----------------------------------------------------------------------
 * Extract the coil values from the message
 *----------------------------------------------------------------------*/
static bool ProcessPER(char *packet)
{
	sscanf(&packet[18],"%X", &count_of_packets_correctly_demodulated);
	packet[18] = '\0';
	sscanf(&packet[14],"%X", &iq_demod_correct_packet_count);
	packet[14] = '\0';
	sscanf(&packet[10],"%X", &iq_demod_error_packet_count);
	packet[10] = '\0';
	sscanf(&packet[6],"%X", &old_demod_correct_packet_count);
	packet[6] = '\0';
	sscanf(&packet[2],"%X", &old_demod_error_packet_count2);

	return true;
}

/*----------------------------------------------------------------------
 * returns true if the message contains an error message
 *----------------------------------------------------------------------*/
bool IsErrorMessage(void)
{
	char *error = strstr(&rxbuf[2], "Q*");
	char *debug_msg = strstr(&rxbuf[2], "Q*Debug");
	if (error != NULL && debug_msg == NULL)					// ERROR received - no use continuing!
	{
		for (int i = 0; i < 28; i++)
			fprintf(fp, ",");
		fprintf(fp, "FAIL,");
		fprintf(fp, "%s\n", error);

		Transmit_Message("B");
		if (Debug)
			printf("\n***ERROR: Error message received: '%s'\n", error);
		return true;
	}
	return false;
}

/*----------------------------------------------------------------------
 * Scan and Power (power_timeout)
 *----------------------------------------------------------------------*/
bool ScanAndPower(int power_timeout, int stable_timeout, int x, int y)
{
	if (Debug)
		printf("Timeout:%dms X:%d Y:%d\n", power_timeout, x, y);

	if (fp == NULL)
	{
		if (Debug)
			printf("***ERROR: Log file is not open (Use TX FILE OPEN filename)\n");
		return 0;
	}

	if (!PortOpen)
	{
		if (Debug)
			printf("***ERROR: Serial port not open (Use TX PORT OPEN port)\n");
		return 0;
	}

	if (MustPrintHeading)
	{
		MustPrintHeading = false;
		fprintf(fp, "X,Y,");
		for (int c = 0; c < 10; c++)
			fprintf(fp, "P%d,", c);
		for (int c = 0; c < 10; c++)
			fprintf(fp, "S%d,", c);
		fprintf(fp, "msTillPower:[%d], StableCount,NotStableCount,", power_timeout);
		fprintf(fp, "Old_Error,");
		fprintf(fp, "Old_Count,");
		fprintf(fp, "IQ_Error,");
		fprintf(fp, "IQ_Count,");
		fprintf(fp, "Qi_Both,");
		fprintf(fp, "Result,Message\n", power_timeout);
	}

	// start the results with the given co-ordinates
	fprintf(fp, "%d,%d,", x, y);

	// enable 'Q' messages
	old_demod_error_packet_count2 = 0;
	old_demod_correct_packet_count = 0;
	iq_demod_error_packet_count = 0;
	iq_demod_correct_packet_count = 0;
	count_of_packets_correctly_demodulated = 0;
	Transmit_Message("P010009B02100000000");			// LOG on + Enable power coil info + send Qi Control Error + Q messages

	// perform a SCAN-AND-POWER
	if (Debug)
		printf("Turning on the power coils\n");
	Transmit_Message("X");	// reset
	Transmit_Message("K");


	// look at all incoming messages for 'power_timeout' period
	if (Debug)
		printf("Waiting for power to stabalise\n");
	char *error;
	char *debug_msg;
	int time_till_power = 0;
	while (power_timeout-- > 0)
	{
		int len = FetchOneMessages(500);
		if (len > 0)
		{
			rxbuf[len-1] = '\0';

			if (strncmp(&rxbuf[2], "QF00", 4) == 0)
			{
				if (Debug)
					printf("---Stable at %d ms\n", time_till_power);
				break;
			}
			else if (strncmp(&rxbuf[2], "QA0", 3) == 0)
				ProcessPowerPacket(&rxbuf[2]);

			// find any errors
			if (IsErrorMessage())
			{
				Transmit_Message("B");		// turn off power
				return false;
			}
		}
		Sleep(100);
		time_till_power += 100;

	}  // End While

	// now make sure it's stable for a while
	int stable_messages = 0;
	int not_stable_messages = 0;
	if (time_till_power < power_timeout)
	{
		if (Debug)
			printf("Making sure it stays stable\n");

		while (stable_timeout-- > 0)
		{
			int len = FetchOneMessages(500);
			if (len > 0)
			{
				rxbuf[len-1] = '\0';

				if (strncmp(&rxbuf[2], "QF00", 4) == 0)
					stable_messages++;
				else if (strncmp(&rxbuf[2], "QF", 2) == 0)
					not_stable_messages++;
				else if (strncmp(&rxbuf[2], "QP0", 3) == 0)
					ProcessPER(&rxbuf[2]);

				// find any errors
				if (IsErrorMessage())
				{
					Transmit_Message("B");		// turn off power
					return false;
				} // End If

			} // End If
			Sleep(100);
		}  // End While
	}
	else
	{
		if (Debug)
			printf("No power - so no we're all done\n");
	}

	// turn off the power coils
	if (Debug)
		printf("Turning off the power coils\n");
	Transmit_Message("B");

	// disable 'Q' messages
	Transmit_Message("P010008B02100000000");			// Disable Q messages	(don't think it's necessary, but still)

	// save the result
	if (Debug)
		printf("Saving results\n");

	// write POWERED status
	int powered = 0;
	for (int c = 0; c < 10; c++)
	{
		switch (pcoil_status[c])
		{
			case 0: fprintf(fp, ","); break;
			case 1: fprintf(fp, "Detected,"); break;
			case 2: fprintf(fp, "Nearby,"); break;
			case 3: fprintf(fp, "Coupled,"); break;
			case 4: fprintf(fp, "POWERED,"); powered++; break;
			case 5: fprintf(fp, "CommsError,"); break;
			case 6: fprintf(fp, "NotCoupled,"); break;
			case 7: fprintf(fp, "FailedFOD,"); break;
			default:  break;
		}
	}

	// write signal strength
	for (int c = 0; c < 10; c++)
		fprintf(fp, "%d,", pcoil_signal_strength[c]);

	// write timeout periods
	if (Debug)
		printf("TimeTillPower:%d   StableCount:%d   NotStableCount:%d\n", time_till_power, stable_messages, not_stable_messages);
	fprintf(fp, "%d,%d,%d,", time_till_power, stable_messages, not_stable_messages);

	fprintf(fp, "%d,", old_demod_error_packet_count2);
	fprintf(fp, "%d,", old_demod_correct_packet_count);
	fprintf(fp, "%d,", iq_demod_error_packet_count);
	fprintf(fp, "%d,", iq_demod_correct_packet_count);
	fprintf(fp, "%d,", count_of_packets_correctly_demodulated);

	// write OK/FAIL
	if (powered > 0 && stable_messages > 0 && not_stable_messages == 0)
	{
		fprintf(fp, "GOOD,Powered and stable\n");
		if (Debug)
			printf("GOOD,Powered and stable\n");
	}
	else if (powered > 0 && stable_messages > 0 && not_stable_messages > 0 && stable_messages > not_stable_messages)
	{
		fprintf(fp, "FAIR,Powered but NOT stable\n");
		if (Debug)
			printf("FAIR,Powered but NOT stable\n");
	}
	else if (powered > 0 && not_stable_messages > 0)
	{
		fprintf(fp, "BAD,Powered but NOT stable\n");
		if (Debug)
			printf("FAIR,Powered but NOT stable\n");
	}
	else
	{
		fprintf(fp, "FAIL,Nothing powered\n");
		if (Debug)
			printf("FAIL,Nothing powered\n");
	}

	return true;
}

/*----------------------------------------------------------------------
 * Scan and Power ON only (power_timeout)
 *----------------------------------------------------------------------*/
bool ScanAndPowerON(int power_timeout, int stable_timeout, int x, int y, int pCoilStatus[10][2])
{

	TxScanDone = false;


	if (Debug)
//		printf("Timeout:%dms X:%d Y:%d\n", power_timeout, x, y);

	if (fp == NULL)
	{
 //		if (Debug)
//			printf("***ERROR: Log file is not open (Use TX FILE OPEN filename)\n");
		return 0;
	}

	if (!PortOpen)
	{
//		if (Debug)
//			printf("***ERROR: Serial port not open (Use TX PORT OPEN port)\n");
		return 0;
	}

	if (MustPrintHeading)
	{
		MustPrintHeading = false;
/*		fprintf(fp, "X,Y,");
		for (int c = 0; c < 10; c++)
			fprintf(fp, "P%d,", c);
		for (int c = 0; c < 10; c++)
			fprintf(fp, "S%d,", c);
		fprintf(fp, "msTillPower:[%d], StableCount,NotStableCount,", power_timeout);
		fprintf(fp, "Old_Error,");
		fprintf(fp, "Old_Count,");
		fprintf(fp, "IQ_Error,");
		fprintf(fp, "IQ_Count,");
		fprintf(fp, "Qi_Both,");
		fprintf(fp, "Result,Message\n", power_timeout);
*/
	}

	// start the results with the given co-ordinates
//	fprintf(fp, "%d,%d,", x, y);

	// enable 'Q' messages
	old_demod_error_packet_count2 = 0;
	old_demod_correct_packet_count = 0;
	iq_demod_error_packet_count = 0;
	iq_demod_correct_packet_count = 0;
	count_of_packets_correctly_demodulated = 0;

//	P010000000100000000
	Transmit_Message("P010009B02100000000");			// LOG on + Enable power coil info + send Qi Control Error + Q messages

// 	Transmit_Message("P010000000100000000");  // Send P Command to allow for Coil State Q Message

	// perform a SCAN-AND-POWER
//	if (Debug)
//		printf("Turning on the power coils\n");
	Transmit_Message("X");	// reset
	Transmit_Message("K");


	// look at all incoming messages for 'power_timeout' period
//	if (Debug)
//		printf("Waiting for power to stabalise\n");
	char *error;
	char *debug_msg;
	int time_till_power = 0;

	while (power_timeout-- > 0)
	{
		int len = FetchOneMessages(500);
		if (len > 0)
		{
			rxbuf[len-1] = '\0';

			if (strncmp(&rxbuf[2], "QF00", 4) == 0)
			{
				if (Debug)
//					printf("---Stable at %d ms\n", time_till_power);
				break;
			}
			else if (strncmp(&rxbuf[2], "QA0", 3) == 0)

			 //	ProcessPowerPacket(&rxbuf[2]);

			// find any errors
			if (IsErrorMessage())
			{
		 //		Transmit_Message("B");		// turn off power
				return false;
			}
		}
		Application->ProcessMessages();

		Sleep(100);

		time_till_power += 100;

	} // End While



// Write POWERED status
	AnsiString AllCoilStatus = "";
 //	int powered = 0;
	for (int c = 0; c < 10; c++)
	{

		pCoilStatus[c][0] = pcoil_status[c];

		switch (pcoil_status[c])
		{
			case 0:
				//fprintf(fp, ",");
				AllCoilStatus = AllCoilStatus + "Nothing";
				break;
			case 1:
				//fprintf(fp, "Detected,");
				AllCoilStatus = AllCoilStatus + "Detected";
				break;
			case 2:
				//fprintf(fp, "Nearby,");
				AllCoilStatus = AllCoilStatus + "Nearby";
				break;
			case 3:
				//fprintf(fp, "Coupled,");
				AllCoilStatus = AllCoilStatus + "Coupled";
				break;
			case 4:
				//fprintf(fp, "POWERED,"); powered++;
				AllCoilStatus = AllCoilStatus + "POWERED";
				break;
			case 5:
				//fprintf(fp, "CommsError,");
				AllCoilStatus = AllCoilStatus + "CommsError";
				break;
			case 6:
				//fprintf(fp, "NotCoupled,");
				AllCoilStatus = AllCoilStatus + "NotCoupled";
				break;
			case 7:
				//fprintf(fp, "FailedFOD,");
				AllCoilStatus = AllCoilStatus + "FailedFOD";
				break;
			default:
				AllCoilStatus = AllCoilStatus + "Unknown";
				break;
		} // End Switch
	}  // End For


	// Write Signal Strength
	for (int c = 0; c < 10; c++)
	{
		//fprintf(fp, "%d,", pcoil_signal_strength[c]);
		pCoilStatus[c][1] = pcoil_signal_strength[c];

	} // End For

	TxScanDone = true;
	return true;
}

/*----------------------------------------------------------------------
 * Scan and Power ON only (power_timeout)
 *----------------------------------------------------------------------*/
bool ScanAndPowerONOnly(int power_timeout, int stable_timeout, int x, int y)
{

	TxScanDone = false;


	if (Debug)
//		printf("Timeout:%dms X:%d Y:%d\n", power_timeout, x, y);

	if (fp == NULL)
	{
 //		if (Debug)
//			printf("***ERROR: Log file is not open (Use TX FILE OPEN filename)\n");
		return 0;
	}

	if (!PortOpen)
	{
//		if (Debug)
//			printf("***ERROR: Serial port not open (Use TX PORT OPEN port)\n");
		return 0;
	}

	if (MustPrintHeading)
	{
		MustPrintHeading = false;
/*		fprintf(fp, "X,Y,");
		for (int c = 0; c < 10; c++)
			fprintf(fp, "P%d,", c);
		for (int c = 0; c < 10; c++)
			fprintf(fp, "S%d,", c);
		fprintf(fp, "msTillPower:[%d], StableCount,NotStableCount,", power_timeout);
		fprintf(fp, "Old_Error,");
		fprintf(fp, "Old_Count,");
		fprintf(fp, "IQ_Error,");
		fprintf(fp, "IQ_Count,");
		fprintf(fp, "Qi_Both,");
		fprintf(fp, "Result,Message\n", power_timeout);
*/
	}

	// start the results with the given co-ordinates
//	fprintf(fp, "%d,%d,", x, y);

	// enable 'Q' messages
	old_demod_error_packet_count2 = 0;
	old_demod_correct_packet_count = 0;
	iq_demod_error_packet_count = 0;
	iq_demod_correct_packet_count = 0;
	count_of_packets_correctly_demodulated = 0;

//	P010000000100000000
	Transmit_Message("P010009B02100000000");			// LOG on + Enable power coil info + send Qi Control Error + Q messages

// 	Transmit_Message("P010000000100000000");  // Send P Command to allow for Coil State Q Message

	// perform a SCAN-AND-POWER
//	if (Debug)
//		printf("Turning on the power coils\n");
	Transmit_Message("X");	// reset
	Transmit_Message("K");

	TxScanDone = true;
	return true;
}

/*----------------------------------------------------------------------
 * Scan and Power ON only (power_timeout)
 *----------------------------------------------------------------------*/
bool PollTxMessages()
{

	TxScanDone = false;

	// look at all incoming messages for 'power_timeout' period
//	if (Debug)
//		printf("Waiting for power to stabalise\n");
		int len = FetchOneMessages(10);
//		int len = FetchOneMessages2(10);
//        int len = 0;
		if (len > 0)
		{
			rxbuf[len-1] = '\0';

			if (strncmp(&rxbuf[2], "QF00", 4) == 0)
			{
//				if (Debug)
//					printf("---Stable at %d ms\n", time_till_power);
//				break;
			   //	TxScanDone = true;
				return true;
			}
			else if (strncmp(&rxbuf[2], "QA0", 3) == 0)

				ProcessPowerPacket(&rxbuf[2]);
				return true;

			// find any errors
			if (IsErrorMessage())
			{
		 //		Transmit_Message("B");		// turn off power
				return false;
			} // End If

		} // End If

//		Application->ProcessMessages();

return false;
//	return true;
}

/*----------------------------------------------------------------------
 * Scan and Power ON only (power_timeout)
 *----------------------------------------------------------------------*/
bool ReadTxMessages(int pCoilStatus[10][2])
{

	TxScanDone = false;

	// Write POWERED status
	AnsiString AllCoilStatus = "";
//	int powered = 0;
	for (int c = 0; c < 10; c++)
	{

		pCoilStatus[c][0] = pcoil_status[c];

		switch (pcoil_status[c])
		{
			case 0:
				//fprintf(fp, ",");
				AllCoilStatus = AllCoilStatus + "Nothing";
				break;
			case 1:
				//fprintf(fp, "Detected,");
				AllCoilStatus = AllCoilStatus + "Detected";
				break;
			case 2:
				//fprintf(fp, "Nearby,");
				AllCoilStatus = AllCoilStatus + "Nearby";
				break;
			case 3:
				//fprintf(fp, "Coupled,");
				AllCoilStatus = AllCoilStatus + "Coupled";
				break;
			case 4:
				//fprintf(fp, "POWERED,"); powered++;
				AllCoilStatus = AllCoilStatus + "POWERED";
				break;
			case 5:
				//fprintf(fp, "CommsError,");
				AllCoilStatus = AllCoilStatus + "CommsError";
				break;
			case 6:
				//fprintf(fp, "NotCoupled,");
				AllCoilStatus = AllCoilStatus + "NotCoupled";
				break;
			case 7:
				//fprintf(fp, "FailedFOD,");
				AllCoilStatus = AllCoilStatus + "FailedFOD";
				break;
			default:
				AllCoilStatus = AllCoilStatus + "Unknown";
				break;
		} // End Switch
	}  // End For


	// Write Signal Strength
	for (int c = 0; c < 10; c++)
	{
		//fprintf(fp, "%d,", pcoil_signal_strength[c]);
		pCoilStatus[c][1] = pcoil_signal_strength[c];

	} // End For

	TxScanDone = true;
	return true;
}


/*----------------------------------------------------------------------
 * Scan and Power ON only (power_timeout)
 *----------------------------------------------------------------------*/
bool ScanAndPowerONPlotter(int power_timeout, int stable_timeout, int x, int y)
{

	TxScanDone = false;

	// enable 'Q' messages
	old_demod_error_packet_count2 = 0;
	old_demod_correct_packet_count = 0;
	iq_demod_error_packet_count = 0;
	iq_demod_correct_packet_count = 0;
	count_of_packets_correctly_demodulated = 0;

//	P010000000100000000
	Transmit_Message("P010009B02100000000");			// LOG on + Enable power coil info + send Qi Control Error + Q messages

	Transmit_Message("P010000000100000000");  // Send P Command to allow for Coil State Q Message

	// perform a SCAN-AND-POWER
//		printf("Turning on the power coils\n");
	Transmit_Message("X");	// reset
	Transmit_Message("K");

	TxScanDone = true;
	return true;
}

/*----------------------------------------------------------------------
 * Set FSK Depth
 *----------------------------------------------------------------------*/
bool SetFSKDepth(char *msgCommandFSKDepth)
{

	Transmit_Message(msgCommandFSKDepth); // FSK Depth eg W6Exx00 where x = 00 to 03 max

//	int len = FetchOneMessages(5);
//		while (len > 0)
//		{
			if (rxbuf[2] == 'w')
			{
				return true;
			}
			else
			{
				return false;
			} // End If
//				return &rxbuf[7];
//			len = FetchOneMessages(500);
//		}

//	return true;

}


/*----------------------------------------------------------------------
 * Scan and Power (power_timeout)
 *----------------------------------------------------------------------*/
bool ScanAndPowerOFF(int power_timeout, int stable_timeout, int x, int y)
{

	// turn off the power coils
//	if (Debug)
//		printf("Turning off the power coils\n");
	Transmit_Message("B");

   //	ShowMessage("sd");

	// disable 'Q' messages
	Transmit_Message("P010008B02100000000");			// Disable Q messages	(don't think it's necessary, but still)

	// save the result
//	if (Debug)
//		printf("Saving results\n");

/*
	// write POWERED status
	int powered = 0;
	for (int c = 0; c < 10; c++)
	{
		switch (pcoil_status[c])
		{
			case 0: fprintf(fp, ","); break;
			case 1: fprintf(fp, "Detected,"); break;
			case 2: fprintf(fp, "Nearby,"); break;
			case 3: fprintf(fp, "Coupled,"); break;
			case 4: fprintf(fp, "POWERED,"); powered++; break;
			case 5: fprintf(fp, "CommsError,"); break;
			case 6: fprintf(fp, "NotCoupled,"); break;
			case 7: fprintf(fp, "FailedFOD,"); break;
			default:  break;
		}
	}

	// write signal strength
	for (int c = 0; c < 10; c++)
		fprintf(fp, "%d,", pcoil_signal_strength[c]);

	// write timeout periods
	if (Debug)
		printf("TimeTillPower:%d   StableCount:%d   NotStableCount:%d\n", time_till_power, stable_messages, not_stable_messages);
	fprintf(fp, "%d,%d,%d,", time_till_power, stable_messages, not_stable_messages);

	fprintf(fp, "%d,", old_demod_error_packet_count2);
	fprintf(fp, "%d,", old_demod_correct_packet_count);
	fprintf(fp, "%d,", iq_demod_error_packet_count);
	fprintf(fp, "%d,", iq_demod_correct_packet_count);
	fprintf(fp, "%d,", count_of_packets_correctly_demodulated);

	// write OK/FAIL
	if (powered > 0 && stable_messages > 0 && not_stable_messages == 0)
	{
		fprintf(fp, "GOOD,Powered and stable\n");
		if (Debug)
			printf("GOOD,Powered and stable\n");
	}
	else if (powered > 0 && stable_messages > 0 && not_stable_messages > 0 && stable_messages > not_stable_messages)
	{
		fprintf(fp, "FAIR,Powered but NOT stable\n");
		if (Debug)
			printf("FAIR,Powered but NOT stable\n");
	}
	else if (powered > 0 && not_stable_messages > 0)
	{
		fprintf(fp, "BAD,Powered but NOT stable\n");
		if (Debug)
			printf("FAIR,Powered but NOT stable\n");
	}
	else
	{
		fprintf(fp, "FAIL,Nothing powered\n");
		if (Debug)
			printf("FAIL,Nothing powered\n");
	}

*/

	return true;
}

/*----------------------------------------------------------------------
 * Scan and Power ON only (power_timeout)
 *----------------------------------------------------------------------*/
bool ScanAndPowerONOnceInit()
{
	if (Debug)
//		printf("Timeout:%dms X:%d Y:%d\n", power_timeout, x, y);

	if (fp == NULL)
	{
 //		if (Debug)
//			printf("***ERROR: Log file is not open (Use TX FILE OPEN filename)\n");
		return 0;
	}

	if (!PortOpen)
	{
//		if (Debug)
//			printf("***ERROR: Serial port not open (Use TX PORT OPEN port)\n");
		return 0;
	}

	if (MustPrintHeading)
	{
		MustPrintHeading = false;
/*		fprintf(fp, "X,Y,");
		for (int c = 0; c < 10; c++)
			fprintf(fp, "P%d,", c);
		for (int c = 0; c < 10; c++)
			fprintf(fp, "S%d,", c);
		fprintf(fp, "msTillPower:[%d], StableCount,NotStableCount,", power_timeout);
		fprintf(fp, "Old_Error,");
		fprintf(fp, "Old_Count,");
		fprintf(fp, "IQ_Error,");
		fprintf(fp, "IQ_Count,");
		fprintf(fp, "Qi_Both,");
		fprintf(fp, "Result,Message\n", power_timeout);
*/
	}

	// start the results with the given co-ordinates
//	fprintf(fp, "%d,%d,", x, y);

	// enable 'Q' messages
	old_demod_error_packet_count2 = 0;
	old_demod_correct_packet_count = 0;
	iq_demod_error_packet_count = 0;
	iq_demod_correct_packet_count = 0;
	count_of_packets_correctly_demodulated = 0;
	Transmit_Message("P010009B02100000000");			// LOG on + Enable power coil info + send Qi Control Error + Q messages



	return true;
}

/*----------------------------------------------------------------------
 * Scan and Power ON only (power_timeout)
 *----------------------------------------------------------------------*/
bool ScanAndPowerONOnce()
{

	// perform a SCAN-AND-POWER
	if (Debug)
//		printf("Turning on the power coils\n");
	Transmit_Message("X");	// reset
	Transmit_Message("K");

	// look at all incoming messages for 'power_timeout' period
	if (Debug)
//		printf("Waiting for power to stabalise\n");
	char *error;
	char *debug_msg;
//	int time_till_power = 0;

		int len = FetchOneMessages(500);
		if (len > 0)
		{
			rxbuf[len-1] = '\0';

			if (strncmp(&rxbuf[2], "QF00", 4) == 0)
			{
//				if (Debug)
//					printf("---Stable at %d ms\n", time_till_power);
//				break;
			}
			else if (strncmp(&rxbuf[2], "QA0", 3) == 0)
//				ProcessPowerPacket(&rxbuf[2]);

			// find any errors
			if (IsErrorMessage())
			{
				Transmit_Message("B");		// turn off power
				return false;
			}
		}

//	ProcessPowerPacket2(&rxbuf[2]);

	return true;
}

/*----------------------------------------------------------------------
 * Scan and Power ON only (power_timeout)
 *----------------------------------------------------------------------*/
bool ScanAndPowerONOnceRead()
{

	// look at all incoming messages for 'power_timeout' period
	if (Debug)
//		printf("Waiting for power to stabalise\n");
	char *error;
	char *debug_msg;
//	int time_till_power = 0;

		int len = FetchOneMessages(500);
		if (len > 0)
		{
			rxbuf[len-1] = '\0';

			if (strncmp(&rxbuf[2], "QF00", 4) == 0)
			{
//				if (Debug)
//					printf("---Stable at %d ms\n", time_till_power);
//				break;
			}
			else if (strncmp(&rxbuf[2], "QA0", 3) == 0)
//				ProcessPowerPacket(&rxbuf[2]);

			// find any errors
			if (IsErrorMessage())
			{
				Transmit_Message("B");		// turn off power
				return false;
			}
		}

	ProcessPowerPacket2(&rxbuf[2]);

	return true;
}
/*----------------------------------------------------------------------
 * Report Tx data
 *----------------------------------------------------------------------*/
AnsiString ReportTxReadData()
{
	AnsiString TxMsgReply;

	for (int i = 0; i < 30; i++) {
		TxMsgReply =  TxMsgReply + rxbufReport[i];
	}
	return TxMsgReply;
}

