/*
=======================================================================
 File Name         : Lib_Misc.h
 File Version      : 1.0
 Creation Date     : 8 Dec 2014
 Programmed By     : ??
 File Description  : USB Meters

 NOTE
 Edited version of Code used by PbP for Plotter

 Ver  Date     Name     Comment
 1.0  8/12/14  GC       Release'

=======================================================================}
*/


//---------------------------------------------------------------------------

#ifndef Lib_MiscH
#define Lib_MiscH

#include <System.Classes.hpp>

#include <stdio.h> // For Files

//#include <System.Classes.hpp>

void __fastcall testLib();
AnsiString __fastcall CreateFileName2(AnsiString FolderName, AnsiString Filename);
//AnsiString __fastcall WriteToCSV(AnsiString FileName, AnsiString Data, AnsiString DefaultTitle);
AnsiString __fastcall WriteCSV(AnsiString FileName, AnsiString Data, bool Show_Message);
AnsiString __fastcall ReadCSV(AnsiString FileName, bool Show_Message);


bool __fastcall isNumber(const AnsiString s);
AnsiString __fastcall Float2NoDecimalString(double value);
void __fastcall Wait_mS(int WaitTimemS);

AnsiString __fastcall SplitStringFrom0(AnsiString String2break, int SubtextNumbFrom0, char delimiter);

float roundoff(float num,int precision);

//---------------------------------------------------------------------------
#endif
