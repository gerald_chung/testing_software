/*
=======================================================================
 File Name         : x.cpp
 File Version      : 1.0
 Creation Date     : 4 Dec 2014
 Programmed By     : Gerald Chung
 File Description  : Main File

 NOTE

=======================================================================}
*/

//---------------------------------------------------------------------------
#include <Registry.hpp>
#include <inifiles.hpp>
#include <vcl.h>
#pragma hdrstop

#include "IniFile.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm2 *Form2;
//---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner)
	: TForm(Owner)
{
}

//---------------------------------------------------------------------------
__fastcall TForm2::test()
{
	ShowMessage("hello");
}

//---------------------------------------------------------------------------
void __fastcall TForm2::Button1Click(TObject *Sender)
{
			 Form2->Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm2::Button2Click(TObject *Sender)
{

	/* Open an instance. */
	TCustomIniFile* SettingsFile = OpenIniFileInstance();
	// Store current form properties to be used in later sessions.
	ShowMessage("Now Write");
	try
	{
		SettingsFile->WriteInteger ("Name", "Top", 1);
//		SettingsFile->WriteInteger (Name, "Left", Left);
//		SettingsFile->WriteInteger (Name, "Width", Width);
//		SettingsFile->WriteInteger (Name, "Height", Height);
		SettingsFile->WriteString  ("Name", "Caption", "String");
//		SettingsFile->WriteBool    ("Name", "InitMax", WindowState == wsMaximized );
		SettingsFile->WriteBool    ("Name", "InitMax", "true");
	}
	catch(Exception* e)
	{
	}

	delete SettingsFile;

	ShowMessage("Done");

}
//---------------------------------------------------------------------------

TCustomIniFile* __fastcall TForm2::OpenIniFileInstance()
{
	/*
	Open/create a new INI file that has the same name as
	your executable, only with the INI extension.
	*/
 //		ShowMessage("Open Ini");
//	switch (RadioGroup1->ItemIndex)
	switch (1)
	{
		case 0:
			/* Registry mode selected: in HKEY_CURRENT_USER\Software\... */
			//return new TRegistryIniFile(String("Software\\") + Application->Title);
			ShowMessage("hi");
		case 1:
			/* Ini file mode selected */
			return new TIniFile(ChangeFileExt(Application->ExeName, ".INI"));
			 //	ShowMessage("Return ini");
			//return new TIniFile("inifile.txt");
		case 2:
			/* Memory based Ini file mode selected */
			//return new TMemIniFile(Change*FileExt(Application->ExeName, ".INI"));
			ShowMessage("hi");
	  }
}

//---------------------------------------------------------------------------
void __fastcall TForm2::Button3Click(TObject *Sender)
{
	TIniFile * StartUp = new TIniFile("inifile.ini");
//	 ShowMessage("Create now write");
//	StartUp->WriteInteger("FormPos", "Left", 2);
	StartUp->WriteString  ("Name", "Caption", "Caption");
//  StartUp->WriteInteger("FormPos", "Top", Top);
//    ShowMessage("Done write");
	delete StartUp;

}
//---------------------------------------------------------------------------

void __fastcall TForm2::Button4Click(TObject *Sender)
{
int Top;
    /* Open an instance */
	TCustomIniFile* SettingsFile = OpenIniFileInstance();

		Top = SettingsFile->ReadInteger("Name", "Top", 1 );
//		Caption = SettingsFile->ReadString ("Name", "Caption", "Caption");

		// Load last window state
		if (SettingsFile->ReadBool("Name", "InitMax", 0))
//			WindowState = wsMaximized;'
			ShowMessage("true");
		else
//			WindowState = wsNormal;
			ShowMessage("false");

}
//---------------------------------------------------------------------------

