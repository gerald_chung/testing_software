// CodeGear C++Builder
// Copyright (c) 1995, 2012 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'AsyncPro.pas' rev: 24.00 (Windows)

#ifndef AsyncproHPP
#define AsyncproHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <OoMisc.hpp>	// Pascal unit
#include <AdExcept.hpp>	// Pascal unit
#include <AdStrMap.hpp>	// Pascal unit
#include <AwUser.hpp>	// Pascal unit
#include <LnsQueue.hpp>	// Pascal unit
#include <AdPort.hpp>	// Pascal unit
#include <LNSWin32.hpp>	// Pascal unit
#include <AdSelCom.hpp>	// Pascal unit
#include <AdTapi.hpp>	// Pascal unit
#include <AdTUtil.hpp>	// Pascal unit
#include <AdTSel.hpp>	// Pascal unit
#include <AdTStat.hpp>	// Pascal unit
#include <AdStatLt.hpp>	// Pascal unit
#include <Winapi.Windows.hpp>	// Pascal unit
#include <System.Internal.ExcUtils.hpp>	// Pascal unit
#include <System.SysUtils.hpp>	// Pascal unit
#include <System.VarUtils.hpp>	// Pascal unit
#include <System.Variants.hpp>	// Pascal unit
#include <System.Math.hpp>	// Pascal unit
#include <System.TimeSpan.hpp>	// Pascal unit
#include <System.SyncObjs.hpp>	// Pascal unit
#include <System.Generics.Defaults.hpp>	// Pascal unit
#include <System.Rtti.hpp>	// Pascal unit
#include <System.TypInfo.hpp>	// Pascal unit
#include <System.Classes.hpp>	// Pascal unit
#include <System.DateUtils.hpp>	// Pascal unit
#include <System.IOUtils.hpp>	// Pascal unit
#include <System.Win.Registry.hpp>	// Pascal unit
#include <Vcl.Graphics.hpp>	// Pascal unit
#include <System.Actions.hpp>	// Pascal unit
#include <Vcl.ActnList.hpp>	// Pascal unit
#include <System.HelpIntfs.hpp>	// Pascal unit
#include <Winapi.UxTheme.hpp>	// Pascal unit
#include <Vcl.GraphUtil.hpp>	// Pascal unit
#include <Vcl.StdCtrls.hpp>	// Pascal unit
#include <Winapi.ShellAPI.hpp>	// Pascal unit
#include <Vcl.Printers.hpp>	// Pascal unit
#include <Vcl.Clipbrd.hpp>	// Pascal unit
#include <Vcl.ComCtrls.hpp>	// Pascal unit
#include <Vcl.Dialogs.hpp>	// Pascal unit
#include <Vcl.ExtCtrls.hpp>	// Pascal unit
#include <Vcl.Themes.hpp>	// Pascal unit
#include <Winapi.FlatSB.hpp>	// Pascal unit
#include <Vcl.Forms.hpp>	// Pascal unit
#include <Vcl.Menus.hpp>	// Pascal unit
#include <System.Win.ComObj.hpp>	// Pascal unit
#include <Vcl.Controls.hpp>	// Pascal unit
#include <Vcl.AxCtrls.hpp>	// Pascal unit
#include <Vcl.OleCtrls.hpp>	// Pascal unit
#include <Vcl.Buttons.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Asyncpro
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
}	/* namespace Asyncpro */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_ASYNCPRO)
using namespace Asyncpro;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// AsyncproHPP
