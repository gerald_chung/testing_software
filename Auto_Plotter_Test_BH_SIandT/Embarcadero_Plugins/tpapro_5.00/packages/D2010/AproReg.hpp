// CodeGear C++Builder
// Copyright (c) 1995, 2012 by Embarcadero Technologies, Inc.
// All rights reserved

// (DO NOT EDIT: machine generated header) 'APROReg.pas' rev: 24.00 (Windows)

#ifndef AproregHPP
#define AproregHPP

#pragma delphiheader begin
#pragma option push
#pragma option -w-      // All warnings off
#pragma option -Vx      // Zero-length empty class member 
#pragma pack(push,8)
#include <System.hpp>	// Pascal unit
#include <SysInit.hpp>	// Pascal unit
#include <DesignIntf.hpp>	// Pascal unit
#include <DesignEditors.hpp>	// Pascal unit

//-- user supplied -----------------------------------------------------------

namespace Aproreg
{
//-- type declarations -------------------------------------------------------
//-- var, const, procedure ---------------------------------------------------
#define AsyncProfessional_TabBaseName L"AsyncPro"
#define APROTabName L"AsyncPro General"
#define APROFaxTabName L"AsyncPro Fax"
#define APROTelephonyTabName L"AsyncPro Telephony"
#define APROStateTabName L"AsyncPro State Machine"
extern DELPHI_PACKAGE void __fastcall Register(void);
}	/* namespace Aproreg */
#if !defined(DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE) && !defined(NO_USING_NAMESPACE_APROREG)
using namespace Aproreg;
#endif
#pragma pack(pop)
#pragma option pop

#pragma delphiheader end.
//-- end unit ----------------------------------------------------------------
#endif	// AproregHPP
