#ifndef TXMAIN_H
#define TXMAIN_H

#include "main.h"

#define DEFAULT_BUFLEN 		5120
#define DEFAULT_PORT 		"27016"

#define NAME				"Proxi-2D TX"
#define MAJOR				1
#define MINOR				0

#endif
