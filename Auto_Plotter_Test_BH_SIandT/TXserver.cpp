/***************************************************************************H1*

	  Filename    : TXserver.cpp
      Description : TX application server
      Created     : July 2015

*******************************************************************************
					Copyright (c) 2015 - Michael Heyns
	   This file contains confidential and proprietary information.
	  All use, disclosure, and/or reproduction is strictly prohibited.
						   All rights reserved.
*******************************************************************************
******************************************************************************/

#include "TXmain.h"

extern FILE *fp;

static void Tokenise(void);

#pragma comment (lib, "Ws2_32.lib")

SOCKET ListenSocket = INVALID_SOCKET;
SOCKET ClientSocket = INVALID_SOCKET;

#define MAXTOKENS	40
#define LINELEN		(MAXTOKENS*2)+1
static int TokenCount;
static char *Token[MAXTOKENS];

static bool PrintedHeading = false;
bool Debug = false;

static char recvbuf[DEFAULT_BUFLEN];

bool txOpenFile(char *fname);
void txCloseFile(void);
char * GetTxVersion(void);
bool OpenTxPort(int port);
void CloseTxPort(void);
int ScanFODA(int N);
bool ScanAndPower(int power_timeout, int stable_timeout, int x, int y);

/*----------------------------------------------------------------------
 * Send back a reply to the client
 *----------------------------------------------------------------------*/
static bool Reply(char *data, int len)
{
	int iSendResult = send( ClientSocket, data, len, 0 );
	if (iSendResult == SOCKET_ERROR)
	{
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ClientSocket);
		WSACleanup();
		return false;
	}
	return true;
}

#define C1(x)		(TokenCount == 1 && stricmp(Token[0], x) == 0)
#define C1Ppp(x)  	(TokenCount > 1 && stricmp(Token[0], x) == 0)
#define C2(x,y)    	(TokenCount == 2 && stricmp(Token[0], x) == 0 && stricmp(Token[1], y) == 0)
#define C3(x,y,z)  	(TokenCount == 3 && stricmp(Token[0], x) == 0 && stricmp(Token[1], y) == 0 && stricmp(Token[2], z) == 0)
#define C2P(x,y)  	(TokenCount == 3 && stricmp(Token[0], x) == 0 && stricmp(Token[1], y) == 0)
#define C3PPPP(x,y) (TokenCount == 6 && stricmp(Token[0], x) == 0 && stricmp(Token[1], y) == 0)

/*----------------------------------------------------------------------
 * Process the receive buffer
 *----------------------------------------------------------------------*/
static bool ProcessRxBuffer(void)
{
	if (Debug)
	{
		printf("TokenCount=%d\n", TokenCount);
		for (int i = 0; i < TokenCount; i++)
			printf("[%s] ", Token[i]);
		printf("\n");
	}

	if (C1("EXIT"))
	{
		printf("Shutting down the server\n");
		Reply("OK", 2);
		return false;
	}
	else if (C2("SERVER", "VERSION"))
	{
		char buf[100];
		sprintf(buf, "%s - Server Version %d.%d\n", NAME, MAJOR, MINOR);
		if (fp != NULL)
			fprintf(fp, "%s\n", buf);
		return Reply(buf, strlen(buf));
	}
	else if (C2("FIRMWARE", "VERSION"))
	{
		char *version = GetTxVersion();
		if (fp != NULL)
			fprintf(fp, "Tx Firmware version: %s\n", version);
		return Reply(version, strlen(version));
	}
	else if (C2("DEBUG", "ON"))
	{
		printf("Debug now ON\n");
		Debug = true;
		Reply("OK", 2);
		return true;
	}
	else if (C2("DEBUG", "OFF"))
	{
		printf("Debug now OFF\n");
		Debug = false;
		Reply("OK", 2);
		return true;
	}
	else if (C2P("FILE", "OPEN"))
	{
		if (!txOpenFile(Token[2]))
		{
			Reply("FAIL", 4);
			return true;
		}
		Reply("OK", 2);
		return true;
	}
	else if (C2("FILE", "CLOSE"))
	{
		txCloseFile();
		Reply("OK", 2);
		return true;
	}
	else if (C2P("PORT", "OPEN"))
	{
		if (!OpenTxPort(atoi(Token[2])))
		{
			Reply("FAIL", 4);
			return true;
		}
		Reply("OK", 2);
		return true;
	}
	else if (C2("PORT", "CLOSE"))
	{
		CloseTxPort();
		Reply("OK", 2);
		return true;
	}
	else if (C2P("FODA", "SCAN"))
	{
		int N = ScanFODA(atoi(Token[2]));
		if (N == 0)
		{
			Reply("FAIL", 4);
			return true;
		}
		Reply("OK", 2);
		return true;
	}
	else if (C3PPPP("POWER", "SCAN"))
	{
		if (!ScanAndPower(atoi(Token[2]), atoi(Token[3]), atoi(Token[4]), atoi(Token[5])))
		{
			Reply("FAIL", 4);
			return true;
		}
		Reply("OK", 2);
		return true;
	}
	else if (C1Ppp("MSG"))
	{
		time_t now = time(NULL);

		static char answer[100];
		strcpy(answer,ctime(&now));
		answer[strlen(answer) - 1] = '\0';
		strcat(answer, ":");

		for (int i = 1; i < TokenCount; i++)
		{
			strcat(answer, " ");
			strcat(answer, Token[i]);
		}
		if (fp != NULL)
			fprintf(fp, "%s\n", answer);
		return Reply(answer, strlen(answer));
	}

	if (Debug)
		printf("***ERROR: Command error\n");
	return Reply("FAIL", 4);
}

/*----------------------------------------------------------------------
 * Tokenises the command line
 *----------------------------------------------------------------------*/
static void Tokenise(void)
{
	char *line = recvbuf;
	TokenCount = 0;

	if (Debug)
		printf("RxBUF: '%s'\n", recvbuf);

	// skip initial white spaces
	while (isspace(*line)) line++;
	Token[TokenCount] = line;

	while (*line)
	{
		if (*line == '"')
		{
			// end off current word (if any)
			*line++ = '\0';
			if (Token[TokenCount][0])
				TokenCount++;

			// reset the start of the word containing the string
			Token[TokenCount] = line;

			// find the end of the string
			while (*line && *line != '"' && *line != '\n' && *line != '\r')
				line++;

			// end off the string
			*line++ = '\0';	// kill ending '"'

			// if no more words left, we're done
			if (TokenCount == MAXTOKENS-1)
				break;

			// find start of next word
			while (*line && isspace(*line))
				line++;
			Token[++TokenCount] = line;
		}
		else if (isspace(*line))
		{
			// end off current word
			*line++ = '\0';

			// if no more words left, we're done
			if (TokenCount == MAXTOKENS-1)
				break;

			// find start of next word
			while (*line && isspace(*line))
				line++;
			if (*line)
				Token[++TokenCount] = line;
		}
		else
			line++;
	}
	if (Token[TokenCount][0])
		TokenCount++;
}

// ===================================================================================================

/*----------------------------------------------------------------------
 * Server
 *----------------------------------------------------------------------*/
int RunAsServer(void)
{
	WSADATA wsaData;
	int iResult;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	if (!PrintedHeading)
	{
		printf("*** %s - Copyright(c) 2015 - M Heyns - All rights reserved ***\n", NAME);
		PrintedHeading = true;
	}

	int recvbuflen = DEFAULT_BUFLEN;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if ( iResult != 0 ) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iResult = bind( ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);
	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// Accept a client socket
	ClientSocket = accept(ListenSocket, NULL, NULL);
	if (ClientSocket == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// No longer need server socket
	closesocket(ListenSocket);

	// Receive until the peer shuts down the connection
	do {
		iResult = recv(ClientSocket, recvbuf, recvbuflen, 0);
		if (iResult > 0)
		{
			recvbuf[iResult] = '\0';

			Tokenise();
			if (!ProcessRxBuffer())
			{
				closesocket(ClientSocket);
				WSACleanup();
				return 1;
			}
		}
		else if (iResult != 0)
		{
			printf("recv failed with error: %d\n", WSAGetLastError());
			closesocket(ClientSocket);
			WSACleanup();
			return 1;
		}

    } while (iResult > 0);

    // shutdown the connection since we're done
	iResult = shutdown(ClientSocket, SD_SEND);
    if (iResult == SOCKET_ERROR) {
        printf("shutdown failed with error: %d\n", WSAGetLastError());
        closesocket(ClientSocket);
        WSACleanup();
        return 1;
	}

	// cleanup
	closesocket(ClientSocket);
	WSACleanup();
	return 0;
}

void __cdecl cleanup(void)
{
	CloseTxPort();
	txCloseFile();
}


