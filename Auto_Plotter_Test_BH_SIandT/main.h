#ifndef MAIN_H
#define MAIN_H

#ifndef MAIN_MODULE
	#define SCOPE		extern
#else
	#define SCOPE
#endif

#undef UNICODE

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <vcl.h>

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <process.h>
#include <string.h>
#include <conio.h>
#include <time.h>

#define MAXPORTS	64
#include "_SerialIO2.h"

#define LOCALHOST		"127.0.0.1"

#define max(a,b)    	(((a) > (b)) ? (a) : (b))
#define min(a,b)    	(((a) < (b)) ? (a) : (b))

#endif
