//---------------------------------------------------------------------------

#pragma hdrstop

#include "AGILENT_34970A.h"
#include "visa.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

bool ManualRun = false;

//============================================================================
//============================================================================
//============================================================================
//		AGILENT VISA CONTROLLER FUNCTION IMPLEMENTATIONS
//============================================================================
//============================================================================
//============================================================================


//---------------------------------------------------------------------------
// Add a line to the USB connection log...
//---------------------------------------------------------------------------
void __fastcall AGILENT_Interface::AddToLog(AnsiString line)
{
	Memo->Lines->Add(line);
}

void __fastcall AGILENT_Interface::DecodeRetString(char *buff, double *retValues)
{

	AnsiString srcString;	//return string from Agilent
	srcString += buff;
	srcString += ',';
	AnsiString strValues[4]; //seperated return value strings
	uint32_t lastIndex = 0;
	uint32_t curValue = 0;
	uint32_t i;

	//break up return string into seperate values
	for(i=0;i<(uint32_t)srcString.Length();i++){

		if(srcString.c_str()[i] == ',' || srcString.c_str()[i] == '\r' || srcString.c_str()[i] == '\n') {
			strValues[curValue] = (curValue == 0) ? srcString.SubString(lastIndex,i-lastIndex) : srcString.SubString(lastIndex+2,i-lastIndex-1);
			lastIndex = i;
			curValue++;
		}

	}

	//convert seperated return strings into float
	retValues[0]=atof(strValues[0].c_str());
	retValues[1]=atof(strValues[1].c_str());
	retValues[2]=atof(strValues[2].c_str());
	retValues[3]=atof(strValues[3].c_str());

	//Show data for Diagnostics Panel
	AddToLog(".................");
	AddToLog("DecodeRetString Procedure");
	AddToLog("Data 1 -> " + strValues[0]);
	AddToLog("Data 2 -> " + strValues[1]);
	AddToLog("Data 3 -> " + strValues[2]);
	AddToLog("Data 4 -> " + strValues[3]);
	AddToLog(".................");
}




typedef enum
{
	AGILENT_IDLE_STATE,
	AGILENT_RESET_STATE,
	AGILENT_SEND_REQ_STATE,
	AGILENT_SEND_REQ_WAIT_STATE,
	AGILENT_DO_DATA_STATE,

} AGILENT_state_t;

AGILENT_state_t DMM_State = AGILENT_IDLE_STATE;
	double data[4*AGILENT_SAMPLES_TO_READ];


void __fastcall AGILENT_Interface::DMM_Task(void)
{

static char retString[256];
static uint32_t samples;

	ViUInt32 retReadCount =0;
//	uint32_t read;


	switch (DMM_State)
	{

	   case AGILENT_IDLE_STATE:
			break;

		case AGILENT_RESET_STATE:
			samples = 0;
			viPrintf (AGILENT_Session, "*RST\n");
			DMM_State = AGILENT_SEND_REQ_STATE;
			break;

		case AGILENT_SEND_REQ_STATE:
			 viPrintf (AGILENT_Session, "MEAS:VOLT:DC? (@101,102,103,104)\n");
			 DMM_State = AGILENT_SEND_REQ_WAIT_STATE;
			 break;

		case AGILENT_SEND_REQ_WAIT_STATE:

			viRead(AGILENT_Session,retString,sizeof(retString)-1,&retReadCount);

			if (retReadCount)
				{
				retString[retReadCount] = 0;
				DecodeRetString(retString,&data[samples*4]);

				samples ++;

				if (samples >= AGILENT_SAMPLES_TO_READ )
					{
					DMM_State =	 AGILENT_DO_DATA_STATE;  //We are all done reading
					}
				 else
					{
					DMM_State =	 AGILENT_SEND_REQ_STATE;
					}
				}

			break;

		case AGILENT_DO_DATA_STATE:

		   //clear buffer
			curValues[0] = 0;
			curValues[1] = 0;
			curValues[2] = 0;
			curValues[3] = 0;
			//add all samples
			for(int i=0; i<AGILENT_SAMPLES_TO_READ; i++)
			{
				curValues[0] += data[(i*4)+0];
				curValues[1] += data[(i*4)+1];
				curValues[2] += data[(i*4)+2];
				curValues[3] += data[(i*4)+3];
			}
			//calculate average
			curValues[0] = curValues[0] / AGILENT_SAMPLES_TO_READ;
			curValues[1] = curValues[1] / AGILENT_SAMPLES_TO_READ;
			curValues[2] = curValues[2] / AGILENT_SAMPLES_TO_READ;
			curValues[3] = curValues[3] / AGILENT_SAMPLES_TO_READ;

			dataReady = true;
			DMM_State =	 AGILENT_IDLE_STATE;
		break;

	}

}


AGILENT_ret_t __fastcall AGILENT_Interface::GetAnalogInputs(double *buff)
{
	uint32_t read, samples = 0;
	uint32_t i, WhileCnt;
	AGILENT_ret_t result = AGILENT_OK;
	double data[4*AGILENT_SAMPLES_TO_READ];
	char retString[256];

	retString[255] = 0;
	ViUInt32 retReadCount =0;

	bool buffread = false;

try
{

	//While the following either condition is true, do things in the while loop
	//What we want is buffread to be true the first time and buff[0]==0 is not true
	//It will go into while loop. Second time around buffread equals true so buffread
	//condition becomes false, and buff[0]==0 maybe false too so it will break out of while loop.

	WhileCnt = 1;  // Loop monitor

//	while(buffread == false || buff[0] < 18.0  && AGILENT_Session != 0)
	//NOTE Input voltage must be greateer than set voltage
//**************
//	while(buffread == false || buff[0] < 2.0)
	do
//**************
	// NOTE Check buff[0] value check below!!
	{
		Application->ProcessMessages( );

		// Reset Data Logger
		viPrintf (AGILENT_Session, "*RST\n");

//		ShowMessage("Wait");
		Sleep(100);
		AddToLog("--------------");
		//repeat measurements SAMPLES_TO_READ nr of times
		for (samples = 0; samples < AGILENT_SAMPLES_TO_READ; samples++)
		{
			AddToLog("Read Data Count = " + FloatToStr(samples+1));
			//Take 4 voltage measurements, autorange, maximum resolution (defaults)
			viPrintf (AGILENT_Session, "MEAS:VOLT:DC? (@101,102,103,104)\n");
			Sleep(750);
			//read the results
			viRead  (AGILENT_Session,retString,255,&retReadCount);
			Sleep(750);
			Application->ProcessMessages( );
//			viPrintf (AGILENT_Session, "*RST\n");
			//can test if retReadCount == 0 option
			DecodeRetString(retString,&data[samples*4]);
			//Monitor Data
		}  // End For

		AddToLog("--------------");
		AddToLog("###DECODE DONE###");
		AddToLog("");

		//average results, and store in buff parameter
		if(samples > 0)
		{   //clear buffer
			buff[0] = 0;
			buff[1] = 0;
			buff[2] = 0;
			buff[3] = 0;
			//add all samples
			for(i=0; i<samples; i++)
			{
				buff[0] += data[(i*4)+0];
				buff[1] += data[(i*4)+1];
				buff[2] += data[(i*4)+2];
				buff[3] += data[(i*4)+3];
			}
			//calculate average
			buff[0] = buff[0] / samples;
			buff[1] = buff[1] / samples;
			buff[2] = buff[2] / samples;
			buff[3] = buff[3] / samples;

			//Flag to make sure that the first sample is read
			buffread = true;
//			AddToLog("Buff Read is TRUE");
			Application->ProcessMessages( );

			//For auto control check if data is valid
			if(buff[0] < 2.0)
			{
			  AddToLog("***Vin Check Error < 2.0 Volts min***");
			  //Retry Connection until input data is good
			  //viPrintf (AGILENT_Session, "*RST\n");
			}
			else
			{
				// Do nothing
				// BeginTimer();
			} //End If
			//ENDS For auto control check if data is valid
		}
		else
		{
			ShowMessage("Samples less than 0.0");
		}  // End If

		//Check While loop count
		WhileCnt = WhileCnt++;
		Application->ProcessMessages( );
		if (WhileCnt > 5)
		{
			buffread = true;
			viPrintf (AGILENT_Session, "*RST\n");
//			ShowMessage("GetAnalogInputs While loop > 10.  Check Data Logger or POWER SUPPLY!!");
			if (MessageDlg("GetAnalogInputs While loop > 10.  Check Data Logger or POWER SUPPLY!!  YES = Exit Retry Loop, NO = Run again",mtConfirmation,mbYesNo,0) == mrYes)
			{
				// Exit Loop
				break;
			}
			else
			{
				//  Do nothing
			}  // End If

			Sleep(1000);
		}
			else
		{
			//Do nothing
		} //End For
		// END Check While loop count

		Application->ProcessMessages( );

		//Check if Manually running ---
/*		if (ManualRun)
		{
			ShowMessage("Breaked");
		}
		else
		{
		   //Do nothing
		   ShowMessage("NOT Breaked");
//			break;
		} // End If
		//Ends Check if Manually running ---
*/

//	}  //End While
//	} while((buffread == false || buff[0] < 2.0) && !ManualRun && !(AGILENT_Session == 0));
	} while((buffread == false) && !ManualRun && !(AGILENT_Session == 0));
	//End Do Loop

	return result;
}
	catch (...)
	{
		ShowMessage("Error in GetAnalogInputs");
		ShowMessage(FloatToStr(buff[0])+ " " +FloatToStr(buff[1]) + " " + FloatToStr(buff[2]) + " " + FloatToStr(buff[3]));
	}
	return result;
}

//---------------------------------------------------------------------------
// Tell all threads to go and get a new measurement
//---------------------------------------------------------------------------
void __fastcall AGILENT_Interface::StartNewMeasurement(void){

	//if the AGILENT task is available
	if(AGILENT_Session != 0)
	{
		//invalidate current data
		dataReady = false;
		//get new data
		AddToLog("About to Get Data ---------->");

		DMM_State = AGILENT_RESET_STATE;


		if(GetAnalogInputs(&newValues[0]) != AGILENT_OK)
		{
			AddToLog("Error getting new measurement");
			Stop();
		} else
		{
			AddToLog("Got Valid measurement Data");
			curValues[0] = newValues[0];
			curValues[1] = newValues[1];
			curValues[2] = newValues[2];
			curValues[3] = newValues[3];
			dataReady = true;
		} //End If


	}
	else
	{
		AddToLog("No Agilent Session!!!!!!");
	} //End If

}

//---------------------------------------------------------------------------
// Ask all threads whether new data is available, if so, return true
//---------------------------------------------------------------------------
bool __fastcall AGILENT_Interface::DataAvailable(void){
//     AddToLog("Data Available !!!!!!!!!!!");
	 return dataReady;
}

//---------------------------------------------------------------------------
// Get the latest output string of a certain Node
//---------------------------------------------------------------------------
double __fastcall AGILENT_Interface::GetOutput(uint8_t channel){
	return curValues[channel];
}

//---------------------------------------------------------------------------
// Return connection state for node
//---------------------------------------------------------------------------
bool __fastcall AGILENT_Interface::MeterConnected(uint8_t channel)
{
	   return (AGILENT_Session != 0) ? true : false;
}

//---------------------------------------------------------------------------
// Kill all threads
//---------------------------------------------------------------------------
void __fastcall AGILENT_Interface::Stop(void)
{
	/* Close session */
	viClose (AGILENT_Session);
	viClose (defaultRM);
	AGILENT_Session = 0;
	AddToLog("AGILENT Task Stopped");
//	ShowMessage("Stop");
}

//---------------------------------------------------------------------------
// Attempt to start DAQ anaog in task. Return true if succesfull
//---------------------------------------------------------------------------
bool __fastcall AGILENT_Interface::Start(void)
{

	//Open session to GPIB device at address 22
	viOpenDefaultRM (&defaultRM);
	//attempt to open interface to AGILENT
	long viResult = viOpen (defaultRM, AGILENT_GPIB_ADDRESS, VI_NULL,VI_NULL, &AGILENT_Session);
	AGILENT_ret_t result = (viResult == VI_SUCCESS) ? AGILENT_OK : AGILENT_ERR;
	AddToLog("viOpen returned : " + String(viResult));

	//if succesfull configure and start
	if(result == AGILENT_OK){
		//Initialize device
		viPrintf (AGILENT_Session, "*RST\n");
		AddToLog("AGILENT Task Created");
	//if unsuccessfull, indicate to user
	} else {
		AddToLog("AGILENT TaskCreate Error");
		/* Close session */
		viClose (AGILENT_Session);
		viClose (defaultRM);
	}

	//return result
	return (result == AGILENT_OK) ? true : false ;

}
