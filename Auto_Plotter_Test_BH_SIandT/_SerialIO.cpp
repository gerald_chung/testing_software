/***************************************************************************H1*

	  Filename    : SerialIO
      Description : Handles all the serial IO
      Created     : January 1, 2003
	  Last Edit   : January 1, 2003
				  : 26 Jun 14  update -> sdWriteSerialCh

*******************************************************************************
					Copyright (c) 2003 - Michael Heyns
       This file contains confidential and proprietary information.
      All use, disclosure, and/or reproduction is strictly prohibited.
						   All rights reserved.
*******************************************************************************

Specifying Serial Ports Larger than COM9

PSS ID Number: Q115831

Authored 05-Jun-1994			Last modified 25-May-1995

The information in this article applies to:

 - Microsoft Win32 Software Development Kit (SDK), versions 3.1, 3.5,
   3.51, and 4.0

CreateFile() can be used to get a handle to a serial port. The "Win32
Programmer's Reference" entry for "CreateFile()" mentions that the share
mode must be 0, the create parameter must be OPEN_EXISTING, and the
template must be NULL.

CreateFile() is successful when you use "COM1" through "COM9" for the name

of the file; however, the message "INVALID_HANDLE_VALUE" is returned if you
use "COM10" or greater.

If the name of the port is \\.\COM10, the correct way to specify the serial
port in a call to CreateFile() is as follows:

   CreateFile(
	  "\\\\.\\COM10",     // address of name of the communications device
	  fdwAccess,          // access (read-write) mode
	  0,                  // share mode
	  NULL,               // address of security descriptor

	  OPEN_EXISTING,      // how to create
	  0,                  // file attributes
	  NULL                // handle of file with attributes to copy
   );

NOTES: This syntax also works for ports COM1 through COM9. Certain boards
will let you choose the port names yourself. This syntax works for those
names as well.

Additional reference words: 3.10 3.50 4.00 95
KBCategory: kbprg
KBSubcategory: BseCommapi

******************************************************************************/

#pragma hdrstop
#include "_SerialIO.h"
#pragma package(smart_init)

#define MAXPORTS	100
static char Name[15];
static char gblHandshaking;

/*----------------------------------------------------------------------
 * Writes a character to the port
* Writes a character to the port
  Update from Aaron 26 Jun 14
 *----------------------------------------------------------------------*/
int __fastcall sdWriteSerialCh(HANDLE hCom, char ch)
{
	BOOL rc;
	DWORD errors;
	COMSTAT comstat;
	unsigned long Written = 0;
	bool result;

	if (hCom != NULL)
	{
		// to be reliable, only send data under the following conditions:
		// 1 - only when the transmit buffer is empty
		// 2 - AND (if handshaking ON) when the CTS line says it's OK
		while (1)
		{
		   result = ClearCommError(hCom, &errors, &comstat);
		   if (result == 0)
			{//We got an error call ClearCommError.  Comport may have been unpluged
			return 0;//Report back error
			}

			if (comstat.cbOutQue == 0 && comstat.fTxim == false && !errors)
			{
				if (!gblHandshaking || (gblHandshaking && comstat.fCtsHold == 0))
					break;
			}
			Application->ProcessMessages();
		}

		// send to the port
		if (!WriteFile(hCom, &ch, 1, &Written, NULL) || Written != 1)
		{
			int retry = 0;
			do
			{
				if (retry++ == 500)
					return(0);
				Sleep(1);
				rc = WriteFile(hCom, &ch, 1, &Written, NULL);
			}
			while (rc == 0 || Written != 1);
		}
		return(1);
	}
	return(0);
}


/*----------------------------------------------------------------------
 * Writes a character buffer to a serial interface
 *----------------------------------------------------------------------*/
int __fastcall sdWriteSerial(HANDLE hCom, char *Str)
{
	int Written = 0;
	while (*Str)
		Written += sdWriteSerialCh(hCom, *Str++);
	return(Written);
}

/*----------------------------------------------------------------------
 * Writes Len characters buffer to a serial interface
 *----------------------------------------------------------------------*/
void __fastcall sdWriteSerial(HANDLE hCom, char *Str,int Len)
{
	while (Len--)
		{
		sdWriteSerialCh(hCom, *Str++);
		}

}

/*----------------------------------------------------------------------
 * Reads a character buffer from a serial interface
 *----------------------------------------------------------------------*/
int __fastcall sdReadSerial(HANDLE hCom, char *Str, unsigned long max)
{
	unsigned long read = 0;
	ReadFile(hCom, &Str[0], max, &read, NULL);

	return(read);
}

/*----------------------------------------------------------------------
 * Opens a serial interface
 * Timeout is till the FIRST character AND between each character
 *----------------------------------------------------------------------*/
HANDLE __fastcall sdOpenSerialPort(int ComPort, int Baud, int Handshaking, int ReadTimeout, int WriteTimeout)
{
	DCB dcb;
	HANDLE hCom;
	BOOL fSuccess;
	COMMTIMEOUTS CommTimeouts;

	// validate port number
	if (ComPort < 0 || ComPort >= MAXPORTS)
	{
		return(NULL);
	}

	// create the 'file'
	sprintf(Name, "\\\\.\\COM%d", ComPort);
	//sprintf(Name, "COM%d", ComPort);
	hCom = CreateFile(Name,
		GENERIC_READ | GENERIC_WRITE,
		0,    					/* comm devices must be opened w/exclusive-access */
		NULL, 					/* no security attrs */
		OPEN_EXISTING, 			/* comm devices must use OPEN_EXISTING */
		0,    					/* not overlapped I/O */
		NULL  					/* hTemplate must be NULL for comm devices */
		);

	// test handle
	if (hCom == INVALID_HANDLE_VALUE)
	{
		/* handle error */
		return(NULL);
	}

	// clear the port
	char ch;
	unsigned long now;
	while (sdDataWaiting(hCom))
		ReadFile(hCom, &ch, 1, &now, NULL);

	// flush the port
	PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

	// clear all outstanding errors
	DWORD errors;
	COMSTAT comstat;
	ClearCommError(hCom, &errors, &comstat);

	// Omit the call to SetupComm to use the default queue sizes and
	// Get the current configuration.
	fSuccess = GetCommState(hCom, &dcb);
	if (!fSuccess)
	{
		/* Handle the error. */
		CloseHandle(hCom);
		return(NULL);
	}

	// Fill in the DCB (Device Control Block)
	dcb.BaudRate = Baud;
	dcb.ByteSize = 8;
	dcb.Parity = NOPARITY;
	dcb.StopBits = ONESTOPBIT;
	dcb.fOutxDsrFlow = false;
	dcb.fDtrControl = DTR_CONTROL_ENABLE;
	dcb.fDsrSensitivity = false;

	if (Handshaking)
	{
		dcb.fOutxCtsFlow = true;
		dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;
		gblHandshaking = 1;
	}
	else
	{
		dcb.fOutxCtsFlow = false;
		dcb.fRtsControl = RTS_CONTROL_DISABLE;
		gblHandshaking = 0;
	}

	// set this state
	fSuccess = SetCommState(hCom, &dcb);
	if (!fSuccess)
	{
		/* Handle the error. */
		CloseHandle(hCom);
		return(NULL);
	}

	// switch DTR ON
	fSuccess = EscapeCommFunction(hCom, SETDTR);
	if (!fSuccess)
	{
		/* Handle the error. */
		CloseHandle(hCom);
		return(NULL);
	}

	// set a timeout value
	CommTimeouts.ReadIntervalTimeout = ReadTimeout;
	CommTimeouts.ReadTotalTimeoutMultiplier = 1;
	CommTimeouts.ReadTotalTimeoutConstant = ReadTimeout;
	CommTimeouts.WriteTotalTimeoutMultiplier = 1;
	CommTimeouts.WriteTotalTimeoutConstant = WriteTimeout;
	SetCommTimeouts(hCom, &CommTimeouts);

	// clear the port
	while (sdDataWaiting(hCom))
		ReadFile(hCom, &ch, 1, &now, NULL);

	// flush the port - again
	PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

	// clear all outstanding errors - again
	ClearCommError(hCom, &errors, &comstat);

	// save the handle
	return(hCom);
}

/*----------------------------------------------------------------------
 * Closes the port
 *----------------------------------------------------------------------*/
void __fastcall sdClosePort(HANDLE hCom)
{
	if (hCom != NULL)
		CloseHandle(hCom);
}

/*----------------------------------------------------------------------
 * Flushes the port
 *----------------------------------------------------------------------*/
void __fastcall sdFlushPort(HANDLE hCom)
{
	if (hCom != NULL)
	{
		// flush the port
		PurgeComm(hCom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);

		// clear all outstanding errors
		DWORD errors;
		COMSTAT comstat;
		ClearCommError(hCom, &errors, &comstat);
	}
}

/*----------------------------------------------------------------------
 * Returns true is data is waiting on the input queue
 *----------------------------------------------------------------------*/
int __fastcall sdDataWaiting(HANDLE hCom)
{
	int nrBytesWaiting = 0;
	if (hCom != NULL)
	{
		DWORD errors;
		COMSTAT comstat;
		ClearCommError(hCom, &errors, &comstat);
		nrBytesWaiting = comstat.cbInQue;
	}

	//return result
	return nrBytesWaiting;

}

/******************************************************************************
                                  End of file
******************************************************************************/
