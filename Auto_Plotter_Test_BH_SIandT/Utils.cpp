//---------------------------------------------------------------------------
#include <vcl.h>
#include <Registry.hpp>
#include <stdio.h>
#include <io.h>
#include <setupapi.h>
#include "Utils.h"

#pragma hdrstop

//---------------------------------------------------------------------------
#pragma package(smart_init)
#define ROOT_KEY HKEY_CURRENT_USER

/*
--> Misc Functions for Ref

if (MessageDlg("Message", mtInformation, mbYesNo, 0) == mrYes)
	 {
		ShowMessage("Yes");
	 }
	 else
	 {
		 ShowMessage("No");
	 }

		int UserAns = MessageDlg("Please Enter Option: \nYes = Move Plotter to Selected Cell \nNo = Exit", mtInformation, mbYesNoCancel, 0);
		switch (UserAns)
		{
			case mrYes:
			{

				break;
			}
			case mrNo:
			{

				break;
			}

		default:
		{
			// 	ShowMessage("No");
		   break;
		}
	}  // End Switch


*/


int __fastcall Reg_ReadInteger(AnsiString sAppRegPath,AnsiString sSection,AnsiString sValue,int iDefault)
{
TRegKeyInfo KeyInfo;
int Result;

  TRegistry *vtRegistry = new TRegistry();

  try
	{
    vtRegistry->RootKey = ROOT_KEY;
    vtRegistry->OpenKey(sAppRegPath + '\\' + sSection,true);
   	vtRegistry->GetKeyInfo(KeyInfo);
  	if (vtRegistry->ValueExists(sValue))
       {
        Result = vtRegistry->ReadInteger(sValue);
       }
    else
       {
       vtRegistry->WriteInteger(sValue,iDefault);
       Result = iDefault;
       }
    vtRegistry->CloseKey();
    }
  catch(ERegistryException &E)
		{
		ShowMessage(E.Message);
    	delete vtRegistry;
        return(Result);
        }
  delete vtRegistry;
  return(Result);
}

//==============================================================================

AnsiString __fastcall Reg_ReadString(AnsiString sAppRegPath,AnsiString sSection,AnsiString sValue,AnsiString sDefault)
{
TRegKeyInfo KeyInfo;
AnsiString Result;

  TRegistry *vtRegistry = new TRegistry();

  try
    {
    vtRegistry->RootKey = ROOT_KEY;
    vtRegistry->OpenKey(sAppRegPath + '\\' + sSection,true);
   	vtRegistry->GetKeyInfo(KeyInfo);
  	if (vtRegistry->ValueExists(sValue))
       {
		Result = vtRegistry->ReadString(sValue);
	   }
    else
       {
       vtRegistry->WriteString(sValue,sDefault);
       Result = sDefault;
       }
    vtRegistry->CloseKey();
    }
  catch(ERegistryException &E)
        {
   	    ShowMessage(E.Message);
    	delete vtRegistry;
        return(Result);
        }
  delete vtRegistry;
  return(Result);
}

//==============================================================================
void __fastcall Reg_WriteInteger(AnsiString sAppRegPath,AnsiString sSection,AnsiString sValue,int iData)
{
  TRegistry *vtRegistry = new TRegistry();
  try
    {
	vtRegistry->RootKey = ROOT_KEY;
	vtRegistry->OpenKey(sAppRegPath + '\\' + sSection,true);
    vtRegistry->WriteInteger(sValue,iData);
    vtRegistry->CloseKey();
    }
  catch(ERegistryException &E)
        {
   	    ShowMessage(E.Message);
    	delete vtRegistry;
        return;
        }
  delete vtRegistry;
  return;

}

//==============================================================================
void __fastcall Reg_WriteString(AnsiString sAppRegPath,AnsiString sSection,AnsiString sValue,AnsiString sData)
{
  TRegistry *vtRegistry = new TRegistry();
  try
    {
    vtRegistry->RootKey = ROOT_KEY;
    vtRegistry->OpenKey(sAppRegPath + '\\' + sSection,true);
    vtRegistry->WriteString(sValue,sData);
	vtRegistry->CloseKey();
	}
  catch(ERegistryException &E)
        {
   	    ShowMessage(E.Message);
    	delete vtRegistry;
        return;
        }
  delete vtRegistry;
  return;

}

// ---------------------------------------------------------------------------

unsigned int __fastcall MakeChecksum(AnsiString sData) {

	unsigned char Checksum = 0;
	unsigned char TempBuffer[255];
	AnsiString sOut = sData.SubString(2, sData.Length());
	unsigned int LoopCount = sOut.Length();
	AnsiString sNum;
	char * Ptr;
	unsigned char iTemp;

	memcpy(TempBuffer, sOut.c_str(), sOut.Length());
	Ptr = TempBuffer;

	while (LoopCount > 0) {
		sNum = AnsiString(Ptr, 2);
		iTemp = StrToInt("0x" + sNum);

		Checksum += iTemp;

		Ptr += 2;
		LoopCount -= 2;
	}

	Checksum = ~Checksum + 1;

	return Checksum;

}

// ---------------------------------------------------------------------------
unsigned short CalcChksum(unsigned char *start_addr, int bytecnt)
{
 //  checksum = CalcChksum(serNum, 13);
   int i;
   unsigned short chksum;

   chksum = 0;
   // Calculate 1's complement chksum
   for(i=0; i<bytecnt; i++)
	  chksum+= (unsigned short)*(start_addr+i);
   chksum = (char)~chksum;
  // return(chksum);

}

// ---------------------------------------------------------------------------
// convert a signle hex character into its 4bit representation
// ---------------------------------------------------------------------------
uint8_t __fastcall HexCharToNibble(uint8_t hexChar) {
	uint8_t result = hexChar;
	result -= (hexChar >= (uint8_t)'A') ? (((uint8_t)'A')-10) : (uint8_t)'0';
	return result & 0x0F;
}

// ---------------------------------------------------------------------------
// Convert two hex characters into a sin uint8_t integer
// ---------------------------------------------------------------------------
uint32_t __fastcall HexStrToUint(char *phexChars, uint8_t len, bool bigEndian) {
	//	int tre = HexStrToUint(&cmdBuff[0], 2, false);

	uint32_t result = 0;
	uint8_t index;
	uint8_t i;

	//build integer result, based on endiannes
	for(i=0;i<len;i++){
		index = (bigEndian) ? i : (len-1)-i;
		result = result << 4;
		result += HexCharToNibble((uint8_t)phexChars[index]);
	}
	//return result
	return result;
}

// ---------------------------------------------------------------------------
// calculate checksum by xor'ing each byte
// ---------------------------------------------------------------------------
uint8_t __fastcall xorCheckSum(uint8_t *buff, uint8_t len)
{
	uint8_t i;
	uint8_t result = buff[0];
	for(i=1;i<len;i++)
	{
		result ^= buff[i];
	}
	return result;
}

// ---------------------------------------------------------------------------
// Calculate the byte representation of a hex-string representation of int
// ---------------------------------------------------------------------------
void __fastcall IntToHexByteBuff(int input, uint8_t *buff, uint8_t len){

	uint8_t i;
	int curVal = input;
	for(i=0;i<len;i++){
		buff[i] = (uint8_t)(curVal & 0x0F);
		buff[i] += (buff[i] < 10) ? (uint8_t)'0' : ((uint8_t)'A')-10;
		curVal = curVal >> 4;
	}

}

// ---------------------------------------------------------------------------
// Calculate the byte representation of a hex-string representation of int
// ---------------------------------------------------------------------------
void __fastcall IntToHexByteBuff2(int input, uint8_t *buff, uint8_t NoOfBytes){

/*	uint8_t i;
	int curVal = input;

	uint8_t LSB;
	uint8_t MSB;

	for(i=0; i < len ; i++)
	{
		LSB = (uint8_t)(curVal & 0x0F);
		MSB = (uint8_t)(curVal & 0xF0);
		MSB = MSB >> 4;

		LSB += (LSB < 10) ? (uint8_t)'0' : ((uint8_t)'A')-10;
		MSB += (MSB < 10) ? (uint8_t)'0' : ((uint8_t)'A')-10;
	}
*/

	AnsiString HexDecodedString = IntToHex(input,8);

	int lenn = HexDecodedString.Length()/2;

	for (int ib = 0; ib < NoOfBytes; ib++)
	{
		AnsiString temp = HexDecodedString.SubString(2*ib+1, 2);

//		if (ib > lenn - 1)
//		{
			// Do nothing
  //			buff[ib] = StrToInt("0");
			//Shuffle
//			buff[1] = buff[0];
//			buff[2] = buff[1];
//			buff[3] = buff[2];

//			buff[0] = StrToInt("0");
//		}
//		else
//		{
			//buff[ib] = StrToInt(temp);

//		char *hexstring = "75";
//		int number = (int)strtol(hexstring, NULL, 16);
		unsigned int Hexnumber = strtoul(temp.c_str(), NULL, 16);
		buff[ib] = Hexnumber;

//		} // End If


	}  // End For


}

// ---------------------------------------------------------------------------
// Convert a string of bytes into a string
// ---------------------------------------------------------------------------
AnsiString __fastcall ByteArrayToStr(uint8_t *buff, uint8_t len)
{
	AnsiString result = "";
	uint8_t i;
	for(i=0;i<len;i++){
     	result += (char)buff[i];
	}
	return result;
}

/*
// ---------------------------------------------------------------------------
// Convert a string to bytes array
// ---------------------------------------------------------------------------
void __fastcall String2ByteArray(AnsiString StringInput)
{
const int SIZE = 1024;
char c_string[SIZE];

for (int index = 0; index < StringInput.length(); index++){
	c_string[index] = StringInput[index];
}
c_string[StringInput.length()] = '\0';// add null terminator

	return result;
}

*/

// ---------------------------------------------------------------------------
// Use Sprintf to get a string representation of a float, with 2 digits precision
// ---------------------------------------------------------------------------
AnsiString __fastcall FloatToDigitString(double value){

	//create containers
	AnsiString result = "";
	char tmpStr[16];
	//call sprintf and store result
	snprintf(tmpStr,15,"%.3f",value);
	result = tmpStr;
	//return result
	return result;
}
// ---------------------------------------------------------------------------
// calculate
// ---------------------------------------------------------------------------
int __fastcall HexCharToChkSum(char *buff, int len)
{
//	int val = HexCharToChkSum(&cmdBuff[0],3);
//	char test = (uint8_t)val;

	int result = 0;
	for (int i = 0; i < len; i++)
	{
		int test = (uint8_t)buff[i];
		result = result + test;

	}  // End For

	return result;
}

