//---------------------------------------------------------------------------

#ifndef XYPlotterH
#define XYPlotterH

#include <vcl.h>
#include <stdio.h>
#include <io.h>
#include <setupapi.h>
#include "Utils.h"

//#include "_SerialIO.h"
#include "AdPort.hpp"
#include "OoMisc.hpp"


typedef enum {
	XYPLOTTER_OK,
} XYPlotter_ret_t;

typedef struct {
	int32_t XPos;
	int32_t YPos;
} XYPlotter_setpoint_t;

typedef struct {
	uint8_t cmd;
	uint8_t len;
} XYPlotter_cmd_t;

#define XYPLOTTER_NO_CMD		  		 (0x00)
#define XYPLOTTER_CMD_SET_RELAY          (0x5A)
#define	XYPLOTTER_CMD_SET_RELAY_LEN	  	 (5)
#define XYPLOTTER_REPLY_SET_RELAY_OK     (0x5B)
#define XYPLOTTER_REPLY_SET_RELAY_ERR    (0x5C)
#define XYPLOTTER_REPLY_SET_RELAY_LEN    (5)
#define	XYPLOTTER_CMD_GOTOXY 		  	 (0x2A)
#define XYPLOTTER_CMD_GOTOXY_LEN	  	 (11)   // CMD,X0,X1,X2,X3,Y0,Y1,Y2,Y3,CHK0,CHK1
#define	XYPLOTTER_CMD_TEST_COMM		  	 (0x4A)
#define	XYPLOTTER_CMD_TEST_COMM_LEN		 (1)
#define	XYPLOTTER_CMD_GETDIST 		  	 (0x3A)
#define XYPLOTTER_CMD_GETDIST_LEN	  	 (1)
#define	XYPLOTTER_CMD_ZERO 		  		 (0x1A)
#define XYPLOTTER_CMD_ZERO_LEN		     (1)
#define	XYPLOTTER_REPLY_DIST 		     (0x3B)
#define XYPLOTTER_REPLY_DIST_LEN	  	 (11)
#define	XYPLOTTER_REPLY_ZERO 		  	 (0x1B)
#define XYPLOTTER_REPLY_ZERO_LEN	  	 (1)
#define XYPLOTTER_REPLY_SETPOINT_OK 	 (0x2B)
#define XYPLOTTER_REPLY_SETPOINT_ERR 	 (0x2C)
#define XYPLOTTER_REPLY_SETPOINT_LEN 	 (1)
#define	XYPLOTTER_REPLY_TEST_COMM        (0x4B)
#define	XYPLOTTER_REPLY_TEST_COMM_LEN	 (1)

#define XYPLOTTER_REF_RELAY ('0')
#define XYPLOTTER_POWER_RELAY ('1')
#define XYPLOTTER_6R8_LOAD_RELAY ('2')
#define XYPLOTTER_5R0_LOAD_RELAY ('3')
#define XYPLOTTER_3R3_LOAD_RELAY ('4')



#define STEPS_PER_MILLIMETER (22)
#define MAX_REPLY_LENGTH (12)
#define MAX_BYTES_TO_PROCESS (256)
#define MAX_COMM_TESTS_BEFORE_DISCONNECT (10)

//simple controller class to control USBMM_Threads
class XYPlotter {

	 public:

		bool relayState;

		// Start the thread suspended
//		__fastcall XYPlotter(HANDLE PlotterComHandle, TMemo *PlotterMemo,TTimer *tmForm1NRT)
		__fastcall XYPlotter(TApdComPort * PlotterComHandle, TMemo *PlotterMemo,TTimer *tmForm1NRT)
		{
			//set plotter comport, and DTR to true
			ComHandle = PlotterComHandle;
			//send test for connection
			portConnected = false;
			portTestCallsPending = 0;
			portConnected = UpdateConnectionState();
			//get user output log
			Memo = PlotterMemo;
			tmNRT = tmForm1NRT;
			//clear initial positions
			distance.XPos=0;
			distance.YPos=0;
			curSetpoint.XPos=0;
			curSetpoint.YPos=0;
			offset.XPos=0;
			offset.YPos=0;
			//initialize other variables
			waitingOnSetpointReply = false;
			relayState = false;
		}

		__fastcall ~XYPlotter(void)
		{
		}

		void __fastcall UpdateSetpoint(XYPlotter_setpoint_t new_setpoint);
		bool __fastcall UpdateConnectionState(void);
//		void __fastcall SetOffset(XYPlotter_setpoint_t new_offset);
		void __fastcall SetPlotStart(XYPlotter_setpoint_t new_offset);

		XYPlotter_setpoint_t __fastcall GetSetpoint();
		XYPlotter_setpoint_t __fastcall GetCurDistance();
		void __fastcall ProcessRXData(int Count, uint8_t * pData);
		void __fastcall	RequestDistance();
		bool __fastcall SetpointReached();
		void __fastcall Home();
		void __fastcall AddToLog(AnsiString line);
		void __fastcall enRefRelay(bool state); //true = on
		void __fastcall enPowerRelay(bool state); //true = on
		void __fastcall enLoadRelay(bool state,int iLoad);
		void __fastcall XYPlotter::SetPlotterType2D(bool Set2D);
		void __fastcall XYPlotter::Set3dZHeight(float ZmmDist);
		void __fastcall XYPlotter::SendCommand(AnsiString Cmd3DPlot);

	private:

		XYPlotter_setpoint_t curSetpoint;
		XYPlotter_setpoint_t newSetpoint;
		XYPlotter_setpoint_t distance;
		XYPlotter_setpoint_t offset;

		bool portConnected;
		bool waitingOnSetpointReply;
		uint8_t portTestCallsPending;
//		HANDLE ComHandle;
		TApdComPort *ComHandle;
		TMemo *Memo;
		TTimer *tmNRT;

		void __fastcall ProcessReply(uint8_t *buff, uint32_t len);
		bool __fastcall IsMessageHeader(uint8_t byte);
		uint32_t __fastcall GetMessageLength(uint8_t header);
		void __fastcall Process_Message(int Count, uint8_t * pData);
		void __fastcall ProcessRX_Byte(uint8_t RxByte);
};

class XYPlotterPSU {

  public:

  private:

};

#endif
