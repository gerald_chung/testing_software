/***************************************************************
Filename:	Auto_Plotter_Test.cpp
Purpose:	2D Efficiency Plotter
Creation:   16 June 2014, Tom Vocke, Joseph Bowkett

Version		Date	 Name	Comments
V1          16/6/14	 TV  	First Release
V2			30/6/14  GC     Sorted Data format etc

**************************************************************/

/*

	latest change = //28/7/15

	[FormCreate]

	State Machine
	[tmTestTimer] = State Timer

	[InitPlotGrid]

	Rgistry Name = sREG

	[updateEfficiency]


	Tx comms
	[btTXOpenCommsClick]


*/


// ---------------------------------------------------------------------------

#include "vcl.h"
#include <stdio.h>
#include <io.h>
#include <setupapi.h>
#pragma hdrstop

#include "mainGUI.h"
#include "Utils.h"
#include "ComportSel.h"
#include "_SerialIO.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TmainForm *mainForm;

// ***
// Globar Vars
// ***
int SelGridRowNumber;
int SelGridColNumber;
int RetryCount;

bool SelectedGrid;
// ***
// ENDS Globar Vars
// ***


enum TX_STATES
	{
	TX_BUFF_EMPTY, INQUIRE, INQUIRE_WAIT, SENDING_RESPONSE, SENDING_DATA,
	WAITING_FOR_ACK
	} ;

enum Message
	{
	START, SEQ, HDR, DATA
	} ;

char TxBuffer[ 400 ];
static char RiBuf[ 400 ];
static unsigned char TxSeqNum = '0' - 1;
static unsigned char NackCount = 0;
static unsigned char RxSeqNum = 'X';
bool Recived_Message = FALSE;
bool Expert_User = false;
enum TX_STATES Tx_State = INQUIRE;
unsigned int Binary_Len;

typedef enum Tri_Coil_State_
{
	TCS_Nothing = 0,TCS_Detected,TCS_NearBy,TCS_Coupled,TCS_Powered,TCS_Failed_Sweep,TCS_Not_Coupled
} Tri_Coil_State_t;


int Qi_Signal_Strength = 0;
bool First_Qi_Coil = true;
int Qi_Coil_Num_1 = 0;
int Qi_Coil_Num_2 = 0;
bool PauseState = false;
bool DAQStartedMeasurement = false;
bool TestStarted = false;


#define STX 0x02
#define ETX 0x03
#define DLE 0x10

#define MEASUREMENT_TIME 1000

// Qi message headers
#define QI_H_SIG_STRENGTH 0x01
#define QI_H_EOT 0x02
#define QI_H_HOLDOFF 0x06
#define QI_H_C_ERROR 0x03
#define QI_H_REC_POWER 0x04
#define QI_H_CH_STATUS 0x05
#define QI_H_CONFIG 0x51
#define QI_H_ID 0x71
#define QI_H_EXT_ID 0x81
// usual length for each type of message
#define QI_H_SIG_STRENGTH_LEN 3
#define QI_H_EOT_LEN 3
#define QI_H_HOLDOFF_LEN 3
#define QI_H_C_ERROR_LEN 3
#define QI_H_REC_POWER_LEN 3
#define QI_H_CH_STATUS_LEN 3
#define QI_H_CONFIG_LEN 7
#define QI_H_ID_LEN 9
#define QI_H_EXT_ID_LEN 10
//Qi error codes
typedef enum qi_error_code
{
	QI_OK = 0,
	QI_CHECKSUM_MISMATCH,
	QI_PARITY_ERROR,
	QI_INVALID_MESSAGE,
	QI_INVALID_CONTENT,
	QI_MSG_SIZE_MISMATCH,
	QI_MSG_PENDING,
	QI_NO_MSG_PENDING,
	QI_START_POWER_TRANSFER,
	QI_STOP_POWER_TRANSFER,
	QI_USING_EXTENDED_ID,
	QI_USING_NORMAL_ID,
	QI_ERROR
} qi_error_code_t;

// typedef

// stores configuration/ID data associated with messages
typedef struct qiMsg_Conf_t {
	uint8_t configured; // zero if this is an unconfigured packet
	// properties coming from config packet
	uint8_t power_class;
	uint8_t max_power;
	uint8_t proprietary;
	uint8_t optional_packets_count;
	uint8_t window_size;
	uint8_t window_offset;
	// properties coming from ID packet
	uint8_t major_version;
	uint8_t minor_version;
	uint16_t manufacturer_code;
	uint32_t basic_device_id;
	uint8_t extended_id_present;
	// properties coming from extended ID packet
	uint64_t extended_id;
} qiMsg_Conf_t;

#define QI_MAX_DATA 10 //maximum data bytes in a message
typedef struct qiMsg_t {
	uint8_t header; // header byte
	uint8_t data[QI_MAX_DATA]; // data bytes
	uint8_t num_bytes;
	// total amount of valid bytes (including header and checksum)
	uint8_t checksum; // checksum byte

	TDateTime received_time; // when was this packet received?

	qiMsg_Conf_t config;
	// with what configuration does this message correspond?

	// this will store the checksum evaluation result
	uint8_t checksum_ok;
	// this will store whether anything else is wrong with the message
	uint8_t errors;
	uint8_t errorcode;
	uint8_t checked_for_errors; // has it been checked yet?
	uint16_t transitions[50];
	uint8_t num_trans;
	AnsiString statusString; //qi status messages from RX
	uint8_t isStatus;
} qiMsg_t;

bool Received_Message = false;


//for state machine
typedef enum Tx_Init_State_ {
	Tx_Init_Idle = 0,
	Tx_Init_Version ,
	Tx_Init_Version_Wait ,
	Tx_Init_Manual_Mode ,
	Tx_Init_Manual_Mode_Wait ,
	Tx_Init_Q_Message_Enable ,
	Tx_Init_Q_Message_Enable_Wait ,

	Tx_Init_Start_Scan_Power ,           //28/7/15
	Tx_Init_Start_Scan_Power_Wait ,     //28/7/15

} Tx_Init_State_t;

static Tx_Init_State_t Tx_Init_State = Tx_Init_Idle; //state variable


//Q MESSAGE SETTINGS DEFINITIONS
#define Q_MESSAGE_CONFIG_VERSION (0x01)
//Settings stored by BM_One
const uint32_t Q_Message_BM_All_Detection           = (0x00000001 << 0);
const uint32_t Q_Message_BM_Buck_Boost_Voltage      = (0x00000001 << 1);
const uint32_t Q_Message_BM_Charging_Current        = (0x00000001 << 2);
const uint32_t Q_Message_BM_Loop_Detection          = (0x00000001 << 3);
//const uint32_t Q_Message_BM_Error_State             = (0x00000001 << 4);    //Do not use
const uint32_t Q_Message_BM_Control_Error           = (0x00000001 << 5);
const uint32_t Q_Message_BM_Decoded_Qi_Messages     = (0x00000001 << 6);
const uint32_t Q_Message_BM_FOD_Calibration_Status  = (0x00000001 << 7);
const uint32_t Q_Message_BM_FOD_Reading             = (0x00000001 << 8);
const uint32_t Q_Message_BM_FOD_Config              = (0x00000001 << 9);
const uint32_t Q_Message_BM_Rx_Bicoil_List          = (0x00000001 << 10);
//const uint32_t Q_Message_BM_FOD_Reading             = (0x00000001 << 9);
//const uint32_t Q_Message_BM_FOD_Reading             = (0x00000001 << 10);
//const uint32_t Q_Message_BM_FOD_Reading             = (0x00000001 << 11);
//const uint32_t Q_Message_BM_FOD_Reading             = (0x00000001 << 12);
//const uint32_t Q_Message_BM_F                   = 0x00000020;
//const uint32_t Q_Message_BM_G                   = 0x00000040;
//const uint32_t Q_Message_BM_H                   = 0x00000080;  //TODO give them better names
//      And so on up to 0x00000001<<31
//Settings stored by BM_Two
//const uint32_t Q_Message_BM_T                   = 0x00000001;
//const uint32_t Q_Message_BM_U                   = 0x00000002;
//const uint32_t Q_Message_BM_V                   = 0x00000004;
//const uint32_t Q_Message_BM_W                   = 0x00000008;
//const uint32_t Q_Message_BM_X                   = 0x00000010;
//const uint32_t Q_Message_BM_Y                   = 0x00000020;
//const uint32_t Q_Message_BM_Z                   = 0x00000040;
//      And so on up to 0x00000001<<31

bool Waiting_On_Manual_Mode = false;
bool Waiting_On_Q_Message_Enable = false;
bool Waiting_On_Scan_n_Power = false;       //28/7/15

// #define EVENT_DRIVEN

class Rx_Thread : public TThread
	{
private:

	HANDLE MyReadHandle;
	int BytesRead;
	uint8_t byteBuff[ MAX_BYTES_TO_PROCESS ];
	int Node;
	OVERLAPPED o;

	void __fastcall SaveData( )
		{
		mainForm->Comport_Rx_Data( BytesRead, byteBuff, Node );
		}

	void __fastcall Execute( void )
		// This is what gets run when the thread starts
		{
		DWORD dwEvtMask;

		while ( !Terminated )
			{

#ifdef EVENT_DRIVEN
			if ( WaitCommEvent( MyReadHandle, &dwEvtMask, &o ) )
				{
				BytesRead = sdReadSerial( MyReadHandle,
					( char * ) &byteBuff[ 0 ], MAX_BYTES_TO_PROCESS );
				if ( BytesRead )
					{
					Synchronize( SaveData );
					}
				}
#else

			if ( sdDataWaiting( MyReadHandle ) )
				{
				BytesRead = sdReadSerial( MyReadHandle,
					( char * ) &byteBuff[ 0 ], MAX_BYTES_TO_PROCESS );
				if ( BytesRead )
					{
					Synchronize( SaveData );
					}
				}
			Sleep( 1 );
#endif

			if ( !Terminated ) // This is optional but normal
				{
				// Synchronize(UpdateTheMainThreadDataOrControls);
				} ;
			} ;
		}
	;

public:
	/* You can provide any data the thread needs to run properly */
	__fastcall Rx_Thread( HANDLE ReadHandle, int fNode ) :
		TThread( TRUE ) // Start the thread suspended
		{
		// Use the StartupData to initialize class instance data here
		Node = fNode;
		MyReadHandle = ReadHandle;
		FreeOnTerminate = true;

#ifdef EVENT_DRIVEN
		SetCommMask( MyReadHandle, EV_RXCHAR );

		memset( & o, 0, sizeof( o ) );
		o.hEvent = CreateEvent(
			NULL, // default security attributes
			TRUE, // manual-reset event
			FALSE, // not signaled
			NULL // no name
			);

		// Initialize the rest of the OVERLAPPED structure to zero.
		o.Internal = 0;
		o.InternalHigh = 0;
		o.Offset = 0;
		o.OffsetHigh = 0;

		assert( o.hEvent );
#endif

		Resume( ); // Thread now runs
		}
	;

	__fastcall ~Rx_Thread( void )
		{
		// Cleanup, deallocate, etc.
		}
	;

	void __fastcall Terminate( void )
		// This is what you call to terminate the thread; use Suspend to pause it
		{
		TThread::Terminate( );
		// If the main loop is waiting on an event, set the event here
		}
	;
	} ;

#ifdef USB_PSU
Rx_Thread * PSUComPort_Rx_Thread;
#endif

Rx_Thread * PlotterComPort_Rx_Thread;
Rx_Thread * TXComPort_Rx_Thread;

typedef enum Rx_Thread_Node_ {
	RX_NODE_PSU_COMPORT = 0,
	RX_NODE_PLOTTER_COMPORT ,
	RX_NODE_TX_COMPORT ,
} Rx_Thread_Node_t;

// ---------------------------------------------------------------------------

void __fastcall TmainForm::Comport_Rx_Data( int Count, uint8_t * pData,	int Node )
{

   if (Node == RX_NODE_TX_COMPORT)
	{
	Qi_ComportRxChar(Count,pData);
	}

   if (Node == RX_NODE_PLOTTER_COMPORT)
	{
	if (Plotter != NULL)
		{
		Plotter->ProcessRXData(Count,pData);
		}
	}

 }

// ---------------------------------------------------------------------------
// Destructor
// ---------------------------------------------------------------------------
//__fastcall TmainForm::~TmainForm(void)
void __fastcall TmainForm::FormDestroy(TObject *Sender)
{
	//disable all timers
	tmMeterUpdate->Enabled = false;
	tmPlotter->Enabled = false;
	tmPlotter_NRT->Enabled = false;
	tmTest->Enabled = false;

	//close PSU comport
#ifdef USB_PSU
	sdFlushPort(PSUComPort);
	sdClosePort(PSUComPort);
#endif
	//close Plotter comport
	sdFlushPort(PlotterComPort);
	sdClosePort(PlotterComPort);

	//todo delete threads

	//close TX comport if opened
	if(TXComPort != NULL){
		sdFlushPort(TXComPort);
		sdClosePort(TXComPort);
		//delete TXComPort_Rx_Thread;
	}

	//close DAQ interface
	btCloseUSBClick(this);

	//kill all threads
	USBMM->Stop();

	//detele member objects
	delete USBMM;
	delete Plotter;
}

// ---------------------------------------------------------------------------
// Constructor
// ---------------------------------------------------------------------------
__fastcall TmainForm::TmainForm(TComponent* Owner) : TForm(Owner)
{
		  // Nothing Here
}

// ---------------------------------------------------------------------------
// Create Form
// ---------------------------------------------------------------------------
void __fastcall TmainForm::FormCreate(TObject *Sender)
{
	int index;

	Plotter_Number = 0;

//	if ( ParamCount > 0)
		{
		for ( index = 1; index <= ParamCount( ); index++ )
			{
			try
				{
				Plotter_Number = StrToInt( ParamStr( index ));
				}
			catch ( ... )
				{
				}
			}
		}
//	 else
//		{
//		ShowMessage("Plesae pass the Plotter number as a parameter");
//		Application->ShowMainForm = false;
//		Application->Terminate( );
//		}


	if (!Plotter_Number)
	{
		ShowMessage("Please pass the Plotter number as a parameter");
		Application->ShowMainForm = false;
		Application->Terminate( );
	}

	//***
	// Registry Name
	// ***
	//	sREG = ("Software\\PbP\\FODA_Plotter_" + IntToStr((int)Plotter_Number) );
	sREG = ("Software\\PbP\\" + edRegistryName->Text + "_" + IntToStr((int)Plotter_Number) );

	//disable all timers
	tmMeterUpdate->Enabled = false;
	tmPlotter->Enabled = false;
	tmTest->Enabled = false;

	measurementRunning = false;

	DAQStartedMeasurement = false;

	//initialize member variables
	testAdmin.testState = TEST_IDLE_STATE;

	//Initialise PSU Control State
	cbEnablePSUControl->Checked = true;

#ifdef USE_NI6009
	USBMM = new NI6009_USB_Interface(memoUSBLog);
#endif

#ifdef USE_NI6210
	USBMM = new NI6210_USB_Interface(memoUSBLog);
	sDQA_Address_Name = Reg_ReadString(sREG, "DAQ", "NI6210", "dev1");
	USBMM->SetDQA_Address_Name(sDQA_Address_Name);

#endif

#ifdef USE_AGILENT
	USBMM = new AGILENT_Interface(memoUSBLog);
#endif

#ifdef USE_DMM
	USBMM = new USBMM_Controller(memoUSBLog);
#endif

#ifdef USE_MOCK_DMM
	USBMM = new MOCK_Interface(memoUSBLog);
#endif



	//acquire the required comports
#ifdef USB_PSU
	PSUComPort = UserSelectComport("Select PSU Com-Port for Plotter " + IntToStr((int)Plotter_Number), "PortPSU", 9600);
	if( PSUComPort == NULL) {
		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
	} else {
		sdFlushPort(PSUComPort);
		PSUComPort_Rx_Thread= new Rx_Thread( PSUComPort, RX_NODE_PSU_COMPORT );
	}
#endif

//Serial Port At Open program Option
//	PlotterComPort = UserSelectComport("Select Plotter Com-Port for Plotter " + IntToStr((int)Plotter_Number) , "PortPlotter", 57600);

	// if com-port could not be opened close application
//	if(  PlotterComPort == NULL) {
//		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
//	} else {
//		sdFlushPort(PlotterComPort);
//		PlotterComPort_Rx_Thread = new Rx_Thread( PlotterComPort, RX_NODE_PLOTTER_COMPORT );
//	}


	//initialize plotter interface
//	Plotter = new XYPlotter(PlotterComPort, memoPlotterLog,tmPlotter_NRT);

	mainForm->Caption = mainForm->Caption + " - CONNECTED to Plotter No. " + IntToStr((int)Plotter_Number)  ;



	//start plotter timer
//	tmPlotter->Enabled = true;

	//get register defaults
	ebOffTime->Text = Reg_ReadString(sREG, "Power", "Off_Time", "100");
	ebPowerUpWait->Text = Reg_ReadString(sREG, "Power", "On_Time", "1000");
	ebXSteps->Text = Reg_ReadString(sREG, "Cords", "X_Steps", "20");
	ebStepSize->Text = Reg_ReadString(sREG, "Cords", "Step_Size", "5");
	ebYSteps->Text = Reg_ReadString(sREG, "Cords", "Y_Steps", "20");
	ebXOffset->Text = Reg_ReadString(sREG, "Cords", "X_Offset", "0");
	ebYOffset->Text = Reg_ReadString(sREG, "Cords", "Y_Offset", "0");

	ebXSetpointStart->Text = Reg_ReadString(sREG, "Cords", "X_SetPoint", "0");
	ebYSetpointStart->Text = Reg_ReadString(sREG, "Cords", "Y_SetPoint", "0");

	AnsiString defaultFilename = "EVK2_Tx#x_vx_x_x_19V_EVK2_Rx#x_vx_x_x_3mm_VERT_5W_resLoad_12sec_5mmStep";
	ebFilename->Text = Reg_ReadString(sREG, "Directories", "TestFileName", defaultFilename);
	ebFolderName->Text = Reg_ReadString(sREG, "Directories", "TestFolderName", "C:\\temp\\");
	ebVinMultiplier->Text = Reg_ReadString(sREG, "Multipliers", "Vin", "1.0");
	ebIinMultiplier->Text =  Reg_ReadString(sREG, "Multipliers", "Iin", "1.0");
	ebVoutMultiplier->Text = Reg_ReadString(sREG, "Multipliers", "Vout", "1.0");
	ebIoutMultiplier->Text = Reg_ReadString(sREG, "Multipliers", "Iout", "1.0");
	ebVinOffset->Text = Reg_ReadString(sREG, "Offsets", "Vin", "0.0");
	ebIinOffset->Text = Reg_ReadString(sREG, "Offsets", "Iin", "0.0");
	ebVoutOffset->Text = Reg_ReadString(sREG, "Offsets", "Vout", "0.0");
	ebIoutOffset->Text = Reg_ReadString(sREG, "Offsets", "Iout", "0.0");
//	rgLoad->ItemIndex =  Reg_ReadInteger(sREG, "Load", "Radio_Index",1 );


	//update plotter offset
//	ebXOffsetChange(this);

	InitPlotGrid();

	ShowTabPanel(0);

	SelectedGrid = false;
	StringGrid1->Enabled = true;

}

// ---------------------------------------------------------------------------

void __fastcall TmainForm::logit(AnsiString FileName, AnsiString Data)
{
//bool New_File = true;

 if (cbSaveLog->Checked)
 {
	FileName = "Plotter_" + IntToStr((int)Plotter_Number) + "_" + FileName;

	Data = Data + "\n";

	//if( (_access( FileName.c_str(), 0 )) != -1 )
	//	{
	//	 New_File = false;
	//	}

	  FILE *log = fopen(FileName.c_str(), "at");
	  if(!log)
		{
		log = fopen(FileName.c_str(), "wt");
		}
	  if(!log)
	  {
		printf("can not open logfile.txt for writing.\n");
		return; // bail out if we can't log
	  }


	  fputs(Data.c_str(), log);
	  // Insert whatever you want logged here

	  fclose(log);
 }
 else
 {
	// Do Nothing
 }// End If
}

// ---------------------------------------------------------------------------
// Open a com-port select dialog, to prompt the user to select a com-port
// ---------------------------------------------------------------------------
void __fastcall TmainForm::InitPlotGrid(void)
{

int ColMax;
int RowMax;

	ColMax = StrToInt(ebXSteps->Text);
	RowMax = StrToInt(ebYSteps->Text);

	//***
	// Set Grid Limits
	//***
	StringGrid1->ColCount = ColMax + 2;     // Include Title Col
	StringGrid1->RowCount = RowMax + 2;     // Includes Title Row


	if (cbDoNotMoveXY->Checked)
	{

		if (MessageDlg("Please Enter Option \n Yes = Clear Grid, No = Do Not clear Grid", mtInformation, mbYesNo, 0) == mrYes)
		 {
			//ShowMessage("Yes");
			// ***
			// Clear Grid
			// ***
			for (int iRow = 0; iRow < RowMax + 2; iRow++)
			{
				StringGrid1->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
			}  // End For
		 }
		 else
		 {
			 // ShowMessage("No");
			 // Do Nothing
		 }

	}
	else
	{
		// ***
		// Clear Grid
		// ***
		for (int iRow = 0; iRow < RowMax + 2; iRow++)
		{
			StringGrid1->Rows[iRow]->Clear(); //= StringGrid1->Rows[row - 1];
		}  // End For

	} // End If


	//***
	// Column Width
	//***
	StringGrid1->DefaultColWidth = 100;
	for (int i = 0; i < ColMax + 2; i++)
	{
		StringGrid1->ColWidths[i]  = 32;
	} // End for
	//***
	// End Column Width
	//***

	// Number Title Columns
	for (int iCol = 0; iCol < ColMax + 2; iCol++)
	{
		StringGrid1->Cells[iCol + 1][0] = iCol;
	}  // End For

	// Number Row Titles
	for (int iRow = 0; iRow < RowMax + 2; iRow++)
	{
		if (iRow == 0)
		{
			StringGrid1->Cells[0][iRow] = "";
		}
		else
		{
			StringGrid1->Cells[0][iRow] = (RowMax + 1) - iRow;
		} // End If

	}  // End For

}

// ---------------------------------------------------------------------------
// Open a com-port select dialog, to prompt the user to select a com-port
// ---------------------------------------------------------------------------
//28/7/15
HANDLE __fastcall TmainForm::UserSelectComport(AnsiString dialogLabel, AnsiString regGroup, uint32_t baudrate)
//void __fastcall TmainForm::UserSelectComport(TApdComPort *pComport,AnsiString dialogLabel, AnsiString regGroup, uint32_t baudrate)

{

	HANDLE comHandle = NULL;
	// create user dialog asking for com-port selection
	TDeviceSelectionForm * SelDialog;
	SelDialog = new TDeviceSelectionForm(this, dialogLabel);
	// set default selection
	SelDialog->ComNumber = Reg_ReadInteger(sREG, regGroup, "Comm Number", 0);

	// if selection is complete, store result and test validity
	if (SelDialog->ShowModal() == mrOk) {
		//open comport with given baudrate, selected comport nr, and no handshaking
		comHandle =  sdOpenSerialPort(SelDialog->ComNumber, (int)baudrate, 0, 10, 10);
		if(comHandle != NULL)
		{
			Reg_WriteInteger(sREG, regGroup, "Comm Number", SelDialog->ComNumber);
		} else {
			// if com-port could not be openen close application
			MessageDlg(	"Error Opening ComPort " + String(SelDialog->ComNumber) + " @ " + String(baudrate) +
						" -> Error : " + String(GetLastError()), mtError, TMsgDlgButtons() << mbOK, 0);
//			delete SelDialog;
//			Application->ShowMainForm = false;
//			Application->Terminate();
			return NULL;
		}
	}
	else {
		// if cancel was pressed, do no open application
//		delete SelDialog;
//		Application->ShowMainForm = false;
//		Application->Terminate();
		return NULL;
	}

	//clear form and return result
	delete SelDialog;
	return comHandle;
}

// ---------------------------------------------------------------------------
// Enable extern power supply
// ---------------------------------------------------------------------------
void __fastcall TmainForm::btEnablePSUClick(TObject *Sender)
{
#ifdef USB_PSU
	sdWriteSerial(PSUComPort,"o1\n"); // Power on
#else
	//send relay command
	Plotter->enPowerRelay(true);
#endif

	lbPSUPower->Caption = "PSU Power : ON";
	TestLogAdd("Power ON");
	//update buttons
	btEnablePSU->Enabled = false;
	btDisablePSU->Enabled = true;

  //	cbPowerSupplyStatus->Checked = true;

}

// ---------------------------------------------------------------------------
// Disable external power supply  (store in register)
// ---------------------------------------------------------------------------
void __fastcall TmainForm::btDisablePSUClick(TObject *Sender)
{

#ifdef USB_PSU
	sdWriteSerial(PSUComPort,"o0\n"); // Power off
#else
	Plotter->enPowerRelay(false);
#endif


	lbPSUPower->Caption = "PSU Power : OFF";
	TestLogAdd("Power OFF");
	//update buttons
	btEnablePSU->Enabled = true;
	btDisablePSU->Enabled = false;

  //	cbPowerSupplyStatus->Checked = false;
}

// ---------------------------------------------------------------------------
// Update X number of steps  (store in register)
// ---------------------------------------------------------------------------
void __fastcall TmainForm::ebXStepsChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Cords", "X_Steps", ebXSteps->Text);
	TestLogAdd("X Steps : " + ebXSteps->Text);

	InitPlotGrid();

}

// ---------------------------------------------------------------------------
// Update U number of steps  (store in register)
// ---------------------------------------------------------------------------
void __fastcall TmainForm::ebYStepsChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Cords", "Y_Steps", ebYSteps->Text);
	TestLogAdd("Y Steps : " + ebYSteps->Text);

	InitPlotGrid();
}

// ---------------------------------------------------------------------------
// Update step-size per step (store in register)
// ---------------------------------------------------------------------------
void __fastcall TmainForm::ebStepSizeChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Cords", "Step_Size", ebStepSize->Text);
	TestLogAdd("Step Size : " + ebStepSize->Text);
}

//---------------------------------------------------------------------------
// Update file to save too, and foldername registry entry
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebFolderNameChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Directories", "TestFolderName", ebFolderName->Text);
}

//---------------------------------------------------------------------------
// Update file to save too, and filename registry entry
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebFilenameChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Directories", "TestFileName", ebFilename->Text);
}

//---------------------------------------------------------------------------
// Write Power-up wait time to registry
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebPowerUpWaitChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Power", "On_Time", ebPowerUpWait->Text);
	TestLogAdd("New [On Time] : " + ebPowerUpWait->Text + "ms");
}

//---------------------------------------------------------------------------
// Write Power-off wait time to registry
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebOffTimeChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Power", "Off_Time", ebOffTime->Text);
	TestLogAdd("New [Off Time] : " + ebOffTime->Text + "ms");
}

// ---------------------------------------------------------------------------
// Update plotter setpoint to 0,0 (home)
// ---------------------------------------------------------------------------
void __fastcall TmainForm::btHomePlotterClick(TObject *Sender)
{
//	ebXSetpointStart->Text = "0";
//	ebYSetpointStart->Text = "0";
	Plotter->Home();


}

// ---------------------------------------------------------------------------
// Update plotter setpoint to user input X,Y
// ---------------------------------------------------------------------------
void __fastcall TmainForm::btGotoXYClick(TObject *Sender)
{
	XYPlotter_setpoint_t tmpSetpoint;
//	tmpSetpoint.XPos = StrToInt(ebXSetpointStart->Text);
//	tmpSetpoint.YPos = StrToInt(ebYSetpointStart->Text);
//	tmpSetpoint.XPos = StrToInt(ebXOffset->Text) + StrToInt(ebXSetpoint->Text);
//	tmpSetpoint.YPos = StrToInt(ebYOffset->Text) + StrToInt(ebYSetpoint->Text);
	tmpSetpoint.XPos = StrToInt(ebXSetpointStart->Text) + StrToInt(ebXOffset->Text);
	tmpSetpoint.YPos = StrToInt(ebYSetpointStart->Text) + StrToInt(ebYOffset->Text);

//	tmpSetpoint.XPos = StrToInt(ebXOffset->Text);
// 	tmpSetpoint.YPos = StrToInt(ebYOffset->Text);

	//update plotter setpoint
	Plotter->UpdateSetpoint(tmpSetpoint);
	//Update user labels
	updatePosLabels(this);

}

// ---------------------------------------------------------------------------
// Update labels that indicate position and distance
// ---------------------------------------------------------------------------
void __fastcall TmainForm::updatePosLabels(TObject *Sender)
{
	//get Plotter position and setpoint
	XYPlotter_setpoint_t curSetPoint = Plotter->GetSetpoint();
	XYPlotter_setpoint_t curDistance = Plotter->GetCurDistance();
	//update labels
	lbCords->Caption = "X: " + String(curSetPoint.XPos) + " Y: " + String(curSetPoint.YPos);
	lbDistance->Caption = "X: " + String(curDistance.XPos) + " Y: " + String(curDistance.YPos);
}

//---------------------------------------------------------------------------
// Calls the USBMM object and tells it to connect all devices
//---------------------------------------------------------------------------
void __fastcall TmainForm::btOpenUSBClick(TObject *Sender)
{

//USBMM->DMM_Task();
//return;

//Start USB Threads
USBMM->Start();

tmMeterUpdate->Enabled = true;

//enable appropriate buttons
btOpenUSB->Enabled = false;
btCloseUSB->Enabled = true;

//todo remove this
USBMM->StartNewMeasurement();

}

//---------------------------------------------------------------------------
// Calls the USBMM object and tells it to disconnect all devices
//---------------------------------------------------------------------------
void __fastcall TmainForm::btCloseUSBClick(TObject *Sender)
{
//	TestLogAdd("STOP USB Button PRESSED");

//if (TestStarted)
//	{
//		//Do nothing
//	}
//	else
//	{
	 //	ShowMessage("Stop Pressed");
//		USBMM->RESETManualControlFlag();

		 //Start USB Threads
		USBMM->Stop();
		tmMeterUpdate->Enabled = false;

		measurementRunning = false;
		//enable appropriate buttons
		btOpenUSB->Enabled = true;
		btCloseUSB->Enabled = false;
		//update indicators
		shInputVoltage->Brush->Color = clSilver;
		shInputCurrent->Brush->Color = clSilver;
		shOutputVoltage->Brush->Color = clSilver;
		shOutputCurrent->Brush->Color = clSilver;

//	} //End If

}


//---------------------------------------------------------------------------
// Start the test
//---------------------------------------------------------------------------
void __fastcall TmainForm::btStartTestClick(TObject *Sender)
{


if (lblRxLoadName->Caption == "?W")
{
	ShowMessage("WARNING: Check Rx Load Setting -> ??W");
}
else
{
	// Do Nothing
} // End If


	// Check if Comport Opened
	if (!btnPlotComportOpen->Enabled)
	{
		if (cbDoNotMoveXY->Checked)
		{
		   StringGrid1->Enabled = false;
		}
		else
		{
			// Do Nothing
		}  // End If

		TestStarted = true;

		cbPowerSupplyStatus->Enabled = false;

		InitPlotGrid();

		//indicate start in log
		TestLogAdd("Starting Test...");

		//set state to init state
		testAdmin.testState = TEST_INIT_STATE;

		//update buttons
		btStartTest->Enabled = false;
		rgLoad->Enabled = false;
		btStopTest->Enabled = true;
		btnPause->Enabled = true;

		//set the load
		// turn off all load relays
		Plotter->enLoadRelay(false,0);
		Plotter->enLoadRelay(false,1);
		Plotter->enLoadRelay(false,2);

		Plotter->enLoadRelay(true,rgLoad->ItemIndex);

		//create test file name
		testAdmin.testFile = CreateFileName();

		//set the relay state to measure TX / RX
		Plotter->enRefRelay(true);

		// USBMM->RESETManualControlFlag();

		// ***
		// System TIMERS
		// ***
		//enable test-system timer
		tmTest->Enabled = true;

		//disable automated meter timer
		tmMeterUpdate->Enabled = false;

		//Enable Plotter Timer
		tmPlotter->Enabled = true;

	}
	else
	{
		ShowMessage("Please Check if Plotter Comport Opened in Set Up Sheet");
	} // End If


}


//---------------------------------------------------------------------------
// Stop the test
//---------------------------------------------------------------------------
void __fastcall TmainForm::btStopTestClick(TObject *Sender)
{
	TestStarted = false;

	cbPowerSupplyStatus->Enabled = true;

	SelectedGrid = false;
	StringGrid1->Enabled = true;

	RetryCount = 0;

	//indicate test stop in log
	TestLogAdd("****************");
	TestLogAdd("Test Stopped...");
	TestLogAdd("****************");

	//change to idle state
	//testAdmin.testState = TEST_IDLE_STATE;

	//disable test-system timer
	tmTest->Enabled = false;
	//possibly enable automated meter timer
	tmMeterUpdate->Enabled = false;

	//Kill Plotter Timer
	tmPlotter->Enabled=false;

	//disable power supply
	btDisablePSUClick(this);

	//update buttons
	btStartTest->Enabled = true;
	rgLoad->Enabled = true;
	btStopTest->Enabled = false;
	btnPause->Enabled = false;

//	Same as btCloseUSB procedure;

	USBMM->Stop();
	tmMeterUpdate->Enabled = false;

	//Set Flag
	measurementRunning = false;

	//enable appropriate buttons
	btOpenUSB->Enabled = true;
	btCloseUSB->Enabled = false;
	//update indicators
	shInputVoltage->Brush->Color = clSilver;
	shInputCurrent->Brush->Color = clSilver;
	shOutputVoltage->Brush->Color = clSilver;
	shOutputCurrent->Brush->Color = clSilver;

//*******************************
	//change to idle state
	testAdmin.testState = TEST_IDLE_STATE;

//	ShowMessage("Stopped Pressed");

}

//---------------------------------------------------------------------------
// Update Plotter X offset to home
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebXOffsetChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Cords", "X_Offset", ebXOffset->Text);
	TestLogAdd("New [X Offset] : " + ebXOffset->Text);

//	Reg_WriteString(sREG, "Cords", "X_SetPoint", ebXSetpoint->Text);
//	TestLogAdd("New [X SetPoint] : " + ebXSetpoint->Text);

	//update plotter settings
	XYPlotter_setpoint_t tmpOffset;
	tmpOffset.XPos=StrToInt(ebXOffset->Text);
	tmpOffset.YPos=StrToInt(ebYOffset->Text);
 //	Plotter->SetOffset(tmpOffset);
   //	Plotter->SetPlotStart(tmpOffset);
}

//---------------------------------------------------------------------------
// Update Plotter X offset to home
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebYOffsetChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Cords", "Y_Offset", ebYOffset->Text);
	TestLogAdd("New [Y Offset] : " + ebYOffset->Text);

//	Reg_WriteString(sREG, "Cords", "Y_SetPoint", ebYSetpoint->Text);
//	TestLogAdd("New [Y SetPoint] : " + ebYSetpoint->Text);

	//update plotter settings
	XYPlotter_setpoint_t tmpOffset;
	tmpOffset.XPos=StrToInt(ebXOffset->Text);
	tmpOffset.YPos=StrToInt(ebYOffset->Text);
	//Plotter->SetPlotStart(tmpOffset);

}

//---------------------------------------------------------------------------
// Update Efficiency Calculation
//---------------------------------------------------------------------------
void __fastcall TmainForm::updateEfficiency(void)
{
   // Warning Also update updateEfficiency2

	char inputPowerStr[15];
	char outputPowerStr[15];
	char effStr[15];
	static double nrCalls = 0;
	nrCalls++;

	//calculate latest results
	curMeas.inputPower = curMeas.values[INPUT_VOLTAGE_INDEX] * curMeas.values[INPUT_CURRENT_INDEX];
	curMeas.outputPower = curMeas.values[OUTPUT_VOLTAGE_INDEX] * curMeas.values[OUTPUT_CURRENT_INDEX];
	//capture devision by 0
	curMeas.efficiency = (curMeas.inputPower > 0.0) ? ( curMeas.outputPower * 100.0 / curMeas.inputPower ) : 100.0;

	//build input power string
	sprintf(&inputPowerStr[0],"%.2f W",curMeas.inputPower);
	//build output power string
	sprintf(&outputPowerStr[0],"%.2f W",curMeas.outputPower);
	//build efficiency string
	sprintf(&effStr[0],"%.2f %",curMeas.efficiency);

	lbEffCalcManual->Caption = &effStr[0];

	//calculate time remaining in the test
	//number of Y lines still to test
	int stepsRemaining = ( StrToInt(ebYSteps->Text) - testAdmin.ySteps - 1) * StrToInt(ebXSteps->Text);

	if (stepsRemaining < 0) {
		stepsRemaining = 0;
	}
	//plus number of X points to go in current Y line
	stepsRemaining += StrToInt(ebXSteps->Text) - testAdmin.xSteps;
	//multiply to find total time in seconds
	int timeRemaining = 2 * stepsRemaining * ( ( MEASUREMENT_TIME + StrToInt(ebPowerUpWait->Text) + StrToInt(ebOffTime->Text) ) / 1000 );
	int tempSecs = timeRemaining % 60;
	int tempMins = ( ( timeRemaining - tempSecs ) / 60 ) % 60;
	int tempHours = ( timeRemaining - tempMins * 60 - tempSecs ) / 3600;

	//display results to user
	lbInputPower->Caption = inputPowerStr;
	lbOutputPower->Caption = outputPowerStr;
	lbEff->Caption = effStr;

	if (cbShowProgChart->Checked)
	{
		Series1->Add(curMeas.efficiency);
	}
	else
	{
    	// Do Nothing
	} // End If


	lblTimeRemaining->Caption = "Time Remaining";
	timeRemainlb->Caption = IntToStr(tempHours) + ":" + IntToStr(tempMins) + ":" + IntToStr(tempSecs);

	// ***
	// Update Grid lbEff
	// ***
	// Check Plot Direction
	if (TestStarted )
	{
		if (testAdmin.sign == 1)
		{
			//+ Direction
			StringGrid1->Cells[1 + testAdmin.xSteps][(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps] = effStr;
	//		StringGrid1->Cells[SelGridColNumber][SelGridRowNumber] = effStr;

		}
		else
		{
			//- Direction
			StringGrid1->Cells[(StrToInt(ebXSteps->Text) + 2) - (1 + testAdmin.xSteps)][(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps] = effStr;
	//		StringGrid1->Cells[SelGridColNumber][SelGridRowNumber] = effStr;
		} // End If


	}
	else
	{
//		SelGridRowNumber = StringGrid1->RowCount - StringGrid1->Row;
		if (SelGridColNumber == 0)
		{
			// Do Nothing
		}
		else
		{
			StringGrid1->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = effStr;
		} // End If

	} // End If


}

//---------------------------------------------------------------------------
// Update Efficiency Calculation
//---------------------------------------------------------------------------
void __fastcall TmainForm::updateEfficiency2(void)
{
	char inputPowerStr[15];
	char outputPowerStr[15];
	char effStr[15];
	static double nrCalls = 0;
	nrCalls++;

	//calculate latest results
	curMeas.inputPower = curMeas.values[INPUT_VOLTAGE_INDEX] * curMeas.values[INPUT_CURRENT_INDEX];
	curMeas.outputPower = curMeas.values[OUTPUT_VOLTAGE_INDEX] * curMeas.values[OUTPUT_CURRENT_INDEX];
	//capture devision by 0
	curMeas.efficiency = (curMeas.inputPower > 0.0) ? ( curMeas.outputPower * 100.0 / curMeas.inputPower ) : 100.0;

	//build input power string
	sprintf(&inputPowerStr[0],"%.2f W",curMeas.inputPower);
	//build output power string
	sprintf(&outputPowerStr[0],"%.2f W",curMeas.outputPower);
	//build efficiency string
	sprintf(&effStr[0],"%.2f %",curMeas.efficiency);
	//calculate time remaining in the test
	//number of Y lines still to test
	int stepsRemaining = ( StrToInt(ebYSteps->Text) - testAdmin.ySteps - 0) * StrToInt(ebXSteps->Text);

	if (stepsRemaining < 0) {
		stepsRemaining = 0;
	}
	//plus number of X points to go in current Y line
	stepsRemaining += StrToInt(ebXSteps->Text) - 0 ; //testAdmin.xSteps;
	//multiply to find total time in seconds
	int timeRemaining = 2 * stepsRemaining * ( ( MEASUREMENT_TIME + StrToInt(ebPowerUpWait->Text) + StrToInt(ebOffTime->Text) ) / 1000 );
	int tempSecs = timeRemaining % 60;
	int tempMins = ( ( timeRemaining - tempSecs ) / 60 ) % 60;
	int tempHours = ( timeRemaining - tempMins * 60 - tempSecs ) / 3600;

	//display results to user
	lbInputPower->Caption = inputPowerStr;
	lbOutputPower->Caption = outputPowerStr;
	lbEff->Caption = effStr;

	if (cbShowProgChart->Checked)
	{
		Series1->Add(curMeas.efficiency);
	}
	else
	{
    	// Do Nothing
	} // End If

//	timeRemainlb->Caption = IntToStr(tempHours) + ":" + IntToStr(tempMins) + ":" + IntToStr(tempSecs);
	lblTimeRemaining->Caption = "Repeated Point Count";
	RetryCount = RetryCount + 1;
	timeRemainlb->Caption = IntToStr(RetryCount);

	// ***
	// Update Grid lbEff
	// ***
	// Check Plot Direction
	if (TestStarted )
	{
		if (testAdmin.sign == 1)
		{

			//+ Direction
			StringGrid1->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = effStr;
	//		StringGrid1->Cells[1 + testAdmin.xSteps][(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps] = effStr;
		}
		else
		{
			//- Direction
			StringGrid1->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = effStr;
	//		StringGrid1->Cells[(StrToInt(ebXSteps->Text) + 2) - (1 + testAdmin.xSteps)][(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps] = effStr;
		} // End If
	}
	else
	{
		if (testAdmin.sign == 1)
		{
			//+ Direction
	//		StringGrid1->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = effStr;
			StringGrid1->Cells[1 + testAdmin.xSteps][(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps] = effStr;
		}
		else
		{
			//- Direction
	//		StringGrid1->Cells[SelGridColNumber][StringGrid1->RowCount - SelGridRowNumber] = effStr;
			StringGrid1->Cells[(StrToInt(ebXSteps->Text) + 2) - (1 + testAdmin.xSteps)][(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps] = effStr;
		} // End If

	} // End If

//	SelGridRowNumber = StringGrid1->RowCount - StringGrid1->Row;
//	SelGridColNumber = StringGrid1->Col; // StringGrid1->ColCount - StringGrid1->Col;

}


//---------------------------------------------------------------------------
// Update voltage, current and position measurements
//---------------------------------------------------------------------------
void __fastcall TmainForm::tmMeterUpdateTimer(TObject *Sender)
{

	tmMeterUpdate->Enabled = false;

	if (USBMM->DataAvailable())
	{

		USBMM->Start();

		USBMM->StartNewMeasurement();

		USBLogAdd("Data Available");
		UpdateDAQFormDisplay();
//		tmMeterUpdate->Enabled = false;

		updateEfficiency();

	tmMeterUpdate->Enabled = true;

	} // End If

}

//---------------------------------------------------------------------------
// Update DAQ Form Display for Data Measurement Results
//---------------------------------------------------------------------------
void __fastcall TmainForm::UpdateDAQFormDisplay()
{

	//list pointers to meter data displays
	TEdit *ebPointers[4] = { ebInputVoltage,ebInputCurrent,ebOutputVoltage,ebOutputCurrent };
	TShape *shPointers[4] = { shInputVoltage,shInputCurrent,shOutputVoltage,shOutputCurrent };
	TEdit *mulPointers[4] = { ebVinMultiplier,ebIinMultiplier,ebVoutMultiplier,ebIoutMultiplier };
	TEdit *offsetPointers[4] = { ebVinOffset,ebIinOffset,ebVoutOffset,ebIoutOffset };

	//loop through all meters
	uint8_t curMeter;
	AnsiString curUnit;
	//if data is available, retreive it and update all meters

try
	{

			// Update Form Data Display---
			TestLogAdd("---MeterUpdateTimer Run Data Avai---");
			for(curMeter=0; curMeter < 4; curMeter++)
			{
				if( USBMM->MeterConnected(curMeter) ) {
					//indicate presence of meter
					shPointers[curMeter]->Brush->Color = clLime;
					//get latest measurement from meter
					curMeas.values[curMeter] = USBMM->GetOutput(curMeter);
					curMeas.values[curMeter] += StrToFloat(offsetPointers[curMeter]->Text);
					curMeas.values[curMeter] *= StrToFloat(mulPointers[curMeter]->Text);
					//update measurement indicator to display
					if( curMeas.values[curMeter] < 1.0 )	{
						curUnit = ((curMeter % 2) == 0) ? " mV" : " mA";
						ebPointers[curMeter]->Text = FloatToDigitString(curMeas.values[curMeter]*1000.0) + curUnit;
					} else {
						curUnit = ((curMeter % 2) == 0) ? " V" : " A";
						ebPointers[curMeter]->Text = FloatToDigitString(curMeas.values[curMeter]) + curUnit;
					}

				} else {
					//indicate missing meter
					shPointers[curMeter]->Brush->Color = clSilver;
					//clear meter value textbox
					ebPointers[curMeter]->Text = "";
				}  // End If
			}  // End For
			// ENDS Update Form Data Display---

	} catch (...)
	{
		ShowMessage("Error " + FloatToStr(curMeas.values[curMeter]));
		lbErrorStatus->Caption = "Error in tmMeterUpdateTimer";
	}  // End Try

}

//---------------------------------------------------------------------------
// Add a line to the Test Log Memo
//---------------------------------------------------------------------------
void __fastcall TmainForm::TestLogAdd(AnsiString line)
{
	logit("Log.txt",line);
	memoTestLog->Lines->Add(line);
}

//---------------------------------------------------------------------------
// Add a line to the USB Log Memo
//---------------------------------------------------------------------------
void __fastcall TmainForm::USBLogAdd(AnsiString line)
{
	logit("Log.txt",line);
	memoUSBLog->Lines->Add(line);
}

//---------------------------------------------------------------------------
// Calculate the next setpoint for the test, and update testAdmin
//---------------------------------------------------------------------------
void __fastcall TmainForm::NextSetpoint()
{

//	TestLogAdd("Move to NEXT Plotter TEST POINT");

	//if more steps are to be taken in the x direction
	if(testAdmin.xSteps < StrToInt(ebXSteps->Text) )
	{   //update setpoint
		testAdmin.curSetpoint.XPos += testAdmin.sign*StrToInt(ebStepSize->Text);
		testAdmin.xSteps++;
	//if more steps are to be taken into the Y direction
	} else if (testAdmin.ySteps < StrToInt(ebYSteps->Text)) {
		//update sign
		testAdmin.sign = -testAdmin.sign;
		//update setpoint
		testAdmin.curSetpoint.YPos += StrToInt(ebStepSize->Text);
		testAdmin.xSteps=0;
		testAdmin.ySteps++;
	}

}

//---------------------------------------------------------------------------
// clear the test coordinates in testAdmin
//---------------------------------------------------------------------------
void __fastcall TmainForm::ResetTestCoordinates()
{
	testAdmin.xSteps = 0;
	testAdmin.ySteps = 0;

//	XYPlotter_setpoint_t tmpSetpoint;
//	testAdmin.xSteps = StrToInt(ebXSteps->Text);
//	testAdmin.ySteps = StrToInt(ebYSteps->Text);

	testAdmin.sign = 1;
	testAdmin.curIteration = 0;
//	testAdmin.curSetpoint.XPos = 0;
//	testAdmin.curSetpoint.YPos = 0;
	testAdmin.curSetpoint.XPos = StrToInt(ebXSetpointStart->Text);
	testAdmin.curSetpoint.YPos = StrToInt(ebYSetpointStart->Text);

//	testAdmin.curSetpoint.XPos = StrToInt(ebXOffset->Text);
//	testAdmin.curSetpoint.YPos = StrToInt(ebYOffset->Text);

	testAdmin.curDistance = Plotter->GetCurDistance();
}

//---------------------------------------------------------------------------
// clear the test coordinates in testAdmin
//---------------------------------------------------------------------------
void __fastcall TmainForm::ResetTestCoordinates2()
{
	testAdmin.xSteps = 0;
	testAdmin.ySteps = 0;

//	XYPlotter_setpoint_t tmpSetpoint;
//	testAdmin.xSteps = StrToInt(ebXSteps->Text);
//	testAdmin.ySteps = StrToInt(ebYSteps->Text);

	testAdmin.sign = 1;
	testAdmin.curIteration = 0;
//	testAdmin.curSetpoint.XPos = 0;
//	testAdmin.curSetpoint.YPos = 0;

//	testAdmin.curSetpoint.XPos = StrToInt(ebXSetpointStart->Text);
//	testAdmin.curSetpoint.YPos = StrToInt(ebYSetpointStart->Text);
	testAdmin.curSetpoint.XPos = ((SelGridColNumber - 1) *  StrToInt(ebStepSize->Text)) + StrToInt(ebXSetpointStart->Text);
	testAdmin.curSetpoint.YPos = ((SelGridRowNumber - 1)) * StrToInt(ebStepSize->Text) + StrToInt(ebYSetpointStart->Text);
	//tmpSetpoint.XPos = ((SelGridColNumber - 1) *  StrToInt(ebStepSize->Text)) + StrToInt(ebXSetpointStart->Text);
	//tmpSetpoint.YPos = ((SelGridRowNumber - 1)) * StrToInt(ebStepSize->Text) + StrToInt(ebYSetpointStart->Text);

//	testAdmin.curSetpoint.XPos = StrToInt(ebXOffset->Text);
//	testAdmin.curSetpoint.YPos = StrToInt(ebYOffset->Text);

	testAdmin.curDistance = Plotter->GetCurDistance();
}
//---------------------------------------------------------------------------
// Generate filename based on user input, and already existing files
//---------------------------------------------------------------------------
AnsiString __fastcall TmainForm::CreateFileName(void) {

	//get folder name
	if (AnsiString(ebFolderName->Text[1]).c_str() != "\\") {
		//ebFolderName->Text +=("\\");
	}
	AnsiString sBaseName = ebFolderName->Text + "Plotter_" + IntToStr((int)Plotter_Number) + "_" + ebFilename->Text + "_";
	AnsiString sFileName = "";
	//increase filename index until not file is found with that name/index
	uint8_t cnt = 0;
	do {
		sFileName = sBaseName + IntToStr(cnt) + ".csv";
		cnt++;
	}
	while (FileExists(sFileName));
	//return result
	return sFileName;
}


//---------------------------------------------------------------------------
// write single line to CSV file
//---------------------------------------------------------------------------
void __fastcall TmainForm::WriteToCSV(AnsiString FileName, AnsiString Data) {

	bool newFile = (FileExists(FileName)) ? false : true;
	//try and open existing file to append
	FILE *fileHandle = fopen(FileName.c_str(), "at");
	//if failed, try and open file to write
	if (fileHandle == NULL){
		fileHandle = fopen(FileName.c_str(), "wt");
	}
	//if file was opened
	if (fileHandle != NULL){
		//write header if desired
		if(newFile){
			fputs("X_Index,Y_Index,X_Distance,Y_Distance,Input Voltage,Input Current,Input Power,Output Voltage,Output Current,Output Power,Efficiency,Qi Signal Strength,Coil 1,Coil 2\n"
					,fileHandle);
		}
		//write CSV line
		fputs(Data.c_str(), fileHandle);
		//close file
		fclose(fileHandle);
		//inform user
		TestLogAdd("====");
//		TestLogAdd("Wrote line to CSV file" + ebFilename->Text);
		TestLogAdd("Wrote line to CSV file ->");
		TestLogAdd(ebFilename->Text);
		TestLogAdd("====");
	} else {
		TestLogAdd("Error opening " + ebFilename->Text);
		ShowMessage("WARNING Error Writing to CSV File");
	}

}

//---------------------------------------------------------------------------
// build CSV line, and write it to output file
//---------------------------------------------------------------------------
AnsiString __fastcall TmainForm::GenerateCSVLine(MAIN_measurement_t meas) {
	AnsiString csvLine = "";
try
{
	//write test indices
	if (testAdmin.sign == 1) {
		csvLine += String(testAdmin.xSteps) + ",";
	} else {
		csvLine += String(StrToInt(ebXSteps->Text) - testAdmin.xSteps) + ",";
	}

	csvLine += String(testAdmin.ySteps) + ",";
	//get Plotter position and setpoint
	XYPlotter_setpoint_t curSetPoint = Plotter->GetSetpoint();
	XYPlotter_setpoint_t curDistance = Plotter->GetCurDistance();
	//write distances
	csvLine += String(curDistance.XPos) + ",";
	csvLine += String(curDistance.YPos) + ",";
	//write input voltage, current and power

	String Vin, Iin, Vout, Iout, Pin, Pout, Eff;
//	M= "Measurement [" + String(testAdmin.curSetpoint.XPos) + "],[" + String(testAdmin.curSetpoint.YPos) + "]";
	Vin = FloatToStr((float)meas.values[INPUT_VOLTAGE_INDEX]);
	Iin = FloatToStr((float)meas.values[INPUT_CURRENT_INDEX]);
	Pin = FloatToStr((float)meas.inputPower);
//	Vout = "V-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_VOLTAGE_INDEX]) + "]";
//	Iout = "I-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_CURRENT_INDEX]) + "]";
	csvLine = 	csvLine + Vin + "," + Iin + "," + Pin + ",";

	//csvLine += FloatToStr(meas.values[INPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";
	//csvLine += FloatToStr(meas.values[INPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";
	//csvLine += FloatToStr(meas.inputPower).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";

	//write output voltage, current and power
	Vout = FloatToStr((float)meas.values[OUTPUT_VOLTAGE_INDEX]);
	Iout = FloatToStr((float)meas.values[OUTPUT_CURRENT_INDEX]);
	Pout = FloatToStr((float)meas.outputPower);
	csvLine = csvLine + Vout + "," + Iout + "," + Pout + ",";

//	csvLine += FloatToStr(meas.values[OUTPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";
//	csvLine += FloatToStr(meas.values[OUTPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";
//	csvLine += FloatToStr(meas.outputPower).SubString(0,CSV_SIGNIFICANT_DIGITS) + ",";

	//write efficiency
//	csvLine += FloatToStr(meas.efficiency).SubString(0,CSV_SIGNIFICANT_DIGITS) + "\n";
	Eff = FloatToStr((float)meas.efficiency);
	csvLine = csvLine + Eff + "," ;

	//write Qi Stuff
	csvLine = csvLine + IntToStr(meas.Qi_Signal_Strength) + "," ;
	csvLine = csvLine + IntToStr(meas.Qi_Coil_Num_1) + "," ;
	csvLine = csvLine + IntToStr(meas.Qi_Coil_Num_2) + "," ;


	 AnsiString sLine = "";
	 for (int i = 0; i < 29; i++)
		 {
		 sLine = sLine + IntToStr( ( int )meas.FODA_0_Phase_Buff[i] ) + ",";
		 }
	 for (int i = 0; i < 29; i++)
		 {
		 sLine = sLine +IntToStr( ( int )meas.FODA_90_Phase_Buff[i] );
	 if (i != 28)
		 {
		 sLine = sLine + "," ;
		 }
		 }

		csvLine = csvLine + sLine;




	csvLine = csvLine + "\n";
	return csvLine;

} catch (...)
{
	ShowMessage("Error in GenerateCSVLine");
	lbErrorStatus->Caption = "Error in GenerateCSVLine";

}
	return csvLine;
}

//---------------------------------------------------------------------------
// Main test-application state machine
//---------------------------------------------------------------------------
void __fastcall TmainForm::tmTestTimer(TObject *Sender)
{
static float WaitTimerTicks;
static float WaitTimerTicksMax;

static float WaitOffTimerTicks;
static float WaitOffTimerTicksMax;

try
{
	switch (testAdmin.testState) {

	   case TEST_IDLE_STATE:
			lbErrorStatus->Caption = "STATE -> Test_Idle State";
			break;


		case TEST_INIT_STATE:

			Series1->Clear();

			WaitTimerTicks = 0;
			WaitOffTimerTicks = 0;

			WaitTimerTicksMax = StrToFloat(ebPowerUpWait->Text)/200;
			WaitOffTimerTicksMax = StrToFloat(ebOffTime->Text)/200;

			//Indicate start of test
			lbErrorStatus->Caption = "STATE -> Test_Init State";
			TestLogAdd("Starting Test : " + ebXSteps->Text + "," + ebYSteps->Text + "," + ebStepSize->Text);
			//disable PSU
			btDisablePSUClick(this);

#ifndef USB_PSU
			tmPlotter_NRT->Enabled = true;
			testAdmin.NRT_Goto = true;
			testAdmin.NRTState =TEST_INIT_STATE;
#endif

		if (cbDoNotMoveXY->Checked)
		{
			// Do Not Move
			ResetTestCoordinates2();

			testAdmin.testState = TEST_MOVE_STATE;

		}
		else
		{

			//initialize test
			ResetTestCoordinates();
			testAdmin.curIteration = 0;

//			if (cbSimPlotting->Checked)
//			{
//				// Do Nothing
//			}
//			else
//			{
				//Set Initial Setpoint
				Plotter->UpdateSetpoint(testAdmin.curSetpoint);
				tmPlotter_NRT->Enabled = true;
				testAdmin.NRT_Goto = true;

//			} // End If

			testAdmin.NRTState =TEST_MOVE_NRT;

		} // End If

			//add log entry to let user validate positions..
			TestLogAdd("Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
						+ String(testAdmin.curSetpoint.YPos) + "]");
			//if plotter has reached destination
			testAdmin.testState = TEST_MOVE_STATE;
			//start measurement equipment

			USBMM->Start();
			btOpenUSB->Enabled = false;
			btCloseUSB->Enabled = true;
			break;

		case TEST_MOVE_NRT:

			Plotter->UpdateSetpoint(testAdmin.curSetpoint);

			if (cbSimPlotting->Checked)
			{
				// Do Nothing
			}
			else
			{
				tmPlotter_NRT->Enabled = true;
			}  // End If


			testAdmin.NRT_Goto = true;
			testAdmin.NRTState =TEST_MOVE_NRT;

  			TestLogAdd("NRT Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
						+ String(testAdmin.curSetpoint.YPos) + "]");

			testAdmin.testState = TEST_MOVE_STATE;

			break;

		case TEST_MOVE_STATE:

		if (WaitOffTimerTicks == 0)
		{

			lbErrorStatus->Caption = "STATE -> Test_Move State";

			if (cbSimPlotting->Checked)
			{
					// Simulate Plotting
					testAdmin.testState = TEST_ENABLE_PSU_STATE;
					// ***
					//  Off Time
					// ***
			   //		tmTest->Interval = 	StrToInt(ebOffTime->Text);  // Set the state timer to power off time

					Qi_Signal_Strength = 0;
					First_Qi_Coil = true;
					Qi_Coil_Num_1 = 0;
					Qi_Coil_Num_2 = 0;

			}
			else
			{
				if( Plotter->SetpointReached() )
				{   //setup timeout

					testAdmin.testState = TEST_ENABLE_PSU_STATE;

					// ***
					// Off Time
					// ***
				   //	tmTest->Interval = 	StrToInt(ebOffTime->Text);  // Set the state timer to power off time

					Qi_Signal_Strength = 0;
					First_Qi_Coil = true;
					Qi_Coil_Num_1 = 0;
					Qi_Coil_Num_2 = 0;

				} // End If

			}  // End If


		}
		else
		{
			// ***
			// Slow Timer down to get wait time
			// ***
			WaitOffTimerTicks = WaitOffTimerTicks + 1.0;
			if (WaitOffTimerTicks > WaitOffTimerTicksMax)
			{
			   // Done
				lbErrorStatus->Caption = "STATE -> Test_Move State";

				if (cbSimPlotting->Checked)
				{
					// Simulate Plotting
					testAdmin.testState = TEST_ENABLE_PSU_STATE;
					// ***
					//  Off Time
					// ***
				   //	tmTest->Interval = 	StrToInt(ebOffTime->Text);  // Set the state timer to power off time

					Qi_Signal_Strength = 0;
					First_Qi_Coil = true;
					Qi_Coil_Num_1 = 0;
					Qi_Coil_Num_2 = 0;

				}
				else
				{
					if( Plotter->SetpointReached() )
					{   //setup timeout

						testAdmin.testState = TEST_ENABLE_PSU_STATE;

					// ***
					// Off Time
					// ***
					//tmTest->Interval = 	StrToInt(ebOffTime->Text);  // Set the state timer to power off time

					Qi_Signal_Strength = 0;
					First_Qi_Coil = true;
					Qi_Coil_Num_1 = 0;
					Qi_Coil_Num_2 = 0;

				} // End If

			}  // End If

			}
			else
			{
				// Do Nothing
			} // End If

		} // End If


			break;

		case TEST_ENABLE_PSU_STATE:

		if (WaitTimerTicks < 1.0)
		{
			  // Run Once only
			lbErrorStatus->Caption = "STATE -> Test_Enable_PSU State";

			// ***
			//enable power supply
			// ***
			btEnablePSUClick(this);

#ifndef USB_PSU
			if (cbSimPlotting->Checked)
			{
				// Do Nothing
			}
			else
			{
				tmPlotter_NRT->Enabled = true;
			} // End If

			testAdmin.NRT_Goto = true;
			testAdmin.NRTState =TEST_ENABLE_PSU_STATE;
#endif

			tmTest->Interval = 	200;  // Set the state timer to default time todo make define

			//setup timeout

//			testAdmin.testState = TEST_GET_DATA_STATE;


		}
		else
		{
			// Do Nothing

		} // End If


			// ***
			// Slow Timer down to get wait time
			// ***
			WaitTimerTicks = WaitTimerTicks + 1.0;
			if (WaitTimerTicks > WaitTimerTicksMax)
			{
				// Move State
				testAdmin.testState = TEST_GET_DATA_STATE;
				WaitTimerTicks = 0;
			}
			else
			{
				// Stay in State
				testAdmin.testState = TEST_ENABLE_PSU_STATE;
				lblWaitCountdown->Caption = FloatToStr(WaitTimerTicksMax - WaitTimerTicks);

			} // End If
//			tmTest->Interval = 	StrToInt(ebPowerUpWait->Text);       // Set the state timer to power On time

			break;

		case TEST_GET_DATA_STATE:

			lbErrorStatus->Caption = "STATE -> Test_Get_Data State";
			tmTest->Interval = 	200;  // Set the state timer to default time todo make define

			TestLogAdd("----");
			TestLogAdd("Start New Measurement Test State");

			if (cbSimPlotting->Checked)
			{
				// Do Nothing
			}
			else
			{
				USBMM->StartNewMeasurement();
			} // End If

			//Change STATE
			testAdmin.testState = TEST_PROCESS_DATA_STATE;
			break;

		case TEST_PROCESS_DATA_STATE:

			//temporary measurement storage
			MAIN_measurement_t tmpMeas;

			lbErrorStatus->Caption = "STATE -> Test_Process_Data State";

			USBMM->DMM_Task();

			//wait till data is available
			if(USBMM->DataAvailable())
			{
				TestLogAdd("STATE -> Test_Process_Data State");
				try
					{
					//process and backup data

					UpdateDAQFormDisplay();

					if (cbDoNotMoveXY->Checked)
					{
						updateEfficiency2();
					}
					else
					{
						updateEfficiency();
					} // Ends If


					tmpMeas = curMeas;
					//display data to user

					// Add the Qi Stuff now
					tmpMeas.Qi_Signal_Strength = Qi_Signal_Strength;
					tmpMeas.Qi_Coil_Num_1 = Qi_Coil_Num_1;
					tmpMeas.Qi_Coil_Num_2 = Qi_Coil_Num_2;
					lbQiSignal_strength->Caption = IntToStr(Qi_Signal_Strength);

//				 ShowMessage("Test2");

					memoUSBLog->Lines->Add("******");
					String M, Vin, Iin, Vout, Iout;
					M= "Measurement [" + String(testAdmin.curSetpoint.XPos) + "],[" + String(testAdmin.curSetpoint.YPos) + "]";
					Vin = "V-in [" + FloatToStr((float)tmpMeas.values[INPUT_VOLTAGE_INDEX]) + "]";
					Iin = "I-in [" + FloatToStr((float)tmpMeas.values[INPUT_CURRENT_INDEX]) + "]";
					Vout = "V-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_VOLTAGE_INDEX]) + "]";
					Iout = "I-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_CURRENT_INDEX]) + "]";

					memoUSBLog->Lines->Add(M);
					memoUSBLog->Lines->Add(Vin);
					memoUSBLog->Lines->Add(Iin);
					memoUSBLog->Lines->Add(Vout);
					memoUSBLog->Lines->Add(Iout);

					memoUSBLog->Lines->Add("*******");

/*					memoUSBLog->Lines->Add("Measurement [" + String(testAdmin.curSetpoint.XPos) + "],[" + String(testAdmin.curSetpoint.YPos) + "]");
					memoUSBLog->Lines->Add("V-in [" + FloatToStr((float)tmpMeas.values[INPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + "]");
					memoUSBLog->Lines->Add("I-in [" + FloatToStr((float)tmpMeas.values[INPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + "]");
					memoUSBLog->Lines->Add("V-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + "]");
					memoUSBLog->Lines->Add("I-out [" + FloatToStr((float)tmpMeas.values[OUTPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS) + "]");
*/
				   } catch (...)
				   {
						TestLogAdd("ERROR in Data available State");
//				   		ShowMessage(FloatToStr(tmpMeas.values[INPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS));
//				   		ShowMessage(FloatToStr(tmpMeas.values[INPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS));
//				   		ShowMessage(FloatToStr(tmpMeas.values[OUTPUT_VOLTAGE_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS));
//				   		ShowMessage(FloatToStr(tmpMeas.values[OUTPUT_CURRENT_INDEX]).SubString(0,CSV_SIGNIFICANT_DIGITS));
				   }

				  //disable PSU
				  if (cbEnablePSUControl->Checked)
				  {
//					ShowMessage("PSU Control Auto ON/OFF");
					btDisablePSUClick(this);
#ifndef USB_PSU
					tmPlotter_NRT->Enabled = true;
					testAdmin.NRT_Goto = true;
					testAdmin.NRTState =TEST_PROCESS_DATA_STATE;
#endif

				  }
				  else
				  {
//					ShowMessage("PSU Control Manual Control");
					//Do nothing
				  } // End If

					//store DATA
					WriteToCSV(testAdmin.testFile,GenerateCSVLine(tmpMeas));

					//if last step has been taken already, stop test
					if(	testAdmin.ySteps == StrToInt(ebYSteps->Text) &&
						testAdmin.xSteps == StrToInt(ebXSteps->Text)   )
					{
						TestLogAdd("Reached Test Steps Max -> Y Steps= " + ebYSteps->Text + " X Steps =  " + ebXSteps->Text);

					   //*******************************
					   // DEBUG AREA

						int count = StrToInt(ebRepeat->Text);
						if(count != 0 )
							{
							 count--;
							 ebRepeat->Text= IntToStr(count);
							}
						if (count)
							{
							btStartTestClick(this);
							}
						else
							{
							btStopTestClick(this);

							TestLogAdd("Test Finished");
							ShowMessage("Test Finished");
							 }
						//For LOOP Testing!!!!!!!!!!
//						btStartTestClick(this);
//						TestLogAdd("Loop Test*********");
					   //*******************************

					}
					else
					{

						if (cbDoNotMoveXY->Checked)
						{
							// Do Not Move Plotter
							//add log entry to let user validate positions..
							TestLogAdd("----");
							TestLogAdd("Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
									+ String(testAdmin.curSetpoint.YPos) + "]");
							//if plotter has reached destination
							testAdmin.testState = TEST_MOVE_STATE;
						}
						else
						{
							// Move to Next Position
							//Prepare next iteration or stop
							NextSetpoint();
							//goto new setpoint
							Plotter->UpdateSetpoint(testAdmin.curSetpoint);
							tmPlotter_NRT->Enabled = true;
							testAdmin.NRT_Goto = true;
							testAdmin.NRTState =TEST_MOVE_NRT;

							//add log entry to let user validate positions..
							TestLogAdd("----");
							TestLogAdd("Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
									+ String(testAdmin.curSetpoint.YPos) + "]");
							//if plotter has reached destination
							testAdmin.testState = TEST_MOVE_STATE;
						} // End If

						if (cbDoNotMoveXY->Checked)
						{
							// Do Not Move Plotter
						}
						else
						{
							// Move to Next Position


						} // End If
					}  // End If
			}
			else
			{

				if (cbSimPlotting->Checked)
				{
				  // ***
				  // SIMULATION
				  // ***

					if (cbDoNotMoveXY->Checked)
					{
						updateEfficiency2();
					}
					else
					{
						updateEfficiency();
					} // Ends If


					// ***
					// Update Grid   lbEff
					// ***
					// Check Plot Direction
/*					if (testAdmin.sign == 1)
					{
						//+ Direction
						StringGrid1->Cells[1 + testAdmin.xSteps][(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps] = 0;
					}
					else
					{
						//- Direction
						StringGrid1->Cells[(StrToInt(ebXSteps->Text) + 2) - (1 + testAdmin.xSteps)][(StrToInt(ebYSteps->Text) + 1) - testAdmin.ySteps] = 0;
					} // End If
*/

		//			StringGrid1->Cells[testAdmin.curSetpoint.XPos][testAdmin.curSetpoint.YPos] = 0;

				  // Do Nothing
				  //disable PSU
				  if (cbEnablePSUControl->Checked)
				  {
					// ShowMessage("PSU Control Auto ON/OFF");
					btDisablePSUClick(this);
					#ifndef USB_PSU
					tmPlotter_NRT->Enabled = true;
					testAdmin.NRT_Goto = true;
					testAdmin.NRTState =TEST_PROCESS_DATA_STATE;
					#endif

				  }
				  else
				  {
					//Do nothing
				  } // End If

					//store DATA
				   	WriteToCSV(testAdmin.testFile,GenerateCSVLine(tmpMeas));

					//if last step has been taken already, stop test
					if(	testAdmin.ySteps == StrToInt(ebYSteps->Text) &&
						testAdmin.xSteps == StrToInt(ebXSteps->Text)   )
					{

						TestLogAdd("Reached Test Steps Max -> Y Steps= " + ebYSteps->Text + " X Steps =  " + ebXSteps->Text);

					   //*******************************
					   // DEBUG AREA
						int count = StrToInt(ebRepeat->Text);
						if(count != 0 )
							{
							 count--;
							 ebRepeat->Text= IntToStr(count);
							}
						if (count)
							{
							btStartTestClick(this);
							}
						else
							{
							btStopTestClick(this);

							TestLogAdd("Test Finished");
							ShowMessage("Test Finished");
							 }
						//For LOOP Testing!!!!!!!!!!
						//btStartTestClick(this);
						//TestLogAdd("Loop Test*********");
					   //*******************************

					}
					else
					{

						if (cbDoNotMoveXY->Checked)
						{
							// Do Not Move Plotter
							//add log entry to let user validate positions..
							TestLogAdd("----");
							TestLogAdd("Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
									+ String(testAdmin.curSetpoint.YPos) + "]");
							//if plotter has reached destination
							testAdmin.testState = TEST_MOVE_STATE;

						}
						else
						{
							// Move to Next Position
							//Prepare next iteration or stop

//                  			Series1->Clear();

							NextSetpoint();
							//goto new setpoint
							Plotter->UpdateSetpoint(testAdmin.curSetpoint);
							tmPlotter_NRT->Enabled = true;
							testAdmin.NRT_Goto = true;
							testAdmin.NRTState =TEST_MOVE_NRT;

							//add log entry to let user validate positions..
							TestLogAdd("----");
							TestLogAdd("Moving To [" + String(testAdmin.curSetpoint.XPos) + ","
									+ String(testAdmin.curSetpoint.YPos) + "]");
							//if plotter has reached destination
							testAdmin.testState = TEST_MOVE_STATE;

						} // End If

					}  // End If

				  // ***
				  // ENDS SIMULATION
				  // ***

				}
				else
				{
					//*************************
					// Handle comms failure here to stop "apparent lock up" loop
					TestLogAdd("USB DAQ Data NOT available");
					//*************************
				}  // End If

			} // End If
			break;

		case TEST_PAUSE_STATE:
			lbErrorStatus->Caption = "STATE -> Test_pause_state";
			// do nothing.  The button click will bring us out of the pause state
			break;

		default:
			//something weird happened, stop test.
			lbErrorStatus->Caption = "STATE -> Undefined State";
			TestLogAdd("Oops : invalid state");
			btStopTestClick(this);
	}

} catch (...)
{
//	ShowMessage("Error in State Machine");
//	TestLogAdd("Oops : invalid state");

	lbErrorStatus->Caption = "Error in State Machine";

}
//return ;
}

//---------------------------------------------------------------------------
// Tell plotter to request new position,
//---------------------------------------------------------------------------
void __fastcall TmainForm::tmPlotterTimer(TObject *Sender)
{

//	TestLogAdd("Plotter Timer Fired****");

	//update plotter connection indicator
	shPlotterConnected->Brush->Color = (Plotter->UpdateConnectionState()) ? clLime :clSilver;
	//get relay connected state
	if(Plotter->relayState == true){
		btRelayOn->Enabled = false;
		btRelayOff->Enabled = true;
	} else {
		btRelayOn->Enabled = true;
		btRelayOff->Enabled = false;
	}
	//instruct plotter to process any received comms
//	Plotter->ProcessRXData();
	//instruct plotter to retreive new data, and print latest

//	if (cbSimPlotting->Checked)
//	{
		// Do Nothing
//	}
//	else
//	{
		Plotter->RequestDistance();
		//update plotter state to latest known
		testAdmin.curDistance = Plotter->GetCurDistance();
		updatePosLabels(this);

//	} // End If

}


//---------------------------------------------------------------------------
// Store Callibration settings when edited
//---------------------------------------------------------------------------
void __fastcall TmainForm::ebVinMultiplierChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Multipliers", "Vin", ebVinMultiplier->Text);
	TestLogAdd("New [Vin *] : " + ebVinMultiplier->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebIinMultiplierChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Multipliers", "Iin", ebIinMultiplier->Text);
	TestLogAdd("New [Iin *] : " + ebIinMultiplier->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebVoutMultiplierChange(TObject *Sender)
{
   Reg_WriteString(sREG, "Multipliers", "Vout", ebVoutMultiplier->Text);
   TestLogAdd("New [Vout *] : " + ebVoutMultiplier->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebIoutMultiplierChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Multipliers", "Iout", ebIoutMultiplier->Text);
	TestLogAdd("New [Iout *] : " + ebIoutMultiplier->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebVinOffsetChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Offsets", "Vin", ebVinOffset->Text);
	TestLogAdd("New [Vin +] : " + ebVinOffset->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebIinOffsetChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Offsets", "Iin", ebIinOffset->Text);
	TestLogAdd("New [Iin +] : " + ebIinOffset->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebVoutOffsetChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Offsets", "Vout", ebVoutOffset->Text);
	TestLogAdd("New [Vout +] : " + ebVoutOffset->Text);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebIoutOffsetChange(TObject *Sender)
{
	Reg_WriteString(sREG, "Offsets", "Iout", ebIoutOffset->Text);
	TestLogAdd("New [Iout +] : " + ebIoutOffset->Text);
}
//---------------------------------------------------------------------------


void __fastcall TmainForm::btTXOpenCommsClick(TObject *Sender)
{

	//acquire the required comports
	TXComPort = UserSelectComport("Select TX Com-Port", "PortTX", 115200);

	// if com-port could not be openen close application
	if( TXComPort == NULL )
	{
		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
		shpTxConnected->Brush->Color = clSilver;

	} else {
		sdFlushPort(TXComPort);
		btTXOpenComms->Enabled = false;
		btTXCloseComms->Enabled = true;
		//start comms timer
		TXComPort_Rx_Thread = new Rx_Thread(TXComPort,RX_NODE_TX_COMPORT );

		shpTxConnected->Brush->Color = clLime;
	}  // End If


/*               //28/7/15
//acquire the required comports
	UserSelectComport(Qi_Comport,"Select TX Com-Port", "PortTX", 115200);

	// if com-port could not be openen close application
	if( Qi_Comport->Open == false ) {
		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
	} else {
		Qi_Comport->FlushInBuffer();
		Qi_Comport->FlushOutBuffer();
		btTXOpenComms->Enabled = false;
		btTXCloseComms->Enabled = true;
		//start comms timer

	}

*/

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btTXCloseCommsClick(TObject *Sender)
{

	shpTxConnected->Brush->Color = clSilver;

	//stop timer
	sdFlushPort(TXComPort);
	sdClosePort(TXComPort);

	TXComPort_Rx_Thread->FreeOnTerminate = false;
	TXComPort_Rx_Thread->Terminate();
	TXComPort_Rx_Thread->WaitFor() ;

	delete TXComPort_Rx_Thread;
	//set buttons
	btTXOpenComms->Enabled = true;
	btTXCloseComms->Enabled = false;
}
//---------------------------------------------------------------------------




// ---------------------------------------------------------------------------
void __fastcall TmainForm::Qi_ComportRxChar(int Count, uint8_t * pData)
{
	enum states
		{
		WAIT_SOM, SEQ, HEADER, DATA, CHECKSUM, DONE
		} ;

	static char RxBuffer[ 400 ];
	static int State = WAIT_SOM;
	char input;
	static char LRC;
	static int i;
	static int Ri_Ptr;
	static boolean Got_DLE = false;

	// if (ComPort1->Open == false)
	// {
	// return;
	// }

	while ( Count != 0 )
		{

		if ( Received_Message == false )
			{

			input = *pData++ ;

			switch ( State )
				{
			case WAIT_SOM:
				if ( input == STX )
					{
					Ri_Ptr = 0;
					RxBuffer[ Ri_Ptr++ ] = input;
					LRC = 0;
					State = SEQ;
					}
				break;

			case SEQ:
				RxBuffer[ Ri_Ptr++ ] = input;
				State = DATA;
				break;

/*			case DATA:  //28/7/15
				if ( ( input == DLE ) && ( !Got_DLE ) )
					{
					Got_DLE = true;
					LRC += DLE; // Add to LRC because I am throwing this away.
					}
				else
					if ( Got_DLE )
						{
						RxBuffer[ Ri_Ptr++ ] = input;
						Got_DLE = false;
						}
					else
						{
						Got_DLE = false;
						if ( input != ETX )
							{
							RxBuffer[ Ri_Ptr++ ] = input;
							}
						else
							{
							// got message do check sum

							for ( i = 1; i <= Ri_Ptr - 2; ++i )
								{
								LRC += RxBuffer[ i ];
								}

							LRC = ( ( LRC + ( ( LRC & 0xC0 ) >> 6 ) )
								& 0x3F ) + 0x20;
							if ( LRC == RxBuffer[ Ri_Ptr - 1 ] )
								// Checksum ok. Copy to working RxBuffer.
								{
								RxBuffer[ Ri_Ptr - 1 ] = 0;
								// (* Null terminated *)

								Received_Message = TRUE;
								memcpy( RiBuf, RxBuffer, sizeof( RxBuffer ) );

//								PostMessage( TmainForm->Handle,
//									UM_RxMESSAGE, 0, 0 );
//								Application->ProcessMessages( );
								if(Received_Message)
								{
								  ProcessRxMessage();
								  Received_Message = FALSE;
								}

								// Form1->Memo1->Lines->Add(RxBuffer);
								State = WAIT_SOM;
								}
							// else 	// Checksum wrong throw data away
							// {
							// State = WAIT_SOM;
							// }
							}
						} // end of got message
				break;

*/

case DATA:
				if (input != ETX)
					{
					if (Ri_Ptr < (sizeof(RxBuffer)-2))
						{
						RxBuffer[Ri_Ptr++] = input;
						}
					else
						{  //buffer overflow, Start looking for STX again
						State = WAIT_SOM;
						TestLogAdd("Overflow Error");
						break;
						}
					}
				else
					{
					// got message do check sum
					for (i = 1; i <= Ri_Ptr - 2; ++i)
						{
						LRC += RxBuffer[i];
						}

					LRC = ((LRC + ((LRC & 0xC0) >> 6)) & 0x3F) + 0x20;
					if (LRC == RxBuffer[Ri_Ptr - 1])  // Checksum ok. Copy to working RxBuffer.
						{
						RxBuffer[Ri_Ptr - 1] = 0;
						// (* Null terminated *)

						Recived_Message = TRUE;
						memcpy(RiBuf, RxBuffer, sizeof(RxBuffer));

						PostMessage(mainForm->Handle, UM_RxMESSAGE, 0, 0);
						Application->ProcessMessages();
						}
					else
						{
						TestLogAdd("Checksum Error");
						}
					State = WAIT_SOM;
					}
				break;





				} // end of case

			Count-- ;

			} // end of (Received_Message == false)
		Application->ProcessMessages( );
		}

}

// ---------------------------------------------------------------------------

void __fastcall TmainForm::ProcessRxMessage( void )
	{
	unsigned char i;
	unsigned char This_Seq = RiBuf[ SEQ ];

	TXCommMemo->Lines->Add( RiBuf );
	logit("Log.txt","--coms--" + AnsiString(RiBuf));

	switch ( RiBuf[ HDR ] ) // switch header
		{

	case 'Q':
		if ( ( This_Seq != RxSeqNum ) || ( This_Seq == '0' ) )
			{
			RxSeqNum = This_Seq;
			// do stuff here
			OutPutQ( AnsiString( & RiBuf[ DATA ] ) );
			}
		break;

	case 'n':

		Waiting_On_Manual_Mode = false;
		if ( ( This_Seq == TxSeqNum ) && ( Tx_State == WAITING_FOR_ACK ) )
			{
			Tx_State = TX_BUFF_EMPTY;
			}
		else
			{
			NackCount++ ;
			Tx_State = SENDING_DATA;
			StartTransmitMessage( ); // resend message
			}
		break;

		//28/7/15
case 'k': // Scan and power
		Waiting_On_Scan_n_Power = false;
		if ( ( This_Seq == TxSeqNum ) && ( Tx_State == WAITING_FOR_ACK ) )
			{
			Tx_State = TX_BUFF_EMPTY;
			}
		else
			{
			NackCount++ ;
			Tx_State = SENDING_DATA;
			StartTransmitMessage( ); // resend message
			}
		break;


	case 'p':
		Waiting_On_Q_Message_Enable = false;
		if ( ( This_Seq == TxSeqNum ) && ( Tx_State == WAITING_FOR_ACK ) )
			{
			Tx_State = TX_BUFF_EMPTY;
			}
		else
			{
			NackCount++ ;
			Tx_State = SENDING_DATA;
			StartTransmitMessage( ); // resend message
			}
		break;


		} // end of switch
	}

// ---------------------------------------------------------------------------

void __fastcall TmainForm::OutPutQ( AnsiString sOutput )
	{
	AnsiString sTemp;
	int16_t iTemp;
	double dTemp, dTemp2;
	unsigned char TempBuffer[ 255 ];
	AnsiString sNum;
	char * Ptr;
	int LoopCount;

	char Flag = sOutput[ 1 ];

	AnsiString sOut = sOutput.SubString( 2, sOutput.Length( ) - 1 );

	memcpy( TempBuffer, sOut.c_str( ), sOut.Length( ) );
	Ptr = TempBuffer;


	if ( Flag == '~' )
		{// the Tx was reset.  Lets clear the GUI
		Reset_GUI();
		}


	if ( Flag == 'G' )
		{ // Qi message
		qiMsg_t message;
		// first byte says how many bytes will follow.
		sNum = AnsiString( Ptr, 2 );
		Ptr += 2;
		message.num_bytes = StrToInt( "0x" + sNum );

		if ( message.num_bytes > 0 ) // is there a header?  no error?
			{
			sNum = AnsiString( Ptr, 2 );
			Ptr += 2;
			message.header = StrToInt( "0x" + sNum );
			}

		if ( message.num_bytes > 2 ) // is there data?
			{
			uint8_t i;
			for ( i = 0; i < message.num_bytes - 2;
			i++ ) // go through all data bytes (not header or checksum)
				{
				sNum = AnsiString( Ptr, 2 );
				Ptr += 2;
				message.data[ i ] = StrToInt( "0x" + sNum );
				}
			}

		if ( message.num_bytes > 1 ) // is there a checksum?
			{
			sNum = AnsiString( Ptr, 2 );
			Ptr += 2;
			message.checksum = StrToInt( "0x" + sNum );
			}

		message.received_time = message.received_time.CurrentTime( );
		// store the time when this was received.

		message.config.configured = 0;
		// flag that this message does not have an associated Qi configuration.
		message.checked_for_errors = 0; // not yet checked for errors.
		message.errors = 0; // no errors yet

		message.isStatus = 0; // not a status update but a packet

		//qiViewer->Add_Qi_Message( message ); // add the message to the display

		if (message.header == QI_H_SIG_STRENGTH )
			{
			//Qi_Signal_Strength = message.data[0];
			//lbQiSignal_strength->Caption = IntToStr(message.data[0]);
			}
		}

	if ( Flag == 'A' )
		{
		unsigned int Index, Status, Result;
		bool success = true;

		try
			{
			sNum = AnsiString( Ptr, 2 ); // index
			Ptr += 2;
			Index = StrToInt( "0x" + sNum );
			sNum = AnsiString( Ptr, 2 ); // status
			Ptr += 2;
			Status = StrToInt( "0x" + sNum );
			sNum = AnsiString( Ptr, 2); // result
			Ptr += 2;
			Result = StrToInt( "0x" + sNum );
			}
		catch ( const EConvertError & error )
			{
			success = false;
			}

		if ( success )
			{

			if (Status == TCS_Powered)
				{
				lbQiSignal_strength->Caption =  IntToStr((int)Result);
				Qi_Signal_Strength = Result;

				if (First_Qi_Coil)
					{
					Qi_Coil_Num_1 = Index +1;
					First_Qi_Coil = false;
					}
				else
					{
					Qi_Coil_Num_2 = Index +1;
					}
				}
			}
		}

	if ( Flag == 'M' )  //FODA Readings
		{
		uint8_t Coil_Index;
		uint16_t Phase_0;
		uint16_t Phase_90;
		uint8_t Is_Calibration_Data;
		bool success = true;

		try
			{
			sNum = AnsiString( Ptr, 2 );
			Ptr += 2;
			Is_Calibration_Data = StrToInt( "0x" + sNum );

			sNum = AnsiString( Ptr, 2 );
			Ptr += 2;
			Coil_Index = StrToInt( "0x" + sNum );

			sNum = AnsiString( Ptr, 4 );
			Ptr += 4;
			Phase_0 = StrToInt( "0x" + sNum );

			sNum = AnsiString( Ptr, 4 );
			Ptr += 4;
			Phase_90 = StrToInt( "0x" + sNum );

			}
		catch ( const EConvertError & error )
			{
			success = false;
			}


		if ( success )
			{

			if (Is_Calibration_Data)
				{
				//coilViewer->SetFODA_Calibration(Coil_Index,Phase_0,Phase_90);
				}
			else       // this is coil reading data
				{
				curMeas.FODA_0_Phase_Buff[Coil_Index] = Phase_0;
				curMeas.FODA_90_Phase_Buff[Coil_Index] = Phase_90;



				if (Coil_Index == 28)
					{
					WriteToCSV(testAdmin.testFile,GenerateCSVLine(curMeas));
					}

//					 AnsiString sLine = "";
//
//					 for (int i = 0; i < 29; i++)
//					 {
//					 sLine = sLine + IntToStr( ( int )FODA_0_Phase_Buff[i] ) + ",";
//					 }
//
//					 for (int i = 0; i < 29; i++)
//					 {
//					 sLine = sLine +IntToStr( ( int )FODA_90_Phase_Buff[i] );
//					 if (i != 28)
//						 {
//						 sLine = sLine + "," ;
//						 }
//					 }
//					sLine = sLine +"\n" ;
//					logit( "FODA_Full.csv",sLine);
//					}


				}
			 }
		}





	}
// ---------------------------------------------------------------------------



void __fastcall TmainForm::btRelayOnClick(TObject *Sender)
{

	//send relay command
	Plotter->enRefRelay(true);

	//set button state accordingly
	btRelayOn->Enabled = false;
	btRelayOff->Enabled = true;
	Plotter->relayState = true;


}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btRelayOffClick(TObject *Sender)
{
	//send relay command
	Plotter->enRefRelay(false);

	//set button state accordingly
	btRelayOn->Enabled = true;
	btRelayOff->Enabled = false;
	Plotter->relayState = false;
}

//---------------------------------------------------------------------------

void __fastcall TmainForm::btnPauseClick(TObject *Sender)
{
// save all parameters and enter pause state
   if (btnPause->Caption == "PAUSED")
   {
	btnPause->Caption = "Pause" ;
	//enable or go to the previous state and previous set parameters.
	PauseState = false;
	testAdmin.testState = testAdmin.LastState;  //restore the last state
   }
   else
   {
	btnPause->Caption = "PAUSED" ;
	testAdmin.LastState = testAdmin.testState;  // save the current state so we can restore it later
	testAdmin.testState = TEST_PAUSE_STATE;
	PauseState = true;
   }  // End If
}
//---------------------------------------------------------------------------


void __fastcall TmainForm::CloseProgram1Click(TObject *Sender)
{
   //Run Shutdown Sequence here

	//close PSU comport
//	sdFlushPort(PSUComPort);
//	sdClosePort(PSUComPort);
	//close Plotter comport
//	sdFlushPort(PlotterComPort);
//	sdClosePort(PlotterComPort);
	//close TX comport if opened
//	if(TXComPort != NULL){
//		sdFlushPort(TXComPort);
//		sdClosePort(TXComPort);
//	}

	USBMM->Stop();
	tmTest->Enabled = false;
	mainForm->Close();
}
//---------------------------------------------------------------------------



void __fastcall TmainForm::cbEnablePSUControlClick(TObject *Sender)
{
/*	if (cbEnablePSUControl->Checked == true")
	{
		cbEnablePSUControl->Caption = "Enable PSU ON/OFF";
	}
	else
	{
		cbEnablePSUControl->Caption = "NO";
	} //End If

*/
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::tmInit_SMTimer(TObject *Sender)
{

//	TestLogAdd("tmInit_SMTimer Timer Fired****");

  switch (Tx_Init_State)
  {
	case Tx_Init_Idle: //nothing to do
		tmInit_SM->Enabled = false;
	  break;

//	case Tx_Init_Version: //Ask for Version
//		Waiting_On_Version = true;
//		btVersionClick(this);
//		Tx_Init_State = Tx_Init_Version_Wait;
//	  break;
//
//	case Tx_Init_Version_Wait: //waiting for Version
//		if (Waiting_On_Version == false)
//			{
//			Tx_Init_State = Tx_Init_Manual_Mode;
//			}
//	  break;
//
	case Tx_Init_Manual_Mode: //Set Run Mode
		Waiting_On_Manual_Mode = true;
		cbManual_ModeClick(this);
		Tx_Init_State = Tx_Init_Manual_Mode_Wait;
	  break;

	case Tx_Init_Manual_Mode_Wait: //waiting for Run mode
		if (Waiting_On_Manual_Mode == false)
			{
			Tx_Init_State = Tx_Init_Q_Message_Enable;
			}
	  break;

	case 	Tx_Init_Q_Message_Enable: //Set Run Mode
		Waiting_On_Q_Message_Enable = true;
		Update_Q_Message_Config();
		Tx_Init_State = Tx_Init_Q_Message_Enable_Wait;
	  break;

	case Tx_Init_Q_Message_Enable_Wait: //waiting for Run mode
		if (Waiting_On_Q_Message_Enable == false)
			{

			  //28/7/15

if (cbManual_Mode->Checked)
				{
				Tx_Init_State = Tx_Init_Start_Scan_Power;
				}
			else
				{
				Tx_Init_State = Tx_Init_Idle;
				}
			}
	  break;

	case Tx_Init_Start_Scan_Power: //Start a scan and power
		Waiting_On_Scan_n_Power = true;
   //28/7/15
   Start_Scan_n_Power();
		Tx_Init_State = Tx_Init_Start_Scan_Power_Wait;
	  break;

	case Tx_Init_Start_Scan_Power_Wait: //waiting for Scan and Power
		if (Waiting_On_Scan_n_Power == false)
			{
			Tx_Init_State = Tx_Init_Idle;
			}
	  break;

  }
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Reset_GUI(void )
{

	TXCommMemo->Clear();
	//Log_Coms("Tx Reset");

	Tx_Init_State = Tx_Init_Manual_Mode;
	tmInit_SM->Enabled = true;

 }

//---------------------------------------------------------------------------

void __fastcall TmainForm::Update_Q_Message_Config(void)
{
	AnsiString sPeram;
uint32_t Q_Message_BM = 0x00;

//if (coilViewer->cb_Coil_Info_Enable->Checked)
	{
	Q_Message_BM |= Q_Message_BM_All_Detection;
	}

//if (qiViewer->cb_Qi_Message_Enable->Checked)
	{
	Q_Message_BM |= Q_Message_BM_Decoded_Qi_Messages;
	}


	sPeram = "P01" + IntToHex((int)Q_Message_BM,8) + IntToHex(0x00,8);
	Transmit_Message( sPeram.c_str( ) );
}

//---------------------------------------------------------------------------

void __fastcall TmainForm::Transmit_Message_N( char * MesPtr, unsigned int Len )
	{
	unsigned int TiPtr = 0;
	char LRC = 0;

	TxBuffer[ TiPtr++ ] = STX;

	if ( ( *MesPtr & 0x20 ) == 0 ) // (* Originator message *)
		{
		TxSeqNum++ ;
		if ( TxSeqNum > '9' )
			{
			TxSeqNum = '1';
			}

		// if (Set_Seq_Zero)
		// {
		// TxSeqNum = '0';
		// Set_Seq_Zero = false;
		// }

		TxBuffer[ TiPtr++ ] = TxSeqNum;
		LRC += TxSeqNum;

		Tx_State = SENDING_DATA;
		NackCount = 0;
		}
	else // (* Response message *)
		{
		TxBuffer[ TiPtr++ ] = RxSeqNum;
		LRC += RxSeqNum;
		Tx_State = SENDING_RESPONSE;
		}

	TxBuffer[ TiPtr++ ] = *MesPtr; // Header
	LRC += *MesPtr;
	Len-- ;
	MesPtr++ ;

	while ( Len != 0 )
		{
		if ( ( *MesPtr == DLE ) || ( *MesPtr == STX ) || ( *MesPtr == ETX ) )
			{
			TxBuffer[ TiPtr++ ] = DLE;
			LRC += DLE;
			}
		TxBuffer[ TiPtr++ ] = *MesPtr;
		LRC += *MesPtr;
		MesPtr++ ;
		Len-- ;
		}

	LRC = ( ( ( LRC + ( ( LRC & 0xC0 ) >> 6 ) ) & 0x3F ) + 0x20 );
	TxBuffer[ TiPtr++ ] = LRC;
	TxBuffer[ TiPtr++ ] = ETX;
	TxBuffer[ TiPtr ] = 0x00;

	Binary_Len = TiPtr;
	StartTransmitMessage( );
	}

// ---------------------------------------------------------------------------------

void __fastcall TmainForm::Transmit_Message( char * MesPtr )
	/* This procedure is called with a pointer to a null terminated message . The message should include the header */
	{
	Transmit_Message_N( MesPtr, StrLen( MesPtr ) );
	}

// ---------------------------------------------------------------------------------

void __fastcall TmainForm::StartTransmitMessage( void )
	{
	char Temp[ 30 ];

	if ( ( Tx_State == SENDING_RESPONSE ) || ( Tx_State == SENDING_DATA ) )
		{
		sdWriteSerial( TXComPort, TxBuffer, Binary_Len );
		}

	TXCommMemo->Lines->Add( TxBuffer );
	logit("Log.txt","Txcoms--" + AnsiString(TxBuffer));


	if ( Tx_State == SENDING_RESPONSE )
		{
		Tx_State = TX_BUFF_EMPTY;
		}
	else
		if ( Tx_State == SENDING_DATA )
			{
			tmAcknowledge->Enabled = false;
			tmAcknowledge->Enabled = true;
			Tx_State = WAITING_FOR_ACK;
			}

	}

// ---------------------------------------------------------------------------------

void __fastcall TmainForm::tmAcknowledgeTimer(TObject *Sender)
{
//	TestLogAdd("tmAcknowledgeTimer Timer Fired****");

		if ( Tx_State == WAITING_FOR_ACK )
		{
		NackCount++ ;
		Tx_State = SENDING_DATA;
		StartTransmitMessage( );
		}

	if ( NackCount > 5 )
		{
		// off line code here.
		NackCount = 0;
		Tx_State = TX_BUFF_EMPTY;
		Tx_Init_State = Tx_Init_Idle;
		ShowMessage("Tx is Offline");
			//todo  clear dispaly.
		}
}

//---------------------------------------------------------------------------
//	AnsiString sPeram;
//
//	if ( cbManual_Mode->Checked )
//		{
//		sPeram = "N0100000001";
//		btScan_Board->Enabled = true;
//		bt_Scan_n_Power->Enabled = true;
//		btPower_Selected->Enabled = true;
//		btTurn_Off_Coils->Enabled = true;
//
//		Enable_Coil_Picker( );
//		}
//	else
//		{
//		sPeram = "N0100000000";
//		btScan_Board->Enabled = false;
//		bt_Scan_n_Power->Enabled = false;
//		btPower_Selected->Enabled = false;
//		btTurn_Off_Coils->Enabled = false;
//		Disable_Coil_Picker( );
//
//		}
//
//	Transmit_Message( sPeram.c_str( ) );
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

void __fastcall TmainForm::cbManual_ModeClick(TObject *Sender)
{
	AnsiString sPeram;

	if ( cbManual_Mode->Checked )
		{
		sPeram = "N0100000001";
		}
	else
		{
		sPeram = "N0100000000";
		}

	Transmit_Message( sPeram.c_str( ) );
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::memoPlotterLogChange(TObject *Sender)
{
if (memoPlotterLog->Lines->Count)
	{
	logit("Log.txt",memoPlotterLog->Lines->Strings[memoPlotterLog->Lines->Count -1]);
	}
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::memoUSBLogChange(TObject *Sender)
{
if (memoUSBLog->Lines->Count)
	{
	logit("Log.txt",memoUSBLog->Lines->Strings[memoUSBLog->Lines->Count -1]);
	}
}
//---------------------------------------------------------------------------


void __fastcall TmainForm::btnPlotComportOpenClick(TObject *Sender)
{
//USBMM->DMM_Task();

	PlotterComPort = UserSelectComport("Select Plotter Com-Port for Plotter " + IntToStr((int)Plotter_Number) , "PortPlotter", 57600);

	// if com-port could not be opened close application
	if(  PlotterComPort == NULL)
	{
		shPlotterConnected->Brush->Color = clSilver;
		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
	} else
	{
		sdFlushPort(PlotterComPort);
		PlotterComPort_Rx_Thread = new Rx_Thread( PlotterComPort, RX_NODE_PLOTTER_COMPORT );
		shPlotterConnected->Brush->Color = clLime;
	}  // End If

   //	shPlotterConnected->Brush->Color = (Plotter->UpdateConnectionState()) ? clLime :clSilver;

	//initialize plotter interface
	Plotter = new XYPlotter(PlotterComPort, memoPlotterLog,tmPlotter_NRT);

	mainForm->Caption = mainForm->Caption + " Plotter " + IntToStr((int)Plotter_Number)  ;


	//set buttons
	btnPlotComportOpen->Enabled = false;
	btnPlotComportClose->Enabled = true;

	btnManGoToTestStart->Enabled = true;
	btnHomePlotterSet->Enabled = true;
	btHomePlotter->Enabled = true;

//	btGotoXY->Enabled = true;
	btnGoToOffset->Enabled = true;
	btnManGoToTestStart->Enabled = true;

	btEnablePSU->Enabled = true;

	rgLoad->Enabled = true;

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::rgLoadClick(TObject *Sender)
{
 Reg_WriteInteger(sREG, "Load", "Radio_Index",rgLoad->ItemIndex );

	//set the load
	// turn off all load relays
	Plotter->enLoadRelay(false,0);
	Plotter->enLoadRelay(false,1);
	Plotter->enLoadRelay(false,2);

	// Turn ON selected Load
	Plotter->enLoadRelay(true,rgLoad->ItemIndex);

	switch (rgLoad->ItemIndex)
	{
		case 0:
			lblRxLoadName->Caption = "3.6W";
			break;
		case 1:
			lblRxLoadName->Caption = "5W";
			break;
		case 2:
			lblRxLoadName->Caption = "7.5W";
			break;
	default:
			lblRxLoadName->Caption = "??W";
		break;
	}


	//set the relay state to measure TX / RX
	Plotter->enRefRelay(true);

}
//---------------------------------------------------------------------------


void __fastcall TmainForm::tmPlotter_NRTTimer(TObject *Sender)
{
memoPlotterLog->Lines->Add("NRT");
tmPlotter_NRT->Enabled = false;

if (testAdmin.NRT_Goto)
	{
	testAdmin.testState = testAdmin.NRTState;
	}
testAdmin.NRT_Goto = false;

}

//28/7/15
//---------------------------------------------------------------------------
void __fastcall TmainForm::Start_Scan_n_Power(void)
	{
	AnsiString sPeram;
	sPeram = "K";

	Transmit_Message( sPeram.c_str( ) );

	}
//---------------------------------------------------------------------------





//---------------------------------------------------------------------------

void __fastcall TmainForm::TabControl1Change(TObject *Sender)
{

	//ShowMessage("tab");

	char buf[3];

	if (TabControl1->TabIndex == -1)
	{
//		ShowMessage("tab1");
	}
	else
	{
//        StatusBar1->SimpleText = "Tab index: " +
  //      String(itoa(TabControl1->TabIndex, buf, 10));

		switch (TabControl1->TabIndex)
		{
			case 0:
			   //		ShowMessage("tab0");
			  //	Panel0->Visible = true;
				ShowTabPanel(0);
				//cbIgnoreMessages->Visible = false;
				break;
			case 1:
		//		ShowMessage("tab1");
				ShowTabPanel(1);
				//cbIgnoreMessages->Visible = true;
				break;
			case 2:
		//		ShowMessage("tab2");
				ShowTabPanel(2);
				//bIgnoreMessages->Visible = true;
				break;
			case 3:
		//				ShowMessage("tab3");
				ShowTabPanel(3);
				break;
			case 4:
		//				ShowMessage("tab4");
				break;
			default:
				break;
		} // End Switch
	}  // End If


}  // End Procedure
//---------------------------------------------------------------------------
void __fastcall TmainForm::ShowTabPanel(int PanelNumb)
{
	switch (PanelNumb)
		{
			case 0:
				Panel2->Visible = true;
				Panel3->Visible = false;
				Panel4->Visible = false;
				Panel5->Visible = false;
				break;
			case 1:
				Panel2->Visible = false;
				Panel3->Visible = true;
				Panel4->Visible = false;
				Panel5->Visible = false;
				break;
			case 2:
				Panel2->Visible = false;
				Panel3->Visible = false;
				Panel4->Visible = true;
				Panel5->Visible = false;
				break;
			case 3:
				Panel2->Visible = false;
				Panel3->Visible = false;
				Panel4->Visible = false;
				Panel5->Visible = true;
				break;
			default:
				break;
		}

}
//---------------------------------------------------------------------------
void __fastcall TmainForm::About1Click(TObject *Sender)
{
	ShowMessage("Plotter developed for 2D Plotter but is Generic for other use");
}
//---------------------------------------------------------------------------

/*
void __fastcall TmainForm::dsgsgs1Click(TObject *Sender)
{
	PlotterComPort = UserSelectComport("Select Plotter Com-Port for Plotter " + IntToStr((int)Plotter_Number) , "PortPlotter", 57600);

	// if com-port could not be opened close application
	if(  PlotterComPort == NULL) {
		MessageDlg("ComPort Error", mtError, TMsgDlgButtons() << mbOK, 0);
	} else {
		sdFlushPort(PlotterComPort);
		PlotterComPort_Rx_Thread = new Rx_Thread( PlotterComPort, RX_NODE_PLOTTER_COMPORT );
	}


}

*/
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnPlotComportCloseClick(TObject *Sender)
{

	//stop timer
	sdFlushPort(PlotterComPort);
	sdClosePort(PlotterComPort);

	PlotterComPort_Rx_Thread->FreeOnTerminate = false;
	PlotterComPort_Rx_Thread->Terminate();
	PlotterComPort_Rx_Thread->WaitFor() ;

	delete PlotterComPort_Rx_Thread;

	//set buttons
	btnPlotComportOpen->Enabled = true;
	btnPlotComportClose->Enabled = false;
	btnManGoToTestStart->Enabled = false;
	btnHomePlotterSet->Enabled = false;

	btHomePlotter->Enabled = false;

  //	btGotoXY->Enabled = false;
   //	btnGoToOffset->enabled = false;
	btnGoToOffset->Enabled = false;
	btnManGoToTestStart->Enabled = false;

	btEnablePSU->Enabled = false;

	rgLoad->Enabled = false;

	shPlotterConnected->Brush->Color = clSilver;

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnProgramCloseClick(TObject *Sender)
{
   //Run Shutdown Sequence here

	//close PSU comport
//	sdFlushPort(PSUComPort);
//	sdClosePort(PSUComPort);
	//close Plotter comport
//	sdFlushPort(PlotterComPort);
//	sdClosePort(PlotterComPort);
	//close TX comport if opened
//	if(TXComPort != NULL){
//		sdFlushPort(TXComPort);
//		sdClosePort(TXComPort);
//	}

	USBMM->Stop();
	tmTest->Enabled = false;
	mainForm->Close();
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::btnManGoToTestStartClick(TObject *Sender)
{

	XYPlotter_setpoint_t tmpSetpoint;
//	tmpSetpoint.XPos = StrToInt(ebXOffset->Text);
//	tmpSetpoint.YPos = StrToInt(ebYOffset->Text);
	tmpSetpoint.XPos = StrToInt(ebXSetpointStart->Text); // - StrToInt(ebXOffset->Text);
	tmpSetpoint.YPos = StrToInt(ebYSetpointStart->Text); // - StrToInt(ebYOffset->Text);
//	tmpSetpoint.XPos = 0;
//	tmpSetpoint.YPos = 0;

	//update plotter setpoint
	Plotter->UpdateSetpoint(tmpSetpoint);
	//Update user labels
	updatePosLabels(this);

}
//---------------------------------------------------------------------------


void __fastcall TmainForm::BitBtn1Click(TObject *Sender)
{
int ColMax = 30;
int RowMax = 30;

	//***
	// Set Grid Limits
	//***
	StringGrid1->ColCount = ColMax + 1;
	StringGrid1->RowCount = RowMax + 1;

	StringGrid1->Rows[RowMax]->Clear(); //= StringGrid1->Rows[row - 1];

	//***
	// Column Width
	//***
	StringGrid1->DefaultColWidth = 100;
	for (int i = 0; i < ColMax + 1; i++)
	{
		StringGrid1->ColWidths[i]  = 32;
	} // End for
	//***
	// End Column Width
	//***


	for (int iRow = 0; iRow < 31; iRow++)
	{
		for (int iCol = 0; iCol < 31; iCol++)
		{

		}  // End For
	}  // End For


	// Number Title Columns
	for (int iCol = 1; iCol < ColMax + 1; iCol++)
	{
		StringGrid1->Cells[iCol][0] = iCol;
	}  // End For

	// Number Row Titles
	for (int iRow = 0; iRow < RowMax + 1; iRow++)
	{
		StringGrid1->Cells[0][iRow] = 30 - iRow;
	}  // End For

	StringGrid1->Cells[3][3] = 65.2;

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::StringGrid1Click(TObject *Sender)
{


	SelGridRowNumber = StringGrid1->RowCount - StringGrid1->Row;
	SelGridColNumber = StringGrid1->Col; // StringGrid1->ColCount - StringGrid1->Col;

//	if (TestStarted || SelectedGrid)
	if (TestStarted)
	{
	   // Do Nothing
//	   ShowMessage("Running Repeat test - Please Stop if finished");

	}
	else
	{

		if (MessageDlg("Please Enter Option: Yes = Move Plotter to Selected Cell, No = Exit"  , mtInformation, mbYesNo, 0) == mrYes)
		{
			//	ShowMessage("Yes");
			//	ebStepSize->Text;

			SelectedGrid = true;

//			StringGrid1->Enabled = false;

			XYPlotter_setpoint_t tmpSetpoint;
			//	tmpSetpoint.XPos = StrToInt(ebXOffset->Text);
			//	tmpSetpoint.YPos = StrToInt(ebYOffset->Text);

			tmpSetpoint.XPos = ((SelGridColNumber - 1) *  StrToInt(ebStepSize->Text)) + StrToInt(ebXSetpointStart->Text);
			tmpSetpoint.YPos = ((SelGridRowNumber - 1)) * StrToInt(ebStepSize->Text) + StrToInt(ebYSetpointStart->Text);

			lbCords->Caption = "X: " + String(tmpSetpoint.XPos) + " Y: " + String(tmpSetpoint.YPos);

//			Series1->Add()
	  //		ShowMessage("NOTE: Refer to 'Set Coordinates' for mm X-Y location");
	//		lbDistance->Caption = "X: " + String(curDistance.XPos) + " Y: " + String(curDistance.YPo

			if (cbSimPlotting->Checked)
			{
				// Do Nothing
			}
			else
			{
				//update plotter setpoint
				Plotter->UpdateSetpoint(tmpSetpoint);
				//Update user labels
				updatePosLabels(this);
			}  // End If
		}
		else
		{
			// 	ShowMessage("No");
			SelectedGrid = false;
		} // End If

	} // End If

}
//---------------------------------------------------------------------------


void __fastcall TmainForm::ebXSetpointStartChange(TObject *Sender)
{
//	Reg_WriteString(sREG, "Cords", "X_Offset", ebXOffset->Text);
//	TestLogAdd("New [X Offset] : " + ebXOffset->Text);

	Reg_WriteString(sREG, "Cords", "X_SetPoint", ebXSetpointStart->Text);
	TestLogAdd("New [X SetPoint] : " + ebXSetpointStart->Text);

	//update plotter settings
	XYPlotter_setpoint_t tmpOffset;
	tmpOffset.XPos=StrToInt(ebXSetpointStart->Text);
	tmpOffset.YPos=StrToInt(ebYSetpointStart->Text);
	//Plotter->SetPlotStart(tmpOffset);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::ebYSetpointStartChange(TObject *Sender)
{
//	Reg_WriteString(sREG, "Cords", "Y_Offset", ebYOffset->Text);
//	TestLogAdd("New [Y Offset] : " + ebYOffset->Text);

	Reg_WriteString(sREG, "Cords", "Y_SetPoint", ebYSetpointStart->Text);
	TestLogAdd("New [Y SetPoint] : " + ebYSetpointStart->Text);

	//update plotter settings
	XYPlotter_setpoint_t tmpOffset;
	tmpOffset.XPos=StrToInt(ebXSetpointStart->Text);
	tmpOffset.YPos=StrToInt(ebYSetpointStart->Text);
	//Plotter->SetPlotStart(tmpOffset);
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::cbShowProgChartClick(TObject *Sender)
{
	if (cbShowProgChart->Checked)
	{
		StringGrid1->Height = 234;
		Chart1->Visible = true;
	}
	else
	{
		Chart1->Visible = false;
		StringGrid1->Height = 391;
	} // End If
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Button1Click(TObject *Sender)
{
	Series1->Clear();
}
//---------------------------------------------------------------------------

void __fastcall TmainForm::cbPowerSupplyStatusClick(TObject *Sender)
{

	if (cbPowerSupplyStatus->Checked)
	{
		btEnablePSUClick(this);
	}
	else
	{
		btDisablePSUClick(this);

	} // End If

}
//---------------------------------------------------------------------------

void __fastcall TmainForm::Plotter_ComportTriggerAvail(TObject *CP, WORD Count)
{
char Temp;

	while(Count--)
	{

//	Temp = Plotter_Comport->GetChar();

	if (Plotter != NULL)
		{
//		Plotter->ProcessRXData(1,&Temp);
		}
     }

}
//---------------------------------------------------------------------------

