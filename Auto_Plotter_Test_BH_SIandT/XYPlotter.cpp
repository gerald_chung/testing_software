/*

   [Home]

	3D Plotter commands
	G1 X0 Y0 Z0 F3000


*/


//---------------------------------------------------------------------------

#pragma hdrstop

#include "XYPlotter.h"

#define STX (0x02)
#define ETX (0x03)

#define CR (0x0D)
#define LF (0x0A)

// ***
// Global Vars
// ***
//XYPlotter_setpoint_t offset;
bool Plotter2DElse3D;
float ZmmHeight;

//---------------------------------------------------------------------------
// Add a line to the plotter log...
//---------------------------------------------------------------------------
void __fastcall XYPlotter::SetPlotterType2D(bool Set2D)
{

	 if (Set2D)
	 {
//		 ShowMessage("Set 2D Plotter");
		 Plotter2DElse3D = true;
	 }
	 else
	 {
//		 ShowMessage("Set 3D Plotter");
		 Plotter2DElse3D = false;
	 } // End If
}

//---------------------------------------------------------------------------
// Add a line to the plotter log...
//---------------------------------------------------------------------------
void __fastcall XYPlotter::AddToLog(AnsiString line)
{
	Memo->Lines->Add(line);

}

//---------------------------------------------------------------------------
// Check if the current setpoint has been reached
//---------------------------------------------------------------------------
bool __fastcall XYPlotter::SetpointReached()
{
	bool result = false;

  if (Plotter2DElse3D)
  {
	if(distance.XPos == 0 && distance.YPos == 0){
	   AddToLog("Setpoint Reached");
	   AddToLog("---------------");
	   result = true;
	}
	return result;
  }
  else
  {
	// 3D
//	Sleep(3000);
	result = true;
	return result;
  } // End If

}

// ---------------------------------------------------------------------------
// Takes a reply and decodes it
// ---------------------------------------------------------------------------
void __fastcall XYPlotter::ProcessReply(uint8_t *buff, uint32_t len)
{

  if (Plotter2DElse3D)
  {
	// if the length of the packet is valid, process it
	if (len == GetMessageLength(buff[0])) {
		// the start of the buffer should contain the command header
		switch (buff[0]) {

		//received distance update
		case XYPLOTTER_REPLY_DIST:
			//update from plotter on current position
			if( xorCheckSum(&buff[0],GetMessageLength(buff[0])-2) == (uint8_t)HexStrToUint((char *)&buff[len-2],2,false) )
			{
				if(!waitingOnSetpointReply){
					distance.XPos = (int16_t)HexStrToUint((char *)&buff[1],4,false)/STEPS_PER_MILLIMETER;
					distance.YPos = (int16_t)HexStrToUint((char *)&buff[5],4,false)/STEPS_PER_MILLIMETER;
				}
			} else
			{   //inform user checksum was incorrect
				AddToLog("Checksum Invalid : " + ByteArrayToStr(buff,GetMessageLength(buff[0])) );
			}
			break;

		//received zeroing operation complete
		case XYPLOTTER_REPLY_ZERO:
			//plotter reached zero point
			AddToLog("Homing Complete..");
			break;

		//received setpoint received update (ok or err)
		case XYPLOTTER_REPLY_SETPOINT_OK:
			//setpoint command was properly received
			curSetpoint = newSetpoint;
			waitingOnSetpointReply = false;
			AddToLog("Setpoint CMD Received OK");
			break;
		case XYPLOTTER_REPLY_SETPOINT_ERR:
			//setpoint command was not received properly
			AddToLog("Setpoint CMD ERR, retrying..");
			//resend current setpoint
			UpdateSetpoint(curSetpoint);
			break;

		//received test connection reply
		case XYPLOTTER_REPLY_TEST_COMM:
			portConnected = true;
			portTestCallsPending = 0;
			break;

		case XYPLOTTER_REPLY_SET_RELAY_OK:
		case XYPLOTTER_REPLY_SET_RELAY_ERR:
			//regardless of succes, this messag contains data on relay state
			if( xorCheckSum(&buff[0],GetMessageLength(buff[0])-2) == (uint8_t)HexStrToUint((char *)&buff[len-2],2,false) )
			{
				relayState = (HexStrToUint((char *)&buff[1],2,false) == 0x00)? false : true;
				AddToLog("Relay State [" + String(HexStrToUint((char *)&buff[1],2,false)) + "]");
			} else {
				AddToLog("Can't Decode Relay State");
			}
			break;
		default:
			AddToLog("Invalid Reply : " + IntToHex(buff[0],2) );
		}
	}
	else
	{
		AddToLog("Invalid Length " + String(len) + " for " + IntToHex(buff[0],2) );
		char tmpStr[MAX_REPLY_LENGTH+1];
		sprintf(&tmpStr[0],"%14s", (char *)&buff[0]);
		AddToLog(tmpStr);
	}
  }
  else
  {
	// 3D plotter


  }  // End If

}

// ---------------------------------------------------------------------------
// Check if the byte is a CMD byte
// ---------------------------------------------------------------------------
bool __fastcall XYPlotter::UpdateConnectionState(void)
{

  if (Plotter2DElse3D)
  {

	//send test connection request
//	sdWriteSerialCh(ComHandle,STX);
//	sdWriteSerialCh(ComHandle,(char)XYPLOTTER_CMD_TEST_COMM);
//	sdWriteSerialCh(ComHandle,ETX);
	ComHandle->PutChar(STX);
	ComHandle->PutChar((char)XYPLOTTER_CMD_TEST_COMM);
	ComHandle->PutChar(ETX);


	//update pending call count and check if still connected
	portTestCallsPending++;
	portConnected = (portTestCallsPending > MAX_COMM_TESTS_BEFORE_DISCONNECT) ? false : true;
	//return current state
	return portConnected;
  }
  else
  {
	// 3D Plotter
	return portConnected;
  }  // End If
}

// ---------------------------------------------------------------------------
// Check if the byte is a CMD byte
// ---------------------------------------------------------------------------
bool __fastcall XYPlotter::IsMessageHeader(uint8_t byte)
{
	bool result = false;

  if (Plotter2DElse3D)
  {

	// all the characters that are not in 0123456789ABCDEF are assumed to be a message header CMD
	result = (byte < (uint8_t)'0') ? true : result;
	result = ((byte > (uint8_t)'9') && (byte < (uint8_t)'A')) ? true : result;
	result = (byte > (uint8_t)'F') ? true : result;
	// return result
	return result;
  }
  else
  {
	// 3d Plot
	result = true;
	return result;

  } // End If
}

//---------------------------------------------------------------------------
// Check if the byte is a CMD byte
//---------------------------------------------------------------------------
uint32_t __fastcall XYPlotter::GetMessageLength(uint8_t header)
{
uint8_t len = 0;

  if (Plotter2DElse3D)
  {
	switch(header)
	{
		case XYPLOTTER_NO_CMD:
			len = 0;
			break;
		case XYPLOTTER_CMD_GOTOXY:
			len = XYPLOTTER_CMD_GOTOXY_LEN;
			break;
		case XYPLOTTER_CMD_GETDIST:
			len = XYPLOTTER_CMD_GETDIST_LEN;
			break;
		case XYPLOTTER_CMD_ZERO:
			len = XYPLOTTER_CMD_ZERO_LEN;
			break;
				case XYPLOTTER_CMD_TEST_COMM:
						len = XYPLOTTER_CMD_TEST_COMM_LEN;
						break;
		case XYPLOTTER_REPLY_DIST:
			len = XYPLOTTER_REPLY_DIST_LEN;
			break;
		case XYPLOTTER_REPLY_ZERO:
			len = XYPLOTTER_REPLY_ZERO_LEN;
			break;
		case XYPLOTTER_REPLY_SETPOINT_OK:
		case XYPLOTTER_REPLY_SETPOINT_ERR:
			len = XYPLOTTER_REPLY_SETPOINT_LEN;
			break;
		case XYPLOTTER_REPLY_TEST_COMM:
			len = XYPLOTTER_REPLY_TEST_COMM_LEN;
			break;
		case XYPLOTTER_CMD_SET_RELAY:
			len = XYPLOTTER_CMD_SET_RELAY_LEN;
			break;
		case XYPLOTTER_REPLY_SET_RELAY_OK:
		case XYPLOTTER_REPLY_SET_RELAY_ERR:
			len = XYPLOTTER_REPLY_SET_RELAY_LEN;
			break;
		default :
			len = 0;
	}
	//return result
	return len;
  }
  else
  {
	// 3d plot
	len = 0;
	return len;
  }  // End If

}

// ---------------------------------------------------------------------------
// Send a relay command to the plotter, based on the bool intput
// ---------------------------------------------------------------------------
void __fastcall XYPlotter::enRefRelay(bool state) //true = on
{

  if (Plotter2DElse3D)
  {
	//Z01B5 = ON, Z01B5 = OFF
	char cmdBuff[5];
	cmdBuff[0] = (char)XYPLOTTER_CMD_SET_RELAY;
	cmdBuff[1] = (state)? '1' : '0';
	cmdBuff[2] = XYPLOTTER_REF_RELAY;
	IntToHexByteBuff(xorCheckSum(&cmdBuff[0], 3),&cmdBuff[3], 2);

//	sdWriteSerialCh(ComHandle,STX);
//	sdWriteSerialCh(ComHandle,cmdBuff[0]);
//	sdWriteSerialCh(ComHandle,cmdBuff[1]);
//	sdWriteSerialCh(ComHandle,cmdBuff[2]);
//	sdWriteSerialCh(ComHandle,cmdBuff[3]);
//	sdWriteSerialCh(ComHandle,cmdBuff[4]);
//	sdWriteSerialCh(ComHandle,ETX);

	ComHandle->PutChar(STX);
	ComHandle->PutChar(cmdBuff[1]);
	ComHandle->PutChar(cmdBuff[2]);
	ComHandle->PutChar(cmdBuff[3]);
	ComHandle->PutChar(cmdBuff[4]);
	ComHandle->PutChar(ETX);

  }
  else
  {
	// 3d Plot
	// Do Nothing. Code is put in Main Unit for 3D as not part of Plotter

  }  // End If

}


// ---------------------------------------------------------------------------
// Send a relay command to the plotter, based on the bool intput
// To power the PSU
// ---------------------------------------------------------------------------
void __fastcall XYPlotter::enPowerRelay(bool state) //true = on
{
  if (Plotter2DElse3D)
  {

	char cmdBuff[5];
	cmdBuff[0] = (char)XYPLOTTER_CMD_SET_RELAY;
	cmdBuff[1] = (state)? '1' : '0';
	cmdBuff[2] = XYPLOTTER_POWER_RELAY;
	IntToHexByteBuff(xorCheckSum(&cmdBuff[0], 3),&cmdBuff[3], 2);

/*	sdWriteSerialCh(ComHandle,STX);
	sdWriteSerialCh(ComHandle,cmdBuff[0]);
	sdWriteSerialCh(ComHandle,cmdBuff[1]);
	sdWriteSerialCh(ComHandle,cmdBuff[2]);
	sdWriteSerialCh(ComHandle,cmdBuff[3]);
	sdWriteSerialCh(ComHandle,cmdBuff[4]);
	sdWriteSerialCh(ComHandle,ETX);
*/
	ComHandle->PutChar(STX);
	ComHandle->PutChar(cmdBuff[0]);
	ComHandle->PutChar(cmdBuff[1]);
	ComHandle->PutChar(cmdBuff[2]);
	ComHandle->PutChar(cmdBuff[3]);
	ComHandle->PutChar(cmdBuff[4]);
	ComHandle->PutChar(ETX);

  }
  else
  {
  // 3d plot


  }  // End If

}

// ---------------------------------------------------------------------------
// Send a relay command to the plotter
// Used to switch the LoadRelay
// 00 == load one;
// 01 == load two;
// 02 == load three;
// ---------------------------------------------------------------------------
void __fastcall XYPlotter::enLoadRelay(bool state,int iLoad)
{

  if (Plotter2DElse3D)
  {
	char cmdBuff[5];
	cmdBuff[0] = (char)XYPLOTTER_CMD_SET_RELAY;
	cmdBuff[1] = (state)? '1' : '0';

	switch (iLoad)
		{
			case 0x00:
			 cmdBuff[2] = XYPLOTTER_6R8_LOAD_RELAY;
			break;

			case 0x01:
			 cmdBuff[2] = XYPLOTTER_5R0_LOAD_RELAY;
			break;

			case 0x02:
			 cmdBuff[2] = XYPLOTTER_3R3_LOAD_RELAY;
			break;

			default:
				  //Opps this is an error. Do nothing.
				  return;

		}

	IntToHexByteBuff(xorCheckSum(&cmdBuff[0], 3),&cmdBuff[3], 2);
/*
	sdWriteSerialCh(ComHandle,STX);
	sdWriteSerialCh(ComHandle,cmdBuff[0]);
	sdWriteSerialCh(ComHandle,cmdBuff[1]);
	sdWriteSerialCh(ComHandle,cmdBuff[2]);
	sdWriteSerialCh(ComHandle,cmdBuff[3]);
	sdWriteSerialCh(ComHandle,cmdBuff[4]);
	sdWriteSerialCh(ComHandle,ETX);
*/
	ComHandle->PutChar(STX);
	ComHandle->PutChar(cmdBuff[0]);
	ComHandle->PutChar(cmdBuff[1]);
	ComHandle->PutChar(cmdBuff[2]);
	ComHandle->PutChar(cmdBuff[3]);
	ComHandle->PutChar(cmdBuff[4]);
	ComHandle->PutChar(ETX);

 }
 else
 {
	 // 3d plot


 } // End If

}


// ---------------------------------------------------------------------------
// Check RX buffer, and process its data
// Pseudo code:
// Read avaialalbe RX bytes to a maximum
// If a full command is received, process it
// If a new header is received, process any waiting cmd, and wait for this to complete
// If a data byte is received store it until a command is complete.
// ---------------------------------------------------------------------------
//void __fastcall XYPlotter::Process_Message(int Count, uint8_t * pData)
//{
//
//	static  uint8_t rxBuff[MAX_REPLY_LENGTH];
//	static	uint32_t rxPos = 0;
////	static	uint8_t byteBuff[MAX_BYTES_TO_PROCESS];
//	uint8_t curByte;
//	//read a maximum nr of MAX_BYTES_TO_PROCESS bytes.
// //	uint32_t nrBytes = sdReadSerial(ComHandle, (char *)&byteBuff[0], MAX_BYTES_TO_PROCESS);
//
//
//	//process each byte
//	for (int i=0; i < Count; i++) {
//		//get current byte to process
//		curByte = pData[i];
//		//if this is a new header, and a cmd is waiting, process waiting cmd, and start anew
//		if( IsMessageHeader(curByte) && rxPos != 0)
//		{
//			ProcessReply(&rxBuff[0],rxPos);
//			//if length for new header is only 1, process immediately
//			if( GetMessageLength(curByte) == 1 )
//			{
//				ProcessReply(&curByte,1);
//				rxPos = 0;
//			//if length is longer, start building new command
//			} else {
//				rxBuff[0] = curByte;
//				rxPos = 1;
//			}
//		//if this is part of an incoming command, add to buffer and process
//		} else if (rxPos != 0) {
//			//add to buffer
//			rxBuff[rxPos]=curByte;
//			rxPos++;
//			//if length of current command has been reached, process it
//			if( rxPos == GetMessageLength(rxBuff[0]) )
//			{
//				ProcessReply(&rxBuff[0],rxPos);
//				rxPos = 0;
//			}
//		//if this is received on an empty buffer, should be header, store/process accordingly
//		} else if ( rxPos == 0 && IsMessageHeader(curByte))
//		{   //if the header type has len=1, process imediately, since no more bytes are needed
//			if( GetMessageLength(curByte) == 1 )
//			{
//				ProcessReply(&curByte,1);
//			} else {
//				//add to buffer
//				rxBuff[rxPos]=curByte;
//				rxPos++;
//			}
//		}
//	}
//}


// ---------------------------------------------------------------------------

void __fastcall XYPlotter::ProcessRX_Byte(uint8_t RxByte)
{
  static uint32_t rxPos = 0;
  static uint8_t rxBuff[MAX_REPLY_LENGTH];

  if (Plotter2DElse3D)
  {

	  if (RxByte == STX)
	  {
		rxPos = 0;    // reset buffer index if we get a new meaage
	  }
	  else
	  {
		if (RxByte == ETX)
		{// we have message time to do something with it.

		  if (rxPos != 0)
		  {
			if (GetMessageLength(rxBuff[0]) == rxPos )  /// todo might be len -1
			{ // the length is right
			  ProcessReply(rxBuff, rxPos);
			  tmNRT->Enabled = false;			// got a message.  Stop the main form NRT timer from running
			}// if not right length do nothing
		  }// if buffer empty do nothing
		  rxPos = 0;
		}
		else
		{ //save data
		  if (rxPos < MAX_REPLY_LENGTH-1)
		  {
			rxBuff[rxPos] = RxByte;
			rxPos ++;
		  }
		  //buffer full. Do nothing and wait for STX
		}
	  }

  }
  else
  {
	// 3d Plotter

  } // End If

}

// ---------------------------------------------------------------------------

void __fastcall XYPlotter::ProcessRXData(int Count, uint8_t * pData)
{
  if (Plotter2DElse3D)
  {

	while(Count--)
	{
		ProcessRX_Byte(*pData);
		pData++;
	}

  }
  else
  {
	// 3d Plotter



  }  // End If
}

// ---------------------------------------------------------------------------


//---------------------------------------------------------------------------
//Request position update from the Plotter controller
//---------------------------------------------------------------------------
void __fastcall	XYPlotter::RequestDistance()
{

  if (Plotter2DElse3D)
  {

	//send request distance command
/*	sdWriteSerialCh(ComHandle,STX);
	sdWriteSerialCh(ComHandle,(char)XYPLOTTER_CMD_GETDIST);
	sdWriteSerialCh(ComHandle,ETX);
*/
	ComHandle->PutChar(STX);
	ComHandle->PutChar((char)XYPLOTTER_CMD_GETDIST);
	ComHandle->PutChar(ETX);

  }
  else
  {
	// 3d Plotter

  }

}

//---------------------------------------------------------------------------
//Update the current plotter setpoint
//---------------------------------------------------------------------------
void __fastcall XYPlotter::UpdateSetpoint(XYPlotter_setpoint_t new_setpoint)
{
	 // Build Commands
//	char cmdBuff[20];

	newSetpoint.XPos = new_setpoint.XPos; // + offset.XPos;
	newSetpoint.YPos = new_setpoint.YPos; // + offset.YPos;
	AddToLog("Setting Setpoint [" + String(newSetpoint.XPos) + "," + String(newSetpoint.YPos) + "]");

	// Update initial distance
	distance.XPos = newSetpoint.XPos - curSetpoint.XPos;
	distance.YPos = newSetpoint.YPos - curSetpoint.YPos;

	//build set setpoint command
	uint8_t cmdBuff[XYPLOTTER_CMD_GOTOXY_LEN];
	cmdBuff[0] = XYPLOTTER_CMD_GOTOXY;
	AnsiString logMessage = "Sent [";
	uint8_t i;

	IntToHexByteBuff(newSetpoint.XPos, &cmdBuff[1], 4);
	IntToHexByteBuff(newSetpoint.YPos, &cmdBuff[5], 4);
	IntToHexByteBuff(xorCheckSum(&cmdBuff[0], XYPLOTTER_CMD_GOTOXY_LEN-2),&cmdBuff[9], 2);
	//build log string, and send over comport at the same time

  if (Plotter2DElse3D)
  {

//	sdWriteSerialCh(ComHandle,STX);
	ComHandle->PutChar(STX);
	for(i=0;i<XYPLOTTER_CMD_GOTOXY_LEN; i++)
	{
//		sdWriteSerialCh(ComHandle,(char)cmdBuff[i]);
ComHandle->PutChar((char)cmdBuff[i]);
		logMessage += (char)cmdBuff[i];
	}
//	sdWriteSerialCh(ComHandle,ETX);
ComHandle->PutChar(ETX);

	//inform user of sent command
	AddToLog(logMessage + "]");
	//indicate retreive distance cmd, that new setpoint might not have been taken into account
	waitingOnSetpointReply = true;

  }
  else
  {
	// 3d plot
	AnsiString Xdist = IntToStr(newSetpoint.XPos);
	AnsiString Ydist = IntToStr(newSetpoint.YPos);
	AnsiString ZmmHeightStr = FloatToStr(ZmmHeight);

	ComHandle->PutChar('G');
	ComHandle->PutChar('1');
	ComHandle->PutChar(' ');
	ComHandle->PutChar('X');
//	ComHandle->PutChar('1');
//	ComHandle->PutChar('0');

	for(int i = 1; i < Xdist.Length() + 1; i++)
	{
		ComHandle->PutChar(char(Xdist[i]));
		logMessage += Xdist[i];
	}  // End For
//	ShowMessage(logMessage);

	ComHandle->PutChar(' ');
	ComHandle->PutChar('Y');
//	ComHandle->PutChar('1');
//	ComHandle->PutChar('0');
	for(int i = 1; i < Ydist.Length() + 1; i++)
	{
		ComHandle->PutChar(char(Ydist[i]));
		logMessage += Ydist[i];
	}  // End For

	ComHandle->PutChar(' ');
	ComHandle->PutChar('Z');
//	ComHandle->PutChar('0');
	for(int i = 1; i < ZmmHeightStr.Length() + 1; i++)
	{
		ComHandle->PutChar(char(ZmmHeightStr[i]));
		logMessage += ZmmHeightStr[i];
	}  // End For

	//mainGUI->ed3DZHeight

//	ComHandle->PutChar('0');

	ComHandle->PutChar(' ');
	ComHandle->PutChar('F');
	ComHandle->PutChar('3');
	ComHandle->PutChar('0');
	ComHandle->PutChar('0');
	ComHandle->PutChar('0');

	ComHandle->PutChar(CR);

	//inform user of sent command
	AddToLog(logMessage + "]");
	//indicate retreive distance cmd, that new setpoint might not have been taken into account
//	waitingOnSetpointReply = true;
	Sleep(2000);

  } // End If


}

//---------------------------------------------------------------------------
//Return the current plotter setpoint
//---------------------------------------------------------------------------
XYPlotter_setpoint_t __fastcall XYPlotter::GetSetpoint()
{
	return curSetpoint;
}

//---------------------------------------------------------------------------
//Return the last known position
//---------------------------------------------------------------------------
XYPlotter_setpoint_t __fastcall XYPlotter::GetCurDistance()
{
	return distance;
}

//---------------------------------------------------------------------------
// Instruct plotter to find [0,0] using limit switches
//---------------------------------------------------------------------------
void __fastcall XYPlotter::Home()
{
/*	sdWriteSerialCh(ComHandle,STX);
	sdWriteSerialCh(ComHandle,(char)XYPLOTTER_CMD_ZERO);
	sdWriteSerialCh(ComHandle,ETX);
*/

  if (Plotter2DElse3D)
  {
//	ShowMessage("Send 2d");
	ComHandle->PutChar(STX);
	ComHandle->PutChar((char)XYPLOTTER_CMD_ZERO);
	ComHandle->PutChar(ETX);

  }
  else
  {
//	ShowMessage("Send 3d");
	ComHandle->PutChar('G');
	ComHandle->PutChar('2');
	ComHandle->PutChar('8');
	ComHandle->PutChar(CR);
//	ComHandle->PutChar(LF);

//	ShowMessage("Done");

  } // End If

}

//---------------------------------------------------------------------------
//Update the current plotter setpoint
//---------------------------------------------------------------------------
void __fastcall XYPlotter::SetPlotStart(XYPlotter_setpoint_t new_offset)
{
//	int32_t XPost;
//	int32_t YPost;

//	offset.XPos = new_offset.XPos;
//	offset.YPos = new_offset.YPos;
//	offset.XPos = 12;
//	offset.YPos = 12;

//	AddToLog("New Offset [" + String(offset.XPos) + "," + String(offset.YPos) + "]");
}

//---------------------------------------------------------------------------
// Add a line to the plotter log...
//---------------------------------------------------------------------------
void __fastcall XYPlotter::Set3dZHeight(float ZmmDist)
{
	 if (Plotter2DElse3D)
	 {
//		 ShowMessage("Set 2D Plotter");

	 }
	 else
	 {
//		 ShowMessage("Set 3D Plotter");

		ZmmHeight = ZmmDist;

	 } // End If
}

//---------------------------------------------------------------------------
// Add a line to the plotter log...
//---------------------------------------------------------------------------
void __fastcall XYPlotter::SendCommand(AnsiString Cmd3DPlot)
{
	 if (Plotter2DElse3D)
	 {
		// 2D Plotter
		//		 ShowMessage("Set 2D Plotter");

	 }
	 else
	 {
		// 3D Plotter
		//		 ShowMessage("Set 3D Plotter");

	 //	ComHandle->PutChar(' ');
	//	ComHandle->PutChar('Z');
	//	ComHandle->PutChar('0');

		for(int i = 1; i < Cmd3DPlot.Length() + 1; i++)
		{
			ComHandle->PutChar(char(Cmd3DPlot[i]));
//			logMessage += Cmd3DPlot[i];
		}  // End For

		ComHandle->PutChar(CR);

	 } // End If
}
#pragma package(smart_init)
