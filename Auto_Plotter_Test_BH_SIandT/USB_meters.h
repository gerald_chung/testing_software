//---------------------------------------------------------------------------

#ifndef USB_metersH
#define USB_metersH

#include <vcl.h>
#include <stdio.h>
#include  <io.h>
#include <setupapi.h>
#include "Utils.h"

//definitions
#define NODE_DEVICE_ID  "Vid_1a86&Pid_e008"

//global type definitions
typedef struct {
	double value;
	AnsiString sUnit; //VDC, VAC, ADC, A-AC
	bool NewData;
} DMM_Data;

typedef struct {
	// The Microsoft HID driver expects a prefix report ID byte
	unsigned char reportID;
	unsigned char report[64];
} USBMM_report_t;

typedef struct {
	HANDLE readhandle;
	HANDLE writehandle;
	bool connected;
} USBMM_device_t;

typedef enum {
	USBMM_OK,
	USBMM_HIB_LIB_LOAD_ERROR,
	USBMM_USBMM_CONNECT_ERROR
} USBMM_ret_t;

//microsoft usb HID interface
typedef BOOLEAN(__stdcall * PHidD_SetFeature)(HANDLE, PVOID, ULONG);


//simple wrapper class to allow threaded USB handling
class USBMM_Thread : public TThread {

	 public:

		// Start the thread suspended
		__fastcall USBMM_Thread(USBMM_device_t usbmm_dev, TMutex *MemoMutex, TMemo *USBMemo) : TThread(TRUE)
		{
			device = usbmm_dev;
			FreeOnTerminate = true;
			memoMux = MemoMutex;
			dataMux = new TMutex(false);
			memo = USBMemo;
			data.sUnit = "VDC";
			data.value = 0.0;
			data.NewData = false;
		}

		__fastcall ~USBMM_Thread(void) {
			// Cleanup, deallocate, etc.
		}

		//get current node data/output
		DMM_Data __fastcall GetCurData();
		//gather new measurement from nodes
		void __fastcall	GetNewMeasurement(void);
		//true if data is available
		bool __fastcall DataAvailable(void);

	private:

		DMM_Data data;
		USBMM_device_t device;
		DWORD BytesRead;
		TMutex *memoMux;
		TMutex *dataMux;
		TMemo *memo;

		// Allocate a memory buffer equal to our endpoint size + 1
		uint8_t InputPacketBuffer[65];

		//private function prototypes
		void __fastcall Execute(void);

		//DMM (USB Meter) function prototypes
		void __fastcall Decode_DMM(uint8_t *pData);
		void __fastcall Build_DMM_Message(uint8_t *pData);
		void __fastcall Decode_USB_Data(uint8_t *buffer, DWORD Len);
		void __fastcall Send_Message_To_Parent(AnsiString message);

};



//simple controller class to control USBMM_Threads
class USBMM_Controller {

	 public:

		// Start the thread suspended
		__fastcall USBMM_Controller(TMemo *USBLogMemo)
		{
			hHID = NULL;
			HidD_SetFeature = NULL;
			Memo = USBLogMemo;
			MemoMutex = new TMutex(false);
			devices[0].connected = false;
			devices[1].connected = false;
			devices[2].connected = false;
			devices[3].connected = false;
		}

		__fastcall ~USBMM_Controller(void) {
			Stop();
		}

		bool __fastcall Start(void);
		void __fastcall Stop(void);
		bool __fastcall MeterConnected(uint8_t meter);
		double __fastcall GetOutput(uint8_t meter);
		void __fastcall StartNewMeasurement(void);
		bool __fastcall DataAvailable(void);
		void __fastcall AddToLog(AnsiString line);

	private:

		USBMM_Thread *threads[4]; //a worker node for each multimeter
		USBMM_device_t devices[4]; //device handle storige for each node
		USBMM_report_t reportBuf;
		HINSTANCE hHID;
		PHidD_SetFeature HidD_SetFeature;

		//Memo for user messages
		TMemo *Memo;
		//Locks for shared resources
		TMutex *MemoMutex;

		//Find corresponding node and connect to it
		USBMM_device_t __fastcall Connect_Device(uint8_t device);
		USBMM_ret_t __fastcall Init_HID_DLL(void);

};


//---------------------------------------------------------------------------
#endif
